.class public final Lcom/google/o/f/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/f/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/f/b;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/f/b;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:J

.field public c:I

.field public d:I

.field public e:J

.field public f:J

.field public g:J

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/google/o/f/c;

    invoke-direct {v0}, Lcom/google/o/f/c;-><init>()V

    sput-object v0, Lcom/google/o/f/b;->PARSER:Lcom/google/n/ax;

    .line 330
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/f/b;->k:Lcom/google/n/aw;

    .line 691
    new-instance v0, Lcom/google/o/f/b;

    invoke-direct {v0}, Lcom/google/o/f/b;-><init>()V

    sput-object v0, Lcom/google/o/f/b;->h:Lcom/google/o/f/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const-wide/16 v0, 0x0

    .line 80
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 259
    iput-byte v2, p0, Lcom/google/o/f/b;->i:B

    .line 293
    iput v2, p0, Lcom/google/o/f/b;->j:I

    .line 81
    iput-wide v0, p0, Lcom/google/o/f/b;->b:J

    .line 82
    iput v3, p0, Lcom/google/o/f/b;->c:I

    .line 83
    iput v3, p0, Lcom/google/o/f/b;->d:I

    .line 84
    iput-wide v0, p0, Lcom/google/o/f/b;->e:J

    .line 85
    iput-wide v0, p0, Lcom/google/o/f/b;->f:J

    .line 86
    iput-wide v0, p0, Lcom/google/o/f/b;->g:J

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 93
    invoke-direct {p0}, Lcom/google/o/f/b;-><init>()V

    .line 94
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 98
    const/4 v0, 0x0

    .line 99
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 101
    sparse-switch v3, :sswitch_data_0

    .line 106
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 108
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/f/b;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/f/b;->au:Lcom/google/n/bn;

    throw v0

    .line 118
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/f/b;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 146
    :catch_1
    move-exception v0

    .line 147
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 148
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/f/b;->d:I

    goto :goto_0

    .line 128
    :sswitch_4
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 129
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/f/b;->e:J

    goto :goto_0

    .line 133
    :sswitch_5
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 134
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/f/b;->f:J

    goto :goto_0

    .line 138
    :sswitch_6
    iget v3, p0, Lcom/google/o/f/b;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/f/b;->a:I

    .line 139
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/f/b;->g:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/b;->au:Lcom/google/n/bn;

    .line 151
    return-void

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x31 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 78
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 259
    iput-byte v0, p0, Lcom/google/o/f/b;->i:B

    .line 293
    iput v0, p0, Lcom/google/o/f/b;->j:I

    .line 79
    return-void
.end method

.method public static a([B)Lcom/google/o/f/b;
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lcom/google/o/f/b;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->b([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/f/b;

    return-object v0
.end method

.method public static d()Lcom/google/o/f/b;
    .locals 1

    .prologue
    .line 694
    sget-object v0, Lcom/google/o/f/b;->h:Lcom/google/o/f/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/f/d;
    .locals 1

    .prologue
    .line 392
    new-instance v0, Lcom/google/o/f/d;

    invoke-direct {v0}, Lcom/google/o/f/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/f/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    sget-object v0, Lcom/google/o/f/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x5

    .line 271
    invoke-virtual {p0}, Lcom/google/o/f/b;->c()I

    .line 272
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 273
    iget-wide v0, p0, Lcom/google/o/f/b;->b:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 276
    iget v0, p0, Lcom/google/o/f/b;->c:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 278
    :cond_1
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 279
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/f/b;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 281
    :cond_2
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 282
    iget-wide v0, p0, Lcom/google/o/f/b;->e:J

    invoke-virtual {p1, v5, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 284
    :cond_3
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 285
    iget-wide v0, p0, Lcom/google/o/f/b;->f:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 287
    :cond_4
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 288
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/o/f/b;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->c(IJ)V

    .line 290
    :cond_5
    iget-object v0, p0, Lcom/google/o/f/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 291
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 261
    iget-byte v1, p0, Lcom/google/o/f/b;->i:B

    .line 262
    if-ne v1, v0, :cond_0

    .line 266
    :goto_0
    return v0

    .line 263
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 265
    :cond_1
    iput-byte v0, p0, Lcom/google/o/f/b;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 295
    iget v0, p0, Lcom/google/o/f/b;->j:I

    .line 296
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 325
    :goto_0
    return v0

    .line 299
    :cond_0
    iget v0, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 300
    iget-wide v2, p0, Lcom/google/o/f/b;->b:J

    .line 301
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 303
    :goto_1
    iget v2, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 304
    iget v2, p0, Lcom/google/o/f/b;->c:I

    .line 305
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 307
    :cond_1
    iget v2, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 308
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/o/f/b;->d:I

    .line 309
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 311
    :cond_2
    iget v2, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 312
    iget-wide v2, p0, Lcom/google/o/f/b;->e:J

    .line 313
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 315
    :cond_3
    iget v2, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 316
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/o/f/b;->f:J

    .line 317
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 319
    :cond_4
    iget v2, p0, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 320
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/o/f/b;->g:J

    .line 321
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 323
    :cond_5
    iget-object v1, p0, Lcom/google/o/f/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    iput v0, p0, Lcom/google/o/f/b;->j:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/google/o/f/b;->newBuilder()Lcom/google/o/f/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/f/d;->a(Lcom/google/o/f/b;)Lcom/google/o/f/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/google/o/f/b;->newBuilder()Lcom/google/o/f/d;

    move-result-object v0

    return-object v0
.end method
