.class public final Lcom/google/o/f/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/f/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/f/b;",
        "Lcom/google/o/f/d;",
        ">;",
        "Lcom/google/o/f/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:J

.field private f:J

.field private g:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 410
    sget-object v0, Lcom/google/o/f/b;->h:Lcom/google/o/f/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 411
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 402
    new-instance v2, Lcom/google/o/f/b;

    invoke-direct {v2, p0}, Lcom/google/o/f/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/f/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-wide v4, p0, Lcom/google/o/f/d;->f:J

    iput-wide v4, v2, Lcom/google/o/f/b;->b:J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/f/d;->b:I

    iput v1, v2, Lcom/google/o/f/b;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/o/f/d;->c:I

    iput v1, v2, Lcom/google/o/f/b;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/o/f/d;->g:J

    iput-wide v4, v2, Lcom/google/o/f/b;->e:J

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/o/f/d;->d:J

    iput-wide v4, v2, Lcom/google/o/f/b;->f:J

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v4, p0, Lcom/google/o/f/d;->e:J

    iput-wide v4, v2, Lcom/google/o/f/b;->g:J

    iput v0, v2, Lcom/google/o/f/b;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 402
    check-cast p1, Lcom/google/o/f/b;

    invoke-virtual {p0, p1}, Lcom/google/o/f/d;->a(Lcom/google/o/f/b;)Lcom/google/o/f/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/f/b;)Lcom/google/o/f/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 466
    invoke-static {}, Lcom/google/o/f/b;->d()Lcom/google/o/f/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 486
    :goto_0
    return-object p0

    .line 467
    :cond_0
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 468
    iget-wide v2, p1, Lcom/google/o/f/b;->b:J

    iget v4, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/f/d;->a:I

    iput-wide v2, p0, Lcom/google/o/f/d;->f:J

    .line 470
    :cond_1
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 471
    iget v2, p1, Lcom/google/o/f/b;->c:I

    iget v3, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/f/d;->a:I

    iput v2, p0, Lcom/google/o/f/d;->b:I

    .line 473
    :cond_2
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 474
    iget v2, p1, Lcom/google/o/f/b;->d:I

    iget v3, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/f/d;->a:I

    iput v2, p0, Lcom/google/o/f/d;->c:I

    .line 476
    :cond_3
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 477
    iget-wide v2, p1, Lcom/google/o/f/b;->e:J

    iget v4, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/f/d;->a:I

    iput-wide v2, p0, Lcom/google/o/f/d;->g:J

    .line 479
    :cond_4
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 480
    iget-wide v2, p1, Lcom/google/o/f/b;->f:J

    iget v4, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/f/d;->a:I

    iput-wide v2, p0, Lcom/google/o/f/d;->d:J

    .line 482
    :cond_5
    iget v2, p1, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 483
    iget-wide v0, p1, Lcom/google/o/f/b;->g:J

    iget v2, p0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/f/d;->a:I

    iput-wide v0, p0, Lcom/google/o/f/d;->e:J

    .line 485
    :cond_6
    iget-object v0, p1, Lcom/google/o/f/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 467
    goto :goto_1

    :cond_8
    move v2, v1

    .line 470
    goto :goto_2

    :cond_9
    move v2, v1

    .line 473
    goto :goto_3

    :cond_a
    move v2, v1

    .line 476
    goto :goto_4

    :cond_b
    move v2, v1

    .line 479
    goto :goto_5

    :cond_c
    move v0, v1

    .line 482
    goto :goto_6
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x1

    return v0
.end method
