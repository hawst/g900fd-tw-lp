.class public final Lcom/google/o/f/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/f/l;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/f/g;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/f/g;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Object;

.field e:I

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/google/o/f/h;

    invoke-direct {v0}, Lcom/google/o/f/h;-><init>()V

    sput-object v0, Lcom/google/o/f/g;->PARSER:Lcom/google/n/ax;

    .line 559
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/f/g;->l:Lcom/google/n/aw;

    .line 1165
    new-instance v0, Lcom/google/o/f/g;

    invoke-direct {v0}, Lcom/google/o/f/g;-><init>()V

    sput-object v0, Lcom/google/o/f/g;->i:Lcom/google/o/f/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 439
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    .line 455
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    .line 470
    iput-byte v2, p0, Lcom/google/o/f/g;->j:B

    .line 513
    iput v2, p0, Lcom/google/o/f/g;->k:I

    .line 104
    iput v3, p0, Lcom/google/o/f/g;->b:I

    .line 105
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    .line 107
    iput v3, p0, Lcom/google/o/f/g;->e:I

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 110
    iget-object v0, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 111
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v8, 0x2

    const/4 v0, 0x0

    .line 117
    invoke-direct {p0}, Lcom/google/o/f/g;-><init>()V

    .line 120
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 123
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 124
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 125
    sparse-switch v0, :sswitch_data_0

    .line 130
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 132
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 128
    goto :goto_0

    .line 137
    :sswitch_1
    iget v0, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/f/g;->a:I

    .line 138
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/f/g;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_1

    .line 204
    iget-object v1, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 206
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/f/g;->au:Lcom/google/n/bn;

    throw v0

    .line 142
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v8, :cond_2

    .line 143
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 144
    or-int/lit8 v1, v1, 0x2

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 199
    :catch_1
    move-exception v0

    .line 200
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 201
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 151
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 152
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v8, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_1
    if-lez v0, :cond_3

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 154
    or-int/lit8 v1, v1, 0x2

    .line 156
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_5

    move v0, v2

    :goto_3
    if-lez v0, :cond_6

    .line 157
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 152
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 156
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 159
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 163
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 164
    iget v6, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/o/f/g;->a:I

    .line 165
    iput-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 169
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 170
    invoke-static {v0}, Lcom/google/o/f/j;->a(I)Lcom/google/o/f/j;

    move-result-object v6

    .line 171
    if-nez v6, :cond_7

    .line 172
    const/4 v6, 0x6

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 174
    :cond_7
    iget v6, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/o/f/g;->a:I

    .line 175
    iput v0, p0, Lcom/google/o/f/g;->e:I

    goto/16 :goto_0

    .line 180
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 181
    iget v6, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/o/f/g;->a:I

    .line 182
    iput-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 186
    :sswitch_7
    iget-object v0, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 187
    iget v0, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/f/g;->a:I

    goto/16 :goto_0

    .line 191
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 192
    iget v0, p0, Lcom/google/o/f/g;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/f/g;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 203
    :cond_8
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_9

    .line 204
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 206
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->au:Lcom/google/n/bn;

    .line 207
    return-void

    .line 125
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x8a -> :sswitch_6
        0x21a -> :sswitch_7
        0x74a -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 101
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 439
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    .line 455
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    .line 470
    iput-byte v1, p0, Lcom/google/o/f/g;->j:B

    .line 513
    iput v1, p0, Lcom/google/o/f/g;->k:I

    .line 102
    return-void
.end method

.method public static d()Lcom/google/o/f/g;
    .locals 1

    .prologue
    .line 1168
    sget-object v0, Lcom/google/o/f/g;->i:Lcom/google/o/f/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/f/i;
    .locals 1

    .prologue
    .line 621
    new-instance v0, Lcom/google/o/f/i;

    invoke-direct {v0}, Lcom/google/o/f/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/f/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    sget-object v0, Lcom/google/o/f/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 488
    invoke-virtual {p0}, Lcom/google/o/f/g;->c()I

    .line 489
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 490
    iget v0, p0, Lcom/google/o/f/g;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 492
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 495
    :cond_1
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 496
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 498
    :cond_2
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 499
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/f/g;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 501
    :cond_3
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 502
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 504
    :cond_4
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 505
    const/16 v0, 0x43

    iget-object v1, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 507
    :cond_5
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 508
    const/16 v0, 0xe9

    iget-object v1, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 510
    :cond_6
    iget-object v0, p0, Lcom/google/o/f/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 511
    return-void

    .line 496
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 502
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 472
    iget-byte v0, p0, Lcom/google/o/f/g;->j:B

    .line 473
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 483
    :goto_0
    return v0

    .line 474
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 476
    :cond_1
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 477
    iget-object v0, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ay;->d()Lcom/google/b/f/ay;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ay;

    invoke-virtual {v0}, Lcom/google/b/f/ay;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 478
    iput-byte v2, p0, Lcom/google/o/f/g;->j:B

    move v0, v2

    .line 479
    goto :goto_0

    :cond_2
    move v0, v2

    .line 476
    goto :goto_1

    .line 482
    :cond_3
    iput-byte v1, p0, Lcom/google/o/f/g;->j:B

    move v0, v1

    .line 483
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 515
    iget v0, p0, Lcom/google/o/f/g;->k:I

    .line 516
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 554
    :goto_0
    return v0

    .line 519
    :cond_0
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 520
    iget v0, p0, Lcom/google/o/f/g;->b:I

    .line 521
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    :goto_2
    move v4, v3

    move v5, v3

    .line 525
    :goto_3
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 526
    iget-object v0, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    .line 527
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 525
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 521
    goto :goto_1

    :cond_2
    move v0, v1

    .line 527
    goto :goto_4

    .line 529
    :cond_3
    add-int v0, v2, v5

    .line 530
    iget-object v2, p0, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 532
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_b

    .line 533
    const/4 v4, 0x5

    .line 534
    iget-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    .line 536
    :goto_6
    iget v2, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_a

    .line 537
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/o/f/g;->e:I

    .line 538
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_4
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    move v1, v0

    .line 540
    :goto_7
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_5

    .line 541
    const/16 v2, 0x11

    .line 542
    iget-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    :goto_8
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 544
    :cond_5
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_6

    .line 545
    const/16 v0, 0x43

    iget-object v2, p0, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    .line 546
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 548
    :cond_6
    iget v0, p0, Lcom/google/o/f/g;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 549
    const/16 v0, 0xe9

    iget-object v2, p0, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    .line 550
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 552
    :cond_7
    iget-object v0, p0, Lcom/google/o/f/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 553
    iput v0, p0, Lcom/google/o/f/g;->k:I

    goto/16 :goto_0

    .line 534
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 542
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_a
    move v1, v0

    goto :goto_7

    :cond_b
    move v0, v2

    goto/16 :goto_6

    :cond_c
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/google/o/f/g;->newBuilder()Lcom/google/o/f/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/f/i;->a(Lcom/google/o/f/g;)Lcom/google/o/f/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/google/o/f/g;->newBuilder()Lcom/google/o/f/i;

    move-result-object v0

    return-object v0
.end method
