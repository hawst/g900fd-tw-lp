.class public final Lcom/google/o/f/i;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/f/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/f/g;",
        "Lcom/google/o/f/i;",
        ">;",
        "Lcom/google/o/f/l;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 639
    sget-object v0, Lcom/google/o/f/g;->i:Lcom/google/o/f/g;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 789
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    .line 855
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/f/i;->d:Ljava/lang/Object;

    .line 931
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/f/i;->e:I

    .line 967
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/f/i;->f:Ljava/lang/Object;

    .line 1043
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/i;->g:Lcom/google/n/ao;

    .line 1102
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/f/i;->h:Lcom/google/n/ao;

    .line 640
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 631
    new-instance v2, Lcom/google/o/f/g;

    invoke-direct {v2, p0}, Lcom/google/o/f/g;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v4, p0, Lcom/google/o/f/i;->b:I

    iput v4, v2, Lcom/google/o/f/g;->b:I

    iget v4, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/f/i;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/f/g;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, p0, Lcom/google/o/f/i;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/o/f/i;->e:I

    iput v4, v2, Lcom/google/o/f/g;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/o/f/i;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/f/i;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/f/i;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v3, v2, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/f/i;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/f/i;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/f/g;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 631
    check-cast p1, Lcom/google/o/f/g;

    invoke-virtual {p0, p1}, Lcom/google/o/f/i;->a(Lcom/google/o/f/g;)Lcom/google/o/f/i;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/f/g;)Lcom/google/o/f/i;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 706
    invoke-static {}, Lcom/google/o/f/g;->d()Lcom/google/o/f/g;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 742
    :goto_0
    return-object p0

    .line 707
    :cond_0
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 708
    iget v2, p1, Lcom/google/o/f/g;->b:I

    iget v3, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/f/i;->a:I

    iput v2, p0, Lcom/google/o/f/i;->b:I

    .line 710
    :cond_1
    iget-object v2, p1, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 711
    iget-object v2, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 712
    iget-object v2, p1, Lcom/google/o/f/g;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    .line 713
    iget v2, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/f/i;->a:I

    .line 720
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 721
    iget v2, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/f/i;->a:I

    .line 722
    iget-object v2, p1, Lcom/google/o/f/g;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/f/i;->d:Ljava/lang/Object;

    .line 725
    :cond_3
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 726
    iget v2, p1, Lcom/google/o/f/g;->e:I

    invoke-static {v2}, Lcom/google/o/f/j;->a(I)Lcom/google/o/f/j;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/o/f/j;->a:Lcom/google/o/f/j;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 707
    goto :goto_1

    .line 715
    :cond_6
    iget v2, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/f/i;->a:I

    .line 716
    :cond_7
    iget-object v2, p0, Lcom/google/o/f/i;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/f/g;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    move v2, v1

    .line 720
    goto :goto_3

    :cond_9
    move v2, v1

    .line 725
    goto :goto_4

    .line 726
    :cond_a
    iget v3, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/f/i;->a:I

    iget v2, v2, Lcom/google/o/f/j;->e:I

    iput v2, p0, Lcom/google/o/f/i;->e:I

    .line 728
    :cond_b
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 729
    iget v2, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/f/i;->a:I

    .line 730
    iget-object v2, p1, Lcom/google/o/f/g;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/f/i;->f:Ljava/lang/Object;

    .line 733
    :cond_c
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 734
    iget-object v2, p0, Lcom/google/o/f/i;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/f/g;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 735
    iget v2, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/f/i;->a:I

    .line 737
    :cond_d
    iget v2, p1, Lcom/google/o/f/g;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    :goto_7
    if-eqz v0, :cond_e

    .line 738
    iget-object v0, p0, Lcom/google/o/f/i;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/f/g;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 739
    iget v0, p0, Lcom/google/o/f/i;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/f/i;->a:I

    .line 741
    :cond_e
    iget-object v0, p1, Lcom/google/o/f/g;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 728
    goto :goto_5

    :cond_10
    move v2, v1

    .line 733
    goto :goto_6

    :cond_11
    move v0, v1

    .line 737
    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 746
    iget v0, p0, Lcom/google/o/f/i;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 747
    iget-object v0, p0, Lcom/google/o/f/i;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ay;->d()Lcom/google/b/f/ay;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ay;

    invoke-virtual {v0}, Lcom/google/b/f/ay;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 752
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 746
    goto :goto_0

    :cond_1
    move v0, v2

    .line 752
    goto :goto_1
.end method
