.class public final Lcom/google/o/e/d;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/e/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/e/b;",
        "Lcom/google/o/e/d;",
        ">;",
        "Lcom/google/o/e/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/google/o/e/b;->d:Lcom/google/o/e/b;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 379
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    .line 438
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/e/d;->b:Ljava/lang/Object;

    .line 315
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 307
    new-instance v2, Lcom/google/o/e/b;

    invoke-direct {v2, p0}, Lcom/google/o/e/b;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/e/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/e/d;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/e/b;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 307
    check-cast p1, Lcom/google/o/e/b;

    invoke-virtual {p0, p1}, Lcom/google/o/e/d;->a(Lcom/google/o/e/b;)Lcom/google/o/e/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/d/a/a/ds;)Lcom/google/o/e/d;
    .locals 2

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 395
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 399
    iget v0, p0, Lcom/google/o/e/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/e/d;->a:I

    .line 400
    return-object p0
.end method

.method public final a(Lcom/google/o/e/b;)Lcom/google/o/e/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 348
    invoke-static {}, Lcom/google/o/e/b;->h()Lcom/google/o/e/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 360
    :goto_0
    return-object p0

    .line 349
    :cond_0
    iget v2, p1, Lcom/google/o/e/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 350
    iget-object v2, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 351
    iget v2, p0, Lcom/google/o/e/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/e/d;->a:I

    .line 353
    :cond_1
    iget v2, p1, Lcom/google/o/e/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 354
    iget v0, p0, Lcom/google/o/e/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/e/d;->a:I

    .line 355
    iget-object v0, p1, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/e/d;->b:Ljava/lang/Object;

    .line 358
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/o/e/d;->a(Lcom/google/n/x;)V

    .line 359
    iget-object v0, p1, Lcom/google/o/e/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 349
    goto :goto_1

    :cond_4
    move v0, v1

    .line 353
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 364
    iget v0, p0, Lcom/google/o/e/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/google/o/e/d;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 374
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 364
    goto :goto_0

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 372
    goto :goto_1

    :cond_2
    move v0, v2

    .line 374
    goto :goto_1
.end method
