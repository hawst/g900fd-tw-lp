.class public final Lcom/google/o/e/b;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/e/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/e/b;",
        ">;",
        "Lcom/google/o/e/e;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/e/b;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/e/b;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/o/e/c;

    invoke-direct {v0}, Lcom/google/o/e/c;-><init>()V

    sput-object v0, Lcom/google/o/e/b;->PARSER:Lcom/google/n/ax;

    .line 235
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/e/b;->g:Lcom/google/n/aw;

    .line 518
    new-instance v0, Lcom/google/o/e/b;

    invoke-direct {v0}, Lcom/google/o/e/b;-><init>()V

    sput-object v0, Lcom/google/o/e/b;->d:Lcom/google/o/e/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 120
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    .line 177
    iput-byte v2, p0, Lcom/google/o/e/b;->e:B

    .line 213
    iput v2, p0, Lcom/google/o/e/b;->f:I

    .line 52
    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    .line 54
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 60
    invoke-direct {p0}, Lcom/google/o/e/b;-><init>()V

    .line 61
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 66
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 67
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 68
    sparse-switch v5, :sswitch_data_0

    .line 73
    iget-object v0, p0, Lcom/google/o/e/b;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/e/b;->d:Lcom/google/o/e/b;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/e/b;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 76
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 71
    goto :goto_0

    .line 81
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 82
    iget v0, p0, Lcom/google/o/e/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/e/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/e/b;->au:Lcom/google/n/bn;

    .line 100
    iget-object v1, p0, Lcom/google/o/e/b;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 86
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 87
    iget v1, p0, Lcom/google/o/e/b;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/e/b;->a:I

    .line 88
    iput-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    .line 96
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 97
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/e/b;->au:Lcom/google/n/bn;

    .line 100
    iget-object v0, p0, Lcom/google/o/e/b;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 101
    :cond_3
    return-void

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/e/b;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 49
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 120
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    .line 177
    iput-byte v1, p0, Lcom/google/o/e/b;->e:B

    .line 213
    iput v1, p0, Lcom/google/o/e/b;->f:I

    .line 50
    return-void
.end method

.method public static h()Lcom/google/o/e/b;
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lcom/google/o/e/b;->d:Lcom/google/o/e/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/e/d;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/google/o/e/d;

    invoke-direct {v0}, Lcom/google/o/e/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    sget-object v0, Lcom/google/o/e/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 199
    invoke-virtual {p0}, Lcom/google/o/e/b;->c()I

    .line 202
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 203
    iget v0, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 206
    :cond_0
    iget v0, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 209
    :cond_1
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 210
    iget-object v0, p0, Lcom/google/o/e/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 211
    return-void

    .line 207
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-byte v0, p0, Lcom/google/o/e/b;->e:B

    .line 180
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 194
    :goto_0
    return v0

    .line 181
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 183
    :cond_1
    iget v0, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 185
    iput-byte v2, p0, Lcom/google/o/e/b;->e:B

    move v0, v2

    .line 186
    goto :goto_0

    :cond_2
    move v0, v2

    .line 183
    goto :goto_1

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 190
    iput-byte v2, p0, Lcom/google/o/e/b;->e:B

    move v0, v2

    .line 191
    goto :goto_0

    .line 193
    :cond_4
    iput-byte v1, p0, Lcom/google/o/e/b;->e:B

    move v0, v1

    .line 194
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 215
    iget v0, p0, Lcom/google/o/e/b;->f:I

    .line 216
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 230
    :goto_0
    return v0

    .line 219
    :cond_0
    iget v0, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 220
    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    .line 221
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 223
    :goto_1
    iget v0, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/google/o/e/b;->g()I

    move-result v0

    add-int/2addr v0, v1

    .line 228
    iget-object v1, p0, Lcom/google/o/e/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    iput v0, p0, Lcom/google/o/e/b;->f:I

    goto :goto_0

    .line 225
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    .line 148
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 149
    check-cast v0, Ljava/lang/String;

    .line 157
    :goto_0
    return-object v0

    .line 151
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 153
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iput-object v1, p0, Lcom/google/o/e/b;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 157
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/google/o/e/b;->newBuilder()Lcom/google/o/e/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/e/d;->a(Lcom/google/o/e/b;)Lcom/google/o/e/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/google/o/e/b;->newBuilder()Lcom/google/o/e/d;

    move-result-object v0

    return-object v0
.end method
