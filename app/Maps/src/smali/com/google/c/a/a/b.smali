.class public final Lcom/google/c/a/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/c/a/a/e;


# static fields
.field static final A:Lcom/google/c/a/a/b;

.field private static volatile D:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/c/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field private B:B

.field private C:I

.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Z

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:I

.field i:Ljava/lang/Object;

.field j:Ljava/lang/Object;

.field k:I

.field l:Lcom/google/n/f;

.field m:Ljava/lang/Object;

.field n:Lcom/google/n/ao;

.field o:Ljava/lang/Object;

.field p:Ljava/lang/Object;

.field q:Ljava/lang/Object;

.field r:Ljava/lang/Object;

.field s:Ljava/lang/Object;

.field t:Ljava/lang/Object;

.field u:Ljava/lang/Object;

.field v:Lcom/google/n/aq;

.field w:Lcom/google/n/aq;

.field x:I

.field y:Z

.field z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568
    new-instance v0, Lcom/google/c/a/a/c;

    invoke-direct {v0}, Lcom/google/c/a/a/c;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b;->PARSER:Lcom/google/n/ax;

    .line 1614
    const/4 v0, 0x0

    sput-object v0, Lcom/google/c/a/a/b;->D:Lcom/google/n/aw;

    .line 3625
    new-instance v0, Lcom/google/c/a/a/b;

    invoke-direct {v0}, Lcom/google/c/a/a/b;-><init>()V

    sput-object v0, Lcom/google/c/a/a/b;->A:Lcom/google/c/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 346
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 982
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    .line 1394
    iput-byte v1, p0, Lcom/google/c/a/a/b;->B:B

    .line 1491
    iput v1, p0, Lcom/google/c/a/a/b;->C:I

    .line 347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;

    .line 348
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;

    .line 349
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    .line 350
    iput-boolean v2, p0, Lcom/google/c/a/a/b;->e:Z

    .line 351
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    .line 352
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    .line 353
    iput v1, p0, Lcom/google/c/a/a/b;->h:I

    .line 354
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    .line 355
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    .line 356
    const/16 v0, 0x21c

    iput v0, p0, Lcom/google/c/a/a/b;->k:I

    .line 357
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/c/a/a/b;->l:Lcom/google/n/f;

    .line 358
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    .line 359
    iget-object v0, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 360
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    .line 361
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    .line 362
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    .line 363
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    .line 364
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    .line 365
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    .line 366
    const-string v0, ""

    iput-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    .line 367
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    .line 368
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    .line 369
    iput v2, p0, Lcom/google/c/a/a/b;->x:I

    .line 370
    iput-boolean v2, p0, Lcom/google/c/a/a/b;->y:Z

    .line 371
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/c/a/a/b;->z:J

    .line 372
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v9, 0x200000

    const/high16 v8, 0x100000

    .line 378
    invoke-direct {p0}, Lcom/google/c/a/a/b;-><init>()V

    .line 381
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 384
    :cond_0
    :goto_0
    if-nez v4, :cond_8

    .line 385
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 386
    sparse-switch v0, :sswitch_data_0

    .line 391
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 393
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 389
    goto :goto_0

    .line 398
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 399
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 400
    iput-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 553
    :catch_0
    move-exception v0

    .line 554
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 559
    :catchall_0
    move-exception v0

    and-int v2, v1, v8

    if-ne v2, v8, :cond_1

    .line 560
    iget-object v2, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    .line 562
    :cond_1
    and-int/2addr v1, v9

    if-ne v1, v9, :cond_2

    .line 563
    iget-object v1, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    .line 565
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/c/a/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 404
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 405
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 406
    iput-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 555
    :catch_1
    move-exception v0

    .line 556
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 557
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 410
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 411
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 412
    iput-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    goto :goto_0

    .line 416
    :sswitch_4
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 417
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/c/a/a/b;->e:Z

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 421
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 422
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 423
    iput-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 427
    :sswitch_6
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 428
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/c/a/a/b;->h:I

    goto/16 :goto_0

    .line 432
    :sswitch_7
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 433
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->l:Lcom/google/n/f;

    goto/16 :goto_0

    .line 437
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 438
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 439
    iput-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 443
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 444
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 445
    iput-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 449
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 450
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 451
    iput-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 455
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 456
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 457
    iput-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 461
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 462
    invoke-static {v0}, Lcom/google/c/b/a/e;->a(I)Lcom/google/c/b/a/e;

    move-result-object v6

    .line 463
    if-nez v6, :cond_4

    .line 464
    const/16 v6, 0xd

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 466
    :cond_4
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 467
    iput v0, p0, Lcom/google/c/a/a/b;->k:I

    goto/16 :goto_0

    .line 472
    :sswitch_d
    iget-object v0, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 473
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    goto/16 :goto_0

    .line 477
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 478
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 479
    iput-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 483
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 484
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 485
    iput-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 489
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 490
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 491
    iput-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 495
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 496
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 497
    iput-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    goto/16 :goto_0

    .line 501
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 502
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 503
    iput-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    goto/16 :goto_0

    .line 507
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 508
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v7, 0x40000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 509
    iput-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    goto/16 :goto_0

    .line 513
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 514
    iget v6, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/c/a/a/b;->a:I

    .line 515
    iput-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    goto/16 :goto_0

    .line 519
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 520
    and-int v6, v1, v8

    if-eq v6, v8, :cond_5

    .line 521
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    .line 522
    or-int/2addr v1, v8

    .line 524
    :cond_5
    iget-object v6, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 528
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 529
    and-int v6, v1, v9

    if-eq v6, v9, :cond_6

    .line 530
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    .line 531
    or-int/2addr v1, v9

    .line 533
    :cond_6
    iget-object v6, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 537
    :sswitch_17
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/2addr v0, v8

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 538
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/c/a/a/b;->x:I

    goto/16 :goto_0

    .line 542
    :sswitch_18
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    or-int/2addr v0, v9

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 543
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_7

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/c/a/a/b;->y:Z

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_2

    .line 547
    :sswitch_19
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/c/a/a/b;->a:I

    .line 548
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/c/a/a/b;->z:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 559
    :cond_8
    and-int v0, v1, v8

    if-ne v0, v8, :cond_9

    .line 560
    iget-object v0, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    .line 562
    :cond_9
    and-int v0, v1, v9

    if-ne v0, v9, :cond_a

    .line 563
    iget-object v0, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    .line 565
    :cond_a
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->au:Lcom/google/n/bn;

    .line 566
    return-void

    .line 386
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc0 -> :sswitch_17
        0xc8 -> :sswitch_18
        0xd0 -> :sswitch_19
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 344
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 982
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    .line 1394
    iput-byte v1, p0, Lcom/google/c/a/a/b;->B:B

    .line 1491
    iput v1, p0, Lcom/google/c/a/a/b;->C:I

    .line 345
    return-void
.end method

.method public static d()Lcom/google/c/a/a/b;
    .locals 1

    .prologue
    .line 3628
    sget-object v0, Lcom/google/c/a/a/b;->A:Lcom/google/c/a/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/c/a/a/d;
    .locals 1

    .prologue
    .line 1676
    new-instance v0, Lcom/google/c/a/a/d;

    invoke-direct {v0}, Lcom/google/c/a/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/c/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    sget-object v0, Lcom/google/c/a/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 1412
    invoke-virtual {p0}, Lcom/google/c/a/a/b;->c()I

    .line 1413
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1416
    :cond_0
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1417
    iget-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1419
    :cond_1
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1420
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1422
    :cond_2
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 1423
    iget-boolean v0, p0, Lcom/google/c/a/a/b;->e:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_17

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1425
    :cond_3
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1426
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1428
    :cond_4
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 1429
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/c/a/a/b;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_19

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1431
    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_6

    .line 1432
    iget-object v0, p0, Lcom/google/c/a/a/b;->l:Lcom/google/n/f;

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1434
    :cond_6
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 1435
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1437
    :cond_7
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    .line 1438
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1440
    :cond_8
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_9

    .line 1441
    const/16 v3, 0xb

    iget-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1443
    :cond_9
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_a

    .line 1444
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1446
    :cond_a
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_b

    .line 1447
    const/16 v0, 0xd

    iget v3, p0, Lcom/google/c/a/a/b;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_1e

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1449
    :cond_b
    :goto_a
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_c

    .line 1450
    const/16 v0, 0xe

    iget-object v3, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1452
    :cond_c
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_d

    .line 1453
    const/16 v3, 0xf

    iget-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    :goto_b
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1455
    :cond_d
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_e

    .line 1456
    const/16 v3, 0x10

    iget-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_20

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    :goto_c
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1458
    :cond_e
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_f

    .line 1459
    const/16 v3, 0x11

    iget-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_21

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    :goto_d
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1461
    :cond_f
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_10

    .line 1462
    const/16 v3, 0x12

    iget-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    :goto_e
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1464
    :cond_10
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_11

    .line 1465
    const/16 v3, 0x13

    iget-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_23

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    :goto_f
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1467
    :cond_11
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_12

    .line 1468
    const/16 v3, 0x14

    iget-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_24

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    :goto_10
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1470
    :cond_12
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_13

    .line 1471
    const/16 v3, 0x15

    iget-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_25

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    :goto_11
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_13
    move v0, v2

    .line 1473
    :goto_12
    iget-object v3, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_26

    .line 1474
    const/16 v3, 0x16

    iget-object v4, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1473
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 1414
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1417
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1420
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    :cond_17
    move v0, v2

    .line 1423
    goto/16 :goto_3

    .line 1426
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 1429
    :cond_19
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 1435
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1438
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 1441
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 1444
    :cond_1d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 1447
    :cond_1e
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_a

    .line 1453
    :cond_1f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_b

    .line 1456
    :cond_20
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    .line 1459
    :cond_21
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_d

    .line 1462
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    .line 1465
    :cond_23
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_f

    .line 1468
    :cond_24
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_10

    .line 1471
    :cond_25
    check-cast v0, Lcom/google/n/f;

    goto :goto_11

    :cond_26
    move v0, v2

    .line 1476
    :goto_13
    iget-object v3, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_27

    .line 1477
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1476
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1479
    :cond_27
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_28

    .line 1480
    const/16 v0, 0x18

    iget v3, p0, Lcom/google/c/a/a/b;->x:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_2b

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1482
    :cond_28
    :goto_14
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_29

    .line 1483
    const/16 v0, 0x19

    iget-boolean v3, p0, Lcom/google/c/a/a/b;->y:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_2c

    :goto_15
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 1485
    :cond_29
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_2a

    .line 1486
    const/16 v0, 0x1a

    iget-wide v4, p0, Lcom/google/c/a/a/b;->z:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 1488
    :cond_2a
    iget-object v0, p0, Lcom/google/c/a/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1489
    return-void

    .line 1480
    :cond_2b
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_14

    :cond_2c
    move v1, v2

    .line 1483
    goto :goto_15
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1396
    iget-byte v0, p0, Lcom/google/c/a/a/b;->B:B

    .line 1397
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1407
    :goto_0
    return v0

    .line 1398
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1400
    :cond_1
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1401
    iget-object v0, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ch;->d()Lcom/google/q/a/ch;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ch;

    invoke-virtual {v0}, Lcom/google/q/a/ch;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1402
    iput-byte v2, p0, Lcom/google/c/a/a/b;->B:B

    move v0, v2

    .line 1403
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1400
    goto :goto_1

    .line 1406
    :cond_3
    iput-byte v1, p0, Lcom/google/c/a/a/b;->B:B

    move v0, v1

    .line 1407
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 1493
    iget v0, p0, Lcom/google/c/a/a/b;->C:I

    .line 1494
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1609
    :goto_0
    return v0

    .line 1497
    :cond_0
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_2b

    .line 1499
    iget-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1501
    :goto_2
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1503
    iget-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1505
    :cond_1
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 1506
    const/4 v4, 0x3

    .line 1507
    iget-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1509
    :cond_2
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 1510
    iget-boolean v0, p0, Lcom/google/c/a/a/b;->e:Z

    .line 1511
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1513
    :cond_3
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_4

    .line 1514
    const/4 v4, 0x5

    .line 1515
    iget-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1517
    :cond_4
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 1518
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/c/a/a/b;->h:I

    .line 1519
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_18

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 1521
    :cond_5
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_6

    .line 1522
    const/16 v0, 0x8

    iget-object v4, p0, Lcom/google/c/a/a/b;->l:Lcom/google/n/f;

    .line 1523
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1525
    :cond_6
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_7

    .line 1526
    const/16 v4, 0x9

    .line 1527
    iget-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->i:Ljava/lang/Object;

    :goto_7
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1529
    :cond_7
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_8

    .line 1531
    iget-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->g:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1533
    :cond_8
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_9

    .line 1534
    const/16 v4, 0xb

    .line 1535
    iget-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->m:Ljava/lang/Object;

    :goto_9
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1537
    :cond_9
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_a

    .line 1538
    const/16 v4, 0xc

    .line 1539
    iget-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->j:Ljava/lang/Object;

    :goto_a
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1541
    :cond_a
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_b

    .line 1542
    const/16 v0, 0xd

    iget v4, p0, Lcom/google/c/a/a/b;->k:I

    .line 1543
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_b
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 1545
    :cond_b
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_c

    .line 1546
    const/16 v0, 0xe

    iget-object v4, p0, Lcom/google/c/a/a/b;->n:Lcom/google/n/ao;

    .line 1547
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1549
    :cond_c
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_d

    .line 1550
    const/16 v4, 0xf

    .line 1551
    iget-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->o:Ljava/lang/Object;

    :goto_c
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1553
    :cond_d
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_e

    .line 1554
    const/16 v4, 0x10

    .line 1555
    iget-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->p:Ljava/lang/Object;

    :goto_d
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1557
    :cond_e
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_f

    .line 1558
    const/16 v4, 0x11

    .line 1559
    iget-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_20

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->q:Ljava/lang/Object;

    :goto_e
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1561
    :cond_f
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_10

    .line 1562
    const/16 v4, 0x12

    .line 1563
    iget-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_21

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->r:Ljava/lang/Object;

    :goto_f
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1565
    :cond_10
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v4, 0x20000

    and-int/2addr v0, v4

    const/high16 v4, 0x20000

    if-ne v0, v4, :cond_11

    .line 1566
    const/16 v4, 0x13

    .line 1567
    iget-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->s:Ljava/lang/Object;

    :goto_10
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1569
    :cond_11
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v4, 0x40000

    and-int/2addr v0, v4

    const/high16 v4, 0x40000

    if-ne v0, v4, :cond_12

    .line 1570
    const/16 v4, 0x14

    .line 1571
    iget-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_23

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->t:Ljava/lang/Object;

    :goto_11
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1573
    :cond_12
    iget v0, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v4, 0x80000

    and-int/2addr v0, v4

    const/high16 v4, 0x80000

    if-ne v0, v4, :cond_13

    .line 1574
    const/16 v4, 0x15

    .line 1575
    iget-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_24

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b;->u:Ljava/lang/Object;

    :goto_12
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    :cond_13
    move v0, v2

    move v4, v2

    .line 1579
    :goto_13
    iget-object v5, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_25

    .line 1580
    iget-object v5, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    .line 1581
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 1579
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1499
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1503
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1507
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 1515
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_18
    move v0, v3

    .line 1519
    goto/16 :goto_6

    .line 1527
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 1531
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 1535
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 1539
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    :cond_1d
    move v0, v3

    .line 1543
    goto/16 :goto_b

    .line 1551
    :cond_1e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    .line 1555
    :cond_1f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_d

    .line 1559
    :cond_20
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    .line 1563
    :cond_21
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_f

    .line 1567
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_10

    .line 1571
    :cond_23
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_11

    .line 1575
    :cond_24
    check-cast v0, Lcom/google/n/f;

    goto :goto_12

    .line 1583
    :cond_25
    add-int v0, v1, v4

    .line 1584
    iget-object v1, p0, Lcom/google/c/a/a/b;->v:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int v4, v0, v1

    move v0, v2

    move v1, v2

    .line 1588
    :goto_14
    iget-object v5, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_26

    .line 1589
    iget-object v5, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    .line 1590
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v1, v5

    .line 1588
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 1592
    :cond_26
    add-int v0, v4, v1

    .line 1593
    iget-object v1, p0, Lcom/google/c/a/a/b;->w:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1595
    iget v1, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v4, 0x100000

    and-int/2addr v1, v4

    const/high16 v4, 0x100000

    if-ne v1, v4, :cond_28

    .line 1596
    const/16 v1, 0x18

    iget v4, p0, Lcom/google/c/a/a/b;->x:I

    .line 1597
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_27

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_27
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1599
    :cond_28
    iget v1, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v1, v3

    const/high16 v3, 0x200000

    if-ne v1, v3, :cond_29

    .line 1600
    const/16 v1, 0x19

    iget-boolean v3, p0, Lcom/google/c/a/a/b;->y:Z

    .line 1601
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1603
    :cond_29
    iget v1, p0, Lcom/google/c/a/a/b;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v1, v3

    const/high16 v3, 0x400000

    if-ne v1, v3, :cond_2a

    .line 1604
    const/16 v1, 0x1a

    iget-wide v4, p0, Lcom/google/c/a/a/b;->z:J

    .line 1605
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1607
    :cond_2a
    iget-object v1, p0, Lcom/google/c/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1608
    iput v0, p0, Lcom/google/c/a/a/b;->C:I

    goto/16 :goto_0

    :cond_2b
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 338
    invoke-static {}, Lcom/google/c/a/a/b;->newBuilder()Lcom/google/c/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/c/a/a/d;->a(Lcom/google/c/a/a/b;)Lcom/google/c/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 338
    invoke-static {}, Lcom/google/c/a/a/b;->newBuilder()Lcom/google/c/a/a/d;

    move-result-object v0

    return-object v0
.end method
