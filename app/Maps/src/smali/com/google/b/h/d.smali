.class public final Lcom/google/b/h/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[J

.field private static final b:[I

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const-wide/16 v4, -0x1

    const/4 v12, 0x1

    const-wide/16 v2, 0x0

    const/16 v1, 0x25

    .line 383
    new-array v0, v1, [J

    sput-object v0, Lcom/google/b/h/d;->a:[J

    .line 384
    new-array v0, v1, [I

    sput-object v0, Lcom/google/b/h/d;->b:[I

    .line 385
    new-array v0, v1, [I

    sput-object v0, Lcom/google/b/h/d;->c:[I

    .line 387
    new-instance v7, Ljava/math/BigInteger;

    const-string v0, "10000000000000000"

    const/16 v1, 0x10

    invoke-direct {v7, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 388
    const/4 v0, 0x2

    move v6, v0

    :goto_0
    const/16 v0, 0x24

    if-gt v6, v0, :cond_4

    .line 389
    sget-object v0, Lcom/google/b/h/d;->a:[J

    int-to-long v8, v6

    invoke-static {v4, v5, v8, v9}, Lcom/google/b/h/d;->b(JJ)J

    move-result-wide v8

    aput-wide v8, v0, v6

    .line 390
    sget-object v8, Lcom/google/b/h/d;->b:[I

    int-to-long v0, v6

    cmp-long v9, v0, v2

    if-gez v9, :cond_1

    invoke-static {v4, v5, v0, v1}, Lcom/google/b/h/d;->a(JJ)I

    move-result v9

    if-gez v9, :cond_0

    move-wide v0, v4

    :goto_1
    long-to-int v0, v0

    aput v0, v8, v6

    .line 391
    sget-object v0, Lcom/google/b/h/d;->c:[I

    invoke-virtual {v7, v6}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v6

    .line 388
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 390
    :cond_0
    sub-long v0, v4, v0

    goto :goto_1

    :cond_1
    cmp-long v9, v4, v2

    if-ltz v9, :cond_2

    rem-long v0, v4, v0

    goto :goto_1

    :cond_2
    ushr-long v10, v4, v12

    div-long/2addr v10, v0

    shl-long/2addr v10, v12

    mul-long/2addr v10, v0

    sub-long v10, v4, v10

    invoke-static {v10, v11, v0, v1}, Lcom/google/b/h/d;->a(JJ)I

    move-result v9

    if-ltz v9, :cond_3

    :goto_2
    sub-long v0, v10, v0

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_2

    .line 393
    :cond_4
    return-void
.end method

.method public static a(JJ)I
    .locals 6

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 76
    xor-long v0, p0, v2

    xor-long/2addr v2, p2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 249
    const/16 v8, 0xa

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NumberFormatException;

    const-string v1, "empty string"

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lcom/google/b/h/d;->c:[I

    aget v0, v0, v8

    add-int/lit8 v9, v0, -0x1

    move v0, v1

    move-wide v2, v4

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_8

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, v8}, Ljava/lang/Character;->digit(CI)I

    move-result v10

    const/4 v6, -0x1

    if-ne v10, v6, :cond_2

    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0, p0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-le v0, v9, :cond_7

    cmp-long v6, v2, v4

    if-ltz v6, :cond_6

    sget-object v6, Lcom/google/b/h/d;->a:[J

    aget-wide v12, v6, v8

    cmp-long v6, v2, v12

    if-gez v6, :cond_3

    move v6, v1

    :goto_1
    if-eqz v6, :cond_7

    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Too large for unsigned long: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    sget-object v6, Lcom/google/b/h/d;->a:[J

    aget-wide v12, v6, v8

    cmp-long v6, v2, v12

    if-lez v6, :cond_4

    move v6, v7

    goto :goto_1

    :cond_4
    sget-object v6, Lcom/google/b/h/d;->b:[I

    aget v6, v6, v8

    if-le v10, v6, :cond_5

    move v6, v7

    goto :goto_1

    :cond_5
    move v6, v1

    goto :goto_1

    :cond_6
    move v6, v7

    goto :goto_1

    :cond_7
    int-to-long v12, v8

    mul-long/2addr v2, v12

    int-to-long v10, v10

    add-long/2addr v2, v10

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    return-wide v2
.end method

.method public static a(J)Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v1, 0x40

    const/16 v0, 0x3f

    const-wide/16 v8, 0x0

    .line 343
    const/16 v4, 0xa

    const-string v2, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    cmp-long v2, p0, v8

    if-nez v2, :cond_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_0
    new-array v5, v1, [C

    cmp-long v2, p0, v8

    if-gez v2, :cond_1

    int-to-long v2, v4

    invoke-static {p0, p1, v2, v3}, Lcom/google/b/h/d;->b(JJ)J

    move-result-wide v2

    int-to-long v6, v4

    mul-long/2addr v6, v2

    sub-long v6, p0, v6

    long-to-int v1, v6

    invoke-static {v1, v4}, Ljava/lang/Character;->forDigit(II)C

    move-result v1

    aput-char v1, v5, v0

    move v1, v0

    move-wide p0, v2

    :cond_1
    :goto_1
    cmp-long v0, p0, v8

    if-lez v0, :cond_2

    add-int/lit8 v0, v1, -0x1

    int-to-long v2, v4

    rem-long v2, p0, v2

    long-to-int v1, v2

    invoke-static {v1, v4}, Ljava/lang/Character;->forDigit(II)C

    move-result v1

    aput-char v1, v5, v0

    int-to-long v2, v4

    div-long/2addr p0, v2

    move v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 v2, v1, 0x40

    invoke-direct {v0, v5, v1, v2}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0
.end method

.method private static b(JJ)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x1

    .line 182
    cmp-long v1, p2, v2

    if-gez v1, :cond_1

    .line 183
    invoke-static {p0, p1, p2, p3}, Lcom/google/b/h/d;->a(JJ)I

    move-result v0

    if-gez v0, :cond_0

    move-wide v0, v2

    .line 203
    :goto_0
    return-wide v0

    .line 186
    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0

    .line 191
    :cond_1
    cmp-long v1, p0, v2

    if-ltz v1, :cond_2

    .line 192
    div-long v0, p0, p2

    goto :goto_0

    .line 201
    :cond_2
    ushr-long v2, p0, v0

    div-long/2addr v2, p2

    shl-long/2addr v2, v0

    .line 202
    mul-long v4, v2, p2

    sub-long v4, p0, v4

    .line 203
    invoke-static {v4, v5, p2, p3}, Lcom/google/b/h/d;->a(JJ)I

    move-result v1

    if-ltz v1, :cond_3

    :goto_1
    int-to-long v0, v0

    add-long/2addr v0, v2

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
