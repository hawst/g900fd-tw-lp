.class public final Lcom/google/b/h/c;
.super Ljava/lang/Number;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Number;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/b/h/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/google/b/h/c;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/b/h/c;-><init>(J)V

    .line 49
    new-instance v0, Lcom/google/b/h/c;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Lcom/google/b/h/c;-><init>(J)V

    .line 50
    new-instance v0, Lcom/google/b/h/c;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Lcom/google/b/h/c;-><init>(J)V

    return-void
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 55
    iput-wide p1, p0, Lcom/google/b/h/c;->a:J

    .line 56
    return-void
.end method

.method public static a(J)Lcom/google/b/h/c;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/google/b/h/c;

    invoke-direct {v0, p0, p1}, Lcom/google/b/h/c;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 43
    check-cast p1, Lcom/google/b/h/c;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    iget-wide v2, p1, Lcom/google/b/h/c;->a:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/b/h/d;->a(JJ)I

    move-result v0

    return v0
.end method

.method public final doubleValue()D
    .locals 6

    .prologue
    .line 298
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    long-to-double v0, v0

    .line 299
    iget-wide v2, p0, Lcom/google/b/h/c;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 300
    const-wide/high16 v2, 0x43e0000000000000L    # 9.223372036854776E18

    add-double/2addr v0, v2

    .line 302
    :cond_0
    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 328
    instance-of v1, p1, Lcom/google/b/h/c;

    if-eqz v1, :cond_0

    .line 329
    check-cast p1, Lcom/google/b/h/c;

    .line 330
    iget-wide v2, p0, Lcom/google/b/h/c;->a:J

    iget-wide v4, p1, Lcom/google/b/h/c;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 332
    :cond_0
    return v0
.end method

.method public final floatValue()F
    .locals 6

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    long-to-float v0, v0

    .line 284
    iget-wide v2, p0, Lcom/google/b/h/c;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 285
    const/high16 v1, 0x5f000000

    add-float/2addr v0, v1

    .line 287
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 323
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final intValue()I
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    long-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .locals 2

    .prologue
    .line 272
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 341
    iget-wide v0, p0, Lcom/google/b/h/c;->a:J

    invoke-static {v0, v1}, Lcom/google/b/h/d;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
