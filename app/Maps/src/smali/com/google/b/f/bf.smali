.class public final Lcom/google/b/f/bf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/cf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bf;",
            ">;"
        }
    .end annotation
.end field

.field static final Q:Lcom/google/b/f/bf;

.field private static volatile T:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field A:I

.field B:I

.field C:I

.field D:I

.field E:I

.field F:I

.field G:I

.field H:I

.field I:I

.field J:Ljava/lang/Object;

.field K:I

.field L:I

.field M:I

.field N:Ljava/lang/Object;

.field O:Lcom/google/n/ao;

.field P:I

.field private R:B

.field private S:I

.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:I

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field k:Lcom/google/n/ao;

.field l:Ljava/lang/Object;

.field m:Lcom/google/n/ao;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field p:Z

.field q:I

.field r:Z

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field x:I

.field y:Lcom/google/n/ao;

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 785
    new-instance v0, Lcom/google/b/f/bg;

    invoke-direct {v0}, Lcom/google/b/f/bg;-><init>()V

    sput-object v0, Lcom/google/b/f/bf;->PARSER:Lcom/google/n/ax;

    .line 4144
    new-instance v0, Lcom/google/b/f/bh;

    invoke-direct {v0}, Lcom/google/b/f/bh;-><init>()V

    .line 5126
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/bf;->T:Lcom/google/n/aw;

    .line 7549
    new-instance v0, Lcom/google/b/f/bf;

    invoke-direct {v0}, Lcom/google/b/f/bf;-><init>()V

    sput-object v0, Lcom/google/b/f/bf;->Q:Lcom/google/b/f/bf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 425
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4173
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    .line 4231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    .line 4468
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    .line 4763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    .line 4794
    iput-byte v4, p0, Lcom/google/b/f/bf;->R:B

    .line 4948
    iput v4, p0, Lcom/google/b/f/bf;->S:I

    .line 426
    iput v1, p0, Lcom/google/b/f/bf;->c:I

    .line 427
    iput v3, p0, Lcom/google/b/f/bf;->d:I

    .line 428
    iput v3, p0, Lcom/google/b/f/bf;->e:I

    .line 429
    iput v1, p0, Lcom/google/b/f/bf;->f:I

    .line 430
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    .line 431
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    .line 432
    iput v3, p0, Lcom/google/b/f/bf;->i:I

    .line 433
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    .line 434
    iget-object v0, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 435
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    .line 436
    iget-object v0, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 437
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    .line 438
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    .line 439
    iput-boolean v1, p0, Lcom/google/b/f/bf;->p:Z

    .line 440
    iput v1, p0, Lcom/google/b/f/bf;->q:I

    .line 441
    iput-boolean v1, p0, Lcom/google/b/f/bf;->r:Z

    .line 442
    iput v1, p0, Lcom/google/b/f/bf;->s:I

    .line 443
    iput v1, p0, Lcom/google/b/f/bf;->t:I

    .line 444
    iput v1, p0, Lcom/google/b/f/bf;->u:I

    .line 445
    iput v1, p0, Lcom/google/b/f/bf;->v:I

    .line 446
    iput v1, p0, Lcom/google/b/f/bf;->w:I

    .line 447
    iput v1, p0, Lcom/google/b/f/bf;->x:I

    .line 448
    iget-object v0, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 449
    iput v1, p0, Lcom/google/b/f/bf;->z:I

    .line 450
    iput v1, p0, Lcom/google/b/f/bf;->A:I

    .line 451
    iput v1, p0, Lcom/google/b/f/bf;->B:I

    .line 452
    iput v1, p0, Lcom/google/b/f/bf;->C:I

    .line 453
    iput v1, p0, Lcom/google/b/f/bf;->D:I

    .line 454
    iput v1, p0, Lcom/google/b/f/bf;->E:I

    .line 455
    iput v1, p0, Lcom/google/b/f/bf;->F:I

    .line 456
    iput v1, p0, Lcom/google/b/f/bf;->G:I

    .line 457
    iput v1, p0, Lcom/google/b/f/bf;->H:I

    .line 458
    iput v1, p0, Lcom/google/b/f/bf;->I:I

    .line 459
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    .line 460
    iput v1, p0, Lcom/google/b/f/bf;->K:I

    .line 461
    iput v1, p0, Lcom/google/b/f/bf;->L:I

    .line 462
    iput v1, p0, Lcom/google/b/f/bf;->M:I

    .line 463
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    .line 464
    iget-object v0, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 465
    iput v1, p0, Lcom/google/b/f/bf;->P:I

    .line 466
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 472
    invoke-direct {p0}, Lcom/google/b/f/bf;-><init>()V

    .line 473
    const/4 v1, 0x0

    .line 474
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 478
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 479
    :cond_0
    :goto_0
    if-nez v2, :cond_11

    .line 480
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 481
    sparse-switch v1, :sswitch_data_0

    .line 486
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 488
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 483
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 484
    goto :goto_0

    .line 493
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 494
    invoke-static {v1}, Lcom/google/b/f/cd;->a(I)Lcom/google/b/f/cd;

    move-result-object v4

    .line 495
    if-nez v4, :cond_4

    .line 496
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 767
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 768
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 773
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit16 v2, v1, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_1

    .line 774
    iget-object v2, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    .line 776
    :cond_1
    and-int/lit16 v2, v1, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_2

    .line 777
    iget-object v2, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    .line 779
    :cond_2
    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_3

    .line 780
    iget-object v1, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    .line 782
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bf;->au:Lcom/google/n/bn;

    throw v0

    .line 498
    :cond_4
    :try_start_2
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 499
    iput v1, p0, Lcom/google/b/f/bf;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 769
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 770
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 771
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 504
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 505
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 506
    iput-object v1, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 773
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_2

    .line 510
    :sswitch_3
    iget-object v1, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 511
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    goto/16 :goto_0

    .line 515
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 516
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 517
    iput-object v1, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 521
    :sswitch_5
    iget-object v1, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 522
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    goto/16 :goto_0

    .line 526
    :sswitch_6
    and-int/lit16 v1, v0, 0x800

    const/16 v4, 0x800

    if-eq v1, v4, :cond_16

    .line 527
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 529
    or-int/lit16 v1, v0, 0x800

    .line 531
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 532
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 531
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 533
    goto/16 :goto_0

    .line 536
    :sswitch_7
    :try_start_6
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 537
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_5
    iput-boolean v1, p0, Lcom/google/b/f/bf;->r:Z

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    .line 541
    :sswitch_8
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 542
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->s:I

    goto/16 :goto_0

    .line 546
    :sswitch_9
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 547
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->t:I

    goto/16 :goto_0

    .line 551
    :sswitch_a
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const v4, 0x8000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 552
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->u:I

    goto/16 :goto_0

    .line 556
    :sswitch_b
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x10000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 557
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->v:I

    goto/16 :goto_0

    .line 561
    :sswitch_c
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x40000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 562
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->x:I

    goto/16 :goto_0

    .line 566
    :sswitch_d
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x1000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 567
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->D:I

    goto/16 :goto_0

    .line 571
    :sswitch_e
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 572
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->z:I

    goto/16 :goto_0

    .line 576
    :sswitch_f
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x400000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 577
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->B:I

    goto/16 :goto_0

    .line 581
    :sswitch_10
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x800000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 582
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->C:I

    goto/16 :goto_0

    .line 586
    :sswitch_11
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x4000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 587
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->F:I

    goto/16 :goto_0

    .line 591
    :sswitch_12
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x8000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 592
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->G:I

    goto/16 :goto_0

    .line 596
    :sswitch_13
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x10000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 597
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->H:I

    goto/16 :goto_0

    .line 601
    :sswitch_14
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 602
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->I:I

    goto/16 :goto_0

    .line 606
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 607
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v5, 0x40000000    # 2.0f

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 608
    iput-object v1, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    goto/16 :goto_0

    .line 612
    :sswitch_16
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x20000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 613
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->w:I

    goto/16 :goto_0

    .line 617
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 618
    invoke-static {v1}, Lcom/google/b/f/br;->a(I)Lcom/google/b/f/br;

    move-result-object v4

    .line 619
    if-nez v4, :cond_6

    .line 620
    const/16 v4, 0x17

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 622
    :cond_6
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 623
    iput v1, p0, Lcom/google/b/f/bf;->i:I

    goto/16 :goto_0

    .line 628
    :sswitch_18
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 629
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->A:I

    goto/16 :goto_0

    .line 633
    :sswitch_19
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x2000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 634
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->E:I

    goto/16 :goto_0

    .line 638
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 639
    iget v4, p0, Lcom/google/b/f/bf;->b:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/bf;->b:I

    .line 640
    iput-object v1, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    goto/16 :goto_0

    .line 644
    :sswitch_1b
    iget-object v1, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 645
    iget v1, p0, Lcom/google/b/f/bf;->b:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/b/f/bf;->b:I

    goto/16 :goto_0

    .line 649
    :sswitch_1c
    iget-object v1, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 650
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    goto/16 :goto_0

    .line 654
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 655
    invoke-static {v1}, Lcom/google/b/f/bp;->a(I)Lcom/google/b/f/bp;

    move-result-object v4

    .line 656
    if-nez v4, :cond_7

    .line 657
    const/16 v4, 0x1d

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 659
    :cond_7
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 660
    iput v1, p0, Lcom/google/b/f/bf;->d:I

    goto/16 :goto_0

    .line 665
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 666
    invoke-static {v1}, Lcom/google/b/f/bt;->a(I)Lcom/google/b/f/bt;

    move-result-object v4

    .line 667
    if-nez v4, :cond_8

    .line 668
    const/16 v4, 0x1e

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 670
    :cond_8
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 671
    iput v1, p0, Lcom/google/b/f/bf;->e:I

    goto/16 :goto_0

    .line 676
    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 677
    invoke-static {v4}, Lcom/google/b/f/bn;->a(I)Lcom/google/b/f/bn;

    move-result-object v1

    .line 678
    if-nez v1, :cond_9

    .line 679
    const/16 v1, 0x1f

    invoke-virtual {v3, v1, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 681
    :cond_9
    and-int/lit16 v1, v0, 0x80

    const/16 v5, 0x80

    if-eq v1, v5, :cond_15

    .line 682
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 683
    or-int/lit16 v1, v0, 0x80

    .line 685
    :goto_6
    :try_start_7
    iget-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 687
    goto/16 :goto_0

    .line 690
    :sswitch_20
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 691
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v4

    move v1, v0

    .line 692
    :goto_7
    :try_start_9
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_a

    const/4 v0, -0x1

    :goto_8
    if-lez v0, :cond_d

    .line 693
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 694
    invoke-static {v0}, Lcom/google/b/f/bn;->a(I)Lcom/google/b/f/bn;

    move-result-object v5

    .line 695
    if-nez v5, :cond_b

    .line 696
    const/16 v5, 0x1f

    invoke-virtual {v3, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_7

    .line 767
    :catch_2
    move-exception v0

    goto/16 :goto_1

    .line 692
    :cond_a
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_8

    .line 698
    :cond_b
    and-int/lit16 v5, v1, 0x80

    const/16 v6, 0x80

    if-eq v5, v6, :cond_c

    .line 699
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    .line 700
    or-int/lit16 v1, v1, 0x80

    .line 702
    :cond_c
    iget-object v5, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 769
    :catch_3
    move-exception v0

    goto/16 :goto_3

    .line 705
    :cond_d
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 706
    goto/16 :goto_0

    .line 709
    :sswitch_21
    :try_start_a
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 710
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->f:I

    goto/16 :goto_0

    .line 714
    :sswitch_22
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 715
    iget v4, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/b/f/bf;->a:I

    .line 716
    iput-object v1, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 720
    :sswitch_23
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, -0x80000000

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 721
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->K:I

    goto/16 :goto_0

    .line 725
    :sswitch_24
    iget v1, p0, Lcom/google/b/f/bf;->b:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/f/bf;->b:I

    .line 726
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->L:I

    goto/16 :goto_0

    .line 730
    :sswitch_25
    iget v1, p0, Lcom/google/b/f/bf;->b:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/b/f/bf;->b:I

    .line 731
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->M:I

    goto/16 :goto_0

    .line 735
    :sswitch_26
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 736
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/b/f/bf;->q:I

    goto/16 :goto_0

    .line 740
    :sswitch_27
    iget v1, p0, Lcom/google/b/f/bf;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/b/f/bf;->a:I

    .line 741
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_e

    const/4 v1, 0x1

    :goto_9
    iput-boolean v1, p0, Lcom/google/b/f/bf;->p:Z

    goto/16 :goto_0

    :cond_e
    const/4 v1, 0x0

    goto :goto_9

    .line 745
    :sswitch_28
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 746
    invoke-static {v1}, Lcom/google/b/f/bv;->a(I)Lcom/google/b/f/bv;

    move-result-object v4

    .line 747
    if-nez v4, :cond_f

    .line 748
    const/16 v4, 0x27

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 750
    :cond_f
    iget v4, p0, Lcom/google/b/f/bf;->b:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/bf;->b:I

    .line 751
    iput v1, p0, Lcom/google/b/f/bf;->P:I

    goto/16 :goto_0

    .line 756
    :sswitch_29
    and-int/lit16 v1, v0, 0x1000

    const/16 v4, 0x1000

    if-eq v1, v4, :cond_10

    .line 757
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    .line 759
    or-int/lit16 v0, v0, 0x1000

    .line 761
    :cond_10
    iget-object v1, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 762
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 761
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 773
    :cond_11
    and-int/lit16 v1, v0, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_12

    .line 774
    iget-object v1, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    .line 776
    :cond_12
    and-int/lit16 v1, v0, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_13

    .line 777
    iget-object v1, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    .line 779
    :cond_13
    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_14

    .line 780
    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    .line 782
    :cond_14
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->au:Lcom/google/n/bn;

    .line 783
    return-void

    :cond_15
    move v1, v0

    goto/16 :goto_6

    :cond_16
    move v1, v0

    goto/16 :goto_4

    .line 481
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0xfa -> :sswitch_20
        0x100 -> :sswitch_21
        0x10a -> :sswitch_22
        0x110 -> :sswitch_23
        0x118 -> :sswitch_24
        0x120 -> :sswitch_25
        0x128 -> :sswitch_26
        0x130 -> :sswitch_27
        0x138 -> :sswitch_28
        0x142 -> :sswitch_29
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 423
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4173
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    .line 4231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    .line 4468
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    .line 4763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    .line 4794
    iput-byte v1, p0, Lcom/google/b/f/bf;->R:B

    .line 4948
    iput v1, p0, Lcom/google/b/f/bf;->S:I

    .line 424
    return-void
.end method

.method public static d()Lcom/google/b/f/bf;
    .locals 1

    .prologue
    .line 7552
    sget-object v0, Lcom/google/b/f/bf;->Q:Lcom/google/b/f/bf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/bi;
    .locals 1

    .prologue
    .line 5188
    new-instance v0, Lcom/google/b/f/bi;

    invoke-direct {v0}, Lcom/google/b/f/bi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797
    sget-object v0, Lcom/google/b/f/bf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 4824
    invoke-virtual {p0}, Lcom/google/b/f/bf;->c()I

    .line 4825
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 4826
    iget v0, p0, Lcom/google/b/f/bf;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 4828
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 4829
    iget-object v0, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4831
    :cond_1
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 4832
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4834
    :cond_2
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_3

    .line 4835
    iget-object v0, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    :goto_2
    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4837
    :cond_3
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_4

    .line 4838
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_4
    move v1, v2

    .line 4840
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 4841
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4840
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4826
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 4829
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 4835
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 4843
    :cond_8
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_9

    .line 4844
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/b/f/bf;->r:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_21

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 4846
    :cond_9
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_a

    .line 4847
    iget v0, p0, Lcom/google/b/f/bf;->s:I

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 4849
    :cond_a
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_b

    .line 4850
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/b/f/bf;->t:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4852
    :cond_b
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_c

    .line 4853
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/b/f/bf;->u:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4855
    :cond_c
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_d

    .line 4856
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/b/f/bf;->v:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4858
    :cond_d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_e

    .line 4859
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/b/f/bf;->x:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4861
    :cond_e
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_f

    .line 4862
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/b/f/bf;->D:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4864
    :cond_f
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_10

    .line 4865
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/b/f/bf;->z:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4867
    :cond_10
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_11

    .line 4868
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/b/f/bf;->B:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4870
    :cond_11
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_12

    .line 4871
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/b/f/bf;->C:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4873
    :cond_12
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_13

    .line 4874
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/b/f/bf;->F:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4876
    :cond_13
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_14

    .line 4877
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/b/f/bf;->G:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4879
    :cond_14
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_15

    .line 4880
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/b/f/bf;->H:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4882
    :cond_15
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_16

    .line 4883
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/b/f/bf;->I:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4885
    :cond_16
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_17

    .line 4886
    const/16 v1, 0x15

    iget-object v0, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4888
    :cond_17
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_18

    .line 4889
    const/16 v0, 0x16

    iget v1, p0, Lcom/google/b/f/bf;->w:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4891
    :cond_18
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_19

    .line 4892
    const/16 v0, 0x17

    iget v1, p0, Lcom/google/b/f/bf;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_23

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4894
    :cond_19
    :goto_6
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_1a

    .line 4895
    const/16 v0, 0x18

    iget v1, p0, Lcom/google/b/f/bf;->A:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4897
    :cond_1a
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_1b

    .line 4898
    const/16 v0, 0x19

    iget v1, p0, Lcom/google/b/f/bf;->E:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4900
    :cond_1b
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1c

    .line 4901
    const/16 v1, 0x1a

    iget-object v0, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_24

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4903
    :cond_1c
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_1d

    .line 4904
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4906
    :cond_1d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_1e

    .line 4907
    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4909
    :cond_1e
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1f

    .line 4910
    const/16 v0, 0x1d

    iget v1, p0, Lcom/google/b/f/bf;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_25

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4912
    :cond_1f
    :goto_8
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_20

    .line 4913
    const/16 v0, 0x1e

    iget v1, p0, Lcom/google/b/f/bf;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_26

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_20
    :goto_9
    move v1, v2

    .line 4915
    :goto_a
    iget-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_28

    .line 4916
    const/16 v4, 0x1f

    iget-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_27

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 4915
    :goto_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    :cond_21
    move v0, v2

    .line 4844
    goto/16 :goto_4

    .line 4886
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 4892
    :cond_23
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    .line 4901
    :cond_24
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 4910
    :cond_25
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    .line 4913
    :cond_26
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    .line 4916
    :cond_27
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_b

    .line 4918
    :cond_28
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_29

    .line 4919
    const/16 v0, 0x20

    iget v1, p0, Lcom/google/b/f/bf;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4921
    :cond_29
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2a

    .line 4922
    const/16 v1, 0x21

    iget-object v0, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_31

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4924
    :cond_2a
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2b

    .line 4925
    const/16 v0, 0x22

    iget v1, p0, Lcom/google/b/f/bf;->K:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4927
    :cond_2b
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2c

    .line 4928
    const/16 v0, 0x23

    iget v1, p0, Lcom/google/b/f/bf;->L:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4930
    :cond_2c
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2d

    .line 4931
    const/16 v0, 0x24

    iget v1, p0, Lcom/google/b/f/bf;->M:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4933
    :cond_2d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_2e

    .line 4934
    const/16 v0, 0x25

    iget v1, p0, Lcom/google/b/f/bf;->q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4936
    :cond_2e
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_2f

    .line 4937
    const/16 v0, 0x26

    iget-boolean v1, p0, Lcom/google/b/f/bf;->p:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_32

    :goto_d
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 4939
    :cond_2f
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_30

    .line 4940
    const/16 v0, 0x27

    iget v1, p0, Lcom/google/b/f/bf;->P:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_33

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 4942
    :cond_30
    :goto_e
    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_34

    .line 4943
    const/16 v1, 0x28

    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4942
    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .line 4922
    :cond_31
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    :cond_32
    move v3, v2

    .line 4937
    goto :goto_d

    .line 4940
    :cond_33
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_e

    .line 4945
    :cond_34
    iget-object v0, p0, Lcom/google/b/f/bf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4946
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4796
    iget-byte v0, p0, Lcom/google/b/f/bf;->R:B

    .line 4797
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 4819
    :cond_0
    :goto_0
    return v2

    .line 4798
    :cond_1
    if-eqz v0, :cond_0

    .line 4800
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 4801
    iget-object v0, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4802
    iput-byte v2, p0, Lcom/google/b/f/bf;->R:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 4800
    goto :goto_1

    :cond_3
    move v1, v2

    .line 4806
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 4807
    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 4808
    iput-byte v2, p0, Lcom/google/b/f/bf;->R:B

    goto :goto_0

    .line 4806
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 4812
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 4813
    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 4814
    iput-byte v2, p0, Lcom/google/b/f/bf;->R:B

    goto :goto_0

    .line 4812
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4818
    :cond_7
    iput-byte v3, p0, Lcom/google/b/f/bf;->R:B

    move v2, v3

    .line 4819
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 4950
    iget v0, p0, Lcom/google/b/f/bf;->S:I

    .line 4951
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5121
    :goto_0
    return v0

    .line 4954
    :cond_0
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_33

    .line 4955
    iget v0, p0, Lcom/google/b/f/bf;->c:I

    .line 4956
    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 4958
    :goto_2
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_1

    .line 4960
    iget-object v0, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v7, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 4962
    :cond_1
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_2

    .line 4963
    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    .line 4964
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 4966
    :cond_2
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_3

    .line 4968
    iget-object v0, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    :goto_4
    invoke-static {v8, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 4970
    :cond_3
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_4

    .line 4971
    const/4 v0, 0x5

    iget-object v4, p0, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    .line 4972
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    :cond_4
    move v4, v2

    move v2, v3

    .line 4974
    :goto_5
    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 4975
    const/4 v5, 0x6

    iget-object v0, p0, Lcom/google/b/f/bf;->n:Ljava/util/List;

    .line 4976
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 4974
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_5
    move v0, v1

    .line 4956
    goto/16 :goto_1

    .line 4960
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 4968
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 4978
    :cond_8
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_9

    .line 4979
    const/4 v0, 0x7

    iget-boolean v2, p0, Lcom/google/b/f/bf;->r:Z

    .line 4980
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 4982
    :cond_9
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_a

    .line 4983
    const/16 v0, 0x8

    iget v2, p0, Lcom/google/b/f/bf;->s:I

    .line 4984
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 4986
    :cond_a
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_b

    .line 4987
    const/16 v0, 0x9

    iget v2, p0, Lcom/google/b/f/bf;->t:I

    .line 4988
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 4990
    :cond_b
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_c

    .line 4991
    iget v0, p0, Lcom/google/b/f/bf;->u:I

    .line 4992
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 4994
    :cond_c
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_d

    .line 4995
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/b/f/bf;->v:I

    .line 4996
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 4998
    :cond_d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x40000

    and-int/2addr v0, v2

    const/high16 v2, 0x40000

    if-ne v0, v2, :cond_e

    .line 4999
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/b/f/bf;->x:I

    .line 5000
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5002
    :cond_e
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x1000000

    and-int/2addr v0, v2

    const/high16 v2, 0x1000000

    if-ne v0, v2, :cond_f

    .line 5003
    const/16 v0, 0xd

    iget v2, p0, Lcom/google/b/f/bf;->D:I

    .line 5004
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5006
    :cond_f
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x100000

    and-int/2addr v0, v2

    const/high16 v2, 0x100000

    if-ne v0, v2, :cond_10

    .line 5007
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/b/f/bf;->z:I

    .line 5008
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5010
    :cond_10
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x400000

    and-int/2addr v0, v2

    const/high16 v2, 0x400000

    if-ne v0, v2, :cond_11

    .line 5011
    const/16 v0, 0xf

    iget v2, p0, Lcom/google/b/f/bf;->B:I

    .line 5012
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5014
    :cond_11
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x800000

    and-int/2addr v0, v2

    const/high16 v2, 0x800000

    if-ne v0, v2, :cond_12

    .line 5015
    const/16 v0, 0x10

    iget v2, p0, Lcom/google/b/f/bf;->C:I

    .line 5016
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5018
    :cond_12
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x4000000

    and-int/2addr v0, v2

    const/high16 v2, 0x4000000

    if-ne v0, v2, :cond_13

    .line 5019
    const/16 v0, 0x11

    iget v2, p0, Lcom/google/b/f/bf;->F:I

    .line 5020
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5022
    :cond_13
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x8000000

    and-int/2addr v0, v2

    const/high16 v2, 0x8000000

    if-ne v0, v2, :cond_14

    .line 5023
    const/16 v0, 0x12

    iget v2, p0, Lcom/google/b/f/bf;->G:I

    .line 5024
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5026
    :cond_14
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x10000000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000000

    if-ne v0, v2, :cond_15

    .line 5027
    const/16 v0, 0x13

    iget v2, p0, Lcom/google/b/f/bf;->H:I

    .line 5028
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5030
    :cond_15
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x20000000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000000

    if-ne v0, v2, :cond_16

    .line 5031
    const/16 v0, 0x14

    iget v2, p0, Lcom/google/b/f/bf;->I:I

    .line 5032
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5034
    :cond_16
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x40000000    # 2.0f

    and-int/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_17

    .line 5035
    const/16 v2, 0x15

    .line 5036
    iget-object v0, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_21

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5038
    :cond_17
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_18

    .line 5039
    const/16 v0, 0x16

    iget v2, p0, Lcom/google/b/f/bf;->w:I

    .line 5040
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5042
    :cond_18
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_19

    .line 5043
    const/16 v0, 0x17

    iget v2, p0, Lcom/google/b/f/bf;->i:I

    .line 5044
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_22

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 5046
    :cond_19
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x200000

    and-int/2addr v0, v2

    const/high16 v2, 0x200000

    if-ne v0, v2, :cond_1a

    .line 5047
    const/16 v0, 0x18

    iget v2, p0, Lcom/google/b/f/bf;->A:I

    .line 5048
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5050
    :cond_1a
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x2000000

    and-int/2addr v0, v2

    const/high16 v2, 0x2000000

    if-ne v0, v2, :cond_1b

    .line 5051
    const/16 v0, 0x19

    iget v2, p0, Lcom/google/b/f/bf;->E:I

    .line 5052
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5054
    :cond_1b
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_1c

    .line 5055
    const/16 v2, 0x1a

    .line 5056
    iget-object v0, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_23

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    :goto_8
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5058
    :cond_1c
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1d

    .line 5059
    const/16 v0, 0x1b

    iget-object v2, p0, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    .line 5060
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5062
    :cond_1d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v2, 0x80000

    and-int/2addr v0, v2

    const/high16 v2, 0x80000

    if-ne v0, v2, :cond_1e

    .line 5063
    const/16 v0, 0x1c

    iget-object v2, p0, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    .line 5064
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 5066
    :cond_1e
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_1f

    .line 5067
    const/16 v0, 0x1d

    iget v2, p0, Lcom/google/b/f/bf;->d:I

    .line 5068
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_24

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 5070
    :cond_1f
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_20

    .line 5071
    const/16 v0, 0x1e

    iget v2, p0, Lcom/google/b/f/bf;->e:I

    .line 5072
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_25

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    :cond_20
    move v2, v3

    move v5, v3

    .line 5076
    :goto_b
    iget-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_27

    .line 5077
    iget-object v0, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    .line 5078
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_26

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v5, v0

    .line 5076
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 5036
    :cond_21
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    :cond_22
    move v0, v1

    .line 5044
    goto/16 :goto_7

    .line 5056
    :cond_23
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    :cond_24
    move v0, v1

    .line 5068
    goto :goto_9

    :cond_25
    move v0, v1

    .line 5072
    goto :goto_a

    :cond_26
    move v0, v1

    .line 5078
    goto :goto_c

    .line 5080
    :cond_27
    add-int v0, v4, v5

    .line 5081
    iget-object v2, p0, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 5083
    iget v2, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_32

    .line 5084
    const/16 v2, 0x20

    iget v4, p0, Lcom/google/b/f/bf;->f:I

    .line 5085
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 5087
    :goto_d
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_28

    .line 5088
    const/16 v4, 0x21

    .line 5089
    iget-object v0, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_30

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    :goto_e
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5091
    :cond_28
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    const/high16 v4, -0x80000000

    and-int/2addr v0, v4

    const/high16 v4, -0x80000000

    if-ne v0, v4, :cond_29

    .line 5092
    const/16 v0, 0x22

    iget v4, p0, Lcom/google/b/f/bf;->K:I

    .line 5093
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5095
    :cond_29
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2a

    .line 5096
    const/16 v0, 0x23

    iget v4, p0, Lcom/google/b/f/bf;->L:I

    .line 5097
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5099
    :cond_2a
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_2b

    .line 5100
    const/16 v0, 0x24

    iget v4, p0, Lcom/google/b/f/bf;->M:I

    .line 5101
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5103
    :cond_2b
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_2c

    .line 5104
    const/16 v0, 0x25

    iget v4, p0, Lcom/google/b/f/bf;->q:I

    .line 5105
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5107
    :cond_2c
    iget v0, p0, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_2d

    .line 5108
    const/16 v0, 0x26

    iget-boolean v4, p0, Lcom/google/b/f/bf;->p:Z

    .line 5109
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 5111
    :cond_2d
    iget v0, p0, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2f

    .line 5112
    const/16 v0, 0x27

    iget v4, p0, Lcom/google/b/f/bf;->P:I

    .line 5113
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_2e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2e
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    :cond_2f
    move v1, v3

    .line 5115
    :goto_f
    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_31

    .line 5116
    const/16 v4, 0x28

    iget-object v0, p0, Lcom/google/b/f/bf;->o:Ljava/util/List;

    .line 5117
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 5115
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 5089
    :cond_30
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    .line 5119
    :cond_31
    iget-object v0, p0, Lcom/google/b/f/bf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 5120
    iput v0, p0, Lcom/google/b/f/bf;->S:I

    goto/16 :goto_0

    :cond_32
    move v2, v0

    goto/16 :goto_d

    :cond_33
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 417
    invoke-static {}, Lcom/google/b/f/bf;->newBuilder()Lcom/google/b/f/bi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/bi;->a(Lcom/google/b/f/bf;)Lcom/google/b/f/bi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 417
    invoke-static {}, Lcom/google/b/f/bf;->newBuilder()Lcom/google/b/f/bi;

    move-result-object v0

    return-object v0
.end method
