.class public final Lcom/google/b/f/b/a/dc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/df;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dc;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/b/a/dc;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/b/f/b/a/dd;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dd;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dc;->PARSER:Lcom/google/n/ax;

    .line 184
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/dc;->h:Lcom/google/n/aw;

    .line 452
    new-instance v0, Lcom/google/b/f/b/a/dc;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dc;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dc;->e:Lcom/google/b/f/b/a/dc;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 89
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    .line 134
    iput-byte v2, p0, Lcom/google/b/f/b/a/dc;->f:B

    .line 159
    iput v2, p0, Lcom/google/b/f/b/a/dc;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v3, p0, Lcom/google/b/f/b/a/dc;->c:I

    .line 20
    iput v3, p0, Lcom/google/b/f/b/a/dc;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/b/f/b/a/dc;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v3, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 48
    iget v3, p0, Lcom/google/b/f/b/a/dc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/dc;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/dc;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/b/a/dc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/dc;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/dc;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/b/f/b/a/dc;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/dc;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/dc;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dc;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 89
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    .line 134
    iput-byte v1, p0, Lcom/google/b/f/b/a/dc;->f:B

    .line 159
    iput v1, p0, Lcom/google/b/f/b/a/dc;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/dc;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/google/b/f/b/a/dc;->e:Lcom/google/b/f/b/a/dc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/de;
    .locals 1

    .prologue
    .line 246
    new-instance v0, Lcom/google/b/f/b/a/de;

    invoke-direct {v0}, Lcom/google/b/f/b/a/de;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/b/f/b/a/dc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 146
    invoke-virtual {p0}, Lcom/google/b/f/b/a/dc;->c()I

    .line 147
    iget v0, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 150
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 151
    iget v0, p0, Lcom/google/b/f/b/a/dc;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 153
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 154
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/dc;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 156
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/dc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 157
    return-void

    .line 151
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 154
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 136
    iget-byte v1, p0, Lcom/google/b/f/b/a/dc;->f:B

    .line 137
    if-ne v1, v0, :cond_0

    .line 141
    :goto_0
    return v0

    .line 138
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/dc;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 161
    iget v0, p0, Lcom/google/b/f/b/a/dc;->g:I

    .line 162
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 179
    :goto_0
    return v0

    .line 165
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 166
    iget-object v0, p0, Lcom/google/b/f/b/a/dc;->b:Lcom/google/n/ao;

    .line 167
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 169
    :goto_1
    iget v2, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 170
    iget v2, p0, Lcom/google/b/f/b/a/dc;->c:I

    .line 171
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 173
    :cond_1
    iget v2, p0, Lcom/google/b/f/b/a/dc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    .line 174
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/dc;->d:I

    .line 175
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/google/b/f/b/a/dc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    iput v0, p0, Lcom/google/b/f/b/a/dc;->g:I

    goto :goto_0

    :cond_4
    move v2, v3

    .line 171
    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/dc;->newBuilder()Lcom/google/b/f/b/a/de;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/de;->a(Lcom/google/b/f/b/a/dc;)Lcom/google/b/f/b/a/de;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/dc;->newBuilder()Lcom/google/b/f/b/a/de;

    move-result-object v0

    return-object v0
.end method
