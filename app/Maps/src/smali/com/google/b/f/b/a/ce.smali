.class public final Lcom/google/b/f/b/a/ce;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/cc;",
        "Lcom/google/b/f/b/a/ce;",
        ">;",
        "Lcom/google/b/f/b/a/cf;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:J

.field public f:J

.field public g:Z

.field public h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 375
    sget-object v0, Lcom/google/b/f/b/a/cc;->i:Lcom/google/b/f/b/a/cc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 376
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 437
    invoke-static {}, Lcom/google/b/f/b/a/cc;->d()Lcom/google/b/f/b/a/cc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 460
    :goto_0
    return-object p0

    .line 438
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 439
    iget v2, p1, Lcom/google/b/f/b/a/cc;->b:I

    iget v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ce;->b:I

    .line 441
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 442
    iget v2, p1, Lcom/google/b/f/b/a/cc;->c:I

    iget v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ce;->c:I

    .line 444
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 445
    iget-wide v2, p1, Lcom/google/b/f/b/a/cc;->d:J

    iget v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/ce;->d:J

    .line 447
    :cond_3
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 448
    iget-wide v2, p1, Lcom/google/b/f/b/a/cc;->e:J

    iget v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/ce;->e:J

    .line 450
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 451
    iget-wide v2, p1, Lcom/google/b/f/b/a/cc;->f:J

    iget v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/ce;->f:J

    .line 453
    :cond_5
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 454
    iget-boolean v2, p1, Lcom/google/b/f/b/a/cc;->g:Z

    iget v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/b/a/ce;->g:Z

    .line 456
    :cond_6
    iget v2, p1, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    :goto_7
    if-eqz v0, :cond_7

    .line 457
    iget-boolean v0, p1, Lcom/google/b/f/b/a/cc;->h:Z

    iget v1, p0, Lcom/google/b/f/b/a/ce;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/b/f/b/a/ce;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/b/a/ce;->h:Z

    .line 459
    :cond_7
    iget-object v0, p1, Lcom/google/b/f/b/a/cc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 438
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 441
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 444
    goto :goto_3

    :cond_b
    move v2, v1

    .line 447
    goto :goto_4

    :cond_c
    move v2, v1

    .line 450
    goto :goto_5

    :cond_d
    move v2, v1

    .line 453
    goto :goto_6

    :cond_e
    move v0, v1

    .line 456
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/b/f/b/a/ce;->c()Lcom/google/b/f/b/a/cc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 367
    check-cast p1, Lcom/google/b/f/b/a/cc;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/b/f/b/a/cc;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 401
    new-instance v2, Lcom/google/b/f/b/a/cc;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/cc;-><init>(Lcom/google/n/v;)V

    .line 402
    iget v3, p0, Lcom/google/b/f/b/a/ce;->a:I

    .line 403
    const/4 v1, 0x0

    .line 404
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 407
    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/ce;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/cc;->b:I

    .line 408
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 409
    or-int/lit8 v0, v0, 0x2

    .line 411
    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/ce;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/cc;->c:I

    .line 412
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 413
    or-int/lit8 v0, v0, 0x4

    .line 415
    :cond_1
    iget-wide v4, p0, Lcom/google/b/f/b/a/ce;->d:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/cc;->d:J

    .line 416
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 417
    or-int/lit8 v0, v0, 0x8

    .line 419
    :cond_2
    iget-wide v4, p0, Lcom/google/b/f/b/a/ce;->e:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/cc;->e:J

    .line 420
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 421
    or-int/lit8 v0, v0, 0x10

    .line 423
    :cond_3
    iget-wide v4, p0, Lcom/google/b/f/b/a/ce;->f:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/cc;->f:J

    .line 424
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 425
    or-int/lit8 v0, v0, 0x20

    .line 427
    :cond_4
    iget-boolean v1, p0, Lcom/google/b/f/b/a/ce;->g:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/cc;->g:Z

    .line 428
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 429
    or-int/lit8 v0, v0, 0x40

    .line 431
    :cond_5
    iget-boolean v1, p0, Lcom/google/b/f/b/a/ce;->h:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/cc;->h:Z

    .line 432
    iput v0, v2, Lcom/google/b/f/b/a/cc;->a:I

    .line 433
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method
