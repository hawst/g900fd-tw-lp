.class public final Lcom/google/b/f/an;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/ax;


# static fields
.field static final P:Lcom/google/b/f/an;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/an;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile S:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field A:Lcom/google/n/ao;

.field B:Ljava/lang/Object;

.field C:Lcom/google/n/ao;

.field D:I

.field E:Ljava/lang/Object;

.field F:Lcom/google/n/ao;

.field G:Lcom/google/n/ao;

.field H:Lcom/google/n/ao;

.field I:Lcom/google/n/ao;

.field J:Z

.field K:Lcom/google/n/ao;

.field L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field M:Lcom/google/n/ao;

.field N:Z

.field O:J

.field private Q:B

.field private R:I

.field a:I

.field b:I

.field c:I

.field d:Ljava/lang/Object;

.field e:J

.field f:Lcom/google/n/aq;

.field g:I

.field h:I

.field i:Lcom/google/n/aq;

.field j:Lcom/google/b/f/ar;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:I

.field m:I

.field n:D

.field o:I

.field p:Ljava/lang/Object;

.field q:Ljava/lang/Object;

.field r:Z

.field s:Lcom/google/n/ao;

.field t:Lcom/google/n/ao;

.field u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field w:I

.field x:Z

.field y:Lcom/google/n/ao;

.field z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10462
    new-instance v0, Lcom/google/b/f/ao;

    invoke-direct {v0}, Lcom/google/b/f/ao;-><init>()V

    sput-object v0, Lcom/google/b/f/an;->PARSER:Lcom/google/n/ax;

    .line 12174
    new-instance v0, Lcom/google/b/f/ap;

    invoke-direct {v0}, Lcom/google/b/f/ap;-><init>()V

    .line 12577
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/an;->S:Lcom/google/n/aw;

    .line 15423
    new-instance v0, Lcom/google/b/f/an;

    invoke-direct {v0}, Lcom/google/b/f/an;-><init>()V

    sput-object v0, Lcom/google/b/f/an;->P:Lcom/google/b/f/an;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v1, -0x1

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10108
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 11767
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    .line 11783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    .line 11915
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    .line 11946
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    .line 12004
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    .line 12077
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    .line 12093
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    .line 12109
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    .line 12125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    .line 12156
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    .line 12203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    .line 12248
    iput-byte v1, p0, Lcom/google/b/f/an;->Q:B

    .line 12393
    iput v1, p0, Lcom/google/b/f/an;->R:I

    .line 10109
    iput v4, p0, Lcom/google/b/f/an;->c:I

    .line 10110
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    .line 10111
    iput-wide v6, p0, Lcom/google/b/f/an;->e:J

    .line 10112
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    .line 10113
    iput v4, p0, Lcom/google/b/f/an;->g:I

    .line 10114
    iput v4, p0, Lcom/google/b/f/an;->h:I

    .line 10115
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    .line 10116
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    .line 10117
    iput v4, p0, Lcom/google/b/f/an;->l:I

    .line 10118
    iput v4, p0, Lcom/google/b/f/an;->m:I

    .line 10119
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/b/f/an;->n:D

    .line 10120
    iput v4, p0, Lcom/google/b/f/an;->o:I

    .line 10121
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    .line 10122
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    .line 10123
    iput-boolean v3, p0, Lcom/google/b/f/an;->r:Z

    .line 10124
    iget-object v0, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10125
    iget-object v0, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10126
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    .line 10127
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    .line 10128
    iput v4, p0, Lcom/google/b/f/an;->w:I

    .line 10129
    iput-boolean v4, p0, Lcom/google/b/f/an;->x:Z

    .line 10130
    iget-object v0, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10131
    iput-wide v6, p0, Lcom/google/b/f/an;->z:J

    .line 10132
    iget-object v0, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10133
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    .line 10134
    iget-object v0, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10135
    iput v4, p0, Lcom/google/b/f/an;->D:I

    .line 10136
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    .line 10137
    iget-object v0, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10138
    iget-object v0, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10139
    iget-object v0, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10140
    iget-object v0, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10141
    iput-boolean v4, p0, Lcom/google/b/f/an;->J:Z

    .line 10142
    iget-object v0, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10143
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 10144
    iget-object v0, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10145
    iput-boolean v4, p0, Lcom/google/b/f/an;->N:Z

    .line 10146
    iput-wide v6, p0, Lcom/google/b/f/an;->O:J

    .line 10147
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    .line 10153
    invoke-direct {p0}, Lcom/google/b/f/an;-><init>()V

    .line 10154
    const/4 v2, 0x0

    .line 10155
    const/4 v1, 0x0

    .line 10157
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 10159
    const/4 v0, 0x0

    move v4, v0

    .line 10160
    :cond_0
    :goto_0
    if-nez v4, :cond_17

    .line 10161
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 10162
    sparse-switch v0, :sswitch_data_0

    .line 10167
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10169
    const/4 v0, 0x1

    move v4, v0

    goto :goto_0

    .line 10164
    :sswitch_0
    const/4 v0, 0x1

    move v4, v0

    .line 10165
    goto :goto_0

    .line 10174
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10175
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->c:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 10435
    :catch_0
    move-exception v0

    .line 10436
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10441
    :catchall_0
    move-exception v0

    and-int/lit8 v3, v2, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_1

    .line 10442
    iget-object v3, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v3

    iput-object v3, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    .line 10444
    :cond_1
    and-int/lit16 v3, v2, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_2

    .line 10445
    iget-object v3, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    .line 10447
    :cond_2
    const/high16 v3, 0x40000

    and-int/2addr v3, v2

    const/high16 v4, 0x40000

    if-ne v3, v4, :cond_3

    .line 10448
    iget-object v3, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    .line 10450
    :cond_3
    const/high16 v3, 0x80000

    and-int/2addr v3, v2

    const/high16 v4, 0x80000

    if-ne v3, v4, :cond_4

    .line 10451
    iget-object v3, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    .line 10453
    :cond_4
    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5

    .line 10454
    iget-object v2, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    .line 10456
    :cond_5
    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_6

    .line 10457
    iget-object v1, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 10459
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/an;->au:Lcom/google/n/bn;

    throw v0

    .line 10179
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10180
    iget v3, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/an;->a:I

    .line 10181
    iput-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 10437
    :catch_1
    move-exception v0

    .line 10438
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 10439
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 10185
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10186
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/an;->e:J

    goto/16 :goto_0

    .line 10190
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10191
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->g:I

    goto/16 :goto_0

    .line 10195
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10196
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->h:I

    goto/16 :goto_0

    .line 10200
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10201
    and-int/lit8 v3, v2, 0x40

    const/16 v6, 0x40

    if-eq v3, v6, :cond_7

    .line 10202
    new-instance v3, Lcom/google/n/ap;

    invoke-direct {v3}, Lcom/google/n/ap;-><init>()V

    iput-object v3, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    .line 10203
    or-int/lit8 v2, v2, 0x40

    .line 10205
    :cond_7
    iget-object v3, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10209
    :sswitch_7
    const/4 v0, 0x0

    .line 10210
    iget v3, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v6, 0x20

    if-ne v3, v6, :cond_1e

    .line 10211
    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    invoke-static {v0}, Lcom/google/b/f/ar;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    move-result-object v0

    move-object v3, v0

    .line 10213
    :goto_1
    const/4 v0, 0x7

    sget-object v6, Lcom/google/b/f/ar;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ar;

    iput-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    .line 10215
    if-eqz v3, :cond_8

    .line 10216
    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    invoke-virtual {v3, v0}, Lcom/google/b/f/at;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    .line 10217
    invoke-virtual {v3}, Lcom/google/b/f/at;->c()Lcom/google/b/f/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    .line 10219
    :cond_8
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10223
    :sswitch_8
    and-int/lit16 v0, v2, 0x100

    const/16 v3, 0x100

    if-eq v0, v3, :cond_9

    .line 10224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    .line 10226
    or-int/lit16 v2, v2, 0x100

    .line 10228
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    .line 10229
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v3, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 10228
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10233
    :sswitch_9
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10234
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->l:I

    goto/16 :goto_0

    .line 10238
    :sswitch_a
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10239
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->m:I

    goto/16 :goto_0

    .line 10243
    :sswitch_b
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10244
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/an;->n:D

    goto/16 :goto_0

    .line 10248
    :sswitch_c
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10249
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->o:I

    goto/16 :goto_0

    .line 10253
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10254
    iget v3, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/b/f/an;->a:I

    .line 10255
    iput-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10259
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10260
    iget v3, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/b/f/an;->a:I

    .line 10261
    iput-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10265
    :sswitch_f
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10266
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/b/f/an;->r:Z

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_2

    .line 10270
    :sswitch_10
    iget-object v0, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10271
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10275
    :sswitch_11
    iget-object v0, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10276
    iget v0, p0, Lcom/google/b/f/an;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10280
    :sswitch_12
    const/high16 v0, 0x40000

    and-int/2addr v0, v2

    const/high16 v3, 0x40000

    if-eq v0, v3, :cond_b

    .line 10281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    .line 10283
    const/high16 v0, 0x40000

    or-int/2addr v2, v0

    .line 10285
    :cond_b
    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    .line 10286
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v3, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 10285
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10290
    :sswitch_13
    const/high16 v0, 0x80000

    and-int/2addr v0, v2

    const/high16 v3, 0x80000

    if-eq v0, v3, :cond_c

    .line 10291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    .line 10293
    const/high16 v0, 0x80000

    or-int/2addr v2, v0

    .line 10295
    :cond_c
    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    .line 10296
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v3, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 10295
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10300
    :sswitch_14
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const v3, 0x8000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10301
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->w:I

    goto/16 :goto_0

    .line 10305
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10306
    and-int/lit8 v3, v2, 0x8

    const/16 v6, 0x8

    if-eq v3, v6, :cond_d

    .line 10307
    new-instance v3, Lcom/google/n/ap;

    invoke-direct {v3}, Lcom/google/n/ap;-><init>()V

    iput-object v3, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    .line 10308
    or-int/lit8 v2, v2, 0x8

    .line 10310
    :cond_d
    iget-object v3, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10314
    :sswitch_16
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10315
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/b/f/an;->x:Z

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_3

    .line 10319
    :sswitch_17
    iget-object v0, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10320
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x20000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10324
    :sswitch_18
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10325
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/an;->z:J

    goto/16 :goto_0

    .line 10329
    :sswitch_19
    iget-object v0, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10330
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x80000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10334
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10335
    iget v3, p0, Lcom/google/b/f/an;->a:I

    const/high16 v6, 0x100000

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/b/f/an;->a:I

    .line 10336
    iput-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10340
    :sswitch_1b
    iget-object v0, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10341
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x200000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10345
    :sswitch_1c
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x400000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10346
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/an;->D:I

    goto/16 :goto_0

    .line 10350
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10351
    iget v3, p0, Lcom/google/b/f/an;->a:I

    const/high16 v6, 0x800000

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/b/f/an;->a:I

    .line 10352
    iput-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10356
    :sswitch_1e
    iget-object v0, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10357
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x1000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10361
    :sswitch_1f
    iget-object v0, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10362
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x2000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10366
    :sswitch_20
    iget-object v0, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10367
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x4000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10371
    :sswitch_21
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 10372
    invoke-static {v0}, Lcom/google/b/f/av;->a(I)Lcom/google/b/f/av;

    move-result-object v3

    .line 10373
    if-nez v3, :cond_f

    .line 10374
    const/16 v3, 0x23

    invoke-virtual {v5, v3, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 10376
    :cond_f
    and-int/lit8 v3, v1, 0x8

    const/16 v6, 0x8

    if-eq v3, v6, :cond_10

    .line 10377
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 10378
    or-int/lit8 v1, v1, 0x8

    .line 10380
    :cond_10
    iget-object v3, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10385
    :sswitch_22
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 10386
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v3

    .line 10387
    :goto_4
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_11

    const/4 v0, -0x1

    :goto_5
    if-lez v0, :cond_14

    .line 10388
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 10389
    invoke-static {v0}, Lcom/google/b/f/av;->a(I)Lcom/google/b/f/av;

    move-result-object v6

    .line 10390
    if-nez v6, :cond_12

    .line 10391
    const/16 v6, 0x23

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_4

    .line 10387
    :cond_11
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_5

    .line 10393
    :cond_12
    and-int/lit8 v6, v1, 0x8

    const/16 v7, 0x8

    if-eq v6, v7, :cond_13

    .line 10394
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 10395
    or-int/lit8 v1, v1, 0x8

    .line 10397
    :cond_13
    iget-object v6, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 10400
    :cond_14
    iput v3, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 10404
    :sswitch_23
    iget-object v0, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10405
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x8000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10409
    :sswitch_24
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x10000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10410
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/google/b/f/an;->J:Z

    goto/16 :goto_0

    :cond_15
    const/4 v0, 0x0

    goto :goto_6

    .line 10414
    :sswitch_25
    iget-object v0, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10415
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x20000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10419
    :sswitch_26
    iget-object v0, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 10420
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    goto/16 :goto_0

    .line 10424
    :sswitch_27
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v3, -0x80000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/an;->a:I

    .line 10425
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/google/b/f/an;->N:Z

    goto/16 :goto_0

    :cond_16
    const/4 v0, 0x0

    goto :goto_7

    .line 10429
    :sswitch_28
    iget v0, p0, Lcom/google/b/f/an;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/an;->b:I

    .line 10430
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/an;->O:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 10441
    :cond_17
    and-int/lit8 v0, v2, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_18

    .line 10442
    iget-object v0, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    .line 10444
    :cond_18
    and-int/lit16 v0, v2, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_19

    .line 10445
    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    .line 10447
    :cond_19
    const/high16 v0, 0x40000

    and-int/2addr v0, v2

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_1a

    .line 10448
    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    .line 10450
    :cond_1a
    const/high16 v0, 0x80000

    and-int/2addr v0, v2

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_1b

    .line 10451
    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    .line 10453
    :cond_1b
    and-int/lit8 v0, v2, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1c

    .line 10454
    iget-object v0, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    .line 10456
    :cond_1c
    and-int/lit8 v0, v1, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1d

    .line 10457
    iget-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 10459
    :cond_1d
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->au:Lcom/google/n/bn;

    .line 10460
    return-void

    :cond_1e
    move-object v3, v0

    goto/16 :goto_1

    .line 10162
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x32 -> :sswitch_6
        0x3b -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
        0x69 -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x88 -> :sswitch_f
        0x92 -> :sswitch_10
        0x9a -> :sswitch_11
        0xa2 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb0 -> :sswitch_14
        0xba -> :sswitch_15
        0xc0 -> :sswitch_16
        0xca -> :sswitch_17
        0xd1 -> :sswitch_18
        0xda -> :sswitch_19
        0xe2 -> :sswitch_1a
        0xea -> :sswitch_1b
        0xf0 -> :sswitch_1c
        0xfa -> :sswitch_1d
        0x102 -> :sswitch_1e
        0x10a -> :sswitch_1f
        0x112 -> :sswitch_20
        0x118 -> :sswitch_21
        0x11a -> :sswitch_22
        0x122 -> :sswitch_23
        0x128 -> :sswitch_24
        0x132 -> :sswitch_25
        0x13a -> :sswitch_26
        0x140 -> :sswitch_27
        0x148 -> :sswitch_28
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 10106
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 11767
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    .line 11783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    .line 11915
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    .line 11946
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    .line 12004
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    .line 12077
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    .line 12093
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    .line 12109
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    .line 12125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    .line 12156
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    .line 12203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    .line 12248
    iput-byte v1, p0, Lcom/google/b/f/an;->Q:B

    .line 12393
    iput v1, p0, Lcom/google/b/f/an;->R:I

    .line 10107
    return-void
.end method

.method public static d()Lcom/google/b/f/an;
    .locals 1

    .prologue
    .line 15426
    sget-object v0, Lcom/google/b/f/an;->P:Lcom/google/b/f/an;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/aq;
    .locals 1

    .prologue
    .line 12639
    new-instance v0, Lcom/google/b/f/aq;

    invoke-direct {v0}, Lcom/google/b/f/aq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10474
    sget-object v0, Lcom/google/b/f/an;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 12272
    invoke-virtual {p0}, Lcom/google/b/f/an;->c()I

    .line 12273
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 12274
    iget v0, p0, Lcom/google/b/f/an;->c:I

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 12276
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 12277
    iget-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12279
    :cond_1
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 12280
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/b/f/an;->e:J

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 12282
    :cond_2
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 12283
    iget v0, p0, Lcom/google/b/f/an;->g:I

    invoke-static {v7, v8}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 12285
    :cond_3
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 12286
    iget v0, p0, Lcom/google/b/f/an;->h:I

    invoke-static {v8, v8}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    :cond_4
    move v0, v1

    .line 12288
    :goto_2
    iget-object v2, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 12289
    const/4 v2, 0x6

    iget-object v4, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12288
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 12274
    :cond_5
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 12277
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 12291
    :cond_7
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_8

    .line 12292
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/b/f/ar;->d()Lcom/google/b/f/ar;

    move-result-object v0

    :goto_3
    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v2, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_8
    move v2, v1

    .line 12294
    :goto_4
    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 12295
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12294
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 12292
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    goto :goto_3

    .line 12297
    :cond_a
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_b

    .line 12298
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/b/f/an;->l:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_14

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 12300
    :cond_b
    :goto_5
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_c

    .line 12301
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/b/f/an;->m:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_15

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 12303
    :cond_c
    :goto_6
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_d

    .line 12304
    const/16 v0, 0xd

    iget-wide v4, p0, Lcom/google/b/f/an;->n:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->c(J)V

    .line 12306
    :cond_d
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_e

    .line 12307
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/b/f/an;->o:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_16

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 12309
    :cond_e
    :goto_7
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_f

    .line 12310
    const/16 v2, 0xf

    iget-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    :goto_8
    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12312
    :cond_f
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_10

    .line 12313
    const/16 v2, 0x10

    iget-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    :goto_9
    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12315
    :cond_10
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_11

    .line 12316
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/b/f/an;->r:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_19

    move v0, v3

    :goto_a
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 12318
    :cond_11
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_12

    .line 12319
    const/16 v0, 0x12

    iget-object v2, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12321
    :cond_12
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_13

    .line 12322
    const/16 v0, 0x13

    iget-object v2, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_13
    move v2, v1

    .line 12324
    :goto_b
    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1a

    .line 12325
    const/16 v4, 0x14

    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12324
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 12298
    :cond_14
    int-to-long v4, v2

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 12301
    :cond_15
    int-to-long v4, v2

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    .line 12307
    :cond_16
    int-to-long v4, v2

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_7

    .line 12310
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 12313
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    :cond_19
    move v0, v1

    .line 12316
    goto/16 :goto_a

    :cond_1a
    move v2, v1

    .line 12327
    :goto_c
    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1b

    .line 12328
    const/16 v4, 0x15

    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12327
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 12330
    :cond_1b
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_1c

    .line 12331
    const/16 v0, 0x16

    iget v2, p0, Lcom/google/b/f/an;->w:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_1d

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    :cond_1c
    :goto_d
    move v0, v1

    .line 12333
    :goto_e
    iget-object v2, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_1e

    .line 12334
    const/16 v2, 0x17

    iget-object v4, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12333
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 12331
    :cond_1d
    int-to-long v4, v2

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_d

    .line 12336
    :cond_1e
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_1f

    .line 12337
    const/16 v0, 0x18

    iget-boolean v2, p0, Lcom/google/b/f/an;->x:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_2a

    move v0, v3

    :goto_f
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 12339
    :cond_1f
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_20

    .line 12340
    const/16 v0, 0x19

    iget-object v2, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12342
    :cond_20
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x40000

    and-int/2addr v0, v2

    const/high16 v2, 0x40000

    if-ne v0, v2, :cond_21

    .line 12343
    const/16 v0, 0x1a

    iget-wide v4, p0, Lcom/google/b/f/an;->z:J

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->c(J)V

    .line 12345
    :cond_21
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x80000

    and-int/2addr v0, v2

    const/high16 v2, 0x80000

    if-ne v0, v2, :cond_22

    .line 12346
    const/16 v0, 0x1b

    iget-object v2, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12348
    :cond_22
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x100000

    and-int/2addr v0, v2

    const/high16 v2, 0x100000

    if-ne v0, v2, :cond_23

    .line 12349
    const/16 v2, 0x1c

    iget-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_2b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    :goto_10
    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12351
    :cond_23
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x200000

    and-int/2addr v0, v2

    const/high16 v2, 0x200000

    if-ne v0, v2, :cond_24

    .line 12352
    const/16 v0, 0x1d

    iget-object v2, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12354
    :cond_24
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x400000

    and-int/2addr v0, v2

    const/high16 v2, 0x400000

    if-ne v0, v2, :cond_25

    .line 12355
    const/16 v0, 0x1e

    iget v2, p0, Lcom/google/b/f/an;->D:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_2c

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 12357
    :cond_25
    :goto_11
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x800000

    and-int/2addr v0, v2

    const/high16 v2, 0x800000

    if-ne v0, v2, :cond_26

    .line 12358
    const/16 v2, 0x1f

    iget-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_2d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    :goto_12
    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12360
    :cond_26
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x1000000

    and-int/2addr v0, v2

    const/high16 v2, 0x1000000

    if-ne v0, v2, :cond_27

    .line 12361
    const/16 v0, 0x20

    iget-object v2, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12363
    :cond_27
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x2000000

    and-int/2addr v0, v2

    const/high16 v2, 0x2000000

    if-ne v0, v2, :cond_28

    .line 12364
    const/16 v0, 0x21

    iget-object v2, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12366
    :cond_28
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x4000000

    and-int/2addr v0, v2

    const/high16 v2, 0x4000000

    if-ne v0, v2, :cond_29

    .line 12367
    const/16 v0, 0x22

    iget-object v2, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_29
    move v2, v1

    .line 12369
    :goto_13
    iget-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2f

    .line 12370
    const/16 v4, 0x23

    iget-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 12369
    :goto_14
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_13

    :cond_2a
    move v0, v1

    .line 12337
    goto/16 :goto_f

    .line 12349
    :cond_2b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_10

    .line 12355
    :cond_2c
    int-to-long v4, v2

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_11

    .line 12358
    :cond_2d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_12

    .line 12370
    :cond_2e
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_14

    .line 12372
    :cond_2f
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x8000000

    and-int/2addr v0, v2

    const/high16 v2, 0x8000000

    if-ne v0, v2, :cond_30

    .line 12373
    const/16 v0, 0x24

    iget-object v2, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12375
    :cond_30
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x10000000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000000

    if-ne v0, v2, :cond_31

    .line 12376
    const/16 v0, 0x25

    iget-boolean v2, p0, Lcom/google/b/f/an;->J:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_36

    move v0, v3

    :goto_15
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 12378
    :cond_31
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x20000000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000000

    if-ne v0, v2, :cond_32

    .line 12379
    const/16 v0, 0x26

    iget-object v2, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12381
    :cond_32
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x40000000    # 2.0f

    and-int/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_33

    .line 12382
    const/16 v0, 0x27

    iget-object v2, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12384
    :cond_33
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, -0x80000000

    and-int/2addr v0, v2

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_34

    .line 12385
    const/16 v0, 0x28

    iget-boolean v2, p0, Lcom/google/b/f/an;->N:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_37

    move v0, v3

    :goto_16
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 12387
    :cond_34
    iget v0, p0, Lcom/google/b/f/an;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_35

    .line 12388
    const/16 v0, 0x29

    iget-wide v2, p0, Lcom/google/b/f/an;->O:J

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 12390
    :cond_35
    iget-object v0, p0, Lcom/google/b/f/an;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12391
    return-void

    :cond_36
    move v0, v1

    .line 12376
    goto :goto_15

    :cond_37
    move v0, v1

    .line 12385
    goto :goto_16
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x1000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12250
    iget-byte v0, p0, Lcom/google/b/f/an;->Q:B

    .line 12251
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 12267
    :goto_0
    return v0

    .line 12252
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 12254
    :cond_1
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 12255
    iget-object v0, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/k;->d()Lcom/google/o/c/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/k;

    invoke-virtual {v0}, Lcom/google/o/c/k;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12256
    iput-byte v2, p0, Lcom/google/b/f/an;->Q:B

    move v0, v2

    .line 12257
    goto :goto_0

    :cond_2
    move v0, v2

    .line 12254
    goto :goto_1

    .line 12260
    :cond_3
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 12261
    iget-object v0, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/f;->d()Lcom/google/o/c/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/f;

    invoke-virtual {v0}, Lcom/google/o/c/f;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 12262
    iput-byte v2, p0, Lcom/google/b/f/an;->Q:B

    move v0, v2

    .line 12263
    goto :goto_0

    :cond_4
    move v0, v2

    .line 12260
    goto :goto_2

    .line 12266
    :cond_5
    iput-byte v1, p0, Lcom/google/b/f/an;->Q:B

    move v0, v1

    .line 12267
    goto :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/high16 v9, -0x80000000

    const/4 v8, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 12395
    iget v0, p0, Lcom/google/b/f/an;->R:I

    .line 12396
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 12572
    :goto_0
    return v0

    .line 12399
    :cond_0
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v8, :cond_34

    .line 12400
    iget v0, p0, Lcom/google/b/f/an;->c:I

    .line 12401
    invoke-static {v8, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 12403
    :goto_2
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 12405
    iget-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12407
    :cond_1
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_2

    .line 12408
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/b/f/an;->e:J

    .line 12409
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12411
    :cond_2
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 12412
    const/4 v0, 0x4

    iget v4, p0, Lcom/google/b/f/an;->g:I

    .line 12413
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v2, v0

    .line 12415
    :cond_3
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_4

    .line 12416
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/b/f/an;->h:I

    .line 12417
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v2, v0

    :cond_4
    move v0, v3

    move v4, v3

    .line 12421
    :goto_4
    iget-object v5, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_7

    .line 12422
    iget-object v5, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    .line 12423
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 12421
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    .line 12401
    goto/16 :goto_1

    .line 12405
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 12425
    :cond_7
    add-int v0, v2, v4

    .line 12426
    iget-object v2, p0, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 12428
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_33

    .line 12429
    const/4 v4, 0x7

    .line 12430
    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/b/f/ar;->d()Lcom/google/b/f/ar;

    move-result-object v0

    :goto_5
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    :goto_6
    move v2, v3

    move v4, v0

    .line 12432
    :goto_7
    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 12433
    iget-object v0, p0, Lcom/google/b/f/an;->k:Ljava/util/List;

    .line 12434
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12432
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 12430
    :cond_8
    iget-object v0, p0, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    goto :goto_5

    .line 12436
    :cond_9
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_a

    .line 12437
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/b/f/an;->l:I

    .line 12438
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_13

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12440
    :cond_a
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_b

    .line 12441
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/b/f/an;->m:I

    .line 12442
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_14

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12444
    :cond_b
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_c

    .line 12445
    const/16 v0, 0xd

    iget-wide v6, p0, Lcom/google/b/f/an;->n:D

    .line 12446
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v4, v0

    .line 12448
    :cond_c
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_d

    .line 12449
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/b/f/an;->o:I

    .line 12450
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_15

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12452
    :cond_d
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_e

    .line 12453
    const/16 v2, 0xf

    .line 12454
    iget-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    :goto_b
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 12456
    :cond_e
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_f

    .line 12457
    const/16 v2, 0x10

    .line 12458
    iget-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    :goto_c
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 12460
    :cond_f
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_10

    .line 12461
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/b/f/an;->r:Z

    .line 12462
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 12464
    :cond_10
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_11

    .line 12465
    const/16 v0, 0x12

    iget-object v2, p0, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    .line 12466
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 12468
    :cond_11
    iget v0, p0, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_12

    .line 12469
    const/16 v0, 0x13

    iget-object v2, p0, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    .line 12470
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    :cond_12
    move v2, v3

    .line 12472
    :goto_d
    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_18

    .line 12473
    const/16 v5, 0x14

    iget-object v0, p0, Lcom/google/b/f/an;->u:Ljava/util/List;

    .line 12474
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12472
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    :cond_13
    move v0, v1

    .line 12438
    goto/16 :goto_8

    :cond_14
    move v0, v1

    .line 12442
    goto/16 :goto_9

    :cond_15
    move v0, v1

    .line 12450
    goto/16 :goto_a

    .line 12454
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_b

    .line 12458
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    :cond_18
    move v2, v3

    .line 12476
    :goto_e
    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_19

    .line 12477
    const/16 v5, 0x15

    iget-object v0, p0, Lcom/google/b/f/an;->v:Ljava/util/List;

    .line 12478
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 12476
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_e

    .line 12480
    :cond_19
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_1a

    .line 12481
    const/16 v0, 0x16

    iget v2, p0, Lcom/google/b/f/an;->w:I

    .line 12482
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_1b

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_f
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    :cond_1a
    move v0, v3

    move v2, v3

    .line 12486
    :goto_10
    iget-object v5, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_1c

    .line 12487
    iget-object v5, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    .line 12488
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 12486
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1b
    move v0, v1

    .line 12482
    goto :goto_f

    .line 12490
    :cond_1c
    add-int v0, v4, v2

    .line 12491
    iget-object v2, p0, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 12493
    iget v2, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v2, v4

    const/high16 v4, 0x10000

    if-ne v2, v4, :cond_1d

    .line 12494
    const/16 v2, 0x18

    iget-boolean v4, p0, Lcom/google/b/f/an;->x:Z

    .line 12495
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 12497
    :cond_1d
    iget v2, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x20000

    and-int/2addr v2, v4

    const/high16 v4, 0x20000

    if-ne v2, v4, :cond_1e

    .line 12498
    const/16 v2, 0x19

    iget-object v4, p0, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    .line 12499
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 12501
    :cond_1e
    iget v2, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x40000

    and-int/2addr v2, v4

    const/high16 v4, 0x40000

    if-ne v2, v4, :cond_1f

    .line 12502
    const/16 v2, 0x1a

    iget-wide v4, p0, Lcom/google/b/f/an;->z:J

    .line 12503
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 12505
    :cond_1f
    iget v2, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x80000

    and-int/2addr v2, v4

    const/high16 v4, 0x80000

    if-ne v2, v4, :cond_32

    .line 12506
    const/16 v2, 0x1b

    iget-object v4, p0, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    .line 12507
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 12509
    :goto_11
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x100000

    and-int/2addr v0, v4

    const/high16 v4, 0x100000

    if-ne v0, v4, :cond_20

    .line 12510
    const/16 v4, 0x1c

    .line 12511
    iget-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_27

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    :goto_12
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12513
    :cond_20
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x200000

    and-int/2addr v0, v4

    const/high16 v4, 0x200000

    if-ne v0, v4, :cond_21

    .line 12514
    const/16 v0, 0x1d

    iget-object v4, p0, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    .line 12515
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12517
    :cond_21
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x400000

    and-int/2addr v0, v4

    const/high16 v4, 0x400000

    if-ne v0, v4, :cond_22

    .line 12518
    const/16 v0, 0x1e

    iget v4, p0, Lcom/google/b/f/an;->D:I

    .line 12519
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_28

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_13
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 12521
    :cond_22
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x800000

    and-int/2addr v0, v4

    const/high16 v4, 0x800000

    if-ne v0, v4, :cond_23

    .line 12522
    const/16 v4, 0x1f

    .line 12523
    iget-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_29

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    :goto_14
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12525
    :cond_23
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x1000000

    and-int/2addr v0, v4

    const/high16 v4, 0x1000000

    if-ne v0, v4, :cond_24

    .line 12526
    const/16 v0, 0x20

    iget-object v4, p0, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    .line 12527
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12529
    :cond_24
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x2000000

    and-int/2addr v0, v4

    const/high16 v4, 0x2000000

    if-ne v0, v4, :cond_25

    .line 12530
    const/16 v0, 0x21

    iget-object v4, p0, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    .line 12531
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 12533
    :cond_25
    iget v0, p0, Lcom/google/b/f/an;->a:I

    const/high16 v4, 0x4000000

    and-int/2addr v0, v4

    const/high16 v4, 0x4000000

    if-ne v0, v4, :cond_26

    .line 12534
    const/16 v0, 0x22

    iget-object v4, p0, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    .line 12535
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    :cond_26
    move v4, v3

    move v5, v3

    .line 12539
    :goto_15
    iget-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2b

    .line 12540
    iget-object v0, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    .line 12541
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_16
    add-int/2addr v5, v0

    .line 12539
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_15

    .line 12511
    :cond_27
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_12

    :cond_28
    move v0, v1

    .line 12519
    goto/16 :goto_13

    .line 12523
    :cond_29
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_14

    :cond_2a
    move v0, v1

    .line 12541
    goto :goto_16

    .line 12543
    :cond_2b
    add-int v0, v2, v5

    .line 12544
    iget-object v1, p0, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 12546
    iget v1, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x8000000

    and-int/2addr v1, v2

    const/high16 v2, 0x8000000

    if-ne v1, v2, :cond_2c

    .line 12547
    const/16 v1, 0x24

    iget-object v2, p0, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    .line 12548
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 12550
    :cond_2c
    iget v1, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x10000000

    and-int/2addr v1, v2

    const/high16 v2, 0x10000000

    if-ne v1, v2, :cond_2d

    .line 12551
    const/16 v1, 0x25

    iget-boolean v2, p0, Lcom/google/b/f/an;->J:Z

    .line 12552
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12554
    :cond_2d
    iget v1, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x20000000

    and-int/2addr v1, v2

    const/high16 v2, 0x20000000

    if-ne v1, v2, :cond_2e

    .line 12555
    const/16 v1, 0x26

    iget-object v2, p0, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    .line 12556
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 12558
    :cond_2e
    iget v1, p0, Lcom/google/b/f/an;->a:I

    const/high16 v2, 0x40000000    # 2.0f

    and-int/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_2f

    .line 12559
    const/16 v1, 0x27

    iget-object v2, p0, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    .line 12560
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 12562
    :cond_2f
    iget v1, p0, Lcom/google/b/f/an;->a:I

    and-int/2addr v1, v9

    if-ne v1, v9, :cond_30

    .line 12563
    const/16 v1, 0x28

    iget-boolean v2, p0, Lcom/google/b/f/an;->N:Z

    .line 12564
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12566
    :cond_30
    iget v1, p0, Lcom/google/b/f/an;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v8, :cond_31

    .line 12567
    const/16 v1, 0x29

    iget-wide v4, p0, Lcom/google/b/f/an;->O:J

    .line 12568
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 12570
    :cond_31
    iget-object v1, p0, Lcom/google/b/f/an;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 12571
    iput v0, p0, Lcom/google/b/f/an;->R:I

    goto/16 :goto_0

    :cond_32
    move v2, v0

    goto/16 :goto_11

    :cond_33
    move v0, v2

    goto/16 :goto_6

    :cond_34
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 10100
    invoke-static {}, Lcom/google/b/f/an;->newBuilder()Lcom/google/b/f/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/aq;->a(Lcom/google/b/f/an;)Lcom/google/b/f/aq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 10100
    invoke-static {}, Lcom/google/b/f/an;->newBuilder()Lcom/google/b/f/aq;

    move-result-object v0

    return-object v0
.end method
