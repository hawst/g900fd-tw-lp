.class public final Lcom/google/b/f/b/a/bk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/bi;",
        "Lcom/google/b/f/b/a/bk;",
        ">;",
        "Lcom/google/b/f/b/a/bx;"
    }
.end annotation


# instance fields
.field public A:Lcom/google/n/ao;

.field public B:I

.field public C:I

.field public D:Ljava/lang/Object;

.field public E:Ljava/lang/Object;

.field public F:I

.field public G:I

.field public H:I

.field private J:Z

.field private K:F

.field private L:I

.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Lcom/google/n/ao;

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public r:I

.field public s:Z

.field public t:I

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/google/n/ao;

.field public x:Lcom/google/n/ao;

.field public y:Lcom/google/n/ao;

.field public z:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2292
    sget-object v0, Lcom/google/b/f/b/a/bi;->L:Lcom/google/b/f/b/a/bi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2680
    iput v1, p0, Lcom/google/b/f/b/a/bk;->c:I

    .line 2876
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->i:Lcom/google/n/ao;

    .line 3160
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    .line 3393
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    .line 3625
    iput v1, p0, Lcom/google/b/f/b/a/bk;->L:I

    .line 3661
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->w:Lcom/google/n/ao;

    .line 3720
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->x:Lcom/google/n/ao;

    .line 3779
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->y:Lcom/google/n/ao;

    .line 3838
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->z:Lcom/google/n/ao;

    .line 3897
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->A:Lcom/google/n/ao;

    .line 4020
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->D:Ljava/lang/Object;

    .line 4096
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->E:Ljava/lang/Object;

    .line 2293
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/bi;)Lcom/google/b/f/b/a/bk;
    .locals 7

    .prologue
    const/high16 v6, 0x10000

    const v4, 0x8000

    const/high16 v5, -0x80000000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2539
    invoke-static {}, Lcom/google/b/f/b/a/bi;->d()Lcom/google/b/f/b/a/bi;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2670
    :goto_0
    return-object p0

    .line 2540
    :cond_0
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 2541
    iget v0, p1, Lcom/google/b/f/b/a/bi;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 2540
    goto :goto_1

    .line 2541
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hm;->h:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->c:I

    .line 2543
    :cond_4
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_28

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 2544
    iget v0, p1, Lcom/google/b/f/b/a/bi;->d:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->d:I

    .line 2546
    :cond_5
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_29

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 2547
    iget v0, p1, Lcom/google/b/f/b/a/bi;->e:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->e:I

    .line 2549
    :cond_6
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    .line 2550
    iget v0, p1, Lcom/google/b/f/b/a/bi;->f:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->f:I

    .line 2552
    :cond_7
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2b

    move v0, v1

    :goto_5
    if-eqz v0, :cond_8

    .line 2553
    iget v0, p1, Lcom/google/b/f/b/a/bi;->g:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->g:I

    .line 2555
    :cond_8
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_9

    .line 2556
    iget v0, p1, Lcom/google/b/f/b/a/bi;->h:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->h:I

    .line 2558
    :cond_9
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2d

    move v0, v1

    :goto_7
    if-eqz v0, :cond_a

    .line 2559
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2560
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2562
    :cond_a
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2e

    move v0, v1

    :goto_8
    if-eqz v0, :cond_b

    .line 2563
    iget v0, p1, Lcom/google/b/f/b/a/bi;->j:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->j:I

    .line 2565
    :cond_b
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2f

    move v0, v1

    :goto_9
    if-eqz v0, :cond_c

    .line 2566
    iget v0, p1, Lcom/google/b/f/b/a/bi;->k:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->k:I

    .line 2568
    :cond_c
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_30

    move v0, v1

    :goto_a
    if-eqz v0, :cond_d

    .line 2569
    iget v0, p1, Lcom/google/b/f/b/a/bi;->l:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->l:I

    .line 2571
    :cond_d
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_31

    move v0, v1

    :goto_b
    if-eqz v0, :cond_e

    .line 2572
    iget v0, p1, Lcom/google/b/f/b/a/bi;->m:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->m:I

    .line 2574
    :cond_e
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_32

    move v0, v1

    :goto_c
    if-eqz v0, :cond_f

    .line 2575
    iget v0, p1, Lcom/google/b/f/b/a/bi;->n:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->n:I

    .line 2577
    :cond_f
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_33

    move v0, v1

    :goto_d
    if-eqz v0, :cond_10

    .line 2578
    iget v0, p1, Lcom/google/b/f/b/a/bi;->o:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->o:I

    .line 2580
    :cond_10
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_34

    move v0, v1

    :goto_e
    if-eqz v0, :cond_11

    .line 2581
    iget v0, p1, Lcom/google/b/f/b/a/bi;->p:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->p:I

    .line 2583
    :cond_11
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 2584
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 2585
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    .line 2586
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2593
    :cond_12
    :goto_f
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_36

    move v0, v1

    :goto_10
    if-eqz v0, :cond_13

    .line 2594
    iget v0, p1, Lcom/google/b/f/b/a/bi;->r:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->r:I

    .line 2596
    :cond_13
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_37

    move v0, v1

    :goto_11
    if-eqz v0, :cond_14

    .line 2597
    iget-boolean v0, p1, Lcom/google/b/f/b/a/bi;->s:Z

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/b/a/bk;->s:Z

    .line 2599
    :cond_14
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_38

    move v0, v1

    :goto_12
    if-eqz v0, :cond_15

    .line 2600
    iget v0, p1, Lcom/google/b/f/b/a/bi;->t:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->t:I

    .line 2602
    :cond_15
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 2603
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2604
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    .line 2605
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const v3, -0x40001

    and-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2612
    :cond_16
    :goto_13
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_3a

    move v0, v1

    :goto_14
    if-eqz v0, :cond_17

    .line 2613
    iget-boolean v0, p1, Lcom/google/b/f/b/a/bi;->v:Z

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/b/a/bk;->v:Z

    .line 2615
    :cond_17
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_3b

    move v0, v1

    :goto_15
    if-eqz v0, :cond_18

    .line 2616
    iget-boolean v0, p1, Lcom/google/b/f/b/a/bi;->w:Z

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/b/a/bk;->J:Z

    .line 2618
    :cond_18
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_3c

    move v0, v1

    :goto_16
    if-eqz v0, :cond_19

    .line 2619
    iget v0, p1, Lcom/google/b/f/b/a/bi;->x:F

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->K:F

    .line 2621
    :cond_19
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_3d

    move v0, v1

    :goto_17
    if-eqz v0, :cond_1b

    .line 2622
    iget v0, p1, Lcom/google/b/f/b/a/bi;->y:I

    invoke-static {v0}, Lcom/google/b/f/b/a/bl;->a(I)Lcom/google/b/f/b/a/bl;

    move-result-object v0

    if-nez v0, :cond_1a

    sget-object v0, Lcom/google/b/f/b/a/bl;->a:Lcom/google/b/f/b/a/bl;

    :cond_1a
    invoke-virtual {p0, v0}, Lcom/google/b/f/b/a/bk;->a(Lcom/google/b/f/b/a/bl;)Lcom/google/b/f/b/a/bk;

    .line 2624
    :cond_1b
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_3e

    move v0, v1

    :goto_18
    if-eqz v0, :cond_1c

    .line 2625
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->w:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2626
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x800000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2628
    :cond_1c
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_3f

    move v0, v1

    :goto_19
    if-eqz v0, :cond_1d

    .line 2629
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->x:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2630
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x1000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2632
    :cond_1d
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v0, v3

    const/high16 v3, 0x800000

    if-ne v0, v3, :cond_40

    move v0, v1

    :goto_1a
    if-eqz v0, :cond_1e

    .line 2633
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->y:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2634
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x2000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2636
    :cond_1e
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_41

    move v0, v1

    :goto_1b
    if-eqz v0, :cond_1f

    .line 2637
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->z:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2638
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x4000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2640
    :cond_1f
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    const/high16 v3, 0x2000000

    if-ne v0, v3, :cond_42

    move v0, v1

    :goto_1c
    if-eqz v0, :cond_20

    .line 2641
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->A:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2642
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x8000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2644
    :cond_20
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v0, v3

    const/high16 v3, 0x4000000

    if-ne v0, v3, :cond_43

    move v0, v1

    :goto_1d
    if-eqz v0, :cond_21

    .line 2645
    iget v0, p1, Lcom/google/b/f/b/a/bi;->E:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->B:I

    .line 2647
    :cond_21
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v0, v3

    const/high16 v3, 0x8000000

    if-ne v0, v3, :cond_44

    move v0, v1

    :goto_1e
    if-eqz v0, :cond_22

    .line 2648
    iget v0, p1, Lcom/google/b/f/b/a/bi;->F:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->C:I

    .line 2650
    :cond_22
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_45

    move v0, v1

    :goto_1f
    if-eqz v0, :cond_23

    .line 2651
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2652
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->D:Ljava/lang/Object;

    .line 2655
    :cond_23
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000000

    if-ne v0, v3, :cond_46

    move v0, v1

    :goto_20
    if-eqz v0, :cond_24

    .line 2656
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 2657
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->E:Ljava/lang/Object;

    .line 2660
    :cond_24
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_47

    move v0, v1

    :goto_21
    if-eqz v0, :cond_25

    .line 2661
    iget v0, p1, Lcom/google/b/f/b/a/bi;->I:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/bk;->b:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->F:I

    .line 2663
    :cond_25
    iget v0, p1, Lcom/google/b/f/b/a/bi;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_48

    move v0, v1

    :goto_22
    if-eqz v0, :cond_26

    .line 2664
    iget v0, p1, Lcom/google/b/f/b/a/bi;->J:I

    iget v3, p0, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/bk;->b:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->G:I

    .line 2666
    :cond_26
    iget v0, p1, Lcom/google/b/f/b/a/bi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_49

    move v0, v1

    :goto_23
    if-eqz v0, :cond_27

    .line 2667
    iget v0, p1, Lcom/google/b/f/b/a/bi;->K:I

    iget v1, p0, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/b/a/bk;->b:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->H:I

    .line 2669
    :cond_27
    iget-object v0, p1, Lcom/google/b/f/b/a/bi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_28
    move v0, v2

    .line 2543
    goto/16 :goto_2

    :cond_29
    move v0, v2

    .line 2546
    goto/16 :goto_3

    :cond_2a
    move v0, v2

    .line 2549
    goto/16 :goto_4

    :cond_2b
    move v0, v2

    .line 2552
    goto/16 :goto_5

    :cond_2c
    move v0, v2

    .line 2555
    goto/16 :goto_6

    :cond_2d
    move v0, v2

    .line 2558
    goto/16 :goto_7

    :cond_2e
    move v0, v2

    .line 2562
    goto/16 :goto_8

    :cond_2f
    move v0, v2

    .line 2565
    goto/16 :goto_9

    :cond_30
    move v0, v2

    .line 2568
    goto/16 :goto_a

    :cond_31
    move v0, v2

    .line 2571
    goto/16 :goto_b

    :cond_32
    move v0, v2

    .line 2574
    goto/16 :goto_c

    :cond_33
    move v0, v2

    .line 2577
    goto/16 :goto_d

    :cond_34
    move v0, v2

    .line 2580
    goto/16 :goto_e

    .line 2588
    :cond_35
    invoke-virtual {p0}, Lcom/google/b/f/b/a/bk;->c()V

    .line 2589
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_f

    :cond_36
    move v0, v2

    .line 2593
    goto/16 :goto_10

    :cond_37
    move v0, v2

    .line 2596
    goto/16 :goto_11

    :cond_38
    move v0, v2

    .line 2599
    goto/16 :goto_12

    .line 2607
    :cond_39
    invoke-virtual {p0}, Lcom/google/b/f/b/a/bk;->d()V

    .line 2608
    iget-object v0, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_13

    :cond_3a
    move v0, v2

    .line 2612
    goto/16 :goto_14

    :cond_3b
    move v0, v2

    .line 2615
    goto/16 :goto_15

    :cond_3c
    move v0, v2

    .line 2618
    goto/16 :goto_16

    :cond_3d
    move v0, v2

    .line 2621
    goto/16 :goto_17

    :cond_3e
    move v0, v2

    .line 2624
    goto/16 :goto_18

    :cond_3f
    move v0, v2

    .line 2628
    goto/16 :goto_19

    :cond_40
    move v0, v2

    .line 2632
    goto/16 :goto_1a

    :cond_41
    move v0, v2

    .line 2636
    goto/16 :goto_1b

    :cond_42
    move v0, v2

    .line 2640
    goto/16 :goto_1c

    :cond_43
    move v0, v2

    .line 2644
    goto/16 :goto_1d

    :cond_44
    move v0, v2

    .line 2647
    goto/16 :goto_1e

    :cond_45
    move v0, v2

    .line 2650
    goto/16 :goto_1f

    :cond_46
    move v0, v2

    .line 2655
    goto/16 :goto_20

    :cond_47
    move v0, v2

    .line 2660
    goto/16 :goto_21

    :cond_48
    move v0, v2

    .line 2663
    goto/16 :goto_22

    :cond_49
    move v0, v2

    .line 2666
    goto/16 :goto_23
.end method

.method public final a(Lcom/google/b/f/b/a/bl;)Lcom/google/b/f/b/a/bk;
    .locals 2

    .prologue
    .line 3643
    if-nez p1, :cond_0

    .line 3644
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3646
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v1, 0x400000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 3647
    iget v0, p1, Lcom/google/b/f/b/a/bl;->d:I

    iput v0, p0, Lcom/google/b/f/b/a/bk;->L:I

    .line 3649
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x10000

    const v10, 0x8000

    const/high16 v9, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2284
    new-instance v3, Lcom/google/b/f/b/a/bi;

    invoke-direct {v3, p0}, Lcom/google/b/f/b/a/bi;-><init>(Lcom/google/n/v;)V

    iget v4, p0, Lcom/google/b/f/b/a/bk;->a:I

    iget v5, p0, Lcom/google/b/f/b/a/bk;->b:I

    and-int/lit8 v0, v4, 0x1

    if-ne v0, v1, :cond_22

    move v0, v1

    :goto_0
    iget v6, p0, Lcom/google/b/f/b/a/bk;->c:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->c:I

    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v6, p0, Lcom/google/b/f/b/a/bk;->d:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->d:I

    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v6, p0, Lcom/google/b/f/b/a/bk;->e:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->e:I

    and-int/lit8 v6, v4, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v6, p0, Lcom/google/b/f/b/a/bk;->f:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->f:I

    and-int/lit8 v6, v4, 0x10

    const/16 v7, 0x10

    if-ne v6, v7, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v6, p0, Lcom/google/b/f/b/a/bk;->g:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->g:I

    and-int/lit8 v6, v4, 0x20

    const/16 v7, 0x20

    if-ne v6, v7, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v6, p0, Lcom/google/b/f/b/a/bk;->h:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->h:I

    and-int/lit8 v6, v4, 0x40

    const/16 v7, 0x40

    if-ne v6, v7, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->i:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->i:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/lit16 v6, v4, 0x80

    const/16 v7, 0x80

    if-ne v6, v7, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v6, p0, Lcom/google/b/f/b/a/bk;->j:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->j:I

    and-int/lit16 v6, v4, 0x100

    const/16 v7, 0x100

    if-ne v6, v7, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v6, p0, Lcom/google/b/f/b/a/bk;->k:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->k:I

    and-int/lit16 v6, v4, 0x200

    const/16 v7, 0x200

    if-ne v6, v7, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v6, p0, Lcom/google/b/f/b/a/bk;->l:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->l:I

    and-int/lit16 v6, v4, 0x400

    const/16 v7, 0x400

    if-ne v6, v7, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v6, p0, Lcom/google/b/f/b/a/bk;->m:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->m:I

    and-int/lit16 v6, v4, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v6, p0, Lcom/google/b/f/b/a/bk;->n:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->n:I

    and-int/lit16 v6, v4, 0x1000

    const/16 v7, 0x1000

    if-ne v6, v7, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v6, p0, Lcom/google/b/f/b/a/bk;->o:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->o:I

    and-int/lit16 v6, v4, 0x2000

    const/16 v7, 0x2000

    if-ne v6, v7, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget v6, p0, Lcom/google/b/f/b/a/bk;->p:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->p:I

    iget v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    and-int/lit16 v6, v6, 0x4000

    const/16 v7, 0x4000

    if-ne v6, v7, :cond_d

    iget-object v6, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    and-int/lit16 v6, v6, -0x4001

    iput v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    :cond_d
    iget-object v6, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    and-int v6, v4, v10

    if-ne v6, v10, :cond_e

    or-int/lit16 v0, v0, 0x4000

    :cond_e
    iget v6, p0, Lcom/google/b/f/b/a/bk;->r:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->r:I

    and-int v6, v4, v11

    if-ne v6, v11, :cond_f

    or-int/2addr v0, v10

    :cond_f
    iget-boolean v6, p0, Lcom/google/b/f/b/a/bk;->s:Z

    iput-boolean v6, v3, Lcom/google/b/f/b/a/bi;->s:Z

    const/high16 v6, 0x20000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_10

    or-int/2addr v0, v11

    :cond_10
    iget v6, p0, Lcom/google/b/f/b/a/bk;->t:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->t:I

    iget v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v7, 0x40000

    and-int/2addr v6, v7

    const/high16 v7, 0x40000

    if-ne v6, v7, :cond_11

    iget-object v6, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    const v7, -0x40001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/b/a/bk;->a:I

    :cond_11
    iget-object v6, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    const/high16 v6, 0x80000

    and-int/2addr v6, v4

    const/high16 v7, 0x80000

    if-ne v6, v7, :cond_12

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    :cond_12
    iget-boolean v6, p0, Lcom/google/b/f/b/a/bk;->v:Z

    iput-boolean v6, v3, Lcom/google/b/f/b/a/bi;->v:Z

    const/high16 v6, 0x100000

    and-int/2addr v6, v4

    const/high16 v7, 0x100000

    if-ne v6, v7, :cond_13

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    :cond_13
    iget-boolean v6, p0, Lcom/google/b/f/b/a/bk;->J:Z

    iput-boolean v6, v3, Lcom/google/b/f/b/a/bi;->w:Z

    const/high16 v6, 0x200000

    and-int/2addr v6, v4

    const/high16 v7, 0x200000

    if-ne v6, v7, :cond_14

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    :cond_14
    iget v6, p0, Lcom/google/b/f/b/a/bk;->K:F

    iput v6, v3, Lcom/google/b/f/b/a/bi;->x:F

    const/high16 v6, 0x400000

    and-int/2addr v6, v4

    const/high16 v7, 0x400000

    if-ne v6, v7, :cond_15

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    :cond_15
    iget v6, p0, Lcom/google/b/f/b/a/bk;->L:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->y:I

    const/high16 v6, 0x800000

    and-int/2addr v6, v4

    const/high16 v7, 0x800000

    if-ne v6, v7, :cond_16

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    :cond_16
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->w:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->w:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x1000000

    and-int/2addr v6, v4

    const/high16 v7, 0x1000000

    if-ne v6, v7, :cond_17

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    :cond_17
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->x:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->x:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x2000000

    and-int/2addr v6, v4

    const/high16 v7, 0x2000000

    if-ne v6, v7, :cond_18

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    :cond_18
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->y:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->y:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x4000000

    and-int/2addr v6, v4

    const/high16 v7, 0x4000000

    if-ne v6, v7, :cond_19

    const/high16 v6, 0x1000000

    or-int/2addr v0, v6

    :cond_19
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->z:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->z:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x8000000

    and-int/2addr v6, v4

    const/high16 v7, 0x8000000

    if-ne v6, v7, :cond_1a

    const/high16 v6, 0x2000000

    or-int/2addr v0, v6

    :cond_1a
    iget-object v6, v3, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/b/a/bk;->A:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/b/a/bk;->A:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x10000000

    and-int/2addr v6, v4

    const/high16 v7, 0x10000000

    if-ne v6, v7, :cond_1b

    const/high16 v6, 0x4000000

    or-int/2addr v0, v6

    :cond_1b
    iget v6, p0, Lcom/google/b/f/b/a/bk;->B:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->E:I

    const/high16 v6, 0x20000000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000000

    if-ne v6, v7, :cond_1c

    const/high16 v6, 0x8000000

    or-int/2addr v0, v6

    :cond_1c
    iget v6, p0, Lcom/google/b/f/b/a/bk;->C:I

    iput v6, v3, Lcom/google/b/f/b/a/bi;->F:I

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v6, v7, :cond_1d

    const/high16 v6, 0x10000000

    or-int/2addr v0, v6

    :cond_1d
    iget-object v6, p0, Lcom/google/b/f/b/a/bk;->D:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_1e

    const/high16 v4, 0x20000000

    or-int/2addr v0, v4

    :cond_1e
    iget-object v4, p0, Lcom/google/b/f/b/a/bk;->E:Ljava/lang/Object;

    iput-object v4, v3, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    and-int/lit8 v4, v5, 0x1

    if-ne v4, v1, :cond_1f

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v0, v4

    :cond_1f
    iget v4, p0, Lcom/google/b/f/b/a/bk;->F:I

    iput v4, v3, Lcom/google/b/f/b/a/bi;->I:I

    and-int/lit8 v4, v5, 0x2

    const/4 v6, 0x2

    if-ne v4, v6, :cond_20

    or-int/2addr v0, v9

    :cond_20
    iget v4, p0, Lcom/google/b/f/b/a/bk;->G:I

    iput v4, v3, Lcom/google/b/f/b/a/bi;->J:I

    and-int/lit8 v4, v5, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_21

    :goto_1
    iget v2, p0, Lcom/google/b/f/b/a/bk;->H:I

    iput v2, v3, Lcom/google/b/f/b/a/bi;->K:I

    iput v0, v3, Lcom/google/b/f/b/a/bi;->a:I

    iput v1, v3, Lcom/google/b/f/b/a/bi;->b:I

    return-object v3

    :cond_21
    move v1, v2

    goto :goto_1

    :cond_22
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2284
    check-cast p1, Lcom/google/b/f/b/a/bi;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/bk;->a(Lcom/google/b/f/b/a/bi;)Lcom/google/b/f/b/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2674
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 3162
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-eq v0, v1, :cond_0

    .line 3163
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    .line 3166
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 3168
    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/high16 v2, 0x40000

    .line 3395
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_0

    .line 3396
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/b/a/bk;->u:Ljava/util/List;

    .line 3399
    iget v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/b/f/b/a/bk;->a:I

    .line 3401
    :cond_0
    return-void
.end method
