.class public final enum Lcom/google/b/f/b/a/bq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/bq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/bq;

.field public static final enum b:Lcom/google/b/f/b/a/bq;

.field public static final enum c:Lcom/google/b/f/b/a/bq;

.field private static final synthetic e:[Lcom/google/b/f/b/a/bq;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 473
    new-instance v0, Lcom/google/b/f/b/a/bq;

    const-string v1, "OKAY"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bq;->a:Lcom/google/b/f/b/a/bq;

    .line 477
    new-instance v0, Lcom/google/b/f/b/a/bq;

    const-string v1, "NO_ENDPOINTS_FOUND"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bq;->b:Lcom/google/b/f/b/a/bq;

    .line 481
    new-instance v0, Lcom/google/b/f/b/a/bq;

    const-string v1, "NO_PATH_FOUND"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bq;->c:Lcom/google/b/f/b/a/bq;

    .line 468
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/f/b/a/bq;

    sget-object v1, Lcom/google/b/f/b/a/bq;->a:Lcom/google/b/f/b/a/bq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/b/a/bq;->b:Lcom/google/b/f/b/a/bq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/b/a/bq;->c:Lcom/google/b/f/b/a/bq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/f/b/a/bq;->e:[Lcom/google/b/f/b/a/bq;

    .line 516
    new-instance v0, Lcom/google/b/f/b/a/br;

    invoke-direct {v0}, Lcom/google/b/f/b/a/br;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 525
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 526
    iput p3, p0, Lcom/google/b/f/b/a/bq;->d:I

    .line 527
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/bq;
    .locals 1

    .prologue
    .line 503
    packed-switch p0, :pswitch_data_0

    .line 507
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 504
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/bq;->a:Lcom/google/b/f/b/a/bq;

    goto :goto_0

    .line 505
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/bq;->b:Lcom/google/b/f/b/a/bq;

    goto :goto_0

    .line 506
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/bq;->c:Lcom/google/b/f/b/a/bq;

    goto :goto_0

    .line 503
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/bq;
    .locals 1

    .prologue
    .line 468
    const-class v0, Lcom/google/b/f/b/a/bq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/bq;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/bq;
    .locals 1

    .prologue
    .line 468
    sget-object v0, Lcom/google/b/f/b/a/bq;->e:[Lcom/google/b/f/b/a/bq;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/bq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/bq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 499
    iget v0, p0, Lcom/google/b/f/b/a/bq;->d:I

    return v0
.end method
