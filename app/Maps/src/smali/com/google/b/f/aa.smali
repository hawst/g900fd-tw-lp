.class public final enum Lcom/google/b/f/aa;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/aa;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/aa;

.field public static final enum b:Lcom/google/b/f/aa;

.field public static final enum c:Lcom/google/b/f/aa;

.field public static final enum d:Lcom/google/b/f/aa;

.field public static final enum e:Lcom/google/b/f/aa;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic g:[Lcom/google/b/f/aa;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 602
    new-instance v0, Lcom/google/b/f/aa;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/aa;->a:Lcom/google/b/f/aa;

    .line 606
    new-instance v0, Lcom/google/b/f/aa;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/aa;->b:Lcom/google/b/f/aa;

    .line 610
    new-instance v0, Lcom/google/b/f/aa;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/aa;->c:Lcom/google/b/f/aa;

    .line 614
    new-instance v0, Lcom/google/b/f/aa;

    const-string v1, "INSERT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/aa;->d:Lcom/google/b/f/aa;

    .line 618
    new-instance v0, Lcom/google/b/f/aa;

    const-string v1, "CLONE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/aa;->e:Lcom/google/b/f/aa;

    .line 597
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/f/aa;

    sget-object v1, Lcom/google/b/f/aa;->a:Lcom/google/b/f/aa;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/aa;->b:Lcom/google/b/f/aa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/aa;->c:Lcom/google/b/f/aa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/aa;->d:Lcom/google/b/f/aa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/aa;->e:Lcom/google/b/f/aa;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/f/aa;->g:[Lcom/google/b/f/aa;

    .line 664
    new-instance v0, Lcom/google/b/f/ab;

    invoke-direct {v0}, Lcom/google/b/f/ab;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674
    iput p3, p0, Lcom/google/b/f/aa;->f:I

    .line 675
    return-void
.end method

.method public static a(I)Lcom/google/b/f/aa;
    .locals 1

    .prologue
    .line 649
    packed-switch p0, :pswitch_data_0

    .line 655
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 650
    :pswitch_0
    sget-object v0, Lcom/google/b/f/aa;->a:Lcom/google/b/f/aa;

    goto :goto_0

    .line 651
    :pswitch_1
    sget-object v0, Lcom/google/b/f/aa;->b:Lcom/google/b/f/aa;

    goto :goto_0

    .line 652
    :pswitch_2
    sget-object v0, Lcom/google/b/f/aa;->c:Lcom/google/b/f/aa;

    goto :goto_0

    .line 653
    :pswitch_3
    sget-object v0, Lcom/google/b/f/aa;->d:Lcom/google/b/f/aa;

    goto :goto_0

    .line 654
    :pswitch_4
    sget-object v0, Lcom/google/b/f/aa;->e:Lcom/google/b/f/aa;

    goto :goto_0

    .line 649
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/aa;
    .locals 1

    .prologue
    .line 597
    const-class v0, Lcom/google/b/f/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/aa;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/aa;
    .locals 1

    .prologue
    .line 597
    sget-object v0, Lcom/google/b/f/aa;->g:[Lcom/google/b/f/aa;

    invoke-virtual {v0}, [Lcom/google/b/f/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/aa;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 645
    iget v0, p0, Lcom/google/b/f/aa;->f:I

    return v0
.end method
