.class public final Lcom/google/b/f/b/a/y;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/w;",
        "Lcom/google/b/f/b/a/y;",
        ">;",
        "Lcom/google/b/f/b/a/z;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field private e:I

.field private f:D

.field private g:D


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 361
    sget-object v0, Lcom/google/b/f/b/a/w;->h:Lcom/google/b/f/b/a/w;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 446
    iput v1, p0, Lcom/google/b/f/b/a/y;->b:I

    .line 482
    iput v1, p0, Lcom/google/b/f/b/a/y;->c:I

    .line 362
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/y;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 417
    invoke-static {}, Lcom/google/b/f/b/a/w;->d()Lcom/google/b/f/b/a/w;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 437
    :goto_0
    return-object p0

    .line 418
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 419
    iget v2, p1, Lcom/google/b/f/b/a/w;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/bc;->a(I)Lcom/google/r/b/a/bc;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/bc;->a:Lcom/google/r/b/a/bc;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 418
    goto :goto_1

    .line 419
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/y;->a:I

    iget v2, v2, Lcom/google/r/b/a/bc;->f:I

    iput v2, p0, Lcom/google/b/f/b/a/y;->b:I

    .line 421
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 422
    iget v2, p1, Lcom/google/b/f/b/a/w;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/gd;->a(I)Lcom/google/r/b/a/gd;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 421
    goto :goto_2

    .line 422
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/y;->a:I

    iget v2, v2, Lcom/google/r/b/a/gd;->e:I

    iput v2, p0, Lcom/google/b/f/b/a/y;->c:I

    .line 424
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 425
    iget v2, p1, Lcom/google/b/f/b/a/w;->d:I

    iget v3, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/y;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/y;->d:I

    .line 427
    :cond_9
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 428
    iget v2, p1, Lcom/google/b/f/b/a/w;->e:I

    iget v3, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/y;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/y;->e:I

    .line 430
    :cond_a
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 431
    iget-wide v2, p1, Lcom/google/b/f/b/a/w;->f:D

    iget v4, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/b/a/y;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/y;->f:D

    .line 433
    :cond_b
    iget v2, p1, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    :goto_6
    if-eqz v0, :cond_c

    .line 434
    iget-wide v0, p1, Lcom/google/b/f/b/a/w;->g:D

    iget v2, p0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/f/b/a/y;->a:I

    iput-wide v0, p0, Lcom/google/b/f/b/a/y;->g:D

    .line 436
    :cond_c
    iget-object v0, p1, Lcom/google/b/f/b/a/w;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 424
    goto :goto_3

    :cond_e
    move v2, v1

    .line 427
    goto :goto_4

    :cond_f
    move v2, v1

    .line 430
    goto :goto_5

    :cond_10
    move v0, v1

    .line 433
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 353
    new-instance v2, Lcom/google/b/f/b/a/w;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/w;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/y;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/y;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/w;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/y;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/w;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/y;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/w;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/y;->e:I

    iput v1, v2, Lcom/google/b/f/b/a/w;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/b/f/b/a/y;->f:D

    iput-wide v4, v2, Lcom/google/b/f/b/a/w;->f:D

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v4, p0, Lcom/google/b/f/b/a/y;->g:D

    iput-wide v4, v2, Lcom/google/b/f/b/a/w;->g:D

    iput v0, v2, Lcom/google/b/f/b/a/w;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 353
    check-cast p1, Lcom/google/b/f/b/a/w;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/y;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 441
    const/4 v0, 0x1

    return v0
.end method
