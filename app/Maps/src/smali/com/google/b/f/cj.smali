.class public final enum Lcom/google/b/f/cj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/cj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/b/f/cj;

.field public static final enum B:Lcom/google/b/f/cj;

.field public static final enum C:Lcom/google/b/f/cj;

.field public static final enum D:Lcom/google/b/f/cj;

.field public static final enum E:Lcom/google/b/f/cj;

.field public static final enum F:Lcom/google/b/f/cj;

.field public static final enum G:Lcom/google/b/f/cj;

.field public static final enum H:Lcom/google/b/f/cj;

.field private static final synthetic J:[Lcom/google/b/f/cj;

.field public static final enum a:Lcom/google/b/f/cj;

.field public static final enum b:Lcom/google/b/f/cj;

.field public static final enum c:Lcom/google/b/f/cj;

.field public static final enum d:Lcom/google/b/f/cj;

.field public static final enum e:Lcom/google/b/f/cj;

.field public static final enum f:Lcom/google/b/f/cj;

.field public static final enum g:Lcom/google/b/f/cj;

.field public static final enum h:Lcom/google/b/f/cj;

.field public static final enum i:Lcom/google/b/f/cj;

.field public static final enum j:Lcom/google/b/f/cj;

.field public static final enum k:Lcom/google/b/f/cj;

.field public static final enum l:Lcom/google/b/f/cj;

.field public static final enum m:Lcom/google/b/f/cj;

.field public static final enum n:Lcom/google/b/f/cj;

.field public static final enum o:Lcom/google/b/f/cj;

.field public static final enum p:Lcom/google/b/f/cj;

.field public static final enum q:Lcom/google/b/f/cj;

.field public static final enum r:Lcom/google/b/f/cj;

.field public static final enum s:Lcom/google/b/f/cj;

.field public static final enum t:Lcom/google/b/f/cj;

.field public static final enum u:Lcom/google/b/f/cj;

.field public static final enum v:Lcom/google/b/f/cj;

.field public static final enum w:Lcom/google/b/f/cj;

.field public static final enum x:Lcom/google/b/f/cj;

.field public static final enum y:Lcom/google/b/f/cj;

.field public static final enum z:Lcom/google/b/f/cj;


# instance fields
.field public final I:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 19
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "UNASSIGNED_USER_ACTION_ID"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    .line 23
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "AUTOMATED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->b:Lcom/google/b/f/cj;

    .line 27
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "USER"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->c:Lcom/google/b/f/cj;

    .line 31
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "GENERIC_CLICK"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->d:Lcom/google/b/f/cj;

    .line 35
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "TAP"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->e:Lcom/google/b/f/cj;

    .line 39
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "KEYBOARD_ENTER"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    .line 43
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "MOUSE_CLICK"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->g:Lcom/google/b/f/cj;

    .line 47
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "LEFT_CLICK"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->h:Lcom/google/b/f/cj;

    .line 51
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "RIGHT_CLICK"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->i:Lcom/google/b/f/cj;

    .line 55
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "HOVER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->j:Lcom/google/b/f/cj;

    .line 59
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "INTO_BOUNDING_BOX"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->k:Lcom/google/b/f/cj;

    .line 63
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "OUT_OF_BOUNDING_BOX"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->l:Lcom/google/b/f/cj;

    .line 67
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "PINCH"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->m:Lcom/google/b/f/cj;

    .line 71
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "PINCH_OPEN"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->n:Lcom/google/b/f/cj;

    .line 75
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "PINCH_CLOSED"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->o:Lcom/google/b/f/cj;

    .line 79
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "INPUT_TEXT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->p:Lcom/google/b/f/cj;

    .line 83
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "INPUT_KEYBOARD"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->q:Lcom/google/b/f/cj;

    .line 87
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "INPUT_VOICE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->r:Lcom/google/b/f/cj;

    .line 91
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "RESIZE_BROWSER"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->s:Lcom/google/b/f/cj;

    .line 95
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "ROTATE_SCREEN"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->t:Lcom/google/b/f/cj;

    .line 99
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "DIRECTIONAL_MOVEMENT"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->u:Lcom/google/b/f/cj;

    .line 103
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "SWIPE"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->v:Lcom/google/b/f/cj;

    .line 107
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "SCROLL_BAR"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->w:Lcom/google/b/f/cj;

    .line 111
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "MOUSE_WHEEL"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->x:Lcom/google/b/f/cj;

    .line 115
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "ARROW_KEYS"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->y:Lcom/google/b/f/cj;

    .line 119
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "NAVIGATE"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->z:Lcom/google/b/f/cj;

    .line 123
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "BACK_BUTTON"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->A:Lcom/google/b/f/cj;

    .line 127
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "UNKNOWN_ACTION"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->B:Lcom/google/b/f/cj;

    .line 131
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "HEAD_MOVEMENT"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->C:Lcom/google/b/f/cj;

    .line 135
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "SHAKE"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->D:Lcom/google/b/f/cj;

    .line 139
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "DRAG"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->E:Lcom/google/b/f/cj;

    .line 143
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "LONG_PRESS"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->F:Lcom/google/b/f/cj;

    .line 147
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "KEY_PRESS"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->G:Lcom/google/b/f/cj;

    .line 151
    new-instance v0, Lcom/google/b/f/cj;

    const-string v1, "ACTION_BY_TIMER"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cj;->H:Lcom/google/b/f/cj;

    .line 14
    const/16 v0, 0x22

    new-array v0, v0, [Lcom/google/b/f/cj;

    sget-object v1, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/cj;->b:Lcom/google/b/f/cj;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/cj;->c:Lcom/google/b/f/cj;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/f/cj;->d:Lcom/google/b/f/cj;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/b/f/cj;->e:Lcom/google/b/f/cj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/f/cj;->g:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/f/cj;->h:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/b/f/cj;->i:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/b/f/cj;->j:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/b/f/cj;->k:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/b/f/cj;->l:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/b/f/cj;->m:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/b/f/cj;->n:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/b/f/cj;->o:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/b/f/cj;->p:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/b/f/cj;->q:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/b/f/cj;->r:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/b/f/cj;->s:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/b/f/cj;->t:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/b/f/cj;->u:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/b/f/cj;->v:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/b/f/cj;->w:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/b/f/cj;->x:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/b/f/cj;->y:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/b/f/cj;->z:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/b/f/cj;->A:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/b/f/cj;->B:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/b/f/cj;->C:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/b/f/cj;->D:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/b/f/cj;->E:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/b/f/cj;->F:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/b/f/cj;->G:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/b/f/cj;->H:Lcom/google/b/f/cj;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/f/cj;->J:[Lcom/google/b/f/cj;

    .line 341
    new-instance v0, Lcom/google/b/f/ck;

    invoke-direct {v0}, Lcom/google/b/f/ck;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 351
    iput p3, p0, Lcom/google/b/f/cj;->I:I

    .line 352
    return-void
.end method

.method public static a(I)Lcom/google/b/f/cj;
    .locals 1

    .prologue
    .line 297
    packed-switch p0, :pswitch_data_0

    .line 332
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 298
    :pswitch_0
    sget-object v0, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    goto :goto_0

    .line 299
    :pswitch_1
    sget-object v0, Lcom/google/b/f/cj;->b:Lcom/google/b/f/cj;

    goto :goto_0

    .line 300
    :pswitch_2
    sget-object v0, Lcom/google/b/f/cj;->c:Lcom/google/b/f/cj;

    goto :goto_0

    .line 301
    :pswitch_3
    sget-object v0, Lcom/google/b/f/cj;->d:Lcom/google/b/f/cj;

    goto :goto_0

    .line 302
    :pswitch_4
    sget-object v0, Lcom/google/b/f/cj;->e:Lcom/google/b/f/cj;

    goto :goto_0

    .line 303
    :pswitch_5
    sget-object v0, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    goto :goto_0

    .line 304
    :pswitch_6
    sget-object v0, Lcom/google/b/f/cj;->g:Lcom/google/b/f/cj;

    goto :goto_0

    .line 305
    :pswitch_7
    sget-object v0, Lcom/google/b/f/cj;->h:Lcom/google/b/f/cj;

    goto :goto_0

    .line 306
    :pswitch_8
    sget-object v0, Lcom/google/b/f/cj;->i:Lcom/google/b/f/cj;

    goto :goto_0

    .line 307
    :pswitch_9
    sget-object v0, Lcom/google/b/f/cj;->j:Lcom/google/b/f/cj;

    goto :goto_0

    .line 308
    :pswitch_a
    sget-object v0, Lcom/google/b/f/cj;->k:Lcom/google/b/f/cj;

    goto :goto_0

    .line 309
    :pswitch_b
    sget-object v0, Lcom/google/b/f/cj;->l:Lcom/google/b/f/cj;

    goto :goto_0

    .line 310
    :pswitch_c
    sget-object v0, Lcom/google/b/f/cj;->m:Lcom/google/b/f/cj;

    goto :goto_0

    .line 311
    :pswitch_d
    sget-object v0, Lcom/google/b/f/cj;->n:Lcom/google/b/f/cj;

    goto :goto_0

    .line 312
    :pswitch_e
    sget-object v0, Lcom/google/b/f/cj;->o:Lcom/google/b/f/cj;

    goto :goto_0

    .line 313
    :pswitch_f
    sget-object v0, Lcom/google/b/f/cj;->p:Lcom/google/b/f/cj;

    goto :goto_0

    .line 314
    :pswitch_10
    sget-object v0, Lcom/google/b/f/cj;->q:Lcom/google/b/f/cj;

    goto :goto_0

    .line 315
    :pswitch_11
    sget-object v0, Lcom/google/b/f/cj;->r:Lcom/google/b/f/cj;

    goto :goto_0

    .line 316
    :pswitch_12
    sget-object v0, Lcom/google/b/f/cj;->s:Lcom/google/b/f/cj;

    goto :goto_0

    .line 317
    :pswitch_13
    sget-object v0, Lcom/google/b/f/cj;->t:Lcom/google/b/f/cj;

    goto :goto_0

    .line 318
    :pswitch_14
    sget-object v0, Lcom/google/b/f/cj;->u:Lcom/google/b/f/cj;

    goto :goto_0

    .line 319
    :pswitch_15
    sget-object v0, Lcom/google/b/f/cj;->v:Lcom/google/b/f/cj;

    goto :goto_0

    .line 320
    :pswitch_16
    sget-object v0, Lcom/google/b/f/cj;->w:Lcom/google/b/f/cj;

    goto :goto_0

    .line 321
    :pswitch_17
    sget-object v0, Lcom/google/b/f/cj;->x:Lcom/google/b/f/cj;

    goto :goto_0

    .line 322
    :pswitch_18
    sget-object v0, Lcom/google/b/f/cj;->y:Lcom/google/b/f/cj;

    goto :goto_0

    .line 323
    :pswitch_19
    sget-object v0, Lcom/google/b/f/cj;->z:Lcom/google/b/f/cj;

    goto :goto_0

    .line 324
    :pswitch_1a
    sget-object v0, Lcom/google/b/f/cj;->A:Lcom/google/b/f/cj;

    goto :goto_0

    .line 325
    :pswitch_1b
    sget-object v0, Lcom/google/b/f/cj;->B:Lcom/google/b/f/cj;

    goto :goto_0

    .line 326
    :pswitch_1c
    sget-object v0, Lcom/google/b/f/cj;->C:Lcom/google/b/f/cj;

    goto :goto_0

    .line 327
    :pswitch_1d
    sget-object v0, Lcom/google/b/f/cj;->D:Lcom/google/b/f/cj;

    goto :goto_0

    .line 328
    :pswitch_1e
    sget-object v0, Lcom/google/b/f/cj;->E:Lcom/google/b/f/cj;

    goto :goto_0

    .line 329
    :pswitch_1f
    sget-object v0, Lcom/google/b/f/cj;->F:Lcom/google/b/f/cj;

    goto :goto_0

    .line 330
    :pswitch_20
    sget-object v0, Lcom/google/b/f/cj;->G:Lcom/google/b/f/cj;

    goto :goto_0

    .line 331
    :pswitch_21
    sget-object v0, Lcom/google/b/f/cj;->H:Lcom/google/b/f/cj;

    goto :goto_0

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/cj;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/b/f/cj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cj;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/cj;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/b/f/cj;->J:[Lcom/google/b/f/cj;

    invoke-virtual {v0}, [Lcom/google/b/f/cj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/cj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/google/b/f/cj;->I:I

    return v0
.end method
