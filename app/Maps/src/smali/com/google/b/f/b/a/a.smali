.class public final Lcom/google/b/f/b/a/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/b/f/b/a/a;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/b/f/b/a/b;

    invoke-direct {v0}, Lcom/google/b/f/b/a/b;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/a;->PARSER:Lcom/google/n/ax;

    .line 225
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/a;->g:Lcom/google/n/aw;

    .line 587
    new-instance v0, Lcom/google/b/f/b/a/a;

    invoke-direct {v0}, Lcom/google/b/f/b/a/a;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/a;->d:Lcom/google/b/f/b/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 176
    iput-byte v0, p0, Lcom/google/b/f/b/a/a;->e:B

    .line 204
    iput v0, p0, Lcom/google/b/f/b/a/a;->f:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x2

    .line 26
    invoke-direct {p0}, Lcom/google/b/f/b/a/a;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 34
    sparse-switch v4, :sswitch_data_0

    .line 39
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 41
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 47
    iget v5, p0, Lcom/google/b/f/b/a/a;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/b/f/b/a/a;->a:I

    .line 48
    iput-object v4, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 63
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 70
    iget-object v1, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 72
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/a;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_2

    .line 53
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 55
    or-int/lit8 v0, v0, 0x2

    .line 57
    :cond_2
    iget-object v4, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 57
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 65
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 66
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 69
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_4

    .line 70
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 72
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->au:Lcom/google/n/bn;

    .line 73
    return-void

    .line 69
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 176
    iput-byte v0, p0, Lcom/google/b/f/b/a/a;->e:B

    .line 204
    iput v0, p0, Lcom/google/b/f/b/a/a;->f:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;
    .locals 1

    .prologue
    .line 290
    invoke-static {}, Lcom/google/b/f/b/a/a;->newBuilder()Lcom/google/b/f/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b/a/a;
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/google/b/f/b/a/a;->d:Lcom/google/b/f/b/a/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/c;
    .locals 1

    .prologue
    .line 287
    new-instance v0, Lcom/google/b/f/b/a/c;

    invoke-direct {v0}, Lcom/google/b/f/b/a/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/google/b/f/b/a/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 194
    invoke-virtual {p0}, Lcom/google/b/f/b/a/a;->c()I

    .line 195
    iget v0, p0, Lcom/google/b/f/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 198
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 196
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 202
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178
    iget-byte v0, p0, Lcom/google/b/f/b/a/a;->e:B

    .line 179
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 189
    :cond_0
    :goto_0
    return v2

    .line 180
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 182
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    iput-byte v2, p0, Lcom/google/b/f/b/a/a;->e:B

    goto :goto_0

    .line 182
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 188
    :cond_3
    iput-byte v3, p0, Lcom/google/b/f/b/a/a;->e:B

    move v2, v3

    .line 189
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 206
    iget v0, p0, Lcom/google/b/f/b/a/a;->f:I

    .line 207
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 220
    :goto_0
    return v0

    .line 210
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 212
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 214
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 215
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 216
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 214
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 212
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/b/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 219
    iput v0, p0, Lcom/google/b/f/b/a/a;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/a;->newBuilder()Lcom/google/b/f/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/a;->newBuilder()Lcom/google/b/f/b/a/c;

    move-result-object v0

    return-object v0
.end method
