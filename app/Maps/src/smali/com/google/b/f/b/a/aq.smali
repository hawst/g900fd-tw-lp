.class public final Lcom/google/b/f/b/a/aq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/ao;",
        "Lcom/google/b/f/b/a/aq;",
        ">;",
        "Lcom/google/b/f/b/a/ar;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:F


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/b/f/b/a/ao;->e:Lcom/google/b/f/b/a/ao;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 264
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/ao;)Lcom/google/b/f/b/a/aq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    invoke-static {}, Lcom/google/b/f/b/a/ao;->d()Lcom/google/b/f/b/a/ao;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 312
    :goto_0
    return-object p0

    .line 302
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 303
    iget v2, p1, Lcom/google/b/f/b/a/ao;->b:I

    iget v3, p0, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/aq;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/aq;->b:I

    .line 305
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 306
    iget v2, p1, Lcom/google/b/f/b/a/ao;->c:F

    iget v3, p0, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/aq;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/aq;->c:F

    .line 308
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 309
    iget v0, p1, Lcom/google/b/f/b/a/ao;->d:F

    iget v1, p0, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/aq;->d:F

    .line 311
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/b/a/ao;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 302
    goto :goto_1

    :cond_5
    move v2, v1

    .line 305
    goto :goto_2

    :cond_6
    move v0, v1

    .line 308
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 255
    new-instance v2, Lcom/google/b/f/b/a/ao;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/ao;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/aq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/aq;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/ao;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/aq;->c:F

    iput v1, v2, Lcom/google/b/f/b/a/ao;->c:F

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/aq;->d:F

    iput v1, v2, Lcom/google/b/f/b/a/ao;->d:F

    iput v0, v2, Lcom/google/b/f/b/a/ao;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 255
    check-cast p1, Lcom/google/b/f/b/a/ao;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/aq;->a(Lcom/google/b/f/b/a/ao;)Lcom/google/b/f/b/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method
