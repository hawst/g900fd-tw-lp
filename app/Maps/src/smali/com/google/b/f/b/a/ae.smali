.class public final Lcom/google/b/f/b/a/ae;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/aj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ae;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/b/a/ae;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/b/f/b/a/af;

    invoke-direct {v0}, Lcom/google/b/f/b/a/af;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ae;->PARSER:Lcom/google/n/ax;

    .line 311
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/ae;->h:Lcom/google/n/aw;

    .line 553
    new-instance v0, Lcom/google/b/f/b/a/ae;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ae;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ae;->e:Lcom/google/b/f/b/a/ae;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 261
    iput-byte v1, p0, Lcom/google/b/f/b/a/ae;->f:B

    .line 286
    iput v1, p0, Lcom/google/b/f/b/a/ae;->g:I

    .line 18
    iput v0, p0, Lcom/google/b/f/b/a/ae;->b:I

    .line 19
    iput v0, p0, Lcom/google/b/f/b/a/ae;->c:I

    .line 20
    iput v0, p0, Lcom/google/b/f/b/a/ae;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/b/f/b/a/ae;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 48
    invoke-static {v3}, Lcom/google/b/f/b/a/ah;->a(I)Lcom/google/b/f/b/a/ah;

    move-result-object v4

    .line 49
    if-nez v4, :cond_1

    .line 50
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/ae;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/b/f/b/a/ae;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/ae;->a:I

    .line 53
    iput v3, p0, Lcom/google/b/f/b/a/ae;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_2
    :try_start_4
    iget v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ae;->c:I

    goto :goto_0

    .line 63
    :sswitch_3
    iget v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ae;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/ae;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 261
    iput-byte v0, p0, Lcom/google/b/f/b/a/ae;->f:B

    .line 286
    iput v0, p0, Lcom/google/b/f/b/a/ae;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/ae;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/google/b/f/b/a/ae;->e:Lcom/google/b/f/b/a/ae;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/ag;
    .locals 1

    .prologue
    .line 373
    new-instance v0, Lcom/google/b/f/b/a/ag;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ag;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/b/f/b/a/ae;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273
    invoke-virtual {p0}, Lcom/google/b/f/b/a/ae;->c()I

    .line 274
    iget v0, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 275
    iget v0, p0, Lcom/google/b/f/b/a/ae;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 277
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 278
    iget v0, p0, Lcom/google/b/f/b/a/ae;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 280
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 281
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/ae;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 283
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/b/a/ae;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 284
    return-void

    .line 275
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 278
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 281
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 263
    iget-byte v1, p0, Lcom/google/b/f/b/a/ae;->f:B

    .line 264
    if-ne v1, v0, :cond_0

    .line 268
    :goto_0
    return v0

    .line 265
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 267
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/ae;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 288
    iget v0, p0, Lcom/google/b/f/b/a/ae;->g:I

    .line 289
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 306
    :goto_0
    return v0

    .line 292
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 293
    iget v0, p0, Lcom/google/b/f/b/a/ae;->b:I

    .line 294
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 296
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 297
    iget v3, p0, Lcom/google/b/f/b/a/ae;->c:I

    .line 298
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 300
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 301
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/ae;->d:I

    .line 302
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 304
    :cond_3
    iget-object v1, p0, Lcom/google/b/f/b/a/ae;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    iput v0, p0, Lcom/google/b/f/b/a/ae;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 294
    goto :goto_1

    :cond_5
    move v3, v1

    .line 298
    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ae;->newBuilder()Lcom/google/b/f/b/a/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/ag;->a(Lcom/google/b/f/b/a/ae;)Lcom/google/b/f/b/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ae;->newBuilder()Lcom/google/b/f/b/a/ag;

    move-result-object v0

    return-object v0
.end method
