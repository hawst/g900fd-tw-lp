.class public final Lcom/google/b/f/b/a/di;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/dn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/dg;",
        "Lcom/google/b/f/b/a/di;",
        ">;",
        "Lcom/google/b/f/b/a/dn;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1087
    sget-object v0, Lcom/google/b/f/b/a/dg;->j:Lcom/google/b/f/b/a/dg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1227
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/di;->b:I

    .line 1263
    iput v1, p0, Lcom/google/b/f/b/a/di;->c:I

    .line 1299
    iput v1, p0, Lcom/google/b/f/b/a/di;->d:I

    .line 1335
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->e:Ljava/lang/Object;

    .line 1411
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->f:Lcom/google/n/ao;

    .line 1470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    .line 1530
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    .line 1666
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    .line 1088
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/dg;)Lcom/google/b/f/b/a/di;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1162
    invoke-static {}, Lcom/google/b/f/b/a/dg;->d()Lcom/google/b/f/b/a/dg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1200
    :goto_0
    return-object p0

    .line 1163
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1164
    iget v2, p1, Lcom/google/b/f/b/a/dg;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/a;->a(I)Lcom/google/r/b/a/a;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1163
    goto :goto_1

    .line 1164
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/di;->a:I

    iget v2, v2, Lcom/google/r/b/a/a;->t:I

    iput v2, p0, Lcom/google/b/f/b/a/di;->b:I

    .line 1166
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 1167
    iget v2, p1, Lcom/google/b/f/b/a/dg;->c:I

    invoke-static {v2}, Lcom/google/b/f/cj;->a(I)Lcom/google/b/f/cj;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1166
    goto :goto_2

    .line 1167
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/di;->a:I

    iget v2, v2, Lcom/google/b/f/cj;->I:I

    iput v2, p0, Lcom/google/b/f/b/a/di;->c:I

    .line 1169
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    .line 1170
    iget v2, p1, Lcom/google/b/f/b/a/dg;->d:I

    invoke-static {v2}, Lcom/google/b/f/ch;->a(I)Lcom/google/b/f/ch;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    :cond_9
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 1169
    goto :goto_3

    .line 1170
    :cond_b
    iget v3, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/di;->a:I

    iget v2, v2, Lcom/google/b/f/ch;->f:I

    iput v2, p0, Lcom/google/b/f/b/a/di;->d:I

    .line 1172
    :cond_c
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_d

    .line 1173
    iget v2, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1174
    iget-object v2, p1, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/b/f/b/a/di;->e:Ljava/lang/Object;

    .line 1177
    :cond_d
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 1178
    iget-object v2, p0, Lcom/google/b/f/b/a/di;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1179
    iget v2, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1181
    :cond_e
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 1182
    iget-object v2, p0, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1183
    iget v2, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1185
    :cond_f
    iget-object v2, p1, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 1186
    iget-object v2, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1187
    iget-object v2, p1, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    .line 1188
    iget v2, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1195
    :cond_10
    :goto_7
    iget v2, p1, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    :goto_8
    if-eqz v0, :cond_11

    .line 1196
    iget-object v0, p0, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1197
    iget v0, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1199
    :cond_11
    iget-object v0, p1, Lcom/google/b/f/b/a/dg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v2, v1

    .line 1172
    goto :goto_4

    :cond_13
    move v2, v1

    .line 1177
    goto :goto_5

    :cond_14
    move v2, v1

    .line 1181
    goto :goto_6

    .line 1190
    :cond_15
    invoke-virtual {p0}, Lcom/google/b/f/b/a/di;->c()V

    .line 1191
    iget-object v2, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    :cond_16
    move v0, v1

    .line 1195
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1079
    new-instance v2, Lcom/google/b/f/b/a/dg;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/dg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/b/f/b/a/di;->b:I

    iput v4, v2, Lcom/google/b/f/b/a/dg;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/b/f/b/a/di;->c:I

    iput v4, v2, Lcom/google/b/f/b/a/dg;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/b/f/b/a/di;->d:I

    iput v4, v2, Lcom/google/b/f/b/a/dg;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/b/f/b/a/di;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/di;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/di;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/b/f/b/a/di;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v3, v2, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/b/f/b/a/dg;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1079
    check-cast p1, Lcom/google/b/f/b/a/dg;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/di;->a(Lcom/google/b/f/b/a/dg;)Lcom/google/b/f/b/a/di;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1204
    iget v0, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1205
    iget-object v0, p0, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1222
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1204
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1210
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1211
    iget-object v0, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1210
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1216
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1217
    iget-object v0, p0, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dj;->d()Lcom/google/b/f/b/a/dj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dj;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 1222
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1216
    goto :goto_3
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1532
    iget v0, p0, Lcom/google/b/f/b/a/di;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 1533
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    .line 1536
    iget v0, p0, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/di;->a:I

    .line 1538
    :cond_0
    return-void
.end method
