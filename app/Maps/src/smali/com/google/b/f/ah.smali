.class public final Lcom/google/b/f/ah;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/al;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/ah;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/ah;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1220
    new-instance v0, Lcom/google/b/f/ai;

    invoke-direct {v0}, Lcom/google/b/f/ai;-><init>()V

    sput-object v0, Lcom/google/b/f/ah;->PARSER:Lcom/google/n/ax;

    .line 1383
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/ah;->h:Lcom/google/n/aw;

    .line 1777
    new-instance v0, Lcom/google/b/f/ah;

    invoke-direct {v0}, Lcom/google/b/f/ah;-><init>()V

    sput-object v0, Lcom/google/b/f/ah;->e:Lcom/google/b/f/ah;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1159
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1236
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/ah;->b:I

    .line 1304
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    .line 1319
    iput-byte v2, p0, Lcom/google/b/f/ah;->f:B

    .line 1358
    iput v2, p0, Lcom/google/b/f/ah;->g:I

    .line 1160
    iget-object v0, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1161
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1167
    invoke-direct {p0}, Lcom/google/b/f/ah;-><init>()V

    .line 1168
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1173
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 1174
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1175
    sparse-switch v0, :sswitch_data_0

    .line 1180
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 1182
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 1178
    goto :goto_0

    .line 1187
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-eq v0, v2, :cond_1

    .line 1188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    .line 1190
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1191
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 1190
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1192
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/b/f/ah;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1211
    :catch_0
    move-exception v0

    .line 1212
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1217
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/ah;->au:Lcom/google/n/bn;

    throw v0

    .line 1196
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1197
    iget v0, p0, Lcom/google/b/f/ah;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/ah;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1213
    :catch_1
    move-exception v0

    .line 1214
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1215
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1201
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-eq v0, v5, :cond_2

    .line 1202
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    .line 1204
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1205
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 1204
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1206
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/b/f/ah;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1217
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/ah;->au:Lcom/google/n/bn;

    .line 1218
    return-void

    .line 1175
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1157
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1236
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/ah;->b:I

    .line 1304
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    .line 1319
    iput-byte v1, p0, Lcom/google/b/f/ah;->f:B

    .line 1358
    iput v1, p0, Lcom/google/b/f/ah;->g:I

    .line 1158
    return-void
.end method

.method public static d()Lcom/google/b/f/ah;
    .locals 1

    .prologue
    .line 1780
    sget-object v0, Lcom/google/b/f/ah;->e:Lcom/google/b/f/ah;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/aj;
    .locals 1

    .prologue
    .line 1445
    new-instance v0, Lcom/google/b/f/aj;

    invoke-direct {v0}, Lcom/google/b/f/aj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232
    sget-object v0, Lcom/google/b/f/ah;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 1343
    invoke-virtual {p0}, Lcom/google/b/f/ah;->c()I

    .line 1344
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v1, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1346
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 1345
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1348
    :cond_0
    iget v0, p0, Lcom/google/b/f/ah;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1349
    iget-object v0, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1351
    :cond_1
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v3, :cond_2

    .line 1352
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1353
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 1352
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1355
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/ah;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1356
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1321
    iget-byte v0, p0, Lcom/google/b/f/ah;->f:B

    .line 1322
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1338
    :goto_0
    return v0

    .line 1323
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1325
    :cond_1
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1326
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    :goto_2
    invoke-virtual {v0}, Lcom/google/b/f/p;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1327
    iput-byte v2, p0, Lcom/google/b/f/ah;->f:B

    move v0, v2

    .line 1328
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1325
    goto :goto_1

    .line 1326
    :cond_3
    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v0

    goto :goto_2

    .line 1331
    :cond_4
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 1332
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v4, :cond_6

    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/l;->d()Lcom/google/b/f/l;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/l;

    :goto_4
    invoke-virtual {v0}, Lcom/google/b/f/l;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1333
    iput-byte v2, p0, Lcom/google/b/f/ah;->f:B

    move v0, v2

    .line 1334
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1331
    goto :goto_3

    .line 1332
    :cond_6
    invoke-static {}, Lcom/google/b/f/l;->d()Lcom/google/b/f/l;

    move-result-object v0

    goto :goto_4

    .line 1337
    :cond_7
    iput-byte v1, p0, Lcom/google/b/f/ah;->f:B

    move v0, v1

    .line 1338
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1360
    iget v0, p0, Lcom/google/b/f/ah;->g:I

    .line 1361
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1378
    :goto_0
    return v0

    .line 1364
    :cond_0
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v3, :cond_3

    .line 1365
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1366
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1368
    :goto_1
    iget v2, p0, Lcom/google/b/f/ah;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1369
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    .line 1370
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1372
    :goto_2
    iget v0, p0, Lcom/google/b/f/ah;->b:I

    if-ne v0, v4, :cond_1

    .line 1373
    iget-object v0, p0, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1374
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1376
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/ah;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1377
    iput v0, p0, Lcom/google/b/f/ah;->g:I

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1151
    invoke-static {}, Lcom/google/b/f/ah;->newBuilder()Lcom/google/b/f/aj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/aj;->a(Lcom/google/b/f/ah;)Lcom/google/b/f/aj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1151
    invoke-static {}, Lcom/google/b/f/ah;->newBuilder()Lcom/google/b/f/aj;

    move-result-object v0

    return-object v0
.end method
