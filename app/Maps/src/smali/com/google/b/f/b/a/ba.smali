.class public final Lcom/google/b/f/b/a/ba;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ba;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/b/f/b/a/ba;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:J

.field d:I

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:Lcom/google/n/ao;

.field n:J

.field o:I

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/b/f/b/a/bb;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bb;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ba;->PARSER:Lcom/google/n/ax;

    .line 518
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/ba;->s:Lcom/google/n/aw;

    .line 1495
    new-instance v0, Lcom/google/b/f/b/a/ba;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ba;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ba;->p:Lcom/google/b/f/b/a/ba;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    .line 216
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    .line 232
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    .line 264
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    .line 280
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    .line 296
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    .line 312
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    .line 328
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    .line 373
    iput-byte v3, p0, Lcom/google/b/f/b/a/ba;->q:B

    .line 449
    iput v3, p0, Lcom/google/b/f/b/a/ba;->r:I

    .line 18
    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->b:J

    .line 19
    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->c:J

    .line 20
    iput v6, p0, Lcom/google/b/f/b/a/ba;->d:I

    .line 21
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->n:J

    .line 31
    iput v6, p0, Lcom/google/b/f/b/a/ba;->o:I

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/b/f/b/a/ba;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 44
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 45
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 46
    sparse-switch v3, :sswitch_data_0

    .line 51
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 53
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 49
    goto :goto_0

    .line 58
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/ba;->au:Lcom/google/n/bn;

    throw v0

    .line 63
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 133
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ba;->d:I

    goto :goto_0

    .line 73
    :sswitch_4
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 74
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto :goto_0

    .line 78
    :sswitch_5
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 79
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto :goto_0

    .line 83
    :sswitch_6
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 84
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 88
    :sswitch_7
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 89
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_8
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 98
    :sswitch_9
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 99
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 103
    :sswitch_a
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 104
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_b
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 109
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_c
    iget-object v3, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 114
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    goto/16 :goto_0

    .line 118
    :sswitch_d
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/ba;->n:J

    goto/16 :goto_0

    .line 123
    :sswitch_e
    iget v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/b/f/b/a/ba;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ba;->o:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 135
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->au:Lcom/google/n/bn;

    .line 136
    return-void

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x82 -> :sswitch_5
        0x8a -> :sswitch_6
        0x92 -> :sswitch_7
        0x9a -> :sswitch_8
        0xa2 -> :sswitch_9
        0xaa -> :sswitch_a
        0xb2 -> :sswitch_b
        0xba -> :sswitch_c
        0xc0 -> :sswitch_d
        0xc8 -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    .line 216
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    .line 232
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    .line 264
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    .line 280
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    .line 296
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    .line 312
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    .line 328
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    .line 373
    iput-byte v1, p0, Lcom/google/b/f/b/a/ba;->q:B

    .line 449
    iput v1, p0, Lcom/google/b/f/b/a/ba;->r:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;
    .locals 1

    .prologue
    .line 583
    invoke-static {}, Lcom/google/b/f/b/a/ba;->newBuilder()Lcom/google/b/f/b/a/bc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b/a/ba;
    .locals 1

    .prologue
    .line 1498
    sget-object v0, Lcom/google/b/f/b/a/ba;->p:Lcom/google/b/f/b/a/ba;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/bc;
    .locals 1

    .prologue
    .line 580
    new-instance v0, Lcom/google/b/f/b/a/bc;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/google/b/f/b/a/ba;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 403
    invoke-virtual {p0}, Lcom/google/b/f/b/a/ba;->c()I

    .line 404
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 405
    iget-wide v0, p0, Lcom/google/b/f/b/a/ba;->b:J

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 407
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 408
    iget-wide v0, p0, Lcom/google/b/f/b/a/ba;->c:J

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 410
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 411
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/ba;->d:I

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 413
    :cond_2
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 414
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 416
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_4

    .line 417
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 419
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 420
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 422
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 423
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 425
    :cond_6
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 426
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 428
    :cond_7
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 429
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 431
    :cond_8
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 432
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 434
    :cond_9
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 435
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 437
    :cond_a
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 438
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 440
    :cond_b
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 441
    const/16 v0, 0x18

    iget-wide v2, p0, Lcom/google/b/f/b/a/ba;->n:J

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 443
    :cond_c
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 444
    const/16 v0, 0x19

    iget v1, p0, Lcom/google/b/f/b/a/ba;->o:I

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 446
    :cond_d
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 447
    return-void

    .line 411
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 444
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 375
    iget-byte v0, p0, Lcom/google/b/f/b/a/ba;->q:B

    .line 376
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 398
    :goto_0
    return v0

    .line 377
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 379
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 380
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dg;->d()Lcom/google/b/f/b/a/dg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dg;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dg;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 381
    iput-byte v2, p0, Lcom/google/b/f/b/a/ba;->q:B

    move v0, v2

    .line 382
    goto :goto_0

    :cond_2
    move v0, v2

    .line 379
    goto :goto_1

    .line 385
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 386
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dr;->d()Lcom/google/b/f/b/a/dr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dr;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dr;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 387
    iput-byte v2, p0, Lcom/google/b/f/b/a/ba;->q:B

    move v0, v2

    .line 388
    goto :goto_0

    :cond_4
    move v0, v2

    .line 385
    goto :goto_2

    .line 391
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 392
    iget-object v0, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/cg;->d()Lcom/google/b/f/b/a/cg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cg;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/cg;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 393
    iput-byte v2, p0, Lcom/google/b/f/b/a/ba;->q:B

    move v0, v2

    .line 394
    goto :goto_0

    :cond_6
    move v0, v2

    .line 391
    goto :goto_3

    .line 397
    :cond_7
    iput-byte v1, p0, Lcom/google/b/f/b/a/ba;->q:B

    move v0, v1

    .line 398
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v3, 0xa

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 451
    iget v0, p0, Lcom/google/b/f/b/a/ba;->r:I

    .line 452
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 513
    :goto_0
    return v0

    .line 455
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_10

    .line 456
    iget-wide v4, p0, Lcom/google/b/f/b/a/ba;->b:J

    .line 457
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 459
    :goto_1
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v7, :cond_1

    .line 460
    iget-wide v4, p0, Lcom/google/b/f/b/a/ba;->c:J

    .line 461
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 463
    :cond_1
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v8, :cond_2

    .line 464
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/ba;->d:I

    .line 465
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 467
    :cond_2
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 468
    iget-object v2, p0, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    .line 469
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 471
    :cond_3
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    .line 472
    const/16 v2, 0x10

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    .line 473
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 475
    :cond_4
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_5

    .line 476
    const/16 v2, 0x11

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    .line 477
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 479
    :cond_5
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_6

    .line 480
    const/16 v2, 0x12

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    .line 481
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 483
    :cond_6
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_7

    .line 484
    const/16 v2, 0x13

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    .line 485
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 487
    :cond_7
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_8

    .line 488
    const/16 v2, 0x14

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    .line 489
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 491
    :cond_8
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_9

    .line 492
    const/16 v2, 0x15

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    .line 493
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 495
    :cond_9
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_a

    .line 496
    const/16 v2, 0x16

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    .line 497
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 499
    :cond_a
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_b

    .line 500
    const/16 v2, 0x17

    iget-object v4, p0, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    .line 501
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 503
    :cond_b
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v4, 0x1000

    if-ne v2, v4, :cond_c

    .line 504
    const/16 v2, 0x18

    iget-wide v4, p0, Lcom/google/b/f/b/a/ba;->n:J

    .line 505
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 507
    :cond_c
    iget v2, p0, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v4, 0x2000

    if-ne v2, v4, :cond_e

    .line 508
    const/16 v2, 0x19

    iget v4, p0, Lcom/google/b/f/b/a/ba;->o:I

    .line 509
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_d
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 511
    :cond_e
    iget-object v1, p0, Lcom/google/b/f/b/a/ba;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 512
    iput v0, p0, Lcom/google/b/f/b/a/ba;->r:I

    goto/16 :goto_0

    :cond_f
    move v2, v3

    .line 465
    goto/16 :goto_2

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ba;->newBuilder()Lcom/google/b/f/b/a/bc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ba;->newBuilder()Lcom/google/b/f/b/a/bc;

    move-result-object v0

    return-object v0
.end method
