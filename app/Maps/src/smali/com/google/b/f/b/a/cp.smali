.class public final enum Lcom/google/b/f/b/a/cp;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/cp;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/cp;

.field public static final enum b:Lcom/google/b/f/b/a/cp;

.field public static final enum c:Lcom/google/b/f/b/a/cp;

.field public static final enum d:Lcom/google/b/f/b/a/cp;

.field private static final synthetic f:[Lcom/google/b/f/b/a/cp;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 198
    new-instance v0, Lcom/google/b/f/b/a/cp;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/b/a/cp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    .line 202
    new-instance v0, Lcom/google/b/f/b/a/cp;

    const-string v1, "HEADER_ONLY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/b/a/cp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cp;->b:Lcom/google/b/f/b/a/cp;

    .line 206
    new-instance v0, Lcom/google/b/f/b/a/cp;

    const-string v1, "TWO_THIRDS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/b/a/cp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cp;->c:Lcom/google/b/f/b/a/cp;

    .line 210
    new-instance v0, Lcom/google/b/f/b/a/cp;

    const-string v1, "FULL_SCREEN"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/b/a/cp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cp;->d:Lcom/google/b/f/b/a/cp;

    .line 193
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/b/f/b/a/cp;

    sget-object v1, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/b/a/cp;->b:Lcom/google/b/f/b/a/cp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/b/a/cp;->c:Lcom/google/b/f/b/a/cp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/b/a/cp;->d:Lcom/google/b/f/b/a/cp;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/b/f/b/a/cp;->f:[Lcom/google/b/f/b/a/cp;

    .line 250
    new-instance v0, Lcom/google/b/f/b/a/cq;

    invoke-direct {v0}, Lcom/google/b/f/b/a/cq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 260
    iput p3, p0, Lcom/google/b/f/b/a/cp;->e:I

    .line 261
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/cp;
    .locals 1

    .prologue
    .line 236
    packed-switch p0, :pswitch_data_0

    .line 241
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 237
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 238
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/cp;->b:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 239
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/cp;->c:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 240
    :pswitch_3
    sget-object v0, Lcom/google/b/f/b/a/cp;->d:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/cp;
    .locals 1

    .prologue
    .line 193
    const-class v0, Lcom/google/b/f/b/a/cp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cp;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/cp;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/google/b/f/b/a/cp;->f:[Lcom/google/b/f/b/a/cp;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/cp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/cp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/google/b/f/b/a/cp;->e:I

    return v0
.end method
