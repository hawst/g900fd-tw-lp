.class public final Lcom/google/b/f/b/a/by;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/cb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/by;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/b/f/b/a/by;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/b/f/b/a/bz;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bz;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/by;->PARSER:Lcom/google/n/ax;

    .line 393
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/by;->o:Lcom/google/n/aw;

    .line 926
    new-instance v0, Lcom/google/b/f/b/a/by;

    invoke-direct {v0}, Lcom/google/b/f/b/a/by;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/by;->l:Lcom/google/b/f/b/a/by;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 294
    iput-byte v1, p0, Lcom/google/b/f/b/a/by;->m:B

    .line 340
    iput v1, p0, Lcom/google/b/f/b/a/by;->n:I

    .line 18
    iput v0, p0, Lcom/google/b/f/b/a/by;->b:I

    .line 19
    iput v0, p0, Lcom/google/b/f/b/a/by;->c:I

    .line 20
    iput v0, p0, Lcom/google/b/f/b/a/by;->d:I

    .line 21
    iput v0, p0, Lcom/google/b/f/b/a/by;->e:I

    .line 22
    iput v0, p0, Lcom/google/b/f/b/a/by;->f:I

    .line 23
    iput v0, p0, Lcom/google/b/f/b/a/by;->g:I

    .line 24
    iput v0, p0, Lcom/google/b/f/b/a/by;->h:I

    .line 25
    iput v0, p0, Lcom/google/b/f/b/a/by;->i:I

    .line 26
    iput v0, p0, Lcom/google/b/f/b/a/by;->j:I

    .line 27
    iput v0, p0, Lcom/google/b/f/b/a/by;->k:I

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Lcom/google/b/f/b/a/by;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 39
    const/4 v0, 0x0

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 42
    sparse-switch v3, :sswitch_data_0

    .line 47
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 55
    invoke-static {v3}, Lcom/google/r/b/a/pi;->a(I)Lcom/google/r/b/a/pi;

    move-result-object v4

    .line 56
    if-nez v4, :cond_1

    .line 57
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/by;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 60
    iput v3, p0, Lcom/google/b/f/b/a/by;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    .line 120
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 121
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 66
    invoke-static {v3}, Lcom/google/r/b/a/dh;->a(I)Lcom/google/r/b/a/dh;

    move-result-object v4

    .line 67
    if-nez v4, :cond_2

    .line 68
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 70
    :cond_2
    iget v4, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 71
    iput v3, p0, Lcom/google/b/f/b/a/by;->c:I

    goto :goto_0

    .line 76
    :sswitch_3
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->d:I

    goto :goto_0

    .line 81
    :sswitch_4
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->e:I

    goto :goto_0

    .line 86
    :sswitch_5
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->f:I

    goto/16 :goto_0

    .line 91
    :sswitch_6
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->g:I

    goto/16 :goto_0

    .line 96
    :sswitch_7
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->h:I

    goto/16 :goto_0

    .line 101
    :sswitch_8
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->i:I

    goto/16 :goto_0

    .line 106
    :sswitch_9
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->j:I

    goto/16 :goto_0

    .line 111
    :sswitch_a
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/b/f/b/a/by;->a:I

    .line 112
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/by;->k:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 123
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/by;->au:Lcom/google/n/bn;

    .line 124
    return-void

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 294
    iput-byte v0, p0, Lcom/google/b/f/b/a/by;->m:B

    .line 340
    iput v0, p0, Lcom/google/b/f/b/a/by;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/by;
    .locals 1

    .prologue
    .line 929
    sget-object v0, Lcom/google/b/f/b/a/by;->l:Lcom/google/b/f/b/a/by;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/ca;
    .locals 1

    .prologue
    .line 455
    new-instance v0, Lcom/google/b/f/b/a/ca;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ca;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/by;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v0, Lcom/google/b/f/b/a/by;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 306
    invoke-virtual {p0}, Lcom/google/b/f/b/a/by;->c()I

    .line 307
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 308
    iget v0, p0, Lcom/google/b/f/b/a/by;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 310
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 311
    iget v0, p0, Lcom/google/b/f/b/a/by;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 313
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 314
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/by;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_c

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 316
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 317
    iget v0, p0, Lcom/google/b/f/b/a/by;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_d

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 319
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 320
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/b/f/b/a/by;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 322
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 323
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/b/f/b/a/by;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 325
    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 326
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/b/f/b/a/by;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 328
    :cond_6
    :goto_6
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 329
    iget v0, p0, Lcom/google/b/f/b/a/by;->i:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_11

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 331
    :cond_7
    :goto_7
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 332
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/b/f/b/a/by;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_12

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 334
    :cond_8
    :goto_8
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 335
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/b/f/b/a/by;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_13

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 337
    :cond_9
    :goto_9
    iget-object v0, p0, Lcom/google/b/f/b/a/by;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 338
    return-void

    .line 308
    :cond_a
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 311
    :cond_b
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 314
    :cond_c
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 317
    :cond_d
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 320
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 323
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 326
    :cond_10
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 329
    :cond_11
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 332
    :cond_12
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    .line 335
    :cond_13
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 296
    iget-byte v1, p0, Lcom/google/b/f/b/a/by;->m:B

    .line 297
    if-ne v1, v0, :cond_0

    .line 301
    :goto_0
    return v0

    .line 298
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 300
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/by;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 342
    iget v0, p0, Lcom/google/b/f/b/a/by;->n:I

    .line 343
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 388
    :goto_0
    return v0

    .line 346
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_14

    .line 347
    iget v0, p0, Lcom/google/b/f/b/a/by;->b:I

    .line 348
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_b

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 350
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 351
    iget v3, p0, Lcom/google/b/f/b/a/by;->c:I

    .line 352
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_c

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 354
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 355
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/by;->d:I

    .line 356
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 358
    :cond_2
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 359
    iget v3, p0, Lcom/google/b/f/b/a/by;->e:I

    .line 360
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_e

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 362
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 363
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/b/f/b/a/by;->f:I

    .line 364
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 366
    :cond_4
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 367
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/b/f/b/a/by;->g:I

    .line 368
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 370
    :cond_5
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 371
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/b/f/b/a/by;->h:I

    .line 372
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 374
    :cond_6
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 375
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/b/f/b/a/by;->i:I

    .line 376
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 378
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 379
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/b/f/b/a/by;->j:I

    .line 380
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_a
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 382
    :cond_8
    iget v3, p0, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_a

    .line 383
    iget v3, p0, Lcom/google/b/f/b/a/by;->k:I

    .line 384
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_9
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 386
    :cond_a
    iget-object v1, p0, Lcom/google/b/f/b/a/by;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    iput v0, p0, Lcom/google/b/f/b/a/by;->n:I

    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 348
    goto/16 :goto_1

    :cond_c
    move v3, v1

    .line 352
    goto/16 :goto_3

    :cond_d
    move v3, v1

    .line 356
    goto/16 :goto_4

    :cond_e
    move v3, v1

    .line 360
    goto/16 :goto_5

    :cond_f
    move v3, v1

    .line 364
    goto/16 :goto_6

    :cond_10
    move v3, v1

    .line 368
    goto/16 :goto_7

    :cond_11
    move v3, v1

    .line 372
    goto :goto_8

    :cond_12
    move v3, v1

    .line 376
    goto :goto_9

    :cond_13
    move v3, v1

    .line 380
    goto :goto_a

    :cond_14
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/by;->newBuilder()Lcom/google/b/f/b/a/ca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/ca;->a(Lcom/google/b/f/b/a/by;)Lcom/google/b/f/b/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/by;->newBuilder()Lcom/google/b/f/b/a/ca;

    move-result-object v0

    return-object v0
.end method
