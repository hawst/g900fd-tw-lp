.class public final Lcom/google/b/f/b/a/aw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/az;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/aw;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/b/f/b/a/aw;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:F

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/b/f/b/a/ax;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ax;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/aw;->PARSER:Lcom/google/n/ax;

    .line 319
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/aw;->l:Lcom/google/n/aw;

    .line 823
    new-instance v0, Lcom/google/b/f/b/a/aw;

    invoke-direct {v0}, Lcom/google/b/f/b/a/aw;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/aw;->i:Lcom/google/b/f/b/a/aw;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 179
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    .line 211
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    .line 241
    iput-byte v4, p0, Lcom/google/b/f/b/a/aw;->j:B

    .line 278
    iput v4, p0, Lcom/google/b/f/b/a/aw;->k:I

    .line 18
    iput v2, p0, Lcom/google/b/f/b/a/aw;->b:I

    .line 19
    iput v2, p0, Lcom/google/b/f/b/a/aw;->c:I

    .line 20
    iput v2, p0, Lcom/google/b/f/b/a/aw;->d:I

    .line 21
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/aw;->h:F

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/b/f/b/a/aw;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 37
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 39
    sparse-switch v3, :sswitch_data_0

    .line 44
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 46
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 52
    invoke-static {v3}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v4

    .line 53
    if-nez v4, :cond_1

    .line 54
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/aw;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    .line 57
    iput v3, p0, Lcom/google/b/f/b/a/aw;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 108
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 109
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 63
    invoke-static {v3}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v4

    .line 64
    if-nez v4, :cond_2

    .line 65
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 67
    :cond_2
    iget v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    .line 68
    iput v3, p0, Lcom/google/b/f/b/a/aw;->c:I

    goto :goto_0

    .line 73
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 74
    invoke-static {v3}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v4

    .line 75
    if-nez v4, :cond_3

    .line 76
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 78
    :cond_3
    iget v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/b/a/aw;->a:I

    .line 79
    iput v3, p0, Lcom/google/b/f/b/a/aw;->d:I

    goto :goto_0

    .line 84
    :sswitch_4
    iget-object v3, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 85
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    goto/16 :goto_0

    .line 89
    :sswitch_5
    iget-object v3, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 90
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    goto/16 :goto_0

    .line 94
    :sswitch_6
    iget-object v3, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 95
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    goto/16 :goto_0

    .line 99
    :sswitch_7
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    .line 100
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/aw;->h:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 111
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->au:Lcom/google/n/bn;

    .line 112
    return-void

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3d -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 179
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    .line 211
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    .line 241
    iput-byte v1, p0, Lcom/google/b/f/b/a/aw;->j:B

    .line 278
    iput v1, p0, Lcom/google/b/f/b/a/aw;->k:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/aw;
    .locals 1

    .prologue
    .line 826
    sget-object v0, Lcom/google/b/f/b/a/aw;->i:Lcom/google/b/f/b/a/aw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/ay;
    .locals 1

    .prologue
    .line 381
    new-instance v0, Lcom/google/b/f/b/a/ay;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ay;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/aw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/google/b/f/b/a/aw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 253
    invoke-virtual {p0}, Lcom/google/b/f/b/a/aw;->c()I

    .line 254
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 255
    iget v0, p0, Lcom/google/b/f/b/a/aw;->b:I

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 257
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 258
    iget v0, p0, Lcom/google/b/f/b/a/aw;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 260
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 261
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/aw;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 263
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 264
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 266
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 267
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 269
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 270
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 272
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 273
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/b/f/b/a/aw;->h:F

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 275
    :cond_6
    iget-object v0, p0, Lcom/google/b/f/b/a/aw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 276
    return-void

    .line 255
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 258
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 261
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243
    iget-byte v1, p0, Lcom/google/b/f/b/a/aw;->j:B

    .line 244
    if-ne v1, v0, :cond_0

    .line 248
    :goto_0
    return v0

    .line 245
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/aw;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 280
    iget v0, p0, Lcom/google/b/f/b/a/aw;->k:I

    .line 281
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 314
    :goto_0
    return v0

    .line 284
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 285
    iget v0, p0, Lcom/google/b/f/b/a/aw;->b:I

    .line 286
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 288
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 289
    iget v3, p0, Lcom/google/b/f/b/a/aw;->c:I

    .line 290
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 292
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_3

    .line 293
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/aw;->d:I

    .line 294
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 296
    :cond_3
    iget v1, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 297
    iget-object v1, p0, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    .line 298
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 300
    :cond_4
    iget v1, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_5

    .line 301
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    .line 302
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 304
    :cond_5
    iget v1, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_6

    .line 305
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    .line 306
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 308
    :cond_6
    iget v1, p0, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_7

    .line 309
    const/4 v1, 0x7

    iget v3, p0, Lcom/google/b/f/b/a/aw;->h:F

    .line 310
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 312
    :cond_7
    iget-object v1, p0, Lcom/google/b/f/b/a/aw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    iput v0, p0, Lcom/google/b/f/b/a/aw;->k:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 286
    goto/16 :goto_1

    :cond_9
    move v3, v1

    .line 290
    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/aw;->newBuilder()Lcom/google/b/f/b/a/ay;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/ay;->a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/aw;->newBuilder()Lcom/google/b/f/b/a/ay;

    move-result-object v0

    return-object v0
.end method
