.class public final Lcom/google/b/f/b/a/ca;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/cb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/by;",
        "Lcom/google/b/f/b/a/ca;",
        ">;",
        "Lcom/google/b/f/b/a/cb;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 473
    sget-object v0, Lcom/google/b/f/b/a/by;->l:Lcom/google/b/f/b/a/by;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 594
    iput v1, p0, Lcom/google/b/f/b/a/ca;->b:I

    .line 630
    iput v1, p0, Lcom/google/b/f/b/a/ca;->c:I

    .line 474
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/by;)Lcom/google/b/f/b/a/ca;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 553
    invoke-static {}, Lcom/google/b/f/b/a/by;->d()Lcom/google/b/f/b/a/by;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 585
    :goto_0
    return-object p0

    .line 554
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 555
    iget v2, p1, Lcom/google/b/f/b/a/by;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/pi;->a(I)Lcom/google/r/b/a/pi;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/pi;->a:Lcom/google/r/b/a/pi;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 554
    goto :goto_1

    .line 555
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iget v2, v2, Lcom/google/r/b/a/pi;->g:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->b:I

    .line 557
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 558
    iget v2, p1, Lcom/google/b/f/b/a/by;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/dh;->a(I)Lcom/google/r/b/a/dh;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/dh;->a:Lcom/google/r/b/a/dh;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 557
    goto :goto_2

    .line 558
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iget v2, v2, Lcom/google/r/b/a/dh;->q:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->c:I

    .line 560
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 561
    iget v2, p1, Lcom/google/b/f/b/a/by;->d:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->h:I

    .line 563
    :cond_9
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 564
    iget v2, p1, Lcom/google/b/f/b/a/by;->e:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->i:I

    .line 566
    :cond_a
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 567
    iget v2, p1, Lcom/google/b/f/b/a/by;->f:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->j:I

    .line 569
    :cond_b
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    .line 570
    iget v2, p1, Lcom/google/b/f/b/a/by;->g:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->k:I

    .line 572
    :cond_c
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_d

    .line 573
    iget v2, p1, Lcom/google/b/f/b/a/by;->h:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->d:I

    .line 575
    :cond_d
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_e

    .line 576
    iget v2, p1, Lcom/google/b/f/b/a/by;->i:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->e:I

    .line 578
    :cond_e
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_f

    .line 579
    iget v2, p1, Lcom/google/b/f/b/a/by;->j:I

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ca;->f:I

    .line 581
    :cond_f
    iget v2, p1, Lcom/google/b/f/b/a/by;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_18

    :goto_a
    if-eqz v0, :cond_10

    .line 582
    iget v0, p1, Lcom/google/b/f/b/a/by;->k:I

    iget v1, p0, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/b/f/b/a/ca;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/ca;->g:I

    .line 584
    :cond_10
    iget-object v0, p1, Lcom/google/b/f/b/a/by;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v2, v1

    .line 560
    goto/16 :goto_3

    :cond_12
    move v2, v1

    .line 563
    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 566
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 569
    goto :goto_6

    :cond_15
    move v2, v1

    .line 572
    goto :goto_7

    :cond_16
    move v2, v1

    .line 575
    goto :goto_8

    :cond_17
    move v2, v1

    .line 578
    goto :goto_9

    :cond_18
    move v0, v1

    .line 581
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 465
    new-instance v2, Lcom/google/b/f/b/a/by;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/by;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/ca;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/ca;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/ca;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/ca;->h:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/ca;->i:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/b/f/b/a/ca;->j:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/b/f/b/a/ca;->k:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/b/f/b/a/ca;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/b/f/b/a/ca;->e:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/b/f/b/a/ca;->f:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/b/f/b/a/ca;->g:I

    iput v1, v2, Lcom/google/b/f/b/a/by;->k:I

    iput v0, v2, Lcom/google/b/f/b/a/by;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 465
    check-cast p1, Lcom/google/b/f/b/a/by;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/ca;->a(Lcom/google/b/f/b/a/by;)Lcom/google/b/f/b/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x1

    return v0
.end method
