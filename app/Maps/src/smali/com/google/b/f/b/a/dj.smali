.class public final Lcom/google/b/f/b/a/dj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/dm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dj;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/b/a/dj;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/b/f/b/a/ck;

.field public c:Lcom/google/b/f/b/a/a;

.field d:Lcom/google/b/f/b/a/cc;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    new-instance v0, Lcom/google/b/f/b/a/dk;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dk;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dj;->PARSER:Lcom/google/n/ax;

    .line 379
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/dj;->h:Lcom/google/n/aw;

    .line 710
    new-instance v0, Lcom/google/b/f/b/a/dj;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dj;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dj;->e:Lcom/google/b/f/b/a/dj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 186
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 323
    iput-byte v0, p0, Lcom/google/b/f/b/a/dj;->f:B

    .line 354
    iput v0, p0, Lcom/google/b/f/b/a/dj;->g:I

    .line 187
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 193
    invoke-direct {p0}, Lcom/google/b/f/b/a/dj;-><init>()V

    .line 194
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 198
    const/4 v0, 0x0

    move v3, v0

    .line 199
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 200
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 201
    sparse-switch v0, :sswitch_data_0

    .line 206
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 208
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 204
    goto :goto_0

    .line 214
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 215
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    move-object v1, v0

    .line 217
    :goto_1
    sget-object v0, Lcom/google/b/f/b/a/ck;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ck;

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    .line 218
    if-eqz v1, :cond_1

    .line 219
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    .line 220
    invoke-virtual {v1}, Lcom/google/b/f/b/a/cm;->c()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    .line 222
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/dj;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 253
    :catch_0
    move-exception v0

    .line 254
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/dj;->au:Lcom/google/n/bn;

    throw v0

    .line 227
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 228
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    invoke-static {}, Lcom/google/b/f/b/a/a;->newBuilder()Lcom/google/b/f/b/a/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v0

    move-object v1, v0

    .line 230
    :goto_2
    sget-object v0, Lcom/google/b/f/b/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/a;

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    .line 231
    if-eqz v1, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    .line 233
    invoke-virtual {v1}, Lcom/google/b/f/b/a/c;->c()Lcom/google/b/f/b/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    .line 235
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/dj;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 255
    :catch_1
    move-exception v0

    .line 256
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 257
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 240
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 241
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    invoke-static {}, Lcom/google/b/f/b/a/cc;->newBuilder()Lcom/google/b/f/b/a/ce;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v0

    move-object v1, v0

    .line 243
    :goto_3
    sget-object v0, Lcom/google/b/f/b/a/cc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cc;

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    .line 244
    if-eqz v1, :cond_3

    .line 245
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    .line 246
    invoke-virtual {v1}, Lcom/google/b/f/b/a/ce;->c()Lcom/google/b/f/b/a/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    .line 248
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/b/a/dj;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 259
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dj;->au:Lcom/google/n/bn;

    .line 260
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_1

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 184
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 323
    iput-byte v0, p0, Lcom/google/b/f/b/a/dj;->f:B

    .line 354
    iput v0, p0, Lcom/google/b/f/b/a/dj;->g:I

    .line 185
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/dj;
    .locals 1

    .prologue
    .line 713
    sget-object v0, Lcom/google/b/f/b/a/dj;->e:Lcom/google/b/f/b/a/dj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/dl;
    .locals 1

    .prologue
    .line 441
    new-instance v0, Lcom/google/b/f/b/a/dl;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    sget-object v0, Lcom/google/b/f/b/a/dj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 341
    invoke-virtual {p0}, Lcom/google/b/f/b/a/dj;->c()I

    .line 342
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/b/f/b/a/ck;->d()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 345
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v0

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 348
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 349
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/b/f/b/a/cc;->d()Lcom/google/b/f/b/a/cc;

    move-result-object v0

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 352
    return-void

    .line 343
    :cond_3
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    goto :goto_0

    .line 346
    :cond_4
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    goto :goto_1

    .line 349
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 325
    iget-byte v0, p0, Lcom/google/b/f/b/a/dj;->f:B

    .line 326
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 336
    :goto_0
    return v0

    .line 327
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 329
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 330
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/b/f/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 331
    iput-byte v2, p0, Lcom/google/b/f/b/a/dj;->f:B

    move v0, v2

    .line 332
    goto :goto_0

    :cond_2
    move v0, v2

    .line 329
    goto :goto_1

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    goto :goto_2

    .line 335
    :cond_4
    iput-byte v1, p0, Lcom/google/b/f/b/a/dj;->f:B

    move v0, v1

    .line 336
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 356
    iget v0, p0, Lcom/google/b/f/b/a/dj;->g:I

    .line 357
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 374
    :goto_0
    return v0

    .line 360
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 362
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/b/f/b/a/ck;->d()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 364
    :goto_2
    iget v2, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 366
    iget-object v2, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 368
    :cond_1
    iget v2, p0, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 369
    const/4 v3, 0x3

    .line 370
    iget-object v2, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    if-nez v2, :cond_5

    invoke-static {}, Lcom/google/b/f/b/a/cc;->d()Lcom/google/b/f/b/a/cc;

    move-result-object v2

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 372
    :cond_2
    iget-object v1, p0, Lcom/google/b/f/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    iput v0, p0, Lcom/google/b/f/b/a/dj;->g:I

    goto :goto_0

    .line 362
    :cond_3
    iget-object v0, p0, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    goto :goto_1

    .line 366
    :cond_4
    iget-object v2, p0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    goto :goto_3

    .line 370
    :cond_5
    iget-object v2, p0, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Lcom/google/b/f/b/a/dj;->newBuilder()Lcom/google/b/f/b/a/dl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/dl;->a(Lcom/google/b/f/b/a/dj;)Lcom/google/b/f/b/a/dl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Lcom/google/b/f/b/a/dj;->newBuilder()Lcom/google/b/f/b/a/dl;

    move-result-object v0

    return-object v0
.end method
