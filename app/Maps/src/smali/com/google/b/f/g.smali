.class public final Lcom/google/b/f/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/g;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/b/f/g;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/b/f/h;

    invoke-direct {v0}, Lcom/google/b/f/h;-><init>()V

    sput-object v0, Lcom/google/b/f/g;->PARSER:Lcom/google/n/ax;

    .line 152
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/g;->f:Lcom/google/n/aw;

    .line 344
    new-instance v0, Lcom/google/b/f/g;

    invoke-direct {v0}, Lcom/google/b/f/g;-><init>()V

    sput-object v0, Lcom/google/b/f/g;->c:Lcom/google/b/f/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 95
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    .line 110
    iput-byte v2, p0, Lcom/google/b/f/g;->d:B

    .line 135
    iput v2, p0, Lcom/google/b/f/g;->e:I

    .line 36
    iget-object v0, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/b/f/g;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 49
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 50
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 51
    sparse-switch v3, :sswitch_data_0

    .line 56
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 58
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 54
    goto :goto_0

    .line 63
    :sswitch_1
    iget-object v3, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 64
    iget v3, p0, Lcom/google/b/f/g;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/g;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/g;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/g;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 33
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 95
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    .line 110
    iput-byte v1, p0, Lcom/google/b/f/g;->d:B

    .line 135
    iput v1, p0, Lcom/google/b/f/g;->e:I

    .line 34
    return-void
.end method

.method public static d()Lcom/google/b/f/g;
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/google/b/f/g;->c:Lcom/google/b/f/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/i;
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lcom/google/b/f/i;

    invoke-direct {v0}, Lcom/google/b/f/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/b/f/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 128
    invoke-virtual {p0}, Lcom/google/b/f/g;->c()I

    .line 129
    iget v0, p0, Lcom/google/b/f/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/b/f/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 133
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    iget-byte v0, p0, Lcom/google/b/f/g;->d:B

    .line 113
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 123
    :goto_0
    return v0

    .line 114
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 116
    :cond_1
    iget v0, p0, Lcom/google/b/f/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ah;->d()Lcom/google/b/f/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ah;

    invoke-virtual {v0}, Lcom/google/b/f/ah;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 118
    iput-byte v2, p0, Lcom/google/b/f/g;->d:B

    move v0, v2

    .line 119
    goto :goto_0

    :cond_2
    move v0, v2

    .line 116
    goto :goto_1

    .line 122
    :cond_3
    iput-byte v1, p0, Lcom/google/b/f/g;->d:B

    move v0, v1

    .line 123
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 137
    iget v1, p0, Lcom/google/b/f/g;->e:I

    .line 138
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 147
    :goto_0
    return v0

    .line 141
    :cond_0
    iget v1, p0, Lcom/google/b/f/g;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 142
    iget-object v1, p0, Lcom/google/b/f/g;->b:Lcom/google/n/ao;

    .line 143
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/google/b/f/g;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    iput v0, p0, Lcom/google/b/f/g;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/b/f/g;->newBuilder()Lcom/google/b/f/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/i;->a(Lcom/google/b/f/g;)Lcom/google/b/f/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/b/f/g;->newBuilder()Lcom/google/b/f/i;

    move-result-object v0

    return-object v0
.end method
