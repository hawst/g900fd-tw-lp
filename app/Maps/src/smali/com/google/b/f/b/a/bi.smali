.class public final Lcom/google/b/f/b/a/bi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bx;


# static fields
.field static final L:Lcom/google/b/f/b/a/bi;

.field private static volatile O:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/bi;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field A:Lcom/google/n/ao;

.field B:Lcom/google/n/ao;

.field C:Lcom/google/n/ao;

.field D:Lcom/google/n/ao;

.field E:I

.field F:I

.field G:Ljava/lang/Object;

.field H:Ljava/lang/Object;

.field I:I

.field J:I

.field K:I

.field private M:B

.field private N:I

.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:Lcom/google/n/ao;

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field r:I

.field s:Z

.field t:I

.field u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field v:Z

.field w:Z

.field x:F

.field y:I

.field z:Lcom/google/n/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295
    new-instance v0, Lcom/google/b/f/b/a/bj;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bj;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/bi;->PARSER:Lcom/google/n/ax;

    .line 2212
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/bi;->O:Lcom/google/n/aw;

    .line 4272
    new-instance v0, Lcom/google/b/f/b/a/bi;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bi;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/bi;->L:Lcom/google/b/f/b/a/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1387
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    .line 1700
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    .line 1716
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    .line 1732
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    .line 1748
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    .line 1764
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    .line 1938
    iput-byte v4, p0, Lcom/google/b/f/b/a/bi;->M:B

    .line 2059
    iput v4, p0, Lcom/google/b/f/b/a/bi;->N:I

    .line 18
    iput v1, p0, Lcom/google/b/f/b/a/bi;->c:I

    .line 19
    iput v1, p0, Lcom/google/b/f/b/a/bi;->d:I

    .line 20
    iput v1, p0, Lcom/google/b/f/b/a/bi;->e:I

    .line 21
    iput v1, p0, Lcom/google/b/f/b/a/bi;->f:I

    .line 22
    iput v1, p0, Lcom/google/b/f/b/a/bi;->g:I

    .line 23
    iput v1, p0, Lcom/google/b/f/b/a/bi;->h:I

    .line 24
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput v1, p0, Lcom/google/b/f/b/a/bi;->j:I

    .line 26
    iput v1, p0, Lcom/google/b/f/b/a/bi;->k:I

    .line 27
    iput v1, p0, Lcom/google/b/f/b/a/bi;->l:I

    .line 28
    iput v1, p0, Lcom/google/b/f/b/a/bi;->m:I

    .line 29
    iput v1, p0, Lcom/google/b/f/b/a/bi;->n:I

    .line 30
    iput v1, p0, Lcom/google/b/f/b/a/bi;->o:I

    .line 31
    iput v1, p0, Lcom/google/b/f/b/a/bi;->p:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    .line 33
    iput v1, p0, Lcom/google/b/f/b/a/bi;->r:I

    .line 34
    iput-boolean v1, p0, Lcom/google/b/f/b/a/bi;->s:Z

    .line 35
    iput v1, p0, Lcom/google/b/f/b/a/bi;->t:I

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    .line 37
    iput-boolean v1, p0, Lcom/google/b/f/b/a/bi;->v:Z

    .line 38
    iput-boolean v1, p0, Lcom/google/b/f/b/a/bi;->w:Z

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->x:F

    .line 40
    iput v1, p0, Lcom/google/b/f/b/a/bi;->y:I

    .line 41
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 42
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 43
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 44
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 45
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 46
    iput v1, p0, Lcom/google/b/f/b/a/bi;->E:I

    .line 47
    iput v1, p0, Lcom/google/b/f/b/a/bi;->F:I

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    .line 50
    iput v1, p0, Lcom/google/b/f/b/a/bi;->I:I

    .line 51
    iput v1, p0, Lcom/google/b/f/b/a/bi;->J:I

    .line 52
    iput v1, p0, Lcom/google/b/f/b/a/bi;->K:I

    .line 53
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x4000

    const/4 v2, 0x1

    const/high16 v8, 0x40000

    const/4 v3, 0x0

    .line 59
    invoke-direct {p0}, Lcom/google/b/f/b/a/bi;-><init>()V

    .line 61
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 66
    :cond_0
    :goto_0
    if-nez v4, :cond_a

    .line 67
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 68
    sparse-switch v0, :sswitch_data_0

    .line 73
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 75
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 71
    goto :goto_0

    .line 80
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 81
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v6

    .line 82
    if-nez v6, :cond_3

    .line 83
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    :catchall_0
    move-exception v0

    and-int v2, v1, v8

    if-ne v2, v8, :cond_1

    .line 287
    iget-object v2, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    .line 289
    :cond_1
    and-int/lit16 v1, v1, 0x4000

    if-ne v1, v9, :cond_2

    .line 290
    iget-object v1, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    .line 292
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/bi;->au:Lcom/google/n/bn;

    throw v0

    .line 85
    :cond_3
    :try_start_2
    iget v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 86
    iput v0, p0, Lcom/google/b/f/b/a/bi;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282
    :catch_1
    move-exception v0

    .line 283
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 284
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 91
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->d:I

    goto :goto_0

    .line 96
    :sswitch_3
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->e:I

    goto :goto_0

    .line 101
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->j:I

    goto/16 :goto_0

    .line 106
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->l:I

    goto/16 :goto_0

    .line 111
    :sswitch_6
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 112
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->m:I

    goto/16 :goto_0

    .line 116
    :sswitch_7
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 117
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->o:I

    goto/16 :goto_0

    .line 121
    :sswitch_8
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 122
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->r:I

    goto/16 :goto_0

    .line 126
    :sswitch_9
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 127
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/b/f/b/a/bi;->s:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 131
    :sswitch_a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 132
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->t:I

    goto/16 :goto_0

    .line 136
    :sswitch_b
    and-int v0, v1, v8

    if-eq v0, v8, :cond_5

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    .line 139
    or-int/2addr v1, v8

    .line 141
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 142
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 141
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 146
    :sswitch_c
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 147
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/b/f/b/a/bi;->v:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_2

    .line 151
    :sswitch_d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/2addr v0, v8

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 152
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_7

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/b/f/b/a/bi;->w:Z

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_3

    .line 156
    :sswitch_e
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 157
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->x:F

    goto/16 :goto_0

    .line 161
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 162
    invoke-static {v0}, Lcom/google/b/f/b/a/bl;->a(I)Lcom/google/b/f/b/a/bl;

    move-result-object v6

    .line 163
    if-nez v6, :cond_8

    .line 164
    const/16 v6, 0xf

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 166
    :cond_8
    iget v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v7, 0x100000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 167
    iput v0, p0, Lcom/google/b/f/b/a/bi;->y:I

    goto/16 :goto_0

    .line 172
    :sswitch_10
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 173
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 177
    :sswitch_11
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 178
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 182
    :sswitch_12
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 183
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 187
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 188
    iget v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v7, 0x10000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 189
    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    goto/16 :goto_0

    .line 193
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 194
    iget v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v7, 0x20000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 195
    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    goto/16 :goto_0

    .line 199
    :sswitch_15
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x40000000    # 2.0f

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 200
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->I:I

    goto/16 :goto_0

    .line 204
    :sswitch_16
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, -0x80000000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 205
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->J:I

    goto/16 :goto_0

    .line 209
    :sswitch_17
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 210
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->n:I

    goto/16 :goto_0

    .line 214
    :sswitch_18
    and-int/lit16 v0, v1, 0x4000

    if-eq v0, v9, :cond_9

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    .line 217
    or-int/lit16 v1, v1, 0x4000

    .line 219
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 220
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 219
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 224
    :sswitch_19
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 225
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->k:I

    goto/16 :goto_0

    .line 229
    :sswitch_1a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 230
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->f:I

    goto/16 :goto_0

    .line 234
    :sswitch_1b
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 235
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->g:I

    goto/16 :goto_0

    .line 239
    :sswitch_1c
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 240
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x1000000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 244
    :sswitch_1d
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 245
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x2000000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 249
    :sswitch_1e
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 250
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->p:I

    goto/16 :goto_0

    .line 254
    :sswitch_1f
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x4000000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 255
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->E:I

    goto/16 :goto_0

    .line 259
    :sswitch_20
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v6, 0x8000000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 260
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->F:I

    goto/16 :goto_0

    .line 264
    :sswitch_21
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    .line 265
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->h:I

    goto/16 :goto_0

    .line 269
    :sswitch_22
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 270
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    goto/16 :goto_0

    .line 274
    :sswitch_23
    iget v0, p0, Lcom/google/b/f/b/a/bi;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/bi;->b:I

    .line 275
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/bi;->K:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 286
    :cond_a
    and-int v0, v1, v8

    if-ne v0, v8, :cond_b

    .line 287
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    .line 289
    :cond_b
    and-int/lit16 v0, v1, 0x4000

    if-ne v0, v9, :cond_c

    .line 290
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    .line 292
    :cond_c
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->au:Lcom/google/n/bn;

    .line 293
    return-void

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x75 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc2 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x100 -> :sswitch_20
        0x108 -> :sswitch_21
        0x112 -> :sswitch_22
        0x118 -> :sswitch_23
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1387
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    .line 1700
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    .line 1716
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    .line 1732
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    .line 1748
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    .line 1764
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    .line 1938
    iput-byte v1, p0, Lcom/google/b/f/b/a/bi;->M:B

    .line 2059
    iput v1, p0, Lcom/google/b/f/b/a/bi;->N:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/bi;
    .locals 1

    .prologue
    .line 4275
    sget-object v0, Lcom/google/b/f/b/a/bi;->L:Lcom/google/b/f/b/a/bi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/bk;
    .locals 1

    .prologue
    .line 2274
    new-instance v0, Lcom/google/b/f/b/a/bk;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/bi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307
    sget-object v0, Lcom/google/b/f/b/a/bi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/high16 v7, -0x80000000

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 1950
    invoke-virtual {p0}, Lcom/google/b/f/b/a/bi;->c()I

    .line 1951
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1952
    iget v0, p0, Lcom/google/b/f/b/a/bi;->c:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1954
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1955
    iget v0, p0, Lcom/google/b/f/b/a/bi;->d:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1957
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_2

    .line 1958
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/b/f/b/a/bi;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_c

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1960
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 1961
    iget v0, p0, Lcom/google/b/f/b/a/bi;->j:I

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_d

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1963
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_4

    .line 1964
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/b/f/b/a/bi;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_e

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1966
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_5

    .line 1967
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/b/f/b/a/bi;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_f

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1969
    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_6

    .line 1970
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/b/f/b/a/bi;->o:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_10

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1972
    :cond_6
    :goto_6
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_7

    .line 1973
    const/16 v0, 0x8

    iget v3, p0, Lcom/google/b/f/b/a/bi;->r:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_11

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1975
    :cond_7
    :goto_7
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_8

    .line 1976
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/b/f/b/a/bi;->s:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_12

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1978
    :cond_8
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_9

    .line 1979
    const/16 v0, 0xa

    iget v3, p0, Lcom/google/b/f/b/a/bi;->t:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_13

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    :cond_9
    :goto_9
    move v3, v2

    .line 1981
    :goto_a
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_14

    .line 1982
    const/16 v4, 0xb

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1981
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    .line 1952
    :cond_a
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 1955
    :cond_b
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 1958
    :cond_c
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 1961
    :cond_d
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 1964
    :cond_e
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 1967
    :cond_f
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 1970
    :cond_10
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    .line 1973
    :cond_11
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_7

    :cond_12
    move v0, v2

    .line 1976
    goto :goto_8

    .line 1979
    :cond_13
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    .line 1984
    :cond_14
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_15

    .line 1985
    const/16 v0, 0xc

    iget-boolean v3, p0, Lcom/google/b/f/b/a/bi;->v:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_21

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1987
    :cond_15
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_16

    .line 1988
    const/16 v0, 0xd

    iget-boolean v3, p0, Lcom/google/b/f/b/a/bi;->w:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_22

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1990
    :cond_16
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_17

    .line 1991
    const/16 v0, 0xe

    iget v3, p0, Lcom/google/b/f/b/a/bi;->x:F

    const/4 v4, 0x5

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v3}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1993
    :cond_17
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_18

    .line 1994
    const/16 v0, 0xf

    iget v3, p0, Lcom/google/b/f/b/a/bi;->y:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_23

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1996
    :cond_18
    :goto_d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_19

    .line 1997
    const/16 v0, 0x10

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1999
    :cond_19
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_1a

    .line 2000
    const/16 v0, 0x11

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2002
    :cond_1a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v0, v3

    const/high16 v3, 0x800000

    if-ne v0, v3, :cond_1b

    .line 2003
    const/16 v0, 0x12

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2005
    :cond_1b
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_1c

    .line 2006
    const/16 v3, 0x13

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_24

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    :goto_e
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2008
    :cond_1c
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000000

    if-ne v0, v3, :cond_1d

    .line 2009
    const/16 v3, 0x14

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_25

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    :goto_f
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2011
    :cond_1d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_1e

    .line 2012
    const/16 v0, 0x15

    iget v3, p0, Lcom/google/b/f/b/a/bi;->I:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_26

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2014
    :cond_1e
    :goto_10
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_1f

    .line 2015
    const/16 v0, 0x16

    iget v3, p0, Lcom/google/b/f/b/a/bi;->J:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_27

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2017
    :cond_1f
    :goto_11
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_20

    .line 2018
    const/16 v0, 0x17

    iget v3, p0, Lcom/google/b/f/b/a/bi;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_28

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    :cond_20
    :goto_12
    move v3, v2

    .line 2020
    :goto_13
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_29

    .line 2021
    const/16 v4, 0x18

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2020
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_13

    :cond_21
    move v0, v2

    .line 1985
    goto/16 :goto_b

    :cond_22
    move v0, v2

    .line 1988
    goto/16 :goto_c

    .line 1994
    :cond_23
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_d

    .line 2006
    :cond_24
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    .line 2009
    :cond_25
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_f

    .line 2012
    :cond_26
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_10

    .line 2015
    :cond_27
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_11

    .line 2018
    :cond_28
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_12

    .line 2023
    :cond_29
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2a

    .line 2024
    const/16 v0, 0x19

    iget v3, p0, Lcom/google/b/f/b/a/bi;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_35

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2026
    :cond_2a
    :goto_14
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2b

    .line 2027
    const/16 v0, 0x1a

    iget v3, p0, Lcom/google/b/f/b/a/bi;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_36

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2029
    :cond_2b
    :goto_15
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2c

    .line 2030
    const/16 v0, 0x1b

    iget v3, p0, Lcom/google/b/f/b/a/bi;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_37

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2032
    :cond_2c
    :goto_16
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_2d

    .line 2033
    const/16 v0, 0x1c

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2035
    :cond_2d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    const/high16 v3, 0x2000000

    if-ne v0, v3, :cond_2e

    .line 2036
    const/16 v0, 0x1d

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2038
    :cond_2e
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_2f

    .line 2039
    const/16 v0, 0x1e

    iget v3, p0, Lcom/google/b/f/b/a/bi;->p:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_38

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2041
    :cond_2f
    :goto_17
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v0, v3

    const/high16 v3, 0x4000000

    if-ne v0, v3, :cond_30

    .line 2042
    const/16 v0, 0x1f

    iget v3, p0, Lcom/google/b/f/b/a/bi;->E:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_39

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2044
    :cond_30
    :goto_18
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v0, v3

    const/high16 v3, 0x8000000

    if-ne v0, v3, :cond_31

    .line 2045
    const/16 v0, 0x20

    iget v3, p0, Lcom/google/b/f/b/a/bi;->F:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_3a

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2047
    :cond_31
    :goto_19
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_32

    .line 2048
    const/16 v0, 0x21

    iget v3, p0, Lcom/google/b/f/b/a/bi;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_3b

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 2050
    :cond_32
    :goto_1a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_33

    .line 2051
    const/16 v0, 0x22

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2053
    :cond_33
    iget v0, p0, Lcom/google/b/f/b/a/bi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_34

    .line 2054
    const/16 v0, 0x23

    iget v1, p0, Lcom/google/b/f/b/a/bi;->K:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_3c

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2056
    :cond_34
    :goto_1b
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2057
    return-void

    .line 2024
    :cond_35
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_14

    .line 2027
    :cond_36
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_15

    .line 2030
    :cond_37
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_16

    .line 2039
    :cond_38
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_17

    .line 2042
    :cond_39
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_18

    .line 2045
    :cond_3a
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_19

    .line 2048
    :cond_3b
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1a

    .line 2054
    :cond_3c
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1b
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1940
    iget-byte v1, p0, Lcom/google/b/f/b/a/bi;->M:B

    .line 1941
    if-ne v1, v0, :cond_0

    .line 1945
    :goto_0
    return v0

    .line 1942
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1944
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/bi;->M:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/high16 v7, -0x80000000

    const/4 v6, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 2061
    iget v0, p0, Lcom/google/b/f/b/a/bi;->N:I

    .line 2062
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2207
    :goto_0
    return v0

    .line 2065
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3a

    .line 2066
    iget v0, p0, Lcom/google/b/f/b/a/bi;->c:I

    .line 2067
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 2069
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v4, :cond_1

    .line 2070
    iget v3, p0, Lcom/google/b/f/b/a/bi;->d:I

    .line 2071
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 2073
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 2074
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/bi;->e:I

    .line 2075
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2077
    :cond_2
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_3

    .line 2078
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/b/f/b/a/bi;->j:I

    .line 2079
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2081
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_4

    .line 2082
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/b/f/b/a/bi;->l:I

    .line 2083
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2085
    :cond_4
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_5

    .line 2086
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/b/f/b/a/bi;->m:I

    .line 2087
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2089
    :cond_5
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_6

    .line 2090
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/b/f/b/a/bi;->o:I

    .line 2091
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2093
    :cond_6
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v3, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_7

    .line 2094
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/b/f/b/a/bi;->r:I

    .line 2095
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2097
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    const v4, 0x8000

    and-int/2addr v3, v4

    const v4, 0x8000

    if-ne v3, v4, :cond_8

    .line 2098
    const/16 v3, 0x9

    iget-boolean v4, p0, Lcom/google/b/f/b/a/bi;->s:Z

    .line 2099
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 2101
    :cond_8
    iget v3, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v3, v4

    const/high16 v4, 0x10000

    if-ne v3, v4, :cond_9

    .line 2102
    iget v3, p0, Lcom/google/b/f/b/a/bi;->t:I

    .line 2103
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_12

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_a
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    :cond_9
    move v3, v2

    move v4, v0

    .line 2105
    :goto_b
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_13

    .line 2106
    const/16 v5, 0xb

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->u:Ljava/util/List;

    .line 2107
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2105
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    :cond_a
    move v0, v1

    .line 2067
    goto/16 :goto_1

    :cond_b
    move v3, v1

    .line 2071
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 2075
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 2079
    goto/16 :goto_5

    :cond_e
    move v3, v1

    .line 2083
    goto/16 :goto_6

    :cond_f
    move v3, v1

    .line 2087
    goto/16 :goto_7

    :cond_10
    move v3, v1

    .line 2091
    goto/16 :goto_8

    :cond_11
    move v3, v1

    .line 2095
    goto :goto_9

    :cond_12
    move v3, v1

    .line 2103
    goto :goto_a

    .line 2109
    :cond_13
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_14

    .line 2110
    const/16 v0, 0xc

    iget-boolean v3, p0, Lcom/google/b/f/b/a/bi;->v:Z

    .line 2111
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 2113
    :cond_14
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_15

    .line 2114
    const/16 v0, 0xd

    iget-boolean v3, p0, Lcom/google/b/f/b/a/bi;->w:Z

    .line 2115
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 2117
    :cond_15
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_16

    .line 2118
    const/16 v0, 0xe

    iget v3, p0, Lcom/google/b/f/b/a/bi;->x:F

    .line 2119
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 2121
    :cond_16
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_17

    .line 2122
    const/16 v0, 0xf

    iget v3, p0, Lcom/google/b/f/b/a/bi;->y:I

    .line 2123
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_20

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2125
    :cond_17
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_18

    .line 2126
    const/16 v0, 0x10

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->z:Lcom/google/n/ao;

    .line 2127
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2129
    :cond_18
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_19

    .line 2130
    const/16 v0, 0x11

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->A:Lcom/google/n/ao;

    .line 2131
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2133
    :cond_19
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v0, v3

    const/high16 v3, 0x800000

    if-ne v0, v3, :cond_1a

    .line 2134
    const/16 v0, 0x12

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->B:Lcom/google/n/ao;

    .line 2135
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2137
    :cond_1a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_1b

    .line 2138
    const/16 v3, 0x13

    .line 2139
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_21

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->G:Ljava/lang/Object;

    :goto_d
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2141
    :cond_1b
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000000

    if-ne v0, v3, :cond_1c

    .line 2142
    const/16 v3, 0x14

    .line 2143
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bi;->H:Ljava/lang/Object;

    :goto_e
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2145
    :cond_1c
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_1d

    .line 2146
    const/16 v0, 0x15

    iget v3, p0, Lcom/google/b/f/b/a/bi;->I:I

    .line 2147
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_23

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_f
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2149
    :cond_1d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_1e

    .line 2150
    const/16 v0, 0x16

    iget v3, p0, Lcom/google/b/f/b/a/bi;->J:I

    .line 2151
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_24

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_10
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2153
    :cond_1e
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_1f

    .line 2154
    const/16 v0, 0x17

    iget v3, p0, Lcom/google/b/f/b/a/bi;->n:I

    .line 2155
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_25

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_11
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    :cond_1f
    move v3, v2

    .line 2157
    :goto_12
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_26

    .line 2158
    const/16 v5, 0x18

    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->q:Ljava/util/List;

    .line 2159
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2157
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_12

    :cond_20
    move v0, v1

    .line 2123
    goto/16 :goto_c

    .line 2139
    :cond_21
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_d

    .line 2143
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    :cond_23
    move v0, v1

    .line 2147
    goto :goto_f

    :cond_24
    move v0, v1

    .line 2151
    goto :goto_10

    :cond_25
    move v0, v1

    .line 2155
    goto :goto_11

    .line 2161
    :cond_26
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_27

    .line 2162
    const/16 v0, 0x19

    iget v3, p0, Lcom/google/b/f/b/a/bi;->k:I

    .line 2163
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_33

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_13
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2165
    :cond_27
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_28

    .line 2166
    const/16 v0, 0x1a

    iget v3, p0, Lcom/google/b/f/b/a/bi;->f:I

    .line 2167
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_34

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_14
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2169
    :cond_28
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_29

    .line 2170
    const/16 v0, 0x1b

    iget v3, p0, Lcom/google/b/f/b/a/bi;->g:I

    .line 2171
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_35

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_15
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2173
    :cond_29
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_2a

    .line 2174
    const/16 v0, 0x1c

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->C:Lcom/google/n/ao;

    .line 2175
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2177
    :cond_2a
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    const/high16 v3, 0x2000000

    if-ne v0, v3, :cond_2b

    .line 2178
    const/16 v0, 0x1d

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->D:Lcom/google/n/ao;

    .line 2179
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2181
    :cond_2b
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_2c

    .line 2182
    const/16 v0, 0x1e

    iget v3, p0, Lcom/google/b/f/b/a/bi;->p:I

    .line 2183
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_36

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_16
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2185
    :cond_2c
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v0, v3

    const/high16 v3, 0x4000000

    if-ne v0, v3, :cond_2d

    .line 2186
    const/16 v0, 0x1f

    iget v3, p0, Lcom/google/b/f/b/a/bi;->E:I

    .line 2187
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_37

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_17
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2189
    :cond_2d
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v0, v3

    const/high16 v3, 0x8000000

    if-ne v0, v3, :cond_2e

    .line 2190
    const/16 v0, 0x20

    iget v3, p0, Lcom/google/b/f/b/a/bi;->F:I

    .line 2191
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_38

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_18
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2193
    :cond_2e
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2f

    .line 2194
    const/16 v0, 0x21

    iget v3, p0, Lcom/google/b/f/b/a/bi;->h:I

    .line 2195
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_39

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_19
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2197
    :cond_2f
    iget v0, p0, Lcom/google/b/f/b/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_30

    .line 2198
    const/16 v0, 0x22

    iget-object v3, p0, Lcom/google/b/f/b/a/bi;->i:Lcom/google/n/ao;

    .line 2199
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2201
    :cond_30
    iget v0, p0, Lcom/google/b/f/b/a/bi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_32

    .line 2202
    const/16 v0, 0x23

    iget v3, p0, Lcom/google/b/f/b/a/bi;->K:I

    .line 2203
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_31

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_31
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 2205
    :cond_32
    iget-object v0, p0, Lcom/google/b/f/b/a/bi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 2206
    iput v0, p0, Lcom/google/b/f/b/a/bi;->N:I

    goto/16 :goto_0

    :cond_33
    move v0, v1

    .line 2163
    goto/16 :goto_13

    :cond_34
    move v0, v1

    .line 2167
    goto/16 :goto_14

    :cond_35
    move v0, v1

    .line 2171
    goto/16 :goto_15

    :cond_36
    move v0, v1

    .line 2183
    goto/16 :goto_16

    :cond_37
    move v0, v1

    .line 2187
    goto/16 :goto_17

    :cond_38
    move v0, v1

    .line 2191
    goto :goto_18

    :cond_39
    move v0, v1

    .line 2195
    goto :goto_19

    :cond_3a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/bi;->newBuilder()Lcom/google/b/f/b/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/bk;->a(Lcom/google/b/f/b/a/bi;)Lcom/google/b/f/b/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/bi;->newBuilder()Lcom/google/b/f/b/a/bk;

    move-result-object v0

    return-object v0
.end method
