.class public final Lcom/google/b/f/n;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/l;",
        "Lcom/google/b/f/n;",
        ">;",
        "Lcom/google/b/f/o;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 751
    sget-object v0, Lcom/google/b/f/l;->d:Lcom/google/b/f/l;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 809
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    .line 752
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/l;)Lcom/google/b/f/n;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 785
    invoke-static {}, Lcom/google/b/f/l;->d()Lcom/google/b/f/l;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 794
    :goto_0
    return-object p0

    .line 786
    :cond_0
    iget v2, p1, Lcom/google/b/f/l;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 787
    iget-object v2, p0, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/l;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 788
    iget v2, p0, Lcom/google/b/f/n;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/f/n;->a:I

    .line 790
    :cond_1
    iget v2, p1, Lcom/google/b/f/l;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 791
    iget-wide v0, p1, Lcom/google/b/f/l;->c:J

    iget v2, p0, Lcom/google/b/f/n;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/b/f/n;->a:I

    iput-wide v0, p0, Lcom/google/b/f/n;->c:J

    .line 793
    :cond_2
    iget-object v0, p1, Lcom/google/b/f/l;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 786
    goto :goto_1

    :cond_4
    move v0, v1

    .line 790
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 743
    new-instance v2, Lcom/google/b/f/l;

    invoke-direct {v2, p0}, Lcom/google/b/f/l;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/n;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/b/f/l;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/b/f/n;->c:J

    iput-wide v4, v2, Lcom/google/b/f/l;->c:J

    iput v0, v2, Lcom/google/b/f/l;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 743
    check-cast p1, Lcom/google/b/f/l;

    invoke-virtual {p0, p1}, Lcom/google/b/f/n;->a(Lcom/google/b/f/l;)Lcom/google/b/f/n;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 798
    iget v0, p0, Lcom/google/b/f/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 799
    iget-object v0, p0, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    invoke-virtual {v0}, Lcom/google/b/f/p;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 804
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 798
    goto :goto_0

    :cond_1
    move v0, v2

    .line 804
    goto :goto_1
.end method
