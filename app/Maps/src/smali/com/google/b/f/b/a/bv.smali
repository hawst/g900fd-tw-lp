.class public final Lcom/google/b/f/b/a/bv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/bt;",
        "Lcom/google/b/f/b/a/bv;",
        ">;",
        "Lcom/google/b/f/b/a/bw;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1082
    sget-object v0, Lcom/google/b/f/b/a/bt;->f:Lcom/google/b/f/b/a/bt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1083
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/bt;)Lcom/google/b/f/b/a/bv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1126
    invoke-static {}, Lcom/google/b/f/b/a/bt;->d()Lcom/google/b/f/b/a/bt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1140
    :goto_0
    return-object p0

    .line 1127
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1128
    iget v2, p1, Lcom/google/b/f/b/a/bt;->b:I

    iget v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/bv;->b:I

    .line 1130
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1131
    iget v2, p1, Lcom/google/b/f/b/a/bt;->c:I

    iget v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/bv;->c:I

    .line 1133
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1134
    iget v2, p1, Lcom/google/b/f/b/a/bt;->d:I

    iget v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/bv;->d:I

    .line 1136
    :cond_3
    iget v2, p1, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 1137
    iget v0, p1, Lcom/google/b/f/b/a/bt;->e:I

    iget v1, p0, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/b/f/b/a/bv;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bv;->e:I

    .line 1139
    :cond_4
    iget-object v0, p1, Lcom/google/b/f/b/a/bt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 1127
    goto :goto_1

    :cond_6
    move v2, v1

    .line 1130
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1133
    goto :goto_3

    :cond_8
    move v0, v1

    .line 1136
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1074
    new-instance v2, Lcom/google/b/f/b/a/bt;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/bt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/bv;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/bv;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/bt;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/bv;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/bt;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/bv;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/bt;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/bv;->e:I

    iput v1, v2, Lcom/google/b/f/b/a/bt;->e:I

    iput v0, v2, Lcom/google/b/f/b/a/bt;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1074
    check-cast p1, Lcom/google/b/f/b/a/bt;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/bv;->a(Lcom/google/b/f/b/a/bt;)Lcom/google/b/f/b/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1144
    const/4 v0, 0x1

    return v0
.end method
