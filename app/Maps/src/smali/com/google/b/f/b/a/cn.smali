.class public final enum Lcom/google/b/f/b/a/cn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/cn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/cn;

.field public static final enum b:Lcom/google/b/f/b/a/cn;

.field public static final enum c:Lcom/google/b/f/b/a/cn;

.field private static final synthetic e:[Lcom/google/b/f/b/a/cn;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    new-instance v0, Lcom/google/b/f/b/a/cn;

    const-string v1, "UP"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/b/a/cn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cn;->a:Lcom/google/b/f/b/a/cn;

    .line 135
    new-instance v0, Lcom/google/b/f/b/a/cn;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/b/a/cn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cn;->b:Lcom/google/b/f/b/a/cn;

    .line 139
    new-instance v0, Lcom/google/b/f/b/a/cn;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/b/a/cn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/cn;->c:Lcom/google/b/f/b/a/cn;

    .line 126
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/f/b/a/cn;

    sget-object v1, Lcom/google/b/f/b/a/cn;->a:Lcom/google/b/f/b/a/cn;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/b/a/cn;->b:Lcom/google/b/f/b/a/cn;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/b/a/cn;->c:Lcom/google/b/f/b/a/cn;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/f/b/a/cn;->e:[Lcom/google/b/f/b/a/cn;

    .line 174
    new-instance v0, Lcom/google/b/f/b/a/co;

    invoke-direct {v0}, Lcom/google/b/f/b/a/co;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/google/b/f/b/a/cn;->d:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/cn;
    .locals 1

    .prologue
    .line 161
    packed-switch p0, :pswitch_data_0

    .line 165
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/cn;->a:Lcom/google/b/f/b/a/cn;

    goto :goto_0

    .line 163
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/cn;->b:Lcom/google/b/f/b/a/cn;

    goto :goto_0

    .line 164
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/cn;->c:Lcom/google/b/f/b/a/cn;

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/cn;
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/b/f/b/a/cn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cn;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/cn;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/b/f/b/a/cn;->e:[Lcom/google/b/f/b/a/cn;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/cn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/cn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/b/f/b/a/cn;->d:I

    return v0
.end method
