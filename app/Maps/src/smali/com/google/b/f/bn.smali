.class public final enum Lcom/google/b/f/bn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/bn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/bn;

.field public static final enum b:Lcom/google/b/f/bn;

.field public static final enum c:Lcom/google/b/f/bn;

.field public static final enum d:Lcom/google/b/f/bn;

.field public static final enum e:Lcom/google/b/f/bn;

.field public static final enum f:Lcom/google/b/f/bn;

.field public static final enum g:Lcom/google/b/f/bn;

.field public static final enum h:Lcom/google/b/f/bn;

.field private static final synthetic j:[Lcom/google/b/f/bn;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1247
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "KEYBOARD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->a:Lcom/google/b/f/bn;

    .line 1251
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "PASTE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->b:Lcom/google/b/f/bn;

    .line 1255
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "ON_SCREEN_KEYBOARD"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->c:Lcom/google/b/f/bn;

    .line 1259
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "IME"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->d:Lcom/google/b/f/bn;

    .line 1263
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "QUERY_BUILDER"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->e:Lcom/google/b/f/bn;

    .line 1267
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "SPEECH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->f:Lcom/google/b/f/bn;

    .line 1271
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "HANDWRITING"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->g:Lcom/google/b/f/bn;

    .line 1275
    new-instance v0, Lcom/google/b/f/bn;

    const-string v1, "TAB"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bn;->h:Lcom/google/b/f/bn;

    .line 1242
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/b/f/bn;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/b/f/bn;->a:Lcom/google/b/f/bn;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/b/f/bn;->b:Lcom/google/b/f/bn;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/bn;->c:Lcom/google/b/f/bn;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/bn;->d:Lcom/google/b/f/bn;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/f/bn;->e:Lcom/google/b/f/bn;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/b/f/bn;->f:Lcom/google/b/f/bn;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/f/bn;->g:Lcom/google/b/f/bn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/f/bn;->h:Lcom/google/b/f/bn;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/f/bn;->j:[Lcom/google/b/f/bn;

    .line 1335
    new-instance v0, Lcom/google/b/f/bo;

    invoke-direct {v0}, Lcom/google/b/f/bo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1344
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1345
    iput p3, p0, Lcom/google/b/f/bn;->i:I

    .line 1346
    return-void
.end method

.method public static a(I)Lcom/google/b/f/bn;
    .locals 1

    .prologue
    .line 1317
    packed-switch p0, :pswitch_data_0

    .line 1326
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1318
    :pswitch_0
    sget-object v0, Lcom/google/b/f/bn;->a:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1319
    :pswitch_1
    sget-object v0, Lcom/google/b/f/bn;->b:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1320
    :pswitch_2
    sget-object v0, Lcom/google/b/f/bn;->c:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1321
    :pswitch_3
    sget-object v0, Lcom/google/b/f/bn;->d:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1322
    :pswitch_4
    sget-object v0, Lcom/google/b/f/bn;->e:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1323
    :pswitch_5
    sget-object v0, Lcom/google/b/f/bn;->f:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1324
    :pswitch_6
    sget-object v0, Lcom/google/b/f/bn;->g:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1325
    :pswitch_7
    sget-object v0, Lcom/google/b/f/bn;->h:Lcom/google/b/f/bn;

    goto :goto_0

    .line 1317
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/bn;
    .locals 1

    .prologue
    .line 1242
    const-class v0, Lcom/google/b/f/bn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bn;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/bn;
    .locals 1

    .prologue
    .line 1242
    sget-object v0, Lcom/google/b/f/bn;->j:[Lcom/google/b/f/bn;

    invoke-virtual {v0}, [Lcom/google/b/f/bn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/bn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1313
    iget v0, p0, Lcom/google/b/f/bn;->i:I

    return v0
.end method
