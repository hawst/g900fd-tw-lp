.class public final Lcom/google/b/f/co;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/cp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/cm;",
        "Lcom/google/b/f/co;",
        ">;",
        "Lcom/google/b/f/cp;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/n/ao;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/google/b/f/cm;->f:Lcom/google/b/f/cm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 494
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/f/co;->e:I

    .line 526
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    .line 592
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    .line 379
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/cm;)Lcom/google/b/f/co;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 425
    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 447
    :goto_0
    return-object p0

    .line 426
    :cond_0
    iget v2, p1, Lcom/google/b/f/cm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 427
    iget v2, p1, Lcom/google/b/f/cm;->b:I

    iget v3, p0, Lcom/google/b/f/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/co;->a:I

    iput v2, p0, Lcom/google/b/f/co;->b:I

    .line 429
    :cond_1
    iget v2, p1, Lcom/google/b/f/cm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 430
    iget v2, p1, Lcom/google/b/f/cm;->c:I

    iget v3, p0, Lcom/google/b/f/co;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/co;->a:I

    iput v2, p0, Lcom/google/b/f/co;->e:I

    .line 432
    :cond_2
    iget-object v2, p1, Lcom/google/b/f/cm;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 433
    iget-object v2, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 434
    iget-object v2, p1, Lcom/google/b/f/cm;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    .line 435
    iget v2, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/b/f/co;->a:I

    .line 442
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/b/f/cm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 443
    iget-object v0, p0, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/b/f/cm;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 444
    iget v0, p0, Lcom/google/b/f/co;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/co;->a:I

    .line 446
    :cond_4
    iget-object v0, p1, Lcom/google/b/f/cm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 426
    goto :goto_1

    :cond_6
    move v2, v1

    .line 429
    goto :goto_2

    .line 437
    :cond_7
    invoke-virtual {p0}, Lcom/google/b/f/co;->c()V

    .line 438
    iget-object v2, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/cm;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_8
    move v0, v1

    .line 442
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 370
    new-instance v2, Lcom/google/b/f/cm;

    invoke-direct {v2, p0}, Lcom/google/b/f/cm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v4, p0, Lcom/google/b/f/co;->b:I

    iput v4, v2, Lcom/google/b/f/cm;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/b/f/co;->e:I

    iput v4, v2, Lcom/google/b/f/cm;->c:I

    iget v4, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/b/f/co;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/b/f/cm;->d:Ljava/util/List;

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v3, v2, Lcom/google/b/f/cm;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/b/f/cm;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 370
    check-cast p1, Lcom/google/b/f/cm;

    invoke-virtual {p0, p1}, Lcom/google/b/f/co;->a(Lcom/google/b/f/cm;)Lcom/google/b/f/co;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 451
    iget v0, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/g;->d()Lcom/google/b/f/g;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/g;

    invoke-virtual {v0}, Lcom/google/b/f/g;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 457
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 451
    goto :goto_0

    :cond_1
    move v0, v2

    .line 457
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 528
    iget v0, p0, Lcom/google/b/f/co;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 529
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/co;->c:Ljava/util/List;

    .line 530
    iget v0, p0, Lcom/google/b/f/co;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/co;->a:I

    .line 532
    :cond_0
    return-void
.end method
