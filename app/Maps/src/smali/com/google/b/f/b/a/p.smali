.class public final Lcom/google/b/f/b/a/p;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/s;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/p;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/b/f/b/a/p;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1731
    new-instance v0, Lcom/google/b/f/b/a/q;

    invoke-direct {v0}, Lcom/google/b/f/b/a/q;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/p;->PARSER:Lcom/google/n/ax;

    .line 1874
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/p;->g:Lcom/google/n/aw;

    .line 2163
    new-instance v0, Lcom/google/b/f/b/a/p;

    invoke-direct {v0}, Lcom/google/b/f/b/a/p;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/p;->d:Lcom/google/b/f/b/a/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1680
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1831
    iput-byte v0, p0, Lcom/google/b/f/b/a/p;->e:B

    .line 1853
    iput v0, p0, Lcom/google/b/f/b/a/p;->f:I

    .line 1681
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    .line 1682
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    .line 1683
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1689
    invoke-direct {p0}, Lcom/google/b/f/b/a/p;-><init>()V

    .line 1690
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1694
    const/4 v0, 0x0

    .line 1695
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1696
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1697
    sparse-switch v3, :sswitch_data_0

    .line 1702
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1704
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1700
    goto :goto_0

    .line 1709
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1710
    iget v4, p0, Lcom/google/b/f/b/a/p;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/p;->a:I

    .line 1711
    iput-object v3, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1722
    :catch_0
    move-exception v0

    .line 1723
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/p;->au:Lcom/google/n/bn;

    throw v0

    .line 1715
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1716
    iget v4, p0, Lcom/google/b/f/b/a/p;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/p;->a:I

    .line 1717
    iput-object v3, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1724
    :catch_1
    move-exception v0

    .line 1725
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1726
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1728
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->au:Lcom/google/n/bn;

    .line 1729
    return-void

    .line 1697
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1678
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1831
    iput-byte v0, p0, Lcom/google/b/f/b/a/p;->e:B

    .line 1853
    iput v0, p0, Lcom/google/b/f/b/a/p;->f:I

    .line 1679
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/p;
    .locals 1

    .prologue
    .line 2166
    sget-object v0, Lcom/google/b/f/b/a/p;->d:Lcom/google/b/f/b/a/p;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/r;
    .locals 1

    .prologue
    .line 1936
    new-instance v0, Lcom/google/b/f/b/a/r;

    invoke-direct {v0}, Lcom/google/b/f/b/a/r;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1743
    sget-object v0, Lcom/google/b/f/b/a/p;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 1843
    invoke-virtual {p0}, Lcom/google/b/f/b/a/p;->c()I

    .line 1844
    iget v0, p0, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 1845
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1847
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1848
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1850
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1851
    return-void

    .line 1845
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1848
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1833
    iget-byte v1, p0, Lcom/google/b/f/b/a/p;->e:B

    .line 1834
    if-ne v1, v0, :cond_0

    .line 1838
    :goto_0
    return v0

    .line 1835
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1837
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/p;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1855
    iget v0, p0, Lcom/google/b/f/b/a/p;->f:I

    .line 1856
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1869
    :goto_0
    return v0

    .line 1859
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1861
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1863
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1865
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1867
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/b/a/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1868
    iput v0, p0, Lcom/google/b/f/b/a/p;->f:I

    goto :goto_0

    .line 1861
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1865
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1672
    invoke-static {}, Lcom/google/b/f/b/a/p;->newBuilder()Lcom/google/b/f/b/a/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/r;->a(Lcom/google/b/f/b/a/p;)Lcom/google/b/f/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1672
    invoke-static {}, Lcom/google/b/f/b/a/p;->newBuilder()Lcom/google/b/f/b/a/r;

    move-result-object v0

    return-object v0
.end method
