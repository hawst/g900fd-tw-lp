.class public final Lcom/google/b/f/bi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/bf;",
        "Lcom/google/b/f/bi;",
        ">;",
        "Lcom/google/b/f/cf;"
    }
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field private D:I

.field private E:I

.field private F:Lcom/google/n/ao;

.field private G:I

.field private H:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:Ljava/lang/Object;

.field private P:Lcom/google/n/ao;

.field private Q:I

.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Lcom/google/n/ao;

.field public e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:Ljava/lang/Object;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/lang/Object;

.field private z:I


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5206
    sget-object v0, Lcom/google/b/f/bf;->Q:Lcom/google/b/f/bf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5665
    iput v2, p0, Lcom/google/b/f/bi;->u:I

    .line 5701
    iput v1, p0, Lcom/google/b/f/bi;->v:I

    .line 5737
    iput v1, p0, Lcom/google/b/f/bi;->w:I

    .line 5805
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bi;->c:Ljava/lang/Object;

    .line 5881
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bi;->y:Ljava/lang/Object;

    .line 5957
    iput v1, p0, Lcom/google/b/f/bi;->z:I

    .line 5994
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    .line 6067
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bi;->d:Lcom/google/n/ao;

    .line 6126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bi;->e:Ljava/lang/Object;

    .line 6202
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    .line 6262
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    .line 6399
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    .line 6823
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bi;->F:Lcom/google/n/ao;

    .line 7202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bi;->t:Ljava/lang/Object;

    .line 7374
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bi;->O:Ljava/lang/Object;

    .line 7450
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bi;->P:Lcom/google/n/ao;

    .line 7509
    iput v2, p0, Lcom/google/b/f/bi;->Q:I

    .line 5207
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/bf;)Lcom/google/b/f/bi;
    .locals 8

    .prologue
    const/high16 v7, 0x10000

    const v6, 0x8000

    const/high16 v5, -0x80000000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5480
    invoke-static {}, Lcom/google/b/f/bf;->d()Lcom/google/b/f/bf;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 5637
    :goto_0
    return-object p0

    .line 5481
    :cond_0
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 5482
    iget v0, p1, Lcom/google/b/f/bf;->c:I

    invoke-static {v0}, Lcom/google/b/f/cd;->a(I)Lcom/google/b/f/cd;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/b/f/cd;->a:Lcom/google/b/f/cd;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 5481
    goto :goto_1

    .line 5482
    :cond_3
    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iget v0, v0, Lcom/google/b/f/cd;->f:I

    iput v0, p0, Lcom/google/b/f/bi;->u:I

    .line 5484
    :cond_4
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 5485
    iget v0, p1, Lcom/google/b/f/bf;->d:I

    invoke-static {v0}, Lcom/google/b/f/bp;->a(I)Lcom/google/b/f/bp;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/b/f/bp;->a:Lcom/google/b/f/bp;

    :cond_5
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    .line 5484
    goto :goto_2

    .line 5485
    :cond_7
    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iget v0, v0, Lcom/google/b/f/bp;->d:I

    iput v0, p0, Lcom/google/b/f/bi;->v:I

    .line 5487
    :cond_8
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_c

    .line 5488
    iget v0, p1, Lcom/google/b/f/bf;->e:I

    invoke-static {v0}, Lcom/google/b/f/bt;->a(I)Lcom/google/b/f/bt;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/b/f/bt;->a:Lcom/google/b/f/bt;

    :cond_9
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v0, v2

    .line 5487
    goto :goto_3

    .line 5488
    :cond_b
    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iget v0, v0, Lcom/google/b/f/bt;->d:I

    iput v0, p0, Lcom/google/b/f/bi;->w:I

    .line 5490
    :cond_c
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_33

    move v0, v1

    :goto_4
    if-eqz v0, :cond_d

    .line 5491
    iget v0, p1, Lcom/google/b/f/bf;->f:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->x:I

    .line 5493
    :cond_d
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_34

    move v0, v1

    :goto_5
    if-eqz v0, :cond_e

    .line 5494
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5495
    iget-object v0, p1, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/bi;->c:Ljava/lang/Object;

    .line 5498
    :cond_e
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_35

    move v0, v1

    :goto_6
    if-eqz v0, :cond_f

    .line 5499
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5500
    iget-object v0, p1, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/bi;->y:Ljava/lang/Object;

    .line 5503
    :cond_f
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_36

    move v0, v1

    :goto_7
    if-eqz v0, :cond_11

    .line 5504
    iget v0, p1, Lcom/google/b/f/bf;->i:I

    invoke-static {v0}, Lcom/google/b/f/br;->a(I)Lcom/google/b/f/br;

    move-result-object v0

    if-nez v0, :cond_10

    sget-object v0, Lcom/google/b/f/br;->a:Lcom/google/b/f/br;

    :cond_10
    invoke-virtual {p0, v0}, Lcom/google/b/f/bi;->a(Lcom/google/b/f/br;)Lcom/google/b/f/bi;

    .line 5506
    :cond_11
    iget-object v0, p1, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 5507
    iget-object v0, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 5508
    iget-object v0, p1, Lcom/google/b/f/bf;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    .line 5509
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5516
    :cond_12
    :goto_8
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_39

    move v0, v1

    :goto_9
    if-eqz v0, :cond_13

    .line 5517
    iget-object v0, p0, Lcom/google/b/f/bi;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5518
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5520
    :cond_13
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_3a

    move v0, v1

    :goto_a
    if-eqz v0, :cond_14

    .line 5521
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5522
    iget-object v0, p1, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/bi;->e:Ljava/lang/Object;

    .line 5525
    :cond_14
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_3b

    move v0, v1

    :goto_b
    if-eqz v0, :cond_15

    .line 5526
    iget-object v0, p0, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5527
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5529
    :cond_15
    iget-object v0, p1, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 5530
    iget-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 5531
    iget-object v0, p1, Lcom/google/b/f/bf;->n:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    .line 5532
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5539
    :cond_16
    :goto_c
    iget-object v0, p1, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_17

    .line 5540
    iget-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 5541
    iget-object v0, p1, Lcom/google/b/f/bf;->o:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    .line 5542
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5549
    :cond_17
    :goto_d
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_3f

    move v0, v1

    :goto_e
    if-eqz v0, :cond_18

    .line 5550
    iget-boolean v0, p1, Lcom/google/b/f/bf;->p:Z

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/bi;->C:Z

    .line 5552
    :cond_18
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_40

    move v0, v1

    :goto_f
    if-eqz v0, :cond_19

    .line 5553
    iget v0, p1, Lcom/google/b/f/bf;->q:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->D:I

    .line 5555
    :cond_19
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_41

    move v0, v1

    :goto_10
    if-eqz v0, :cond_1a

    .line 5556
    iget-boolean v0, p1, Lcom/google/b/f/bf;->r:Z

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/bi;->h:Z

    .line 5558
    :cond_1a
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_42

    move v0, v1

    :goto_11
    if-eqz v0, :cond_1b

    .line 5559
    iget v0, p1, Lcom/google/b/f/bf;->s:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/2addr v3, v7

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->i:I

    .line 5561
    :cond_1b
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_43

    move v0, v1

    :goto_12
    if-eqz v0, :cond_1c

    .line 5562
    iget v0, p1, Lcom/google/b/f/bf;->t:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->j:I

    .line 5564
    :cond_1c
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_44

    move v0, v1

    :goto_13
    if-eqz v0, :cond_1d

    .line 5565
    iget v0, p1, Lcom/google/b/f/bf;->u:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x40000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->k:I

    .line 5567
    :cond_1d
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_45

    move v0, v1

    :goto_14
    if-eqz v0, :cond_1e

    .line 5568
    iget v0, p1, Lcom/google/b/f/bf;->v:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->l:I

    .line 5570
    :cond_1e
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_46

    move v0, v1

    :goto_15
    if-eqz v0, :cond_1f

    .line 5571
    iget v0, p1, Lcom/google/b/f/bf;->w:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->E:I

    .line 5573
    :cond_1f
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_47

    move v0, v1

    :goto_16
    if-eqz v0, :cond_20

    .line 5574
    iget v0, p1, Lcom/google/b/f/bf;->x:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->m:I

    .line 5576
    :cond_20
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_48

    move v0, v1

    :goto_17
    if-eqz v0, :cond_21

    .line 5577
    iget-object v0, p0, Lcom/google/b/f/bi;->F:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5578
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v3, 0x400000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5580
    :cond_21
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_49

    move v0, v1

    :goto_18
    if-eqz v0, :cond_22

    .line 5581
    iget v0, p1, Lcom/google/b/f/bf;->z:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x800000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->G:I

    .line 5583
    :cond_22
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_4a

    move v0, v1

    :goto_19
    if-eqz v0, :cond_23

    .line 5584
    iget v0, p1, Lcom/google/b/f/bf;->A:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x1000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->H:I

    .line 5586
    :cond_23
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_4b

    move v0, v1

    :goto_1a
    if-eqz v0, :cond_24

    .line 5587
    iget v0, p1, Lcom/google/b/f/bf;->B:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x2000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->J:I

    .line 5589
    :cond_24
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v0, v3

    const/high16 v3, 0x800000

    if-ne v0, v3, :cond_4c

    move v0, v1

    :goto_1b
    if-eqz v0, :cond_25

    .line 5590
    iget v0, p1, Lcom/google/b/f/bf;->C:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x4000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->K:I

    .line 5592
    :cond_25
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_4d

    move v0, v1

    :goto_1c
    if-eqz v0, :cond_26

    .line 5593
    iget v0, p1, Lcom/google/b/f/bf;->D:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x8000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->n:I

    .line 5595
    :cond_26
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    const/high16 v3, 0x2000000

    if-ne v0, v3, :cond_4e

    move v0, v1

    :goto_1d
    if-eqz v0, :cond_27

    .line 5596
    iget v0, p1, Lcom/google/b/f/bf;->E:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->o:I

    .line 5598
    :cond_27
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v0, v3

    const/high16 v3, 0x4000000

    if-ne v0, v3, :cond_4f

    move v0, v1

    :goto_1e
    if-eqz v0, :cond_28

    .line 5599
    iget v0, p1, Lcom/google/b/f/bf;->F:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->p:I

    .line 5601
    :cond_28
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v0, v3

    const/high16 v3, 0x8000000

    if-ne v0, v3, :cond_50

    move v0, v1

    :goto_1f
    if-eqz v0, :cond_29

    .line 5602
    iget v0, p1, Lcom/google/b/f/bf;->G:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->q:I

    .line 5604
    :cond_29
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_51

    move v0, v1

    :goto_20
    if-eqz v0, :cond_2a

    .line 5605
    iget v0, p1, Lcom/google/b/f/bf;->H:I

    iget v3, p0, Lcom/google/b/f/bi;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/b/f/bi;->a:I

    iput v0, p0, Lcom/google/b/f/bi;->r:I

    .line 5607
    :cond_2a
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000000

    if-ne v0, v3, :cond_52

    move v0, v1

    :goto_21
    if-eqz v0, :cond_2b

    .line 5608
    iget v0, p1, Lcom/google/b/f/bf;->I:I

    iget v3, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/bi;->b:I

    iput v0, p0, Lcom/google/b/f/bi;->s:I

    .line 5610
    :cond_2b
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_53

    move v0, v1

    :goto_22
    if-eqz v0, :cond_2c

    .line 5611
    iget v0, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/bi;->b:I

    .line 5612
    iget-object v0, p1, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/bi;->t:Ljava/lang/Object;

    .line 5615
    :cond_2c
    iget v0, p1, Lcom/google/b/f/bf;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_54

    move v0, v1

    :goto_23
    if-eqz v0, :cond_2d

    .line 5616
    iget v0, p1, Lcom/google/b/f/bf;->K:I

    iget v3, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/bi;->b:I

    iput v0, p0, Lcom/google/b/f/bi;->L:I

    .line 5618
    :cond_2d
    iget v0, p1, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_55

    move v0, v1

    :goto_24
    if-eqz v0, :cond_2e

    .line 5619
    iget v0, p1, Lcom/google/b/f/bf;->L:I

    iget v3, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/bi;->b:I

    iput v0, p0, Lcom/google/b/f/bi;->M:I

    .line 5621
    :cond_2e
    iget v0, p1, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_56

    move v0, v1

    :goto_25
    if-eqz v0, :cond_2f

    .line 5622
    iget v0, p1, Lcom/google/b/f/bf;->M:I

    iget v3, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/bi;->b:I

    iput v0, p0, Lcom/google/b/f/bi;->N:I

    .line 5624
    :cond_2f
    iget v0, p1, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_57

    move v0, v1

    :goto_26
    if-eqz v0, :cond_30

    .line 5625
    iget v0, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/bi;->b:I

    .line 5626
    iget-object v0, p1, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/bi;->O:Ljava/lang/Object;

    .line 5629
    :cond_30
    iget v0, p1, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_58

    move v0, v1

    :goto_27
    if-eqz v0, :cond_31

    .line 5630
    iget-object v0, p0, Lcom/google/b/f/bi;->P:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5631
    iget v0, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/bi;->b:I

    .line 5633
    :cond_31
    iget v0, p1, Lcom/google/b/f/bf;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_59

    move v0, v1

    :goto_28
    if-eqz v0, :cond_5b

    .line 5634
    iget v0, p1, Lcom/google/b/f/bf;->P:I

    invoke-static {v0}, Lcom/google/b/f/bv;->a(I)Lcom/google/b/f/bv;

    move-result-object v0

    if-nez v0, :cond_32

    sget-object v0, Lcom/google/b/f/bv;->a:Lcom/google/b/f/bv;

    :cond_32
    if-nez v0, :cond_5a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_33
    move v0, v2

    .line 5490
    goto/16 :goto_4

    :cond_34
    move v0, v2

    .line 5493
    goto/16 :goto_5

    :cond_35
    move v0, v2

    .line 5498
    goto/16 :goto_6

    :cond_36
    move v0, v2

    .line 5503
    goto/16 :goto_7

    .line 5511
    :cond_37
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-eq v0, v3, :cond_38

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5512
    :cond_38
    iget-object v0, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/bf;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_39
    move v0, v2

    .line 5516
    goto/16 :goto_9

    :cond_3a
    move v0, v2

    .line 5520
    goto/16 :goto_a

    :cond_3b
    move v0, v2

    .line 5525
    goto/16 :goto_b

    .line 5534
    :cond_3c
    invoke-virtual {p0}, Lcom/google/b/f/bi;->c()V

    .line 5535
    iget-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/bf;->n:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c

    .line 5544
    :cond_3d
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-eq v0, v3, :cond_3e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5545
    :cond_3e
    iget-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/bf;->o:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_d

    :cond_3f
    move v0, v2

    .line 5549
    goto/16 :goto_e

    :cond_40
    move v0, v2

    .line 5552
    goto/16 :goto_f

    :cond_41
    move v0, v2

    .line 5555
    goto/16 :goto_10

    :cond_42
    move v0, v2

    .line 5558
    goto/16 :goto_11

    :cond_43
    move v0, v2

    .line 5561
    goto/16 :goto_12

    :cond_44
    move v0, v2

    .line 5564
    goto/16 :goto_13

    :cond_45
    move v0, v2

    .line 5567
    goto/16 :goto_14

    :cond_46
    move v0, v2

    .line 5570
    goto/16 :goto_15

    :cond_47
    move v0, v2

    .line 5573
    goto/16 :goto_16

    :cond_48
    move v0, v2

    .line 5576
    goto/16 :goto_17

    :cond_49
    move v0, v2

    .line 5580
    goto/16 :goto_18

    :cond_4a
    move v0, v2

    .line 5583
    goto/16 :goto_19

    :cond_4b
    move v0, v2

    .line 5586
    goto/16 :goto_1a

    :cond_4c
    move v0, v2

    .line 5589
    goto/16 :goto_1b

    :cond_4d
    move v0, v2

    .line 5592
    goto/16 :goto_1c

    :cond_4e
    move v0, v2

    .line 5595
    goto/16 :goto_1d

    :cond_4f
    move v0, v2

    .line 5598
    goto/16 :goto_1e

    :cond_50
    move v0, v2

    .line 5601
    goto/16 :goto_1f

    :cond_51
    move v0, v2

    .line 5604
    goto/16 :goto_20

    :cond_52
    move v0, v2

    .line 5607
    goto/16 :goto_21

    :cond_53
    move v0, v2

    .line 5610
    goto/16 :goto_22

    :cond_54
    move v0, v2

    .line 5615
    goto/16 :goto_23

    :cond_55
    move v0, v2

    .line 5618
    goto/16 :goto_24

    :cond_56
    move v0, v2

    .line 5621
    goto/16 :goto_25

    :cond_57
    move v0, v2

    .line 5624
    goto/16 :goto_26

    :cond_58
    move v0, v2

    .line 5629
    goto/16 :goto_27

    :cond_59
    move v0, v2

    .line 5633
    goto/16 :goto_28

    .line 5634
    :cond_5a
    iget v1, p0, Lcom/google/b/f/bi;->b:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/b/f/bi;->b:I

    iget v0, v0, Lcom/google/b/f/bv;->f:I

    iput v0, p0, Lcom/google/b/f/bi;->Q:I

    .line 5636
    :cond_5b
    iget-object v0, p1, Lcom/google/b/f/bf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/b/f/br;)Lcom/google/b/f/bi;
    .locals 1

    .prologue
    .line 5975
    if-nez p1, :cond_0

    .line 5976
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5978
    :cond_0
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 5979
    iget v0, p1, Lcom/google/b/f/br;->u:I

    iput v0, p0, Lcom/google/b/f/bi;->z:I

    .line 5981
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x10000

    const v10, 0x8000

    const/high16 v9, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5198
    new-instance v3, Lcom/google/b/f/bf;

    invoke-direct {v3, p0}, Lcom/google/b/f/bf;-><init>(Lcom/google/n/v;)V

    iget v4, p0, Lcom/google/b/f/bi;->a:I

    iget v5, p0, Lcom/google/b/f/bi;->b:I

    and-int/lit8 v0, v4, 0x1

    if-ne v0, v1, :cond_27

    move v0, v1

    :goto_0
    iget v6, p0, Lcom/google/b/f/bi;->u:I

    iput v6, v3, Lcom/google/b/f/bf;->c:I

    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v6, p0, Lcom/google/b/f/bi;->v:I

    iput v6, v3, Lcom/google/b/f/bf;->d:I

    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v6, p0, Lcom/google/b/f/bi;->w:I

    iput v6, v3, Lcom/google/b/f/bf;->e:I

    and-int/lit8 v6, v4, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v6, p0, Lcom/google/b/f/bi;->x:I

    iput v6, v3, Lcom/google/b/f/bf;->f:I

    and-int/lit8 v6, v4, 0x10

    const/16 v7, 0x10

    if-ne v6, v7, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v6, p0, Lcom/google/b/f/bi;->c:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/bf;->g:Ljava/lang/Object;

    and-int/lit8 v6, v4, 0x20

    const/16 v7, 0x20

    if-ne v6, v7, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v6, p0, Lcom/google/b/f/bi;->y:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/bf;->h:Ljava/lang/Object;

    and-int/lit8 v6, v4, 0x40

    const/16 v7, 0x40

    if-ne v6, v7, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v6, p0, Lcom/google/b/f/bi;->z:I

    iput v6, v3, Lcom/google/b/f/bf;->i:I

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, 0x80

    const/16 v7, 0x80

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, -0x81

    iput v6, p0, Lcom/google/b/f/bi;->a:I

    :cond_6
    iget-object v6, p0, Lcom/google/b/f/bi;->A:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/bf;->j:Ljava/util/List;

    and-int/lit16 v6, v4, 0x100

    const/16 v7, 0x100

    if-ne v6, v7, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v6, v3, Lcom/google/b/f/bf;->k:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/bi;->d:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/bi;->d:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/lit16 v6, v4, 0x200

    const/16 v7, 0x200

    if-ne v6, v7, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v6, p0, Lcom/google/b/f/bi;->e:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/bf;->l:Ljava/lang/Object;

    and-int/lit16 v6, v4, 0x400

    const/16 v7, 0x400

    if-ne v6, v7, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v6, v3, Lcom/google/b/f/bf;->m:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_a

    iget-object v6, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, -0x801

    iput v6, p0, Lcom/google/b/f/bi;->a:I

    :cond_a
    iget-object v6, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/bf;->n:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, 0x1000

    const/16 v7, 0x1000

    if-ne v6, v7, :cond_b

    iget-object v6, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v6, v6, -0x1001

    iput v6, p0, Lcom/google/b/f/bi;->a:I

    :cond_b
    iget-object v6, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/bf;->o:Ljava/util/List;

    and-int/lit16 v6, v4, 0x2000

    const/16 v7, 0x2000

    if-ne v6, v7, :cond_c

    or-int/lit16 v0, v0, 0x400

    :cond_c
    iget-boolean v6, p0, Lcom/google/b/f/bi;->C:Z

    iput-boolean v6, v3, Lcom/google/b/f/bf;->p:Z

    and-int/lit16 v6, v4, 0x4000

    const/16 v7, 0x4000

    if-ne v6, v7, :cond_d

    or-int/lit16 v0, v0, 0x800

    :cond_d
    iget v6, p0, Lcom/google/b/f/bi;->D:I

    iput v6, v3, Lcom/google/b/f/bf;->q:I

    and-int v6, v4, v10

    if-ne v6, v10, :cond_e

    or-int/lit16 v0, v0, 0x1000

    :cond_e
    iget-boolean v6, p0, Lcom/google/b/f/bi;->h:Z

    iput-boolean v6, v3, Lcom/google/b/f/bf;->r:Z

    and-int v6, v4, v11

    if-ne v6, v11, :cond_f

    or-int/lit16 v0, v0, 0x2000

    :cond_f
    iget v6, p0, Lcom/google/b/f/bi;->i:I

    iput v6, v3, Lcom/google/b/f/bf;->s:I

    const/high16 v6, 0x20000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_10

    or-int/lit16 v0, v0, 0x4000

    :cond_10
    iget v6, p0, Lcom/google/b/f/bi;->j:I

    iput v6, v3, Lcom/google/b/f/bf;->t:I

    const/high16 v6, 0x40000

    and-int/2addr v6, v4

    const/high16 v7, 0x40000

    if-ne v6, v7, :cond_11

    or-int/2addr v0, v10

    :cond_11
    iget v6, p0, Lcom/google/b/f/bi;->k:I

    iput v6, v3, Lcom/google/b/f/bf;->u:I

    const/high16 v6, 0x80000

    and-int/2addr v6, v4

    const/high16 v7, 0x80000

    if-ne v6, v7, :cond_12

    or-int/2addr v0, v11

    :cond_12
    iget v6, p0, Lcom/google/b/f/bi;->l:I

    iput v6, v3, Lcom/google/b/f/bf;->v:I

    const/high16 v6, 0x100000

    and-int/2addr v6, v4

    const/high16 v7, 0x100000

    if-ne v6, v7, :cond_13

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    :cond_13
    iget v6, p0, Lcom/google/b/f/bi;->E:I

    iput v6, v3, Lcom/google/b/f/bf;->w:I

    const/high16 v6, 0x200000

    and-int/2addr v6, v4

    const/high16 v7, 0x200000

    if-ne v6, v7, :cond_14

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    :cond_14
    iget v6, p0, Lcom/google/b/f/bi;->m:I

    iput v6, v3, Lcom/google/b/f/bf;->x:I

    const/high16 v6, 0x400000

    and-int/2addr v6, v4

    const/high16 v7, 0x400000

    if-ne v6, v7, :cond_15

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    :cond_15
    iget-object v6, v3, Lcom/google/b/f/bf;->y:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/bi;->F:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/bi;->F:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x800000

    and-int/2addr v6, v4

    const/high16 v7, 0x800000

    if-ne v6, v7, :cond_16

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    :cond_16
    iget v6, p0, Lcom/google/b/f/bi;->G:I

    iput v6, v3, Lcom/google/b/f/bf;->z:I

    const/high16 v6, 0x1000000

    and-int/2addr v6, v4

    const/high16 v7, 0x1000000

    if-ne v6, v7, :cond_17

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    :cond_17
    iget v6, p0, Lcom/google/b/f/bi;->H:I

    iput v6, v3, Lcom/google/b/f/bf;->A:I

    const/high16 v6, 0x2000000

    and-int/2addr v6, v4

    const/high16 v7, 0x2000000

    if-ne v6, v7, :cond_18

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    :cond_18
    iget v6, p0, Lcom/google/b/f/bi;->J:I

    iput v6, v3, Lcom/google/b/f/bf;->B:I

    const/high16 v6, 0x4000000

    and-int/2addr v6, v4

    const/high16 v7, 0x4000000

    if-ne v6, v7, :cond_19

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    :cond_19
    iget v6, p0, Lcom/google/b/f/bi;->K:I

    iput v6, v3, Lcom/google/b/f/bf;->C:I

    const/high16 v6, 0x8000000

    and-int/2addr v6, v4

    const/high16 v7, 0x8000000

    if-ne v6, v7, :cond_1a

    const/high16 v6, 0x1000000

    or-int/2addr v0, v6

    :cond_1a
    iget v6, p0, Lcom/google/b/f/bi;->n:I

    iput v6, v3, Lcom/google/b/f/bf;->D:I

    const/high16 v6, 0x10000000

    and-int/2addr v6, v4

    const/high16 v7, 0x10000000

    if-ne v6, v7, :cond_1b

    const/high16 v6, 0x2000000

    or-int/2addr v0, v6

    :cond_1b
    iget v6, p0, Lcom/google/b/f/bi;->o:I

    iput v6, v3, Lcom/google/b/f/bf;->E:I

    const/high16 v6, 0x20000000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000000

    if-ne v6, v7, :cond_1c

    const/high16 v6, 0x4000000

    or-int/2addr v0, v6

    :cond_1c
    iget v6, p0, Lcom/google/b/f/bi;->p:I

    iput v6, v3, Lcom/google/b/f/bf;->F:I

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v6, v7, :cond_1d

    const/high16 v6, 0x8000000

    or-int/2addr v0, v6

    :cond_1d
    iget v6, p0, Lcom/google/b/f/bi;->q:I

    iput v6, v3, Lcom/google/b/f/bf;->G:I

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_1e

    const/high16 v4, 0x10000000

    or-int/2addr v0, v4

    :cond_1e
    iget v4, p0, Lcom/google/b/f/bi;->r:I

    iput v4, v3, Lcom/google/b/f/bf;->H:I

    and-int/lit8 v4, v5, 0x1

    if-ne v4, v1, :cond_1f

    const/high16 v4, 0x20000000

    or-int/2addr v0, v4

    :cond_1f
    iget v4, p0, Lcom/google/b/f/bi;->s:I

    iput v4, v3, Lcom/google/b/f/bf;->I:I

    and-int/lit8 v4, v5, 0x2

    const/4 v6, 0x2

    if-ne v4, v6, :cond_20

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v0, v4

    :cond_20
    iget-object v4, p0, Lcom/google/b/f/bi;->t:Ljava/lang/Object;

    iput-object v4, v3, Lcom/google/b/f/bf;->J:Ljava/lang/Object;

    and-int/lit8 v4, v5, 0x4

    const/4 v6, 0x4

    if-ne v4, v6, :cond_21

    or-int/2addr v0, v9

    :cond_21
    iget v4, p0, Lcom/google/b/f/bi;->L:I

    iput v4, v3, Lcom/google/b/f/bf;->K:I

    and-int/lit8 v4, v5, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_26

    :goto_1
    iget v4, p0, Lcom/google/b/f/bi;->M:I

    iput v4, v3, Lcom/google/b/f/bf;->L:I

    and-int/lit8 v4, v5, 0x10

    const/16 v6, 0x10

    if-ne v4, v6, :cond_22

    or-int/lit8 v1, v1, 0x2

    :cond_22
    iget v4, p0, Lcom/google/b/f/bi;->N:I

    iput v4, v3, Lcom/google/b/f/bf;->M:I

    and-int/lit8 v4, v5, 0x20

    const/16 v6, 0x20

    if-ne v4, v6, :cond_23

    or-int/lit8 v1, v1, 0x4

    :cond_23
    iget-object v4, p0, Lcom/google/b/f/bi;->O:Ljava/lang/Object;

    iput-object v4, v3, Lcom/google/b/f/bf;->N:Ljava/lang/Object;

    and-int/lit8 v4, v5, 0x40

    const/16 v6, 0x40

    if-ne v4, v6, :cond_24

    or-int/lit8 v1, v1, 0x8

    :cond_24
    iget-object v4, v3, Lcom/google/b/f/bf;->O:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/b/f/bi;->P:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/b/f/bi;->P:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v2, v5, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_25

    or-int/lit8 v1, v1, 0x10

    :cond_25
    iget v2, p0, Lcom/google/b/f/bi;->Q:I

    iput v2, v3, Lcom/google/b/f/bf;->P:I

    iput v0, v3, Lcom/google/b/f/bf;->a:I

    iput v1, v3, Lcom/google/b/f/bf;->b:I

    return-object v3

    :cond_26
    move v1, v2

    goto :goto_1

    :cond_27
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5198
    check-cast p1, Lcom/google/b/f/bf;

    invoke-virtual {p0, p1}, Lcom/google/b/f/bi;->a(Lcom/google/b/f/bf;)Lcom/google/b/f/bi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5641
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 5642
    iget-object v0, p0, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5659
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 5641
    goto :goto_0

    :cond_2
    move v1, v2

    .line 5647
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 5648
    iget-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5647
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 5653
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 5654
    iget-object v0, p0, Lcom/google/b/f/bi;->B:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    invoke-virtual {v0}, Lcom/google/b/f/bx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5653
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 5659
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 6264
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_0

    .line 6265
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/bi;->g:Ljava/util/List;

    .line 6268
    iget v0, p0, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/b/f/bi;->a:I

    .line 6270
    :cond_0
    return-void
.end method
