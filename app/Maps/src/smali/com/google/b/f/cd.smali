.class public final enum Lcom/google/b/f/cd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/cd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/cd;

.field public static final enum b:Lcom/google/b/f/cd;

.field public static final enum c:Lcom/google/b/f/cd;

.field public static final enum d:Lcom/google/b/f/cd;

.field public static final enum e:Lcom/google/b/f/cd;

.field private static final synthetic g:[Lcom/google/b/f/cd;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 808
    new-instance v0, Lcom/google/b/f/cd;

    const-string v1, "NO_STATUS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cd;->a:Lcom/google/b/f/cd;

    .line 812
    new-instance v0, Lcom/google/b/f/cd;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/b/f/cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cd;->b:Lcom/google/b/f/cd;

    .line 816
    new-instance v0, Lcom/google/b/f/cd;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/b/f/cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cd;->c:Lcom/google/b/f/cd;

    .line 820
    new-instance v0, Lcom/google/b/f/cd;

    const-string v1, "DEPRECATED_EXEMPTED"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/b/f/cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cd;->d:Lcom/google/b/f/cd;

    .line 824
    new-instance v0, Lcom/google/b/f/cd;

    const-string v1, "DEPRECATED_INCOMPLETE"

    invoke-direct {v0, v1, v6, v4}, Lcom/google/b/f/cd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/cd;->e:Lcom/google/b/f/cd;

    .line 803
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/f/cd;

    sget-object v1, Lcom/google/b/f/cd;->a:Lcom/google/b/f/cd;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/cd;->b:Lcom/google/b/f/cd;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/cd;->c:Lcom/google/b/f/cd;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/cd;->d:Lcom/google/b/f/cd;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/cd;->e:Lcom/google/b/f/cd;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/f/cd;->g:[Lcom/google/b/f/cd;

    .line 869
    new-instance v0, Lcom/google/b/f/ce;

    invoke-direct {v0}, Lcom/google/b/f/ce;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 878
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 879
    iput p3, p0, Lcom/google/b/f/cd;->f:I

    .line 880
    return-void
.end method

.method public static a(I)Lcom/google/b/f/cd;
    .locals 1

    .prologue
    .line 854
    packed-switch p0, :pswitch_data_0

    .line 860
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 855
    :pswitch_0
    sget-object v0, Lcom/google/b/f/cd;->a:Lcom/google/b/f/cd;

    goto :goto_0

    .line 856
    :pswitch_1
    sget-object v0, Lcom/google/b/f/cd;->b:Lcom/google/b/f/cd;

    goto :goto_0

    .line 857
    :pswitch_2
    sget-object v0, Lcom/google/b/f/cd;->c:Lcom/google/b/f/cd;

    goto :goto_0

    .line 858
    :pswitch_3
    sget-object v0, Lcom/google/b/f/cd;->d:Lcom/google/b/f/cd;

    goto :goto_0

    .line 859
    :pswitch_4
    sget-object v0, Lcom/google/b/f/cd;->e:Lcom/google/b/f/cd;

    goto :goto_0

    .line 854
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/cd;
    .locals 1

    .prologue
    .line 803
    const-class v0, Lcom/google/b/f/cd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cd;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/cd;
    .locals 1

    .prologue
    .line 803
    sget-object v0, Lcom/google/b/f/cd;->g:[Lcom/google/b/f/cd;

    invoke-virtual {v0}, [Lcom/google/b/f/cd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/cd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 850
    iget v0, p0, Lcom/google/b/f/cd;->f:I

    return v0
.end method
