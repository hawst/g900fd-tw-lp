.class public final Lcom/google/b/f/b/a/k;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/n;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/k;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/b/a/k;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 507
    new-instance v0, Lcom/google/b/f/b/a/l;

    invoke-direct {v0}, Lcom/google/b/f/b/a/l;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/k;->PARSER:Lcom/google/n/ax;

    .line 630
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/k;->h:Lcom/google/n/aw;

    .line 910
    new-instance v0, Lcom/google/b/f/b/a/k;

    invoke-direct {v0}, Lcom/google/b/f/b/a/k;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/k;->e:Lcom/google/b/f/b/a/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 433
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 575
    iput-byte v0, p0, Lcom/google/b/f/b/a/k;->f:B

    .line 600
    iput v0, p0, Lcom/google/b/f/b/a/k;->g:I

    .line 434
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 435
    iput-boolean v1, p0, Lcom/google/b/f/b/a/k;->c:Z

    .line 436
    iput-boolean v1, p0, Lcom/google/b/f/b/a/k;->d:Z

    .line 437
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 443
    invoke-direct {p0}, Lcom/google/b/f/b/a/k;-><init>()V

    .line 444
    const/4 v1, 0x0

    .line 446
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 448
    const/4 v0, 0x0

    move v2, v0

    .line 449
    :cond_0
    :goto_0
    if-nez v2, :cond_9

    .line 450
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 451
    sparse-switch v0, :sswitch_data_0

    .line 456
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 453
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 454
    goto :goto_0

    .line 463
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    const/4 v4, 0x1

    if-eq v0, v4, :cond_1

    .line 464
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 465
    or-int/lit8 v1, v1, 0x1

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 495
    :catch_0
    move-exception v0

    .line 496
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 502
    iget-object v1, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 504
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/k;->au:Lcom/google/n/bn;

    throw v0

    .line 471
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 472
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 473
    and-int/lit8 v0, v1, 0x1

    const/4 v5, 0x1

    if-eq v0, v5, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_4

    const/4 v0, -0x1

    :goto_1
    if-lez v0, :cond_3

    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 475
    or-int/lit8 v1, v1, 0x1

    .line 477
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_5

    const/4 v0, -0x1

    :goto_3
    if-lez v0, :cond_6

    .line 478
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 497
    :catch_1
    move-exception v0

    .line 498
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 499
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 473
    :cond_4
    :try_start_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_1

    .line 477
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_3

    .line 480
    :cond_6
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 484
    :sswitch_3
    iget v0, p0, Lcom/google/b/f/b/a/k;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/k;->a:I

    .line 485
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/b/f/b/a/k;->c:Z

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 489
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/b/a/k;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/k;->a:I

    .line 490
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/b/f/b/a/k;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    .line 501
    :cond_9
    and-int/lit8 v0, v1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 502
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 504
    :cond_a
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/k;->au:Lcom/google/n/bn;

    .line 505
    return-void

    .line 451
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 431
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 575
    iput-byte v0, p0, Lcom/google/b/f/b/a/k;->f:B

    .line 600
    iput v0, p0, Lcom/google/b/f/b/a/k;->g:I

    .line 432
    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;
    .locals 1

    .prologue
    .line 695
    invoke-static {}, Lcom/google/b/f/b/a/k;->newBuilder()Lcom/google/b/f/b/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/m;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b/a/k;
    .locals 1

    .prologue
    .line 913
    sget-object v0, Lcom/google/b/f/b/a/k;->e:Lcom/google/b/f/b/a/k;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/m;
    .locals 1

    .prologue
    .line 692
    new-instance v0, Lcom/google/b/f/b/a/m;

    invoke-direct {v0}, Lcom/google/b/f/b/a/m;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    sget-object v0, Lcom/google/b/f/b/a/k;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 587
    invoke-virtual {p0}, Lcom/google/b/f/b/a/k;->c()I

    move v1, v2

    .line 588
    :goto_0
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 588
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 591
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 592
    iget-boolean v0, p0, Lcom/google/b/f/b/a/k;->c:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 594
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 595
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/b/f/b/a/k;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_4

    :goto_2
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 597
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 598
    return-void

    :cond_3
    move v0, v2

    .line 592
    goto :goto_1

    :cond_4
    move v3, v2

    .line 595
    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 577
    iget-byte v1, p0, Lcom/google/b/f/b/a/k;->f:B

    .line 578
    if-ne v1, v0, :cond_0

    .line 582
    :goto_0
    return v0

    .line 579
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 581
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/k;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 602
    iget v0, p0, Lcom/google/b/f/b/a/k;->g:I

    .line 603
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 625
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 608
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 609
    iget-object v0, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 610
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v0

    add-int/2addr v3, v0

    .line 608
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 612
    :cond_1
    add-int/lit8 v0, v3, 0x0

    .line 613
    iget-object v1, p0, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 615
    iget v1, p0, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 616
    iget-boolean v1, p0, Lcom/google/b/f/b/a/k;->c:Z

    .line 617
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 619
    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_3

    .line 620
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/b/f/b/a/k;->d:Z

    .line 621
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 623
    :cond_3
    iget-object v1, p0, Lcom/google/b/f/b/a/k;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 624
    iput v0, p0, Lcom/google/b/f/b/a/k;->g:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 425
    invoke-static {}, Lcom/google/b/f/b/a/k;->newBuilder()Lcom/google/b/f/b/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/m;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 425
    invoke-static {}, Lcom/google/b/f/b/a/k;->newBuilder()Lcom/google/b/f/b/a/m;

    move-result-object v0

    return-object v0
.end method
