.class public final Lcom/google/b/f/b/a/cc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/cf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/cc;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/b/f/b/a/cc;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:J

.field e:J

.field f:J

.field g:Z

.field h:Z

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/google/b/f/b/a/cd;

    invoke-direct {v0}, Lcom/google/b/f/b/a/cd;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/cc;->PARSER:Lcom/google/n/ax;

    .line 295
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/cc;->l:Lcom/google/n/aw;

    .line 697
    new-instance v0, Lcom/google/b/f/b/a/cc;

    invoke-direct {v0}, Lcom/google/b/f/b/a/cc;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/cc;->i:Lcom/google/b/f/b/a/cc;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 217
    iput-byte v1, p0, Lcom/google/b/f/b/a/cc;->j:B

    .line 254
    iput v1, p0, Lcom/google/b/f/b/a/cc;->k:I

    .line 18
    iput v0, p0, Lcom/google/b/f/b/a/cc;->b:I

    .line 19
    iput v0, p0, Lcom/google/b/f/b/a/cc;->c:I

    .line 20
    iput-wide v2, p0, Lcom/google/b/f/b/a/cc;->d:J

    .line 21
    iput-wide v2, p0, Lcom/google/b/f/b/a/cc;->e:J

    .line 22
    iput-wide v2, p0, Lcom/google/b/f/b/a/cc;->f:J

    .line 23
    iput-boolean v0, p0, Lcom/google/b/f/b/a/cc;->g:Z

    .line 24
    iput-boolean v0, p0, Lcom/google/b/f/b/a/cc;->h:Z

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 31
    invoke-direct {p0}, Lcom/google/b/f/b/a/cc;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 37
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 39
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 46
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 52
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/cc;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/cc;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b/a/cc;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 89
    :catch_1
    move-exception v0

    .line 90
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 91
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/b/a/cc;->d:J

    goto :goto_0

    .line 66
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/b/a/cc;->e:J

    goto :goto_0

    .line 71
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/b/f/b/a/cc;->f:J

    goto :goto_0

    .line 76
    :sswitch_6
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/b/f/b/a/cc;->g:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 81
    :sswitch_7
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/b/f/b/a/cc;->h:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 93
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/cc;->au:Lcom/google/n/bn;

    .line 94
    return-void

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 217
    iput-byte v0, p0, Lcom/google/b/f/b/a/cc;->j:B

    .line 254
    iput v0, p0, Lcom/google/b/f/b/a/cc;->k:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;
    .locals 1

    .prologue
    .line 360
    invoke-static {}, Lcom/google/b/f/b/a/cc;->newBuilder()Lcom/google/b/f/b/a/ce;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b/a/cc;
    .locals 1

    .prologue
    .line 700
    sget-object v0, Lcom/google/b/f/b/a/cc;->i:Lcom/google/b/f/b/a/cc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/ce;
    .locals 1

    .prologue
    .line 357
    new-instance v0, Lcom/google/b/f/b/a/ce;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ce;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lcom/google/b/f/b/a/cc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    invoke-virtual {p0}, Lcom/google/b/f/b/a/cc;->c()I

    .line 230
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 231
    iget v0, p0, Lcom/google/b/f/b/a/cc;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 233
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 234
    iget v0, p0, Lcom/google/b/f/b/a/cc;->c:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 236
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 237
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->d:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 239
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 240
    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->e:J

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 242
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 243
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->f:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 245
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 246
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/b/f/b/a/cc;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 248
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 249
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/b/f/b/a/cc;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    :goto_3
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 251
    :cond_6
    iget-object v0, p0, Lcom/google/b/f/b/a/cc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 252
    return-void

    .line 231
    :cond_7
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 234
    :cond_8
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_9
    move v0, v2

    .line 246
    goto :goto_2

    :cond_a
    move v1, v2

    .line 249
    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 219
    iget-byte v1, p0, Lcom/google/b/f/b/a/cc;->j:B

    .line 220
    if-ne v1, v0, :cond_0

    .line 224
    :goto_0
    return v0

    .line 221
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/cc;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 256
    iget v0, p0, Lcom/google/b/f/b/a/cc;->k:I

    .line 257
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 290
    :goto_0
    return v0

    .line 260
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 261
    iget v0, p0, Lcom/google/b/f/b/a/cc;->b:I

    .line 262
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 264
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 265
    iget v3, p0, Lcom/google/b/f/b/a/cc;->c:I

    .line 266
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 268
    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_3

    .line 269
    const/4 v1, 0x3

    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->d:J

    .line 270
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 272
    :cond_3
    iget v1, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 273
    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->e:J

    .line 274
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 276
    :cond_4
    iget v1, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_5

    .line 277
    const/4 v1, 0x5

    iget-wide v4, p0, Lcom/google/b/f/b/a/cc;->f:J

    .line 278
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 280
    :cond_5
    iget v1, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_6

    .line 281
    const/4 v1, 0x6

    iget-boolean v3, p0, Lcom/google/b/f/b/a/cc;->g:Z

    .line 282
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 284
    :cond_6
    iget v1, p0, Lcom/google/b/f/b/a/cc;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_7

    .line 285
    const/4 v1, 0x7

    iget-boolean v3, p0, Lcom/google/b/f/b/a/cc;->h:Z

    .line 286
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 288
    :cond_7
    iget-object v1, p0, Lcom/google/b/f/b/a/cc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    iput v0, p0, Lcom/google/b/f/b/a/cc;->k:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 262
    goto/16 :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/cc;->newBuilder()Lcom/google/b/f/b/a/ce;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/cc;->newBuilder()Lcom/google/b/f/b/a/ce;

    move-result-object v0

    return-object v0
.end method
