.class public final enum Lcom/google/b/f/br;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/br;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/br;

.field public static final enum b:Lcom/google/b/f/br;

.field public static final enum c:Lcom/google/b/f/br;

.field public static final enum d:Lcom/google/b/f/br;

.field public static final enum e:Lcom/google/b/f/br;

.field public static final enum f:Lcom/google/b/f/br;

.field public static final enum g:Lcom/google/b/f/br;

.field public static final enum h:Lcom/google/b/f/br;

.field public static final enum i:Lcom/google/b/f/br;

.field public static final enum j:Lcom/google/b/f/br;

.field public static final enum k:Lcom/google/b/f/br;

.field public static final enum l:Lcom/google/b/f/br;

.field public static final enum m:Lcom/google/b/f/br;

.field public static final enum n:Lcom/google/b/f/br;

.field public static final enum o:Lcom/google/b/f/br;

.field public static final enum p:Lcom/google/b/f/br;

.field public static final enum q:Lcom/google/b/f/br;

.field public static final enum r:Lcom/google/b/f/br;

.field public static final enum s:Lcom/google/b/f/br;

.field public static final enum t:Lcom/google/b/f/br;

.field private static final synthetic v:[Lcom/google/b/f/br;


# instance fields
.field final u:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1027
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "CLICKED_SUGGESTION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->a:Lcom/google/b/f/br;

    .line 1031
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "DELETE_KEY"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->b:Lcom/google/b/f/br;

    .line 1035
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "ENTER_KEY"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->c:Lcom/google/b/f/br;

    .line 1039
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SHIFT_ENTER_KEY"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->d:Lcom/google/b/f/br;

    .line 1043
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "ESCAPE_KEY"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->e:Lcom/google/b/f/br;

    .line 1047
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "FEELING_LUCKY_ARROW_KEY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->f:Lcom/google/b/f/br;

    .line 1051
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "FEELING_LUCKY_BUTTON"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->g:Lcom/google/b/f/br;

    .line 1055
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "FEELING_LUCKY_BUTTON_INLINE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->h:Lcom/google/b/f/br;

    .line 1059
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "FEELING_LUCKY_LINK"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->i:Lcom/google/b/f/br;

    .line 1063
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "PREFETCH"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->j:Lcom/google/b/f/br;

    .line 1067
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SCROLL"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->k:Lcom/google/b/f/br;

    .line 1071
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SEARCH_ANYWAY_LINK"

    const/16 v2, 0xb

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->l:Lcom/google/b/f/br;

    .line 1075
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SEARCH_BUTTON"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->m:Lcom/google/b/f/br;

    .line 1079
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SEARCH_BUTTON_INLINE"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->n:Lcom/google/b/f/br;

    .line 1083
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SECONDARY_BUTTON_INLINE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->o:Lcom/google/b/f/br;

    .line 1087
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SPEECH_RECOGNITION"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->p:Lcom/google/b/f/br;

    .line 1091
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SPEECH2_CLICK"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->q:Lcom/google/b/f/br;

    .line 1095
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SPEECH2_FINAL_RESULT"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->r:Lcom/google/b/f/br;

    .line 1099
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SPEECH2_IDLE_TIMEOUT"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->s:Lcom/google/b/f/br;

    .line 1103
    new-instance v0, Lcom/google/b/f/br;

    const-string v1, "SPEECH2_RESTORE"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/br;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/br;->t:Lcom/google/b/f/br;

    .line 1022
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/google/b/f/br;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/b/f/br;->a:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/b/f/br;->b:Lcom/google/b/f/br;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/br;->c:Lcom/google/b/f/br;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/br;->d:Lcom/google/b/f/br;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/f/br;->e:Lcom/google/b/f/br;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/b/f/br;->f:Lcom/google/b/f/br;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/f/br;->g:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/f/br;->h:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/b/f/br;->i:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/b/f/br;->j:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/b/f/br;->k:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/b/f/br;->l:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/b/f/br;->m:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/b/f/br;->n:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/b/f/br;->o:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/b/f/br;->p:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/b/f/br;->q:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/b/f/br;->r:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/b/f/br;->s:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/b/f/br;->t:Lcom/google/b/f/br;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/f/br;->v:[Lcom/google/b/f/br;

    .line 1223
    new-instance v0, Lcom/google/b/f/bs;

    invoke-direct {v0}, Lcom/google/b/f/bs;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1233
    iput p3, p0, Lcom/google/b/f/br;->u:I

    .line 1234
    return-void
.end method

.method public static a(I)Lcom/google/b/f/br;
    .locals 1

    .prologue
    .line 1193
    packed-switch p0, :pswitch_data_0

    .line 1214
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1194
    :pswitch_0
    sget-object v0, Lcom/google/b/f/br;->a:Lcom/google/b/f/br;

    goto :goto_0

    .line 1195
    :pswitch_1
    sget-object v0, Lcom/google/b/f/br;->b:Lcom/google/b/f/br;

    goto :goto_0

    .line 1196
    :pswitch_2
    sget-object v0, Lcom/google/b/f/br;->c:Lcom/google/b/f/br;

    goto :goto_0

    .line 1197
    :pswitch_3
    sget-object v0, Lcom/google/b/f/br;->d:Lcom/google/b/f/br;

    goto :goto_0

    .line 1198
    :pswitch_4
    sget-object v0, Lcom/google/b/f/br;->e:Lcom/google/b/f/br;

    goto :goto_0

    .line 1199
    :pswitch_5
    sget-object v0, Lcom/google/b/f/br;->f:Lcom/google/b/f/br;

    goto :goto_0

    .line 1200
    :pswitch_6
    sget-object v0, Lcom/google/b/f/br;->g:Lcom/google/b/f/br;

    goto :goto_0

    .line 1201
    :pswitch_7
    sget-object v0, Lcom/google/b/f/br;->h:Lcom/google/b/f/br;

    goto :goto_0

    .line 1202
    :pswitch_8
    sget-object v0, Lcom/google/b/f/br;->i:Lcom/google/b/f/br;

    goto :goto_0

    .line 1203
    :pswitch_9
    sget-object v0, Lcom/google/b/f/br;->j:Lcom/google/b/f/br;

    goto :goto_0

    .line 1204
    :pswitch_a
    sget-object v0, Lcom/google/b/f/br;->k:Lcom/google/b/f/br;

    goto :goto_0

    .line 1205
    :pswitch_b
    sget-object v0, Lcom/google/b/f/br;->l:Lcom/google/b/f/br;

    goto :goto_0

    .line 1206
    :pswitch_c
    sget-object v0, Lcom/google/b/f/br;->m:Lcom/google/b/f/br;

    goto :goto_0

    .line 1207
    :pswitch_d
    sget-object v0, Lcom/google/b/f/br;->n:Lcom/google/b/f/br;

    goto :goto_0

    .line 1208
    :pswitch_e
    sget-object v0, Lcom/google/b/f/br;->o:Lcom/google/b/f/br;

    goto :goto_0

    .line 1209
    :pswitch_f
    sget-object v0, Lcom/google/b/f/br;->p:Lcom/google/b/f/br;

    goto :goto_0

    .line 1210
    :pswitch_10
    sget-object v0, Lcom/google/b/f/br;->q:Lcom/google/b/f/br;

    goto :goto_0

    .line 1211
    :pswitch_11
    sget-object v0, Lcom/google/b/f/br;->r:Lcom/google/b/f/br;

    goto :goto_0

    .line 1212
    :pswitch_12
    sget-object v0, Lcom/google/b/f/br;->s:Lcom/google/b/f/br;

    goto :goto_0

    .line 1213
    :pswitch_13
    sget-object v0, Lcom/google/b/f/br;->t:Lcom/google/b/f/br;

    goto :goto_0

    .line 1193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_b
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/br;
    .locals 1

    .prologue
    .line 1022
    const-class v0, Lcom/google/b/f/br;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/br;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/br;
    .locals 1

    .prologue
    .line 1022
    sget-object v0, Lcom/google/b/f/br;->v:[Lcom/google/b/f/br;

    invoke-virtual {v0}, [Lcom/google/b/f/br;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/br;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1189
    iget v0, p0, Lcom/google/b/f/br;->u:I

    return v0
.end method
