.class public final Lcom/google/b/f/b/a/am;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/an;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/ak;",
        "Lcom/google/b/f/b/a/am;",
        ">;",
        "Lcom/google/b/f/b/a/an;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:F

.field public e:F


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 291
    sget-object v0, Lcom/google/b/f/b/a/ak;->f:Lcom/google/b/f/b/a/ak;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 292
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/ak;)Lcom/google/b/f/b/a/am;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 335
    invoke-static {}, Lcom/google/b/f/b/a/ak;->d()Lcom/google/b/f/b/a/ak;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 349
    :goto_0
    return-object p0

    .line 336
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/ak;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 337
    iget v2, p1, Lcom/google/b/f/b/a/ak;->b:I

    iget v3, p0, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/am;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/am;->b:I

    .line 339
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/ak;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 340
    iget v2, p1, Lcom/google/b/f/b/a/ak;->c:F

    iget v3, p0, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/am;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/am;->c:F

    .line 342
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/ak;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 343
    iget v2, p1, Lcom/google/b/f/b/a/ak;->d:F

    iget v3, p0, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/am;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/am;->d:F

    .line 345
    :cond_3
    iget v2, p1, Lcom/google/b/f/b/a/ak;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 346
    iget v0, p1, Lcom/google/b/f/b/a/ak;->e:F

    iget v1, p0, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/b/f/b/a/am;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/am;->e:F

    .line 348
    :cond_4
    iget-object v0, p1, Lcom/google/b/f/b/a/ak;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 336
    goto :goto_1

    :cond_6
    move v2, v1

    .line 339
    goto :goto_2

    :cond_7
    move v2, v1

    .line 342
    goto :goto_3

    :cond_8
    move v0, v1

    .line 345
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 283
    new-instance v2, Lcom/google/b/f/b/a/ak;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/ak;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/am;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/am;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/ak;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/am;->c:F

    iput v1, v2, Lcom/google/b/f/b/a/ak;->c:F

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/am;->d:F

    iput v1, v2, Lcom/google/b/f/b/a/ak;->d:F

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/am;->e:F

    iput v1, v2, Lcom/google/b/f/b/a/ak;->e:F

    iput v0, v2, Lcom/google/b/f/b/a/ak;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 283
    check-cast p1, Lcom/google/b/f/b/a/ak;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/am;->a(Lcom/google/b/f/b/a/ak;)Lcom/google/b/f/b/a/am;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x1

    return v0
.end method
