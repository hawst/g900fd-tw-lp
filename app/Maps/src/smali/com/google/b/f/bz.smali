.class public final Lcom/google/b/f/bz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/cc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/bx;",
        "Lcom/google/b/f/bz;",
        ">;",
        "Lcom/google/b/f/cc;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2524
    sget-object v0, Lcom/google/b/f/bx;->g:Lcom/google/b/f/bx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2612
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/f/bz;->b:I

    .line 2676
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    .line 2774
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/bz;->f:I

    .line 2525
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2678
    iget v0, p0, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2679
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    .line 2680
    iget v0, p0, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/bz;->a:I

    .line 2682
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/bx;)Lcom/google/b/f/bz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2575
    invoke-static {}, Lcom/google/b/f/bx;->d()Lcom/google/b/f/bx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2599
    :goto_0
    return-object p0

    .line 2576
    :cond_0
    iget v2, p1, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2577
    iget v2, p1, Lcom/google/b/f/bx;->b:I

    iget v3, p0, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/bz;->a:I

    iput v2, p0, Lcom/google/b/f/bz;->b:I

    .line 2579
    :cond_1
    iget v2, p1, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2580
    iget v2, p1, Lcom/google/b/f/bx;->c:I

    iget v3, p0, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/bz;->a:I

    iput v2, p0, Lcom/google/b/f/bz;->c:I

    .line 2582
    :cond_2
    iget-object v2, p1, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2583
    iget-object v2, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2584
    iget-object v2, p1, Lcom/google/b/f/bx;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    .line 2585
    iget v2, p0, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/b/f/bz;->a:I

    .line 2592
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2593
    iget v2, p1, Lcom/google/b/f/bx;->e:I

    iget v3, p0, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/bz;->a:I

    iput v2, p0, Lcom/google/b/f/bz;->e:I

    .line 2595
    :cond_4
    iget v2, p1, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_c

    .line 2596
    iget v0, p1, Lcom/google/b/f/bx;->f:I

    invoke-static {v0}, Lcom/google/b/f/ca;->a(I)Lcom/google/b/f/ca;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/b/f/ca;->a:Lcom/google/b/f/ca;

    :cond_5
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 2576
    goto :goto_1

    :cond_7
    move v2, v1

    .line 2579
    goto :goto_2

    .line 2587
    :cond_8
    invoke-direct {p0}, Lcom/google/b/f/bz;->c()V

    .line 2588
    iget-object v2, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v2, v1

    .line 2592
    goto :goto_4

    :cond_a
    move v0, v1

    .line 2595
    goto :goto_5

    .line 2596
    :cond_b
    iget v1, p0, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/b/f/bz;->a:I

    iget v0, v0, Lcom/google/b/f/ca;->f:I

    iput v0, p0, Lcom/google/b/f/bz;->f:I

    .line 2598
    :cond_c
    iget-object v0, p1, Lcom/google/b/f/bx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/b/f/bz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/b/f/bz;"
        }
    .end annotation

    .prologue
    .line 2726
    invoke-direct {p0}, Lcom/google/b/f/bz;->c()V

    .line 2727
    iget-object v0, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2730
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2516
    new-instance v2, Lcom/google/b/f/bx;

    invoke-direct {v2, p0}, Lcom/google/b/f/bx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/bz;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/b/f/bz;->b:I

    iput v1, v2, Lcom/google/b/f/bx;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/bz;->c:I

    iput v1, v2, Lcom/google/b/f/bx;->c:I

    iget v1, p0, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/b/f/bz;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/b/f/bz;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/b/f/bx;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/b/f/bz;->e:I

    iput v1, v2, Lcom/google/b/f/bx;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v1, p0, Lcom/google/b/f/bz;->f:I

    iput v1, v2, Lcom/google/b/f/bx;->f:I

    iput v0, v2, Lcom/google/b/f/bx;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2516
    check-cast p1, Lcom/google/b/f/bx;

    invoke-virtual {p0, p1}, Lcom/google/b/f/bz;->a(Lcom/google/b/f/bx;)Lcom/google/b/f/bz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2603
    iget v2, p0, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 2607
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 2603
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2607
    goto :goto_1
.end method
