.class public final Lcom/google/b/f/bx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/cc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bx;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/b/f/bx;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2187
    new-instance v0, Lcom/google/b/f/by;

    invoke-direct {v0}, Lcom/google/b/f/by;-><init>()V

    sput-object v0, Lcom/google/b/f/bx;->PARSER:Lcom/google/n/ax;

    .line 2444
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/bx;->j:Lcom/google/n/aw;

    .line 2814
    new-instance v0, Lcom/google/b/f/bx;

    invoke-direct {v0}, Lcom/google/b/f/bx;-><init>()V

    sput-object v0, Lcom/google/b/f/bx;->g:Lcom/google/b/f/bx;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2095
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2371
    iput-byte v0, p0, Lcom/google/b/f/bx;->h:B

    .line 2406
    iput v0, p0, Lcom/google/b/f/bx;->i:I

    .line 2096
    iput v0, p0, Lcom/google/b/f/bx;->b:I

    .line 2097
    iput v1, p0, Lcom/google/b/f/bx;->c:I

    .line 2098
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2099
    iput v1, p0, Lcom/google/b/f/bx;->e:I

    .line 2100
    iput v1, p0, Lcom/google/b/f/bx;->f:I

    .line 2101
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x4

    .line 2107
    invoke-direct {p0}, Lcom/google/b/f/bx;-><init>()V

    .line 2110
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 2113
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 2114
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 2115
    sparse-switch v0, :sswitch_data_0

    .line 2120
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 2122
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2118
    goto :goto_0

    .line 2127
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/bx;->a:I

    .line 2128
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/bx;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2175
    :catch_0
    move-exception v0

    .line 2176
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2181
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_1

    .line 2182
    iget-object v1, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2184
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bx;->au:Lcom/google/n/bn;

    throw v0

    .line 2132
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/bx;->a:I

    .line 2133
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/bx;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2177
    :catch_1
    move-exception v0

    .line 2178
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 2179
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2137
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_2

    .line 2138
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2139
    or-int/lit8 v1, v1, 0x4

    .line 2141
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2145
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 2146
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 2147
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_1
    if-lez v0, :cond_3

    .line 2148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2149
    or-int/lit8 v1, v1, 0x4

    .line 2151
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_5

    move v0, v2

    :goto_3
    if-lez v0, :cond_6

    .line 2152
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2147
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 2151
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 2154
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 2158
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/bx;->a:I

    .line 2159
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/bx;->e:I

    goto/16 :goto_0

    .line 2163
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 2164
    invoke-static {v0}, Lcom/google/b/f/ca;->a(I)Lcom/google/b/f/ca;

    move-result-object v6

    .line 2165
    if-nez v6, :cond_7

    .line 2166
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 2168
    :cond_7
    iget v6, p0, Lcom/google/b/f/bx;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/b/f/bx;->a:I

    .line 2169
    iput v0, p0, Lcom/google/b/f/bx;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2181
    :cond_8
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_9

    .line 2182
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2184
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bx;->au:Lcom/google/n/bn;

    .line 2185
    return-void

    .line 2115
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2093
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2371
    iput-byte v0, p0, Lcom/google/b/f/bx;->h:B

    .line 2406
    iput v0, p0, Lcom/google/b/f/bx;->i:I

    .line 2094
    return-void
.end method

.method public static d()Lcom/google/b/f/bx;
    .locals 1

    .prologue
    .line 2817
    sget-object v0, Lcom/google/b/f/bx;->g:Lcom/google/b/f/bx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/bz;
    .locals 1

    .prologue
    .line 2506
    new-instance v0, Lcom/google/b/f/bz;

    invoke-direct {v0}, Lcom/google/b/f/bz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2199
    sget-object v0, Lcom/google/b/f/bx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2387
    invoke-virtual {p0}, Lcom/google/b/f/bx;->c()I

    .line 2388
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2389
    iget v0, p0, Lcom/google/b/f/bx;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2391
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2392
    iget v0, p0, Lcom/google/b/f/bx;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_1
    move v1, v2

    .line 2394
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2395
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2394
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2389
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 2397
    :cond_3
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 2398
    iget v0, p0, Lcom/google/b/f/bx;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2400
    :cond_4
    :goto_2
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 2401
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/b/f/bx;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2403
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/bx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2404
    return-void

    .line 2398
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 2401
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2373
    iget-byte v2, p0, Lcom/google/b/f/bx;->h:B

    .line 2374
    if-ne v2, v0, :cond_0

    .line 2382
    :goto_0
    return v0

    .line 2375
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 2377
    :cond_1
    iget v2, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 2378
    iput-byte v1, p0, Lcom/google/b/f/bx;->h:B

    move v0, v1

    .line 2379
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2377
    goto :goto_1

    .line 2381
    :cond_3
    iput-byte v0, p0, Lcom/google/b/f/bx;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 2408
    iget v0, p0, Lcom/google/b/f/bx;->i:I

    .line 2409
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2439
    :goto_0
    return v0

    .line 2412
    :cond_0
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 2413
    iget v0, p0, Lcom/google/b/f/bx;->b:I

    .line 2414
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 2416
    :goto_2
    iget v3, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_7

    .line 2417
    iget v3, p0, Lcom/google/b/f/bx;->c:I

    .line 2418
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    :goto_3
    move v4, v2

    move v5, v2

    .line 2422
    :goto_4
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 2423
    iget-object v0, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    .line 2424
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v5, v0

    .line 2422
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_1
    move v0, v1

    .line 2414
    goto :goto_1

    .line 2426
    :cond_2
    add-int v0, v3, v5

    .line 2427
    iget-object v3, p0, Lcom/google/b/f/bx;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v0

    .line 2429
    iget v0, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 2430
    iget v0, p0, Lcom/google/b/f/bx;->e:I

    .line 2431
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v0, v3

    .line 2433
    :goto_6
    iget v3, p0, Lcom/google/b/f/bx;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 2434
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/b/f/bx;->f:I

    .line 2435
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2437
    :cond_4
    iget-object v1, p0, Lcom/google/b/f/bx;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2438
    iput v0, p0, Lcom/google/b/f/bx;->i:I

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 2431
    goto :goto_5

    :cond_6
    move v0, v3

    goto :goto_6

    :cond_7
    move v3, v0

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2087
    invoke-static {}, Lcom/google/b/f/bx;->newBuilder()Lcom/google/b/f/bz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/bz;->a(Lcom/google/b/f/bx;)Lcom/google/b/f/bz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2087
    invoke-static {}, Lcom/google/b/f/bx;->newBuilder()Lcom/google/b/f/bz;

    move-result-object v0

    return-object v0
.end method
