.class public final Lcom/google/b/f/b/a/r;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/p;",
        "Lcom/google/b/f/b/a/r;",
        ">;",
        "Lcom/google/b/f/b/a/s;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1954
    sget-object v0, Lcom/google/b/f/b/a/p;->d:Lcom/google/b/f/b/a/p;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2007
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/r;->b:Ljava/lang/Object;

    .line 2083
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/r;->c:Ljava/lang/Object;

    .line 1955
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/p;)Lcom/google/b/f/b/a/r;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1986
    invoke-static {}, Lcom/google/b/f/b/a/p;->d()Lcom/google/b/f/b/a/p;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1998
    :goto_0
    return-object p0

    .line 1987
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1988
    iget v2, p0, Lcom/google/b/f/b/a/r;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/f/b/a/r;->a:I

    .line 1989
    iget-object v2, p1, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/b/f/b/a/r;->b:Ljava/lang/Object;

    .line 1992
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 1993
    iget v0, p0, Lcom/google/b/f/b/a/r;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/r;->a:I

    .line 1994
    iget-object v0, p1, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/b/a/r;->c:Ljava/lang/Object;

    .line 1997
    :cond_2
    iget-object v0, p1, Lcom/google/b/f/b/a/p;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1987
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1992
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1946
    new-instance v2, Lcom/google/b/f/b/a/p;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/p;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/r;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/b/f/b/a/r;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/b/a/p;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/b/f/b/a/r;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/b/a/p;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/b/f/b/a/p;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1946
    check-cast p1, Lcom/google/b/f/b/a/p;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/r;->a(Lcom/google/b/f/b/a/p;)Lcom/google/b/f/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2002
    const/4 v0, 0x1

    return v0
.end method
