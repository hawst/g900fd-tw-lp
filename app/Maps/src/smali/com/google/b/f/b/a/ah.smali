.class public final enum Lcom/google/b/f/b/a/ah;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/ah;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/ah;

.field public static final enum b:Lcom/google/b/f/b/a/ah;

.field public static final enum c:Lcom/google/b/f/b/a/ah;

.field public static final enum d:Lcom/google/b/f/b/a/ah;

.field public static final enum e:Lcom/google/b/f/b/a/ah;

.field public static final enum f:Lcom/google/b/f/b/a/ah;

.field public static final enum g:Lcom/google/b/f/b/a/ah;

.field public static final enum h:Lcom/google/b/f/b/a/ah;

.field public static final enum i:Lcom/google/b/f/b/a/ah;

.field private static final synthetic k:[Lcom/google/b/f/b/a/ah;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 101
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    .line 105
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "NOT_SHOWN_BECAUSE_EXPIRED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->b:Lcom/google/b/f/b/a/ah;

    .line 109
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "CLICKED_ACCEPT"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->c:Lcom/google/b/f/b/a/ah;

    .line 113
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "CLICKED_CANCEL"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    .line 117
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "DISMISSED_BY_TIMEOUT"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->e:Lcom/google/b/f/b/a/ah;

    .line 121
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "DISMISSED_BY_GUIDANCE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->f:Lcom/google/b/f/b/a/ah;

    .line 125
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "DISMISSED_BY_REROUTE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->g:Lcom/google/b/f/b/a/ah;

    .line 129
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "DISMISSED_BY_DESTINATION_REACHED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->h:Lcom/google/b/f/b/a/ah;

    .line 133
    new-instance v0, Lcom/google/b/f/b/a/ah;

    const-string v1, "DISMISSED_BY_NAVIGATION_EXITED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/b/a/ah;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/ah;->i:Lcom/google/b/f/b/a/ah;

    .line 96
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/b/f/b/a/ah;

    sget-object v1, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/b/a/ah;->b:Lcom/google/b/f/b/a/ah;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/b/a/ah;->c:Lcom/google/b/f/b/a/ah;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/b/f/b/a/ah;->e:Lcom/google/b/f/b/a/ah;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/b/f/b/a/ah;->f:Lcom/google/b/f/b/a/ah;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/f/b/a/ah;->g:Lcom/google/b/f/b/a/ah;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/f/b/a/ah;->h:Lcom/google/b/f/b/a/ah;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/b/f/b/a/ah;->i:Lcom/google/b/f/b/a/ah;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/f/b/a/ah;->k:[Lcom/google/b/f/b/a/ah;

    .line 198
    new-instance v0, Lcom/google/b/f/b/a/ai;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ai;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 207
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 208
    iput p3, p0, Lcom/google/b/f/b/a/ah;->j:I

    .line 209
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/ah;
    .locals 1

    .prologue
    .line 179
    packed-switch p0, :pswitch_data_0

    .line 189
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 180
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 181
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/ah;->b:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 182
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/ah;->c:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 183
    :pswitch_3
    sget-object v0, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 184
    :pswitch_4
    sget-object v0, Lcom/google/b/f/b/a/ah;->e:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 185
    :pswitch_5
    sget-object v0, Lcom/google/b/f/b/a/ah;->f:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 186
    :pswitch_6
    sget-object v0, Lcom/google/b/f/b/a/ah;->g:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 187
    :pswitch_7
    sget-object v0, Lcom/google/b/f/b/a/ah;->h:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 188
    :pswitch_8
    sget-object v0, Lcom/google/b/f/b/a/ah;->i:Lcom/google/b/f/b/a/ah;

    goto :goto_0

    .line 179
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/ah;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/b/f/b/a/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ah;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/ah;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/b/f/b/a/ah;->k:[Lcom/google/b/f/b/a/ah;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/ah;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/b/f/b/a/ah;->j:I

    return v0
.end method
