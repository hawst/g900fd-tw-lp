.class public final Lcom/google/b/f/b/a/m;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/k;",
        "Lcom/google/b/f/b/a/m;",
        ">;",
        "Lcom/google/b/f/b/a/n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 710
    sget-object v0, Lcom/google/b/f/b/a/k;->e:Lcom/google/b/f/b/a/k;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 776
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    .line 711
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 749
    invoke-static {}, Lcom/google/b/f/b/a/k;->d()Lcom/google/b/f/b/a/k;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 767
    :goto_0
    return-object p0

    .line 750
    :cond_0
    iget-object v2, p1, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 751
    iget-object v2, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 752
    iget-object v2, p1, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    .line 753
    iget v2, p0, Lcom/google/b/f/b/a/m;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/b/f/b/a/m;->a:I

    .line 760
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 761
    iget-boolean v2, p1, Lcom/google/b/f/b/a/k;->c:Z

    iget v3, p0, Lcom/google/b/f/b/a/m;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/m;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/b/a/m;->c:Z

    .line 763
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/k;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 764
    iget-boolean v0, p1, Lcom/google/b/f/b/a/k;->d:Z

    iget v1, p0, Lcom/google/b/f/b/a/m;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/b/a/m;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/b/a/m;->d:Z

    .line 766
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/b/a/k;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 755
    :cond_4
    iget v2, p0, Lcom/google/b/f/b/a/m;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/b/f/b/a/m;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/f/b/a/m;->a:I

    .line 756
    :cond_5
    iget-object v2, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 760
    goto :goto_2

    :cond_7
    move v0, v1

    .line 763
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/google/b/f/b/a/m;->c()Lcom/google/b/f/b/a/k;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 702
    check-cast p1, Lcom/google/b/f/b/a/k;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/m;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/b/f/b/a/k;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 728
    new-instance v2, Lcom/google/b/f/b/a/k;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/k;-><init>(Lcom/google/n/v;)V

    .line 729
    iget v3, p0, Lcom/google/b/f/b/a/m;->a:I

    .line 730
    const/4 v1, 0x0

    .line 731
    iget v4, p0, Lcom/google/b/f/b/a/m;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 732
    iget-object v4, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    .line 733
    iget v4, p0, Lcom/google/b/f/b/a/m;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/b/f/b/a/m;->a:I

    .line 735
    :cond_0
    iget-object v4, p0, Lcom/google/b/f/b/a/m;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/b/f/b/a/k;->b:Ljava/util/List;

    .line 736
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 739
    :goto_0
    iget-boolean v1, p0, Lcom/google/b/f/b/a/m;->c:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/k;->c:Z

    .line 740
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 741
    or-int/lit8 v0, v0, 0x2

    .line 743
    :cond_1
    iget-boolean v1, p0, Lcom/google/b/f/b/a/m;->d:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/k;->d:Z

    .line 744
    iput v0, v2, Lcom/google/b/f/b/a/k;->a:I

    .line 745
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
