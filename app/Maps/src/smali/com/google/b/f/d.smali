.class public final Lcom/google/b/f/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b;",
        "Lcom/google/b/f/d;",
        ">;",
        "Lcom/google/b/f/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:I

.field private l:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 595
    sget-object v0, Lcom/google/b/f/b;->m:Lcom/google/b/f/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 725
    iput v1, p0, Lcom/google/b/f/d;->b:I

    .line 757
    iput v1, p0, Lcom/google/b/f/d;->h:I

    .line 853
    iput v1, p0, Lcom/google/b/f/d;->e:I

    .line 885
    iput v1, p0, Lcom/google/b/f/d;->f:I

    .line 949
    iput v1, p0, Lcom/google/b/f/d;->i:I

    .line 596
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b;)Lcom/google/b/f/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 681
    invoke-static {}, Lcom/google/b/f/b;->d()Lcom/google/b/f/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 716
    :goto_0
    return-object p0

    .line 682
    :cond_0
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 683
    iget v2, p1, Lcom/google/b/f/b;->b:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->b:I

    .line 685
    :cond_1
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 686
    iget v2, p1, Lcom/google/b/f/b;->c:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->h:I

    .line 688
    :cond_2
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 689
    iget v2, p1, Lcom/google/b/f/b;->d:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->c:I

    .line 691
    :cond_3
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 692
    iget v2, p1, Lcom/google/b/f/b;->e:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->d:I

    .line 694
    :cond_4
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 695
    iget v2, p1, Lcom/google/b/f/b;->f:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->e:I

    .line 697
    :cond_5
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 698
    iget v2, p1, Lcom/google/b/f/b;->g:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->f:I

    .line 700
    :cond_6
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 701
    iget v2, p1, Lcom/google/b/f/b;->h:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->g:I

    .line 703
    :cond_7
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 704
    iget v2, p1, Lcom/google/b/f/b;->i:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->i:I

    .line 706
    :cond_8
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 707
    iget-boolean v2, p1, Lcom/google/b/f/b;->j:Z

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/d;->j:Z

    .line 709
    :cond_9
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 710
    iget v2, p1, Lcom/google/b/f/b;->k:I

    iget v3, p0, Lcom/google/b/f/d;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/b/f/d;->a:I

    iput v2, p0, Lcom/google/b/f/d;->k:I

    .line 712
    :cond_a
    iget v2, p1, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_16

    :goto_b
    if-eqz v0, :cond_b

    .line 713
    iget v0, p1, Lcom/google/b/f/b;->l:I

    iget v1, p0, Lcom/google/b/f/d;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/b/f/d;->a:I

    iput v0, p0, Lcom/google/b/f/d;->l:I

    .line 715
    :cond_b
    iget-object v0, p1, Lcom/google/b/f/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 682
    goto/16 :goto_1

    :cond_d
    move v2, v1

    .line 685
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 688
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 691
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 694
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 697
    goto/16 :goto_6

    :cond_12
    move v2, v1

    .line 700
    goto :goto_7

    :cond_13
    move v2, v1

    .line 703
    goto :goto_8

    :cond_14
    move v2, v1

    .line 706
    goto :goto_9

    :cond_15
    move v2, v1

    .line 709
    goto :goto_a

    :cond_16
    move v0, v1

    .line 712
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 587
    new-instance v2, Lcom/google/b/f/b;

    invoke-direct {v2, p0}, Lcom/google/b/f/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget v1, p0, Lcom/google/b/f/d;->b:I

    iput v1, v2, Lcom/google/b/f/b;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/d;->h:I

    iput v1, v2, Lcom/google/b/f/b;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/d;->c:I

    iput v1, v2, Lcom/google/b/f/b;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/b/f/d;->d:I

    iput v1, v2, Lcom/google/b/f/b;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/b/f/d;->e:I

    iput v1, v2, Lcom/google/b/f/b;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/b/f/d;->f:I

    iput v1, v2, Lcom/google/b/f/b;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/b/f/d;->g:I

    iput v1, v2, Lcom/google/b/f/b;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/b/f/d;->i:I

    iput v1, v2, Lcom/google/b/f/b;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/b/f/d;->j:Z

    iput-boolean v1, v2, Lcom/google/b/f/b;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/b/f/d;->k:I

    iput v1, v2, Lcom/google/b/f/b;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/b/f/d;->l:I

    iput v1, v2, Lcom/google/b/f/b;->l:I

    iput v0, v2, Lcom/google/b/f/b;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 587
    check-cast p1, Lcom/google/b/f/b;

    invoke-virtual {p0, p1}, Lcom/google/b/f/d;->a(Lcom/google/b/f/b;)Lcom/google/b/f/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 720
    const/4 v0, 0x1

    return v0
.end method
