.class public final Lcom/google/b/f/b/a/du;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/dx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/du;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/b/f/b/a/du;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/f/b/a/do;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/google/b/f/b/a/dv;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dv;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/du;->PARSER:Lcom/google/n/ax;

    .line 406
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/du;->i:Lcom/google/n/aw;

    .line 832
    new-instance v0, Lcom/google/b/f/b/a/du;

    invoke-direct {v0}, Lcom/google/b/f/b/a/du;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/du;->f:Lcom/google/b/f/b/a/du;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 158
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 349
    iput-byte v0, p0, Lcom/google/b/f/b/a/du;->g:B

    .line 377
    iput v0, p0, Lcom/google/b/f/b/a/du;->h:I

    .line 159
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;

    .line 160
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    .line 161
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/b/f/b/a/du;->d:J

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/du;->e:I

    .line 163
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 169
    invoke-direct {p0}, Lcom/google/b/f/b/a/du;-><init>()V

    .line 172
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 175
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 176
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 177
    sparse-switch v4, :sswitch_data_0

    .line 182
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 184
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 180
    goto :goto_0

    .line 189
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 190
    iget v5, p0, Lcom/google/b/f/b/a/du;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/b/f/b/a/du;->a:I

    .line 191
    iput-object v4, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 221
    iget-object v1, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    .line 223
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/du;->au:Lcom/google/n/bn;

    throw v0

    .line 195
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v6, :cond_2

    .line 196
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    .line 197
    or-int/lit8 v1, v1, 0x2

    .line 199
    :cond_2
    iget-object v4, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    sget-object v5, Lcom/google/b/f/b/a/do;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 216
    :catch_1
    move-exception v0

    .line 217
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 218
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 203
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/b/f/b/a/du;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/du;->a:I

    .line 204
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/du;->d:J

    goto :goto_0

    .line 208
    :sswitch_4
    iget v4, p0, Lcom/google/b/f/b/a/du;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/b/a/du;->a:I

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/b/f/b/a/du;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 220
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_4

    .line 221
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    .line 223
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->au:Lcom/google/n/bn;

    .line 224
    return-void

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 156
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 349
    iput-byte v0, p0, Lcom/google/b/f/b/a/du;->g:B

    .line 377
    iput v0, p0, Lcom/google/b/f/b/a/du;->h:I

    .line 157
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/du;
    .locals 1

    .prologue
    .line 835
    sget-object v0, Lcom/google/b/f/b/a/du;->f:Lcom/google/b/f/b/a/du;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/dw;
    .locals 1

    .prologue
    .line 468
    new-instance v0, Lcom/google/b/f/b/a/dw;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/du;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    sget-object v0, Lcom/google/b/f/b/a/du;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 361
    invoke-virtual {p0}, Lcom/google/b/f/b/a/du;->c()I

    .line 362
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 365
    :goto_1
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 365
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 363
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 368
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 369
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/b/f/b/a/du;->d:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 371
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 372
    iget v0, p0, Lcom/google/b/f/b/a/du;->e:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 374
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 375
    return-void

    .line 372
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 351
    iget-byte v1, p0, Lcom/google/b/f/b/a/du;->g:B

    .line 352
    if-ne v1, v0, :cond_0

    .line 356
    :goto_0
    return v0

    .line 353
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 355
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/du;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 379
    iget v0, p0, Lcom/google/b/f/b/a/du;->h:I

    .line 380
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 401
    :goto_0
    return v0

    .line 383
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 385
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/du;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 387
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 388
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->c:Ljava/util/List;

    .line 389
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 387
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 385
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 391
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 392
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/b/f/b/a/du;->d:J

    .line 393
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 395
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/du;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 396
    iget v0, p0, Lcom/google/b/f/b/a/du;->e:I

    .line 397
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 399
    :cond_4
    iget-object v0, p0, Lcom/google/b/f/b/a/du;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 400
    iput v0, p0, Lcom/google/b/f/b/a/du;->h:I

    goto/16 :goto_0

    .line 397
    :cond_5
    const/16 v0, 0xa

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/google/b/f/b/a/du;->newBuilder()Lcom/google/b/f/b/a/dw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/dw;->a(Lcom/google/b/f/b/a/du;)Lcom/google/b/f/b/a/dw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/google/b/f/b/a/du;->newBuilder()Lcom/google/b/f/b/a/dw;

    move-result-object v0

    return-object v0
.end method
