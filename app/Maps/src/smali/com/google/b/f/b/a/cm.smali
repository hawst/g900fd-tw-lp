.class public final Lcom/google/b/f/b/a/cm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/ct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/ck;",
        "Lcom/google/b/f/b/a/cm;",
        ">;",
        "Lcom/google/b/f/b/a/ct;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 557
    sget-object v0, Lcom/google/b/f/b/a/ck;->g:Lcom/google/b/f/b/a/ck;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 665
    iput v1, p0, Lcom/google/b/f/b/a/cm;->c:I

    .line 701
    iput v1, p0, Lcom/google/b/f/b/a/cm;->d:I

    .line 737
    iput v1, p0, Lcom/google/b/f/b/a/cm;->e:I

    .line 773
    iput v1, p0, Lcom/google/b/f/b/a/cm;->f:I

    .line 558
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 607
    invoke-static {}, Lcom/google/b/f/b/a/ck;->d()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 624
    :goto_0
    return-object p0

    .line 608
    :cond_0
    iget v0, p1, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 609
    iget v0, p1, Lcom/google/b/f/b/a/ck;->b:I

    iget v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/cm;->b:I

    .line 611
    :cond_1
    iget v0, p1, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 612
    iget v0, p1, Lcom/google/b/f/b/a/ck;->c:I

    invoke-static {v0}, Lcom/google/b/f/b/a/cn;->a(I)Lcom/google/b/f/b/a/cn;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/b/f/b/a/cn;->a:Lcom/google/b/f/b/a/cn;

    :cond_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 608
    goto :goto_1

    :cond_4
    move v0, v2

    .line 611
    goto :goto_2

    .line 612
    :cond_5
    iget v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    iget v0, v0, Lcom/google/b/f/b/a/cn;->d:I

    iput v0, p0, Lcom/google/b/f/b/a/cm;->c:I

    .line 614
    :cond_6
    iget v0, p1, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_a

    .line 615
    iget v0, p1, Lcom/google/b/f/b/a/ck;->d:I

    invoke-static {v0}, Lcom/google/b/f/b/a/cp;->a(I)Lcom/google/b/f/b/a/cp;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    :cond_7
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v2

    .line 614
    goto :goto_3

    .line 615
    :cond_9
    iget v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    iget v0, v0, Lcom/google/b/f/b/a/cp;->e:I

    iput v0, p0, Lcom/google/b/f/b/a/cm;->d:I

    .line 617
    :cond_a
    iget v0, p1, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_4
    if-eqz v0, :cond_c

    .line 618
    iget v0, p1, Lcom/google/b/f/b/a/ck;->e:I

    invoke-static {v0}, Lcom/google/b/f/b/a/cp;->a(I)Lcom/google/b/f/b/a/cp;

    move-result-object v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    :cond_b
    invoke-virtual {p0, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cp;)Lcom/google/b/f/b/a/cm;

    .line 620
    :cond_c
    iget v0, p1, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_5
    if-eqz v0, :cond_e

    .line 621
    iget v0, p1, Lcom/google/b/f/b/a/ck;->f:I

    invoke-static {v0}, Lcom/google/b/f/b/a/cr;->a(I)Lcom/google/b/f/b/a/cr;

    move-result-object v0

    if-nez v0, :cond_d

    sget-object v0, Lcom/google/b/f/b/a/cr;->a:Lcom/google/b/f/b/a/cr;

    :cond_d
    invoke-virtual {p0, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cr;)Lcom/google/b/f/b/a/cm;

    .line 623
    :cond_e
    iget-object v0, p1, Lcom/google/b/f/b/a/ck;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v0, v2

    .line 617
    goto :goto_4

    :cond_10
    move v0, v2

    .line 620
    goto :goto_5
.end method

.method public final a(Lcom/google/b/f/b/a/cp;)Lcom/google/b/f/b/a/cm;
    .locals 1

    .prologue
    .line 755
    if-nez p1, :cond_0

    .line 756
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 758
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/b/a/cm;->a:I

    .line 759
    iget v0, p1, Lcom/google/b/f/b/a/cp;->e:I

    iput v0, p0, Lcom/google/b/f/b/a/cm;->e:I

    .line 761
    return-object p0
.end method

.method public final a(Lcom/google/b/f/b/a/cr;)Lcom/google/b/f/b/a/cm;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 791
    if-nez p1, :cond_0

    .line 792
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 794
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/cm;->a:I

    .line 795
    iget v0, p1, Lcom/google/b/f/b/a/cr;->d:I

    iput v0, p0, Lcom/google/b/f/b/a/cm;->f:I

    .line 797
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/b/f/b/a/cm;->c()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 549
    check-cast p1, Lcom/google/b/f/b/a/ck;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 628
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/b/f/b/a/ck;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 579
    new-instance v2, Lcom/google/b/f/b/a/ck;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/ck;-><init>(Lcom/google/n/v;)V

    .line 580
    iget v3, p0, Lcom/google/b/f/b/a/cm;->a:I

    .line 581
    const/4 v1, 0x0

    .line 582
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 585
    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/cm;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/ck;->b:I

    .line 586
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 587
    or-int/lit8 v0, v0, 0x2

    .line 589
    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/cm;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/ck;->c:I

    .line 590
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 591
    or-int/lit8 v0, v0, 0x4

    .line 593
    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/cm;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/ck;->d:I

    .line 594
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 595
    or-int/lit8 v0, v0, 0x8

    .line 597
    :cond_2
    iget v1, p0, Lcom/google/b/f/b/a/cm;->e:I

    iput v1, v2, Lcom/google/b/f/b/a/ck;->e:I

    .line 598
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 599
    or-int/lit8 v0, v0, 0x10

    .line 601
    :cond_3
    iget v1, p0, Lcom/google/b/f/b/a/cm;->f:I

    iput v1, v2, Lcom/google/b/f/b/a/ck;->f:I

    .line 602
    iput v0, v2, Lcom/google/b/f/b/a/ck;->a:I

    .line 603
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
