.class public final Lcom/google/b/f/b/a/bc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/ba;",
        "Lcom/google/b/f/b/a/bc;",
        ">;",
        "Lcom/google/b/f/b/a/bd;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field public k:J

.field public l:I

.field private m:J

.field private n:J

.field private o:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 598
    sget-object v0, Lcom/google/b/f/b/a/ba;->p:Lcom/google/b/f/b/a/ba;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 896
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->o:Lcom/google/n/ao;

    .line 955
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    .line 1014
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    .line 1073
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    .line 1132
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->f:Lcom/google/n/ao;

    .line 1191
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->g:Lcom/google/n/ao;

    .line 1250
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->h:Lcom/google/n/ao;

    .line 1309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    .line 1368
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/bc;->j:Lcom/google/n/ao;

    .line 599
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/bc;
    .locals 2

    .prologue
    .line 1088
    if-nez p1, :cond_0

    .line 1089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1091
    :cond_0
    iget-object v0, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1093
    iget v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 1094
    return-object p0
.end method

.method public final a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 720
    invoke-static {}, Lcom/google/b/f/b/a/ba;->d()Lcom/google/b/f/b/a/ba;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 773
    :goto_0
    return-object p0

    .line 721
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_f

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 722
    iget-wide v2, p1, Lcom/google/b/f/b/a/ba;->b:J

    iget v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/bc;->m:J

    .line 724
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 725
    iget-wide v2, p1, Lcom/google/b/f/b/a/ba;->c:J

    iget v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/bc;->n:J

    .line 727
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 728
    iget v2, p1, Lcom/google/b/f/b/a/ba;->d:I

    iget v3, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/bc;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/bc;->b:I

    .line 730
    :cond_3
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 731
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 732
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 734
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 735
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 736
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 738
    :cond_5
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 739
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 740
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 742
    :cond_6
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 743
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 744
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 746
    :cond_7
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 747
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 748
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 750
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 751
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 752
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 754
    :cond_9
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 755
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 756
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 758
    :cond_a
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 759
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 760
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 762
    :cond_b
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 763
    iget-object v2, p0, Lcom/google/b/f/b/a/bc;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 764
    iget v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 766
    :cond_c
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 767
    iget-wide v2, p1, Lcom/google/b/f/b/a/ba;->n:J

    iget v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/b/f/b/a/bc;->a:I

    iput-wide v2, p0, Lcom/google/b/f/b/a/bc;->k:J

    .line 769
    :cond_d
    iget v2, p1, Lcom/google/b/f/b/a/ba;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_1c

    :goto_e
    if-eqz v0, :cond_e

    .line 770
    iget v0, p1, Lcom/google/b/f/b/a/ba;->o:I

    iget v1, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lcom/google/b/f/b/a/bc;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/bc;->l:I

    .line 772
    :cond_e
    iget-object v0, p1, Lcom/google/b/f/b/a/ba;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 721
    goto/16 :goto_1

    :cond_10
    move v2, v1

    .line 724
    goto/16 :goto_2

    :cond_11
    move v2, v1

    .line 727
    goto/16 :goto_3

    :cond_12
    move v2, v1

    .line 730
    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 734
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 738
    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 742
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 746
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 750
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 754
    goto/16 :goto_a

    :cond_19
    move v2, v1

    .line 758
    goto :goto_b

    :cond_1a
    move v2, v1

    .line 762
    goto :goto_c

    :cond_1b
    move v2, v1

    .line 766
    goto :goto_d

    :cond_1c
    move v0, v1

    .line 769
    goto :goto_e
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 590
    new-instance v2, Lcom/google/b/f/b/a/ba;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/ba;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_d

    :goto_0
    iget-wide v4, p0, Lcom/google/b/f/b/a/bc;->m:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/ba;->b:J

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/b/f/b/a/bc;->n:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/ba;->c:J

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/b/f/b/a/bc;->b:I

    iput v4, v2, Lcom/google/b/f/b/a/ba;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v4, v2, Lcom/google/b/f/b/a/ba;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/bc;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/bc;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-wide v4, p0, Lcom/google/b/f/b/a/bc;->k:J

    iput-wide v4, v2, Lcom/google/b/f/b/a/ba;->n:J

    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget v1, p0, Lcom/google/b/f/b/a/bc;->l:I

    iput v1, v2, Lcom/google/b/f/b/a/ba;->o:I

    iput v0, v2, Lcom/google/b/f/b/a/ba;->a:I

    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 590
    check-cast p1, Lcom/google/b/f/b/a/ba;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 777
    iget v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 778
    iget-object v0, p0, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dg;->d()Lcom/google/b/f/b/a/dg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dg;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dg;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 795
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 777
    goto :goto_0

    .line 783
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 784
    iget-object v0, p0, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dr;->d()Lcom/google/b/f/b/a/dr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dr;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dr;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 786
    goto :goto_1

    :cond_2
    move v0, v1

    .line 783
    goto :goto_2

    .line 789
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 790
    iget-object v0, p0, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/cg;->d()Lcom/google/b/f/b/a/cg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cg;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/cg;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 792
    goto :goto_1

    :cond_4
    move v0, v1

    .line 789
    goto :goto_3

    :cond_5
    move v0, v2

    .line 795
    goto :goto_1
.end method
