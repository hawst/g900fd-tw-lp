.class public final Lcom/google/b/f/r;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/p;",
        "Lcom/google/b/f/r;",
        ">;",
        "Lcom/google/b/f/s;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/google/b/f/p;->e:Lcom/google/b/f/p;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 312
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/p;)Lcom/google/b/f/r;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 349
    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 360
    :goto_0
    return-object p0

    .line 350
    :cond_0
    iget v2, p1, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 351
    iget-wide v2, p1, Lcom/google/b/f/p;->b:J

    iget v4, p0, Lcom/google/b/f/r;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/r;->a:I

    iput-wide v2, p0, Lcom/google/b/f/r;->b:J

    .line 353
    :cond_1
    iget v2, p1, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 354
    iget v2, p1, Lcom/google/b/f/p;->c:I

    iget v3, p0, Lcom/google/b/f/r;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/r;->a:I

    iput v2, p0, Lcom/google/b/f/r;->c:I

    .line 356
    :cond_2
    iget v2, p1, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 357
    iget v0, p1, Lcom/google/b/f/p;->d:I

    iget v1, p0, Lcom/google/b/f/r;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/r;->a:I

    iput v0, p0, Lcom/google/b/f/r;->d:I

    .line 359
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/p;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 350
    goto :goto_1

    :cond_5
    move v2, v1

    .line 353
    goto :goto_2

    :cond_6
    move v0, v1

    .line 356
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 303
    new-instance v2, Lcom/google/b/f/p;

    invoke-direct {v2, p0}, Lcom/google/b/f/p;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/r;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-wide v4, p0, Lcom/google/b/f/r;->b:J

    iput-wide v4, v2, Lcom/google/b/f/p;->b:J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/r;->c:I

    iput v1, v2, Lcom/google/b/f/p;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/r;->d:I

    iput v1, v2, Lcom/google/b/f/p;->d:I

    iput v0, v2, Lcom/google/b/f/p;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 303
    check-cast p1, Lcom/google/b/f/p;

    invoke-virtual {p0, p1}, Lcom/google/b/f/r;->a(Lcom/google/b/f/p;)Lcom/google/b/f/r;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 364
    iget v2, p0, Lcom/google/b/f/r;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 376
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 364
    goto :goto_0

    .line 368
    :cond_2
    iget v2, p0, Lcom/google/b/f/r;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 372
    iget v2, p0, Lcom/google/b/f/r;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    move v0, v1

    .line 376
    goto :goto_1

    :cond_3
    move v2, v0

    .line 368
    goto :goto_2

    :cond_4
    move v2, v0

    .line 372
    goto :goto_3
.end method
