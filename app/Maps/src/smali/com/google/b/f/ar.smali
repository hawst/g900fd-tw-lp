.class public final Lcom/google/b/f/ar;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/au;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/ar;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/b/f/ar;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:F

.field c:F

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11089
    new-instance v0, Lcom/google/b/f/as;

    invoke-direct {v0}, Lcom/google/b/f/as;-><init>()V

    sput-object v0, Lcom/google/b/f/ar;->PARSER:Lcom/google/n/ax;

    .line 11178
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/ar;->g:Lcom/google/n/aw;

    .line 11375
    new-instance v0, Lcom/google/b/f/ar;

    invoke-direct {v0}, Lcom/google/b/f/ar;-><init>()V

    sput-object v0, Lcom/google/b/f/ar;->d:Lcom/google/b/f/ar;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 11040
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 11135
    iput-byte v1, p0, Lcom/google/b/f/ar;->e:B

    .line 11157
    iput v1, p0, Lcom/google/b/f/ar;->f:I

    .line 11041
    iput v0, p0, Lcom/google/b/f/ar;->b:F

    .line 11042
    iput v0, p0, Lcom/google/b/f/ar;->c:F

    .line 11043
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 11049
    invoke-direct {p0}, Lcom/google/b/f/ar;-><init>()V

    .line 11050
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 11054
    const/4 v0, 0x0

    .line 11055
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 11056
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 11057
    sparse-switch v3, :sswitch_data_0

    .line 11062
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 11064
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 11060
    goto :goto_0

    .line 11069
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/ar;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/ar;->a:I

    .line 11070
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/b/f/ar;->b:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 11080
    :catch_0
    move-exception v0

    .line 11081
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 11086
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/ar;->au:Lcom/google/n/bn;

    throw v0

    .line 11074
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/ar;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/ar;->a:I

    .line 11075
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/b/f/ar;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 11082
    :catch_1
    move-exception v0

    .line 11083
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 11084
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 11086
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/ar;->au:Lcom/google/n/bn;

    .line 11087
    return-void

    .line 11057
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x45 -> :sswitch_1
        0x4d -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 11038
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 11135
    iput-byte v0, p0, Lcom/google/b/f/ar;->e:B

    .line 11157
    iput v0, p0, Lcom/google/b/f/ar;->f:I

    .line 11039
    return-void
.end method

.method public static a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;
    .locals 1

    .prologue
    .line 11243
    invoke-static {}, Lcom/google/b/f/ar;->newBuilder()Lcom/google/b/f/at;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/at;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/ar;
    .locals 1

    .prologue
    .line 11378
    sget-object v0, Lcom/google/b/f/ar;->d:Lcom/google/b/f/ar;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/at;
    .locals 1

    .prologue
    .line 11240
    new-instance v0, Lcom/google/b/f/at;

    invoke-direct {v0}, Lcom/google/b/f/at;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/ar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11101
    sget-object v0, Lcom/google/b/f/ar;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 11147
    invoke-virtual {p0}, Lcom/google/b/f/ar;->c()I

    .line 11148
    iget v0, p0, Lcom/google/b/f/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 11149
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/b/f/ar;->b:F

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 11151
    :cond_0
    iget v0, p0, Lcom/google/b/f/ar;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 11152
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/b/f/ar;->c:F

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 11154
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/ar;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 11155
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 11137
    iget-byte v1, p0, Lcom/google/b/f/ar;->e:B

    .line 11138
    if-ne v1, v0, :cond_0

    .line 11142
    :goto_0
    return v0

    .line 11139
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 11141
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/ar;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11159
    iget v0, p0, Lcom/google/b/f/ar;->f:I

    .line 11160
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 11173
    :goto_0
    return v0

    .line 11163
    :cond_0
    iget v0, p0, Lcom/google/b/f/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 11164
    const/16 v0, 0x8

    iget v2, p0, Lcom/google/b/f/ar;->b:F

    .line 11165
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 11167
    :goto_1
    iget v2, p0, Lcom/google/b/f/ar;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 11168
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/b/f/ar;->c:F

    .line 11169
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 11171
    :cond_1
    iget-object v1, p0, Lcom/google/b/f/ar;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 11172
    iput v0, p0, Lcom/google/b/f/ar;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 11032
    invoke-static {}, Lcom/google/b/f/ar;->newBuilder()Lcom/google/b/f/at;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/at;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 11032
    invoke-static {}, Lcom/google/b/f/ar;->newBuilder()Lcom/google/b/f/at;

    move-result-object v0

    return-object v0
.end method
