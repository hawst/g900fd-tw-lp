.class public final Lcom/google/b/f/b/a/ck;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/ct;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ck;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/b/f/b/a/ck;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/b/f/b/a/cl;

    invoke-direct {v0}, Lcom/google/b/f/b/a/cl;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ck;->PARSER:Lcom/google/n/ax;

    .line 477
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/ck;->j:Lcom/google/n/aw;

    .line 813
    new-instance v0, Lcom/google/b/f/b/a/ck;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ck;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ck;->g:Lcom/google/b/f/b/a/ck;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 413
    iput-byte v1, p0, Lcom/google/b/f/b/a/ck;->h:B

    .line 444
    iput v1, p0, Lcom/google/b/f/b/a/ck;->i:I

    .line 18
    iput v0, p0, Lcom/google/b/f/b/a/ck;->b:I

    .line 19
    iput v0, p0, Lcom/google/b/f/b/a/ck;->c:I

    .line 20
    iput v0, p0, Lcom/google/b/f/b/a/ck;->d:I

    .line 21
    iput v0, p0, Lcom/google/b/f/b/a/ck;->e:I

    .line 22
    iput v0, p0, Lcom/google/b/f/b/a/ck;->f:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/b/f/b/a/ck;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    const/4 v0, 0x0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ck;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/ck;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 55
    invoke-static {v3}, Lcom/google/b/f/b/a/cn;->a(I)Lcom/google/b/f/b/a/cn;

    move-result-object v4

    .line 56
    if-nez v4, :cond_1

    .line 57
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    .line 60
    iput v3, p0, Lcom/google/b/f/b/a/ck;->c:I

    goto :goto_0

    .line 65
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 66
    invoke-static {v3}, Lcom/google/b/f/b/a/cp;->a(I)Lcom/google/b/f/b/a/cp;

    move-result-object v4

    .line 67
    if-nez v4, :cond_2

    .line 68
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 70
    :cond_2
    iget v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    .line 71
    iput v3, p0, Lcom/google/b/f/b/a/ck;->d:I

    goto :goto_0

    .line 76
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 77
    invoke-static {v3}, Lcom/google/b/f/b/a/cr;->a(I)Lcom/google/b/f/b/a/cr;

    move-result-object v4

    .line 78
    if-nez v4, :cond_3

    .line 79
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 81
    :cond_3
    iget v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    .line 82
    iput v3, p0, Lcom/google/b/f/b/a/ck;->f:I

    goto :goto_0

    .line 87
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 88
    invoke-static {v3}, Lcom/google/b/f/b/a/cp;->a(I)Lcom/google/b/f/b/a/cp;

    move-result-object v4

    .line 89
    if-nez v4, :cond_4

    .line 90
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_4
    iget v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/b/f/b/a/ck;->a:I

    .line 93
    iput v3, p0, Lcom/google/b/f/b/a/ck;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_5
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/ck;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 413
    iput-byte v0, p0, Lcom/google/b/f/b/a/ck;->h:B

    .line 444
    iput v0, p0, Lcom/google/b/f/b/a/ck;->i:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;
    .locals 1

    .prologue
    .line 542
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b/a/ck;
    .locals 1

    .prologue
    .line 816
    sget-object v0, Lcom/google/b/f/b/a/ck;->g:Lcom/google/b/f/b/a/ck;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/cm;
    .locals 1

    .prologue
    .line 539
    new-instance v0, Lcom/google/b/f/b/a/cm;

    invoke-direct {v0}, Lcom/google/b/f/b/a/cm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ck;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/b/f/b/a/ck;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 425
    invoke-virtual {p0}, Lcom/google/b/f/b/a/ck;->c()I

    .line 426
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 427
    iget v0, p0, Lcom/google/b/f/b/a/ck;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 429
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 430
    iget v0, p0, Lcom/google/b/f/b/a/ck;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 432
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 433
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/ck;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 435
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 436
    iget v0, p0, Lcom/google/b/f/b/a/ck;->f:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 438
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 439
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/b/f/b/a/ck;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 441
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/b/f/b/a/ck;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 442
    return-void

    .line 427
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 430
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 433
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 436
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 439
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 415
    iget-byte v1, p0, Lcom/google/b/f/b/a/ck;->h:B

    .line 416
    if-ne v1, v0, :cond_0

    .line 420
    :goto_0
    return v0

    .line 417
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 419
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/ck;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 446
    iget v0, p0, Lcom/google/b/f/b/a/ck;->i:I

    .line 447
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 472
    :goto_0
    return v0

    .line 450
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 451
    iget v0, p0, Lcom/google/b/f/b/a/ck;->b:I

    .line 452
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 454
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 455
    iget v3, p0, Lcom/google/b/f/b/a/ck;->c:I

    .line 456
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 458
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 459
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/ck;->d:I

    .line 460
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 462
    :cond_2
    iget v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    .line 463
    iget v3, p0, Lcom/google/b/f/b/a/ck;->f:I

    .line 464
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 466
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/ck;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    .line 467
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/b/f/b/a/ck;->e:I

    .line 468
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_4
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 470
    :cond_5
    iget-object v1, p0, Lcom/google/b/f/b/a/ck;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    iput v0, p0, Lcom/google/b/f/b/a/ck;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 452
    goto :goto_1

    :cond_7
    move v3, v1

    .line 456
    goto :goto_3

    :cond_8
    move v3, v1

    .line 460
    goto :goto_4

    :cond_9
    move v3, v1

    .line 464
    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v0

    return-object v0
.end method
