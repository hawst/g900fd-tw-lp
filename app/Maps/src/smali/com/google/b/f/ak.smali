.class public final enum Lcom/google/b/f/ak;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/ak;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/ak;

.field public static final enum b:Lcom/google/b/f/ak;

.field public static final enum c:Lcom/google/b/f/ak;

.field private static final synthetic e:[Lcom/google/b/f/ak;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1240
    new-instance v0, Lcom/google/b/f/ak;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ak;->a:Lcom/google/b/f/ak;

    .line 1241
    new-instance v0, Lcom/google/b/f/ak;

    const-string v1, "CLIENT_EVENT"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/b/f/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ak;->b:Lcom/google/b/f/ak;

    .line 1242
    new-instance v0, Lcom/google/b/f/ak;

    const-string v1, "EVENTS_NOT_SET"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/b/f/ak;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ak;->c:Lcom/google/b/f/ak;

    .line 1238
    new-array v0, v5, [Lcom/google/b/f/ak;

    sget-object v1, Lcom/google/b/f/ak;->a:Lcom/google/b/f/ak;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/ak;->b:Lcom/google/b/f/ak;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/ak;->c:Lcom/google/b/f/ak;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/f/ak;->e:[Lcom/google/b/f/ak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1243
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/ak;->d:I

    .line 1245
    iput p3, p0, Lcom/google/b/f/ak;->d:I

    .line 1246
    return-void
.end method

.method public static a(I)Lcom/google/b/f/ak;
    .locals 2

    .prologue
    .line 1248
    packed-switch p0, :pswitch_data_0

    .line 1252
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1249
    :pswitch_1
    sget-object v0, Lcom/google/b/f/ak;->a:Lcom/google/b/f/ak;

    .line 1251
    :goto_0
    return-object v0

    .line 1250
    :pswitch_2
    sget-object v0, Lcom/google/b/f/ak;->b:Lcom/google/b/f/ak;

    goto :goto_0

    .line 1251
    :pswitch_3
    sget-object v0, Lcom/google/b/f/ak;->c:Lcom/google/b/f/ak;

    goto :goto_0

    .line 1248
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/ak;
    .locals 1

    .prologue
    .line 1238
    const-class v0, Lcom/google/b/f/ak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ak;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/ak;
    .locals 1

    .prologue
    .line 1238
    sget-object v0, Lcom/google/b/f/ak;->e:[Lcom/google/b/f/ak;

    invoke-virtual {v0}, [Lcom/google/b/f/ak;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/ak;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1257
    iget v0, p0, Lcom/google/b/f/ak;->d:I

    return v0
.end method
