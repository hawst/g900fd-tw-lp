.class public final Lcom/google/b/f/b/a/ay;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/az;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/aw;",
        "Lcom/google/b/f/b/a/ay;",
        ">;",
        "Lcom/google/b/f/b/a/az;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:F


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 399
    sget-object v0, Lcom/google/b/f/b/a/aw;->i:Lcom/google/b/f/b/a/aw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 502
    iput v1, p0, Lcom/google/b/f/b/a/ay;->b:I

    .line 538
    iput v1, p0, Lcom/google/b/f/b/a/ay;->c:I

    .line 574
    iput v1, p0, Lcom/google/b/f/b/a/ay;->d:I

    .line 610
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ay;->e:Lcom/google/n/ao;

    .line 669
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ay;->f:Lcom/google/n/ao;

    .line 728
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ay;->g:Lcom/google/n/ao;

    .line 400
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ay;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 467
    invoke-static {}, Lcom/google/b/f/b/a/aw;->d()Lcom/google/b/f/b/a/aw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 493
    :goto_0
    return-object p0

    .line 468
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 469
    iget v2, p1, Lcom/google/b/f/b/a/aw;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 468
    goto :goto_1

    .line 469
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    iget v2, v2, Lcom/google/r/b/a/jl;->f:I

    iput v2, p0, Lcom/google/b/f/b/a/ay;->b:I

    .line 471
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 472
    iget v2, p1, Lcom/google/b/f/b/a/aw;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 471
    goto :goto_2

    .line 472
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    iget v2, v2, Lcom/google/r/b/a/jl;->f:I

    iput v2, p0, Lcom/google/b/f/b/a/ay;->c:I

    .line 474
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    .line 475
    iget v2, p1, Lcom/google/b/f/b/a/aw;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/jl;->a(I)Lcom/google/r/b/a/jl;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    :cond_9
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 474
    goto :goto_3

    .line 475
    :cond_b
    iget v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    iget v2, v2, Lcom/google/r/b/a/jl;->f:I

    iput v2, p0, Lcom/google/b/f/b/a/ay;->d:I

    .line 477
    :cond_c
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_d

    .line 478
    iget-object v2, p0, Lcom/google/b/f/b/a/ay;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 479
    iget v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    .line 481
    :cond_d
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 482
    iget-object v2, p0, Lcom/google/b/f/b/a/ay;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 483
    iget v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    .line 485
    :cond_e
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 486
    iget-object v2, p0, Lcom/google/b/f/b/a/ay;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 487
    iget v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/f/b/a/ay;->a:I

    .line 489
    :cond_f
    iget v2, p1, Lcom/google/b/f/b/a/aw;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    :goto_7
    if-eqz v0, :cond_10

    .line 490
    iget v0, p1, Lcom/google/b/f/b/a/aw;->h:F

    iget v1, p0, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/b/f/b/a/ay;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/ay;->h:F

    .line 492
    :cond_10
    iget-object v0, p1, Lcom/google/b/f/b/a/aw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v2, v1

    .line 477
    goto :goto_4

    :cond_12
    move v2, v1

    .line 481
    goto :goto_5

    :cond_13
    move v2, v1

    .line 485
    goto :goto_6

    :cond_14
    move v0, v1

    .line 489
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 391
    new-instance v2, Lcom/google/b/f/b/a/aw;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/aw;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/ay;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v4, p0, Lcom/google/b/f/b/a/ay;->b:I

    iput v4, v2, Lcom/google/b/f/b/a/aw;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/b/f/b/a/ay;->c:I

    iput v4, v2, Lcom/google/b/f/b/a/aw;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/b/f/b/a/ay;->d:I

    iput v4, v2, Lcom/google/b/f/b/a/aw;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/b/f/b/a/aw;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ay;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ay;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/b/f/b/a/aw;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ay;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ay;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/b/f/b/a/aw;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ay;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ay;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/b/f/b/a/ay;->h:F

    iput v1, v2, Lcom/google/b/f/b/a/aw;->h:F

    iput v0, v2, Lcom/google/b/f/b/a/aw;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 391
    check-cast p1, Lcom/google/b/f/b/a/aw;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/ay;->a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x1

    return v0
.end method
