.class public final enum Lcom/google/b/f/ch;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/ch;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/ch;

.field public static final enum b:Lcom/google/b/f/ch;

.field public static final enum c:Lcom/google/b/f/ch;

.field public static final enum d:Lcom/google/b/f/ch;

.field public static final enum e:Lcom/google/b/f/ch;

.field private static final synthetic g:[Lcom/google/b/f/ch;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 365
    new-instance v0, Lcom/google/b/f/ch;

    const-string v1, "UNASSIGNED_DIRECTIONAL_MOVEMENT_ID"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/ch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    .line 369
    new-instance v0, Lcom/google/b/f/ch;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/ch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ch;->b:Lcom/google/b/f/ch;

    .line 373
    new-instance v0, Lcom/google/b/f/ch;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/ch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ch;->c:Lcom/google/b/f/ch;

    .line 377
    new-instance v0, Lcom/google/b/f/ch;

    const-string v1, "UP"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/ch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ch;->d:Lcom/google/b/f/ch;

    .line 381
    new-instance v0, Lcom/google/b/f/ch;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/ch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ch;->e:Lcom/google/b/f/ch;

    .line 360
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/f/ch;

    sget-object v1, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/ch;->b:Lcom/google/b/f/ch;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/ch;->c:Lcom/google/b/f/ch;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/ch;->d:Lcom/google/b/f/ch;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/ch;->e:Lcom/google/b/f/ch;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/f/ch;->g:[Lcom/google/b/f/ch;

    .line 426
    new-instance v0, Lcom/google/b/f/ci;

    invoke-direct {v0}, Lcom/google/b/f/ci;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 436
    iput p3, p0, Lcom/google/b/f/ch;->f:I

    .line 437
    return-void
.end method

.method public static a(I)Lcom/google/b/f/ch;
    .locals 1

    .prologue
    .line 411
    packed-switch p0, :pswitch_data_0

    .line 417
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 412
    :pswitch_0
    sget-object v0, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    goto :goto_0

    .line 413
    :pswitch_1
    sget-object v0, Lcom/google/b/f/ch;->b:Lcom/google/b/f/ch;

    goto :goto_0

    .line 414
    :pswitch_2
    sget-object v0, Lcom/google/b/f/ch;->c:Lcom/google/b/f/ch;

    goto :goto_0

    .line 415
    :pswitch_3
    sget-object v0, Lcom/google/b/f/ch;->d:Lcom/google/b/f/ch;

    goto :goto_0

    .line 416
    :pswitch_4
    sget-object v0, Lcom/google/b/f/ch;->e:Lcom/google/b/f/ch;

    goto :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/ch;
    .locals 1

    .prologue
    .line 360
    const-class v0, Lcom/google/b/f/ch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ch;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/ch;
    .locals 1

    .prologue
    .line 360
    sget-object v0, Lcom/google/b/f/ch;->g:[Lcom/google/b/f/ch;

    invoke-virtual {v0}, [Lcom/google/b/f/ch;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/ch;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/google/b/f/ch;->f:I

    return v0
.end method
