.class public final Lcom/google/b/f/b/a/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/e;",
        "Lcom/google/b/f/b/a/g;",
        ">;",
        "Lcom/google/b/f/b/a/v;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3609
    sget-object v0, Lcom/google/b/f/b/a/e;->g:Lcom/google/b/f/b/a/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3694
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/g;->b:I

    .line 3730
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    .line 3789
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/g;->d:Lcom/google/n/ao;

    .line 3848
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/g;->e:Lcom/google/n/ao;

    .line 3907
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/g;->f:I

    .line 3610
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/e;)Lcom/google/b/f/b/a/g;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3665
    invoke-static {}, Lcom/google/b/f/b/a/e;->d()Lcom/google/b/f/b/a/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3685
    :goto_0
    return-object p0

    .line 3666
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 3667
    iget v2, p1, Lcom/google/b/f/b/a/e;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/av;->a(I)Lcom/google/r/b/a/av;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 3666
    goto :goto_1

    .line 3667
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/g;->a:I

    iget v2, v2, Lcom/google/r/b/a/av;->v:I

    iput v2, p0, Lcom/google/b/f/b/a/g;->b:I

    .line 3669
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 3670
    iget-object v2, p0, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/e;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3671
    iget v2, p0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/b/f/b/a/g;->a:I

    .line 3673
    :cond_5
    iget v2, p1, Lcom/google/b/f/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 3674
    iget-object v2, p0, Lcom/google/b/f/b/a/g;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/e;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3675
    iget v2, p0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/b/f/b/a/g;->a:I

    .line 3677
    :cond_6
    iget v2, p1, Lcom/google/b/f/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 3678
    iget-object v2, p0, Lcom/google/b/f/b/a/g;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/e;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3679
    iget v2, p0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/b/f/b/a/g;->a:I

    .line 3681
    :cond_7
    iget v2, p1, Lcom/google/b/f/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    :goto_5
    if-eqz v0, :cond_9

    .line 3682
    iget v0, p1, Lcom/google/b/f/b/a/e;->f:I

    invoke-static {v0}, Lcom/google/b/f/b/a/t;->a(I)Lcom/google/b/f/b/a/t;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/b/f/b/a/g;->a(Lcom/google/b/f/b/a/t;)Lcom/google/b/f/b/a/g;

    .line 3684
    :cond_9
    iget-object v0, p1, Lcom/google/b/f/b/a/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 3669
    goto :goto_2

    :cond_b
    move v2, v1

    .line 3673
    goto :goto_3

    :cond_c
    move v2, v1

    .line 3677
    goto :goto_4

    :cond_d
    move v0, v1

    .line 3681
    goto :goto_5
.end method

.method public final a(Lcom/google/b/f/b/a/t;)Lcom/google/b/f/b/a/g;
    .locals 1

    .prologue
    .line 3925
    if-nez p1, :cond_0

    .line 3926
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3928
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/g;->a:I

    .line 3929
    iget v0, p1, Lcom/google/b/f/b/a/t;->g:I

    iput v0, p0, Lcom/google/b/f/b/a/g;->f:I

    .line 3931
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3601
    new-instance v2, Lcom/google/b/f/b/a/e;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/e;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/b/f/b/a/g;->b:I

    iput v4, v2, Lcom/google/b/f/b/a/e;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/b/f/b/a/e;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/b/f/b/a/e;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/g;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/g;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/b/f/b/a/e;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/g;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/g;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/b/f/b/a/g;->f:I

    iput v1, v2, Lcom/google/b/f/b/a/e;->f:I

    iput v0, v2, Lcom/google/b/f/b/a/e;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3601
    check-cast p1, Lcom/google/b/f/b/a/e;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/g;->a(Lcom/google/b/f/b/a/e;)Lcom/google/b/f/b/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3689
    const/4 v0, 0x1

    return v0
.end method
