.class public final Lcom/google/b/f/b/a/dl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/dm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/dj;",
        "Lcom/google/b/f/b/a/dl;",
        ">;",
        "Lcom/google/b/f/b/a/dm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/b/f/b/a/ck;

.field public c:Lcom/google/b/f/b/a/a;

.field public d:Lcom/google/b/f/b/a/cc;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 459
    sget-object v0, Lcom/google/b/f/b/a/dj;->e:Lcom/google/b/f/b/a/dj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 523
    iput-object v1, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    .line 584
    iput-object v1, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    .line 645
    iput-object v1, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    .line 460
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/dj;)Lcom/google/b/f/b/a/dl;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 497
    invoke-static {}, Lcom/google/b/f/b/a/dj;->d()Lcom/google/b/f/b/a/dj;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 508
    :goto_0
    return-object p0

    .line 498
    :cond_0
    iget v0, p1, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/b/f/b/a/ck;->d()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/dl;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_6

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    invoke-static {}, Lcom/google/b/f/b/a/ck;->d()Lcom/google/b/f/b/a/ck;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    invoke-static {v3}, Lcom/google/b/f/b/a/ck;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/b/a/cm;->c()Lcom/google/b/f/b/a/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    .line 501
    :cond_1
    iget v0, p1, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_7

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 502
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/b/f/b/a/dl;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_9

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    invoke-static {v3}, Lcom/google/b/f/b/a/a;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/b/a/c;->c()Lcom/google/b/f/b/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    :goto_6
    iget v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    .line 504
    :cond_2
    iget v0, p1, Lcom/google/b/f/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_a

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 505
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/b/f/b/a/cc;->d()Lcom/google/b/f/b/a/cc;

    move-result-object v0

    :goto_8
    iget v1, p0, Lcom/google/b/f/b/a/dl;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_c

    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    invoke-static {}, Lcom/google/b/f/b/a/cc;->d()Lcom/google/b/f/b/a/cc;

    move-result-object v2

    if-eq v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    invoke-static {v1}, Lcom/google/b/f/b/a/cc;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ce;->a(Lcom/google/b/f/b/a/cc;)Lcom/google/b/f/b/a/ce;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ce;->c()Lcom/google/b/f/b/a/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    :goto_9
    iget v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    .line 507
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 498
    goto/16 :goto_1

    .line 499
    :cond_5
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    goto/16 :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    goto/16 :goto_3

    :cond_7
    move v0, v2

    .line 501
    goto/16 :goto_4

    .line 502
    :cond_8
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    goto :goto_5

    :cond_9
    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    goto :goto_6

    :cond_a
    move v0, v2

    .line 504
    goto :goto_7

    .line 505
    :cond_b
    iget-object v0, p1, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    goto :goto_8

    :cond_c
    iput-object v0, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 451
    new-instance v2, Lcom/google/b/f/b/a/dj;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/dj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/dl;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    iput-object v1, v2, Lcom/google/b/f/b/a/dj;->b:Lcom/google/b/f/b/a/ck;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    iput-object v1, v2, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/b/f/b/a/dl;->d:Lcom/google/b/f/b/a/cc;

    iput-object v1, v2, Lcom/google/b/f/b/a/dj;->d:Lcom/google/b/f/b/a/cc;

    iput v0, v2, Lcom/google/b/f/b/a/dj;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 451
    check-cast p1, Lcom/google/b/f/b/a/dj;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/dl;->a(Lcom/google/b/f/b/a/dj;)Lcom/google/b/f/b/a/dl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 512
    iget v0, p0, Lcom/google/b/f/b/a/dl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 513
    iget-object v0, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/b/f/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 518
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 512
    goto :goto_0

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 518
    goto :goto_2
.end method
