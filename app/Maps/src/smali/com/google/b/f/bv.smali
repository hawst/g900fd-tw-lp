.class public final enum Lcom/google/b/f/bv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/bv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/bv;

.field public static final enum b:Lcom/google/b/f/bv;

.field public static final enum c:Lcom/google/b/f/bv;

.field public static final enum d:Lcom/google/b/f/bv;

.field public static final enum e:Lcom/google/b/f/bv;

.field private static final synthetic g:[Lcom/google/b/f/bv;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1359
    new-instance v0, Lcom/google/b/f/bv;

    const-string v1, "NO_CONTEXT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bv;->a:Lcom/google/b/f/bv;

    .line 1363
    new-instance v0, Lcom/google/b/f/bv;

    const-string v1, "GEL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bv;->b:Lcom/google/b/f/bv;

    .line 1367
    new-instance v0, Lcom/google/b/f/bv;

    const-string v1, "GSA"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bv;->c:Lcom/google/b/f/bv;

    .line 1371
    new-instance v0, Lcom/google/b/f/bv;

    const-string v1, "GMM"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bv;->d:Lcom/google/b/f/bv;

    .line 1375
    new-instance v0, Lcom/google/b/f/bv;

    const-string v1, "CHROME"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bv;->e:Lcom/google/b/f/bv;

    .line 1354
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/f/bv;

    sget-object v1, Lcom/google/b/f/bv;->a:Lcom/google/b/f/bv;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/bv;->b:Lcom/google/b/f/bv;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/bv;->c:Lcom/google/b/f/bv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/bv;->d:Lcom/google/b/f/bv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/bv;->e:Lcom/google/b/f/bv;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/f/bv;->g:[Lcom/google/b/f/bv;

    .line 1420
    new-instance v0, Lcom/google/b/f/bw;

    invoke-direct {v0}, Lcom/google/b/f/bw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1430
    iput p3, p0, Lcom/google/b/f/bv;->f:I

    .line 1431
    return-void
.end method

.method public static a(I)Lcom/google/b/f/bv;
    .locals 1

    .prologue
    .line 1405
    packed-switch p0, :pswitch_data_0

    .line 1411
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1406
    :pswitch_0
    sget-object v0, Lcom/google/b/f/bv;->a:Lcom/google/b/f/bv;

    goto :goto_0

    .line 1407
    :pswitch_1
    sget-object v0, Lcom/google/b/f/bv;->b:Lcom/google/b/f/bv;

    goto :goto_0

    .line 1408
    :pswitch_2
    sget-object v0, Lcom/google/b/f/bv;->c:Lcom/google/b/f/bv;

    goto :goto_0

    .line 1409
    :pswitch_3
    sget-object v0, Lcom/google/b/f/bv;->d:Lcom/google/b/f/bv;

    goto :goto_0

    .line 1410
    :pswitch_4
    sget-object v0, Lcom/google/b/f/bv;->e:Lcom/google/b/f/bv;

    goto :goto_0

    .line 1405
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/bv;
    .locals 1

    .prologue
    .line 1354
    const-class v0, Lcom/google/b/f/bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bv;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/bv;
    .locals 1

    .prologue
    .line 1354
    sget-object v0, Lcom/google/b/f/bv;->g:[Lcom/google/b/f/bv;

    invoke-virtual {v0}, [Lcom/google/b/f/bv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/bv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1401
    iget v0, p0, Lcom/google/b/f/bv;->f:I

    return v0
.end method
