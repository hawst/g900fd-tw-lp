.class public final Lcom/google/b/f/b/a/h;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/o;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/h;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/b/f/b/a/h;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:I

.field e:Z

.field f:Z

.field g:Lcom/google/b/f/b/a/k;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372
    new-instance v0, Lcom/google/b/f/b/a/i;

    invoke-direct {v0}, Lcom/google/b/f/b/a/i;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/h;->PARSER:Lcom/google/n/ax;

    .line 1139
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/h;->k:Lcom/google/n/aw;

    .line 1625
    new-instance v0, Lcom/google/b/f/b/a/h;

    invoke-direct {v0}, Lcom/google/b/f/b/a/h;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/h;->h:Lcom/google/b/f/b/a/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 284
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1068
    iput-byte v0, p0, Lcom/google/b/f/b/a/h;->i:B

    .line 1102
    iput v0, p0, Lcom/google/b/f/b/a/h;->j:I

    .line 285
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    .line 286
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    .line 287
    iput v1, p0, Lcom/google/b/f/b/a/h;->d:I

    .line 288
    iput-boolean v1, p0, Lcom/google/b/f/b/a/h;->e:Z

    .line 289
    iput-boolean v1, p0, Lcom/google/b/f/b/a/h;->f:Z

    .line 290
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 296
    invoke-direct {p0}, Lcom/google/b/f/b/a/h;-><init>()V

    .line 297
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    .line 302
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 303
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 304
    sparse-switch v0, :sswitch_data_0

    .line 309
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 311
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 307
    goto :goto_0

    .line 316
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 317
    iget v1, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/f/b/a/h;->a:I

    .line 318
    iput-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/h;->au:Lcom/google/n/bn;

    throw v0

    .line 322
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 323
    iget v1, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/b/f/b/a/h;->a:I

    .line 324
    iput-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 365
    :catch_1
    move-exception v0

    .line 366
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 367
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 328
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 329
    invoke-static {v0}, Lcom/google/r/b/a/gb;->a(I)Lcom/google/r/b/a/gb;

    move-result-object v1

    .line 330
    if-nez v1, :cond_1

    .line 331
    const/4 v1, 0x3

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 333
    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/b/a/h;->a:I

    .line 334
    iput v0, p0, Lcom/google/b/f/b/a/h;->d:I

    goto :goto_0

    .line 339
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/b/a/h;->a:I

    .line 340
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/b/f/b/a/h;->e:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 344
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/h;->a:I

    .line 345
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/b/f/b/a/h;->f:Z

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_2

    .line 349
    :sswitch_6
    const/4 v0, 0x0

    .line 350
    iget v1, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v6, 0x20

    if-ne v1, v6, :cond_6

    .line 351
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    invoke-static {v0}, Lcom/google/b/f/b/a/k;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v0

    move-object v1, v0

    .line 353
    :goto_3
    sget-object v0, Lcom/google/b/f/b/a/k;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/k;

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    .line 354
    if-eqz v1, :cond_4

    .line 355
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/m;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    .line 356
    invoke-virtual {v1}, Lcom/google/b/f/b/a/m;->c()Lcom/google/b/f/b/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    .line 358
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/b/a/h;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 369
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->au:Lcom/google/n/bn;

    .line 370
    return-void

    :cond_6
    move-object v1, v0

    goto :goto_3

    .line 304
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 282
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1068
    iput-byte v0, p0, Lcom/google/b/f/b/a/h;->i:B

    .line 1102
    iput v0, p0, Lcom/google/b/f/b/a/h;->j:I

    .line 283
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/h;
    .locals 1

    .prologue
    .line 1628
    sget-object v0, Lcom/google/b/f/b/a/h;->h:Lcom/google/b/f/b/a/h;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/j;
    .locals 1

    .prologue
    .line 1201
    new-instance v0, Lcom/google/b/f/b/a/j;

    invoke-direct {v0}, Lcom/google/b/f/b/a/j;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    sget-object v0, Lcom/google/b/f/b/a/h;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 1080
    invoke-virtual {p0}, Lcom/google/b/f/b/a/h;->c()I

    .line 1081
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1084
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1085
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1087
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 1088
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/b/f/b/a/h;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_8

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1090
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1091
    iget-boolean v0, p0, Lcom/google/b/f/b/a/h;->e:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1093
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1094
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/b/f/b/a/h;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 1096
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1097
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/b/f/b/a/k;->d()Lcom/google/b/f/b/a/k;

    move-result-object v0

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1099
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1100
    return-void

    .line 1082
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1085
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1088
    :cond_8
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_9
    move v0, v2

    .line 1091
    goto :goto_3

    :cond_a
    move v1, v2

    .line 1094
    goto :goto_4

    .line 1097
    :cond_b
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1070
    iget-byte v1, p0, Lcom/google/b/f/b/a/h;->i:B

    .line 1071
    if-ne v1, v0, :cond_0

    .line 1075
    :goto_0
    return v0

    .line 1072
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1074
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/h;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104
    iget v0, p0, Lcom/google/b/f/b/a/h;->j:I

    .line 1105
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1134
    :goto_0
    return v0

    .line 1108
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 1110
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1112
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1114
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1116
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1117
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/b/f/b/a/h;->d:I

    .line 1118
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1120
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1121
    iget-boolean v0, p0, Lcom/google/b/f/b/a/h;->e:Z

    .line 1122
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1124
    :cond_3
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1125
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/b/f/b/a/h;->f:Z

    .line 1126
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1128
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1129
    const/4 v3, 0x6

    .line 1130
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/b/f/b/a/k;->d()Lcom/google/b/f/b/a/k;

    move-result-object v0

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1132
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1133
    iput v0, p0, Lcom/google/b/f/b/a/h;->j:I

    goto/16 :goto_0

    .line 1110
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1114
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1118
    :cond_8
    const/16 v0, 0xa

    goto :goto_4

    .line 1130
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    goto :goto_5

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lcom/google/b/f/b/a/h;->newBuilder()Lcom/google/b/f/b/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/j;->a(Lcom/google/b/f/b/a/h;)Lcom/google/b/f/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lcom/google/b/f/b/a/h;->newBuilder()Lcom/google/b/f/b/a/j;

    move-result-object v0

    return-object v0
.end method
