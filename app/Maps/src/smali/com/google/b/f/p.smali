.class public final Lcom/google/b/f/p;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/s;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/p;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/p;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:I

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/b/f/q;

    invoke-direct {v0}, Lcom/google/b/f/q;-><init>()V

    sput-object v0, Lcom/google/b/f/p;->PARSER:Lcom/google/n/ax;

    .line 231
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/p;->h:Lcom/google/n/aw;

    .line 481
    new-instance v0, Lcom/google/b/f/p;

    invoke-direct {v0}, Lcom/google/b/f/p;-><init>()V

    sput-object v0, Lcom/google/b/f/p;->e:Lcom/google/b/f/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 169
    iput-byte v0, p0, Lcom/google/b/f/p;->f:B

    .line 206
    iput v0, p0, Lcom/google/b/f/p;->g:I

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/b/f/p;->b:J

    .line 55
    iput v2, p0, Lcom/google/b/f/p;->c:I

    .line 56
    iput v2, p0, Lcom/google/b/f/p;->d:I

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/b/f/p;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 68
    const/4 v0, 0x0

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 71
    sparse-switch v3, :sswitch_data_0

    .line 76
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/p;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/p;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/p;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/p;->au:Lcom/google/n/bn;

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/p;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/p;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/p;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/b/f/p;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/p;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/p;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/p;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 169
    iput-byte v0, p0, Lcom/google/b/f/p;->f:B

    .line 206
    iput v0, p0, Lcom/google/b/f/p;->g:I

    .line 52
    return-void
.end method

.method public static d()Lcom/google/b/f/p;
    .locals 1

    .prologue
    .line 484
    sget-object v0, Lcom/google/b/f/p;->e:Lcom/google/b/f/p;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/r;
    .locals 1

    .prologue
    .line 293
    new-instance v0, Lcom/google/b/f/r;

    invoke-direct {v0}, Lcom/google/b/f/r;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/b/f/p;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 193
    invoke-virtual {p0}, Lcom/google/b/f/p;->c()I

    .line 194
    iget v0, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 195
    iget-wide v0, p0, Lcom/google/b/f/p;->b:J

    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 197
    :cond_0
    iget v0, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 198
    iget v0, p0, Lcom/google/b/f/p;->c:I

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 200
    :cond_1
    iget v0, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 201
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/p;->d:I

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 204
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    iget-byte v2, p0, Lcom/google/b/f/p;->f:B

    .line 172
    if-ne v2, v0, :cond_0

    .line 188
    :goto_0
    return v0

    .line 173
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 175
    :cond_1
    iget v2, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 176
    iput-byte v1, p0, Lcom/google/b/f/p;->f:B

    move v0, v1

    .line 177
    goto :goto_0

    :cond_2
    move v2, v1

    .line 175
    goto :goto_1

    .line 179
    :cond_3
    iget v2, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 180
    iput-byte v1, p0, Lcom/google/b/f/p;->f:B

    move v0, v1

    .line 181
    goto :goto_0

    :cond_4
    move v2, v1

    .line 179
    goto :goto_2

    .line 183
    :cond_5
    iget v2, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 184
    iput-byte v1, p0, Lcom/google/b/f/p;->f:B

    move v0, v1

    .line 185
    goto :goto_0

    :cond_6
    move v2, v1

    .line 183
    goto :goto_3

    .line 187
    :cond_7
    iput-byte v0, p0, Lcom/google/b/f/p;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 208
    iget v0, p0, Lcom/google/b/f/p;->g:I

    .line 209
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 226
    :goto_0
    return v0

    .line 212
    :cond_0
    iget v0, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 213
    iget-wide v2, p0, Lcom/google/b/f/p;->b:J

    .line 214
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_1
    iget v2, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 217
    iget v2, p0, Lcom/google/b/f/p;->c:I

    .line 218
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 220
    :cond_1
    iget v2, p0, Lcom/google/b/f/p;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 221
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/b/f/p;->d:I

    .line 222
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/google/b/f/p;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    iput v0, p0, Lcom/google/b/f/p;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/b/f/p;->newBuilder()Lcom/google/b/f/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/r;->a(Lcom/google/b/f/p;)Lcom/google/b/f/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/b/f/p;->newBuilder()Lcom/google/b/f/r;

    move-result-object v0

    return-object v0
.end method
