.class public final Lcom/google/b/f/b/a/ag;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/aj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/ae;",
        "Lcom/google/b/f/b/a/ag;",
        ">;",
        "Lcom/google/b/f/b/a/aj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/google/b/f/b/a/ae;->e:Lcom/google/b/f/b/a/ae;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 449
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/ag;->b:I

    .line 392
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/ae;)Lcom/google/b/f/b/a/ag;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 429
    invoke-static {}, Lcom/google/b/f/b/a/ae;->d()Lcom/google/b/f/b/a/ae;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 440
    :goto_0
    return-object p0

    .line 430
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 431
    iget v2, p1, Lcom/google/b/f/b/a/ae;->b:I

    invoke-static {v2}, Lcom/google/b/f/b/a/ah;->a(I)Lcom/google/b/f/b/a/ah;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 430
    goto :goto_1

    .line 431
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ag;->a:I

    iget v2, v2, Lcom/google/b/f/b/a/ah;->j:I

    iput v2, p0, Lcom/google/b/f/b/a/ag;->b:I

    .line 433
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 434
    iget v2, p1, Lcom/google/b/f/b/a/ae;->c:I

    iget v3, p0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ag;->a:I

    iput v2, p0, Lcom/google/b/f/b/a/ag;->c:I

    .line 436
    :cond_5
    iget v2, p1, Lcom/google/b/f/b/a/ae;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_6

    .line 437
    iget v0, p1, Lcom/google/b/f/b/a/ae;->d:I

    iget v1, p0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/b/a/ag;->a:I

    iput v0, p0, Lcom/google/b/f/b/a/ag;->d:I

    .line 439
    :cond_6
    iget-object v0, p1, Lcom/google/b/f/b/a/ae;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v2, v1

    .line 433
    goto :goto_2

    :cond_8
    move v0, v1

    .line 436
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 383
    new-instance v2, Lcom/google/b/f/b/a/ae;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/ae;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/ag;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/b/f/b/a/ag;->b:I

    iput v1, v2, Lcom/google/b/f/b/a/ae;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/b/f/b/a/ag;->c:I

    iput v1, v2, Lcom/google/b/f/b/a/ae;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/ag;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/ae;->d:I

    iput v0, v2, Lcom/google/b/f/b/a/ae;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 383
    check-cast p1, Lcom/google/b/f/b/a/ae;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/ag;->a(Lcom/google/b/f/b/a/ae;)Lcom/google/b/f/b/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x1

    return v0
.end method
