.class public final Lcom/google/b/f/b/a/ac;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/ad;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/aa;",
        "Lcom/google/b/f/b/a/ac;",
        ">;",
        "Lcom/google/b/f/b/a/ad;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 417
    sget-object v0, Lcom/google/b/f/b/a/aa;->j:Lcom/google/b/f/b/a/aa;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 541
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/ac;->b:I

    .line 577
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->e:Lcom/google/n/ao;

    .line 636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    .line 695
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->f:Lcom/google/n/ao;

    .line 754
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->g:Lcom/google/n/ao;

    .line 813
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->h:Lcom/google/n/ao;

    .line 872
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->i:Lcom/google/n/ao;

    .line 931
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/ac;->d:Lcom/google/n/ao;

    .line 418
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/ac;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 499
    invoke-static {}, Lcom/google/b/f/b/a/aa;->d()Lcom/google/b/f/b/a/aa;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 532
    :goto_0
    return-object p0

    .line 500
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 501
    iget v2, p1, Lcom/google/b/f/b/a/aa;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/av;->a(I)Lcom/google/r/b/a/av;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 500
    goto :goto_1

    .line 501
    :cond_3
    iget v3, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ac;->a:I

    iget v2, v2, Lcom/google/r/b/a/av;->v:I

    iput v2, p0, Lcom/google/b/f/b/a/ac;->b:I

    .line 503
    :cond_4
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 504
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 505
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 507
    :cond_5
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 508
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 509
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 511
    :cond_6
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 512
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 513
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 515
    :cond_7
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 516
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 517
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 519
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 520
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 521
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 523
    :cond_9
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 524
    iget-object v2, p0, Lcom/google/b/f/b/a/ac;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/b/a/aa;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 525
    iget v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 527
    :cond_a
    iget v2, p1, Lcom/google/b/f/b/a/aa;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 528
    iget-object v0, p0, Lcom/google/b/f/b/a/ac;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/b/f/b/a/aa;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 529
    iget v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 531
    :cond_b
    iget-object v0, p1, Lcom/google/b/f/b/a/aa;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 503
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 507
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 511
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 515
    goto :goto_5

    :cond_10
    move v2, v1

    .line 519
    goto :goto_6

    :cond_11
    move v2, v1

    .line 523
    goto :goto_7

    :cond_12
    move v0, v1

    .line 527
    goto :goto_8
.end method

.method public final a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ac;
    .locals 2

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 890
    :cond_0
    iget-object v0, p0, Lcom/google/b/f/b/a/ac;->i:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 892
    iget v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 893
    return-object p0
.end method

.method public final a(Lcom/google/b/f/b/a/cy;)Lcom/google/b/f/b/a/ac;
    .locals 2

    .prologue
    .line 769
    if-nez p1, :cond_0

    .line 770
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/google/b/f/b/a/ac;->g:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 774
    iget v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 775
    return-object p0
.end method

.method public final a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/ac;
    .locals 2

    .prologue
    .line 592
    if-nez p1, :cond_0

    .line 593
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/google/b/f/b/a/ac;->e:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 597
    iget v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/ac;->a:I

    .line 598
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 409
    new-instance v2, Lcom/google/b/f/b/a/aa;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/aa;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/ac;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/b/f/b/a/ac;->b:I

    iput v4, v2, Lcom/google/b/f/b/a/aa;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/b/f/b/a/aa;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/f/b/a/ac;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/b/f/b/a/aa;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/b/f/b/a/ac;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/b/f/b/a/ac;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/b/f/b/a/aa;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 409
    check-cast p1, Lcom/google/b/f/b/a/aa;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x1

    return v0
.end method
