.class public final enum Lcom/google/b/f/bt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/bt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/bt;

.field public static final enum b:Lcom/google/b/f/bt;

.field public static final enum c:Lcom/google/b/f/bt;

.field private static final synthetic e:[Lcom/google/b/f/bt;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 960
    new-instance v0, Lcom/google/b/f/bt;

    const-string v1, "VALID_SIGNATURE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/b/f/bt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bt;->a:Lcom/google/b/f/bt;

    .line 964
    new-instance v0, Lcom/google/b/f/bt;

    const-string v1, "INVALID_SIGNATURE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/bt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bt;->b:Lcom/google/b/f/bt;

    .line 968
    new-instance v0, Lcom/google/b/f/bt;

    const-string v1, "EXEMPTED"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/b/f/bt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bt;->c:Lcom/google/b/f/bt;

    .line 955
    new-array v0, v5, [Lcom/google/b/f/bt;

    sget-object v1, Lcom/google/b/f/bt;->a:Lcom/google/b/f/bt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/bt;->b:Lcom/google/b/f/bt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/bt;->c:Lcom/google/b/f/bt;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/b/f/bt;->e:[Lcom/google/b/f/bt;

    .line 1003
    new-instance v0, Lcom/google/b/f/bu;

    invoke-direct {v0}, Lcom/google/b/f/bu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1012
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1013
    iput p3, p0, Lcom/google/b/f/bt;->d:I

    .line 1014
    return-void
.end method

.method public static a(I)Lcom/google/b/f/bt;
    .locals 1

    .prologue
    .line 990
    packed-switch p0, :pswitch_data_0

    .line 994
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 991
    :pswitch_0
    sget-object v0, Lcom/google/b/f/bt;->a:Lcom/google/b/f/bt;

    goto :goto_0

    .line 992
    :pswitch_1
    sget-object v0, Lcom/google/b/f/bt;->b:Lcom/google/b/f/bt;

    goto :goto_0

    .line 993
    :pswitch_2
    sget-object v0, Lcom/google/b/f/bt;->c:Lcom/google/b/f/bt;

    goto :goto_0

    .line 990
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/bt;
    .locals 1

    .prologue
    .line 955
    const-class v0, Lcom/google/b/f/bt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bt;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/bt;
    .locals 1

    .prologue
    .line 955
    sget-object v0, Lcom/google/b/f/bt;->e:[Lcom/google/b/f/bt;

    invoke-virtual {v0}, [Lcom/google/b/f/bt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/bt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 986
    iget v0, p0, Lcom/google/b/f/bt;->d:I

    return v0
.end method
