.class public final Lcom/google/b/f/aj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/al;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/ah;",
        "Lcom/google/b/f/aj;",
        ">;",
        "Lcom/google/b/f/al;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:I

.field public d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1463
    sget-object v0, Lcom/google/b/f/ah;->e:Lcom/google/b/f/ah;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1554
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/aj;->a:I

    .line 1714
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aj;->d:Lcom/google/n/ao;

    .line 1464
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/ah;)Lcom/google/b/f/aj;
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 1506
    invoke-static {}, Lcom/google/b/f/ah;->d()Lcom/google/b/f/ah;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1535
    :goto_0
    return-object p0

    .line 1507
    :cond_0
    iget v0, p1, Lcom/google/b/f/ah;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    .line 1508
    iget-object v0, p0, Lcom/google/b/f/aj;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1509
    iget v0, p0, Lcom/google/b/f/aj;->c:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/aj;->c:I

    .line 1511
    :cond_1
    sget-object v0, Lcom/google/b/f/w;->a:[I

    iget v1, p1, Lcom/google/b/f/ah;->b:I

    invoke-static {v1}, Lcom/google/b/f/ak;->a(I)Lcom/google/b/f/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/f/ak;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1528
    :goto_2
    iget-object v0, p1, Lcom/google/b/f/ah;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1507
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1513
    :pswitch_0
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-eq v0, v2, :cond_3

    .line 1514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    .line 1516
    :cond_3
    iget-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1517
    iget-object v1, p1, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 1516
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1518
    iput v2, p0, Lcom/google/b/f/aj;->a:I

    goto :goto_2

    .line 1522
    :pswitch_1
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-eq v0, v3, :cond_4

    .line 1523
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    .line 1525
    :cond_4
    iget-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1526
    iget-object v1, p1, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 1525
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1527
    iput v3, p0, Lcom/google/b/f/aj;->a:I

    goto :goto_2

    .line 1511
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 1455
    new-instance v4, Lcom/google/b/f/ah;

    invoke-direct {v4, p0}, Lcom/google/b/f/ah;-><init>(Lcom/google/n/v;)V

    iget v5, p0, Lcom/google/b/f/aj;->c:I

    iget v0, p0, Lcom/google/b/f/aj;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/b/f/ah;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_1
    and-int/lit8 v0, v5, 0x4

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_0
    iget-object v1, v4, Lcom/google/b/f/ah;->d:Lcom/google/n/ao;

    iget-object v2, p0, Lcom/google/b/f/aj;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    iget-object v5, p0, Lcom/google/b/f/aj;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    iput v0, v4, Lcom/google/b/f/ah;->a:I

    iget v0, p0, Lcom/google/b/f/aj;->a:I

    iput v0, v4, Lcom/google/b/f/ah;->b:I

    return-object v4

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1455
    check-cast p1, Lcom/google/b/f/ah;

    invoke-virtual {p0, p1}, Lcom/google/b/f/aj;->a(Lcom/google/b/f/ah;)Lcom/google/b/f/aj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1539
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1540
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    :goto_1
    invoke-virtual {v0}, Lcom/google/b/f/p;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1551
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 1539
    goto :goto_0

    .line 1540
    :cond_1
    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v0

    goto :goto_1

    .line 1545
    :cond_2
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-ne v0, v4, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 1546
    iget v0, p0, Lcom/google/b/f/aj;->a:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/l;->d()Lcom/google/b/f/l;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/l;

    :goto_4
    invoke-virtual {v0}, Lcom/google/b/f/l;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1548
    goto :goto_2

    :cond_3
    move v0, v1

    .line 1545
    goto :goto_3

    .line 1546
    :cond_4
    invoke-static {}, Lcom/google/b/f/l;->d()Lcom/google/b/f/l;

    move-result-object v0

    goto :goto_4

    :cond_5
    move v0, v2

    .line 1551
    goto :goto_2
.end method
