.class public final Lcom/google/b/f/b/a/ao;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/ar;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ao;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/b/a/ao;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:F

.field d:F

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/b/f/b/a/ap;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ap;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ao;->PARSER:Lcom/google/n/ax;

    .line 183
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/ao;->h:Lcom/google/n/aw;

    .line 421
    new-instance v0, Lcom/google/b/f/b/a/ao;

    invoke-direct {v0}, Lcom/google/b/f/b/a/ao;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/ao;->e:Lcom/google/b/f/b/a/ao;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 133
    iput-byte v0, p0, Lcom/google/b/f/b/a/ao;->f:B

    .line 158
    iput v0, p0, Lcom/google/b/f/b/a/ao;->g:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/ao;->b:I

    .line 19
    iput v1, p0, Lcom/google/b/f/b/a/ao;->c:F

    .line 20
    iput v1, p0, Lcom/google/b/f/b/a/ao;->d:F

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/b/f/b/a/ao;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    .line 48
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ao;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/ao;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ao;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/ao;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/ao;->d:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/ao;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 133
    iput-byte v0, p0, Lcom/google/b/f/b/a/ao;->f:B

    .line 158
    iput v0, p0, Lcom/google/b/f/b/a/ao;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/ao;
    .locals 1

    .prologue
    .line 424
    sget-object v0, Lcom/google/b/f/b/a/ao;->e:Lcom/google/b/f/b/a/ao;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/aq;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/b/f/b/a/aq;

    invoke-direct {v0}, Lcom/google/b/f/b/a/aq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/ao;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/b/f/b/a/ao;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 145
    invoke-virtual {p0}, Lcom/google/b/f/b/a/ao;->c()I

    .line 146
    iget v0, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 147
    iget v0, p0, Lcom/google/b/f/b/a/ao;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 149
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 150
    iget v0, p0, Lcom/google/b/f/b/a/ao;->c:F

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 152
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 153
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/ao;->d:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/b/a/ao;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 156
    return-void

    .line 147
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 135
    iget-byte v1, p0, Lcom/google/b/f/b/a/ao;->f:B

    .line 136
    if-ne v1, v0, :cond_0

    .line 140
    :goto_0
    return v0

    .line 137
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/ao;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 160
    iget v0, p0, Lcom/google/b/f/b/a/ao;->g:I

    .line 161
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 178
    :goto_0
    return v0

    .line 164
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 165
    iget v0, p0, Lcom/google/b/f/b/a/ao;->b:I

    .line 166
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 168
    :goto_2
    iget v2, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 169
    iget v2, p0, Lcom/google/b/f/b/a/ao;->c:F

    .line 170
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 172
    :cond_1
    iget v2, p0, Lcom/google/b/f/b/a/ao;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 173
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/b/f/b/a/ao;->d:F

    .line 174
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/google/b/f/b/a/ao;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    iput v0, p0, Lcom/google/b/f/b/a/ao;->g:I

    goto :goto_0

    .line 166
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/aq;->a(Lcom/google/b/f/b/a/ao;)Lcom/google/b/f/b/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v0

    return-object v0
.end method
