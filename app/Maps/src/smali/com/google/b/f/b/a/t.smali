.class public final enum Lcom/google/b/f/b/a/t;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/t;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/t;

.field public static final enum b:Lcom/google/b/f/b/a/t;

.field public static final enum c:Lcom/google/b/f/b/a/t;

.field public static final enum d:Lcom/google/b/f/b/a/t;

.field public static final enum e:Lcom/google/b/f/b/a/t;

.field public static final enum f:Lcom/google/b/f/b/a/t;

.field private static final synthetic h:[Lcom/google/b/f/b/a/t;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 119
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "TURNED_SCREEN_OFF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    .line 123
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "USER_INTERACTION"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    .line 127
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "PLUGGED_IN"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->c:Lcom/google/b/f/b/a/t;

    .line 131
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "REACHED_NEXT_STEP"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->d:Lcom/google/b/f/b/a/t;

    .line 135
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "ROUTE_AROUND_TRAFFIC"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->e:Lcom/google/b/f/b/a/t;

    .line 139
    new-instance v0, Lcom/google/b/f/b/a/t;

    const-string v1, "ROUTE_AROUND_CLOSURE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/google/b/f/b/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/t;->f:Lcom/google/b/f/b/a/t;

    .line 114
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/b/f/b/a/t;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/b/a/t;->c:Lcom/google/b/f/b/a/t;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/b/a/t;->d:Lcom/google/b/f/b/a/t;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/b/a/t;->e:Lcom/google/b/f/b/a/t;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/f/b/a/t;->f:Lcom/google/b/f/b/a/t;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/b/f/b/a/t;->h:[Lcom/google/b/f/b/a/t;

    .line 189
    new-instance v0, Lcom/google/b/f/b/a/u;

    invoke-direct {v0}, Lcom/google/b/f/b/a/u;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 199
    iput p3, p0, Lcom/google/b/f/b/a/t;->g:I

    .line 200
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/t;
    .locals 1

    .prologue
    .line 173
    packed-switch p0, :pswitch_data_0

    .line 180
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 175
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 176
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/t;->c:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 177
    :pswitch_3
    sget-object v0, Lcom/google/b/f/b/a/t;->d:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 178
    :pswitch_4
    sget-object v0, Lcom/google/b/f/b/a/t;->e:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 179
    :pswitch_5
    sget-object v0, Lcom/google/b/f/b/a/t;->f:Lcom/google/b/f/b/a/t;

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/t;
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/google/b/f/b/a/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/t;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/t;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/google/b/f/b/a/t;->h:[Lcom/google/b/f/b/a/t;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/t;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/google/b/f/b/a/t;->g:I

    return v0
.end method
