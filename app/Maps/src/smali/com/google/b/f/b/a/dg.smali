.class public final Lcom/google/b/f/b/a/dg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/dn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dg;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/b/f/b/a/dg;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/b/f/b/a/dh;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dh;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dg;->PARSER:Lcom/google/n/ax;

    .line 1007
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/dg;->m:Lcom/google/n/aw;

    .line 1729
    new-instance v0, Lcom/google/b/f/b/a/dg;

    invoke-direct {v0}, Lcom/google/b/f/b/a/dg;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/dg;->j:Lcom/google/b/f/b/a/dg;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    .line 830
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    .line 889
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    .line 904
    iput-byte v3, p0, Lcom/google/b/f/b/a/dg;->k:B

    .line 962
    iput v3, p0, Lcom/google/b/f/b/a/dg;->l:I

    .line 18
    iput v2, p0, Lcom/google/b/f/b/a/dg;->b:I

    .line 19
    iput v4, p0, Lcom/google/b/f/b/a/dg;->c:I

    .line 20
    iput v4, p0, Lcom/google/b/f/b/a/dg;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    .line 25
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x40

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/b/f/b/a/dg;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 40
    sparse-switch v4, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 53
    invoke-static {v4}, Lcom/google/r/b/a/a;->a(I)Lcom/google/r/b/a/a;

    move-result-object v5

    .line 54
    if-nez v5, :cond_2

    .line 55
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x40

    if-ne v1, v7, :cond_1

    .line 124
    iget-object v1, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    .line 126
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/dg;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    .line 58
    iput v4, p0, Lcom/google/b/f/b/a/dg;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    .line 120
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 121
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    iget-object v4, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 64
    iget v4, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/b/f/b/a/dg;->a:I

    goto :goto_0

    .line 68
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 69
    iget v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    .line 70
    iput-object v4, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    goto :goto_0

    .line 74
    :sswitch_4
    iget-object v4, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 75
    iget v4, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/b/f/b/a/dg;->a:I

    goto/16 :goto_0

    .line 79
    :sswitch_5
    and-int/lit8 v4, v1, 0x40

    if-eq v4, v7, :cond_3

    .line 80
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    .line 82
    or-int/lit8 v1, v1, 0x40

    .line 84
    :cond_3
    iget-object v4, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 84
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 89
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 90
    invoke-static {v4}, Lcom/google/b/f/cj;->a(I)Lcom/google/b/f/cj;

    move-result-object v5

    .line 91
    if-nez v5, :cond_4

    .line 92
    const/4 v5, 0x6

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 94
    :cond_4
    iget v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    .line 95
    iput v4, p0, Lcom/google/b/f/b/a/dg;->c:I

    goto/16 :goto_0

    .line 100
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 101
    invoke-static {v4}, Lcom/google/b/f/ch;->a(I)Lcom/google/b/f/ch;

    move-result-object v5

    .line 102
    if-nez v5, :cond_5

    .line 103
    const/4 v5, 0x7

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 105
    :cond_5
    iget v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/b/f/b/a/dg;->a:I

    .line 106
    iput v4, p0, Lcom/google/b/f/b/a/dg;->d:I

    goto/16 :goto_0

    .line 111
    :sswitch_8
    iget-object v4, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 112
    iget v4, p0, Lcom/google/b/f/b/a/dg;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/b/f/b/a/dg;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 123
    :cond_6
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v7, :cond_7

    .line 124
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    .line 126
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->au:Lcom/google/n/bn;

    .line 127
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x82 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    .line 830
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    .line 889
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    .line 904
    iput-byte v1, p0, Lcom/google/b/f/b/a/dg;->k:B

    .line 962
    iput v1, p0, Lcom/google/b/f/b/a/dg;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/dg;
    .locals 1

    .prologue
    .line 1732
    sget-object v0, Lcom/google/b/f/b/a/dg;->j:Lcom/google/b/f/b/a/dg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/di;
    .locals 1

    .prologue
    .line 1069
    new-instance v0, Lcom/google/b/f/b/a/di;

    invoke-direct {v0}, Lcom/google/b/f/b/a/di;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/dg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/google/b/f/b/a/dg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 934
    invoke-virtual {p0}, Lcom/google/b/f/b/a/dg;->c()I

    .line 935
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 936
    iget v0, p0, Lcom/google/b/f/b/a/dg;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 938
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 939
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 941
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 942
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 944
    :cond_2
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_3

    .line 945
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 947
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 948
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 947
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 936
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 942
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 950
    :cond_6
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_7

    .line 951
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/b/f/b/a/dg;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 953
    :cond_7
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_8

    .line 954
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/b/f/b/a/dg;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 956
    :cond_8
    :goto_4
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 957
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 959
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 960
    return-void

    .line 951
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 954
    :cond_b
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 906
    iget-byte v0, p0, Lcom/google/b/f/b/a/dg;->k:B

    .line 907
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 929
    :cond_0
    :goto_0
    return v2

    .line 908
    :cond_1
    if-eqz v0, :cond_0

    .line 910
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 911
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 912
    iput-byte v2, p0, Lcom/google/b/f/b/a/dg;->k:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 910
    goto :goto_1

    :cond_3
    move v1, v2

    .line 916
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 917
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 918
    iput-byte v2, p0, Lcom/google/b/f/b/a/dg;->k:B

    goto :goto_0

    .line 916
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 922
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 923
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/dj;->d()Lcom/google/b/f/b/a/dj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dj;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dj;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 924
    iput-byte v2, p0, Lcom/google/b/f/b/a/dg;->k:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 922
    goto :goto_3

    .line 928
    :cond_7
    iput-byte v3, p0, Lcom/google/b/f/b/a/dg;->k:B

    move v2, v3

    .line 929
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 964
    iget v0, p0, Lcom/google/b/f/b/a/dg;->l:I

    .line 965
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1002
    :goto_0
    return v0

    .line 968
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 969
    iget v0, p0, Lcom/google/b/f/b/a/dg;->b:I

    .line 970
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 972
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_b

    .line 973
    iget-object v3, p0, Lcom/google/b/f/b/a/dg;->g:Lcom/google/n/ao;

    .line 974
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 976
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1

    .line 977
    const/4 v4, 0x3

    .line 978
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/dg;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 980
    :cond_1
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2

    .line 981
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->f:Lcom/google/n/ao;

    .line 982
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_2
    move v4, v3

    move v3, v2

    .line 984
    :goto_5
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 985
    const/4 v5, 0x5

    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->h:Ljava/util/List;

    .line 986
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 984
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_3
    move v0, v1

    .line 970
    goto/16 :goto_1

    .line 978
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 988
    :cond_5
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 989
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/b/f/b/a/dg;->c:I

    .line 990
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 992
    :cond_6
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_8

    .line 993
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/b/f/b/a/dg;->d:I

    .line 994
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 996
    :cond_8
    iget v0, p0, Lcom/google/b/f/b/a/dg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 997
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/b/f/b/a/dg;->i:Lcom/google/n/ao;

    .line 998
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1000
    :cond_9
    iget-object v0, p0, Lcom/google/b/f/b/a/dg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1001
    iput v0, p0, Lcom/google/b/f/b/a/dg;->l:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 990
    goto :goto_6

    :cond_b
    move v3, v0

    goto/16 :goto_3

    :cond_c
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/dg;->newBuilder()Lcom/google/b/f/b/a/di;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/di;->a(Lcom/google/b/f/b/a/dg;)Lcom/google/b/f/b/a/di;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/dg;->newBuilder()Lcom/google/b/f/b/a/di;

    move-result-object v0

    return-object v0
.end method
