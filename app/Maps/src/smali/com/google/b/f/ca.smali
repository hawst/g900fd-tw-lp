.class public final enum Lcom/google/b/f/ca;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/ca;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/ca;

.field public static final enum b:Lcom/google/b/f/ca;

.field public static final enum c:Lcom/google/b/f/ca;

.field public static final enum d:Lcom/google/b/f/ca;

.field public static final enum e:Lcom/google/b/f/ca;

.field private static final synthetic g:[Lcom/google/b/f/ca;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2210
    new-instance v0, Lcom/google/b/f/ca;

    const-string v1, "COMPLETE_SERVER"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ca;->a:Lcom/google/b/f/ca;

    .line 2214
    new-instance v0, Lcom/google/b/f/ca;

    const-string v1, "CONTENT_PROVIDERS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ca;->b:Lcom/google/b/f/ca;

    .line 2218
    new-instance v0, Lcom/google/b/f/ca;

    const-string v1, "ICING"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ca;->c:Lcom/google/b/f/ca;

    .line 2222
    new-instance v0, Lcom/google/b/f/ca;

    const-string v1, "SSB_CONTEXT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/b/f/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ca;->d:Lcom/google/b/f/ca;

    .line 2226
    new-instance v0, Lcom/google/b/f/ca;

    const-string v1, "NOW_PROMO"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/b/f/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/ca;->e:Lcom/google/b/f/ca;

    .line 2205
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/f/ca;

    sget-object v1, Lcom/google/b/f/ca;->a:Lcom/google/b/f/ca;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/ca;->b:Lcom/google/b/f/ca;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/ca;->c:Lcom/google/b/f/ca;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/ca;->d:Lcom/google/b/f/ca;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/f/ca;->e:Lcom/google/b/f/ca;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/f/ca;->g:[Lcom/google/b/f/ca;

    .line 2271
    new-instance v0, Lcom/google/b/f/cb;

    invoke-direct {v0}, Lcom/google/b/f/cb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2281
    iput p3, p0, Lcom/google/b/f/ca;->f:I

    .line 2282
    return-void
.end method

.method public static a(I)Lcom/google/b/f/ca;
    .locals 1

    .prologue
    .line 2256
    packed-switch p0, :pswitch_data_0

    .line 2262
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2257
    :pswitch_0
    sget-object v0, Lcom/google/b/f/ca;->a:Lcom/google/b/f/ca;

    goto :goto_0

    .line 2258
    :pswitch_1
    sget-object v0, Lcom/google/b/f/ca;->b:Lcom/google/b/f/ca;

    goto :goto_0

    .line 2259
    :pswitch_2
    sget-object v0, Lcom/google/b/f/ca;->c:Lcom/google/b/f/ca;

    goto :goto_0

    .line 2260
    :pswitch_3
    sget-object v0, Lcom/google/b/f/ca;->d:Lcom/google/b/f/ca;

    goto :goto_0

    .line 2261
    :pswitch_4
    sget-object v0, Lcom/google/b/f/ca;->e:Lcom/google/b/f/ca;

    goto :goto_0

    .line 2256
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/ca;
    .locals 1

    .prologue
    .line 2205
    const-class v0, Lcom/google/b/f/ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ca;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/ca;
    .locals 1

    .prologue
    .line 2205
    sget-object v0, Lcom/google/b/f/ca;->g:[Lcom/google/b/f/ca;

    invoke-virtual {v0}, [Lcom/google/b/f/ca;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/ca;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2252
    iget v0, p0, Lcom/google/b/f/ca;->f:I

    return v0
.end method
