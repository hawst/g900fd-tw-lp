.class public final Lcom/google/b/f/b/a/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/a;",
        "Lcom/google/b/f/b/a/c;",
        ">;",
        "Lcom/google/b/f/b/a/d;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lcom/google/b/f/b/a/a;->d:Lcom/google/b/f/b/a/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 370
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/c;->b:Ljava/lang/Object;

    .line 447
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    .line 306
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 338
    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 355
    :goto_0
    return-object p0

    .line 339
    :cond_0
    iget v1, p1, Lcom/google/b/f/b/a/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 340
    iget v0, p0, Lcom/google/b/f/b/a/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b/a/c;->a:I

    .line 341
    iget-object v0, p1, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/b/a/c;->b:Ljava/lang/Object;

    .line 344
    :cond_1
    iget-object v0, p1, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 345
    iget-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 346
    iget-object v0, p1, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    .line 347
    iget v0, p0, Lcom/google/b/f/b/a/c;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/b/f/b/a/c;->a:I

    .line 354
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/b/f/b/a/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 339
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 349
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/b/a/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b/a/c;->a:I

    .line 350
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/b/f/b/a/c;->c()Lcom/google/b/f/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 297
    check-cast p1, Lcom/google/b/f/b/a/a;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/c;->a(Lcom/google/b/f/b/a/a;)Lcom/google/b/f/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 359
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    :goto_1
    return v2

    .line 359
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 365
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/b/f/b/a/a;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 321
    new-instance v2, Lcom/google/b/f/b/a/a;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/a;-><init>(Lcom/google/n/v;)V

    .line 322
    iget v3, p0, Lcom/google/b/f/b/a/c;->a:I

    .line 323
    const/4 v1, 0x0

    .line 324
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    .line 327
    :goto_0
    iget-object v1, p0, Lcom/google/b/f/b/a/c;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    .line 328
    iget v1, p0, Lcom/google/b/f/b/a/c;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 329
    iget-object v1, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    .line 330
    iget v1, p0, Lcom/google/b/f/b/a/c;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/b/f/b/a/c;->a:I

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/google/b/f/b/a/c;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/b/f/b/a/a;->c:Ljava/util/List;

    .line 333
    iput v0, v2, Lcom/google/b/f/b/a/a;->a:I

    .line 334
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
