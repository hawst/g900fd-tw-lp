.class public final enum Lcom/google/b/f/b/a/bl;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/b/a/bl;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/b/a/bl;

.field public static final enum b:Lcom/google/b/f/b/a/bl;

.field public static final enum c:Lcom/google/b/f/b/a/bl;

.field private static final synthetic e:[Lcom/google/b/f/b/a/bl;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 318
    new-instance v0, Lcom/google/b/f/b/a/bl;

    const-string v1, "GPS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/b/f/b/a/bl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bl;->a:Lcom/google/b/f/b/a/bl;

    .line 322
    new-instance v0, Lcom/google/b/f/b/a/bl;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/b/f/b/a/bl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bl;->b:Lcom/google/b/f/b/a/bl;

    .line 326
    new-instance v0, Lcom/google/b/f/b/a/bl;

    const-string v1, "FUSED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/b/f/b/a/bl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/b/a/bl;->c:Lcom/google/b/f/b/a/bl;

    .line 313
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/f/b/a/bl;

    sget-object v1, Lcom/google/b/f/b/a/bl;->a:Lcom/google/b/f/b/a/bl;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/b/a/bl;->b:Lcom/google/b/f/b/a/bl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/f/b/a/bl;->c:Lcom/google/b/f/b/a/bl;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/f/b/a/bl;->e:[Lcom/google/b/f/b/a/bl;

    .line 361
    new-instance v0, Lcom/google/b/f/b/a/bm;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 371
    iput p3, p0, Lcom/google/b/f/b/a/bl;->d:I

    .line 372
    return-void
.end method

.method public static a(I)Lcom/google/b/f/b/a/bl;
    .locals 1

    .prologue
    .line 348
    packed-switch p0, :pswitch_data_0

    .line 352
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 349
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/bl;->a:Lcom/google/b/f/b/a/bl;

    goto :goto_0

    .line 350
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/bl;->b:Lcom/google/b/f/b/a/bl;

    goto :goto_0

    .line 351
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/bl;->c:Lcom/google/b/f/b/a/bl;

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/b/a/bl;
    .locals 1

    .prologue
    .line 313
    const-class v0, Lcom/google/b/f/b/a/bl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/bl;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/b/a/bl;
    .locals 1

    .prologue
    .line 313
    sget-object v0, Lcom/google/b/f/b/a/bl;->e:[Lcom/google/b/f/b/a/bl;

    invoke-virtual {v0}, [Lcom/google/b/f/b/a/bl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/b/a/bl;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/google/b/f/b/a/bl;->d:I

    return v0
.end method
