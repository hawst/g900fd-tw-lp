.class public final Lcom/google/b/f/aq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/ax;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/an;",
        "Lcom/google/b/f/aq;",
        ">;",
        "Lcom/google/b/f/ax;"
    }
.end annotation


# instance fields
.field private A:Lcom/google/n/ao;

.field private B:Ljava/lang/Object;

.field private C:Lcom/google/n/ao;

.field private D:I

.field private E:Ljava/lang/Object;

.field private F:Lcom/google/n/ao;

.field private G:Lcom/google/n/ao;

.field private H:Lcom/google/n/ao;

.field private J:Lcom/google/n/ao;

.field private K:Z

.field private L:Lcom/google/n/ao;

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private N:Lcom/google/n/ao;

.field private O:Z

.field private P:J

.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:Lcom/google/n/aq;

.field private g:I

.field private h:I

.field private i:Lcom/google/n/aq;

.field private j:Lcom/google/b/f/ar;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:D

.field private o:I

.field private p:Ljava/lang/Object;

.field private q:Ljava/lang/Object;

.field private r:Z

.field private s:Lcom/google/n/ao;

.field private t:Lcom/google/n/ao;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private w:I

.field private x:Z

.field private y:Lcom/google/n/ao;

.field private z:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12657
    sget-object v0, Lcom/google/b/f/an;->P:Lcom/google/b/f/an;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 13178
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/aq;->d:Ljava/lang/Object;

    .line 13286
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    .line 13443
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    .line 13536
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    .line 13598
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    .line 13862
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/aq;->p:Ljava/lang/Object;

    .line 13938
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/aq;->q:Ljava/lang/Object;

    .line 14014
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/b/f/aq;->r:Z

    .line 14046
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->s:Lcom/google/n/ao;

    .line 14105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->t:Lcom/google/n/ao;

    .line 14165
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    .line 14302
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    .line 14502
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->y:Lcom/google/n/ao;

    .line 14593
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->A:Lcom/google/n/ao;

    .line 14652
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/aq;->B:Ljava/lang/Object;

    .line 14728
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->C:Lcom/google/n/ao;

    .line 14819
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/aq;->E:Ljava/lang/Object;

    .line 14895
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->F:Lcom/google/n/ao;

    .line 14954
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->G:Lcom/google/n/ao;

    .line 15013
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->H:Lcom/google/n/ao;

    .line 15072
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->J:Lcom/google/n/ao;

    .line 15163
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->L:Lcom/google/n/ao;

    .line 15223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    .line 15296
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/aq;->N:Lcom/google/n/ao;

    .line 12658
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/an;)Lcom/google/b/f/aq;
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/high16 v7, 0x80000

    const/high16 v6, 0x40000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12942
    invoke-static {}, Lcom/google/b/f/an;->d()Lcom/google/b/f/an;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 13124
    :goto_0
    return-object p0

    .line 12943
    :cond_0
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_28

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 12944
    iget v0, p1, Lcom/google/b/f/an;->c:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->c:I

    .line 12946
    :cond_1
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_29

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 12947
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12948
    iget-object v0, p1, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/aq;->d:Ljava/lang/Object;

    .line 12951
    :cond_2
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 12952
    iget-wide v4, p1, Lcom/google/b/f/an;->e:J

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    iput-wide v4, p0, Lcom/google/b/f/aq;->e:J

    .line 12954
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 12955
    iget-object v0, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 12956
    iget-object v0, p1, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    .line 12957
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12964
    :cond_4
    :goto_4
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2d

    move v0, v1

    :goto_5
    if-eqz v0, :cond_5

    .line 12965
    iget v0, p1, Lcom/google/b/f/an;->g:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->g:I

    .line 12967
    :cond_5
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2e

    move v0, v1

    :goto_6
    if-eqz v0, :cond_6

    .line 12968
    iget v0, p1, Lcom/google/b/f/an;->h:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->h:I

    .line 12970
    :cond_6
    iget-object v0, p1, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 12971
    iget-object v0, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 12972
    iget-object v0, p1, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    .line 12973
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12980
    :cond_7
    :goto_7
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_31

    move v0, v1

    :goto_8
    if-eqz v0, :cond_8

    .line 12981
    iget-object v0, p1, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    if-nez v0, :cond_32

    invoke-static {}, Lcom/google/b/f/ar;->d()Lcom/google/b/f/ar;

    move-result-object v0

    :goto_9
    iget v3, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_33

    iget-object v3, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    if-eqz v3, :cond_33

    iget-object v3, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    invoke-static {}, Lcom/google/b/f/ar;->d()Lcom/google/b/f/ar;

    move-result-object v4

    if-eq v3, v4, :cond_33

    iget-object v3, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    invoke-static {v3}, Lcom/google/b/f/ar;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/b/f/at;->a(Lcom/google/b/f/ar;)Lcom/google/b/f/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/at;->c()Lcom/google/b/f/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    :goto_a
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12983
    :cond_8
    iget-object v0, p1, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 12984
    iget-object v0, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 12985
    iget-object v0, p1, Lcom/google/b/f/an;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    .line 12986
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12993
    :cond_9
    :goto_b
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_36

    move v0, v1

    :goto_c
    if-eqz v0, :cond_a

    .line 12994
    iget v0, p1, Lcom/google/b/f/an;->l:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->l:I

    .line 12996
    :cond_a
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_37

    move v0, v1

    :goto_d
    if-eqz v0, :cond_b

    .line 12997
    iget v0, p1, Lcom/google/b/f/an;->m:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->m:I

    .line 12999
    :cond_b
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_38

    move v0, v1

    :goto_e
    if-eqz v0, :cond_c

    .line 13000
    iget-wide v4, p1, Lcom/google/b/f/an;->n:D

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    iput-wide v4, p0, Lcom/google/b/f/aq;->n:D

    .line 13002
    :cond_c
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_39

    move v0, v1

    :goto_f
    if-eqz v0, :cond_d

    .line 13003
    iget v0, p1, Lcom/google/b/f/an;->o:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->o:I

    .line 13005
    :cond_d
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_3a

    move v0, v1

    :goto_10
    if-eqz v0, :cond_e

    .line 13006
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13007
    iget-object v0, p1, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/aq;->p:Ljava/lang/Object;

    .line 13010
    :cond_e
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_3b

    move v0, v1

    :goto_11
    if-eqz v0, :cond_f

    .line 13011
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13012
    iget-object v0, p1, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/aq;->q:Ljava/lang/Object;

    .line 13015
    :cond_f
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_3c

    move v0, v1

    :goto_12
    if-eqz v0, :cond_10

    .line 13016
    iget-boolean v0, p1, Lcom/google/b/f/an;->r:Z

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    const v4, 0x8000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/aq;->r:Z

    .line 13018
    :cond_10
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_3d

    move v0, v1

    :goto_13
    if-eqz v0, :cond_11

    .line 13019
    iget-object v0, p0, Lcom/google/b/f/aq;->s:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13020
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13022
    :cond_11
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_3e

    move v0, v1

    :goto_14
    if-eqz v0, :cond_12

    .line 13023
    iget-object v0, p0, Lcom/google/b/f/aq;->t:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13024
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x20000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13026
    :cond_12
    iget-object v0, p1, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 13027
    iget-object v0, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 13028
    iget-object v0, p1, Lcom/google/b/f/an;->u:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    .line 13029
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const v3, -0x40001

    and-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13036
    :cond_13
    :goto_15
    iget-object v0, p1, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 13037
    iget-object v0, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 13038
    iget-object v0, p1, Lcom/google/b/f/an;->v:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    .line 13039
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const v3, -0x80001

    and-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13046
    :cond_14
    :goto_16
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_43

    move v0, v1

    :goto_17
    if-eqz v0, :cond_15

    .line 13047
    iget v0, p1, Lcom/google/b/f/an;->w:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->w:I

    .line 13049
    :cond_15
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_44

    move v0, v1

    :goto_18
    if-eqz v0, :cond_16

    .line 13050
    iget-boolean v0, p1, Lcom/google/b/f/an;->x:Z

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput-boolean v0, p0, Lcom/google/b/f/aq;->x:Z

    .line 13052
    :cond_16
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_45

    move v0, v1

    :goto_19
    if-eqz v0, :cond_17

    .line 13053
    iget-object v0, p0, Lcom/google/b/f/aq;->y:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13054
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x400000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13056
    :cond_17
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_46

    move v0, v1

    :goto_1a
    if-eqz v0, :cond_18

    .line 13057
    iget-wide v4, p1, Lcom/google/b/f/an;->z:J

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x800000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    iput-wide v4, p0, Lcom/google/b/f/aq;->z:J

    .line 13059
    :cond_18
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_47

    move v0, v1

    :goto_1b
    if-eqz v0, :cond_19

    .line 13060
    iget-object v0, p0, Lcom/google/b/f/aq;->A:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13061
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x1000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13063
    :cond_19
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_48

    move v0, v1

    :goto_1c
    if-eqz v0, :cond_1a

    .line 13064
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x2000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13065
    iget-object v0, p1, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/aq;->B:Ljava/lang/Object;

    .line 13068
    :cond_1a
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_49

    move v0, v1

    :goto_1d
    if-eqz v0, :cond_1b

    .line 13069
    iget-object v0, p0, Lcom/google/b/f/aq;->C:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13070
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x4000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13072
    :cond_1b
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_4a

    move v0, v1

    :goto_1e
    if-eqz v0, :cond_1c

    .line 13073
    iget v0, p1, Lcom/google/b/f/an;->D:I

    iget v3, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v4, 0x8000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/b/f/aq;->a:I

    iput v0, p0, Lcom/google/b/f/aq;->D:I

    .line 13075
    :cond_1c
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v0, v3

    const/high16 v3, 0x800000

    if-ne v0, v3, :cond_4b

    move v0, v1

    :goto_1f
    if-eqz v0, :cond_1d

    .line 13076
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x10000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13077
    iget-object v0, p1, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/f/aq;->E:Ljava/lang/Object;

    .line 13080
    :cond_1d
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_4c

    move v0, v1

    :goto_20
    if-eqz v0, :cond_1e

    .line 13081
    iget-object v0, p0, Lcom/google/b/f/aq;->F:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13082
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x20000000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13084
    :cond_1e
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    const/high16 v3, 0x2000000

    if-ne v0, v3, :cond_4d

    move v0, v1

    :goto_21
    if-eqz v0, :cond_1f

    .line 13085
    iget-object v0, p0, Lcom/google/b/f/aq;->G:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13086
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13088
    :cond_1f
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v0, v3

    const/high16 v3, 0x4000000

    if-ne v0, v3, :cond_4e

    move v0, v1

    :goto_22
    if-eqz v0, :cond_20

    .line 13089
    iget-object v0, p0, Lcom/google/b/f/aq;->H:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13090
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/2addr v0, v8

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13092
    :cond_20
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v0, v3

    const/high16 v3, 0x8000000

    if-ne v0, v3, :cond_4f

    move v0, v1

    :goto_23
    if-eqz v0, :cond_21

    .line 13093
    iget-object v0, p0, Lcom/google/b/f/aq;->J:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13094
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/aq;->b:I

    .line 13096
    :cond_21
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000000

    if-ne v0, v3, :cond_50

    move v0, v1

    :goto_24
    if-eqz v0, :cond_22

    .line 13097
    iget-boolean v0, p1, Lcom/google/b/f/an;->J:Z

    iget v3, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/aq;->b:I

    iput-boolean v0, p0, Lcom/google/b/f/aq;->K:Z

    .line 13099
    :cond_22
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000000

    if-ne v0, v3, :cond_51

    move v0, v1

    :goto_25
    if-eqz v0, :cond_23

    .line 13100
    iget-object v0, p0, Lcom/google/b/f/aq;->L:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13101
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/aq;->b:I

    .line 13103
    :cond_23
    iget-object v0, p1, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_24

    .line 13104
    iget-object v0, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 13105
    iget-object v0, p1, Lcom/google/b/f/an;->L:Ljava/util/List;

    iput-object v0, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    .line 13106
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/b/f/aq;->b:I

    .line 13113
    :cond_24
    :goto_26
    iget v0, p1, Lcom/google/b/f/an;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v0, v3, :cond_54

    move v0, v1

    :goto_27
    if-eqz v0, :cond_25

    .line 13114
    iget-object v0, p0, Lcom/google/b/f/aq;->N:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13115
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/aq;->b:I

    .line 13117
    :cond_25
    iget v0, p1, Lcom/google/b/f/an;->a:I

    and-int/2addr v0, v8

    if-ne v0, v8, :cond_55

    move v0, v1

    :goto_28
    if-eqz v0, :cond_26

    .line 13118
    iget-boolean v0, p1, Lcom/google/b/f/an;->N:Z

    iget v3, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/aq;->b:I

    iput-boolean v0, p0, Lcom/google/b/f/aq;->O:Z

    .line 13120
    :cond_26
    iget v0, p1, Lcom/google/b/f/an;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_56

    move v0, v1

    :goto_29
    if-eqz v0, :cond_27

    .line 13121
    iget-wide v0, p1, Lcom/google/b/f/an;->O:J

    iget v2, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/b/f/aq;->b:I

    iput-wide v0, p0, Lcom/google/b/f/aq;->P:J

    .line 13123
    :cond_27
    iget-object v0, p1, Lcom/google/b/f/an;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_28
    move v0, v2

    .line 12943
    goto/16 :goto_1

    :cond_29
    move v0, v2

    .line 12946
    goto/16 :goto_2

    :cond_2a
    move v0, v2

    .line 12951
    goto/16 :goto_3

    .line 12959
    :cond_2b
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-eq v0, v3, :cond_2c

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12960
    :cond_2c
    iget-object v0, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_2d
    move v0, v2

    .line 12964
    goto/16 :goto_5

    :cond_2e
    move v0, v2

    .line 12967
    goto/16 :goto_6

    .line 12975
    :cond_2f
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-eq v0, v3, :cond_30

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12976
    :cond_30
    iget-object v0, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_31
    move v0, v2

    .line 12980
    goto/16 :goto_8

    .line 12981
    :cond_32
    iget-object v0, p1, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    goto/16 :goto_9

    :cond_33
    iput-object v0, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    goto/16 :goto_a

    .line 12988
    :cond_34
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-eq v0, v3, :cond_35

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 12989
    :cond_35
    iget-object v0, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/an;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_36
    move v0, v2

    .line 12993
    goto/16 :goto_c

    :cond_37
    move v0, v2

    .line 12996
    goto/16 :goto_d

    :cond_38
    move v0, v2

    .line 12999
    goto/16 :goto_e

    :cond_39
    move v0, v2

    .line 13002
    goto/16 :goto_f

    :cond_3a
    move v0, v2

    .line 13005
    goto/16 :goto_10

    :cond_3b
    move v0, v2

    .line 13010
    goto/16 :goto_11

    :cond_3c
    move v0, v2

    .line 13015
    goto/16 :goto_12

    :cond_3d
    move v0, v2

    .line 13018
    goto/16 :goto_13

    :cond_3e
    move v0, v2

    .line 13022
    goto/16 :goto_14

    .line 13031
    :cond_3f
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/2addr v0, v6

    if-eq v0, v6, :cond_40

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13032
    :cond_40
    iget-object v0, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/an;->u:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_15

    .line 13041
    :cond_41
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/2addr v0, v7

    if-eq v0, v7, :cond_42

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/aq;->a:I

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/b/f/aq;->a:I

    .line 13042
    :cond_42
    iget-object v0, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/an;->v:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_16

    :cond_43
    move v0, v2

    .line 13046
    goto/16 :goto_17

    :cond_44
    move v0, v2

    .line 13049
    goto/16 :goto_18

    :cond_45
    move v0, v2

    .line 13052
    goto/16 :goto_19

    :cond_46
    move v0, v2

    .line 13056
    goto/16 :goto_1a

    :cond_47
    move v0, v2

    .line 13059
    goto/16 :goto_1b

    :cond_48
    move v0, v2

    .line 13063
    goto/16 :goto_1c

    :cond_49
    move v0, v2

    .line 13068
    goto/16 :goto_1d

    :cond_4a
    move v0, v2

    .line 13072
    goto/16 :goto_1e

    :cond_4b
    move v0, v2

    .line 13075
    goto/16 :goto_1f

    :cond_4c
    move v0, v2

    .line 13080
    goto/16 :goto_20

    :cond_4d
    move v0, v2

    .line 13084
    goto/16 :goto_21

    :cond_4e
    move v0, v2

    .line 13088
    goto/16 :goto_22

    :cond_4f
    move v0, v2

    .line 13092
    goto/16 :goto_23

    :cond_50
    move v0, v2

    .line 13096
    goto/16 :goto_24

    :cond_51
    move v0, v2

    .line 13099
    goto/16 :goto_25

    .line 13108
    :cond_52
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-eq v0, v3, :cond_53

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    iget v0, p0, Lcom/google/b/f/aq;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/aq;->b:I

    .line 13109
    :cond_53
    iget-object v0, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    iget-object v3, p1, Lcom/google/b/f/an;->L:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_26

    :cond_54
    move v0, v2

    .line 13113
    goto/16 :goto_27

    :cond_55
    move v0, v2

    .line 13117
    goto/16 :goto_28

    :cond_56
    move v0, v2

    .line 13120
    goto/16 :goto_29
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x10000

    const v10, 0x8000

    const/high16 v9, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12649
    new-instance v3, Lcom/google/b/f/an;

    invoke-direct {v3, p0}, Lcom/google/b/f/an;-><init>(Lcom/google/n/v;)V

    iget v4, p0, Lcom/google/b/f/aq;->a:I

    iget v5, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v0, v4, 0x1

    if-ne v0, v1, :cond_26

    move v0, v1

    :goto_0
    iget v6, p0, Lcom/google/b/f/aq;->c:I

    iput v6, v3, Lcom/google/b/f/an;->c:I

    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v6, p0, Lcom/google/b/f/aq;->d:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/an;->d:Ljava/lang/Object;

    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v6, p0, Lcom/google/b/f/aq;->e:J

    iput-wide v6, v3, Lcom/google/b/f/an;->e:J

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v6, v6, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    invoke-interface {v6}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v6, v6, -0x9

    iput v6, p0, Lcom/google/b/f/aq;->a:I

    :cond_2
    iget-object v6, p0, Lcom/google/b/f/aq;->f:Lcom/google/n/aq;

    iput-object v6, v3, Lcom/google/b/f/an;->f:Lcom/google/n/aq;

    and-int/lit8 v6, v4, 0x10

    const/16 v7, 0x10

    if-ne v6, v7, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v6, p0, Lcom/google/b/f/aq;->g:I

    iput v6, v3, Lcom/google/b/f/an;->g:I

    and-int/lit8 v6, v4, 0x20

    const/16 v7, 0x20

    if-ne v6, v7, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget v6, p0, Lcom/google/b/f/aq;->h:I

    iput v6, v3, Lcom/google/b/f/an;->h:I

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v6, v6, 0x40

    const/16 v7, 0x40

    if-ne v6, v7, :cond_5

    iget-object v6, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    invoke-interface {v6}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit8 v6, v6, -0x41

    iput v6, p0, Lcom/google/b/f/aq;->a:I

    :cond_5
    iget-object v6, p0, Lcom/google/b/f/aq;->i:Lcom/google/n/aq;

    iput-object v6, v3, Lcom/google/b/f/an;->i:Lcom/google/n/aq;

    and-int/lit16 v6, v4, 0x80

    const/16 v7, 0x80

    if-ne v6, v7, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v6, p0, Lcom/google/b/f/aq;->j:Lcom/google/b/f/ar;

    iput-object v6, v3, Lcom/google/b/f/an;->j:Lcom/google/b/f/ar;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit16 v6, v6, 0x100

    const/16 v7, 0x100

    if-ne v6, v7, :cond_7

    iget-object v6, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    and-int/lit16 v6, v6, -0x101

    iput v6, p0, Lcom/google/b/f/aq;->a:I

    :cond_7
    iget-object v6, p0, Lcom/google/b/f/aq;->k:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/an;->k:Ljava/util/List;

    and-int/lit16 v6, v4, 0x200

    const/16 v7, 0x200

    if-ne v6, v7, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget v6, p0, Lcom/google/b/f/aq;->l:I

    iput v6, v3, Lcom/google/b/f/an;->l:I

    and-int/lit16 v6, v4, 0x400

    const/16 v7, 0x400

    if-ne v6, v7, :cond_9

    or-int/lit16 v0, v0, 0x80

    :cond_9
    iget v6, p0, Lcom/google/b/f/aq;->m:I

    iput v6, v3, Lcom/google/b/f/an;->m:I

    and-int/lit16 v6, v4, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_a

    or-int/lit16 v0, v0, 0x100

    :cond_a
    iget-wide v6, p0, Lcom/google/b/f/aq;->n:D

    iput-wide v6, v3, Lcom/google/b/f/an;->n:D

    and-int/lit16 v6, v4, 0x1000

    const/16 v7, 0x1000

    if-ne v6, v7, :cond_b

    or-int/lit16 v0, v0, 0x200

    :cond_b
    iget v6, p0, Lcom/google/b/f/aq;->o:I

    iput v6, v3, Lcom/google/b/f/an;->o:I

    and-int/lit16 v6, v4, 0x2000

    const/16 v7, 0x2000

    if-ne v6, v7, :cond_c

    or-int/lit16 v0, v0, 0x400

    :cond_c
    iget-object v6, p0, Lcom/google/b/f/aq;->p:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/an;->p:Ljava/lang/Object;

    and-int/lit16 v6, v4, 0x4000

    const/16 v7, 0x4000

    if-ne v6, v7, :cond_d

    or-int/lit16 v0, v0, 0x800

    :cond_d
    iget-object v6, p0, Lcom/google/b/f/aq;->q:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/an;->q:Ljava/lang/Object;

    and-int v6, v4, v10

    if-ne v6, v10, :cond_e

    or-int/lit16 v0, v0, 0x1000

    :cond_e
    iget-boolean v6, p0, Lcom/google/b/f/aq;->r:Z

    iput-boolean v6, v3, Lcom/google/b/f/an;->r:Z

    and-int v6, v4, v11

    if-ne v6, v11, :cond_f

    or-int/lit16 v0, v0, 0x2000

    :cond_f
    iget-object v6, v3, Lcom/google/b/f/an;->s:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->s:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->s:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x20000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_10

    or-int/lit16 v0, v0, 0x4000

    :cond_10
    iget-object v6, v3, Lcom/google/b/f/an;->t:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->t:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->t:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v7, 0x40000

    and-int/2addr v6, v7

    const/high16 v7, 0x40000

    if-ne v6, v7, :cond_11

    iget-object v6, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    const v7, -0x40001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/aq;->a:I

    :cond_11
    iget-object v6, p0, Lcom/google/b/f/aq;->u:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/an;->u:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    const/high16 v7, 0x80000

    and-int/2addr v6, v7

    const/high16 v7, 0x80000

    if-ne v6, v7, :cond_12

    iget-object v6, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    iget v6, p0, Lcom/google/b/f/aq;->a:I

    const v7, -0x80001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/b/f/aq;->a:I

    :cond_12
    iget-object v6, p0, Lcom/google/b/f/aq;->v:Ljava/util/List;

    iput-object v6, v3, Lcom/google/b/f/an;->v:Ljava/util/List;

    const/high16 v6, 0x100000

    and-int/2addr v6, v4

    const/high16 v7, 0x100000

    if-ne v6, v7, :cond_13

    or-int/2addr v0, v10

    :cond_13
    iget v6, p0, Lcom/google/b/f/aq;->w:I

    iput v6, v3, Lcom/google/b/f/an;->w:I

    const/high16 v6, 0x200000

    and-int/2addr v6, v4

    const/high16 v7, 0x200000

    if-ne v6, v7, :cond_14

    or-int/2addr v0, v11

    :cond_14
    iget-boolean v6, p0, Lcom/google/b/f/aq;->x:Z

    iput-boolean v6, v3, Lcom/google/b/f/an;->x:Z

    const/high16 v6, 0x400000

    and-int/2addr v6, v4

    const/high16 v7, 0x400000

    if-ne v6, v7, :cond_15

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    :cond_15
    iget-object v6, v3, Lcom/google/b/f/an;->y:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->y:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->y:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x800000

    and-int/2addr v6, v4

    const/high16 v7, 0x800000

    if-ne v6, v7, :cond_16

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    :cond_16
    iget-wide v6, p0, Lcom/google/b/f/aq;->z:J

    iput-wide v6, v3, Lcom/google/b/f/an;->z:J

    const/high16 v6, 0x1000000

    and-int/2addr v6, v4

    const/high16 v7, 0x1000000

    if-ne v6, v7, :cond_17

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    :cond_17
    iget-object v6, v3, Lcom/google/b/f/an;->A:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->A:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->A:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x2000000

    and-int/2addr v6, v4

    const/high16 v7, 0x2000000

    if-ne v6, v7, :cond_18

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    :cond_18
    iget-object v6, p0, Lcom/google/b/f/aq;->B:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/an;->B:Ljava/lang/Object;

    const/high16 v6, 0x4000000

    and-int/2addr v6, v4

    const/high16 v7, 0x4000000

    if-ne v6, v7, :cond_19

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    :cond_19
    iget-object v6, v3, Lcom/google/b/f/an;->C:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->C:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->C:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x8000000

    and-int/2addr v6, v4

    const/high16 v7, 0x8000000

    if-ne v6, v7, :cond_1a

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    :cond_1a
    iget v6, p0, Lcom/google/b/f/aq;->D:I

    iput v6, v3, Lcom/google/b/f/an;->D:I

    const/high16 v6, 0x10000000

    and-int/2addr v6, v4

    const/high16 v7, 0x10000000

    if-ne v6, v7, :cond_1b

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    :cond_1b
    iget-object v6, p0, Lcom/google/b/f/aq;->E:Ljava/lang/Object;

    iput-object v6, v3, Lcom/google/b/f/an;->E:Ljava/lang/Object;

    const/high16 v6, 0x20000000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000000

    if-ne v6, v7, :cond_1c

    const/high16 v6, 0x1000000

    or-int/2addr v0, v6

    :cond_1c
    iget-object v6, v3, Lcom/google/b/f/an;->F:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->F:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->F:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v6, v7, :cond_1d

    const/high16 v6, 0x2000000

    or-int/2addr v0, v6

    :cond_1d
    iget-object v6, v3, Lcom/google/b/f/an;->G:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/b/f/aq;->G:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/b/f/aq;->G:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_1e

    const/high16 v4, 0x4000000

    or-int/2addr v0, v4

    :cond_1e
    iget-object v4, v3, Lcom/google/b/f/an;->H:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/b/f/aq;->H:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/b/f/aq;->H:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x1

    if-ne v4, v1, :cond_1f

    const/high16 v4, 0x8000000

    or-int/2addr v0, v4

    :cond_1f
    iget-object v4, v3, Lcom/google/b/f/an;->I:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/b/f/aq;->J:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/b/f/aq;->J:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x2

    const/4 v6, 0x2

    if-ne v4, v6, :cond_20

    const/high16 v4, 0x10000000

    or-int/2addr v0, v4

    :cond_20
    iget-boolean v4, p0, Lcom/google/b/f/aq;->K:Z

    iput-boolean v4, v3, Lcom/google/b/f/an;->J:Z

    and-int/lit8 v4, v5, 0x4

    const/4 v6, 0x4

    if-ne v4, v6, :cond_21

    const/high16 v4, 0x20000000

    or-int/2addr v0, v4

    :cond_21
    iget-object v4, v3, Lcom/google/b/f/an;->K:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/b/f/aq;->L:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/b/f/aq;->L:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v4, v4, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_22

    iget-object v4, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    iget v4, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/b/f/aq;->b:I

    :cond_22
    iget-object v4, p0, Lcom/google/b/f/aq;->M:Ljava/util/List;

    iput-object v4, v3, Lcom/google/b/f/an;->L:Ljava/util/List;

    and-int/lit8 v4, v5, 0x10

    const/16 v6, 0x10

    if-ne v4, v6, :cond_23

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v0, v4

    :cond_23
    iget-object v4, v3, Lcom/google/b/f/an;->M:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/b/f/aq;->N:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/b/f/aq;->N:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x20

    const/16 v6, 0x20

    if-ne v4, v6, :cond_24

    or-int/2addr v0, v9

    :cond_24
    iget-boolean v4, p0, Lcom/google/b/f/aq;->O:Z

    iput-boolean v4, v3, Lcom/google/b/f/an;->N:Z

    and-int/lit8 v4, v5, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_25

    :goto_1
    iget-wide v4, p0, Lcom/google/b/f/aq;->P:J

    iput-wide v4, v3, Lcom/google/b/f/an;->O:J

    iput v0, v3, Lcom/google/b/f/an;->a:I

    iput v1, v3, Lcom/google/b/f/an;->b:I

    return-object v3

    :cond_25
    move v1, v2

    goto :goto_1

    :cond_26
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 12649
    check-cast p1, Lcom/google/b/f/an;

    invoke-virtual {p0, p1}, Lcom/google/b/f/aq;->a(Lcom/google/b/f/an;)Lcom/google/b/f/aq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/high16 v3, 0x20000000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13128
    iget v0, p0, Lcom/google/b/f/aq;->a:I

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 13129
    iget-object v0, p0, Lcom/google/b/f/aq;->F:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/k;->d()Lcom/google/o/c/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/k;

    invoke-virtual {v0}, Lcom/google/o/c/k;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 13140
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 13128
    goto :goto_0

    .line 13134
    :cond_1
    iget v0, p0, Lcom/google/b/f/aq;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 13135
    iget-object v0, p0, Lcom/google/b/f/aq;->N:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/f;->d()Lcom/google/o/c/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/f;

    invoke-virtual {v0}, Lcom/google/o/c/f;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 13137
    goto :goto_1

    :cond_2
    move v0, v1

    .line 13134
    goto :goto_2

    :cond_3
    move v0, v2

    .line 13140
    goto :goto_1
.end method
