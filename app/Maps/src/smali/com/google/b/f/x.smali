.class public final Lcom/google/b/f/x;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/ac;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/x;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/x;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 579
    new-instance v0, Lcom/google/b/f/y;

    invoke-direct {v0}, Lcom/google/b/f/y;-><init>()V

    sput-object v0, Lcom/google/b/f/x;->PARSER:Lcom/google/n/ax;

    .line 791
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/x;->h:Lcom/google/n/aw;

    .line 1105
    new-instance v0, Lcom/google/b/f/x;

    invoke-direct {v0}, Lcom/google/b/f/x;-><init>()V

    sput-object v0, Lcom/google/b/f/x;->e:Lcom/google/b/f/x;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 518
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 682
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    .line 714
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    .line 729
    iput-byte v2, p0, Lcom/google/b/f/x;->f:B

    .line 766
    iput v2, p0, Lcom/google/b/f/x;->g:I

    .line 519
    iget-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 520
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/x;->c:I

    .line 521
    iget-object v0, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 522
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 528
    invoke-direct {p0}, Lcom/google/b/f/x;-><init>()V

    .line 529
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 534
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 535
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 536
    sparse-switch v3, :sswitch_data_0

    .line 541
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 543
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 539
    goto :goto_0

    .line 548
    :sswitch_1
    iget-object v3, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 549
    iget v3, p0, Lcom/google/b/f/x;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/x;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 570
    :catch_0
    move-exception v0

    .line 571
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/x;->au:Lcom/google/n/bn;

    throw v0

    .line 553
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 554
    invoke-static {v3}, Lcom/google/b/f/aa;->a(I)Lcom/google/b/f/aa;

    move-result-object v4

    .line 555
    if-nez v4, :cond_1

    .line 556
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 572
    :catch_1
    move-exception v0

    .line 573
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 574
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 558
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/b/f/x;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/x;->a:I

    .line 559
    iput v3, p0, Lcom/google/b/f/x;->c:I

    goto :goto_0

    .line 564
    :sswitch_3
    iget-object v3, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 565
    iget v3, p0, Lcom/google/b/f/x;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/x;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 576
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/x;->au:Lcom/google/n/bn;

    .line 577
    return-void

    .line 536
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 516
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 682
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    .line 714
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    .line 729
    iput-byte v1, p0, Lcom/google/b/f/x;->f:B

    .line 766
    iput v1, p0, Lcom/google/b/f/x;->g:I

    .line 517
    return-void
.end method

.method public static d()Lcom/google/b/f/x;
    .locals 1

    .prologue
    .line 1108
    sget-object v0, Lcom/google/b/f/x;->e:Lcom/google/b/f/x;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/z;
    .locals 1

    .prologue
    .line 853
    new-instance v0, Lcom/google/b/f/z;

    invoke-direct {v0}, Lcom/google/b/f/z;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    sget-object v0, Lcom/google/b/f/x;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 753
    invoke-virtual {p0}, Lcom/google/b/f/x;->c()I

    .line 754
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 755
    iget-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 757
    :cond_0
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 758
    iget v0, p0, Lcom/google/b/f/x;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 760
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 761
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 763
    :cond_2
    iget-object v0, p0, Lcom/google/b/f/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 764
    return-void

    .line 758
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 731
    iget-byte v0, p0, Lcom/google/b/f/x;->f:B

    .line 732
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 748
    :goto_0
    return v0

    .line 733
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 735
    :cond_1
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 736
    iget-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ah;->d()Lcom/google/b/f/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ah;

    invoke-virtual {v0}, Lcom/google/b/f/ah;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 737
    iput-byte v2, p0, Lcom/google/b/f/x;->f:B

    move v0, v2

    .line 738
    goto :goto_0

    :cond_2
    move v0, v2

    .line 735
    goto :goto_1

    .line 741
    :cond_3
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 742
    iget-object v0, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ah;->d()Lcom/google/b/f/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ah;

    invoke-virtual {v0}, Lcom/google/b/f/ah;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 743
    iput-byte v2, p0, Lcom/google/b/f/x;->f:B

    move v0, v2

    .line 744
    goto :goto_0

    :cond_4
    move v0, v2

    .line 741
    goto :goto_2

    .line 747
    :cond_5
    iput-byte v1, p0, Lcom/google/b/f/x;->f:B

    move v0, v1

    .line 748
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 768
    iget v0, p0, Lcom/google/b/f/x;->g:I

    .line 769
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 786
    :goto_0
    return v0

    .line 772
    :cond_0
    iget v0, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 773
    iget-object v0, p0, Lcom/google/b/f/x;->b:Lcom/google/n/ao;

    .line 774
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 776
    :goto_1
    iget v2, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 777
    iget v2, p0, Lcom/google/b/f/x;->c:I

    .line 778
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 780
    :cond_1
    iget v2, p0, Lcom/google/b/f/x;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 781
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/b/f/x;->d:Lcom/google/n/ao;

    .line 782
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 784
    :cond_2
    iget-object v1, p0, Lcom/google/b/f/x;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    iput v0, p0, Lcom/google/b/f/x;->g:I

    goto :goto_0

    .line 778
    :cond_3
    const/16 v2, 0xa

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 510
    invoke-static {}, Lcom/google/b/f/x;->newBuilder()Lcom/google/b/f/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/z;->a(Lcom/google/b/f/x;)Lcom/google/b/f/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 510
    invoke-static {}, Lcom/google/b/f/x;->newBuilder()Lcom/google/b/f/z;

    move-result-object v0

    return-object v0
.end method
