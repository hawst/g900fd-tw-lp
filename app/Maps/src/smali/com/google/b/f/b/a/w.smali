.class public final Lcom/google/b/f/b/a/w;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/z;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/w;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/b/f/b/a/w;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:D

.field g:D

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/b/f/b/a/x;

    invoke-direct {v0}, Lcom/google/b/f/b/a/x;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/w;->PARSER:Lcom/google/n/ax;

    .line 281
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/w;->k:Lcom/google/n/aw;

    .line 650
    new-instance v0, Lcom/google/b/f/b/a/w;

    invoke-direct {v0}, Lcom/google/b/f/b/a/w;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/w;->h:Lcom/google/b/f/b/a/w;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 210
    iput-byte v1, p0, Lcom/google/b/f/b/a/w;->i:B

    .line 244
    iput v1, p0, Lcom/google/b/f/b/a/w;->j:I

    .line 18
    iput v0, p0, Lcom/google/b/f/b/a/w;->b:I

    .line 19
    iput v0, p0, Lcom/google/b/f/b/a/w;->c:I

    .line 20
    iput v0, p0, Lcom/google/b/f/b/a/w;->d:I

    .line 21
    iput v0, p0, Lcom/google/b/f/b/a/w;->e:I

    .line 22
    iput-wide v2, p0, Lcom/google/b/f/b/a/w;->f:D

    .line 23
    iput-wide v2, p0, Lcom/google/b/f/b/a/w;->g:D

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0}, Lcom/google/b/f/b/a/w;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 38
    sparse-switch v3, :sswitch_data_0

    .line 43
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 51
    invoke-static {v3}, Lcom/google/r/b/a/bc;->a(I)Lcom/google/r/b/a/bc;

    move-result-object v4

    .line 52
    if-nez v4, :cond_1

    .line 53
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/w;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 56
    iput v3, p0, Lcom/google/b/f/b/a/w;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    .line 96
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 97
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 62
    invoke-static {v3}, Lcom/google/r/b/a/gd;->a(I)Lcom/google/r/b/a/gd;

    move-result-object v4

    .line 63
    if-nez v4, :cond_2

    .line 64
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 66
    :cond_2
    iget v4, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 67
    iput v3, p0, Lcom/google/b/f/b/a/w;->c:I

    goto :goto_0

    .line 72
    :sswitch_3
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/w;->d:I

    goto :goto_0

    .line 77
    :sswitch_4
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/w;->e:I

    goto :goto_0

    .line 82
    :sswitch_5
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/w;->f:D

    goto/16 :goto_0

    .line 87
    :sswitch_6
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/b/f/b/a/w;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/b/f/b/a/w;->g:D
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 99
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/w;->au:Lcom/google/n/bn;

    .line 100
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x29 -> :sswitch_5
        0x31 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 210
    iput-byte v0, p0, Lcom/google/b/f/b/a/w;->i:B

    .line 244
    iput v0, p0, Lcom/google/b/f/b/a/w;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/w;
    .locals 1

    .prologue
    .line 653
    sget-object v0, Lcom/google/b/f/b/a/w;->h:Lcom/google/b/f/b/a/w;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/y;
    .locals 1

    .prologue
    .line 343
    new-instance v0, Lcom/google/b/f/b/a/y;

    invoke-direct {v0}, Lcom/google/b/f/b/a/y;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/google/b/f/b/a/w;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 222
    invoke-virtual {p0}, Lcom/google/b/f/b/a/w;->c()I

    .line 223
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 224
    iget v0, p0, Lcom/google/b/f/b/a/w;->b:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 226
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 227
    iget v0, p0, Lcom/google/b/f/b/a/w;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 229
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 230
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/w;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 232
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 233
    iget v0, p0, Lcom/google/b/f/b/a/w;->e:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 235
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 236
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/b/f/b/a/w;->f:D

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 238
    :cond_4
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 239
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/b/f/b/a/w;->g:D

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 241
    :cond_5
    iget-object v0, p0, Lcom/google/b/f/b/a/w;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 242
    return-void

    .line 224
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 227
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 230
    :cond_8
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 233
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 212
    iget-byte v1, p0, Lcom/google/b/f/b/a/w;->i:B

    .line 213
    if-ne v1, v0, :cond_0

    .line 217
    :goto_0
    return v0

    .line 214
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 216
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/w;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 246
    iget v0, p0, Lcom/google/b/f/b/a/w;->j:I

    .line 247
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 276
    :goto_0
    return v0

    .line 250
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 251
    iget v0, p0, Lcom/google/b/f/b/a/w;->b:I

    .line 252
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 254
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 255
    iget v3, p0, Lcom/google/b/f/b/a/w;->c:I

    .line 256
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 258
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 259
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/w;->d:I

    .line 260
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 262
    :cond_2
    iget v3, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 263
    iget v3, p0, Lcom/google/b/f/b/a/w;->e:I

    .line 264
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 266
    :cond_4
    iget v1, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_5

    .line 267
    const/4 v1, 0x5

    iget-wide v4, p0, Lcom/google/b/f/b/a/w;->f:D

    .line 268
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 270
    :cond_5
    iget v1, p0, Lcom/google/b/f/b/a/w;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_6

    .line 271
    const/4 v1, 0x6

    iget-wide v4, p0, Lcom/google/b/f/b/a/w;->g:D

    .line 272
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 274
    :cond_6
    iget-object v1, p0, Lcom/google/b/f/b/a/w;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    iput v0, p0, Lcom/google/b/f/b/a/w;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 252
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 256
    goto :goto_3

    :cond_9
    move v3, v1

    .line 260
    goto :goto_4

    :cond_a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/w;->newBuilder()Lcom/google/b/f/b/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/y;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/b/f/b/a/w;->newBuilder()Lcom/google/b/f/b/a/y;

    move-result-object v0

    return-object v0
.end method
