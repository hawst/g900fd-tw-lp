.class public final enum Lcom/google/b/f/bp;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/f/bp;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/f/bp;

.field public static final enum b:Lcom/google/b/f/bp;

.field public static final enum c:Lcom/google/b/f/bp;

.field private static final synthetic e:[Lcom/google/b/f/bp;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 893
    new-instance v0, Lcom/google/b/f/bp;

    const-string v1, "VALID_PARAMETERS"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/b/f/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bp;->a:Lcom/google/b/f/bp;

    .line 897
    new-instance v0, Lcom/google/b/f/bp;

    const-string v1, "INCOMPLETE_PARAMETERS"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/f/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bp;->b:Lcom/google/b/f/bp;

    .line 901
    new-instance v0, Lcom/google/b/f/bp;

    const-string v1, "INVALID_PARAMETERS"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/b/f/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/b/f/bp;->c:Lcom/google/b/f/bp;

    .line 888
    new-array v0, v5, [Lcom/google/b/f/bp;

    sget-object v1, Lcom/google/b/f/bp;->a:Lcom/google/b/f/bp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/f/bp;->b:Lcom/google/b/f/bp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/f/bp;->c:Lcom/google/b/f/bp;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/b/f/bp;->e:[Lcom/google/b/f/bp;

    .line 936
    new-instance v0, Lcom/google/b/f/bq;

    invoke-direct {v0}, Lcom/google/b/f/bq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 945
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 946
    iput p3, p0, Lcom/google/b/f/bp;->d:I

    .line 947
    return-void
.end method

.method public static a(I)Lcom/google/b/f/bp;
    .locals 1

    .prologue
    .line 923
    packed-switch p0, :pswitch_data_0

    .line 927
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 924
    :pswitch_0
    sget-object v0, Lcom/google/b/f/bp;->a:Lcom/google/b/f/bp;

    goto :goto_0

    .line 925
    :pswitch_1
    sget-object v0, Lcom/google/b/f/bp;->b:Lcom/google/b/f/bp;

    goto :goto_0

    .line 926
    :pswitch_2
    sget-object v0, Lcom/google/b/f/bp;->c:Lcom/google/b/f/bp;

    goto :goto_0

    .line 923
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/f/bp;
    .locals 1

    .prologue
    .line 888
    const-class v0, Lcom/google/b/f/bp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bp;

    return-object v0
.end method

.method public static values()[Lcom/google/b/f/bp;
    .locals 1

    .prologue
    .line 888
    sget-object v0, Lcom/google/b/f/bp;->e:[Lcom/google/b/f/bp;

    invoke-virtual {v0}, [Lcom/google/b/f/bp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/f/bp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 919
    iget v0, p0, Lcom/google/b/f/bp;->d:I

    return v0
.end method
