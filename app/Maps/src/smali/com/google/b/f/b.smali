.class public final Lcom/google/b/f/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/b/f/b;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field c:I

.field public d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:Z

.field k:I

.field l:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    new-instance v0, Lcom/google/b/f/c;

    invoke-direct {v0}, Lcom/google/b/f/c;-><init>()V

    sput-object v0, Lcom/google/b/f/b;->PARSER:Lcom/google/n/ax;

    .line 515
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b;->p:Lcom/google/n/aw;

    .line 1081
    new-instance v0, Lcom/google/b/f/b;

    invoke-direct {v0}, Lcom/google/b/f/b;-><init>()V

    sput-object v0, Lcom/google/b/f/b;->m:Lcom/google/b/f/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 125
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 409
    iput-byte v0, p0, Lcom/google/b/f/b;->n:B

    .line 458
    iput v0, p0, Lcom/google/b/f/b;->o:I

    .line 126
    iput v0, p0, Lcom/google/b/f/b;->b:I

    .line 127
    iput v0, p0, Lcom/google/b/f/b;->c:I

    .line 128
    iput v1, p0, Lcom/google/b/f/b;->d:I

    .line 129
    iput v1, p0, Lcom/google/b/f/b;->e:I

    .line 130
    iput v0, p0, Lcom/google/b/f/b;->f:I

    .line 131
    iput v0, p0, Lcom/google/b/f/b;->g:I

    .line 132
    iput v1, p0, Lcom/google/b/f/b;->h:I

    .line 133
    iput v0, p0, Lcom/google/b/f/b;->i:I

    .line 134
    iput-boolean v1, p0, Lcom/google/b/f/b;->j:Z

    .line 135
    iput v1, p0, Lcom/google/b/f/b;->k:I

    .line 136
    iput v1, p0, Lcom/google/b/f/b;->l:I

    .line 137
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 143
    invoke-direct {p0}, Lcom/google/b/f/b;-><init>()V

    .line 144
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 149
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 150
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 151
    sparse-switch v0, :sswitch_data_0

    .line 156
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 158
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 154
    goto :goto_0

    .line 163
    :sswitch_1
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 164
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b;->au:Lcom/google/n/bn;

    throw v0

    .line 168
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 221
    :catch_1
    move-exception v0

    .line 222
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 223
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 173
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 174
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->k:I

    goto :goto_0

    .line 178
    :sswitch_4
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 179
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->l:I

    goto :goto_0

    .line 183
    :sswitch_5
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 184
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->f:I

    goto :goto_0

    .line 188
    :sswitch_6
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 189
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->g:I

    goto :goto_0

    .line 193
    :sswitch_7
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 194
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->h:I

    goto/16 :goto_0

    .line 198
    :sswitch_8
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 199
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->e:I

    goto/16 :goto_0

    .line 203
    :sswitch_9
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 204
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->i:I

    goto/16 :goto_0

    .line 208
    :sswitch_a
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/b/f/b;->j:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 213
    :sswitch_b
    iget v0, p0, Lcom/google/b/f/b;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/b;->a:I

    .line 214
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/b;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 225
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b;->au:Lcom/google/n/bn;

    .line 226
    return-void

    .line 151
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 123
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 409
    iput-byte v0, p0, Lcom/google/b/f/b;->n:B

    .line 458
    iput v0, p0, Lcom/google/b/f/b;->o:I

    .line 124
    return-void
.end method

.method public static a([B)Lcom/google/b/f/b;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/google/b/f/b;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->b([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    return-object v0
.end method

.method public static d()Lcom/google/b/f/b;
    .locals 1

    .prologue
    .line 1084
    sget-object v0, Lcom/google/b/f/b;->m:Lcom/google/b/f/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/d;
    .locals 1

    .prologue
    .line 577
    new-instance v0, Lcom/google/b/f/d;

    invoke-direct {v0}, Lcom/google/b/f/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    sget-object v0, Lcom/google/b/f/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 421
    invoke-virtual {p0}, Lcom/google/b/f/b;->c()I

    .line 422
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 423
    iget v2, p0, Lcom/google/b/f/b;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_b

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 425
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_1

    .line 426
    iget v2, p0, Lcom/google/b/f/b;->d:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_c

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 428
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2

    .line 429
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/b/f/b;->k:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_d

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 431
    :cond_2
    :goto_2
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_3

    .line 432
    iget v2, p0, Lcom/google/b/f/b;->l:I

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_e

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 434
    :cond_3
    :goto_3
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 435
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/b/f/b;->f:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_f

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 437
    :cond_4
    :goto_4
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 438
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/b/f/b;->g:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_10

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 440
    :cond_5
    :goto_5
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 441
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/b/f/b;->h:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_11

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 443
    :cond_6
    :goto_6
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_7

    .line 444
    iget v2, p0, Lcom/google/b/f/b;->e:I

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_12

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 446
    :cond_7
    :goto_7
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 447
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/b/f/b;->i:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_13

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 449
    :cond_8
    :goto_8
    iget v2, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_9

    .line 450
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/b/f/b;->j:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_14

    :goto_9
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 452
    :cond_9
    iget v0, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_a

    .line 453
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/b/f/b;->c:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_15

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 455
    :cond_a
    :goto_a
    iget-object v0, p0, Lcom/google/b/f/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 456
    return-void

    .line 423
    :cond_b
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 426
    :cond_c
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 429
    :cond_d
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 432
    :cond_e
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 435
    :cond_f
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 438
    :cond_10
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 441
    :cond_11
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    .line 444
    :cond_12
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 447
    :cond_13
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    :cond_14
    move v0, v1

    .line 450
    goto :goto_9

    .line 453
    :cond_15
    int-to-long v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 411
    iget-byte v1, p0, Lcom/google/b/f/b;->n:B

    .line 412
    if-ne v1, v0, :cond_0

    .line 416
    :goto_0
    return v0

    .line 413
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 415
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b;->n:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 460
    iget v0, p0, Lcom/google/b/f/b;->o:I

    .line 461
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 510
    :goto_0
    return v0

    .line 464
    :cond_0
    iget v0, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_15

    .line 465
    iget v0, p0, Lcom/google/b/f/b;->b:I

    .line 466
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 468
    :goto_2
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_1

    .line 469
    iget v3, p0, Lcom/google/b/f/b;->d:I

    .line 470
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 472
    :cond_1
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_2

    .line 473
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b;->k:I

    .line 474
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 476
    :cond_2
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_3

    .line 477
    iget v3, p0, Lcom/google/b/f/b;->l:I

    .line 478
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 480
    :cond_3
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 481
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/b/f/b;->f:I

    .line 482
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 484
    :cond_4
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 485
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/b/f/b;->g:I

    .line 486
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 488
    :cond_5
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 489
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/b/f/b;->h:I

    .line 490
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 492
    :cond_6
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_7

    .line 493
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/b/f/b;->e:I

    .line 494
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 496
    :cond_7
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_8

    .line 497
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/b/f/b;->i:I

    .line 498
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_14

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_a
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 500
    :cond_8
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_9

    .line 501
    iget-boolean v3, p0, Lcom/google/b/f/b;->j:Z

    .line 502
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 504
    :cond_9
    iget v3, p0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_b

    .line 505
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/b/f/b;->c:I

    .line 506
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_a
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 508
    :cond_b
    iget-object v1, p0, Lcom/google/b/f/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 509
    iput v0, p0, Lcom/google/b/f/b;->o:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 466
    goto/16 :goto_1

    :cond_d
    move v3, v1

    .line 470
    goto/16 :goto_3

    :cond_e
    move v3, v1

    .line 474
    goto/16 :goto_4

    :cond_f
    move v3, v1

    .line 478
    goto/16 :goto_5

    :cond_10
    move v3, v1

    .line 482
    goto/16 :goto_6

    :cond_11
    move v3, v1

    .line 486
    goto/16 :goto_7

    :cond_12
    move v3, v1

    .line 490
    goto/16 :goto_8

    :cond_13
    move v3, v1

    .line 494
    goto :goto_9

    :cond_14
    move v3, v1

    .line 498
    goto :goto_a

    :cond_15
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/b/f/b;->newBuilder()Lcom/google/b/f/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/d;->a(Lcom/google/b/f/b;)Lcom/google/b/f/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/b/f/b;->newBuilder()Lcom/google/b/f/d;

    move-result-object v0

    return-object v0
.end method
