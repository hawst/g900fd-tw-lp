.class public final Lcom/google/b/f/b/a/bt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/bw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/bt;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/b/f/b/a/bt;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 869
    new-instance v0, Lcom/google/b/f/b/a/bu;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bu;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/bt;->PARSER:Lcom/google/n/ax;

    .line 1002
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/b/a/bt;->i:Lcom/google/n/aw;

    .line 1281
    new-instance v0, Lcom/google/b/f/b/a/bt;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bt;-><init>()V

    sput-object v0, Lcom/google/b/f/b/a/bt;->f:Lcom/google/b/f/b/a/bt;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 808
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 945
    iput-byte v1, p0, Lcom/google/b/f/b/a/bt;->g:B

    .line 973
    iput v1, p0, Lcom/google/b/f/b/a/bt;->h:I

    .line 809
    iput v0, p0, Lcom/google/b/f/b/a/bt;->b:I

    .line 810
    iput v0, p0, Lcom/google/b/f/b/a/bt;->c:I

    .line 811
    iput v0, p0, Lcom/google/b/f/b/a/bt;->d:I

    .line 812
    iput v0, p0, Lcom/google/b/f/b/a/bt;->e:I

    .line 813
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 819
    invoke-direct {p0}, Lcom/google/b/f/b/a/bt;-><init>()V

    .line 820
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 824
    const/4 v0, 0x0

    .line 825
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 826
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 827
    sparse-switch v3, :sswitch_data_0

    .line 832
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 834
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 830
    goto :goto_0

    .line 839
    :sswitch_1
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    .line 840
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/bt;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 860
    :catch_0
    move-exception v0

    .line 861
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 866
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/b/a/bt;->au:Lcom/google/n/bn;

    throw v0

    .line 844
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    .line 845
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/bt;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 862
    :catch_1
    move-exception v0

    .line 863
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 864
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 849
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    .line 850
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/bt;->d:I

    goto :goto_0

    .line 854
    :sswitch_4
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    .line 855
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/b/f/b/a/bt;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 866
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/bt;->au:Lcom/google/n/bn;

    .line 867
    return-void

    .line 827
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 806
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 945
    iput-byte v0, p0, Lcom/google/b/f/b/a/bt;->g:B

    .line 973
    iput v0, p0, Lcom/google/b/f/b/a/bt;->h:I

    .line 807
    return-void
.end method

.method public static d()Lcom/google/b/f/b/a/bt;
    .locals 1

    .prologue
    .line 1284
    sget-object v0, Lcom/google/b/f/b/a/bt;->f:Lcom/google/b/f/b/a/bt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/b/a/bv;
    .locals 1

    .prologue
    .line 1064
    new-instance v0, Lcom/google/b/f/b/a/bv;

    invoke-direct {v0}, Lcom/google/b/f/b/a/bv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/b/a/bt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 881
    sget-object v0, Lcom/google/b/f/b/a/bt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 957
    invoke-virtual {p0}, Lcom/google/b/f/b/a/bt;->c()I

    .line 958
    iget v0, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 959
    iget v0, p0, Lcom/google/b/f/b/a/bt;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 961
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 962
    iget v0, p0, Lcom/google/b/f/b/a/bt;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 964
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 965
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/b/a/bt;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 967
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 968
    iget v0, p0, Lcom/google/b/f/b/a/bt;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 970
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/b/f/b/a/bt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 971
    return-void

    .line 959
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 962
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 965
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 968
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 947
    iget-byte v1, p0, Lcom/google/b/f/b/a/bt;->g:B

    .line 948
    if-ne v1, v0, :cond_0

    .line 952
    :goto_0
    return v0

    .line 949
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 951
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/b/a/bt;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 975
    iget v0, p0, Lcom/google/b/f/b/a/bt;->h:I

    .line 976
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 997
    :goto_0
    return v0

    .line 979
    :cond_0
    iget v0, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 980
    iget v0, p0, Lcom/google/b/f/b/a/bt;->b:I

    .line 981
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 983
    :goto_2
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 984
    iget v3, p0, Lcom/google/b/f/b/a/bt;->c:I

    .line 985
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 987
    :cond_1
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 988
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/b/f/b/a/bt;->d:I

    .line 989
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 991
    :cond_2
    iget v3, p0, Lcom/google/b/f/b/a/bt;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 992
    iget v3, p0, Lcom/google/b/f/b/a/bt;->e:I

    .line 993
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 995
    :cond_4
    iget-object v1, p0, Lcom/google/b/f/b/a/bt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 996
    iput v0, p0, Lcom/google/b/f/b/a/bt;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 981
    goto :goto_1

    :cond_6
    move v3, v1

    .line 985
    goto :goto_3

    :cond_7
    move v3, v1

    .line 989
    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 800
    invoke-static {}, Lcom/google/b/f/b/a/bt;->newBuilder()Lcom/google/b/f/b/a/bv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/b/a/bv;->a(Lcom/google/b/f/b/a/bt;)Lcom/google/b/f/b/a/bv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 800
    invoke-static {}, Lcom/google/b/f/b/a/bt;->newBuilder()Lcom/google/b/f/b/a/bv;

    move-result-object v0

    return-object v0
.end method
