.class public final Lcom/google/b/f/bl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/bm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/bj;",
        "Lcom/google/b/f/bl;",
        ">;",
        "Lcom/google/b/f/bm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1815
    sget-object v0, Lcom/google/b/f/bj;->e:Lcom/google/b/f/bj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1875
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bl;->b:Ljava/lang/Object;

    .line 1816
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/bj;)Lcom/google/b/f/bl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1853
    invoke-static {}, Lcom/google/b/f/bj;->d()Lcom/google/b/f/bj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1866
    :goto_0
    return-object p0

    .line 1854
    :cond_0
    iget v2, p1, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1855
    iget v2, p0, Lcom/google/b/f/bl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/f/bl;->a:I

    .line 1856
    iget-object v2, p1, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/b/f/bl;->b:Ljava/lang/Object;

    .line 1859
    :cond_1
    iget v2, p1, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1860
    iget-boolean v2, p1, Lcom/google/b/f/bj;->c:Z

    iget v3, p0, Lcom/google/b/f/bl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/b/f/bl;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/bl;->c:Z

    .line 1862
    :cond_2
    iget v2, p1, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1863
    iget v0, p1, Lcom/google/b/f/bj;->d:I

    iget v1, p0, Lcom/google/b/f/bl;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/b/f/bl;->a:I

    iput v0, p0, Lcom/google/b/f/bl;->d:I

    .line 1865
    :cond_3
    iget-object v0, p1, Lcom/google/b/f/bj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1854
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1859
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1862
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1807
    new-instance v2, Lcom/google/b/f/bj;

    invoke-direct {v2, p0}, Lcom/google/b/f/bj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/bl;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/b/f/bl;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/b/f/bl;->c:Z

    iput-boolean v1, v2, Lcom/google/b/f/bj;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/bl;->d:I

    iput v1, v2, Lcom/google/b/f/bj;->d:I

    iput v0, v2, Lcom/google/b/f/bj;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1807
    check-cast p1, Lcom/google/b/f/bj;

    invoke-virtual {p0, p1}, Lcom/google/b/f/bl;->a(Lcom/google/b/f/bj;)Lcom/google/b/f/bl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1870
    const/4 v0, 0x1

    return v0
.end method
