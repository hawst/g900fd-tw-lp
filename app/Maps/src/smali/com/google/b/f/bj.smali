.class public final Lcom/google/b/f/bj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/b/f/bm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bj;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/b/f/bj;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Z

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1539
    new-instance v0, Lcom/google/b/f/bk;

    invoke-direct {v0}, Lcom/google/b/f/bk;-><init>()V

    sput-object v0, Lcom/google/b/f/bj;->PARSER:Lcom/google/n/ax;

    .line 1735
    const/4 v0, 0x0

    sput-object v0, Lcom/google/b/f/bj;->h:Lcom/google/n/aw;

    .line 2019
    new-instance v0, Lcom/google/b/f/bj;

    invoke-direct {v0}, Lcom/google/b/f/bj;-><init>()V

    sput-object v0, Lcom/google/b/f/bj;->e:Lcom/google/b/f/bj;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1483
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1685
    iput-byte v0, p0, Lcom/google/b/f/bj;->f:B

    .line 1710
    iput v0, p0, Lcom/google/b/f/bj;->g:I

    .line 1484
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    .line 1485
    iput-boolean v1, p0, Lcom/google/b/f/bj;->c:Z

    .line 1486
    iput v1, p0, Lcom/google/b/f/bj;->d:I

    .line 1487
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1493
    invoke-direct {p0}, Lcom/google/b/f/bj;-><init>()V

    .line 1494
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 1499
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 1500
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1501
    sparse-switch v0, :sswitch_data_0

    .line 1506
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 1508
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 1504
    goto :goto_0

    .line 1513
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1514
    iget v5, p0, Lcom/google/b/f/bj;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/b/f/bj;->a:I

    .line 1515
    iput-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1530
    :catch_0
    move-exception v0

    .line 1531
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1536
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/f/bj;->au:Lcom/google/n/bn;

    throw v0

    .line 1519
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/b/f/bj;->a:I

    .line 1520
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/b/f/bj;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1532
    :catch_1
    move-exception v0

    .line 1533
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1534
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 1520
    goto :goto_1

    .line 1524
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/f/bj;->a:I

    .line 1525
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/b/f/bj;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1536
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bj;->au:Lcom/google/n/bn;

    .line 1537
    return-void

    .line 1501
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1481
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1685
    iput-byte v0, p0, Lcom/google/b/f/bj;->f:B

    .line 1710
    iput v0, p0, Lcom/google/b/f/bj;->g:I

    .line 1482
    return-void
.end method

.method public static d()Lcom/google/b/f/bj;
    .locals 1

    .prologue
    .line 2022
    sget-object v0, Lcom/google/b/f/bj;->e:Lcom/google/b/f/bj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/b/f/bl;
    .locals 1

    .prologue
    .line 1797
    new-instance v0, Lcom/google/b/f/bl;

    invoke-direct {v0}, Lcom/google/b/f/bl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/b/f/bj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1551
    sget-object v0, Lcom/google/b/f/bj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1697
    invoke-virtual {p0}, Lcom/google/b/f/bj;->c()I

    .line 1698
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1699
    iget-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1701
    :cond_0
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1702
    iget-boolean v0, p0, Lcom/google/b/f/bj;->c:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1704
    :cond_1
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1705
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/b/f/bj;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1707
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/b/f/bj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1708
    return-void

    .line 1699
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1702
    goto :goto_1

    .line 1705
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1687
    iget-byte v1, p0, Lcom/google/b/f/bj;->f:B

    .line 1688
    if-ne v1, v0, :cond_0

    .line 1692
    :goto_0
    return v0

    .line 1689
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1691
    :cond_1
    iput-byte v0, p0, Lcom/google/b/f/bj;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1712
    iget v0, p0, Lcom/google/b/f/bj;->g:I

    .line 1713
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1730
    :goto_0
    return v0

    .line 1716
    :cond_0
    iget v0, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 1718
    iget-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/bj;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1720
    :goto_2
    iget v2, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1721
    iget-boolean v2, p0, Lcom/google/b/f/bj;->c:Z

    .line 1722
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1724
    :cond_1
    iget v2, p0, Lcom/google/b/f/bj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1725
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/b/f/bj;->d:I

    .line 1726
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1728
    :cond_2
    iget-object v1, p0, Lcom/google/b/f/bj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1729
    iput v0, p0, Lcom/google/b/f/bj;->g:I

    goto :goto_0

    .line 1718
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1726
    :cond_4
    const/16 v1, 0xa

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1475
    invoke-static {}, Lcom/google/b/f/bj;->newBuilder()Lcom/google/b/f/bl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/b/f/bl;->a(Lcom/google/b/f/bj;)Lcom/google/b/f/bl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1475
    invoke-static {}, Lcom/google/b/f/bj;->newBuilder()Lcom/google/b/f/bl;

    move-result-object v0

    return-object v0
.end method
