.class public final Lcom/google/b/f/b/a/j;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/b/f/b/a/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/b/f/b/a/h;",
        "Lcom/google/b/f/b/a/j;",
        ">;",
        "Lcom/google/b/f/b/a/o;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:I

.field public e:Z

.field public f:Z

.field private g:Lcom/google/b/f/b/a/k;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1219
    sget-object v0, Lcom/google/b/f/b/a/h;->h:Lcom/google/b/f/b/a/h;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1308
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/j;->b:Ljava/lang/Object;

    .line 1384
    const-string v0, ""

    iput-object v0, p0, Lcom/google/b/f/b/a/j;->c:Ljava/lang/Object;

    .line 1460
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/f/b/a/j;->d:I

    .line 1560
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    .line 1220
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/h;)Lcom/google/b/f/b/a/j;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1275
    invoke-static {}, Lcom/google/b/f/b/a/h;->d()Lcom/google/b/f/b/a/h;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1299
    :goto_0
    return-object p0

    .line 1276
    :cond_0
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1277
    iget v2, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/f/b/a/j;->a:I

    .line 1278
    iget-object v2, p1, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/b/f/b/a/j;->b:Ljava/lang/Object;

    .line 1281
    :cond_1
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1282
    iget v2, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/b/f/b/a/j;->a:I

    .line 1283
    iget-object v2, p1, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/b/f/b/a/j;->c:Ljava/lang/Object;

    .line 1286
    :cond_2
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1287
    iget v2, p1, Lcom/google/b/f/b/a/h;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/gb;->a(I)Lcom/google/r/b/a/gb;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1276
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1281
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1286
    goto :goto_3

    .line 1287
    :cond_7
    iget v3, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/b/f/b/a/j;->a:I

    iget v2, v2, Lcom/google/r/b/a/gb;->h:I

    iput v2, p0, Lcom/google/b/f/b/a/j;->d:I

    .line 1289
    :cond_8
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 1290
    iget-boolean v2, p1, Lcom/google/b/f/b/a/h;->e:Z

    iget v3, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/b/f/b/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/b/a/j;->e:Z

    .line 1292
    :cond_9
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 1293
    iget-boolean v2, p1, Lcom/google/b/f/b/a/h;->f:Z

    iget v3, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/b/f/b/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/b/f/b/a/j;->f:Z

    .line 1295
    :cond_a
    iget v2, p1, Lcom/google/b/f/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_e

    :goto_6
    if-eqz v0, :cond_b

    .line 1296
    iget-object v0, p1, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/b/f/b/a/k;->d()Lcom/google/b/f/b/a/k;

    move-result-object v0

    :goto_7
    iget v1, p0, Lcom/google/b/f/b/a/j;->a:I

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v4, :cond_10

    iget-object v1, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    invoke-static {}, Lcom/google/b/f/b/a/k;->d()Lcom/google/b/f/b/a/k;

    move-result-object v2

    if-eq v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    invoke-static {v1}, Lcom/google/b/f/b/a/k;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/m;->a(Lcom/google/b/f/b/a/k;)Lcom/google/b/f/b/a/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/b/a/m;->c()Lcom/google/b/f/b/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    :goto_8
    iget v0, p0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/b/f/b/a/j;->a:I

    .line 1298
    :cond_b
    iget-object v0, p1, Lcom/google/b/f/b/a/h;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 1289
    goto :goto_4

    :cond_d
    move v2, v1

    .line 1292
    goto :goto_5

    :cond_e
    move v0, v1

    .line 1295
    goto :goto_6

    .line 1296
    :cond_f
    iget-object v0, p1, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    goto :goto_7

    :cond_10
    iput-object v0, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1211
    new-instance v2, Lcom/google/b/f/b/a/h;

    invoke-direct {v2, p0}, Lcom/google/b/f/b/a/h;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/b/f/b/a/j;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/b/f/b/a/j;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/b/a/h;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/b/f/b/a/j;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/b/f/b/a/h;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/b/f/b/a/j;->d:I

    iput v1, v2, Lcom/google/b/f/b/a/h;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/b/f/b/a/j;->e:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/h;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/b/f/b/a/j;->f:Z

    iput-boolean v1, v2, Lcom/google/b/f/b/a/h;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/b/f/b/a/j;->g:Lcom/google/b/f/b/a/k;

    iput-object v1, v2, Lcom/google/b/f/b/a/h;->g:Lcom/google/b/f/b/a/k;

    iput v0, v2, Lcom/google/b/f/b/a/h;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1211
    check-cast p1, Lcom/google/b/f/b/a/h;

    invoke-virtual {p0, p1}, Lcom/google/b/f/b/a/j;->a(Lcom/google/b/f/b/a/h;)Lcom/google/b/f/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1303
    const/4 v0, 0x1

    return v0
.end method
