.class public Lcom/google/b/j/a/m;
.super Ljava/util/concurrent/FutureTask;
.source "PG"

# interfaces
.implements Lcom/google/b/j/a/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;",
        "Lcom/google/b/j/a/l",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/b/j/a/e;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 42
    new-instance v0, Lcom/google/b/j/a/e;

    invoke-direct {v0}, Lcom/google/b/j/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/b/j/a/m;->a:Lcom/google/b/j/a/e;

    .line 77
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 42
    new-instance v0, Lcom/google/b/j/a/e;

    invoke-direct {v0}, Lcom/google/b/j/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/b/j/a/m;->a:Lcom/google/b/j/a/e;

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/b/j/a/m;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TV;)",
            "Lcom/google/b/j/a/m",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/google/b/j/a/m;

    invoke-direct {v0, p0, p1}, Lcom/google/b/j/a/m;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lcom/google/b/j/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "Lcom/google/b/j/a/m",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/google/b/j/a/m;

    invoke-direct {v0, p0}, Lcom/google/b/j/a/m;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/b/j/a/m;->a:Lcom/google/b/j/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/b/j/a/e;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 81
    return-void
.end method

.method protected done()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/b/j/a/m;->a:Lcom/google/b/j/a/e;

    invoke-virtual {v0}, Lcom/google/b/j/a/e;->a()V

    .line 90
    return-void
.end method
