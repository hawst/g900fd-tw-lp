.class public final Lcom/google/b/j/a/o;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Lcom/google/b/j/a/n;Ljava/util/Collection;ZJ)Ljava/lang/Object;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/j/a/n;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;ZJ)TT;"
        }
    .end annotation

    .prologue
    .line 511
    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 512
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v3

    .line 513
    if-lez v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 514
    :cond_2
    if-ltz v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 515
    new-instance v13, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v13}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 526
    const/4 v4, 0x0

    .line 527
    if-eqz p2, :cond_5

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 528
    :goto_2
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 530
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Callable;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Lcom/google/b/j/a/n;->a(Ljava/util/concurrent/Callable;)Lcom/google/b/j/a/l;

    move-result-object v2

    new-instance v5, Lcom/google/b/j/a/p;

    invoke-direct {v5, v13, v2}, Lcom/google/b/j/a/p;-><init>(Ljava/util/concurrent/BlockingQueue;Lcom/google/b/j/a/l;)V

    new-instance v8, Lcom/google/b/j/a/q;

    invoke-direct {v8}, Lcom/google/b/j/a/q;-><init>()V

    invoke-interface {v2, v5, v8}, Lcom/google/b/j/a/l;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    add-int/lit8 v5, v3, -0x1

    .line 532
    const/4 v2, 0x1

    move v3, v5

    move-wide/from16 v10, p3

    move v5, v2

    .line 535
    :goto_3
    invoke-interface {v13}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 536
    if-nez v2, :cond_e

    .line 537
    if-lez v3, :cond_6

    .line 538
    add-int/lit8 v8, v3, -0x1

    .line 539
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Callable;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Lcom/google/b/j/a/n;->a(Ljava/util/concurrent/Callable;)Lcom/google/b/j/a/l;

    move-result-object v3

    new-instance v9, Lcom/google/b/j/a/p;

    invoke-direct {v9, v13, v3}, Lcom/google/b/j/a/p;-><init>(Ljava/util/concurrent/BlockingQueue;Lcom/google/b/j/a/l;)V

    new-instance v15, Lcom/google/b/j/a/q;

    invoke-direct {v15}, Lcom/google/b/j/a/q;-><init>()V

    invoke-interface {v3, v9, v15}, Lcom/google/b/j/a/l;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    add-int/lit8 v3, v5, 0x1

    move v5, v8

    move-wide v8, v10

    move-object/from16 v16, v2

    move v2, v3

    move-object/from16 v3, v16

    .line 555
    :goto_4
    if-eqz v3, :cond_d

    .line 556
    add-int/lit8 v2, v2, -0x1

    .line 558
    :try_start_1
    invoke-interface {v3}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 572
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 573
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_5

    .line 527
    :cond_5
    const-wide/16 v6, 0x0

    goto :goto_2

    .line 541
    :cond_6
    if-eqz v5, :cond_9

    .line 542
    if-eqz p2, :cond_8

    .line 544
    :try_start_2
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v13, v10, v11, v2}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 545
    if-nez v2, :cond_7

    .line 546
    new-instance v2, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v2}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 572
    :catchall_0
    move-exception v2

    move-object v3, v2

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 573
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_6

    .line 548
    :cond_7
    :try_start_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 549
    sub-long v6, v8, v6

    sub-long v6, v10, v6

    move-object/from16 v16, v2

    move v2, v5

    move v5, v3

    move-object/from16 v3, v16

    move-wide/from16 v17, v8

    move-wide v8, v6

    move-wide/from16 v6, v17

    .line 551
    goto :goto_4

    .line 552
    :cond_8
    invoke-interface {v13}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    move-wide v8, v10

    move/from16 v16, v5

    move v5, v3

    move-object v3, v2

    move/from16 v2, v16

    goto :goto_4

    .line 559
    :catch_0
    move-exception v4

    move v3, v5

    move-wide v10, v8

    move v5, v2

    .line 563
    goto/16 :goto_3

    .line 561
    :catch_1
    move-exception v4

    .line 562
    new-instance v3, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v3, v4}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    :goto_7
    move-object v4, v3

    move-wide v10, v8

    move v3, v5

    move v5, v2

    .line 565
    goto/16 :goto_3

    .line 567
    :cond_9
    if-nez v4, :cond_a

    .line 568
    new-instance v4, Ljava/util/concurrent/ExecutionException;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    .line 570
    :cond_a
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 574
    :cond_b
    throw v3

    :cond_c
    return-object v3

    :cond_d
    move-object v3, v4

    goto :goto_7

    :cond_e
    move-wide v8, v10

    move/from16 v16, v5

    move v5, v3

    move-object v3, v2

    move/from16 v2, v16

    goto/16 :goto_4
.end method
