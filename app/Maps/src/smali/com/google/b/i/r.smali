.class abstract enum Lcom/google/b/i/r;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/i/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/i/r;

.field public static final enum b:Lcom/google/b/i/r;

.field static final c:Lcom/google/b/i/r;

.field private static final synthetic d:[Lcom/google/b/i/r;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 104
    new-instance v0, Lcom/google/b/i/s;

    const-string v2, "OWNED_BY_ENCLOSING_CLASS"

    invoke-direct {v0, v2, v1}, Lcom/google/b/i/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/r;->a:Lcom/google/b/i/r;

    .line 111
    new-instance v0, Lcom/google/b/i/u;

    const-string v2, "LOCAL_CLASS_HAS_NO_OWNER"

    invoke-direct {v0, v2, v3}, Lcom/google/b/i/u;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/r;->b:Lcom/google/b/i/r;

    .line 102
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/b/i/r;

    sget-object v2, Lcom/google/b/i/r;->a:Lcom/google/b/i/r;

    aput-object v2, v0, v1

    sget-object v2, Lcom/google/b/i/r;->b:Lcom/google/b/i/r;

    aput-object v2, v0, v3

    sput-object v0, Lcom/google/b/i/r;->d:[Lcom/google/b/i/r;

    .line 126
    new-instance v0, Lcom/google/b/i/v;

    invoke-direct {v0}, Lcom/google/b/i/v;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-static {}, Lcom/google/b/i/r;->values()[Lcom/google/b/i/r;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    const-class v5, Lcom/google/b/i/t;

    invoke-virtual {v4, v5}, Lcom/google/b/i/r;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v6

    if-ne v5, v6, :cond_0

    sput-object v4, Lcom/google/b/i/r;->c:Lcom/google/b/i/r;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/i/r;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/google/b/i/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/i/r;

    return-object v0
.end method

.method public static values()[Lcom/google/b/i/r;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/b/i/r;->d:[Lcom/google/b/i/r;

    invoke-virtual {v0}, [Lcom/google/b/i/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/i/r;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Class;)Ljava/lang/Class;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method
