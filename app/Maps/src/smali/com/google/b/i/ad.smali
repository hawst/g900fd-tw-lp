.class final Lcom/google/b/i/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/reflect/WildcardType;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    .locals 1

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    const-string v0, "lower bound for wildcard"

    invoke-static {p1, v0}, Lcom/google/b/i/p;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 388
    const-string v0, "upper bound for wildcard"

    invoke-static {p2, v0}, Lcom/google/b/i/p;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 389
    sget-object v0, Lcom/google/b/i/x;->c:Lcom/google/b/i/x;

    invoke-virtual {v0, p1}, Lcom/google/b/i/x;->a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/i/ad;->a:Lcom/google/b/c/cv;

    .line 390
    sget-object v0, Lcom/google/b/i/x;->c:Lcom/google/b/i/x;

    invoke-virtual {v0, p2}, Lcom/google/b/i/x;->a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/i/ad;->b:Lcom/google/b/c/cv;

    .line 391
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 403
    instance-of v1, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v1, :cond_0

    .line 404
    check-cast p1, Ljava/lang/reflect/WildcardType;

    .line 405
    iget-object v1, p0, Lcom/google/b/i/ad;->a:Lcom/google/b/c/cv;

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/b/i/ad;->b:Lcom/google/b/c/cv;

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 408
    :cond_0
    return v0
.end method

.method public final getLowerBounds()[Ljava/lang/reflect/Type;
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/b/i/ad;->a:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final getUpperBounds()[Ljava/lang/reflect/Type;
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/b/i/ad;->b:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/b/i/ad;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/b/i/ad;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 418
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lcom/google/b/i/ad;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 420
    const-string v3, " super "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/b/i/p;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/b/i/ad;->b:Lcom/google/b/c/cv;

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/b/a/as;->a(Ljava/lang/Object;)Lcom/google/b/a/ar;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/a/as;->a(Lcom/google/b/a/ar;)Lcom/google/b/a/ar;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/b/c/eb;->a(Ljava/lang/Iterable;Lcom/google/b/a/ar;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 423
    const-string v3, " extends "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/b/i/p;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 425
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
