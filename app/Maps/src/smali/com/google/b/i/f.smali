.class public abstract Lcom/google/b/i/f;
.super Lcom/google/b/i/a;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/i/a",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/reflect/Type;

.field private transient b:Lcom/google/b/i/b;


# direct methods
.method protected constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    invoke-direct {p0}, Lcom/google/b/i/a;-><init>()V

    .line 123
    invoke-virtual {p0}, Lcom/google/b/i/f;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    .line 124
    iget-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead."

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 130
    :cond_1
    return-void
.end method

.method constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/b/i/a;-><init>()V

    .line 161
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/reflect/Type;

    iput-object p1, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    .line 162
    return-void
.end method

.method static a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/b/i/f",
            "<-TT;>;>;"
        }
    .end annotation

    .prologue
    .line 364
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 365
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, p0, v1

    .line 368
    new-instance v4, Lcom/google/b/i/g;

    invoke-direct {v4, v0}, Lcom/google/b/i/g;-><init>(Ljava/lang/reflect/Type;)V

    .line 369
    iget-object v0, v4, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {v2, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 365
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 373
    :cond_1
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Lcom/google/b/i/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/google/b/i/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 166
    new-instance v0, Lcom/google/b/i/g;

    invoke-direct {v0, p0}, Lcom/google/b/i/g;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private static b([Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 917
    invoke-static {}, Lcom/google/b/c/dn;->h()Lcom/google/b/c/dp;

    move-result-object v1

    .line 918
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 919
    invoke-static {v3}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/b/c/dp;->b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;

    .line 918
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 921
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/dp;->a()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/i/f",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 324
    new-instance v1, Lcom/google/b/i/g;

    invoke-direct {v1, p0}, Lcom/google/b/i/g;-><init>(Ljava/lang/reflect/Type;)V

    .line 325
    iget-object v0, v1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    const/4 v0, 0x0

    .line 331
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 897
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 898
    check-cast p0, Ljava/lang/Class;

    invoke-static {p0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    .line 910
    :goto_0
    return-object v0

    .line 899
    :cond_0
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_1

    .line 900
    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    .line 902
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    goto :goto_0

    .line 903
    :cond_1
    instance-of v0, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_2

    .line 904
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    .line 905
    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/b/i/p;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    goto :goto_0

    .line 907
    :cond_2
    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_3

    .line 908
    check-cast p0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/i/f;->b([Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    goto :goto_0

    .line 909
    :cond_3
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_4

    .line 910
    check-cast p0, Ljava/lang/reflect/WildcardType;

    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/i/f;->b([Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    goto :goto_0

    .line 912
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unsupported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method a(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/i/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 283
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/b/i/f;->b:Lcom/google/b/i/b;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    new-instance v1, Lcom/google/b/i/b;

    invoke-direct {v1}, Lcom/google/b/i/b;-><init>()V

    invoke-static {v0}, Lcom/google/b/i/d;->a(Ljava/lang/reflect/Type;)Lcom/google/b/c/dc;

    move-result-object v0

    invoke-static {}, Lcom/google/b/c/dc;->h()Lcom/google/b/c/dd;

    move-result-object v5

    iget-object v1, v1, Lcom/google/b/i/b;->a:Lcom/google/b/c/dc;

    invoke-virtual {v5, v1}, Lcom/google/b/c/dd;->a(Ljava/util/Map;)Lcom/google/b/c/dd;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    const-string v7, "Type variable %s bound to itself"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v1, v8, v4

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v7, v8}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    invoke-virtual {v5, v1, v0}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/b/i/b;

    invoke-virtual {v5}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/i/b;-><init>(Lcom/google/b/c/dc;)V

    iput-object v0, p0, Lcom/google/b/i/f;->b:Lcom/google/b/i/b;

    :cond_4
    invoke-virtual {v0, p1}, Lcom/google/b/i/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    new-instance v1, Lcom/google/b/i/g;

    invoke-direct {v1, v0}, Lcom/google/b/i/g;-><init>(Ljava/lang/reflect/Type;)V

    .line 285
    iget-object v0, p0, Lcom/google/b/i/f;->b:Lcom/google/b/i/b;

    iput-object v0, v1, Lcom/google/b/i/f;->b:Lcom/google/b/i/b;

    .line 286
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 686
    instance-of v0, p1, Lcom/google/b/i/f;

    if-eqz v0, :cond_0

    .line 687
    check-cast p1, Lcom/google/b/i/f;

    .line 688
    iget-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    iget-object v1, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 690
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/p;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 707
    new-instance v0, Lcom/google/b/i/b;

    invoke-direct {v0}, Lcom/google/b/i/b;-><init>()V

    iget-object v1, p0, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Lcom/google/b/i/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    new-instance v1, Lcom/google/b/i/g;

    invoke-direct {v1, v0}, Lcom/google/b/i/g;-><init>(Ljava/lang/reflect/Type;)V

    return-object v1
.end method
