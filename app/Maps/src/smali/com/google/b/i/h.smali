.class public abstract Lcom/google/b/i/h;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/b/i/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/i/h",
            "<",
            "Lcom/google/b/i/f",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/b/i/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/i/h",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1055
    new-instance v0, Lcom/google/b/i/i;

    invoke-direct {v0}, Lcom/google/b/i/i;-><init>()V

    sput-object v0, Lcom/google/b/i/h;->a:Lcom/google/b/i/h;

    .line 1073
    new-instance v0, Lcom/google/b/i/j;

    invoke-direct {v0}, Lcom/google/b/i/j;-><init>()V

    sput-object v0, Lcom/google/b/i/h;->b:Lcom/google/b/i/h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/util/Map;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Map",
            "<-TK;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1132
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1133
    if-eqz v0, :cond_0

    .line 1135
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1152
    :goto_0
    return v0

    .line 1137
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/b/i/h;->a(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1139
    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/b/i/h;->b(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1140
    invoke-direct {p0, v2, p2}, Lcom/google/b/i/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 1137
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1142
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/b/i/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1143
    if-eqz v1, :cond_3

    .line 1144
    invoke-direct {p0, v1, p2}, Lcom/google/b/i/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1151
    :cond_3
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TK;>;)",
            "Lcom/google/b/c/cv",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 1123
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1124
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1125
    invoke-direct {p0, v2, v0}, Lcom/google/b/i/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    goto :goto_0

    .line 1127
    :cond_0
    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/c/io;->a()Lcom/google/b/c/io;

    move-result-object v1

    new-instance v2, Lcom/google/b/i/k;

    invoke-direct {v2, v1, v0}, Lcom/google/b/i/k;-><init>(Ljava/util/Comparator;Ljava/util/Map;)V

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/util/Collection;

    :goto_1
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v0, v1

    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/es;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-static {v0}, Lcom/google/b/c/cv;->b([Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/Object;)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method abstract b(Ljava/lang/Object;)Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/lang/Iterable",
            "<+TK;>;"
        }
    .end annotation
.end method

.method abstract c(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation
.end method
