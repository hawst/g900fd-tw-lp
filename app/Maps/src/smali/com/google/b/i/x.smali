.class abstract enum Lcom/google/b/i/x;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/i/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/i/x;

.field public static final enum b:Lcom/google/b/i/x;

.field static final c:Lcom/google/b/i/x;

.field private static final synthetic d:[Lcom/google/b/i/x;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 459
    new-instance v0, Lcom/google/b/i/z;

    const-string v1, "JAVA6"

    invoke-direct {v0, v1, v2}, Lcom/google/b/i/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/x;->a:Lcom/google/b/i/x;

    .line 477
    new-instance v0, Lcom/google/b/i/aa;

    const-string v1, "JAVA7"

    invoke-direct {v0, v1, v3}, Lcom/google/b/i/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/x;->b:Lcom/google/b/i/x;

    .line 457
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/b/i/x;

    sget-object v1, Lcom/google/b/i/x;->a:Lcom/google/b/i/x;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/i/x;->b:Lcom/google/b/i/x;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/b/i/x;->d:[Lcom/google/b/i/x;

    .line 493
    new-instance v0, Lcom/google/b/i/y;

    invoke-direct {v0}, Lcom/google/b/i/y;-><init>()V

    invoke-virtual {v0}, Lcom/google/b/i/a;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/i/x;->b:Lcom/google/b/i/x;

    :goto_0
    sput-object v0, Lcom/google/b/i/x;->c:Lcom/google/b/i/x;

    return-void

    :cond_0
    sget-object v0, Lcom/google/b/i/x;->a:Lcom/google/b/i/x;

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/i/x;
    .locals 1

    .prologue
    .line 457
    const-class v0, Lcom/google/b/i/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/i/x;

    return-object v0
.end method

.method public static values()[Lcom/google/b/i/x;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lcom/google/b/i/x;->d:[Lcom/google/b/i/x;

    invoke-virtual {v0}, [Lcom/google/b/i/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/i/x;

    return-object v0
.end method


# virtual methods
.method final a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 502
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 503
    invoke-virtual {p0, v3}, Lcom/google/b/i/x;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 502
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 505
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method abstract b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method
