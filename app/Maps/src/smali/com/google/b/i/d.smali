.class final Lcom/google/b/i/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/b/i/e;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/TypeVariable",
            "<*>;",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 244
    new-instance v0, Lcom/google/b/i/e;

    invoke-direct {v0}, Lcom/google/b/i/e;-><init>()V

    sput-object v0, Lcom/google/b/i/d;->a:Lcom/google/b/i/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    .line 247
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/b/i/d;->c:Ljava/util/Set;

    return-void
.end method

.method static a(Ljava/lang/reflect/Type;)Lcom/google/b/c/dc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/b/c/dc",
            "<",
            "Ljava/lang/reflect/TypeVariable",
            "<*>;",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    new-instance v0, Lcom/google/b/i/d;

    invoke-direct {v0}, Lcom/google/b/i/d;-><init>()V

    .line 255
    sget-object v1, Lcom/google/b/i/d;->a:Lcom/google/b/i/e;

    invoke-virtual {v1, p0}, Lcom/google/b/i/e;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    .line 256
    iget-object v0, v0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/b/c/dc;->a(Ljava/util/Map;)Lcom/google/b/c/dc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    .line 280
    invoke-virtual {p1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 281
    invoke-direct {p0, v3}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/reflect/Type;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 260
    iget-object v0, p0, Lcom/google/b/i/d;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_8

    .line 264
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v4

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    array-length v2, v4

    array-length v3, v5

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v2, v1

    :goto_2
    array-length v1, v4

    if-ge v2, v1, :cond_7

    aget-object v6, v4, v2

    aget-object v3, v5, v2

    iget-object v1, p0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    move-object v1, v3

    :goto_3
    if-eqz v1, :cond_5

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v1, v3

    :goto_4
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    goto :goto_4

    :cond_4
    iget-object v7, p0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/google/b/i/d;->b:Ljava/util/Map;

    invoke-interface {v1, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/b/i/d;->a(Ljava/lang/Class;)V

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    goto :goto_0

    .line 265
    :cond_8
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_9

    .line 266
    check-cast p1, Ljava/lang/Class;

    invoke-direct {p0, p1}, Lcom/google/b/i/d;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 267
    :cond_9
    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_a

    .line 268
    check-cast p1, Ljava/lang/reflect/TypeVariable;

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    array-length v2, v0

    :goto_5
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 269
    invoke-direct {p0, v3}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    .line 268
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 271
    :cond_a
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_0

    .line 272
    check-cast p1, Ljava/lang/reflect/WildcardType;

    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    array-length v2, v0

    :goto_6
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 273
    invoke-direct {p0, v3}, Lcom/google/b/i/d;->b(Ljava/lang/reflect/Type;)V

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method
