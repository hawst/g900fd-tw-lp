.class final enum Lcom/google/b/i/z;
.super Lcom/google/b/i/x;
.source "PG"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 459
    invoke-direct {p0, p1, p2}, Lcom/google/b/i/x;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method final synthetic a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 459
    new-instance v0, Lcom/google/b/i/w;

    invoke-direct {v0, p1}, Lcom/google/b/i/w;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method final b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 2

    .prologue
    .line 467
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 468
    :cond_0
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 469
    check-cast v0, Ljava/lang/Class;

    .line 470
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    new-instance p1, Lcom/google/b/i/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/b/i/w;-><init>(Ljava/lang/reflect/Type;)V

    .line 474
    :cond_1
    return-object p1
.end method
