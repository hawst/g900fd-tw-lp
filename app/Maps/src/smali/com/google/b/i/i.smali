.class final Lcom/google/b/i/i;
.super Lcom/google/b/i/h;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/i/h",
        "<",
        "Lcom/google/b/i/f",
        "<*>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1055
    invoke-direct {p0}, Lcom/google/b/i/h;-><init>()V

    return-void
.end method


# virtual methods
.method final synthetic a(Ljava/lang/Object;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1055
    check-cast p1, Lcom/google/b/i/f;

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Iterable;
    .locals 5

    .prologue
    .line 1055
    check-cast p1, Lcom/google/b/i/f;

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/i/f;->a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/i/f;->a([Ljava/lang/reflect/Type;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {p1, v4}, Lcom/google/b/i/f;->a(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method

.method final synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1055
    check-cast p1, Lcom/google/b/i/f;

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/google/b/i/f;->b(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/google/b/i/f;->b(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/b/i/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/b/i/f;->c(Ljava/lang/reflect/Type;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/b/i/f;->a(Ljava/lang/reflect/Type;)Lcom/google/b/i/f;

    move-result-object v0

    goto :goto_0
.end method
