.class abstract enum Lcom/google/b/i/l;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/b/a/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/i/l;",
        ">;",
        "Lcom/google/b/a/ar",
        "<",
        "Lcom/google/b/i/f",
        "<*>;>;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/i/l;

.field public static final enum b:Lcom/google/b/i/l;

.field private static final synthetic c:[Lcom/google/b/i/l;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 669
    new-instance v0, Lcom/google/b/i/m;

    const-string v1, "IGNORE_TYPE_VARIABLE_OR_WILDCARD"

    invoke-direct {v0, v1, v2}, Lcom/google/b/i/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/l;->a:Lcom/google/b/i/l;

    .line 674
    new-instance v0, Lcom/google/b/i/n;

    const-string v1, "INTERFACE_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/google/b/i/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/i/l;->b:Lcom/google/b/i/l;

    .line 667
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/b/i/l;

    sget-object v1, Lcom/google/b/i/l;->a:Lcom/google/b/i/l;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/i/l;->b:Lcom/google/b/i/l;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/b/i/l;->c:[Lcom/google/b/i/l;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 667
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/i/l;
    .locals 1

    .prologue
    .line 667
    const-class v0, Lcom/google/b/i/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/i/l;

    return-object v0
.end method

.method public static values()[Lcom/google/b/i/l;
    .locals 1

    .prologue
    .line 667
    sget-object v0, Lcom/google/b/i/l;->c:[Lcom/google/b/i/l;

    invoke-virtual {v0}, [Lcom/google/b/i/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/i/l;

    return-object v0
.end method
