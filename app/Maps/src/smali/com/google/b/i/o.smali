.class public Lcom/google/b/i/o;
.super Lcom/google/b/c/bg;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/bg",
        "<",
        "Lcom/google/b/i/f",
        "<-TT;>;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final synthetic a:Lcom/google/b/i/f;

.field private transient b:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/b/i/f",
            "<-TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/b/i/f;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/google/b/i/o;->a:Lcom/google/b/i/f;

    invoke-direct {p0}, Lcom/google/b/c/bg;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/google/b/i/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/google/b/i/o;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/b/i/f",
            "<-TT;>;>;"
        }
    .end annotation

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/b/i/o;->b:Lcom/google/b/c/dn;

    .line 549
    if-nez v0, :cond_0

    .line 552
    sget-object v0, Lcom/google/b/i/h;->a:Lcom/google/b/i/h;

    iget-object v1, p0, Lcom/google/b/i/o;->a:Lcom/google/b/i/f;

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/b/i/h;->a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 554
    instance-of v1, v0, Lcom/google/b/c/ay;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/b/c/ay;

    :goto_0
    sget-object v1, Lcom/google/b/i/l;->a:Lcom/google/b/i/l;

    iget-object v0, v0, Lcom/google/b/c/ay;->a:Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lcom/google/b/c/eb;->a(Ljava/lang/Iterable;Lcom/google/b/a/ar;)Ljava/lang/Iterable;

    move-result-object v0

    instance-of v1, v0, Lcom/google/b/c/ay;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/b/c/ay;

    :goto_1
    iget-object v0, v0, Lcom/google/b/c/ay;->a:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/google/b/c/dn;->a(Ljava/lang/Iterable;)Lcom/google/b/c/dn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/i/o;->b:Lcom/google/b/c/dn;

    .line 557
    :cond_0
    return-object v0

    .line 554
    :cond_1
    new-instance v1, Lcom/google/b/c/az;

    invoke-direct {v1, v0, v0}, Lcom/google/b/c/az;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/b/c/az;

    invoke-direct {v1, v0, v0}, Lcom/google/b/c/az;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    move-object v0, v1

    goto :goto_1
.end method
