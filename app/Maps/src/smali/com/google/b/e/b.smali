.class final Lcom/google/b/e/b;
.super Lcom/google/b/a/f;
.source "PG"


# instance fields
.field final f:[C

.field final g:I

.field final h:I

.field final i:I

.field final j:I

.field final k:[B

.field final l:[Z

.field private final m:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[C)V
    .locals 10

    .prologue
    const/16 v4, 0x8

    const/4 v9, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 458
    invoke-direct {p0}, Lcom/google/b/a/f;-><init>()V

    .line 459
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/b/e/b;->m:Ljava/lang/String;

    .line 460
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, [C

    iput-object v0, p0, Lcom/google/b/e/b;->f:[C

    .line 462
    :try_start_0
    array-length v0, p2

    sget-object v3, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {v0, v3}, Lcom/google/b/g/d;->a(ILjava/math/RoundingMode;)I

    move-result v0

    iput v0, p0, Lcom/google/b/e/b;->h:I
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    iget v0, p0, Lcom/google/b/e/b;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->lowestOneBit(I)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 472
    div-int v3, v4, v0

    iput v3, p0, Lcom/google/b/e/b;->i:I

    .line 473
    iget v3, p0, Lcom/google/b/e/b;->h:I

    div-int v0, v3, v0

    iput v0, p0, Lcom/google/b/e/b;->j:I

    .line 475
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/e/b;->g:I

    .line 477
    const/16 v0, 0x80

    new-array v4, v0, [B

    .line 478
    invoke-static {v4, v9}, Ljava/util/Arrays;->fill([BB)V

    move v3, v2

    .line 479
    :goto_0
    array-length v0, p2

    if-ge v3, v0, :cond_5

    .line 480
    aget-char v5, p2, v3

    .line 481
    sget-object v0, Lcom/google/b/a/f;->b:Lcom/google/b/a/f;

    invoke-virtual {v0, v5}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    const-string v6, "Non-ASCII character: %s"

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v6, v7}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :catch_0
    move-exception v0

    .line 464
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal alphabet length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 482
    :cond_2
    aget-byte v0, v4, v5

    if-ne v0, v9, :cond_3

    move v0, v1

    :goto_1
    const-string v6, "Duplicate character: %s"

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    aput-object v8, v7, v2

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v6, v7}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 483
    :cond_4
    int-to-byte v0, v3

    aput-byte v0, v4, v5

    .line 479
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 485
    :cond_5
    iput-object v4, p0, Lcom/google/b/e/b;->k:[B

    .line 487
    iget v0, p0, Lcom/google/b/e/b;->i:I

    new-array v0, v0, [Z

    .line 488
    :goto_2
    iget v3, p0, Lcom/google/b/e/b;->j:I

    if-ge v2, v3, :cond_6

    .line 489
    shl-int/lit8 v3, v2, 0x3

    iget v4, p0, Lcom/google/b/e/b;->h:I

    sget-object v5, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v3, v4, v5}, Lcom/google/b/g/d;->a(IILjava/math/RoundingMode;)I

    move-result v3

    aput-boolean v1, v0, v3

    .line 488
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 491
    :cond_6
    iput-object v0, p0, Lcom/google/b/e/b;->l:[Z

    .line 492
    return-void
.end method


# virtual methods
.method public final b(C)Z
    .locals 2

    .prologue
    .line 555
    sget-object v0, Lcom/google/b/a/f;->b:Lcom/google/b/a/f;

    invoke-virtual {v0, p1}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/e/b;->k:[B

    aget-byte v0, v0, p1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/b/e/b;->m:Ljava/lang/String;

    return-object v0
.end method
