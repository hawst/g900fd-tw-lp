.class public final Lcom/google/b/e/t;
.super Lcom/google/b/e/f;
.source "PG"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/b/e/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/io/File;[Lcom/google/b/e/r;)V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/b/e/f;-><init>()V

    .line 211
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/io/File;

    iput-object p1, p0, Lcom/google/b/e/t;->a:Ljava/io/File;

    .line 212
    invoke-static {p2}, Lcom/google/b/c/dn;->a([Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/e/t;->b:Lcom/google/b/c/dn;

    .line 213
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 205
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/b/e/t;->a:Ljava/io/File;

    iget-object v2, p0, Lcom/google/b/e/t;->b:Lcom/google/b/c/dn;

    sget-object v3, Lcom/google/b/e/r;->a:Lcom/google/b/e/r;

    invoke-virtual {v2, v3}, Lcom/google/b/c/dn;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Files.asByteSink("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/b/e/t;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/e/t;->b:Lcom/google/b/c/dn;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
