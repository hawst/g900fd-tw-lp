.class final Lcom/google/b/e/c;
.super Lcom/google/b/e/a;
.source "PG"


# instance fields
.field final a:Lcom/google/b/e/b;

.field final b:Ljava/lang/Character;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/b/e/b;Ljava/lang/Character;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 575
    invoke-direct {p0}, Lcom/google/b/e/a;-><init>()V

    .line 576
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/b/e/b;

    iput-object v0, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    .line 577
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Character;->charValue()C

    move-result v0

    sget-object v3, Lcom/google/b/a/f;->b:Lcom/google/b/a/f;

    invoke-virtual {v3, v0}, Lcom/google/b/a/f;->b(C)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p1, Lcom/google/b/e/b;->k:[B

    aget-byte v0, v3, v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Padding character %s was already in alphabet"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v2}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 579
    :cond_4
    iput-object p2, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    .line 580
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
    .locals 2
    .param p3    # Ljava/lang/Character;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 572
    new-instance v0, Lcom/google/b/e/b;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/b/e/b;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0, p3}, Lcom/google/b/e/c;-><init>(Lcom/google/b/e/b;Ljava/lang/Character;)V

    .line 573
    return-void
.end method


# virtual methods
.method final a(I)I
    .locals 3

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v0, v0, Lcom/google/b/e/b;->i:I

    iget-object v1, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->j:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, v1, v2}, Lcom/google/b/g/d;->a(IILjava/math/RoundingMode;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method final a()Lcom/google/b/a/f;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/b/a/f;->c:Lcom/google/b/a/f;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Lcom/google/b/a/f;->a(C)Lcom/google/b/a/f;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Lcom/google/b/e/z;)Lcom/google/b/e/x;
    .locals 1

    .prologue
    .line 640
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 641
    :cond_0
    new-instance v0, Lcom/google/b/e/e;

    invoke-direct {v0, p0, p1}, Lcom/google/b/e/e;-><init>(Lcom/google/b/e/c;Lcom/google/b/e/z;)V

    return-object v0
.end method

.method final a(Lcom/google/b/e/aa;)Lcom/google/b/e/y;
    .locals 1

    .prologue
    .line 594
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 595
    :cond_0
    new-instance v0, Lcom/google/b/e/d;

    invoke-direct {v0, p0, p1}, Lcom/google/b/e/d;-><init>(Lcom/google/b/e/c;Lcom/google/b/e/aa;)V

    return-object v0
.end method

.method final b(I)I
    .locals 4

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v0, v0, Lcom/google/b/e/b;->h:I

    int-to-long v0, v0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x7

    add-long/2addr v0, v2

    const-wide/16 v2, 0x8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final b()Lcom/google/b/e/a;
    .locals 3

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/b/e/c;

    iget-object v1, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/b/e/c;-><init>(Lcom/google/b/e/b;Ljava/lang/Character;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 737
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BaseEncoding."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 738
    iget-object v1, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    invoke-virtual {v1}, Lcom/google/b/e/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v2, v2, Lcom/google/b/e/b;->h:I

    rem-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 740
    iget-object v1, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    if-nez v1, :cond_1

    .line 741
    const-string v1, ".omitPadding()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 743
    :cond_1
    const-string v1, ".withPadChar("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
