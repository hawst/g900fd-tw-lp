.class Lcom/google/b/e/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/e/y;


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lcom/google/b/e/aa;

.field final synthetic e:Lcom/google/b/e/c;


# direct methods
.method constructor <init>(Lcom/google/b/e/c;Lcom/google/b/e/aa;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 595
    iput-object p1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iput-object p2, p0, Lcom/google/b/e/d;->d:Lcom/google/b/e/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 596
    iput v0, p0, Lcom/google/b/e/d;->a:I

    .line 597
    iput v0, p0, Lcom/google/b/e/d;->b:I

    .line 598
    iput v0, p0, Lcom/google/b/e/d;->c:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 617
    iget v0, p0, Lcom/google/b/e/d;->b:I

    if-lez v0, :cond_0

    .line 618
    iget v0, p0, Lcom/google/b/e/d;->a:I

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->h:I

    iget v2, p0, Lcom/google/b/e/d;->b:I

    sub-int/2addr v1, v2

    shl-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->g:I

    and-int/2addr v0, v1

    .line 619
    iget-object v1, p0, Lcom/google/b/e/d;->d:Lcom/google/b/e/aa;

    iget-object v2, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v2, v2, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget-object v2, v2, Lcom/google/b/e/b;->f:[C

    aget-char v0, v2, v0

    invoke-interface {v1, v0}, Lcom/google/b/e/aa;->a(C)V

    .line 620
    iget v0, p0, Lcom/google/b/e/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/e/d;->c:I

    .line 621
    iget-object v0, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v0, v0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    if-eqz v0, :cond_0

    .line 622
    :goto_0
    iget v0, p0, Lcom/google/b/e/d;->c:I

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->i:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/google/b/e/d;->d:Lcom/google/b/e/aa;

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/b/e/aa;->a(C)V

    .line 624
    iget v0, p0, Lcom/google/b/e/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/e/d;->c:I

    goto :goto_0

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/google/b/e/d;->d:Lcom/google/b/e/aa;

    .line 629
    return-void
.end method

.method public final a(B)V
    .locals 3

    .prologue
    .line 601
    iget v0, p0, Lcom/google/b/e/d;->a:I

    shl-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/e/d;->a:I

    .line 602
    iget v0, p0, Lcom/google/b/e/d;->a:I

    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/e/d;->a:I

    .line 603
    iget v0, p0, Lcom/google/b/e/d;->b:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/b/e/d;->b:I

    .line 604
    :goto_0
    iget v0, p0, Lcom/google/b/e/d;->b:I

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->h:I

    if-lt v0, v1, :cond_0

    .line 605
    iget v0, p0, Lcom/google/b/e/d;->a:I

    iget v1, p0, Lcom/google/b/e/d;->b:I

    iget-object v2, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v2, v2, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v2, v2, Lcom/google/b/e/b;->h:I

    sub-int/2addr v1, v2

    shr-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->g:I

    and-int/2addr v0, v1

    .line 606
    iget-object v1, p0, Lcom/google/b/e/d;->d:Lcom/google/b/e/aa;

    iget-object v2, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v2, v2, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget-object v2, v2, Lcom/google/b/e/b;->f:[C

    aget-char v0, v2, v0

    invoke-interface {v1, v0}, Lcom/google/b/e/aa;->a(C)V

    .line 607
    iget v0, p0, Lcom/google/b/e/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/e/d;->c:I

    .line 608
    iget v0, p0, Lcom/google/b/e/d;->b:I

    iget-object v1, p0, Lcom/google/b/e/d;->e:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v1, v1, Lcom/google/b/e/b;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/e/d;->b:I

    goto :goto_0

    .line 610
    :cond_0
    return-void
.end method
