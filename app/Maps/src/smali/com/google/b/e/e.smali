.class Lcom/google/b/e/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/e/x;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Z

.field final e:Lcom/google/b/a/f;

.field final synthetic f:Lcom/google/b/e/z;

.field final synthetic g:Lcom/google/b/e/c;


# direct methods
.method constructor <init>(Lcom/google/b/e/c;Lcom/google/b/e/z;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 641
    iput-object p1, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iput-object p2, p0, Lcom/google/b/e/e;->f:Lcom/google/b/e/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642
    iput v0, p0, Lcom/google/b/e/e;->a:I

    .line 643
    iput v0, p0, Lcom/google/b/e/e;->b:I

    .line 644
    iput v0, p0, Lcom/google/b/e/e;->c:I

    .line 645
    iput-boolean v0, p0, Lcom/google/b/e/e;->d:Z

    .line 646
    iget-object v0, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v1, v0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/b/a/f;->c:Lcom/google/b/a/f;

    :goto_0
    iput-object v0, p0, Lcom/google/b/e/e;->e:Lcom/google/b/a/f;

    return-void

    :cond_0
    iget-object v0, v0, Lcom/google/b/e/c;->b:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Lcom/google/b/a/f;->a(C)Lcom/google/b/a/f;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 650
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/b/e/e;->f:Lcom/google/b/e/z;

    invoke-interface {v1}, Lcom/google/b/e/z;->a()I

    move-result v1

    .line 651
    if-ne v1, v0, :cond_1

    .line 652
    iget-boolean v1, p0, Lcom/google/b/e/e;->d:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v2, p0, Lcom/google/b/e/e;->c:I

    iget-object v3, v1, Lcom/google/b/e/b;->l:[Z

    iget v1, v1, Lcom/google/b/e/b;->i:I

    rem-int v1, v2, v1

    aget-boolean v1, v3, v1

    if-nez v1, :cond_8

    .line 653
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid input length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/b/e/e;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 657
    :cond_1
    iget v2, p0, Lcom/google/b/e/e;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/e/e;->c:I

    .line 658
    int-to-char v1, v1

    .line 659
    iget-object v2, p0, Lcom/google/b/e/e;->e:Lcom/google/b/a/f;

    invoke-virtual {v2, v1}, Lcom/google/b/a/f;->b(C)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 660
    iget-boolean v1, p0, Lcom/google/b/e/e;->d:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/b/e/e;->c:I

    if-eq v1, v5, :cond_2

    iget-object v1, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v1, v1, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v2, p0, Lcom/google/b/e/e;->c:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, v1, Lcom/google/b/e/b;->l:[Z

    iget v1, v1, Lcom/google/b/e/b;->i:I

    rem-int v1, v2, v1

    aget-boolean v1, v3, v1

    if-nez v1, :cond_3

    .line 662
    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Padding cannot start at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/b/e/e;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_3
    iput-boolean v5, p0, Lcom/google/b/e/e;->d:Z

    goto :goto_0

    .line 665
    :cond_4
    iget-boolean v2, p0, Lcom/google/b/e/e;->d:Z

    if-eqz v2, :cond_5

    .line 666
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected padding character but found \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/b/e/e;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669
    :cond_5
    iget v2, p0, Lcom/google/b/e/e;->a:I

    iget-object v3, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v3, v3, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v3, v3, Lcom/google/b/e/b;->h:I

    shl-int/2addr v2, v3

    iput v2, p0, Lcom/google/b/e/e;->a:I

    .line 670
    iget v2, p0, Lcom/google/b/e/e;->a:I

    iget-object v3, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v3, v3, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    const/16 v4, 0x7f

    if-gt v1, v4, :cond_6

    iget-object v4, v3, Lcom/google/b/e/b;->k:[B

    aget-byte v4, v4, v1

    if-ne v4, v0, :cond_7

    :cond_6
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized character: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v3, v3, Lcom/google/b/e/b;->k:[B

    aget-byte v1, v3, v1

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/b/e/e;->a:I

    .line 671
    iget v1, p0, Lcom/google/b/e/e;->b:I

    iget-object v2, p0, Lcom/google/b/e/e;->g:Lcom/google/b/e/c;

    iget-object v2, v2, Lcom/google/b/e/c;->a:Lcom/google/b/e/b;

    iget v2, v2, Lcom/google/b/e/b;->h:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/b/e/e;->b:I

    .line 673
    iget v1, p0, Lcom/google/b/e/e;->b:I

    const/16 v2, 0x8

    if-lt v1, v2, :cond_0

    .line 674
    iget v0, p0, Lcom/google/b/e/e;->b:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/google/b/e/e;->b:I

    .line 675
    iget v0, p0, Lcom/google/b/e/e;->a:I

    iget v1, p0, Lcom/google/b/e/e;->b:I

    shr-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xff

    :cond_8
    return v0
.end method
