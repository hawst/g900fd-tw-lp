.class public abstract Lcom/google/b/e/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/b/e/a;

.field private static final b:Lcom/google/b/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x3d

    .line 346
    new-instance v0, Lcom/google/b/e/c;

    const-string v1, "base64()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/google/b/e/a;->a:Lcom/google/b/e/a;

    .line 366
    new-instance v0, Lcom/google/b/e/c;

    const-string v1, "base64Url()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, Lcom/google/b/e/a;->b:Lcom/google/b/e/a;

    .line 387
    new-instance v0, Lcom/google/b/e/c;

    const-string v1, "base32()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    .line 407
    new-instance v0, Lcom/google/b/e/c;

    const-string v1, "base32Hex()"

    const-string v2, "0123456789ABCDEFGHIJKLMNOPQRSTUV"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    .line 426
    new-instance v0, Lcom/google/b/e/c;

    const-string v1, "base16()"

    const-string v2, "0123456789ABCDEF"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/b/e/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static c()Lcom/google/b/e/a;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lcom/google/b/e/a;->a:Lcom/google/b/e/a;

    return-object v0
.end method

.method public static d()Lcom/google/b/e/a;
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/google/b/e/a;->b:Lcom/google/b/e/a;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method

.method abstract a()Lcom/google/b/a/f;
.end method

.method abstract a(Lcom/google/b/e/z;)Lcom/google/b/e/x;
.end method

.method abstract a(Lcom/google/b/e/aa;)Lcom/google/b/e/y;
.end method

.method public final a([BII)Ljava/lang/String;
    .locals 4

    .prologue
    .line 153
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 154
    :cond_0
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, Lcom/google/b/a/aq;->a(III)V

    .line 155
    invoke-virtual {p0, p3}, Lcom/google/b/e/a;->a(I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v2, Lcom/google/b/e/w;

    invoke-direct {v2, v1}, Lcom/google/b/e/w;-><init>(Ljava/lang/StringBuilder;)V

    .line 156
    invoke-virtual {p0, v2}, Lcom/google/b/e/a;->a(Lcom/google/b/e/aa;)Lcom/google/b/e/y;

    move-result-object v1

    .line 158
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    .line 159
    add-int v3, p2, v0

    :try_start_0
    aget-byte v3, p1, v3

    invoke-interface {v1, v3}, Lcom/google/b/e/y;->a(B)V

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    invoke-interface {v1}, Lcom/google/b/e/y;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 163
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "impossible"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Ljava/lang/CharSequence;)[B
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/google/b/e/a;->a()Lcom/google/b/a/f;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/b/a/f;->b(C)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 228
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 229
    :cond_2
    new-instance v1, Lcom/google/b/e/v;

    invoke-direct {v1, v0}, Lcom/google/b/e/v;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcom/google/b/e/a;->a(Lcom/google/b/e/z;)Lcom/google/b/e/x;

    move-result-object v5

    .line 230
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/b/e/a;->b(I)I

    move-result v0

    new-array v0, v0, [B

    .line 233
    :try_start_0
    invoke-interface {v5}, Lcom/google/b/e/x;->a()I

    move-result v1

    move v2, v3

    :goto_2
    const/4 v4, -0x1

    if-eq v1, v4, :cond_3

    .line 234
    add-int/lit8 v4, v2, 0x1

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    .line 233
    invoke-interface {v5}, Lcom/google/b/e/x;->a()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v2, v4

    goto :goto_2

    .line 236
    :catch_0
    move-exception v0

    .line 237
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 239
    :cond_3
    array-length v1, v0

    if-ne v2, v1, :cond_4

    :goto_3
    return-object v0

    :cond_4
    new-array v1, v2, [B

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    goto :goto_3
.end method

.method abstract b(I)I
.end method

.method public abstract b()Lcom/google/b/e/a;
.end method
