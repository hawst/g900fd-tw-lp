.class public final Lcom/google/b/e/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final d:Lcom/google/b/e/o;


# instance fields
.field final a:Lcom/google/b/e/o;

.field public final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lcom/google/b/e/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/e/n;->a:Lcom/google/b/e/n;

    :goto_0
    sput-object v0, Lcom/google/b/e/l;->d:Lcom/google/b/e/o;

    return-void

    :cond_0
    sget-object v0, Lcom/google/b/e/m;->a:Lcom/google/b/e/m;

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/b/e/o;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/b/e/l;->b:Ljava/util/LinkedList;

    .line 112
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/b/e/o;

    iput-object p1, p0, Lcom/google/b/e/l;->a:Lcom/google/b/e/o;

    .line 113
    return-void
.end method

.method public static a()Lcom/google/b/e/l;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/google/b/e/l;

    sget-object v1, Lcom/google/b/e/l;->d:Lcom/google/b/e/o;

    invoke-direct {v0, v1}, Lcom/google/b/e/l;-><init>(Lcom/google/b/e/o;)V

    return-object v0
.end method


# virtual methods
.method public final close()V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/b/e/l;->c:Ljava/lang/Throwable;

    move-object v1, v0

    .line 204
    :goto_0
    iget-object v0, p0, Lcom/google/b/e/l;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/b/e/l;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 207
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v2

    .line 209
    if-nez v1, :cond_0

    move-object v1, v2

    .line 210
    goto :goto_0

    .line 212
    :cond_0
    iget-object v3, p0, Lcom/google/b/e/l;->a:Lcom/google/b/e/o;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/b/e/o;->a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/b/e/l;->c:Ljava/lang/Throwable;

    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 218
    const-class v0, Ljava/io/IOException;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    invoke-static {v1}, Lcom/google/b/a/ca;->a(Ljava/lang/Throwable;)V

    .line 219
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 221
    :cond_3
    return-void
.end method
