.class public final Lcom/google/b/e/ab;
.super Ljava/io/FilterInputStream;
.source "PG"

# interfaces
.implements Ljava/io/DataInput;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 52
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/io/InputStream;

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final readBoolean()Z
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readUnsignedByte()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final readByte()B
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readUnsignedByte()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public final readChar()C
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readUnsignedShort()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public final readDouble()D
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final readFloat()F
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final readFully([B)V
    .locals 0

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/google/b/e/h;->a(Ljava/io/InputStream;[B)V

    .line 65
    return-void
.end method

.method public final readFully([BII)V
    .locals 0

    .prologue
    .line 68
    invoke-static {p0, p1, p2, p3}, Lcom/google/b/e/h;->a(Ljava/io/InputStream;[BII)V

    .line 69
    return-void
.end method

.method public final readInt()I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 111
    iget-object v0, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ne v4, v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    int-to-byte v0, v0

    .line 112
    iget-object v1, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v4, v1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    int-to-byte v1, v1

    .line 113
    iget-object v2, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    if-ne v4, v2, :cond_2

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_2
    int-to-byte v2, v2

    .line 114
    iget-object v3, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    if-ne v4, v3, :cond_3

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_3
    int-to-byte v3, v3

    .line 116
    shl-int/lit8 v3, v3, 0x18

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v3

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v1, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final readLine()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "readLine is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final readLong()J
    .locals 14

    .prologue
    const-wide/16 v12, 0xff

    const/4 v8, -0x1

    .line 129
    iget-object v0, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ne v8, v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    int-to-byte v0, v0

    .line 130
    iget-object v1, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v8, v1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    int-to-byte v1, v1

    .line 131
    iget-object v2, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    if-ne v8, v2, :cond_2

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_2
    int-to-byte v2, v2

    .line 132
    iget-object v3, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    if-ne v8, v3, :cond_3

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_3
    int-to-byte v3, v3

    .line 133
    iget-object v4, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v4

    if-ne v8, v4, :cond_4

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_4
    int-to-byte v4, v4

    .line 134
    iget-object v5, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v5

    if-ne v8, v5, :cond_5

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_5
    int-to-byte v5, v5

    .line 135
    iget-object v6, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    if-ne v8, v6, :cond_6

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_6
    int-to-byte v6, v6

    .line 136
    iget-object v7, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    if-ne v8, v7, :cond_7

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    int-to-byte v7, v7

    .line 138
    int-to-long v8, v7

    and-long/2addr v8, v12

    const/16 v7, 0x38

    shl-long/2addr v8, v7

    int-to-long v6, v6

    and-long/2addr v6, v12

    const/16 v10, 0x30

    shl-long/2addr v6, v10

    or-long/2addr v6, v8

    int-to-long v8, v5

    and-long/2addr v8, v12

    const/16 v5, 0x28

    shl-long/2addr v8, v5

    or-long/2addr v6, v8

    int-to-long v4, v4

    and-long/2addr v4, v12

    const/16 v8, 0x20

    shl-long/2addr v4, v8

    or-long/2addr v4, v6

    int-to-long v6, v3

    and-long/2addr v6, v12

    const/16 v3, 0x18

    shl-long/2addr v6, v3

    or-long/2addr v4, v6

    int-to-long v2, v2

    and-long/2addr v2, v12

    const/16 v6, 0x10

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    int-to-long v4, v1

    and-long/2addr v4, v12

    const/16 v1, 0x8

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    int-to-long v0, v0

    and-long/2addr v0, v12

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public final readShort()S
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/b/e/ab;->readUnsignedShort()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public final readUTF()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Ljava/io/DataInputStream;

    iget-object v1, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final readUnsignedByte()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 77
    if-gez v0, :cond_0

    .line 78
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 81
    :cond_0
    return v0
.end method

.method public final readUnsignedShort()I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 95
    iget-object v0, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ne v2, v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    int-to-byte v0, v0

    .line 96
    iget-object v1, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v2, v1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    int-to-byte v1, v1

    .line 98
    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public final skipBytes(I)I
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/b/e/ab;->in:Ljava/io/InputStream;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method
