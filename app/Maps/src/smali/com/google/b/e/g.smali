.class public abstract Lcom/google/b/e/g;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/e/f;)J
    .locals 6

    .prologue
    .line 197
    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 199
    :cond_0
    invoke-static {}, Lcom/google/b/e/l;->a()Lcom/google/b/e/l;

    move-result-object v4

    .line 201
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/e/g;->a()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, v4, Lcom/google/b/e/l;->b:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    check-cast v2, Ljava/io/InputStream;

    .line 202
    invoke-virtual {p1}, Lcom/google/b/e/f;->a()Ljava/io/OutputStream;

    move-result-object v3

    iget-object v5, v4, Lcom/google/b/e/l;->b:Ljava/util/LinkedList;

    invoke-virtual {v5, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    check-cast v3, Ljava/io/OutputStream;

    .line 203
    invoke-static {v2, v3}, Lcom/google/b/e/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 207
    invoke-virtual {v4}, Lcom/google/b/e/l;->close()V

    return-wide v2

    .line 204
    :catch_0
    move-exception v3

    .line 205
    :try_start_1
    iput-object v3, v4, Lcom/google/b/e/l;->c:Ljava/lang/Throwable;

    const-class v2, Ljava/io/IOException;

    if-eqz v3, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :catchall_0
    move-exception v2

    invoke-virtual {v4}, Lcom/google/b/e/l;->close()V

    throw v2

    .line 205
    :cond_1
    :try_start_2
    invoke-static {v3}, Lcom/google/b/a/ca;->a(Ljava/lang/Throwable;)V

    if-nez v3, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_2
    move-object v0, v3

    check-cast v0, Ljava/lang/Throwable;

    move-object v2, v0

    invoke-static {v2}, Lcom/google/b/a/ca;->a(Ljava/lang/Throwable;)V

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public abstract a()Ljava/io/InputStream;
.end method
