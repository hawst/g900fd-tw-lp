.class Lcom/google/b/a/v;
.super Lcom/google/b/a/f;
.source "PG"


# instance fields
.field private final f:[C

.field private final g:[C


# direct methods
.method constructor <init>(Ljava/lang/String;[C[C)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1}, Lcom/google/b/a/f;-><init>(Ljava/lang/String;)V

    .line 109
    iput-object p2, p0, Lcom/google/b/a/v;->f:[C

    .line 110
    iput-object p3, p0, Lcom/google/b/a/v;->g:[C

    .line 111
    array-length v0, p2

    array-length v2, p3

    if-ne v0, v2, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 112
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_6

    .line 113
    aget-char v2, p2, v0

    aget-char v4, p3, v0

    if-gt v2, v4, :cond_2

    move v2, v3

    :goto_2
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    goto :goto_2

    .line 114
    :cond_3
    add-int/lit8 v2, v0, 0x1

    array-length v4, p2

    if-ge v2, v4, :cond_5

    .line 115
    aget-char v2, p3, v0

    add-int/lit8 v4, v0, 0x1

    aget-char v4, p2, v4

    if-ge v2, v4, :cond_4

    move v2, v3

    :goto_3
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    goto :goto_3

    .line 112
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :cond_6
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 103
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    return v0
.end method

.method public final b(C)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 122
    iget-object v1, p0, Lcom/google/b/a/v;->f:[C

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([CC)I

    move-result v1

    .line 123
    if-ltz v1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    xor-int/lit8 v1, v1, -0x1

    add-int/lit8 v1, v1, -0x1

    .line 127
    if-ltz v1, :cond_2

    iget-object v2, p0, Lcom/google/b/a/v;->g:[C

    aget-char v1, v2, v1

    if-le p1, v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
