.class Lcom/google/b/a/u;
.super Lcom/google/b/a/f;
.source "PG"


# instance fields
.field final f:Lcom/google/b/a/f;

.field final g:Lcom/google/b/a/f;


# direct methods
.method constructor <init>(Lcom/google/b/a/f;Lcom/google/b/a/f;)V
    .locals 2

    .prologue
    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CharMatcher.or("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/b/a/u;-><init>(Lcom/google/b/a/f;Lcom/google/b/a/f;Ljava/lang/String;)V

    .line 770
    return-void
.end method

.method private constructor <init>(Lcom/google/b/a/f;Lcom/google/b/a/f;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0, p3}, Lcom/google/b/a/f;-><init>(Ljava/lang/String;)V

    .line 764
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/b/a/f;

    iput-object p1, p0, Lcom/google/b/a/u;->f:Lcom/google/b/a/f;

    .line 765
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/b/a/f;

    iput-object p2, p0, Lcom/google/b/a/u;->g:Lcom/google/b/a/f;

    .line 766
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/google/b/a/f;
    .locals 3

    .prologue
    .line 786
    new-instance v0, Lcom/google/b/a/u;

    iget-object v1, p0, Lcom/google/b/a/u;->f:Lcom/google/b/a/f;

    iget-object v2, p0, Lcom/google/b/a/u;->g:Lcom/google/b/a/f;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/b/a/u;-><init>(Lcom/google/b/a/f;Lcom/google/b/a/f;Ljava/lang/String;)V

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 758
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    return v0
.end method

.method public final b(C)Z
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/b/a/u;->f:Lcom/google/b/a/f;

    invoke-virtual {v0, p1}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/b/a/u;->g:Lcom/google/b/a/f;

    invoke-virtual {v0, p1}, Lcom/google/b/a/f;->b(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
