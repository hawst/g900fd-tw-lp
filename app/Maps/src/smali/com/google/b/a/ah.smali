.class public final Lcom/google/b/a/ah;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/b/a/ai;

.field private d:Lcom/google/b/a/ai;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    new-instance v0, Lcom/google/b/a/ai;

    invoke-direct {v0}, Lcom/google/b/a/ai;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/ah;->c:Lcom/google/b/a/ai;

    .line 133
    iget-object v0, p0, Lcom/google/b/a/ah;->c:Lcom/google/b/a/ai;

    iput-object v0, p0, Lcom/google/b/a/ah;->d:Lcom/google/b/a/ai;

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/b/a/ah;->a:Z

    .line 140
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/b/a/ah;->b:Ljava/lang/String;

    .line 141
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 351
    new-instance v0, Lcom/google/b/a/ai;

    invoke-direct {v0}, Lcom/google/b/a/ai;-><init>()V

    iget-object v1, p0, Lcom/google/b/a/ah;->d:Lcom/google/b/a/ai;

    iput-object v0, v1, Lcom/google/b/a/ai;->c:Lcom/google/b/a/ai;

    iput-object v0, p0, Lcom/google/b/a/ah;->d:Lcom/google/b/a/ai;

    .line 352
    iput-object p2, v0, Lcom/google/b/a/ai;->b:Ljava/lang/Object;

    .line 353
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, v0, Lcom/google/b/a/ai;->a:Ljava/lang/String;

    .line 354
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 319
    iget-boolean v2, p0, Lcom/google/b/a/ah;->a:Z

    .line 320
    const-string v1, ""

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lcom/google/b/a/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    .line 322
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 323
    iget-object v0, p0, Lcom/google/b/a/ah;->c:Lcom/google/b/a/ai;

    iget-object v0, v0, Lcom/google/b/a/ai;->c:Lcom/google/b/a/ai;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 325
    if-eqz v2, :cond_0

    iget-object v4, v1, Lcom/google/b/a/ai;->b:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 326
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string v0, ", "

    .line 329
    iget-object v4, v1, Lcom/google/b/a/ai;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 330
    iget-object v4, v1, Lcom/google/b/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 332
    :cond_1
    iget-object v4, v1, Lcom/google/b/a/ai;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 324
    :cond_2
    iget-object v1, v1, Lcom/google/b/a/ai;->c:Lcom/google/b/a/ai;

    goto :goto_0

    .line 335
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
