.class Lcom/google/b/a/ad;
.super Lcom/google/b/a/ab;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/b/a/ab;


# direct methods
.method constructor <init>(Lcom/google/b/a/ab;Lcom/google/b/a/ab;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/google/b/a/ad;->b:Lcom/google/b/a/ab;

    invoke-direct {p0, p2}, Lcom/google/b/a/ab;-><init>(Lcom/google/b/a/ab;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/b/a/ab;
    .locals 2

    .prologue
    .line 311
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 312
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "already specified skipNulls"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/Appendable;",
            ">(TA;",
            "Ljava/util/Iterator",
            "<*>;)TA;"
        }
    .end annotation

    .prologue
    .line 290
    const-string v0, "appendable"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 291
    :cond_0
    const-string v0, "parts"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 292
    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_1

    .line 295
    iget-object v1, p0, Lcom/google/b/a/ad;->b:Lcom/google/b/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 299
    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_2

    .line 302
    iget-object v1, p0, Lcom/google/b/a/ad;->b:Lcom/google/b/a/ab;

    iget-object v1, v1, Lcom/google/b/a/ab;->a:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 303
    iget-object v1, p0, Lcom/google/b/a/ad;->b:Lcom/google/b/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    .line 306
    :cond_3
    return-object p1
.end method

.method public final b(Ljava/lang/String;)Lcom/google/b/a/af;
    .locals 2

    .prologue
    .line 317
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 318
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t use .skipNulls() with maps"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
