.class abstract enum Lcom/google/b/a/aw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/b/a/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/a/aw;",
        ">;",
        "Lcom/google/b/a/ar",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/a/aw;

.field public static final enum b:Lcom/google/b/a/aw;

.field public static final enum c:Lcom/google/b/a/aw;

.field public static final enum d:Lcom/google/b/a/aw;

.field private static final synthetic e:[Lcom/google/b/a/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 268
    new-instance v0, Lcom/google/b/a/ax;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1, v2}, Lcom/google/b/a/ax;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/a/aw;->a:Lcom/google/b/a/aw;

    .line 273
    new-instance v0, Lcom/google/b/a/ay;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1, v3}, Lcom/google/b/a/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/a/aw;->b:Lcom/google/b/a/aw;

    .line 278
    new-instance v0, Lcom/google/b/a/az;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1, v4}, Lcom/google/b/a/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/a/aw;->c:Lcom/google/b/a/aw;

    .line 283
    new-instance v0, Lcom/google/b/a/ba;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1, v5}, Lcom/google/b/a/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/a/aw;->d:Lcom/google/b/a/aw;

    .line 267
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/b/a/aw;

    sget-object v1, Lcom/google/b/a/aw;->a:Lcom/google/b/a/aw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/a/aw;->b:Lcom/google/b/a/aw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/a/aw;->c:Lcom/google/b/a/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/a/aw;->d:Lcom/google/b/a/aw;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/b/a/aw;->e:[Lcom/google/b/a/aw;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/a/aw;
    .locals 1

    .prologue
    .line 267
    const-class v0, Lcom/google/b/a/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/aw;

    return-object v0
.end method

.method public static values()[Lcom/google/b/a/aw;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/google/b/a/aw;->e:[Lcom/google/b/a/aw;

    invoke-virtual {v0}, [Lcom/google/b/a/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/a/aw;

    return-object v0
.end method
