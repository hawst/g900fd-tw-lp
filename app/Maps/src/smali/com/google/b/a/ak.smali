.class public final Lcom/google/b/a/ak;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/b/a/al;

.field public b:Z

.field private final c:Ljava/lang/String;

.field private d:Lcom/google/b/a/al;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Lcom/google/b/a/al;

    invoke-direct {v0}, Lcom/google/b/a/al;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/ak;->d:Lcom/google/b/a/al;

    .line 198
    iget-object v0, p0, Lcom/google/b/a/ak;->d:Lcom/google/b/a/al;

    iput-object v0, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/b/a/ak;->b:Z

    .line 205
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/b/a/ak;->c:Ljava/lang/String;

    .line 206
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/b/a/ak;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 297
    new-instance v0, Lcom/google/b/a/al;

    invoke-direct {v0}, Lcom/google/b/a/al;-><init>()V

    iget-object v1, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v1, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v0, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object p1, v0, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/google/b/a/ak;
    .locals 3

    .prologue
    .line 277
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/b/a/al;

    invoke-direct {v1}, Lcom/google/b/a/al;-><init>()V

    iget-object v2, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v1, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v1, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, v1, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/b/a/ak;
    .locals 4

    .prologue
    .line 287
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/b/a/al;

    invoke-direct {v1}, Lcom/google/b/a/al;-><init>()V

    iget-object v2, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v1, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v1, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, v1, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 227
    new-instance v0, Lcom/google/b/a/al;

    invoke-direct {v0}, Lcom/google/b/a/al;-><init>()V

    iget-object v1, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v1, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v0, p0, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object p2, v0, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, v0, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 385
    iget-boolean v2, p0, Lcom/google/b/a/ak;->b:Z

    .line 386
    const-string v1, ""

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lcom/google/b/a/ak;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 388
    iget-object v0, p0, Lcom/google/b/a/ak;->d:Lcom/google/b/a/al;

    iget-object v0, v0, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 389
    if-eqz v2, :cond_0

    iget-object v4, v1, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 390
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    const-string v0, ", "

    .line 393
    iget-object v4, v1, Lcom/google/b/a/al;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 394
    iget-object v4, v1, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 396
    :cond_1
    iget-object v4, v1, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 388
    :cond_2
    iget-object v1, v1, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    goto :goto_0

    .line 399
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
