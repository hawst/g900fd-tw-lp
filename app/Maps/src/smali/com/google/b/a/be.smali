.class public final Lcom/google/b/a/be;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/b/a/f;

.field final b:Z

.field public final c:Lcom/google/b/a/bl;

.field public final d:I


# direct methods
.method public constructor <init>(Lcom/google/b/a/bl;)V
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x0

    sget-object v1, Lcom/google/b/a/f;->c:Lcom/google/b/a/f;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;ZLcom/google/b/a/f;I)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/google/b/a/bl;ZLcom/google/b/a/f;I)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/google/b/a/be;->c:Lcom/google/b/a/bl;

    .line 113
    iput-boolean p2, p0, Lcom/google/b/a/be;->b:Z

    .line 114
    iput-object p3, p0, Lcom/google/b/a/be;->a:Lcom/google/b/a/f;

    .line 115
    iput p4, p0, Lcom/google/b/a/be;->d:I

    .line 116
    return-void
.end method

.method public static a(C)Lcom/google/b/a/be;
    .locals 3

    .prologue
    .line 127
    invoke-static {p0}, Lcom/google/b/a/f;->a(C)Lcom/google/b/a/f;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    new-instance v1, Lcom/google/b/a/be;

    new-instance v2, Lcom/google/b/a/bf;

    invoke-direct {v2, v0}, Lcom/google/b/a/bf;-><init>(Lcom/google/b/a/f;)V

    invoke-direct {v1, v2}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;)V

    return-object v1
.end method
