.class Lcom/google/b/c/db;
.super Lcom/google/b/c/cv;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/cv",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final transient a:I

.field final transient c:I

.field final synthetic d:Lcom/google/b/c/cv;


# direct methods
.method constructor <init>(Lcom/google/b/c/cv;II)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/b/c/db;->d:Lcom/google/b/c/cv;

    invoke-direct {p0}, Lcom/google/b/c/cv;-><init>()V

    .line 390
    iput p2, p0, Lcom/google/b/c/db;->a:I

    .line 391
    iput p3, p0, Lcom/google/b/c/db;->c:I

    .line 392
    return-void
.end method


# virtual methods
.method public final a(II)Lcom/google/b/c/cv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 405
    iget v0, p0, Lcom/google/b/c/db;->c:I

    invoke-static {p1, p2, v0}, Lcom/google/b/a/aq;->a(III)V

    .line 406
    iget-object v0, p0, Lcom/google/b/c/db;->d:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/db;->a:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/b/c/db;->a:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cv;->a(II)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 399
    iget v0, p0, Lcom/google/b/c/db;->c:I

    invoke-static {p1, v0}, Lcom/google/b/a/aq;->a(II)I

    .line 400
    iget-object v0, p0, Lcom/google/b/c/db;->d:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/db;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, Lcom/google/b/c/cv;->d()Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0, p1}, Lcom/google/b/c/cv;->a(I)Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/google/b/c/db;->c:I

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 3

    .prologue
    .line 385
    iget v0, p0, Lcom/google/b/c/db;->c:I

    invoke-static {p1, p2, v0}, Lcom/google/b/a/aq;->a(III)V

    iget-object v0, p0, Lcom/google/b/c/db;->d:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/db;->a:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/b/c/db;->a:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cv;->a(II)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method
