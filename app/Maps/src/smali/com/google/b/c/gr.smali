.class public abstract enum Lcom/google/b/c/gr;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/gr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/gr;

.field public static final enum b:Lcom/google/b/c/gr;

.field public static final enum c:Lcom/google/b/c/gr;

.field private static final synthetic d:[Lcom/google/b/c/gr;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 292
    new-instance v0, Lcom/google/b/c/gs;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/gs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    .line 306
    new-instance v0, Lcom/google/b/c/gt;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lcom/google/b/c/gt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/gr;->b:Lcom/google/b/c/gr;

    .line 320
    new-instance v0, Lcom/google/b/c/gu;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lcom/google/b/c/gu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    .line 286
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/c/gr;

    sget-object v1, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/c/gr;->b:Lcom/google/b/c/gr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/c/gr;->d:[Lcom/google/b/c/gr;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/gr;
    .locals 1

    .prologue
    .line 286
    const-class v0, Lcom/google/b/c/gr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gr;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/gr;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Lcom/google/b/c/gr;->d:[Lcom/google/b/c/gr;

    invoke-virtual {v0}, [Lcom/google/b/c/gr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/gr;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(Lcom/google/b/c/go;Lcom/google/b/c/gn;Ljava/lang/Object;)Lcom/google/b/c/hb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/go",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;TV;)",
            "Lcom/google/b/c/hb",
            "<TK;TV;>;"
        }
    .end annotation
.end method
