.class final Lcom/google/b/c/co;
.super Lcom/google/b/c/dc;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Enum",
        "<TK;>;V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dc",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final transient a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/EnumMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/b/c/dc;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    .line 55
    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :cond_1
    return-void
.end method

.method static a(Ljava/util/EnumMap;)Lcom/google/b/c/dc;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Enum",
            "<TK;>;V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/EnumMap",
            "<TK;TV;>;)",
            "Lcom/google/b/c/dc",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    invoke-virtual {p0}, Ljava/util/EnumMap;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 47
    new-instance v0, Lcom/google/b/c/co;

    invoke-direct {v0, p0}, Lcom/google/b/c/co;-><init>(Ljava/util/EnumMap;)V

    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    sget-object v0, Lcom/google/b/c/at;->a:Lcom/google/b/c/at;

    goto :goto_0

    .line 43
    :pswitch_1
    invoke-virtual {p0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/eg;->d(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 44
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    const-string v0, "null key in entry: null=%s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v0, "null value in entry: %s=null"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Lcom/google/b/c/jv;

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/jv;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method final a()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/google/b/c/cp;

    invoke-direct {v0, p0}, Lcom/google/b/c/cp;-><init>(Lcom/google/b/c/co;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lcom/google/b/c/cq;

    invoke-direct {v0, p0}, Lcom/google/b/c/cq;-><init>(Lcom/google/b/c/co;)V

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    return v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/google/b/c/cs;

    iget-object v1, p0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-direct {v0, v1}, Lcom/google/b/c/cs;-><init>(Ljava/util/EnumMap;)V

    return-object v0
.end method
