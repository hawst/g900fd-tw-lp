.class Lcom/google/b/c/s;
.super Lcom/google/b/c/q;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/f",
        "<TK;TV;>.com/google/b/c/p.com/google/b/c/q;",
        "Ljava/util/ListIterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic d:Lcom/google/b/c/r;


# direct methods
.method constructor <init>(Lcom/google/b/c/r;)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/google/b/c/s;->d:Lcom/google/b/c/r;

    invoke-direct {p0, p1}, Lcom/google/b/c/q;-><init>(Lcom/google/b/c/p;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/b/c/r;I)V
    .locals 1

    .prologue
    .line 753
    iput-object p1, p0, Lcom/google/b/c/s;->d:Lcom/google/b/c/r;

    .line 754
    iget-object v0, p1, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/b/c/q;-><init>(Lcom/google/b/c/p;Ljava/util/Iterator;)V

    .line 755
    return-void
.end method

.method private a()Ljava/util/ListIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 758
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-virtual {v0}, Lcom/google/b/c/p;->a()V

    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/b/c/q;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/b/c/s;->d:Lcom/google/b/c/r;

    invoke-virtual {v0}, Lcom/google/b/c/r;->isEmpty()Z

    move-result v0

    .line 783
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 784
    iget-object v1, p0, Lcom/google/b/c/s;->d:Lcom/google/b/c/r;

    iget-object v1, v1, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    iget v2, v1, Lcom/google/b/c/f;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/c/f;->b:I

    .line 785
    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/google/b/c/s;->d:Lcom/google/b/c/r;

    invoke-virtual {v0}, Lcom/google/b/c/r;->c()V

    .line 788
    :cond_0
    return-void
.end method

.method public hasPrevious()Z
    .locals 1

    .prologue
    .line 762
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 770
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 766
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 778
    invoke-direct {p0}, Lcom/google/b/c/s;->a()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 779
    return-void
.end method
