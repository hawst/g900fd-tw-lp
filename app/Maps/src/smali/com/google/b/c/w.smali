.class Lcom/google/b/c/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/b/c/ia",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TE;",
            "Lcom/google/b/c/as;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/google/b/c/v;


# direct methods
.method constructor <init>(Lcom/google/b/c/v;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/b/c/w;->c:Lcom/google/b/c/v;

    iput-object p2, p0, Lcom/google/b/c/w;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/b/c/w;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/b/c/w;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/google/b/c/w;->a:Ljava/util/Map$Entry;

    new-instance v1, Lcom/google/b/c/x;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/x;-><init>(Lcom/google/b/c/w;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public remove()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/b/c/w;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/eg;->a(Z)V

    .line 119
    iget-object v2, p0, Lcom/google/b/c/w;->c:Lcom/google/b/c/v;

    iget-object v0, p0, Lcom/google/b/c/w;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    iget v3, v0, Lcom/google/b/c/as;->a:I

    iput v1, v0, Lcom/google/b/c/as;->a:I

    int-to-long v0, v3

    iget-wide v4, v2, Lcom/google/b/c/v;->b:J

    sub-long v0, v4, v0

    iput-wide v0, v2, Lcom/google/b/c/v;->b:J

    .line 120
    iget-object v0, p0, Lcom/google/b/c/w;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/c/w;->a:Ljava/util/Map$Entry;

    .line 122
    return-void

    :cond_0
    move v0, v1

    .line 118
    goto :goto_0
.end method
