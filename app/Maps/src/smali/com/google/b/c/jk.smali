.class final Lcom/google/b/c/jk;
.super Lcom/google/b/c/dx;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dx",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient e:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/cv;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<TE;>;",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p2}, Lcom/google/b/c/dx;-><init>(Ljava/util/Comparator;)V

    .line 54
    iput-object p1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    .line 55
    invoke-virtual {p1}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :cond_1
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 267
    if-nez p1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 272
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    iget-object v2, p0, Lcom/google/b/c/jk;->d:Ljava/util/Comparator;

    sget-object v3, Lcom/google/b/c/kf;->a:Lcom/google/b/c/kf;

    sget-object v4, Lcom/google/b/c/kb;->c:Lcom/google/b/c/kb;

    invoke-static {v1, p1, v2, v3, v4}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 277
    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 275
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method final a(II)Lcom/google/b/c/dx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 255
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/jk;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 261
    :goto_0
    return-object p0

    .line 257
    :cond_0
    if-ge p1, p2, :cond_1

    .line 258
    new-instance v0, Lcom/google/b/c/jk;

    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v1, p1, p2}, Lcom/google/b/c/cv;->a(II)Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/c/jk;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/jk;-><init>(Lcom/google/b/c/cv;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/jk;->d:Ljava/util/Comparator;

    sget-object v1, Lcom/google/b/c/dx;->a:Ljava/util/Comparator;

    invoke-interface {v1, v0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p0, Lcom/google/b/c/dx;->c:Lcom/google/b/c/dx;

    goto :goto_0

    :cond_2
    new-instance p0, Lcom/google/b/c/ax;

    invoke-direct {p0, v0}, Lcom/google/b/c/ax;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/Object;Z)Lcom/google/b/c/dx;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 222
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    :goto_0
    sget-object v4, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    invoke-static {v2, p1, v3, v0, v4}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/b/c/jk;->a(II)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    goto :goto_0
.end method

.method final a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/b/c/dx;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v2

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    :goto_0
    sget-object v3, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    invoke-static {v1, p1, v2, v0, v3}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/b/c/jk;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/b/c/jk;->a(II)Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/b/c/dx;->a(Ljava/lang/Object;Z)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    goto :goto_0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->a()Z

    move-result v0

    return v0
.end method

.method final b(Ljava/lang/Object;Z)Lcom/google/b/c/dx;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 238
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v2

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    :goto_0
    sget-object v3, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    invoke-static {v1, p1, v2, v0, v3}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/b/c/jk;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/b/c/jk;->a(II)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    goto :goto_0
.end method

.method public final b()Lcom/google/b/c/lg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/Object;Z)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v2

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    :goto_0
    sget-object v3, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    invoke-static {v1, p1, v2, v0, v3}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I

    move-result v0

    return v0

    :cond_1
    sget-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    goto :goto_0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 80
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    iget-object v2, p0, Lcom/google/b/c/jk;->d:Ljava/util/Comparator;

    invoke-static {v1, p1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 82
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/b/c/jz;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-gt v2, v0, :cond_2

    .line 93
    :cond_0
    invoke-super {p0, p1}, Lcom/google/b/c/dx;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 129
    :cond_1
    :goto_0
    return v0

    .line 100
    :cond_2
    iget-object v2, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v2}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v3

    .line 101
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 102
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 106
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 108
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    invoke-interface {v6, v5, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 110
    if-nez v5, :cond_4

    .line 112
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_1

    .line 119
    :cond_4
    if-lez v5, :cond_3

    move v0, v1

    .line 120
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 129
    goto :goto_0
.end method

.method final d(Ljava/lang/Object;Z)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/jk;->comparator()Ljava/util/Comparator;

    move-result-object v2

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    :goto_0
    sget-object v3, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    invoke-static {v1, p1, v2, v0, v3}, Lcom/google/b/c/ka;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I

    move-result v0

    return v0

    :cond_1
    sget-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    goto :goto_0
.end method

.method public final e()Lcom/google/b/c/lg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->ar_()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 153
    if-ne p1, p0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_2

    move v0, v1

    .line 157
    goto :goto_0

    .line 160
    :cond_2
    check-cast p1, Ljava/util/Set;

    .line 161
    invoke-virtual {p0}, Lcom/google/b/c/jk;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 162
    goto :goto_0

    .line 165
    :cond_3
    iget-object v2, p0, Lcom/google/b/c/jk;->d:Ljava/util/Comparator;

    invoke-static {v2, p1}, Lcom/google/b/c/jz;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 166
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 168
    :try_start_0
    iget-object v3, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v3

    .line 169
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 171
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 172
    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    invoke-interface {v6, v4, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_4

    :cond_5
    move v0, v1

    .line 173
    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 180
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 183
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/b/c/jk;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method final f()Lcom/google/b/c/cv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 282
    new-instance v0, Lcom/google/b/c/dr;

    iget-object v1, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/dr;-><init>(Lcom/google/b/c/dx;Lcom/google/b/c/cv;)V

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {p0}, Lcom/google/b/c/jk;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/b/c/jk;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
