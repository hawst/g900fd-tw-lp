.class Lcom/google/b/c/u;
.super Lcom/google/b/c/p;
.source "PG"

# interfaces
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/f",
        "<TK;TV;>.com/google/b/c/p;",
        "Ljava/util/SortedSet",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic f:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/b/c/p;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/b/c/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/SortedSet",
            "<TV;>;",
            "Lcom/google/b/c/f",
            "<TK;TV;>.com/google/b/c/p;)V"
        }
    .end annotation

    .prologue
    .line 628
    iput-object p1, p0, Lcom/google/b/c/u;->f:Lcom/google/b/c/f;

    .line 629
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/b/c/p;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/b/c/p;)V

    .line 630
    return-void
.end method


# virtual methods
.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/google/b/c/u;->a()V

    .line 642
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/google/b/c/u;->a()V

    .line 652
    new-instance v1, Lcom/google/b/c/u;

    iget-object v2, p0, Lcom/google/b/c/u;->f:Lcom/google/b/c/f;

    iget-object v3, p0, Lcom/google/b/c/p;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/b/c/u;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/b/c/p;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    goto :goto_0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/google/b/c/u;->a()V

    .line 647
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/google/b/c/u;->a()V

    .line 658
    new-instance v1, Lcom/google/b/c/u;

    iget-object v2, p0, Lcom/google/b/c/u;->f:Lcom/google/b/c/f;

    iget-object v3, p0, Lcom/google/b/c/p;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/b/c/u;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/b/c/p;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    goto :goto_0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/b/c/u;->a()V

    .line 664
    new-instance v1, Lcom/google/b/c/u;

    iget-object v2, p0, Lcom/google/b/c/u;->f:Lcom/google/b/c/f;

    iget-object v3, p0, Lcom/google/b/c/p;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v4, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v1, v2, v3, v0, p0}, Lcom/google/b/c/u;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/b/c/p;)V

    return-object v1

    :cond_0
    iget-object p0, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    goto :goto_0
.end method
