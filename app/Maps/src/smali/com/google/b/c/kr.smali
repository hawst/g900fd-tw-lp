.class Lcom/google/b/c/kr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TC;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Lcom/google/b/c/kq;


# direct methods
.method constructor <init>(Lcom/google/b/c/kq;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/b/c/kr;->b:Lcom/google/b/c/kq;

    iput-object p2, p0, Lcom/google/b/c/kr;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/b/c/kr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/b/c/kr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Lcom/google/b/c/ks;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/ks;-><init>(Lcom/google/b/c/kr;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/b/c/kr;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 485
    iget-object v0, p0, Lcom/google/b/c/kr;->b:Lcom/google/b/c/kq;

    iget-object v0, v0, Lcom/google/b/c/kq;->a:Lcom/google/b/c/ko;

    invoke-virtual {v0}, Lcom/google/b/c/ko;->b()V

    .line 486
    return-void
.end method
