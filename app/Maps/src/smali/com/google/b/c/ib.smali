.class public final Lcom/google/b/c/ib;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1082
    new-instance v0, Lcom/google/b/c/ic;

    invoke-direct {v0}, Lcom/google/b/c/ic;-><init>()V

    return-void
.end method

.method static a(Lcom/google/b/c/hz;Ljava/lang/Object;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/hz",
            "<TE;>;TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 886
    const-string v3, "count"

    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "%s cannot be negative: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4, v5}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 888
    :cond_1
    invoke-interface {p0, p1}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;)I

    move-result v0

    .line 890
    sub-int v1, p2, v0

    .line 891
    if-lez v1, :cond_3

    .line 892
    invoke-interface {p0, p1, v1}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;I)I

    .line 897
    :cond_2
    :goto_1
    return v0

    .line 893
    :cond_3
    if-gez v1, :cond_2

    .line 894
    neg-int v1, v1

    invoke-interface {p0, p1, v1}, Lcom/google/b/c/hz;->b(Ljava/lang/Object;I)I

    goto :goto_1
.end method

.method static a(Lcom/google/b/c/hz;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/hz",
            "<TE;>;)",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1013
    new-instance v0, Lcom/google/b/c/ih;

    invoke-interface {p0}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/ih;-><init>(Lcom/google/b/c/hz;Ljava/util/Iterator;)V

    return-object v0
.end method

.method static a(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1072
    if-ltz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "%s cannot be negative: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 1073
    :cond_1
    return-void
.end method

.method static a(Lcom/google/b/c/hz;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hz",
            "<*>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 819
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 840
    :goto_0
    return v0

    .line 822
    :cond_0
    instance-of v0, p1, Lcom/google/b/c/hz;

    if-eqz v0, :cond_5

    .line 823
    check-cast p1, Lcom/google/b/c/hz;

    .line 830
    invoke-interface {p0}, Lcom/google/b/c/hz;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/b/c/hz;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-interface {p0}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v0, v2

    .line 831
    goto :goto_0

    .line 833
    :cond_2
    invoke-interface {p1}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/ia;

    .line 834
    invoke-interface {v0}, Lcom/google/b/c/ia;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lcom/google/b/c/ia;->b()I

    move-result v0

    if-eq v4, v0, :cond_3

    move v0, v2

    .line 835
    goto :goto_0

    :cond_4
    move v0, v1

    .line 838
    goto :goto_0

    :cond_5
    move v0, v2

    .line 840
    goto :goto_0
.end method

.method static a(Lcom/google/b/c/hz;Ljava/lang/Object;II)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/hz",
            "<TE;>;TE;II)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 904
    const-string v3, "oldCount"

    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "%s cannot be negative: %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4, v5}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 905
    :cond_1
    const-string v3, "newCount"

    if-ltz p3, :cond_2

    move v0, v1

    :goto_1
    const-string v4, "%s cannot be negative: %s"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4, v5}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 907
    :cond_3
    invoke-interface {p0, p1}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p2, :cond_4

    .line 908
    invoke-interface {p0, p1, p3}, Lcom/google/b/c/hz;->c(Ljava/lang/Object;I)I

    .line 911
    :goto_2
    return v1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method static a(Lcom/google/b/c/hz;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/hz",
            "<TE;>;",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 847
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 848
    const/4 v0, 0x0

    .line 858
    :goto_0
    return v0

    .line 850
    :cond_0
    instance-of v0, p1, Lcom/google/b/c/hz;

    if-eqz v0, :cond_1

    .line 851
    check-cast p1, Lcom/google/b/c/hz;

    .line 852
    invoke-interface {p1}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/ia;

    .line 853
    invoke-interface {v0}, Lcom/google/b/c/ia;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/b/c/ia;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;I)I

    goto :goto_1

    .line 856
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/b/c/eg;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 858
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Lcom/google/b/c/hz;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hz",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 1064
    const-wide/16 v0, 0x0

    .line 1065
    invoke-interface {p0}, Lcom/google/b/c/hz;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/ia;

    .line 1066
    invoke-interface {v0}, Lcom/google/b/c/ia;->b()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 1067
    goto :goto_0

    .line 1068
    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    const v0, 0x7fffffff

    :goto_1
    return v0

    :cond_1
    const-wide/32 v0, -0x80000000

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_2
    long-to-int v0, v2

    goto :goto_1
.end method

.method static b(Lcom/google/b/c/hz;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hz",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 865
    instance-of v0, p1, Lcom/google/b/c/hz;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/b/c/hz;

    invoke-interface {p1}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object p1

    .line 868
    :cond_0
    invoke-interface {p0}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lcom/google/b/c/hz;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hz",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 875
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 876
    :cond_0
    instance-of v0, p1, Lcom/google/b/c/hz;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/b/c/hz;

    invoke-interface {p1}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object p1

    .line 879
    :cond_1
    invoke-interface {p0}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
