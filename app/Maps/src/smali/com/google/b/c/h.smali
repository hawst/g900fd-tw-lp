.class Lcom/google/b/c/h;
.super Lcom/google/b/c/ho;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/ho",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/g;


# direct methods
.method constructor <init>(Lcom/google/b/c/g;)V
    .locals 0

    .prologue
    .line 1127
    iput-object p1, p0, Lcom/google/b/c/h;->a:Lcom/google/b/c/g;

    invoke-direct {p0}, Lcom/google/b/c/ho;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/google/b/c/h;->a:Lcom/google/b/c/g;

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/google/b/c/h;->a:Lcom/google/b/c/g;

    iget-object v0, v0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/c/an;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 1136
    new-instance v0, Lcom/google/b/c/i;

    iget-object v1, p0, Lcom/google/b/c/h;->a:Lcom/google/b/c/g;

    invoke-direct {v0, v1}, Lcom/google/b/c/i;-><init>(Lcom/google/b/c/g;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1148
    invoke-virtual {p0, p1}, Lcom/google/b/c/h;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1149
    const/4 v0, 0x0

    .line 1153
    :goto_0
    return v0

    .line 1151
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 1152
    iget-object v0, p0, Lcom/google/b/c/h;->a:Lcom/google/b/c/g;

    iget-object v1, v0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, v1, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-static {v2, v0}, Lcom/google/b/c/hj;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    iget v0, v1, Lcom/google/b/c/f;->b:I

    sub-int/2addr v0, v2

    iput v0, v1, Lcom/google/b/c/f;->b:I

    .line 1153
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
