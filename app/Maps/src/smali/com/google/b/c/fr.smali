.class abstract Lcom/google/b/c/fr;
.super Lcom/google/b/c/bb;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/bb",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x3L


# instance fields
.field final a:Lcom/google/b/c/gr;

.field final b:Lcom/google/b/c/gr;

.field final c:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final d:J

.field final e:J

.field final f:I

.field final g:I

.field final h:Lcom/google/b/c/fl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/fl",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field transient i:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/gr;Lcom/google/b/c/gr;Lcom/google/b/a/x;Lcom/google/b/a/x;JJIILcom/google/b/c/fl;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gr;",
            "Lcom/google/b/c/gr;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;JJII",
            "Lcom/google/b/c/fl",
            "<-TK;-TV;>;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3807
    invoke-direct {p0}, Lcom/google/b/c/bb;-><init>()V

    .line 3808
    iput-object p1, p0, Lcom/google/b/c/fr;->a:Lcom/google/b/c/gr;

    .line 3809
    iput-object p2, p0, Lcom/google/b/c/fr;->b:Lcom/google/b/c/gr;

    .line 3810
    iput-object p3, p0, Lcom/google/b/c/fr;->c:Lcom/google/b/a/x;

    .line 3811
    iput-wide p5, p0, Lcom/google/b/c/fr;->d:J

    .line 3813
    iput-wide p7, p0, Lcom/google/b/c/fr;->e:J

    .line 3814
    iput p9, p0, Lcom/google/b/c/fr;->f:I

    .line 3815
    iput p10, p0, Lcom/google/b/c/fr;->g:I

    .line 3816
    iput-object p11, p0, Lcom/google/b/c/fr;->h:Lcom/google/b/c/fl;

    .line 3817
    iput-object p12, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    .line 3818
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3822
    iget-object v0, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3787
    iget-object v0, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method protected final bridge synthetic c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 3787
    iget-object v0, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method
