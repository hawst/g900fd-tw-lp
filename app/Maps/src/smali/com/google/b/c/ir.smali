.class Lcom/google/b/c/ir;
.super Lcom/google/b/c/cd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/cd",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/b/c/ci;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ci",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<+TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/ci;Lcom/google/b/c/cv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/ci",
            "<TE;>;",
            "Lcom/google/b/c/cv",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/b/c/cd;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/b/c/ir;->a:Lcom/google/b/c/ci;

    .line 36
    iput-object p2, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/google/b/c/ci;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/ci",
            "<TE;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p2}, Lcom/google/b/c/cv;->b([Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/b/c/ir;-><init>(Lcom/google/b/c/ci;Lcom/google/b/c/cv;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/b/c/lh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/b/c/lh",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->a(I)Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method e()Lcom/google/b/c/ci;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ci",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/b/c/ir;->a:Lcom/google/b/c/ci;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->a(I)Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/b/c/ir;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
