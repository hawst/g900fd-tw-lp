.class abstract enum Lcom/google/b/c/fs;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/fs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/fs;

.field public static final enum b:Lcom/google/b/c/fs;

.field public static final enum c:Lcom/google/b/c/fs;

.field public static final enum d:Lcom/google/b/c/fs;

.field public static final enum e:Lcom/google/b/c/fs;

.field public static final enum f:Lcom/google/b/c/fs;

.field public static final enum g:Lcom/google/b/c/fs;

.field public static final enum h:Lcom/google/b/c/fs;

.field static final i:[[Lcom/google/b/c/fs;

.field private static final synthetic j:[Lcom/google/b/c/fs;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 352
    new-instance v0, Lcom/google/b/c/ft;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lcom/google/b/c/ft;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->a:Lcom/google/b/c/fs;

    .line 360
    new-instance v0, Lcom/google/b/c/fu;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lcom/google/b/c/fu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->b:Lcom/google/b/c/fs;

    .line 376
    new-instance v0, Lcom/google/b/c/fv;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lcom/google/b/c/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->c:Lcom/google/b/c/fs;

    .line 392
    new-instance v0, Lcom/google/b/c/fw;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Lcom/google/b/c/fw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->d:Lcom/google/b/c/fs;

    .line 410
    new-instance v0, Lcom/google/b/c/fx;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lcom/google/b/c/fx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->e:Lcom/google/b/c/fs;

    .line 418
    new-instance v0, Lcom/google/b/c/fy;

    const-string v1, "WEAK_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/fy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->f:Lcom/google/b/c/fs;

    .line 434
    new-instance v0, Lcom/google/b/c/fz;

    const-string v1, "WEAK_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->g:Lcom/google/b/c/fs;

    .line 450
    new-instance v0, Lcom/google/b/c/ga;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/fs;->h:Lcom/google/b/c/fs;

    .line 351
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/b/c/fs;

    sget-object v1, Lcom/google/b/c/fs;->a:Lcom/google/b/c/fs;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/c/fs;->b:Lcom/google/b/c/fs;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/c/fs;->c:Lcom/google/b/c/fs;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/c/fs;->d:Lcom/google/b/c/fs;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/c/fs;->e:Lcom/google/b/c/fs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/b/c/fs;->f:Lcom/google/b/c/fs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/c/fs;->g:Lcom/google/b/c/fs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/c/fs;->h:Lcom/google/b/c/fs;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/c/fs;->j:[Lcom/google/b/c/fs;

    .line 478
    new-array v0, v6, [[Lcom/google/b/c/fs;

    new-array v1, v7, [Lcom/google/b/c/fs;

    sget-object v2, Lcom/google/b/c/fs;->a:Lcom/google/b/c/fs;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/b/c/fs;->b:Lcom/google/b/c/fs;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/b/c/fs;->c:Lcom/google/b/c/fs;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/b/c/fs;->d:Lcom/google/b/c/fs;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v3, [Lcom/google/b/c/fs;

    aput-object v1, v0, v4

    new-array v1, v7, [Lcom/google/b/c/fs;

    sget-object v2, Lcom/google/b/c/fs;->e:Lcom/google/b/c/fs;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/b/c/fs;->f:Lcom/google/b/c/fs;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/b/c/fs;->g:Lcom/google/b/c/fs;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/b/c/fs;->h:Lcom/google/b/c/fs;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/b/c/fs;->i:[[Lcom/google/b/c/fs;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/google/b/c/gr;ZZ)Lcom/google/b/c/fs;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 486
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 487
    sget-object v1, Lcom/google/b/c/fs;->i:[[Lcom/google/b/c/fs;

    invoke-virtual {p0}, Lcom/google/b/c/gr;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    .line 486
    goto :goto_0
.end method

.method static a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 517
    invoke-interface {p0}, Lcom/google/b/c/gn;->e()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/b/c/gn;->a(J)V

    .line 519
    invoke-interface {p0}, Lcom/google/b/c/gn;->g()Lcom/google/b/c/gn;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/c/fn;->a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    .line 520
    invoke-interface {p0}, Lcom/google/b/c/gn;->f()Lcom/google/b/c/gn;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/b/c/fn;->a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    .line 522
    invoke-static {p0}, Lcom/google/b/c/fn;->a(Lcom/google/b/c/gn;)V

    .line 523
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/fs;
    .locals 1

    .prologue
    .line 351
    const-class v0, Lcom/google/b/c/fs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/fs;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/fs;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lcom/google/b/c/fs;->j:[Lcom/google/b/c/fs;

    invoke-virtual {v0}, [Lcom/google/b/c/fs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/fs;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/b/c/go;Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/go",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-interface {p2}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/b/c/gn;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/google/b/c/fs;->a(Lcom/google/b/c/go;Ljava/lang/Object;ILcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/google/b/c/go;Ljava/lang/Object;ILcom/google/b/c/gn;)Lcom/google/b/c/gn;
    .param p4    # Lcom/google/b/c/gn;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/go",
            "<TK;TV;>;TK;I",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end method
