.class final Lcom/google/b/c/jl;
.super Lcom/google/b/c/io;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/io",
        "<",
        "Ljava/lang/Comparable;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/b/c/jl;

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/b/c/jl;

    invoke-direct {v0}, Lcom/google/b/c/jl;-><init>()V

    sput-object v0, Lcom/google/b/c/jl;->a:Lcom/google/b/c/jl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/b/c/io;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/google/b/c/jl;->a:Lcom/google/b/c/jl;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/b/c/io;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Ljava/lang/Comparable;",
            ">()",
            "Lcom/google/b/c/io",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1}, Lcom/google/b/c/ik;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1, p2}, Lcom/google/b/c/ik;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic a(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1}, Lcom/google/b/c/ik;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1}, Lcom/google/b/c/ik;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1, p2}, Lcom/google/b/c/ik;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic b(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/b/c/ik;->a:Lcom/google/b/c/ik;

    invoke-virtual {v0, p1}, Lcom/google/b/c/ik;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-interface {p2, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string v0, "Ordering.natural().reverse()"

    return-object v0
.end method
