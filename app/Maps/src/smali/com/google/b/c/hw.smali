.class abstract Lcom/google/b/c/hw;
.super Ljava/util/AbstractCollection;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1977
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/google/b/c/hu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/hu",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 2005
    invoke-virtual {p0}, Lcom/google/b/c/hw;->a()Lcom/google/b/c/hu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/c/hu;->f()V

    .line 2006
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1987
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 1988
    check-cast p1, Ljava/util/Map$Entry;

    .line 1989
    invoke-virtual {p0}, Lcom/google/b/c/hw;->a()Lcom/google/b/c/hu;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/b/c/hu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1991
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1996
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 1997
    check-cast p1, Ljava/util/Map$Entry;

    .line 1998
    invoke-virtual {p0}, Lcom/google/b/c/hw;->a()Lcom/google/b/c/hu;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/b/c/hu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 2000
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1982
    invoke-virtual {p0}, Lcom/google/b/c/hw;->a()Lcom/google/b/c/hu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/c/hu;->e()I

    move-result v0

    return v0
.end method
