.class final Lcom/google/b/c/iv;
.super Lcom/google/b/c/cf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/cf",
        "<TV;TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/is;


# direct methods
.method constructor <init>(Lcom/google/b/c/is;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    invoke-direct {p0}, Lcom/google/b/c/cf;-><init>()V

    .line 237
    return-void
.end method


# virtual methods
.method public final bridge synthetic ap_()Lcom/google/b/c/ak;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    return-object v0
.end method

.method public final b()Lcom/google/b/c/cf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    return-object v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TV;TK;>;>;"
        }
    .end annotation

    .prologue
    .line 234
    new-instance v0, Lcom/google/b/c/iw;

    invoke-direct {v0, p0}, Lcom/google/b/c/iw;-><init>(Lcom/google/b/c/iv;)V

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 219
    if-nez p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-object v0

    .line 222
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/b/c/cc;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    iget v2, v2, Lcom/google/b/c/is;->c:I

    and-int/2addr v1, v2

    .line 223
    iget-object v2, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    iget-object v2, v2, Lcom/google/b/c/is;->a:[Lcom/google/b/c/iu;

    aget-object v1, v2, v1

    :goto_1
    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {v1}, Lcom/google/b/c/iu;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 226
    invoke-virtual {v1}, Lcom/google/b/c/iu;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 223
    :cond_2
    invoke-virtual {v1}, Lcom/google/b/c/iu;->b()Lcom/google/b/c/iu;

    move-result-object v1

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    invoke-virtual {v0}, Lcom/google/b/c/cf;->size()I

    move-result v0

    return v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Lcom/google/b/c/iy;

    iget-object v1, p0, Lcom/google/b/c/iv;->a:Lcom/google/b/c/is;

    invoke-direct {v0, v1}, Lcom/google/b/c/iy;-><init>(Lcom/google/b/c/cf;)V

    return-object v0
.end method
