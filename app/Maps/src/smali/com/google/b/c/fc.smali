.class Lcom/google/b/c/fc;
.super Lcom/google/b/c/bd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/bd",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/c/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ew",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/google/b/c/ew;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "Lcom/google/b/c/ew",
            "<-TK;-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/google/b/c/bd;-><init>()V

    .line 295
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    .line 296
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/b/c/ew;

    iput-object p2, p0, Lcom/google/b/c/fc;->a:Lcom/google/b/c/ew;

    .line 297
    return-void
.end method


# virtual methods
.method protected final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    return-object v0
.end method

.method protected final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/b/c/fc;->c:Ljava/util/Set;

    .line 307
    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/c/fc;->a:Lcom/google/b/c/ew;

    new-instance v0, Lcom/google/b/c/fb;

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/fb;-><init>(Ljava/util/Set;Lcom/google/b/c/ew;)V

    iput-object v0, p0, Lcom/google/b/c/fc;->c:Ljava/util/Set;

    .line 310
    :cond_0
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/b/c/fc;->a:Lcom/google/b/c/ew;

    invoke-interface {v0, p1, p2}, Lcom/google/b/c/ew;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 316
    iget-object v0, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 321
    iget-object v1, p0, Lcom/google/b/c/fc;->b:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/b/c/fc;->a:Lcom/google/b/c/ew;

    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v5, v0}, Lcom/google/b/c/ew;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 322
    return-void
.end method
