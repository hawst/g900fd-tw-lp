.class final Lcom/google/b/c/dh;
.super Lcom/google/b/c/dn;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dn",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/dc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/dc",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/b/c/dn;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/b/c/dh;->a:Lcom/google/b/c/dc;

    .line 39
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lcom/google/b/c/lg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/b/c/dh;->c()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/b/c/dh;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dc;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final f()Lcom/google/b/c/cv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/b/c/dh;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->c()Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->c()Lcom/google/b/c/cv;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/google/b/c/di;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/di;-><init>(Lcom/google/b/c/dh;Lcom/google/b/c/cv;)V

    return-object v1
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/b/c/dh;->c()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/b/c/dh;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->size()I

    move-result v0

    return v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/google/b/c/dj;

    iget-object v1, p0, Lcom/google/b/c/dh;->a:Lcom/google/b/c/dc;

    invoke-direct {v0, v1}, Lcom/google/b/c/dj;-><init>(Lcom/google/b/c/dc;)V

    return-object v0
.end method
