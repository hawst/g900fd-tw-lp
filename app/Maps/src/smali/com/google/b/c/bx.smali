.class abstract Lcom/google/b/c/bx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:I

.field c:Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:I

.field final synthetic f:Lcom/google/b/c/bl;


# direct methods
.method constructor <init>(Lcom/google/b/c/bl;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 329
    iput-object p1, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/c/bx;->b:I

    .line 331
    iput-object v1, p0, Lcom/google/b/c/bx;->c:Lcom/google/b/c/bm;

    .line 332
    iput-object v1, p0, Lcom/google/b/c/bx;->d:Lcom/google/b/c/bm;

    .line 333
    iget-object v0, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget v0, v0, Lcom/google/b/c/bl;->c:I

    iput v0, p0, Lcom/google/b/c/bx;->e:I

    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/b/c/bm;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;)TT;"
        }
    .end annotation
.end method

.method public hasNext()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 342
    iget-object v1, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget v1, v1, Lcom/google/b/c/bl;->c:I

    iget v2, p0, Lcom/google/b/c/bx;->e:I

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/google/b/c/bx;->c:Lcom/google/b/c/bm;

    if-eqz v1, :cond_2

    .line 353
    :goto_0
    return v0

    .line 351
    :cond_1
    iget v1, p0, Lcom/google/b/c/bx;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/c/bx;->b:I

    .line 346
    :cond_2
    iget v1, p0, Lcom/google/b/c/bx;->b:I

    iget-object v2, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget-object v2, v2, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 347
    iget-object v1, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget-object v1, v1, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    iget v2, p0, Lcom/google/b/c/bx;->b:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    .line 348
    iget-object v1, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget-object v1, v1, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    iget v2, p0, Lcom/google/b/c/bx;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/b/c/bx;->b:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/b/c/bx;->c:Lcom/google/b/c/bm;

    goto :goto_0

    .line 353
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget v0, v0, Lcom/google/b/c/bl;->c:I

    iget v1, p0, Lcom/google/b/c/bx;->e:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 358
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/bx;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 359
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/bx;->c:Lcom/google/b/c/bm;

    .line 363
    iget-object v1, v0, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    iput-object v1, p0, Lcom/google/b/c/bx;->c:Lcom/google/b/c/bm;

    .line 364
    iput-object v0, p0, Lcom/google/b/c/bx;->d:Lcom/google/b/c/bm;

    .line 365
    invoke-virtual {p0, v0}, Lcom/google/b/c/bx;->a(Lcom/google/b/c/bm;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget v0, v0, Lcom/google/b/c/bl;->c:I

    iget v1, p0, Lcom/google/b/c/bx;->e:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/bx;->d:Lcom/google/b/c/bm;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Only one remove() call allowed per call to next"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 371
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget-object v1, p0, Lcom/google/b/c/bx;->d:Lcom/google/b/c/bm;

    invoke-static {v0, v1}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bl;Lcom/google/b/c/bm;)V

    .line 372
    iget-object v0, p0, Lcom/google/b/c/bx;->f:Lcom/google/b/c/bl;

    iget v0, v0, Lcom/google/b/c/bl;->c:I

    iput v0, p0, Lcom/google/b/c/bx;->e:I

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/c/bx;->d:Lcom/google/b/c/bm;

    .line 374
    return-void
.end method
