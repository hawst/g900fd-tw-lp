.class Lcom/google/b/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation
.end field

.field b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation
.end field

.field final synthetic e:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;)V
    .locals 1

    .prologue
    .line 992
    iput-object p1, p0, Lcom/google/b/c/j;->e:Lcom/google/b/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 993
    iget-object v0, p1, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/j;->a:Ljava/util/Iterator;

    .line 994
    iget-object v0, p0, Lcom/google/b/c/j;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995
    invoke-direct {p0}, Lcom/google/b/c/j;->a()V

    .line 999
    :goto_0
    return-void

    .line 997
    :cond_0
    invoke-static {}, Lcom/google/b/c/eg;->b()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/google/b/c/j;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1003
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/b/c/j;->b:Ljava/lang/Object;

    .line 1004
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/b/c/j;->c:Ljava/util/Collection;

    .line 1005
    iget-object v0, p0, Lcom/google/b/c/j;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    .line 1006
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/google/b/c/j;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/b/c/j;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/b/c/j;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/c/hj;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/b/c/j;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1021
    iget-object v0, p0, Lcom/google/b/c/j;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/google/b/c/j;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/j;->e:Lcom/google/b/c/f;

    iget v1, v0, Lcom/google/b/c/f;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/b/c/f;->b:I

    .line 1025
    return-void
.end method
