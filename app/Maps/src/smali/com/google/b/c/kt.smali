.class Lcom/google/b/c/kt;
.super Lcom/google/b/c/kx;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/kl",
        "<TR;TC;TV;>.com/google/b/c/kx<TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/kl;


# direct methods
.method constructor <init>(Lcom/google/b/c/kl;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/google/b/c/kt;->a:Lcom/google/b/c/kl;

    invoke-direct {p0, p1}, Lcom/google/b/c/kx;-><init>(Lcom/google/b/c/kl;)V

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/b/c/kt;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0, p1}, Lcom/google/b/c/kl;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/b/c/kt;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0}, Lcom/google/b/c/kl;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 788
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/b/c/kt;->a:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/b/c/kt;->a:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
