.class Lcom/google/b/c/is;
.super Lcom/google/b/c/cf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/cf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final transient a:[Lcom/google/b/c/iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/iu",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final transient b:[Lcom/google/b/c/iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/iu",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final transient c:I

.field final transient d:I

.field private final transient e:[Lcom/google/b/c/iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/iu",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient f:Lcom/google/b/c/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cf",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Collection;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Lcom/google/b/c/cf;-><init>()V

    .line 88
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    .line 89
    const-wide v4, 0x3ff3333333333333L    # 1.2

    invoke-static {v2, v4, v5}, Lcom/google/b/c/cc;->a(ID)I

    move-result v3

    .line 90
    add-int/lit8 v4, v3, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/b/c/is;->c:I

    .line 91
    new-array v8, v3, [Lcom/google/b/c/iu;

    .line 92
    new-array v9, v3, [Lcom/google/b/c/iu;

    .line 93
    new-array v10, v2, [Lcom/google/b/c/iu;

    .line 94
    const/4 v3, 0x0

    .line 95
    const/4 v2, 0x0

    .line 97
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v4, v3

    move v3, v2

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 98
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    if-nez v12, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 99
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    if-nez v13, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 101
    :cond_1
    invoke-virtual {v12}, Ljava/lang/Object;->hashCode()I

    move-result v14

    .line 102
    invoke-virtual {v13}, Ljava/lang/Object;->hashCode()I

    move-result v15

    .line 103
    invoke-static {v14}, Lcom/google/b/c/cc;->a(I)I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/b/c/is;->c:I

    and-int v16, v5, v6

    .line 104
    invoke-static {v15}, Lcom/google/b/c/cc;->a(I)I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/b/c/is;->c:I

    and-int v17, v5, v6

    .line 106
    aget-object v7, v8, v16

    move-object v5, v7

    .line 107
    :goto_1
    if-eqz v5, :cond_3

    .line 109
    invoke-virtual {v5}, Lcom/google/b/c/iu;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v12, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 110
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Multiple entries with same key: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 107
    :cond_2
    invoke-virtual {v5}, Lcom/google/b/c/iu;->a()Lcom/google/b/c/iu;

    move-result-object v5

    goto :goto_1

    .line 114
    :cond_3
    aget-object v6, v9, v17

    move-object v5, v6

    .line 115
    :goto_2
    if-eqz v5, :cond_5

    .line 117
    invoke-virtual {v5}, Lcom/google/b/c/iu;->getValue()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 118
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Multiple entries with same value: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 115
    :cond_4
    invoke-virtual {v5}, Lcom/google/b/c/iu;->b()Lcom/google/b/c/iu;

    move-result-object v5

    goto :goto_2

    .line 122
    :cond_5
    if-nez v7, :cond_6

    if-nez v6, :cond_6

    new-instance v2, Lcom/google/b/c/iu;

    invoke-direct {v2, v12, v13}, Lcom/google/b/c/iu;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 125
    :goto_3
    aput-object v2, v8, v16

    .line 126
    aput-object v2, v9, v17

    .line 127
    add-int/lit8 v5, v4, 0x1

    aput-object v2, v10, v4

    .line 128
    xor-int v2, v14, v15

    add-int/2addr v2, v3

    move v3, v2

    move v4, v5

    .line 129
    goto/16 :goto_0

    .line 122
    :cond_6
    new-instance v2, Lcom/google/b/c/iz;

    invoke-direct {v2, v12, v13, v7, v6}, Lcom/google/b/c/iz;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/iu;Lcom/google/b/c/iu;)V

    goto :goto_3

    .line 131
    :cond_7
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/b/c/is;->e:[Lcom/google/b/c/iu;

    .line 132
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/b/c/is;->a:[Lcom/google/b/c/iu;

    .line 133
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/b/c/is;->b:[Lcom/google/b/c/iu;

    .line 134
    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/b/c/is;->d:I

    .line 135
    return-void
.end method


# virtual methods
.method public final synthetic ap_()Lcom/google/b/c/ak;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/b/c/is;->b()Lcom/google/b/c/cf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/b/c/cf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cf",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/b/c/is;->f:Lcom/google/b/c/cf;

    .line 203
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/iv;

    invoke-direct {v0, p0}, Lcom/google/b/c/iv;-><init>(Lcom/google/b/c/is;)V

    iput-object v0, p0, Lcom/google/b/c/is;->f:Lcom/google/b/c/cf;

    :cond_0
    return-object v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lcom/google/b/c/it;

    invoke-direct {v0, p0}, Lcom/google/b/c/it;-><init>(Lcom/google/b/c/is;)V

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 145
    if-nez p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-object v0

    .line 148
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/b/c/cc;->a(I)I

    move-result v1

    iget v2, p0, Lcom/google/b/c/is;->c:I

    and-int/2addr v1, v2

    .line 149
    iget-object v2, p0, Lcom/google/b/c/is;->e:[Lcom/google/b/c/iu;

    aget-object v1, v2, v1

    :goto_1
    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {v1}, Lcom/google/b/c/iu;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 152
    invoke-virtual {v1}, Lcom/google/b/c/iu;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {v1}, Lcom/google/b/c/iu;->a()Lcom/google/b/c/iu;

    move-result-object v1

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/b/c/is;->b:[Lcom/google/b/c/iu;

    array-length v0, v0

    return v0
.end method
