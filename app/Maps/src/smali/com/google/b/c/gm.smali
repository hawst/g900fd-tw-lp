.class final enum Lcom/google/b/c/gm;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/b/c/gn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/gm;",
        ">;",
        "Lcom/google/b/c/gn",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/gm;

.field private static final synthetic b:[Lcom/google/b/c/gm;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 724
    new-instance v0, Lcom/google/b/c/gm;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    .line 723
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/c/gm;

    sget-object v1, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/b/c/gm;->b:[Lcom/google/b/c/gm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/gm;
    .locals 1

    .prologue
    .line 723
    const-class v0, Lcom/google/b/c/gm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gm;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/gm;
    .locals 1

    .prologue
    .line 723
    sget-object v0, Lcom/google/b/c/gm;->b:[Lcom/google/b/c/gm;

    invoke-virtual {v0}, [Lcom/google/b/c/gm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/gm;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/b/c/hb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/hb",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 727
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public final a(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 754
    return-void
.end method

.method public final a(Lcom/google/b/c/hb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hb",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 730
    return-void
.end method

.method public final b()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 733
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 760
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 737
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 766
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 772
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 745
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final f()Lcom/google/b/c/gn;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 751
    return-object p0
.end method

.method public final g()Lcom/google/b/c/gn;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757
    return-object p0
.end method

.method public final h()Lcom/google/b/c/gn;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763
    return-object p0
.end method

.method public final i()Lcom/google/b/c/gn;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 769
    return-object p0
.end method
