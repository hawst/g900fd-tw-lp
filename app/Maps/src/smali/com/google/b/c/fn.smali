.class Lcom/google/b/c/fn;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:Ljava/util/logging/Logger;

.field static final q:Lcom/google/b/c/hb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/hb",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x5L


# instance fields
.field final transient b:I

.field final transient c:I

.field final transient d:[Lcom/google/b/c/go;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/go",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final e:I

.field final f:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/b/c/gr;

.field final i:Lcom/google/b/c/gr;

.field final j:I

.field final k:J

.field final l:J

.field final m:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/c/fm",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final n:Lcom/google/b/c/fl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/fl",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final transient o:Lcom/google/b/c/fs;

.field final p:Lcom/google/b/a/cb;

.field transient s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field transient t:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field transient u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    const-class v0, Lcom/google/b/c/fn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/b/c/fn;->a:Ljava/util/logging/Logger;

    .line 586
    new-instance v0, Lcom/google/b/c/fo;

    invoke-direct {v0}, Lcom/google/b/c/fo;-><init>()V

    sput-object v0, Lcom/google/b/c/fn;->q:Lcom/google/b/c/hb;

    .line 844
    new-instance v0, Lcom/google/b/c/fp;

    invoke-direct {v0}, Lcom/google/b/c/fp;-><init>()V

    sput-object v0, Lcom/google/b/c/fn;->r:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/google/b/c/fd;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const-wide/16 v2, 0x0

    const/4 v7, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 195
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 196
    iget v0, p1, Lcom/google/b/c/fd;->d:I

    if-ne v0, v7, :cond_3

    const/4 v0, 0x4

    :goto_0
    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/b/c/fn;->e:I

    .line 198
    invoke-virtual {p1}, Lcom/google/b/c/fd;->b()Lcom/google/b/c/gr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    .line 199
    iget-object v0, p1, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    sget-object v1, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eqz v0, :cond_4

    :goto_1
    check-cast v0, Lcom/google/b/c/gr;

    iput-object v0, p0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    .line 201
    iget-object v0, p1, Lcom/google/b/c/fd;->k:Lcom/google/b/a/x;

    invoke-virtual {p1}, Lcom/google/b/c/fd;->b()Lcom/google/b/c/gr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/c/gr;->a()Lcom/google/b/a/x;

    move-result-object v1

    if-eqz v0, :cond_6

    :goto_2
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, p0, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    .line 202
    iget-object v0, p0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    invoke-virtual {v0}, Lcom/google/b/c/gr;->a()Lcom/google/b/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/fn;->g:Lcom/google/b/a/x;

    .line 204
    iget v0, p1, Lcom/google/b/c/fd;->e:I

    iput v0, p0, Lcom/google/b/c/fn;->j:I

    .line 205
    iget-wide v0, p1, Lcom/google/b/c/fd;->i:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_8

    move-wide v0, v2

    :goto_3
    iput-wide v0, p0, Lcom/google/b/c/fn;->k:J

    .line 206
    iget-wide v0, p1, Lcom/google/b/c/fd;->h:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_9

    move-wide v0, v2

    :goto_4
    iput-wide v0, p0, Lcom/google/b/c/fn;->l:J

    .line 208
    iget-object v6, p0, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    iget-wide v0, p0, Lcom/google/b/c/fn;->l:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    move v0, v5

    :goto_5
    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/b/c/fn;->k:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_b

    move v0, v5

    :goto_6
    if-eqz v0, :cond_c

    :cond_0
    move v0, v5

    :goto_7
    iget v1, p0, Lcom/google/b/c/fn;->j:I

    if-eq v1, v7, :cond_d

    move v1, v5

    :goto_8
    invoke-static {v6, v0, v1}, Lcom/google/b/c/fs;->a(Lcom/google/b/c/gr;ZZ)Lcom/google/b/c/fs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/fn;->o:Lcom/google/b/c/fs;

    .line 209
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/b/a/cb;->b()Lcom/google/b/a/cb;

    move-result-object v1

    if-eqz v0, :cond_e

    :goto_9
    check-cast v0, Lcom/google/b/a/cb;

    iput-object v0, p0, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    .line 211
    invoke-virtual {p1}, Lcom/google/b/c/fd;->a()Lcom/google/b/c/fl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/fn;->n:Lcom/google/b/c/fl;

    .line 212
    iget-object v0, p0, Lcom/google/b/c/fn;->n:Lcom/google/b/c/fl;

    sget-object v1, Lcom/google/b/c/bi;->a:Lcom/google/b/c/bi;

    if-ne v0, v1, :cond_10

    sget-object v0, Lcom/google/b/c/fn;->r:Ljava/util/Queue;

    :goto_a
    iput-object v0, p0, Lcom/google/b/c/fn;->m:Ljava/util/Queue;

    .line 216
    iget v0, p1, Lcom/google/b/c/fd;->c:I

    if-ne v0, v7, :cond_11

    const/16 v0, 0x10

    :goto_b
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 217
    iget v1, p0, Lcom/google/b/c/fn;->j:I

    if-eq v1, v7, :cond_12

    move v1, v5

    :goto_c
    if-eqz v1, :cond_1

    .line 218
    iget v1, p0, Lcom/google/b/c/fn;->j:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_1
    move v1, v5

    move v2, v4

    .line 226
    :goto_d
    iget v3, p0, Lcom/google/b/c/fn;->e:I

    if-ge v1, v3, :cond_14

    iget v3, p0, Lcom/google/b/c/fn;->j:I

    if-eq v3, v7, :cond_13

    move v3, v5

    :goto_e
    if-eqz v3, :cond_2

    shl-int/lit8 v3, v1, 0x1

    iget v6, p0, Lcom/google/b/c/fn;->j:I

    if-gt v3, v6, :cond_14

    .line 227
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 228
    shl-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 196
    :cond_3
    iget v0, p1, Lcom/google/b/c/fd;->d:I

    goto/16 :goto_0

    .line 199
    :cond_4
    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move-object v0, v1

    goto/16 :goto_1

    .line 201
    :cond_6
    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move-object v0, v1

    goto/16 :goto_2

    .line 205
    :cond_8
    iget-wide v0, p1, Lcom/google/b/c/fd;->i:J

    goto/16 :goto_3

    .line 206
    :cond_9
    iget-wide v0, p1, Lcom/google/b/c/fd;->h:J

    goto/16 :goto_4

    :cond_a
    move v0, v4

    .line 208
    goto/16 :goto_5

    :cond_b
    move v0, v4

    goto :goto_6

    :cond_c
    move v0, v4

    goto :goto_7

    :cond_d
    move v1, v4

    goto :goto_8

    .line 209
    :cond_e
    if-nez v1, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move-object v0, v1

    goto :goto_9

    .line 212
    :cond_10
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_a

    .line 216
    :cond_11
    iget v0, p1, Lcom/google/b/c/fd;->c:I

    goto :goto_b

    :cond_12
    move v1, v4

    .line 217
    goto :goto_c

    :cond_13
    move v3, v4

    .line 226
    goto :goto_e

    .line 230
    :cond_14
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/c/fn;->c:I

    .line 231
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/c/fn;->b:I

    .line 233
    new-array v2, v1, [Lcom/google/b/c/go;

    iput-object v2, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    .line 235
    div-int v2, v0, v1

    .line 236
    mul-int v3, v2, v1

    if-ge v3, v0, :cond_1a

    .line 237
    add-int/lit8 v0, v2, 0x1

    :goto_f
    move v2, v5

    .line 241
    :goto_10
    if-ge v2, v0, :cond_15

    .line 242
    shl-int/lit8 v2, v2, 0x1

    goto :goto_10

    .line 245
    :cond_15
    iget v0, p0, Lcom/google/b/c/fn;->j:I

    if-eq v0, v7, :cond_17

    :goto_11
    if-eqz v5, :cond_18

    .line 247
    iget v0, p0, Lcom/google/b/c/fn;->j:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 248
    iget v3, p0, Lcom/google/b/c/fn;->j:I

    rem-int v1, v3, v1

    .line 249
    :goto_12
    iget-object v3, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    array-length v3, v3

    if-ge v4, v3, :cond_19

    .line 250
    if-ne v4, v1, :cond_16

    .line 251
    add-int/lit8 v0, v0, -0x1

    .line 253
    :cond_16
    iget-object v3, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    new-instance v5, Lcom/google/b/c/go;

    invoke-direct {v5, p0, v2, v0}, Lcom/google/b/c/go;-><init>(Lcom/google/b/c/fn;II)V

    aput-object v5, v3, v4

    .line 249
    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    :cond_17
    move v5, v4

    .line 245
    goto :goto_11

    .line 256
    :cond_18
    :goto_13
    iget-object v0, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    array-length v0, v0

    if-ge v4, v0, :cond_19

    .line 257
    iget-object v0, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    new-instance v1, Lcom/google/b/c/go;

    invoke-direct {v1, p0, v2, v7}, Lcom/google/b/c/go;-><init>(Lcom/google/b/c/fn;II)V

    aput-object v1, v0, v4

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    .line 260
    :cond_19
    return-void

    :cond_1a
    move v0, v2

    goto :goto_f
.end method

.method private a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 1759
    iget-object v0, p0, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v0, p1}, Lcom/google/b/a/x;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1760
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static a(Lcom/google/b/c/gn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1843
    sget-object v0, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    .line 1844
    invoke-interface {p0, v0}, Lcom/google/b/c/gn;->a(Lcom/google/b/c/gn;)V

    .line 1845
    invoke-interface {p0, v0}, Lcom/google/b/c/gn;->b(Lcom/google/b/c/gn;)V

    .line 1846
    return-void
.end method

.method static a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1837
    invoke-interface {p0, p1}, Lcom/google/b/c/gn;->a(Lcom/google/b/c/gn;)V

    .line 1838
    invoke-interface {p1, p0}, Lcom/google/b/c/gn;->b(Lcom/google/b/c/gn;)V

    .line 1839
    return-void
.end method

.method static a(Lcom/google/b/c/gn;J)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    .line 1832
    invoke-interface {p0}, Lcom/google/b/c/gn;->e()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b()Lcom/google/b/c/hb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/b/c/hb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 618
    sget-object v0, Lcom/google/b/c/fn;->q:Lcom/google/b/c/hb;

    return-object v0
.end method

.method static b(Lcom/google/b/c/gn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1875
    sget-object v0, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    .line 1876
    invoke-interface {p0, v0}, Lcom/google/b/c/gn;->c(Lcom/google/b/c/gn;)V

    .line 1877
    invoke-interface {p0, v0}, Lcom/google/b/c/gn;->d(Lcom/google/b/c/gn;)V

    .line 1878
    return-void
.end method

.method static b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1869
    invoke-interface {p0, p1}, Lcom/google/b/c/gn;->c(Lcom/google/b/c/gn;)V

    .line 1870
    invoke-interface {p1, p0}, Lcom/google/b/c/gn;->d(Lcom/google/b/c/gn;)V

    .line 1871
    return-void
.end method

.method static c()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 841
    sget-object v0, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    return-object v0
.end method

.method static d()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 875
    sget-object v0, Lcom/google/b/c/fn;->r:Ljava/util/Queue;

    return-object v0
.end method


# virtual methods
.method final a()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 267
    iget-wide v2, p0, Lcom/google/b/c/fn;->l:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/b/c/fn;->k:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public clear()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3453
    iget-object v5, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_b

    aget-object v7, v5, v4

    .line 3454
    iget v0, v7, Lcom/google/b/c/go;->b:I

    if-eqz v0, :cond_8

    invoke-virtual {v7}, Lcom/google/b/c/go;->lock()V

    :try_start_0
    iget-object v8, v7, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, v7, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->m:Ljava/util/Queue;

    sget-object v3, Lcom/google/b/c/fn;->r:Ljava/util/Queue;

    if-eq v0, v3, :cond_2

    move v3, v2

    :goto_1
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_2

    invoke-virtual {v8, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    :goto_2
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/b/c/hb;->b()Z

    move-result v9

    if-nez v9, :cond_0

    sget-object v9, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;

    invoke-interface {v0}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    invoke-interface {v0}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v9}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    :cond_0
    invoke-interface {v0}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v0

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_3
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, v7, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    sget-object v3, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v3, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, v7, Lcom/google/b/c/go;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_5
    iget-object v0, v7, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    sget-object v3, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, v7, Lcom/google/b/c/go;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_7
    iget-object v0, v7, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, v7, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, v7, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, v7, Lcom/google/b/c/go;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v7, Lcom/google/b/c/go;->c:I

    const/4 v0, 0x0

    iput v0, v7, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v7}, Lcom/google/b/c/go;->unlock()V

    invoke-virtual {v7}, Lcom/google/b/c/go;->d()V

    .line 3453
    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 3454
    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_5

    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lcom/google/b/c/go;->unlock()V

    invoke-virtual {v7}, Lcom/google/b/c/go;->d()V

    throw v0

    .line 3456
    :cond_b
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 3349
    if-nez p1, :cond_0

    .line 3350
    const/4 v0, 0x0

    .line 3353
    :goto_0
    return v0

    .line 3352
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3353
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/go;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 20
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 3358
    if-nez p1, :cond_0

    .line 3359
    const/4 v2, 0x0

    .line 3392
    :goto_0
    return v2

    .line 3367
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    .line 3368
    const-wide/16 v8, -0x1

    .line 3369
    const/4 v2, 0x0

    move v7, v2

    move-wide v10, v8

    :goto_1
    const/4 v2, 0x3

    if-ge v7, v2, :cond_c

    .line 3370
    const-wide/16 v4, 0x0

    .line 3371
    array-length v13, v12

    const/4 v2, 0x0

    move v6, v2

    move-wide v8, v4

    :goto_2
    if-ge v6, v13, :cond_b

    aget-object v14, v12, v6

    .line 3374
    iget v2, v14, Lcom/google/b/c/go;->b:I

    .line 3376
    iget-object v15, v14, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3377
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_a

    .line 3378
    invoke-virtual {v15, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/b/c/gn;

    move-object v5, v2

    :goto_4
    if-eqz v5, :cond_9

    .line 3379
    invoke-interface {v5}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {v14}, Lcom/google/b/c/go;->tryLock()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    invoke-virtual {v14}, Lcom/google/b/c/go;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    :cond_1
    const/4 v2, 0x0

    .line 3380
    :cond_2
    :goto_5
    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/c/fn;->g:Lcom/google/b/a/x;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v2}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3381
    const/4 v2, 0x1

    goto :goto_0

    .line 3379
    :catchall_0
    move-exception v2

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    throw v2

    :cond_3
    invoke-interface {v5}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    invoke-virtual {v14}, Lcom/google/b/c/go;->tryLock()Z

    move-result v2

    if-eqz v2, :cond_4

    :try_start_1
    invoke-virtual {v14}, Lcom/google/b/c/go;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    :catchall_1
    move-exception v2

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    throw v2

    :cond_5
    iget-object v4, v14, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-virtual {v4}, Lcom/google/b/c/fn;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v14, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v4, v4, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v4}, Lcom/google/b/a/cb;->a()J

    move-result-wide v16

    invoke-interface {v5}, Lcom/google/b/c/gn;->e()J

    move-result-wide v18

    sub-long v16, v16, v18

    const-wide/16 v18, 0x0

    cmp-long v4, v16, v18

    if-lez v4, :cond_7

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_2

    invoke-virtual {v14}, Lcom/google/b/c/go;->tryLock()Z

    move-result v2

    if-eqz v2, :cond_6

    :try_start_2
    invoke-virtual {v14}, Lcom/google/b/c/go;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    const/4 v4, 0x0

    goto :goto_6

    :catchall_2
    move-exception v2

    invoke-virtual {v14}, Lcom/google/b/c/go;->unlock()V

    throw v2

    .line 3378
    :cond_8
    invoke-interface {v5}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v2

    move-object v5, v2

    goto :goto_4

    .line 3377
    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_3

    .line 3385
    :cond_a
    iget v2, v14, Lcom/google/b/c/go;->c:I

    int-to-long v2, v2

    add-long v4, v8, v2

    .line 3371
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-wide v8, v4

    goto/16 :goto_2

    .line 3387
    :cond_b
    cmp-long v2, v8, v10

    if-eqz v2, :cond_c

    .line 3369
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move-wide v10, v8

    goto/16 :goto_1

    .line 3392
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3478
    iget-object v0, p0, Lcom/google/b/c/fn;->u:Ljava/util/Set;

    .line 3479
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/gc;

    invoke-direct {v0, p0}, Lcom/google/b/c/gc;-><init>(Lcom/google/b/c/fn;)V

    iput-object v0, p0, Lcom/google/b/c/fn;->u:Ljava/util/Set;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3328
    if-nez p1, :cond_0

    .line 3329
    const/4 v0, 0x0

    .line 3332
    :goto_0
    return-object v0

    .line 3331
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3332
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3293
    .line 3294
    iget-object v6, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    move v0, v1

    move-wide v2, v4

    .line 3295
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3296
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/c/go;->b:I

    if-eqz v7, :cond_1

    .line 3313
    :cond_0
    :goto_1
    return v1

    .line 3299
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/c/go;->c:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3295
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3302
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3303
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3304
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/c/go;->b:I

    if-nez v7, :cond_0

    .line 3307
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/c/go;->c:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3303
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3309
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3313
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3462
    iget-object v0, p0, Lcom/google/b/c/fn;->s:Ljava/util/Set;

    .line 3463
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/gl;

    invoke-direct {v0, p0}, Lcom/google/b/c/gl;-><init>(Lcom/google/b/c/fn;)V

    iput-object v0, p0, Lcom/google/b/c/fn;->s:Ljava/util/Set;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3397
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3398
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3399
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3400
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/b/c/go;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3412
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/b/c/fn;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3415
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3404
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3405
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3406
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3407
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/b/c/go;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3419
    if-nez p1, :cond_0

    .line 3420
    const/4 v0, 0x0

    .line 3423
    :goto_0
    return-object v0

    .line 3422
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3423
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/go;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 3427
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3428
    :cond_0
    const/4 v0, 0x0

    .line 3431
    :goto_0
    return v0

    .line 3430
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3431
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/b/c/go;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3445
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3446
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3447
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3448
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/b/c/go;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 3435
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3436
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3437
    :cond_1
    if-nez p2, :cond_2

    .line 3438
    const/4 v0, 0x0

    .line 3441
    :goto_0
    return v0

    .line 3440
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/b/c/fn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3441
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v2, p0, Lcom/google/b/c/fn;->c:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/c/fn;->b:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/google/b/c/go;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 3318
    iget-object v1, p0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    .line 3319
    const-wide/16 v2, 0x0

    .line 3320
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 3321
    aget-object v4, v1, v0

    iget v4, v4, Lcom/google/b/c/go;->b:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3323
    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    const v0, 0x7fffffff

    :goto_1
    return v0

    :cond_1
    const-wide/32 v0, -0x80000000

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_2
    long-to-int v0, v2

    goto :goto_1
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3470
    iget-object v0, p0, Lcom/google/b/c/fn;->t:Ljava/util/Collection;

    .line 3471
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/hc;

    invoke-direct {v0, p0}, Lcom/google/b/c/hc;-><init>(Lcom/google/b/c/fn;)V

    iput-object v0, p0, Lcom/google/b/c/fn;->t:Ljava/util/Collection;

    goto :goto_0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 3778
    new-instance v1, Lcom/google/b/c/gp;

    iget-object v2, p0, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    iget-object v3, p0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    iget-object v4, p0, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    iget-object v5, p0, Lcom/google/b/c/fn;->g:Lcom/google/b/a/x;

    iget-wide v6, p0, Lcom/google/b/c/fn;->l:J

    iget-wide v8, p0, Lcom/google/b/c/fn;->k:J

    iget v10, p0, Lcom/google/b/c/fn;->j:I

    iget v11, p0, Lcom/google/b/c/fn;->e:I

    iget-object v12, p0, Lcom/google/b/c/fn;->n:Lcom/google/b/c/fl;

    move-object v13, p0

    invoke-direct/range {v1 .. v13}, Lcom/google/b/c/gp;-><init>(Lcom/google/b/c/gr;Lcom/google/b/c/gr;Lcom/google/b/a/x;Lcom/google/b/a/x;JJIILcom/google/b/c/fl;Ljava/util/concurrent/ConcurrentMap;)V

    return-object v1
.end method
