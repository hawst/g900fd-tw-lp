.class Lcom/google/b/c/kl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/c/ky;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "C:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/b/c/ky",
        "<TR;TC;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;"
        }
    .end annotation
.end field

.field final b:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<+",
            "Ljava/util/Map",
            "<TC;TV;>;>;"
        }
    .end annotation
.end field

.field private transient c:Lcom/google/b/c/kn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/kl",
            "<TR;TC;TV;>.com/google/b/c/kn;"
        }
    .end annotation
.end field

.field private transient d:Lcom/google/b/c/kt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/kl",
            "<TR;TC;TV;>.com/google/b/c/kt;"
        }
    .end annotation
.end field

.field private transient e:Lcom/google/b/c/ku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/kl",
            "<TR;TC;TV;>.com/google/b/c/ku;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/google/b/a/bw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;",
            "Lcom/google/b/a/bw",
            "<+",
            "Ljava/util/Map",
            "<TC;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    .line 72
    iput-object p2, p0, Lcom/google/b/c/kl;->b:Lcom/google/b/a/bw;

    .line 73
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TC;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 174
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 175
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 176
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/b/c/kl;->b:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 98
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 78
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 82
    if-eqz v0, :cond_2

    invoke-static {v0, p2}, Lcom/google/b/c/hj;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 114
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 118
    :goto_0
    return-object v0

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 118
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0, p2}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/b/c/kl;->e:Lcom/google/b/c/ku;

    .line 957
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/ku;

    invoke-direct {v0, p0}, Lcom/google/b/c/ku;-><init>(Lcom/google/b/c/kl;)V

    iput-object v0, p0, Lcom/google/b/c/kl;->e:Lcom/google/b/c/ku;

    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Lcom/google/b/c/ko;

    invoke-direct {v0, p0, p1}, Lcom/google/b/c/ko;-><init>(Lcom/google/b/c/kl;Ljava/lang/Object;)V

    return-object v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 187
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 198
    :goto_0
    return-object v0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 191
    if-nez v0, :cond_2

    move-object v0, v1

    .line 192
    goto :goto_0

    .line 194
    :cond_2
    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 195
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 196
    iget-object v0, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    .line 198
    goto :goto_0
.end method

.method public c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/b/c/kl;->d:Lcom/google/b/c/kt;

    .line 767
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/kt;

    invoke-direct {v0, p0}, Lcom/google/b/c/kt;-><init>(Lcom/google/b/c/kl;)V

    iput-object v0, p0, Lcom/google/b/c/kl;->d:Lcom/google/b/c/kt;

    :cond_0
    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/b/c/kz",
            "<TR;TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/b/c/kl;->c:Lcom/google/b/c/kn;

    .line 278
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/kn;

    invoke-direct {v0, p0}, Lcom/google/b/c/kn;-><init>(Lcom/google/b/c/kl;)V

    iput-object v0, p0, Lcom/google/b/c/kl;->c:Lcom/google/b/c/kn;

    :cond_0
    return-object v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 128
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 129
    goto :goto_0

    .line 130
    :cond_0
    return v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 135
    if-ne p1, p0, :cond_0

    .line 136
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    .line 138
    :cond_0
    instance-of v0, p1, Lcom/google/b/c/ky;

    if-eqz v0, :cond_1

    .line 139
    check-cast p1, Lcom/google/b/c/ky;

    .line 140
    invoke-virtual {p0}, Lcom/google/b/c/kl;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/b/c/ky;->d()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/b/c/kl;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/b/c/kl;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
