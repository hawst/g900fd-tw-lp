.class Lcom/google/b/c/ko;
.super Ljava/util/AbstractMap;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<TC;TV;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation
.end field

.field c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TC;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TC;TV;>;>;"
        }
    .end annotation
.end field

.field final synthetic e:Lcom/google/b/c/kl;


# direct methods
.method constructor <init>(Lcom/google/b/c/kl;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/b/c/ko;->e:Lcom/google/b/c/kl;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 345
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/google/b/c/ko;->a:Ljava/lang/Object;

    .line 346
    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/c/ko;->e:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/b/c/ko;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/b/c/ko;->e:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/b/c/ko;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/b/c/ko;->e:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/b/c/ko;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    .line 365
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    .line 404
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 406
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/ko;->b()V

    .line 407
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 370
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/b/c/ko;->d:Ljava/util/Set;

    .line 431
    if-nez v0, :cond_0

    .line 432
    new-instance v0, Lcom/google/b/c/kq;

    invoke-direct {v0, p0}, Lcom/google/b/c/kq;-><init>(Lcom/google/b/c/ko;)V

    iput-object v0, p0, Lcom/google/b/c/ko;->d:Ljava/util/Set;

    .line 434
    :cond_0
    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 376
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/b/c/ko;->c:Ljava/util/Set;

    .line 414
    if-nez v0, :cond_0

    .line 415
    new-instance v0, Lcom/google/b/c/kp;

    invoke-direct {v0, p0}, Lcom/google/b/c/kp;-><init>(Lcom/google/b/c/ko;)V

    iput-object v0, p0, Lcom/google/b/c/ko;->c:Ljava/util/Set;

    .line 423
    :cond_0
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 381
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 382
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 384
    iget-object v0, p0, Lcom/google/b/c/ko;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 386
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/b/c/ko;->e:Lcom/google/b/c/kl;

    iget-object v1, p0, Lcom/google/b/c/ko;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/b/c/kl;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 392
    if-nez v0, :cond_0

    .line 393
    const/4 v0, 0x0

    .line 397
    :goto_0
    return-object v0

    .line 395
    :cond_0
    invoke-static {v0, p1}, Lcom/google/b/c/hj;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 396
    invoke-virtual {p0}, Lcom/google/b/c/ko;->b()V

    goto :goto_0
.end method
