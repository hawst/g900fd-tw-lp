.class abstract Lcom/google/b/c/gj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/b/c/go;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/go",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field e:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/b/c/hi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/fn",
            "<TK;TV;>.com/google/b/c/hi;"
        }
    .end annotation
.end field

.field g:Lcom/google/b/c/hi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/fn",
            "<TK;TV;>.com/google/b/c/hi;"
        }
    .end annotation
.end field

.field final synthetic h:Lcom/google/b/c/fn;


# direct methods
.method constructor <init>(Lcom/google/b/c/fn;)V
    .locals 1

    .prologue
    .line 3494
    iput-object p1, p0, Lcom/google/b/c/gj;->h:Lcom/google/b/c/fn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3495
    iget-object v0, p1, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/c/gj;->a:I

    .line 3496
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/c/gj;->b:I

    .line 3497
    invoke-virtual {p0}, Lcom/google/b/c/gj;->a()V

    .line 3498
    return-void
.end method

.method private a(Lcom/google/b/c/gn;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 3559
    :try_start_0
    invoke-interface {p1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v5

    .line 3560
    iget-object v4, p0, Lcom/google/b/c/gj;->h:Lcom/google/b/c/fn;

    invoke-interface {p1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 3561
    :cond_0
    :goto_0
    if-eqz v2, :cond_5

    .line 3562
    new-instance v1, Lcom/google/b/c/hi;

    iget-object v3, p0, Lcom/google/b/c/gj;->h:Lcom/google/b/c/fn;

    invoke-direct {v1, v3, v5, v2}, Lcom/google/b/c/hi;-><init>(Lcom/google/b/c/fn;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/b/c/gj;->f:Lcom/google/b/c/hi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3563
    iget-object v1, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    iget-object v2, v1, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    and-int/lit8 v2, v2, 0x3f

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/b/c/go;->c()V

    invoke-virtual {v1}, Lcom/google/b/c/go;->d()V

    .line 3566
    :cond_1
    :goto_1
    return v0

    .line 3560
    :cond_2
    :try_start_1
    invoke-interface {p1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Lcom/google/b/c/fn;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v4, v4, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v4}, Lcom/google/b/a/cb;->a()J

    move-result-wide v6

    invoke-interface {p1}, Lcom/google/b/c/gn;->e()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-lez v4, :cond_4

    move v4, v0

    :goto_2
    if-nez v4, :cond_0

    :cond_3
    move-object v2, v3

    goto :goto_0

    :cond_4
    move v4, v1

    goto :goto_2

    .line 3566
    :cond_5
    iget-object v0, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    iget-object v2, v0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    and-int/lit8 v2, v2, 0x3f

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {v0}, Lcom/google/b/c/go;->d()V

    :cond_6
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    iget-object v2, v1, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    and-int/lit8 v2, v2, 0x3f

    if-nez v2, :cond_7

    invoke-virtual {v1}, Lcom/google/b/c/go;->c()V

    invoke-virtual {v1}, Lcom/google/b/c/go;->d()V

    :cond_7
    throw v0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 3529
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    if-eqz v0, :cond_1

    .line 3530
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    :goto_0
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    if-eqz v0, :cond_1

    .line 3531
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    invoke-direct {p0, v0}, Lcom/google/b/c/gj;->a(Lcom/google/b/c/gn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3532
    const/4 v0, 0x1

    .line 3536
    :goto_1
    return v0

    .line 3530
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    goto :goto_0

    .line 3536
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 3543
    :cond_0
    iget v0, p0, Lcom/google/b/c/gj;->b:I

    if-ltz v0, :cond_2

    .line 3544
    iget-object v0, p0, Lcom/google/b/c/gj;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/google/b/c/gj;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/c/gj;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    iput-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    if-eqz v0, :cond_0

    .line 3545
    iget-object v0, p0, Lcom/google/b/c/gj;->e:Lcom/google/b/c/gn;

    invoke-direct {p0, v0}, Lcom/google/b/c/gj;->a(Lcom/google/b/c/gn;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/b/c/gj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3546
    :cond_1
    const/4 v0, 0x1

    .line 3550
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 3503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/c/gj;->f:Lcom/google/b/c/hi;

    .line 3505
    invoke-direct {p0}, Lcom/google/b/c/gj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3523
    :cond_0
    :goto_0
    return-void

    .line 3509
    :cond_1
    invoke-direct {p0}, Lcom/google/b/c/gj;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3513
    :cond_2
    iget v0, p0, Lcom/google/b/c/gj;->a:I

    if-ltz v0, :cond_0

    .line 3514
    iget-object v0, p0, Lcom/google/b/c/gj;->h:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v1, p0, Lcom/google/b/c/gj;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/c/gj;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    .line 3515
    iget-object v0, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    iget v0, v0, Lcom/google/b/c/go;->b:I

    if-eqz v0, :cond_2

    .line 3516
    iget-object v0, p0, Lcom/google/b/c/gj;->c:Lcom/google/b/c/go;

    iget-object v0, v0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lcom/google/b/c/gj;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3517
    iget-object v0, p0, Lcom/google/b/c/gj;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/c/gj;->b:I

    .line 3518
    invoke-direct {p0}, Lcom/google/b/c/gj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 3574
    iget-object v0, p0, Lcom/google/b/c/gj;->f:Lcom/google/b/c/hi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 3587
    iget-object v0, p0, Lcom/google/b/c/gj;->g:Lcom/google/b/c/hi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3588
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/gj;->h:Lcom/google/b/c/fn;

    iget-object v1, p0, Lcom/google/b/c/gj;->g:Lcom/google/b/c/hi;

    invoke-virtual {v1}, Lcom/google/b/c/hi;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/b/c/fn;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3589
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/c/gj;->g:Lcom/google/b/c/hi;

    .line 3590
    return-void
.end method
