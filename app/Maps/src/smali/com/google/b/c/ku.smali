.class Lcom/google/b/c/ku;
.super Lcom/google/b/c/hp;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/hp",
        "<TR;",
        "Ljava/util/Map",
        "<TC;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/kl;


# direct methods
.method constructor <init>(Lcom/google/b/c/kl;)V
    .locals 0

    .prologue
    .line 960
    iput-object p1, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    invoke-direct {p0}, Lcom/google/b/c/hp;-><init>()V

    .line 988
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 985
    new-instance v0, Lcom/google/b/c/kv;

    invoke-direct {v0, p0}, Lcom/google/b/c/kv;-><init>(Lcom/google/b/c/ku;)V

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0, p1}, Lcom/google/b/c/kl;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 960
    iget-object v0, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0, p1}, Lcom/google/b/c/kl;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0, p1}, Lcom/google/b/c/kl;->b(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 975
    iget-object v0, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0}, Lcom/google/b/c/kl;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 960
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/b/c/ku;->a:Lcom/google/b/c/kl;

    iget-object v0, v0, Lcom/google/b/c/kl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0
.end method
