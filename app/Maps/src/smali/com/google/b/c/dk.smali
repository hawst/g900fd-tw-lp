.class final Lcom/google/b/c/dk;
.super Lcom/google/b/c/ci;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ci",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/dc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/dc",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/b/c/ci;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    .line 37
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lcom/google/b/c/lg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->c()Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Lcom/google/b/c/lg;)Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dc;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final f()Lcom/google/b/c/cv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->c()Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->c()Lcom/google/b/c/cv;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/google/b/c/dl;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/dl;-><init>(Lcom/google/b/c/dk;Lcom/google/b/c/cv;)V

    return-object v1
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->c()Lcom/google/b/c/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Lcom/google/b/c/lg;)Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->size()I

    move-result v0

    return v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/google/b/c/dm;

    iget-object v1, p0, Lcom/google/b/c/dk;->a:Lcom/google/b/c/dc;

    invoke-direct {v0, v1}, Lcom/google/b/c/dm;-><init>(Lcom/google/b/c/dc;)V

    return-object v0
.end method
