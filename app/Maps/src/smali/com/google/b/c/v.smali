.class abstract Lcom/google/b/c/v;
.super Lcom/google/b/c/ae;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ae",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x1f3c5464cd7009c6L


# instance fields
.field transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TE;",
            "Lcom/google/b/c/as;",
            ">;"
        }
    .end annotation
.end field

.field transient b:J


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TE;",
            "Lcom/google/b/c/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/b/c/ae;-><init>()V

    .line 61
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    .line 62
    invoke-super {p0}, Lcom/google/b/c/ae;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/b/c/v;->b:J

    .line 63
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 197
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lcom/google/b/c/as;->a:I

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)I
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 211
    if-nez p2, :cond_0

    .line 212
    invoke-virtual {p0, p1}, Lcom/google/b/c/v;->a(Ljava/lang/Object;)I

    move-result v2

    .line 227
    :goto_0
    return v2

    .line 214
    :cond_0
    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 215
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 217
    if-nez v0, :cond_3

    .line 219
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    new-instance v1, Lcom/google/b/c/as;

    invoke-direct {v1, p2}, Lcom/google/b/c/as;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :goto_2
    iget-wide v0, p0, Lcom/google/b/c/v;->b:J

    int-to-long v4, p2

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/b/c/v;->b:J

    goto :goto_0

    .line 221
    :cond_3
    iget v4, v0, Lcom/google/b/c/as;->a:I

    .line 222
    int-to-long v6, v4

    int-to-long v8, p2

    add-long/2addr v6, v8

    .line 223
    const-wide/32 v8, 0x7fffffff

    cmp-long v3, v6, v8

    if-gtz v3, :cond_4

    move v3, v1

    :goto_3
    const-string v5, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v3, v2

    goto :goto_3

    .line 224
    :cond_5
    iget v1, v0, Lcom/google/b/c/as;->a:I

    add-int/2addr v1, p2

    iput v1, v0, Lcom/google/b/c/as;->a:I

    move v2, v4

    goto :goto_2
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/b/c/ia",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/b/c/ae;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    if-nez p2, :cond_1

    .line 233
    invoke-virtual {p0, p1}, Lcom/google/b/c/v;->a(Ljava/lang/Object;)I

    move-result v2

    .line 253
    :cond_0
    :goto_0
    return v2

    .line 235
    :cond_1
    if-lez p2, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 236
    :cond_3
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 237
    if-eqz v0, :cond_0

    .line 241
    iget v1, v0, Lcom/google/b/c/as;->a:I

    .line 244
    if-le v1, p2, :cond_4

    .line 251
    :goto_2
    neg-int v2, p2

    iget v3, v0, Lcom/google/b/c/as;->a:I

    add-int/2addr v2, v3

    iput v2, v0, Lcom/google/b/c/as;->a:I

    .line 252
    iget-wide v2, p0, Lcom/google/b/c/v;->b:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/b/c/v;->b:J

    move v2, v1

    .line 253
    goto :goto_0

    .line 248
    :cond_4
    iget-object v2, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    goto :goto_2
.end method

.method final b()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/b/c/ia",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/google/b/c/w;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/w;-><init>(Lcom/google/b/c/v;Ljava/util/Iterator;)V

    return-object v1
.end method

.method final c()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;I)I
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 259
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/google/b/c/ib;->a(ILjava/lang/String;)V

    .line 263
    if-nez p2, :cond_1

    .line 264
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 265
    if-nez v0, :cond_0

    move v0, v1

    .line 275
    :goto_0
    iget-wide v2, p0, Lcom/google/b/c/v;->b:J

    sub-int v1, p2, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/b/c/v;->b:J

    .line 276
    return v0

    .line 265
    :cond_0
    iget v1, v0, Lcom/google/b/c/as;->a:I

    iput p2, v0, Lcom/google/b/c/as;->a:I

    move v0, v1

    goto :goto_0

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 268
    if-nez v0, :cond_3

    .line 270
    :goto_1
    if-nez v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    new-instance v2, Lcom/google/b/c/as;

    invoke-direct {v2, p2}, Lcom/google/b/c/as;-><init>(I)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move v0, v1

    goto :goto_0

    .line 268
    :cond_3
    iget v1, v0, Lcom/google/b/c/as;->a:I

    iput p2, v0, Lcom/google/b/c/as;->a:I

    goto :goto_1
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    .line 129
    const/4 v2, 0x0

    iput v2, v0, Lcom/google/b/c/as;->a:I

    goto :goto_0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 132
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/b/c/v;->b:J

    .line 133
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v0, Lcom/google/b/c/y;

    invoke-direct {v0, p0}, Lcom/google/b/c/y;-><init>(Lcom/google/b/c/v;)V

    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/google/b/c/v;->b:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    const/high16 v0, -0x80000000

    goto :goto_0

    :cond_1
    long-to-int v0, v0

    goto :goto_0
.end method
