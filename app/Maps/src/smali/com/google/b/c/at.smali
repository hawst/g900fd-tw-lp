.class final Lcom/google/b/c/at;
.super Lcom/google/b/c/cf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/cf",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/b/c/at;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/b/c/at;

    invoke-direct {v0}, Lcom/google/b/c/at;-><init>()V

    sput-object v0, Lcom/google/b/c/at;->a:Lcom/google/b/c/at;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/b/c/cf;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic ap_()Lcom/google/b/c/ak;
    .locals 0

    .prologue
    .line 28
    return-object p0
.end method

.method public final b()Lcom/google/b/c/cf;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    return-object p0
.end method

.method public final c()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final e()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/b/c/at;->a:Lcom/google/b/c/at;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
