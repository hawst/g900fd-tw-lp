.class final Lcom/google/b/c/ka;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/b/c/kf;Lcom/google/b/c/kb;)I
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<+TE;>;TE;",
            "Ljava/util/Comparator",
            "<-TE;>;",
            "Lcom/google/b/c/kf;",
            "Lcom/google/b/c/kb;",
            ")I"
        }
    .end annotation

    .prologue
    .line 256
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 257
    :cond_0
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 258
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 259
    :cond_2
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 260
    :cond_3
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_4

    .line 261
    invoke-static {p0}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p0

    .line 265
    :cond_4
    const/4 v1, 0x0

    .line 266
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 268
    :goto_0
    if-gt v1, v0, :cond_7

    .line 269
    add-int v2, v1, v0

    ushr-int/lit8 v2, v2, 0x1

    .line 270
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 271
    if-gez v3, :cond_5

    .line 272
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 273
    :cond_5
    if-lez v3, :cond_6

    .line 274
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 276
    :cond_6
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p0, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    sub-int/2addr v2, v1

    invoke-virtual {p3, p2, p1, v0, v2}, Lcom/google/b/c/kf;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I

    move-result v0

    add-int/2addr v0, v1

    .line 280
    :goto_1
    return v0

    :cond_7
    invoke-virtual {p4, v1}, Lcom/google/b/c/kb;->a(I)I

    move-result v0

    goto :goto_1
.end method
