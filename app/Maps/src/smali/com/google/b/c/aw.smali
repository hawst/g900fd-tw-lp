.class final Lcom/google/b/c/aw;
.super Lcom/google/b/c/ds;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ds",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient b:Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dx",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/b/c/ds;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/b/c/dx;->a(Ljava/util/Comparator;)Lcom/google/b/c/dx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/aw;->b:Lcom/google/b/c/dx;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 92
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 93
    :cond_0
    return-object p0
.end method

.method public final aq_()Lcom/google/b/c/ci;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ci",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final as_()Lcom/google/b/c/dx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dx",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/b/c/aw;->b:Lcom/google/b/c/dx;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 98
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_0
    return-object p0
.end method

.method public final c()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic e()Lcom/google/b/c/dn;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/b/c/aw;->b:Lcom/google/b/c/dx;

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/b/c/aw;->b:Lcom/google/b/c/dx;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "{}"

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method
