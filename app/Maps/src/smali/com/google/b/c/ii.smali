.class public final Lcom/google/b/c/ii;
.super Lcom/google/b/c/fc;
.source "PG"

# interfaces
.implements Lcom/google/b/c/am;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/fc",
        "<",
        "Ljava/lang/Class",
        "<+TB;>;TB;>;",
        "Lcom/google/b/c/am",
        "<TB;>;"
    }
.end annotation


# static fields
.field private static final b:Lcom/google/b/c/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ew",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/b/c/ij;

    invoke-direct {v0}, Lcom/google/b/c/ij;-><init>()V

    sput-object v0, Lcom/google/b/c/ii;->b:Lcom/google/b/c/ew;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+TB;>;TB;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lcom/google/b/c/ii;->b:Lcom/google/b/c/ew;

    invoke-direct {p0, p1, v0}, Lcom/google/b/c/fc;-><init>(Ljava/util/Map;Lcom/google/b/c/ew;)V

    .line 58
    return-void
.end method

.method public static a(Ljava/util/Map;)Lcom/google/b/c/ii;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+TB;>;TB;>;)",
            "Lcom/google/b/c/ii",
            "<TB;>;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/google/b/c/ii;

    invoke-direct {v0, p0}, Lcom/google/b/c/ii;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/b/c/fc;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic putAll(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/b/c/fc;->putAll(Ljava/util/Map;)V

    return-void
.end method
