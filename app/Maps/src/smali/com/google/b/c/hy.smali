.class Lcom/google/b/c/hy;
.super Ljava/util/AbstractCollection;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/c/hu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/hu",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/hu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/hu",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1953
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 1954
    iput-object p1, p0, Lcom/google/b/c/hy;->a:Lcom/google/b/c/hu;

    .line 1955
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/google/b/c/hy;->a:Lcom/google/b/c/hu;

    invoke-interface {v0}, Lcom/google/b/c/hu;->f()V

    .line 1971
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1966
    iget-object v0, p0, Lcom/google/b/c/hy;->a:Lcom/google/b/c/hu;

    invoke-interface {v0, p1}, Lcom/google/b/c/hu;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/google/b/c/hy;->a:Lcom/google/b/c/hu;

    invoke-interface {v0}, Lcom/google/b/c/hu;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/hj;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/google/b/c/hy;->a:Lcom/google/b/c/hu;

    invoke-interface {v0}, Lcom/google/b/c/hu;->e()I

    move-result v0

    return v0
.end method
