.class Lcom/google/b/c/eq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/c/ip;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/b/c/ip",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TE;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1176
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/Iterator;

    iput-object p1, p0, Lcom/google/b/c/eq;->a:Ljava/util/Iterator;

    .line 1177
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1199
    iget-boolean v0, p0, Lcom/google/b/c/eq;->b:Z

    if-nez v0, :cond_0

    .line 1200
    iget-object v0, p0, Lcom/google/b/c/eq;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/eq;->c:Ljava/lang/Object;

    .line 1201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/b/c/eq;->b:Z

    .line 1203
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/eq;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 1180
    iget-boolean v0, p0, Lcom/google/b/c/eq;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/eq;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1184
    iget-boolean v0, p0, Lcom/google/b/c/eq;->b:Z

    if-nez v0, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/google/b/c/eq;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1190
    :goto_0
    return-object v0

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/eq;->c:Ljava/lang/Object;

    .line 1188
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/b/c/eq;->b:Z

    .line 1189
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/b/c/eq;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 1194
    iget-boolean v0, p0, Lcom/google/b/c/eq;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t remove after you\'ve peeked at next"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1195
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/eq;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1196
    return-void
.end method
