.class final Lcom/google/b/c/gl;
.super Ljava/util/AbstractSet;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/fn;


# direct methods
.method constructor <init>(Lcom/google/b/c/fn;)V
    .locals 0

    .prologue
    .line 3664
    iput-object p1, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 3693
    iget-object v0, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0}, Lcom/google/b/c/fn;->clear()V

    .line 3694
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3683
    iget-object v0, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0, p1}, Lcom/google/b/c/fn;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 3678
    iget-object v0, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0}, Lcom/google/b/c/fn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3668
    new-instance v0, Lcom/google/b/c/gk;

    iget-object v1, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-direct {v0, v1}, Lcom/google/b/c/gk;-><init>(Lcom/google/b/c/fn;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3688
    iget-object v0, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0, p1}, Lcom/google/b/c/fn;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 3673
    iget-object v0, p0, Lcom/google/b/c/gl;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0}, Lcom/google/b/c/fn;->size()I

    move-result v0

    return v0
.end method
