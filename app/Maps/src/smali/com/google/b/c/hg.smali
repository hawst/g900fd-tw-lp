.class final Lcom/google/b/c/hg;
.super Lcom/google/b/c/hd;
.source "PG"

# interfaces
.implements Lcom/google/b/c/gn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/hd",
        "<TK;TV;>;",
        "Lcom/google/b/c/gn",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile d:J

.field e:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field g:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field h:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/b/c/gn;)V
    .locals 2
    .param p4    # Lcom/google/b/c/gn;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1538
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/b/c/hd;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/b/c/gn;)V

    .line 1543
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/b/c/hg;->d:J

    .line 1555
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/hg;->e:Lcom/google/b/c/gn;

    .line 1568
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/hg;->f:Lcom/google/b/c/gn;

    .line 1583
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/hg;->g:Lcom/google/b/c/gn;

    .line 1596
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/hg;->h:Lcom/google/b/c/gn;

    .line 1539
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 1552
    iput-wide p1, p0, Lcom/google/b/c/hg;->d:J

    .line 1553
    return-void
.end method

.method public final a(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1565
    iput-object p1, p0, Lcom/google/b/c/hg;->e:Lcom/google/b/c/gn;

    .line 1566
    return-void
.end method

.method public final b(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1578
    iput-object p1, p0, Lcom/google/b/c/hg;->f:Lcom/google/b/c/gn;

    .line 1579
    return-void
.end method

.method public final c(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/google/b/c/hg;->g:Lcom/google/b/c/gn;

    .line 1594
    return-void
.end method

.method public final d(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1606
    iput-object p1, p0, Lcom/google/b/c/hg;->h:Lcom/google/b/c/gn;

    .line 1607
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1547
    iget-wide v0, p0, Lcom/google/b/c/hg;->d:J

    return-wide v0
.end method

.method public final f()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1560
    iget-object v0, p0, Lcom/google/b/c/hg;->e:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final g()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/b/c/hg;->f:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final h()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/google/b/c/hg;->g:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final i()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1601
    iget-object v0, p0, Lcom/google/b/c/hg;->h:Lcom/google/b/c/gn;

    return-object v0
.end method
