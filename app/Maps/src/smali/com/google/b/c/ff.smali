.class abstract enum Lcom/google/b/c/ff;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/ff;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/ff;

.field public static final enum b:Lcom/google/b/c/ff;

.field public static final enum c:Lcom/google/b/c/ff;

.field public static final enum d:Lcom/google/b/c/ff;

.field public static final enum e:Lcom/google/b/c/ff;

.field private static final synthetic f:[Lcom/google/b/c/ff;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 691
    new-instance v0, Lcom/google/b/c/fg;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/fg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;

    .line 705
    new-instance v0, Lcom/google/b/c/fh;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lcom/google/b/c/fh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/ff;->b:Lcom/google/b/c/ff;

    .line 717
    new-instance v0, Lcom/google/b/c/fi;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lcom/google/b/c/fi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    .line 729
    new-instance v0, Lcom/google/b/c/fj;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lcom/google/b/c/fj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/ff;->d:Lcom/google/b/c/ff;

    .line 741
    new-instance v0, Lcom/google/b/c/fk;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lcom/google/b/c/fk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/ff;->e:Lcom/google/b/c/ff;

    .line 686
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/c/ff;

    sget-object v1, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/c/ff;->b:Lcom/google/b/c/ff;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/c/ff;->d:Lcom/google/b/c/ff;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/c/ff;->e:Lcom/google/b/c/ff;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/c/ff;->f:[Lcom/google/b/c/ff;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 686
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/ff;
    .locals 1

    .prologue
    .line 686
    const-class v0, Lcom/google/b/c/ff;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/ff;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/ff;
    .locals 1

    .prologue
    .line 686
    sget-object v0, Lcom/google/b/c/ff;->f:[Lcom/google/b/c/ff;

    invoke-virtual {v0}, [Lcom/google/b/c/ff;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/ff;

    return-object v0
.end method
