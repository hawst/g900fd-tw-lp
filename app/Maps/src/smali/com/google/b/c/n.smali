.class Lcom/google/b/c/n;
.super Lcom/google/b/c/g;
.source "PG"

# interfaces
.implements Ljava/util/SortedMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/f",
        "<TK;TV;>.com/google/b/c/g;",
        "Ljava/util/SortedMap",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field d:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<TK;>;"
        }
    .end annotation
.end field

.field final synthetic e:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1181
    iput-object p1, p0, Lcom/google/b/c/n;->e:Lcom/google/b/c/f;

    .line 1182
    invoke-direct {p0, p1, p2}, Lcom/google/b/c/g;-><init>(Lcom/google/b/c/f;Ljava/util/Map;)V

    .line 1183
    return-void
.end method


# virtual methods
.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/SortedMap",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1202
    new-instance v1, Lcom/google/b/c/n;

    iget-object v2, p0, Lcom/google/b/c/n;->e:Lcom/google/b/c/f;

    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/b/c/n;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 3

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/b/c/n;->d:Ljava/util/SortedSet;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/b/c/o;

    iget-object v2, p0, Lcom/google/b/c/n;->e:Lcom/google/b/c/f;

    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v2, v0}, Lcom/google/b/c/o;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    iput-object v1, p0, Lcom/google/b/c/n;->d:Ljava/util/SortedSet;

    move-object v0, v1

    :cond_0
    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TK;)",
            "Ljava/util/SortedMap",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1206
    new-instance v1, Lcom/google/b/c/n;

    iget-object v2, p0, Lcom/google/b/c/n;->e:Lcom/google/b/c/f;

    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/b/c/n;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/SortedMap",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1210
    new-instance v1, Lcom/google/b/c/n;

    iget-object v2, p0, Lcom/google/b/c/n;->e:Lcom/google/b/c/f;

    iget-object v0, p0, Lcom/google/b/c/n;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/b/c/n;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    return-object v1
.end method
