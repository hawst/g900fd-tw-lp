.class public final Lcom/google/b/c/dy;
.super Lcom/google/b/c/dp;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dp",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/google/b/c/dp;-><init>()V

    .line 504
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/Comparator;

    iput-object p1, p0, Lcom/google/b/c/dy;->c:Ljava/util/Comparator;

    .line 505
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Iterable;)Lcom/google/b/c/ck;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;)Lcom/google/b/c/ck;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final synthetic a(Ljava/util/Iterator;)Lcom/google/b/c/ck;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/util/Iterator;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final synthetic a()Lcom/google/b/c/dn;
    .locals 3

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/b/c/dy;->a:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/b/c/dy;->c:Ljava/util/Comparator;

    iget v2, p0, Lcom/google/b/c/dy;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/b/c/dx;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->size()I

    move-result v1

    iput v1, p0, Lcom/google/b/c/dy;->b:I

    return-object v0
.end method

.method public final bridge synthetic a([Ljava/lang/Object;)Lcom/google/b/c/dp;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->a([Ljava/lang/Object;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final bridge synthetic b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Lcom/google/b/c/dp;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final bridge synthetic b(Ljava/util/Iterator;)Lcom/google/b/c/dp;
    .locals 0

    .prologue
    .line 496
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/util/Iterator;)Lcom/google/b/c/dp;

    return-object p0
.end method

.method public final varargs b([Ljava/lang/Object;)Lcom/google/b/c/dy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "Lcom/google/b/c/dy",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 533
    invoke-super {p0, p1}, Lcom/google/b/c/dp;->a([Ljava/lang/Object;)Lcom/google/b/c/dp;

    .line 534
    return-object p0
.end method
