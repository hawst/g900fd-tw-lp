.class Lcom/google/b/c/bt;
.super Lcom/google/b/c/z;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/z",
        "<TV;TK;>;"
    }
.end annotation


# instance fields
.field a:Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/b/c/bs;


# direct methods
.method constructor <init>(Lcom/google/b/c/bs;Lcom/google/b/c/bm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 597
    iput-object p1, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    invoke-direct {p0}, Lcom/google/b/c/z;-><init>()V

    .line 598
    iput-object p2, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    .line 599
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget-object v0, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget-object v0, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 613
    iget-object v0, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget-object v3, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    .line 614
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v4

    .line 615
    iget-object v0, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget v0, v0, Lcom/google/b/c/bm;->b:I

    if-ne v4, v0, :cond_3

    if-eq p1, v3, :cond_0

    if-eqz p1, :cond_2

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    .line 626
    :goto_2
    return-object p1

    .line 614
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 615
    goto :goto_1

    .line 618
    :cond_3
    iget-object v0, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    iget-object v0, v0, Lcom/google/b/c/bs;->a:Lcom/google/b/c/br;

    iget-object v0, v0, Lcom/google/b/c/br;->a:Lcom/google/b/c/bq;

    iget-object v0, v0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-virtual {v0, p1, v4}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    const-string v5, "value already present: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5, v2}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v1

    goto :goto_3

    .line 619
    :cond_5
    iget-object v0, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    iget-object v0, v0, Lcom/google/b/c/bs;->a:Lcom/google/b/c/br;

    iget-object v0, v0, Lcom/google/b/c/br;->a:Lcom/google/b/c/bq;

    iget-object v0, v0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    iget-object v1, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    invoke-static {v0, v1}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bl;Lcom/google/b/c/bm;)V

    .line 620
    new-instance v0, Lcom/google/b/c/bm;

    iget-object v1, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget-object v1, v1, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/bt;->a:Lcom/google/b/c/bm;

    iget v2, v2, Lcom/google/b/c/bm;->d:I

    invoke-direct {v0, p1, v4, v1, v2}, Lcom/google/b/c/bm;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 622
    iget-object v1, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    iget-object v1, v1, Lcom/google/b/c/bs;->a:Lcom/google/b/c/br;

    iget-object v1, v1, Lcom/google/b/c/br;->a:Lcom/google/b/c/bq;

    iget-object v1, v1, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-virtual {v1, v0}, Lcom/google/b/c/bl;->b(Lcom/google/b/c/bm;)V

    .line 623
    iget-object v0, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    iget-object v1, p0, Lcom/google/b/c/bt;->b:Lcom/google/b/c/bs;

    iget-object v1, v1, Lcom/google/b/c/bs;->a:Lcom/google/b/c/br;

    iget-object v1, v1, Lcom/google/b/c/br;->a:Lcom/google/b/c/bq;

    iget-object v1, v1, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    iget v1, v1, Lcom/google/b/c/bl;->c:I

    iput v1, v0, Lcom/google/b/c/bs;->e:I

    move-object p1, v3

    .line 626
    goto :goto_2
.end method
