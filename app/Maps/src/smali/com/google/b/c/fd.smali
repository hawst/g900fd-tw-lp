.class public final Lcom/google/b/c/fd;
.super Lcom/google/b/c/bh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/bh",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field b:Z

.field c:I

.field d:I

.field e:I

.field f:Lcom/google/b/c/gr;

.field g:Lcom/google/b/c/gr;

.field h:J

.field i:J

.field j:Lcom/google/b/c/ff;

.field k:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 139
    invoke-direct {p0}, Lcom/google/b/c/bh;-><init>()V

    .line 119
    iput v0, p0, Lcom/google/b/c/fd;->c:I

    .line 120
    iput v0, p0, Lcom/google/b/c/fd;->d:I

    .line 121
    iput v0, p0, Lcom/google/b/c/fd;->e:I

    .line 126
    iput-wide v2, p0, Lcom/google/b/c/fd;->h:J

    .line 127
    iput-wide v2, p0, Lcom/google/b/c/fd;->i:J

    .line 139
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/b/c/fd;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 245
    iget v0, p0, Lcom/google/b/c/fd;->d:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "concurrency level was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/b/c/fd;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 247
    :cond_1
    if-lez p1, :cond_2

    :goto_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 248
    :cond_3
    iput p1, p0, Lcom/google/b/c/fd;->d:I

    .line 249
    return-object p0
.end method

.method public final a(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 274
    iget-object v0, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    aput-object v5, v4, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 275
    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/b/c/gr;

    iput-object v0, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    .line 276
    iget-object v0, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    sget-object v3, Lcom/google/b/c/gr;->b:Lcom/google/b/c/gr;

    if-eq v0, v3, :cond_3

    move v2, v1

    :cond_3
    const-string v0, "Soft keys are not supported"

    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 277
    :cond_4
    sget-object v0, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq p1, v0, :cond_5

    .line 279
    iput-boolean v1, p0, Lcom/google/b/c/fd;->b:Z

    .line 281
    :cond_5
    return-object p0
.end method

.method a(JLjava/util/concurrent/TimeUnit;)V
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 390
    iget-wide v4, p0, Lcom/google/b/c/fd;->h:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/b/c/fd;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 392
    :cond_1
    iget-wide v4, p0, Lcom/google/b/c/fd;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/b/c/fd;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 394
    :cond_3
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_4

    move v0, v1

    :goto_2
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    .line 395
    :cond_5
    return-void
.end method

.method public final b(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 338
    iget-object v0, p0, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Value strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    aput-object v5, v4, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 339
    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/b/c/gr;

    iput-object v0, p0, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    .line 340
    sget-object v0, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq p1, v0, :cond_3

    .line 342
    iput-boolean v1, p0, Lcom/google/b/c/fd;->b:Z

    .line 344
    :cond_3
    return-object p0
.end method

.method final b()Lcom/google/b/c/gr;
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    sget-object v1, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/b/c/gr;

    return-object v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c()Ljava/util/concurrent/ConcurrentMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 506
    iget-boolean v0, p0, Lcom/google/b/c/fd;->b:Z

    if-nez v0, :cond_2

    .line 507
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    iget v0, p0, Lcom/google/b/c/fd;->c:I

    if-ne v0, v4, :cond_0

    const/16 v0, 0x10

    :goto_0
    const/high16 v3, 0x3f400000    # 0.75f

    iget v1, p0, Lcom/google/b/c/fd;->d:I

    if-ne v1, v4, :cond_1

    const/4 v1, 0x4

    :goto_1
    invoke-direct {v2, v0, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    move-object v0, v2

    .line 509
    :goto_2
    return-object v0

    .line 507
    :cond_0
    iget v0, p0, Lcom/google/b/c/fd;->c:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/b/c/fd;->d:I

    goto :goto_1

    .line 509
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/c/fn;

    invoke-direct {v0, p0}, Lcom/google/b/c/fn;-><init>(Lcom/google/b/c/fd;)V

    :goto_3
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/b/c/fe;

    invoke-direct {v0, p0}, Lcom/google/b/c/fe;-><init>(Lcom/google/b/c/fd;)V

    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    .line 597
    new-instance v0, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 598
    iget v1, p0, Lcom/google/b/c/fd;->c:I

    if-eq v1, v3, :cond_0

    .line 599
    const-string v1, "initialCapacity"

    iget v2, p0, Lcom/google/b/c/fd;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;I)Lcom/google/b/a/ak;

    .line 601
    :cond_0
    iget v1, p0, Lcom/google/b/c/fd;->d:I

    if-eq v1, v3, :cond_1

    .line 602
    const-string v1, "concurrencyLevel"

    iget v2, p0, Lcom/google/b/c/fd;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;I)Lcom/google/b/a/ak;

    .line 604
    :cond_1
    iget v1, p0, Lcom/google/b/c/fd;->e:I

    if-eq v1, v3, :cond_2

    .line 605
    const-string v1, "maximumSize"

    iget v2, p0, Lcom/google/b/c/fd;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;I)Lcom/google/b/a/ak;

    .line 607
    :cond_2
    iget-wide v2, p0, Lcom/google/b/c/fd;->h:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 608
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/google/b/c/fd;->h:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 610
    :cond_3
    iget-wide v2, p0, Lcom/google/b/c/fd;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 611
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/google/b/c/fd;->i:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 613
    :cond_4
    iget-object v1, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    if-eqz v1, :cond_5

    .line 614
    const-string v1, "keyStrength"

    iget-object v2, p0, Lcom/google/b/c/fd;->f:Lcom/google/b/c/gr;

    invoke-virtual {v2}, Lcom/google/b/c/gr;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 616
    :cond_5
    iget-object v1, p0, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    if-eqz v1, :cond_6

    .line 617
    const-string v1, "valueStrength"

    iget-object v2, p0, Lcom/google/b/c/fd;->g:Lcom/google/b/c/gr;

    invoke-virtual {v2}, Lcom/google/b/c/gr;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 619
    :cond_6
    iget-object v1, p0, Lcom/google/b/c/fd;->k:Lcom/google/b/a/x;

    if-eqz v1, :cond_7

    .line 620
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/b/a/ak;->a(Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 622
    :cond_7
    iget-object v1, p0, Lcom/google/b/c/fd;->a:Lcom/google/b/c/fl;

    if-eqz v1, :cond_8

    .line 623
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lcom/google/b/a/ak;->a(Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 625
    :cond_8
    invoke-virtual {v0}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
