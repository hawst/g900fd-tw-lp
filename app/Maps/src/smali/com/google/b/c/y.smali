.class Lcom/google/b/c/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TE;",
            "Lcom/google/b/c/as;",
            ">;>;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TE;",
            "Lcom/google/b/c/as;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Z

.field final synthetic e:Lcom/google/b/c/v;


# direct methods
.method constructor <init>(Lcom/google/b/c/v;)V
    .locals 1

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/b/c/y;->e:Lcom/google/b/c/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iget-object v0, p1, Lcom/google/b/c/v;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/y;->a:Ljava/util/Iterator;

    .line 165
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/b/c/y;->c:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/y;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 172
    iget v0, p0, Lcom/google/b/c/y;->c:I

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/b/c/y;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/google/b/c/y;->b:Ljava/util/Map$Entry;

    .line 174
    iget-object v0, p0, Lcom/google/b/c/y;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    iget v0, v0, Lcom/google/b/c/as;->a:I

    iput v0, p0, Lcom/google/b/c/y;->c:I

    .line 176
    :cond_0
    iget v0, p0, Lcom/google/b/c/y;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/c/y;->c:I

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/b/c/y;->d:Z

    .line 178
    iget-object v0, p0, Lcom/google/b/c/y;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 6

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/google/b/c/y;->d:Z

    const-string v1, "no calls to next() since the last call to remove()"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/y;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    iget v0, v0, Lcom/google/b/c/as;->a:I

    .line 184
    if-gtz v0, :cond_1

    .line 185
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/y;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/as;

    const/4 v1, -0x1

    iget v2, v0, Lcom/google/b/c/as;->a:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/google/b/c/as;->a:I

    if-nez v1, :cond_2

    .line 188
    iget-object v0, p0, Lcom/google/b/c/y;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/y;->e:Lcom/google/b/c/v;

    iget-wide v2, v0, Lcom/google/b/c/v;->b:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/b/c/v;->b:J

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/b/c/y;->d:Z

    .line 192
    return-void
.end method
