.class public abstract Lcom/google/b/c/ds;
.super Lcom/google/b/c/dw;
.source "PG"

# interfaces
.implements Ljava/util/SortedMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dw",
        "<TK;TV;>;",
        "Ljava/util/SortedMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/b/c/ds;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ds",
            "<",
            "Ljava/lang/Comparable;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v0

    sput-object v0, Lcom/google/b/c/ds;->b:Ljava/util/Comparator;

    .line 67
    new-instance v0, Lcom/google/b/c/aw;

    sget-object v1, Lcom/google/b/c/ds;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/b/c/aw;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/b/c/ds;->a:Lcom/google/b/c/ds;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Lcom/google/b/c/dw;-><init>()V

    return-void
.end method

.method static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/google/b/c/ds;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/c/ds;->a:Lcom/google/b/c/ds;

    .line 91
    :goto_0
    return-object v0

    .line 81
    :cond_0
    new-instance v0, Lcom/google/b/c/aw;

    invoke-direct {v0, p0}, Lcom/google/b/c/aw;-><init>(Ljava/util/Comparator;)V

    goto :goto_0

    .line 84
    :cond_1
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 85
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 86
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 88
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 91
    :cond_2
    new-instance v0, Lcom/google/b/c/jh;

    new-instance v3, Lcom/google/b/c/jk;

    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v3, v1, p0}, Lcom/google/b/c/jk;-><init>(Lcom/google/b/c/cv;Ljava/util/Comparator;)V

    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/google/b/c/jh;-><init>(Lcom/google/b/c/jk;Lcom/google/b/c/cv;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract aq_()Lcom/google/b/c/ci;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ci",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract as_()Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dx",
            "<TK;>;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public c()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 437
    invoke-super {p0}, Lcom/google/b/c/dw;->c()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/google/b/c/ds;->aq_()Lcom/google/b/c/ci;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/b/c/ci;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic e()Lcom/google/b/c/dn;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/b/c/ds;->c()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method f()Z
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/ds;->aq_()Lcom/google/b/c/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/ci;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/ds;->a(Ljava/lang/Object;Z)Lcom/google/b/c/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/google/b/c/ds;->as_()Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/google/b/c/ds;->aq_()Lcom/google/b/c/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/ci;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/ds;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "expected fromKey <= toKey but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p2, v4, v1

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p2, v2}, Lcom/google/b/c/ds;->a(Ljava/lang/Object;Z)Lcom/google/b/c/ds;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lcom/google/b/c/ds;->b(Ljava/lang/Object;Z)Lcom/google/b/c/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/ds;->b(Ljava/lang/Object;Z)Lcom/google/b/c/ds;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/b/c/ds;->aq_()Lcom/google/b/c/ci;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 684
    new-instance v0, Lcom/google/b/c/dv;

    invoke-direct {v0, p0}, Lcom/google/b/c/dv;-><init>(Lcom/google/b/c/ds;)V

    return-object v0
.end method
