.class Lcom/google/b/c/t;
.super Lcom/google/b/c/p;
.source "PG"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/f",
        "<TK;TV;>.com/google/b/c/p;",
        "Ljava/util/Set",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic f:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Set;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Set",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 600
    iput-object p1, p0, Lcom/google/b/c/t;->f:Lcom/google/b/c/f;

    .line 601
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/b/c/p;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/b/c/p;)V

    .line 602
    return-void
.end method


# virtual methods
.method public removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 606
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    const/4 v0, 0x0

    .line 620
    :cond_0
    :goto_0
    return v0

    .line 609
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/t;->size()I

    move-result v1

    .line 614
    iget-object v0, p0, Lcom/google/b/c/t;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Collection;)Z

    move-result v0

    .line 615
    if-eqz v0, :cond_0

    .line 616
    iget-object v2, p0, Lcom/google/b/c/t;->b:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 617
    iget-object v3, p0, Lcom/google/b/c/t;->f:Lcom/google/b/c/f;

    sub-int v1, v2, v1

    iget v2, v3, Lcom/google/b/c/f;->b:I

    add-int/2addr v1, v2

    iput v1, v3, Lcom/google/b/c/f;->b:I

    .line 618
    invoke-virtual {p0}, Lcom/google/b/c/t;->b()V

    goto :goto_0
.end method
