.class Lcom/google/b/c/kq;
.super Lcom/google/b/c/ho;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/ho",
        "<TC;TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/ko;


# direct methods
.method constructor <init>(Lcom/google/b/c/ko;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/b/c/kq;->a:Lcom/google/b/c/ko;

    invoke-direct {p0}, Lcom/google/b/c/ho;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TC;TV;>;"
        }
    .end annotation

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/b/c/kq;->a:Lcom/google/b/c/ko;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/b/c/kq;->a:Lcom/google/b/c/ko;

    invoke-virtual {v0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 453
    if-nez v0, :cond_0

    .line 454
    invoke-static {}, Lcom/google/b/c/eg;->b()Ljava/util/Iterator;

    move-result-object v0

    .line 457
    :goto_0
    return-object v0

    .line 456
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 457
    new-instance v0, Lcom/google/b/c/kr;

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/kr;-><init>(Lcom/google/b/c/kq;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/b/c/kq;->a:Lcom/google/b/c/ko;

    invoke-virtual {v0}, Lcom/google/b/c/ko;->a()Ljava/util/Map;

    move-result-object v0

    .line 447
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method
