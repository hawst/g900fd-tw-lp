.class public abstract Lcom/google/b/c/dx;
.super Lcom/google/b/c/ea;
.source "PG"

# interfaces
.implements Lcom/google/b/c/jy;
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ea",
        "<TE;>;",
        "Lcom/google/b/c/jy",
        "<TE;>;",
        "Ljava/util/SortedSet",
        "<TE;>;"
    }
.end annotation


# static fields
.field static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dx",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final transient d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 98
    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v0

    sput-object v0, Lcom/google/b/c/dx;->a:Ljava/util/Comparator;

    .line 100
    new-instance v0, Lcom/google/b/c/ax;

    sget-object v1, Lcom/google/b/c/dx;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/b/c/ax;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/b/c/dx;->c:Lcom/google/b/c/dx;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/google/b/c/ea;-><init>()V

    .line 596
    iput-object p1, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    .line 597
    return-void
.end method

.method static a(Ljava/util/Comparator;)Lcom/google/b/c/dx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lcom/google/b/c/dx;->a:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/google/b/c/dx;->c:Lcom/google/b/c/dx;

    .line 112
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/ax;

    invoke-direct {v0, p0}, Lcom/google/b/c/ax;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static varargs a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/google/b/c/dx;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;I[TE;)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 439
    if-nez p1, :cond_0

    .line 440
    :goto_0
    if-nez v1, :cond_4

    .line 441
    sget-object v0, Lcom/google/b/c/dx;->a:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/b/c/dx;->c:Lcom/google/b/c/dx;

    .line 445
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 439
    :goto_2
    if-ge v2, p1, :cond_1

    aget-object v3, p2, v2

    invoke-static {v3, v2}, Lcom/google/b/c/in;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    invoke-static {p2, v1, p1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    move v2, v0

    move v1, v0

    :goto_3
    if-ge v2, p1, :cond_2

    aget-object v3, p2, v2

    add-int/lit8 v0, v1, -0x1

    aget-object v0, p2, v0

    invoke-interface {p0, v3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v0, v1, 0x1

    aput-object v3, p2, v1

    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    invoke-static {p2, v1, p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    goto :goto_0

    .line 441
    :cond_3
    new-instance v0, Lcom/google/b/c/ax;

    invoke-direct {v0, p0}, Lcom/google/b/c/ax;-><init>(Ljava/util/Comparator;)V

    goto :goto_1

    .line 442
    :cond_4
    array-length v0, p2

    if-ge v1, v0, :cond_5

    .line 443
    invoke-static {p2, v1}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    .line 445
    :cond_5
    new-instance v0, Lcom/google/b/c/jk;

    invoke-static {p2}, Lcom/google/b/c/cv;->b([Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/b/c/jk;-><init>(Lcom/google/b/c/cv;Ljava/util/Comparator;)V

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_4
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 827
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
.end method

.method abstract a(Ljava/lang/Object;Z)Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation
.end method

.method abstract b(Ljava/lang/Object;Z)Lcom/google/b/c/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lcom/google/b/c/dx",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract b()Lcom/google/b/c/lg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TE;>;"
        }
    .end annotation
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public abstract e()Lcom/google/b/c/lg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TE;>;"
        }
    .end annotation
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 737
    invoke-virtual {p0}, Lcom/google/b/c/dx;->b()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/dx;->a(Ljava/lang/Object;Z)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/b/c/dx;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 741
    invoke-virtual {p0}, Lcom/google/b/c/dx;->e()Lcom/google/b/c/lg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/lg;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1, v1, p2, v2}, Lcom/google/b/c/dx;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/dx;->b(Ljava/lang/Object;Z)Lcom/google/b/c/dx;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 832
    new-instance v0, Lcom/google/b/c/dz;

    iget-object v1, p0, Lcom/google/b/c/dx;->d:Ljava/util/Comparator;

    invoke-virtual {p0}, Lcom/google/b/c/dx;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/dz;-><init>(Ljava/util/Comparator;[Ljava/lang/Object;)V

    return-object v0
.end method
