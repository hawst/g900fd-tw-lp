.class Lcom/google/b/c/cz;
.super Lcom/google/b/c/lh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/lh",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/lh;

.field final synthetic b:Lcom/google/b/c/cy;


# direct methods
.method constructor <init>(Lcom/google/b/c/cy;Lcom/google/b/c/lh;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/google/b/c/cz;->b:Lcom/google/b/c/cy;

    iput-object p2, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-direct {p0}, Lcom/google/b/c/lh;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v0}, Lcom/google/b/c/lh;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v0}, Lcom/google/b/c/lh;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v0}, Lcom/google/b/c/lh;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/b/c/cz;->b:Lcom/google/b/c/cy;

    iget-object v1, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v1}, Lcom/google/b/c/lh;->previousIndex()I

    move-result v1

    iget v0, v0, Lcom/google/b/c/cy;->a:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v0}, Lcom/google/b/c/lh;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/b/c/cz;->b:Lcom/google/b/c/cy;

    iget-object v1, p0, Lcom/google/b/c/cz;->a:Lcom/google/b/c/lh;

    invoke-virtual {v1}, Lcom/google/b/c/lh;->nextIndex()I

    move-result v1

    iget v0, v0, Lcom/google/b/c/cy;->a:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    return v0
.end method
