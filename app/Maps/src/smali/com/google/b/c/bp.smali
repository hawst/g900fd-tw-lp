.class Lcom/google/b/c/bp;
.super Lcom/google/b/c/z;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/z",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field a:Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/b/c/bo;


# direct methods
.method constructor <init>(Lcom/google/b/c/bo;Lcom/google/b/c/bm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 443
    iput-object p1, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    invoke-direct {p0}, Lcom/google/b/c/z;-><init>()V

    .line 444
    iput-object p2, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    .line 445
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget-object v0, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget-object v0, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 459
    iget-object v0, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget-object v3, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    .line 460
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v4

    .line 461
    iget-object v0, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget v0, v0, Lcom/google/b/c/bm;->d:I

    if-ne v4, v0, :cond_3

    if-eq p1, v3, :cond_0

    if-eqz p1, :cond_2

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    .line 474
    :goto_2
    return-object p1

    .line 460
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 461
    goto :goto_1

    .line 464
    :cond_3
    iget-object v0, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v0, v0, Lcom/google/b/c/bo;->a:Lcom/google/b/c/bn;

    iget-object v0, v0, Lcom/google/b/c/bn;->a:Lcom/google/b/c/bl;

    invoke-virtual {v0, p1, v4}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    const-string v5, "value already present: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5, v2}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v1

    goto :goto_3

    .line 465
    :cond_5
    iget-object v0, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v0, v0, Lcom/google/b/c/bo;->a:Lcom/google/b/c/bn;

    iget-object v0, v0, Lcom/google/b/c/bn;->a:Lcom/google/b/c/bl;

    iget-object v1, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    invoke-static {v0, v1}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bl;Lcom/google/b/c/bm;)V

    .line 466
    new-instance v0, Lcom/google/b/c/bm;

    iget-object v1, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget-object v1, v1, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    iget v2, v2, Lcom/google/b/c/bm;->b:I

    invoke-direct {v0, v1, v2, p1, v4}, Lcom/google/b/c/bm;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 468
    iget-object v1, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v1, v1, Lcom/google/b/c/bo;->a:Lcom/google/b/c/bn;

    iget-object v1, v1, Lcom/google/b/c/bn;->a:Lcom/google/b/c/bl;

    invoke-virtual {v1, v0}, Lcom/google/b/c/bl;->b(Lcom/google/b/c/bm;)V

    .line 469
    iget-object v1, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v2, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v2, v2, Lcom/google/b/c/bo;->a:Lcom/google/b/c/bn;

    iget-object v2, v2, Lcom/google/b/c/bn;->a:Lcom/google/b/c/bl;

    iget v2, v2, Lcom/google/b/c/bl;->c:I

    iput v2, v1, Lcom/google/b/c/bo;->e:I

    .line 470
    iget-object v1, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iget-object v1, v1, Lcom/google/b/c/bo;->d:Lcom/google/b/c/bm;

    iget-object v2, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    if-ne v1, v2, :cond_6

    .line 471
    iget-object v1, p0, Lcom/google/b/c/bp;->b:Lcom/google/b/c/bo;

    iput-object v0, v1, Lcom/google/b/c/bo;->d:Lcom/google/b/c/bm;

    .line 473
    :cond_6
    iput-object v0, p0, Lcom/google/b/c/bp;->a:Lcom/google/b/c/bm;

    move-object p1, v3

    .line 474
    goto :goto_2
.end method
