.class Lcom/google/b/c/cy;
.super Lcom/google/b/c/cv;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/cv",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final transient a:I

.field private final transient c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/cv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 485
    invoke-direct {p0}, Lcom/google/b/c/cv;-><init>()V

    .line 486
    iput-object p1, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    .line 487
    invoke-virtual {p1}, Lcom/google/b/c/cv;->size()I

    move-result v0

    iput v0, p0, Lcom/google/b/c/cy;->a:I

    .line 488
    return-void
.end method


# virtual methods
.method public final a(II)Lcom/google/b/c/cv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 527
    iget v0, p0, Lcom/google/b/c/cy;->a:I

    invoke-static {p1, p2, v0}, Lcom/google/b/a/aq;->a(III)V

    .line 528
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/cy;->a:I

    sub-int/2addr v1, p2

    iget v2, p0, Lcom/google/b/c/cy;->a:I

    sub-int/2addr v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cv;->a(II)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->ar_()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/b/c/lh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/b/c/lh",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 538
    iget v0, p0, Lcom/google/b/c/cy;->a:I

    invoke-static {p1, v0}, Lcom/google/b/a/aq;->b(II)I

    .line 539
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/cy;->a:I

    sub-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->a(I)Lcom/google/b/c/lh;

    move-result-object v0

    .line 540
    new-instance v1, Lcom/google/b/c/cz;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/cz;-><init>(Lcom/google/b/c/cy;Lcom/google/b/c/lh;)V

    return-object v1
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->a()Z

    move-result v0

    return v0
.end method

.method public final ar_()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 532
    iget v0, p0, Lcom/google/b/c/cy;->a:I

    invoke-static {p1, v0}, Lcom/google/b/a/aq;->a(II)I

    .line 533
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/b/c/cy;->a:I

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 516
    if-ltz v0, :cond_0

    iget v1, p0, Lcom/google/b/c/cy;->a:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 481
    invoke-super {p0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/b/c/cy;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 522
    if-ltz v0, :cond_0

    iget v1, p0, Lcom/google/b/c/cy;->a:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 481
    invoke-super {p0}, Lcom/google/b/c/cv;->d()Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0, p1}, Lcom/google/b/c/cy;->a(I)Lcom/google/b/c/lh;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 568
    iget v0, p0, Lcom/google/b/c/cy;->a:I

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0, p1, p2}, Lcom/google/b/c/cy;->a(II)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method
