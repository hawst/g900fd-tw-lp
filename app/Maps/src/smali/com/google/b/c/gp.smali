.class final Lcom/google/b/c/gp;
.super Lcom/google/b/c/fr;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/fr",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x3L


# direct methods
.method constructor <init>(Lcom/google/b/c/gr;Lcom/google/b/c/gr;Lcom/google/b/a/x;Lcom/google/b/a/x;JJIILcom/google/b/c/fl;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gr;",
            "Lcom/google/b/c/gr;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;JJII",
            "Lcom/google/b/c/fl",
            "<-TK;-TV;>;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3879
    invoke-direct/range {p0 .. p12}, Lcom/google/b/c/fr;-><init>(Lcom/google/b/c/gr;Lcom/google/b/c/gr;Lcom/google/b/a/x;Lcom/google/b/a/x;JJIILcom/google/b/c/fl;Ljava/util/concurrent/ConcurrentMap;)V

    .line 3881
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3889
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 3890
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v3

    new-instance v4, Lcom/google/b/c/fd;

    invoke-direct {v4}, Lcom/google/b/c/fd;-><init>()V

    iget v0, v4, Lcom/google/b/c/fd;->c:I

    if-ne v0, v12, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "initial capacity was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v4, Lcom/google/b/c/fd;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-ltz v3, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput v3, v4, Lcom/google/b/c/fd;->c:I

    iget-object v0, p0, Lcom/google/b/c/fr;->a:Lcom/google/b/c/gr;

    invoke-virtual {v4, v0}, Lcom/google/b/c/fd;->a(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    iget-object v3, p0, Lcom/google/b/c/fr;->b:Lcom/google/b/c/gr;

    invoke-virtual {v0, v3}, Lcom/google/b/c/fd;->b(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v4

    iget-object v0, p0, Lcom/google/b/c/fr;->c:Lcom/google/b/a/x;

    iget-object v3, v4, Lcom/google/b/c/fd;->k:Lcom/google/b/a/x;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    const-string v5, "key equivalence was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v4, Lcom/google/b/c/fd;->k:Lcom/google/b/a/x;

    aput-object v7, v6, v2

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, v4, Lcom/google/b/c/fd;->k:Lcom/google/b/a/x;

    iput-boolean v1, v4, Lcom/google/b/c/fd;->b:Z

    iget v0, p0, Lcom/google/b/c/fr;->g:I

    invoke-virtual {v4, v0}, Lcom/google/b/c/fd;->a(I)Lcom/google/b/c/fd;

    move-result-object v4

    iget-object v0, p0, Lcom/google/b/c/fr;->h:Lcom/google/b/c/fl;

    iget-object v3, v4, Lcom/google/b/c/fd;->a:Lcom/google/b/c/fl;

    if-nez v3, :cond_7

    move v3, v1

    :goto_3
    if-nez v3, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_7
    move v3, v2

    goto :goto_3

    :cond_8
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Lcom/google/b/c/fl;

    iput-object v0, v4, Lcom/google/b/c/bh;->a:Lcom/google/b/c/fl;

    iput-boolean v1, v4, Lcom/google/b/c/fd;->b:Z

    iget-wide v6, p0, Lcom/google/b/c/fr;->d:J

    cmp-long v0, v6, v10

    if-lez v0, :cond_b

    iget-wide v6, p0, Lcom/google/b/c/fr;->d:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Lcom/google/b/c/fd;->a(JLjava/util/concurrent/TimeUnit;)V

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/google/b/c/fd;->h:J

    cmp-long v0, v6, v10

    if-nez v0, :cond_a

    iget-object v0, v4, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/b/c/ff;->d:Lcom/google/b/c/ff;

    iput-object v0, v4, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    :cond_a
    iput-boolean v1, v4, Lcom/google/b/c/fd;->b:Z

    :cond_b
    iget-wide v6, p0, Lcom/google/b/c/fr;->e:J

    cmp-long v0, v6, v10

    if-lez v0, :cond_d

    iget-wide v6, p0, Lcom/google/b/c/fr;->e:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Lcom/google/b/c/fd;->a(JLjava/util/concurrent/TimeUnit;)V

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/google/b/c/fd;->i:J

    cmp-long v0, v6, v10

    if-nez v0, :cond_c

    iget-object v0, v4, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/b/c/ff;->d:Lcom/google/b/c/ff;

    iput-object v0, v4, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    :cond_c
    iput-boolean v1, v4, Lcom/google/b/c/fd;->b:Z

    :cond_d
    iget v0, p0, Lcom/google/b/c/fr;->f:I

    if-eq v0, v12, :cond_12

    iget v3, p0, Lcom/google/b/c/fr;->f:I

    iget v0, v4, Lcom/google/b/c/fd;->e:I

    if-ne v0, v12, :cond_e

    move v0, v1

    :goto_4
    const-string v5, "maximum size was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v4, Lcom/google/b/c/fd;->e:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    move v0, v2

    goto :goto_4

    :cond_f
    if-ltz v3, :cond_10

    move v2, v1

    :cond_10
    const-string v0, "maximum size must not be negative"

    if-nez v2, :cond_11

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    iput v3, v4, Lcom/google/b/c/fd;->e:I

    iput-boolean v1, v4, Lcom/google/b/c/fd;->b:Z

    iget v0, v4, Lcom/google/b/c/fd;->e:I

    if-nez v0, :cond_12

    sget-object v0, Lcom/google/b/c/ff;->e:Lcom/google/b/c/ff;

    iput-object v0, v4, Lcom/google/b/c/fd;->j:Lcom/google/b/c/ff;

    .line 3891
    :cond_12
    invoke-virtual {v4}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gp;->i:Ljava/util/concurrent/ConcurrentMap;

    .line 3892
    :goto_5
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 3893
    :cond_13
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3896
    iget-object v0, p0, Lcom/google/b/c/gp;->i:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3

    .prologue
    .line 3884
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 3885
    iget-object v0, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/b/c/fr;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 3886
    return-void
.end method
