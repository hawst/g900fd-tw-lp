.class Lcom/google/b/c/kn;
.super Lcom/google/b/c/kx;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/kl",
        "<TR;TC;TV;>.com/google/b/c/kx<",
        "Lcom/google/b/c/kz",
        "<TR;TC;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/kl;


# direct methods
.method constructor <init>(Lcom/google/b/c/kl;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/b/c/kn;->a:Lcom/google/b/c/kl;

    invoke-direct {p0, p1}, Lcom/google/b/c/kx;-><init>(Lcom/google/b/c/kl;)V

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 294
    instance-of v1, p1, Lcom/google/b/c/kz;

    if-eqz v1, :cond_0

    .line 295
    check-cast p1, Lcom/google/b/c/kz;

    .line 296
    iget-object v1, p0, Lcom/google/b/c/kn;->a:Lcom/google/b/c/kl;

    invoke-interface {p1}, Lcom/google/b/c/kz;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/b/c/kz;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/b/c/kz;->c()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v2, v3}, Lcom/google/b/c/kl;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 298
    :cond_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/b/c/kz",
            "<TR;TC;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 284
    new-instance v0, Lcom/google/b/c/km;

    iget-object v1, p0, Lcom/google/b/c/kn;->a:Lcom/google/b/c/kl;

    invoke-direct {v0, v1}, Lcom/google/b/c/km;-><init>(Lcom/google/b/c/kl;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 303
    instance-of v2, p1, Lcom/google/b/c/kz;

    if-eqz v2, :cond_2

    .line 304
    check-cast p1, Lcom/google/b/c/kz;

    .line 305
    iget-object v3, p0, Lcom/google/b/c/kn;->a:Lcom/google/b/c/kl;

    invoke-interface {p1}, Lcom/google/b/c/kz;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/b/c/kz;->b()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p1}, Lcom/google/b/c/kz;->c()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v3, v4, v5}, Lcom/google/b/c/kl;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v3, v4, v5}, Lcom/google/b/c/kl;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 305
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 307
    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/b/c/kn;->a:Lcom/google/b/c/kl;

    invoke-virtual {v0}, Lcom/google/b/c/kl;->e()I

    move-result v0

    return v0
.end method
