.class abstract Lcom/google/b/c/df;
.super Lcom/google/b/c/dn;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dn",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/b/c/dn;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/b/c/df;->e()Lcom/google/b/c/dc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dc;->f()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 45
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 46
    check-cast p1, Ljava/util/Map$Entry;

    .line 47
    invoke-virtual {p0}, Lcom/google/b/c/df;->e()Lcom/google/b/c/dc;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/b/c/dc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 48
    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 50
    :cond_0
    return v0
.end method

.method abstract e()Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dc",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public size()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/b/c/df;->e()Lcom/google/b/c/dc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dc;->size()I

    move-result v0

    return v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/b/c/dg;

    invoke-virtual {p0}, Lcom/google/b/c/df;->e()Lcom/google/b/c/dc;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/c/dg;-><init>(Lcom/google/b/c/dc;)V

    return-object v0
.end method
