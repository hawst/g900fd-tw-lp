.class Lcom/google/b/c/g;
.super Ljava/util/AbstractMap;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field transient b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation
.end field

.field final synthetic c:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1043
    iput-object p1, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 1044
    iput-object p2, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    .line 1045
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    iget-object v1, v1, Lcom/google/b/c/f;->a:Ljava/util/Map;

    if-ne v0, v1, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-virtual {v0}, Lcom/google/b/c/f;->f()V

    .line 1120
    :goto_0
    return-void

    .line 1118
    :cond_0
    new-instance v0, Lcom/google/b/c/i;

    invoke-direct {v0, p0}, Lcom/google/b/c/i;-><init>(Lcom/google/b/c/g;)V

    invoke-static {v0}, Lcom/google/b/c/eg;->f(Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/b/c/g;->b:Ljava/util/Set;

    .line 1052
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/h;

    invoke-direct {v0, p0}, Lcom/google/b/c/h;-><init>(Lcom/google/b/c/g;)V

    iput-object v0, p0, Lcom/google/b/c/g;->b:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1099
    if-eq p0, p1, :cond_0

    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/b/c/hj;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/f;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-virtual {v0}, Lcom/google/b/c/f;->l()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-virtual {v1}, Lcom/google/b/c/f;->c()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/b/c/g;->c:Lcom/google/b/c/f;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    iget v4, v2, Lcom/google/b/c/f;->b:I

    sub-int v3, v4, v3

    iput v3, v2, Lcom/google/b/c/f;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    move-object v0, v1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/google/b/c/g;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
