.class Lcom/google/b/c/de;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final a:[Ljava/lang/Object;

.field private final b:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/b/c/dc;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/dc",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 518
    invoke-virtual {p1}, Lcom/google/b/c/dc;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/c/de;->a:[Ljava/lang/Object;

    .line 519
    invoke-virtual {p1}, Lcom/google/b/c/dc;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/c/de;->b:[Ljava/lang/Object;

    .line 520
    const/4 v0, 0x0

    .line 521
    invoke-virtual {p1}, Lcom/google/b/c/dc;->c()Lcom/google/b/c/dn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/c/dn;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 522
    iget-object v3, p0, Lcom/google/b/c/de;->a:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 523
    iget-object v3, p0, Lcom/google/b/c/de;->b:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v1

    .line 524
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 525
    goto :goto_0

    .line 526
    :cond_0
    return-void
.end method


# virtual methods
.method final a(Lcom/google/b/c/dd;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/dd",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 534
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/b/c/de;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/b/c/de;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/b/c/de;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    .line 534
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 537
    :cond_0
    invoke-virtual {p1}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v0

    return-object v0
.end method

.method readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 529
    new-instance v0, Lcom/google/b/c/dd;

    invoke-direct {v0}, Lcom/google/b/c/dd;-><init>()V

    .line 530
    invoke-virtual {p0, v0}, Lcom/google/b/c/de;->a(Lcom/google/b/c/dd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
