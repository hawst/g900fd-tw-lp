.class public abstract enum Lcom/google/b/c/kf;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/kf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/kf;

.field public static final enum b:Lcom/google/b/c/kf;

.field public static final enum c:Lcom/google/b/c/kf;

.field public static final enum d:Lcom/google/b/c/kf;

.field public static final enum e:Lcom/google/b/c/kf;

.field private static final synthetic f:[Lcom/google/b/c/kf;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/google/b/c/kg;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/kg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kf;->a:Lcom/google/b/c/kf;

    .line 65
    new-instance v0, Lcom/google/b/c/kh;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lcom/google/b/c/kh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kf;->b:Lcom/google/b/c/kf;

    .line 90
    new-instance v0, Lcom/google/b/c/ki;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lcom/google/b/c/ki;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    .line 117
    new-instance v0, Lcom/google/b/c/kj;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Lcom/google/b/c/kj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    .line 129
    new-instance v0, Lcom/google/b/c/kk;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lcom/google/b/c/kk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kf;->e:Lcom/google/b/c/kf;

    .line 49
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/c/kf;

    sget-object v1, Lcom/google/b/c/kf;->a:Lcom/google/b/c/kf;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/c/kf;->b:Lcom/google/b/c/kf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/c/kf;->c:Lcom/google/b/c/kf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/c/kf;->d:Lcom/google/b/c/kf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/c/kf;->e:Lcom/google/b/c/kf;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/c/kf;->f:[Lcom/google/b/c/kf;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/kf;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/b/c/kf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/kf;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/kf;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/b/c/kf;->f:[Lcom/google/b/c/kf;

    invoke-virtual {v0}, [Lcom/google/b/c/kf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/kf;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;",
            "Ljava/util/List",
            "<+TE;>;I)I"
        }
    .end annotation
.end method
