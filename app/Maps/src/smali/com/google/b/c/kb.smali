.class public abstract enum Lcom/google/b/c/kb;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/c/kb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/c/kb;

.field public static final enum b:Lcom/google/b/c/kb;

.field public static final enum c:Lcom/google/b/c/kb;

.field private static final synthetic d:[Lcom/google/b/c/kb;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    new-instance v0, Lcom/google/b/c/kc;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/kc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kb;->a:Lcom/google/b/c/kb;

    .line 161
    new-instance v0, Lcom/google/b/c/kd;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Lcom/google/b/c/kd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    .line 180
    new-instance v0, Lcom/google/b/c/ke;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lcom/google/b/c/ke;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/c/kb;->c:Lcom/google/b/c/kb;

    .line 145
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/c/kb;

    sget-object v1, Lcom/google/b/c/kb;->a:Lcom/google/b/c/kb;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/c/kb;->b:Lcom/google/b/c/kb;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/c/kb;->c:Lcom/google/b/c/kb;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/c/kb;->d:[Lcom/google/b/c/kb;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/c/kb;
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/google/b/c/kb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/kb;

    return-object v0
.end method

.method public static values()[Lcom/google/b/c/kb;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/google/b/c/kb;->d:[Lcom/google/b/c/kb;

    invoke-virtual {v0}, [Lcom/google/b/c/kb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/c/kb;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method
