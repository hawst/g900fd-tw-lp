.class Lcom/google/b/c/go;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/c/fn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/fn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:I

.field c:I

.field d:I

.field volatile e:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final f:I

.field final g:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;"
        }
    .end annotation
.end field

.field final h:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field

.field final i:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final j:Ljava/util/concurrent/atomic/AtomicInteger;

.field final k:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/fn;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/fn",
            "<TK;TV;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1999
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 1983
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2000
    iput-object p1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    .line 2001
    iput p3, p0, Lcom/google/b/c/go;->f:I

    .line 2002
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/b/c/go;->d:I

    iget v4, p0, Lcom/google/b/c/go;->d:I

    iget v5, p0, Lcom/google/b/c/go;->f:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/google/b/c/go;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/c/go;->d:I

    :cond_0
    iput-object v0, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2004
    iget-object v0, p1, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    sget-object v4, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v4, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_1
    iput-object v0, p0, Lcom/google/b/c/go;->g:Ljava/lang/ref/ReferenceQueue;

    .line 2006
    iget-object v0, p1, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    sget-object v4, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v4, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_1
    iput-object v1, p0, Lcom/google/b/c/go;->h:Ljava/lang/ref/ReferenceQueue;

    .line 2008
    iget v0, p1, Lcom/google/b/c/fn;->j:I

    if-eq v0, v6, :cond_6

    move v0, v2

    :goto_3
    if-nez v0, :cond_2

    iget-wide v0, p1, Lcom/google/b/c/fn;->k:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_7

    move v0, v2

    :goto_4
    if-eqz v0, :cond_8

    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_5
    iput-object v0, p0, Lcom/google/b/c/go;->i:Ljava/util/Queue;

    .line 2011
    iget v0, p1, Lcom/google/b/c/fn;->j:I

    if-eq v0, v6, :cond_9

    move v0, v2

    :goto_6
    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/b/c/gd;

    invoke-direct {v0}, Lcom/google/b/c/gd;-><init>()V

    :goto_7
    iput-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    .line 2014
    invoke-virtual {p1}, Lcom/google/b/c/fn;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/b/c/gg;

    invoke-direct {v0}, Lcom/google/b/c/gg;-><init>()V

    :goto_8
    iput-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    .line 2016
    return-void

    :cond_3
    move v0, v3

    .line 2004
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move v0, v3

    .line 2006
    goto :goto_2

    :cond_6
    move v0, v3

    .line 2008
    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4

    :cond_8
    invoke-static {}, Lcom/google/b/c/fn;->d()Ljava/util/Queue;

    move-result-object v0

    goto :goto_5

    :cond_9
    move v0, v3

    .line 2011
    goto :goto_6

    :cond_a
    invoke-static {}, Lcom/google/b/c/fn;->d()Ljava/util/Queue;

    move-result-object v0

    goto :goto_7

    .line 2014
    :cond_b
    invoke-static {}, Lcom/google/b/c/fn;->d()Ljava/util/Queue;

    move-result-object v0

    goto :goto_8
.end method

.method private a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2042
    invoke-interface {p1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2056
    :cond_0
    :goto_0
    return-object v0

    .line 2047
    :cond_1
    invoke-interface {p1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v1

    .line 2048
    invoke-interface {v1}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2049
    if-nez v2, :cond_2

    invoke-interface {v1}, Lcom/google/b/c/hb;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2054
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->o:Lcom/google/b/c/fs;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/b/c/fs;->a(Lcom/google/b/c/go;Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2055
    iget-object v3, p0, Lcom/google/b/c/go;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/b/c/hb;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/b/c/gn;)Lcom/google/b/c/hb;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/b/c/gn;->a(Lcom/google/b/c/hb;)V

    goto :goto_0
.end method

.method private a(Lcom/google/b/c/gn;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2172
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v0, v0, Lcom/google/b/c/fn;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2173
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v0, v0, Lcom/google/b/c/fn;->k:J

    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v2}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-interface {p1, v0, v1}, Lcom/google/b/c/gn;->a(J)V

    .line 2174
    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2176
    :cond_0
    return-void

    .line 2172
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/b/c/gn;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 2064
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/b/c/gr;->a(Lcom/google/b/c/go;Lcom/google/b/c/gn;Ljava/lang/Object;)Lcom/google/b/c/hb;

    move-result-object v0

    .line 2065
    invoke-interface {p1, v0}, Lcom/google/b/c/gn;->a(Lcom/google/b/c/hb;)V

    .line 2066
    invoke-direct {p0}, Lcom/google/b/c/go;->e()V

    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-virtual {v0}, Lcom/google/b/c/fn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v0, v0, Lcom/google/b/c/fn;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v0, v0, Lcom/google/b/c/fn;->k:J

    :goto_1
    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v2}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-interface {p1, v0, v1}, Lcom/google/b/c/gn;->a(J)V

    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2067
    :cond_0
    return-void

    .line 2066
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v0, v0, Lcom/google/b/c/fn;->l:J

    goto :goto_1
.end method

.method private a(Lcom/google/b/c/gn;ILcom/google/b/c/ff;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;I",
            "Lcom/google/b/c/ff;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2893
    iget v0, p0, Lcom/google/b/c/go;->b:I

    .line 2894
    iget-object v2, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2895
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 2896
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v1, v0

    .line 2898
    :goto_0
    if-eqz v1, :cond_1

    .line 2899
    if-ne v1, p1, :cond_0

    .line 2900
    iget v4, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/c/go;->c:I

    .line 2901
    invoke-interface {v1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v4, v5, p3}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2902
    invoke-direct {p0, v0, v1}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2903
    iget v1, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2904
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2905
    iput v1, p0, Lcom/google/b/c/go;->b:I

    .line 2906
    const/4 v0, 0x1

    .line 2910
    :goto_1
    return v0

    .line 2898
    :cond_0
    invoke-interface {v1}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v1

    goto :goto_0

    .line 2910
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2767
    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2768
    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2770
    iget v2, p0, Lcom/google/b/c/go;->b:I

    .line 2771
    invoke-interface {p2}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v1

    .line 2772
    :goto_0
    if-eq p1, p2, :cond_1

    .line 2773
    invoke-direct {p0, p1, v1}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2774
    if-eqz v0, :cond_0

    move v1, v2

    .line 2772
    :goto_1
    invoke-interface {p1}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2777
    :cond_0
    sget-object v0, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-interface {p1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/b/c/gn;->c()I

    invoke-interface {p1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2778
    add-int/lit8 v0, v2, -0x1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_1

    .line 2781
    :cond_1
    iput v2, p0, Lcom/google/b/c/go;->b:I

    .line 2782
    return-object v1
.end method

.method private d(Ljava/lang/Object;I)Lcom/google/b/c/gn;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2304
    iget v0, p0, Lcom/google/b/c/go;->b:I

    if-eqz v0, :cond_2

    .line 2305
    iget-object v0, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    :goto_0
    if-eqz v0, :cond_2

    .line 2306
    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 2307
    invoke-interface {v0}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v1

    .line 2311
    if-nez v1, :cond_1

    .line 2312
    invoke-virtual {p0}, Lcom/google/b/c/go;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2305
    :cond_0
    invoke-interface {v0}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v0

    goto :goto_0

    .line 2312
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    throw v0

    .line 2316
    :cond_1
    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v2, p1, v1}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2322
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e(Ljava/lang/Object;I)Lcom/google/b/c/gn;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2326
    invoke-direct {p0, p1, p2}, Lcom/google/b/c/go;->d(Ljava/lang/Object;I)Lcom/google/b/c/gn;

    move-result-object v1

    .line 2327
    if-nez v1, :cond_1

    .line 2333
    :cond_0
    :goto_0
    return-object v0

    .line 2329
    :cond_1
    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-virtual {v2}, Lcom/google/b/c/fn;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v2}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    invoke-interface {v1}, Lcom/google/b/c/gn;->e()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 2330
    invoke-virtual {p0}, Lcom/google/b/c/go;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    goto :goto_0

    .line 2329
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 2330
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    throw v0

    :cond_3
    move-object v0, v1

    .line 2333
    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2206
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/b/c/go;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    if-eqz v0, :cond_3

    .line 2211
    iget-object v1, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2212
    iget-object v1, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2214
    :cond_1
    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v2, v1, Lcom/google/b/c/fn;->k:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2215
    iget-object v1, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2214
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 2218
    :cond_3
    return-void
.end method

.method private f()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2280
    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget v2, v2, Lcom/google/b/c/fn;->j:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/b/c/go;->b:I

    iget v3, p0, Lcom/google/b/c/go;->f:I

    if-lt v2, v3, :cond_2

    .line 2281
    invoke-direct {p0}, Lcom/google/b/c/go;->e()V

    .line 2283
    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    .line 2284
    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    move-result v2

    sget-object v3, Lcom/google/b/c/ff;->e:Lcom/google/b/c/ff;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;ILcom/google/b/c/ff;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2285
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v2, v0

    .line 2280
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2289
    :cond_2
    return v0
.end method


# virtual methods
.method final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    .line 2338
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/b/c/go;->e(Ljava/lang/Object;I)Lcom/google/b/c/gn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2339
    if-nez v2, :cond_2

    .line 2340
    iget-object v0, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_0
    const/4 v0, 0x0

    .line 2351
    :cond_1
    :goto_0
    return-object v0

    .line 2343
    :cond_2
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2344
    if-eqz v0, :cond_6

    .line 2345
    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v4, v1, Lcom/google/b/c/fn;->k:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-wide v4, v1, Lcom/google/b/c/fn;->k:J

    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v1, v1, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-interface {v2, v4, v5}, Lcom/google/b/c/gn;->a(J)V

    :cond_3
    iget-object v1, p0, Lcom/google/b/c/go;->i:Ljava/util/Queue;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2351
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_0

    .line 2345
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 2347
    :cond_6
    :try_start_2
    invoke-virtual {p0}, Lcom/google/b/c/go;->tryLock()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {p0}, Lcom/google/b/c/go;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 2351
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_7
    throw v0

    .line 2347
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2592
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2594
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    .line 2596
    iget-object v5, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2597
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, p2, v0

    .line 2598
    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v4, v0

    .line 2600
    :goto_0
    if-eqz v4, :cond_5

    .line 2601
    invoke-interface {v4}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v7

    .line 2602
    invoke-interface {v4}, Lcom/google/b/c/gn;->c()I

    move-result v2

    if-ne v2, p2, :cond_4

    if-eqz v7, :cond_4

    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v2, p1, v7}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2606
    invoke-interface {v4}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v8

    .line 2607
    invoke-interface {v8}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2608
    if-nez v2, :cond_3

    .line 2609
    invoke-interface {v8}, Lcom/google/b/c/hb;->b()Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_0
    :goto_1
    if-eqz v3, :cond_1

    .line 2610
    iget v3, p0, Lcom/google/b/c/go;->b:I

    .line 2611
    iget v3, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/c/go;->c:I

    .line 2612
    sget-object v3, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-virtual {p0, v7, v2, v3}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2613
    invoke-direct {p0, v0, v4}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2614
    iget v2, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v2, v2, -0x1

    .line 2615
    invoke-virtual {v5, v6, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2616
    iput v2, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2618
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2631
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v1

    :goto_2
    return-object v0

    .line 2609
    :cond_2
    :try_start_1
    invoke-interface {v8}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    .line 2621
    :cond_3
    iget v0, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/go;->c:I

    .line 2622
    sget-object v0, Lcom/google/b/c/ff;->b:Lcom/google/b/c/ff;

    invoke-virtual {p0, p1, v2, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2623
    invoke-direct {p0, v4, p3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2630
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2631
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v2

    goto :goto_2

    .line 2600
    :cond_4
    :try_start_2
    invoke-interface {v4}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v4, v2

    goto :goto_0

    .line 2628
    :cond_5
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2631
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v1

    goto :goto_2

    .line 2630
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2631
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    .prologue
    .line 2401
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2403
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    .line 2405
    iget v0, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 2406
    iget v0, p0, Lcom/google/b/c/go;->d:I

    if-le v1, v0, :cond_5

    .line 2407
    iget-object v7, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000    # 2.0f

    if-ge v8, v0, :cond_4

    iget v1, p0, Lcom/google/b/c/go;->b:I

    shl-int/lit8 v0, v8, 0x1

    new-instance v9, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/c/go;->d:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_3

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    if-eqz v0, :cond_e

    invoke-interface {v0}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    move-result v2

    and-int v3, v2, v10

    if-nez v4, :cond_0

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v5, v0

    :goto_2
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/google/b/c/gn;->c()I

    move-result v2

    and-int/2addr v2, v10

    if-eq v2, v3, :cond_d

    move-object v3, v4

    :goto_3
    invoke-interface {v4}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v4

    move-object v5, v3

    move v3, v2

    goto :goto_2

    :cond_1
    invoke-virtual {v9, v3, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    :goto_4
    if-eq v2, v5, :cond_e

    invoke-interface {v2}, Lcom/google/b/c/gn;->c()I

    move-result v0

    and-int v3, v0, v10

    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    invoke-direct {p0, v2, v0}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    :goto_5
    invoke-interface {v2}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    :cond_2
    sget-object v0, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-interface {v2}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/b/c/gn;->c()I

    invoke-interface {v2}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    iget-object v0, p0, Lcom/google/b/c/go;->k:Ljava/util/Queue;

    invoke-interface {v0, v2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0, v2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    :cond_3
    iput-object v9, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v1, p0, Lcom/google/b/c/go;->b:I

    .line 2408
    :cond_4
    iget v0, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 2411
    :cond_5
    iget-object v3, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2412
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2413
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v2, v0

    .line 2416
    :goto_6
    if-eqz v2, :cond_b

    .line 2417
    invoke-interface {v2}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2418
    invoke-interface {v2}, Lcom/google/b/c/gn;->c()I

    move-result v6

    if-ne v6, p2, :cond_a

    if-eqz v5, :cond_a

    iget-object v6, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v6, v6, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v6, p1, v5}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2422
    invoke-interface {v2}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v3

    .line 2423
    invoke-interface {v3}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2425
    if-nez v0, :cond_8

    .line 2426
    iget v4, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/c/go;->c:I

    .line 2427
    invoke-direct {p0, v2, p3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Ljava/lang/Object;)V

    .line 2428
    invoke-interface {v3}, Lcom/google/b/c/hb;->b()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2429
    sget-object v1, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2430
    iget v1, p0, Lcom/google/b/c/go;->b:I

    .line 2434
    :cond_6
    :goto_7
    iput v1, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2435
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2464
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    const/4 v0, 0x0

    :goto_8
    return-object v0

    .line 2431
    :cond_7
    :try_start_1
    invoke-direct {p0}, Lcom/google/b/c/go;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2432
    iget v0, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v0, 0x1

    goto :goto_7

    .line 2436
    :cond_8
    if-eqz p4, :cond_9

    .line 2440
    invoke-direct {p0, v2}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2463
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2464
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_8

    .line 2444
    :cond_9
    :try_start_2
    iget v1, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/c/go;->c:I

    .line 2445
    sget-object v1, Lcom/google/b/c/ff;->b:Lcom/google/b/c/ff;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2446
    invoke-direct {p0, v2, p3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2463
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2464
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_8

    .line 2416
    :cond_a
    :try_start_3
    invoke-interface {v2}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;

    move-result-object v2

    goto :goto_6

    .line 2453
    :cond_b
    iget v2, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/c/go;->c:I

    .line 2454
    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->o:Lcom/google/b/c/fs;

    invoke-virtual {v2, p0, p1, p2, v0}, Lcom/google/b/c/fs;->a(Lcom/google/b/c/go;Ljava/lang/Object;ILcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2455
    invoke-direct {p0, v0, p3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Ljava/lang/Object;)V

    .line 2456
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2457
    invoke-direct {p0}, Lcom/google/b/c/go;->f()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2458
    iget v0, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2460
    :goto_9
    iput v0, p0, Lcom/google/b/c/go;->b:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2461
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2464
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    const/4 v0, 0x0

    goto :goto_8

    .line 2463
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2464
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0

    :cond_c
    move v0, v1

    goto :goto_9

    :cond_d
    move v2, v3

    move-object v3, v5

    goto/16 :goto_3

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method final a()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2090
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->h:Lcom/google/b/c/gr;

    sget-object v1, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    move v1, v2

    .line 2091
    :goto_1
    iget-object v0, p0, Lcom/google/b/c/go;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/google/b/c/gn;

    iget-object v4, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    move-result v5

    iget-object v6, v4, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v7, v4, Lcom/google/b/c/fn;->c:I

    ushr-int v7, v5, v7

    iget v4, v4, Lcom/google/b/c/fn;->b:I

    and-int/2addr v4, v7

    aget-object v4, v6, v4

    invoke-virtual {v4, v0, v5}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;I)Z

    add-int/lit8 v0, v1, 0x1

    if-eq v0, v8, :cond_1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2090
    goto :goto_0

    .line 2093
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->i:Lcom/google/b/c/gr;

    sget-object v1, Lcom/google/b/c/gr;->a:Lcom/google/b/c/gr;

    if-eq v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 2094
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/go;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lcom/google/b/c/hb;

    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-interface {v0}, Lcom/google/b/c/hb;->a()Lcom/google/b/c/gn;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/b/c/gn;->c()I

    move-result v4

    iget-object v5, v1, Lcom/google/b/c/fn;->d:[Lcom/google/b/c/go;

    iget v6, v1, Lcom/google/b/c/fn;->c:I

    ushr-int v6, v4, v6

    iget v1, v1, Lcom/google/b/c/fn;->b:I

    and-int/2addr v1, v6

    aget-object v1, v5, v1

    invoke-interface {v3}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;ILcom/google/b/c/hb;)Z

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v8, :cond_2

    .line 2096
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 2093
    goto :goto_2
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Lcom/google/b/c/ff;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2266
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->m:Ljava/util/Queue;

    sget-object v1, Lcom/google/b/c/fn;->r:Ljava/util/Queue;

    if-eq v0, v1, :cond_0

    .line 2267
    new-instance v0, Lcom/google/b/c/fm;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/b/c/fm;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2268
    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v1, v1, Lcom/google/b/c/fn;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2270
    :cond_0
    return-void
.end method

.method final a(Lcom/google/b/c/gn;I)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;I)Z"
        }
    .end annotation

    .prologue
    .line 2795
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2797
    :try_start_0
    iget v0, p0, Lcom/google/b/c/go;->b:I

    .line 2798
    iget-object v2, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2799
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 2800
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v1, v0

    .line 2802
    :goto_0
    if-eqz v1, :cond_1

    .line 2803
    if-ne v1, p1, :cond_0

    .line 2804
    iget v4, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/b/c/go;->c:I

    .line 2805
    invoke-interface {v1}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2807
    invoke-direct {p0, v0, v1}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2808
    iget v1, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2809
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2810
    iput v1, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2811
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2818
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2802
    :cond_0
    :try_start_1
    invoke-interface {v1}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 2815
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2818
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    const/4 v0, 0x0

    goto :goto_1

    .line 2817
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2818
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILcom/google/b/c/hb;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/c/hb",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2826
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2828
    :try_start_0
    iget v0, p0, Lcom/google/b/c/go;->b:I

    .line 2829
    iget-object v3, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2830
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2831
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v2, v0

    .line 2833
    :goto_0
    if-eqz v2, :cond_4

    .line 2834
    invoke-interface {v2}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v5

    .line 2835
    invoke-interface {v2}, Lcom/google/b/c/gn;->c()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v6, v6, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v6, p1, v5}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2837
    invoke-interface {v2}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v5

    .line 2838
    if-ne v5, p3, :cond_1

    .line 2839
    iget v1, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/c/go;->c:I

    .line 2840
    invoke-interface {p3}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-virtual {p0, p1, v1, v5}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2841
    invoke-direct {p0, v0, v2}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2842
    iget v1, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2843
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2844
    iput v1, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2845
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2854
    invoke-virtual {p0}, Lcom/google/b/c/go;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2855
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2847
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2854
    invoke-virtual {p0}, Lcom/google/b/c/go;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2855
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_2
    move v0, v1

    goto :goto_1

    .line 2833
    :cond_3
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2851
    :cond_4
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2854
    invoke-virtual {p0}, Lcom/google/b/c/go;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2855
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_5
    move v0, v1

    goto :goto_1

    .line 2853
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2854
    invoke-virtual {p0}, Lcom/google/b/c/go;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2855
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_6
    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2541
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2543
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    .line 2545
    iget-object v4, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2546
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2547
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v3, v0

    .line 2549
    :goto_0
    if-eqz v3, :cond_6

    .line 2550
    invoke-interface {v3}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v6

    .line 2551
    invoke-interface {v3}, Lcom/google/b/c/gn;->c()I

    move-result v7

    if-ne v7, p2, :cond_5

    if-eqz v6, :cond_5

    iget-object v7, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v7, v7, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v7, p1, v6}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2555
    invoke-interface {v3}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v7

    .line 2556
    invoke-interface {v7}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v8

    .line 2557
    if-nez v8, :cond_3

    .line 2558
    invoke-interface {v7}, Lcom/google/b/c/hb;->b()Z

    move-result v9

    if-eqz v9, :cond_2

    move v2, v1

    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 2559
    iget v2, p0, Lcom/google/b/c/go;->b:I

    .line 2560
    iget v2, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/c/go;->c:I

    .line 2561
    sget-object v2, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    invoke-virtual {p0, v6, v8, v2}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2562
    invoke-direct {p0, v0, v3}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2563
    iget v2, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v2, v2, -0x1

    .line 2564
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2565
    iput v2, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2567
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2587
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move v0, v1

    :goto_2
    return v0

    .line 2558
    :cond_2
    :try_start_1
    invoke-interface {v7}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    move v2, v1

    goto :goto_1

    .line 2570
    :cond_3
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->g:Lcom/google/b/a/x;

    invoke-virtual {v0, p3, v8}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2571
    iget v0, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/go;->c:I

    .line 2572
    sget-object v0, Lcom/google/b/c/ff;->b:Lcom/google/b/c/ff;

    invoke-virtual {p0, p1, v8, v0}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2573
    invoke-direct {p0, v3, p4}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2587
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move v0, v2

    goto :goto_2

    .line 2578
    :cond_4
    :try_start_2
    invoke-direct {p0, v3}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2579
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2587
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move v0, v1

    goto :goto_2

    .line 2549
    :cond_5
    :try_start_3
    invoke-interface {v3}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto/16 :goto_0

    .line 2584
    :cond_6
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2587
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move v0, v1

    goto :goto_2

    .line 2586
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2587
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0
.end method

.method final b()V
    .locals 5

    .prologue
    .line 2243
    invoke-direct {p0}, Lcom/google/b/c/go;->e()V

    .line 2245
    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257
    :cond_0
    return-void

    .line 2250
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v0, v0, Lcom/google/b/c/fn;->p:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    .line 2252
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/go;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    invoke-static {v0, v2, v3}, Lcom/google/b/c/fn;->a(Lcom/google/b/c/gn;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2253
    invoke-interface {v0}, Lcom/google/b/c/gn;->c()I

    move-result v1

    sget-object v4, Lcom/google/b/c/ff;->d:Lcom/google/b/c/ff;

    invoke-direct {p0, v0, v1, v4}, Lcom/google/b/c/go;->a(Lcom/google/b/c/gn;ILcom/google/b/c/ff;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2254
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final b(Ljava/lang/Object;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2357
    :try_start_0
    iget v1, p0, Lcom/google/b/c/go;->b:I

    if-eqz v1, :cond_3

    .line 2358
    invoke-direct {p0, p1, p2}, Lcom/google/b/c/go;->e(Ljava/lang/Object;I)Lcom/google/b/c/gn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2359
    if-nez v1, :cond_1

    .line 2360
    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    .line 2367
    :cond_0
    :goto_0
    return v0

    .line 2362
    :cond_1
    :try_start_1
    invoke-interface {v1}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 2367
    :cond_2
    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_0

    .line 2365
    :cond_3
    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    :cond_4
    throw v0
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2679
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2681
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    .line 2683
    iget v0, p0, Lcom/google/b/c/go;->b:I

    .line 2684
    iget-object v5, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2685
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, p2, v0

    .line 2686
    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v4, v0

    .line 2688
    :goto_0
    if-eqz v4, :cond_6

    .line 2689
    invoke-interface {v4}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v7

    .line 2690
    invoke-interface {v4}, Lcom/google/b/c/gn;->c()I

    move-result v3

    if-ne v3, p2, :cond_5

    if-eqz v7, :cond_5

    iget-object v3, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v3, v3, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v3, p1, v7}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2692
    invoke-interface {v4}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v3

    .line 2693
    invoke-interface {v3}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v8

    .line 2696
    iget-object v9, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v9, v9, Lcom/google/b/c/fn;->g:Lcom/google/b/a/x;

    invoke-virtual {v9, p3, v8}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2697
    sget-object v3, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;

    .line 2704
    :goto_1
    iget v9, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/google/b/c/go;->c:I

    .line 2705
    invoke-virtual {p0, v7, v8, v3}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2706
    invoke-direct {p0, v0, v4}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2707
    iget v4, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v4, v4, -0x1

    .line 2708
    invoke-virtual {v5, v6, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2709
    iput v4, p0, Lcom/google/b/c/go;->b:I

    .line 2710
    sget-object v0, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v3, v0, :cond_4

    move v0, v1

    .line 2716
    :goto_2
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2717
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move v2, v0

    :goto_3
    return v2

    .line 2698
    :cond_0
    :try_start_1
    invoke-interface {v3}, Lcom/google/b/c/hb;->b()Z

    move-result v9

    if-eqz v9, :cond_1

    move v3, v2

    :goto_4
    if-eqz v3, :cond_3

    .line 2699
    sget-object v3, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    goto :goto_1

    .line 2698
    :cond_1
    invoke-interface {v3}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-nez v3, :cond_2

    move v3, v1

    goto :goto_4

    :cond_2
    move v3, v2

    goto :goto_4

    .line 2701
    :cond_3
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2717
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_3

    :cond_4
    move v0, v2

    .line 2710
    goto :goto_2

    .line 2688
    :cond_5
    :try_start_2
    invoke-interface {v4}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    move-object v4, v3

    goto :goto_0

    .line 2714
    :cond_6
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2717
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    goto :goto_3

    .line 2716
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2717
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0
.end method

.method final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2636
    invoke-virtual {p0}, Lcom/google/b/c/go;->lock()V

    .line 2638
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->c()V

    .line 2640
    iget v0, p0, Lcom/google/b/c/go;->b:I

    .line 2641
    iget-object v5, p0, Lcom/google/b/c/go;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2642
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, p2, v0

    .line 2643
    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/gn;

    move-object v4, v0

    .line 2645
    :goto_0
    if-eqz v4, :cond_5

    .line 2646
    invoke-interface {v4}, Lcom/google/b/c/gn;->d()Ljava/lang/Object;

    move-result-object v7

    .line 2647
    invoke-interface {v4}, Lcom/google/b/c/gn;->c()I

    move-result v2

    if-ne v2, p2, :cond_4

    if-eqz v7, :cond_4

    iget-object v2, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    iget-object v2, v2, Lcom/google/b/c/fn;->f:Lcom/google/b/a/x;

    invoke-virtual {v2, p1, v7}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2649
    invoke-interface {v4}, Lcom/google/b/c/gn;->a()Lcom/google/b/c/hb;

    move-result-object v8

    .line 2650
    invoke-interface {v8}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2653
    if-eqz v2, :cond_0

    .line 2654
    sget-object v1, Lcom/google/b/c/ff;->a:Lcom/google/b/c/ff;

    .line 2661
    :goto_1
    iget v3, p0, Lcom/google/b/c/go;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/b/c/go;->c:I

    .line 2662
    invoke-virtual {p0, v7, v2, v1}, Lcom/google/b/c/go;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/ff;)V

    .line 2663
    invoke-direct {p0, v0, v4}, Lcom/google/b/c/go;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)Lcom/google/b/c/gn;

    move-result-object v0

    .line 2664
    iget v1, p0, Lcom/google/b/c/go;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2665
    invoke-virtual {v5, v6, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2666
    iput v1, p0, Lcom/google/b/c/go;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2673
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2674
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v2

    :goto_2
    return-object v0

    .line 2655
    :cond_0
    :try_start_1
    invoke-interface {v8}, Lcom/google/b/c/hb;->b()Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    :goto_3
    if-eqz v3, :cond_3

    .line 2656
    sget-object v1, Lcom/google/b/c/ff;->c:Lcom/google/b/c/ff;

    goto :goto_1

    .line 2655
    :cond_2
    invoke-interface {v8}, Lcom/google/b/c/hb;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    if-nez v8, :cond_1

    const/4 v3, 0x1

    goto :goto_3

    .line 2658
    :cond_3
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2674
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v1

    goto :goto_2

    .line 2645
    :cond_4
    :try_start_2
    invoke-interface {v4}, Lcom/google/b/c/gn;->b()Lcom/google/b/c/gn;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v4, v2

    goto :goto_0

    .line 2671
    :cond_5
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2674
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    move-object v0, v1

    goto :goto_2

    .line 2673
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2674
    invoke-virtual {p0}, Lcom/google/b/c/go;->d()V

    throw v0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 2981
    invoke-virtual {p0}, Lcom/google/b/c/go;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2983
    :try_start_0
    invoke-virtual {p0}, Lcom/google/b/c/go;->a()V

    .line 2984
    invoke-virtual {p0}, Lcom/google/b/c/go;->b()V

    .line 2985
    iget-object v0, p0, Lcom/google/b/c/go;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2987
    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    .line 2990
    :cond_0
    return-void

    .line 2987
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/c/go;->unlock()V

    throw v0
.end method

.method final d()V
    .locals 5

    .prologue
    .line 2994
    invoke-virtual {p0}, Lcom/google/b/c/go;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2995
    iget-object v1, p0, Lcom/google/b/c/go;->a:Lcom/google/b/c/fn;

    :goto_0
    iget-object v0, v1, Lcom/google/b/c/fn;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/fm;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, v1, Lcom/google/b/c/fn;->n:Lcom/google/b/c/fl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/b/c/fn;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Exception thrown by removal listener"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2997
    :cond_0
    return-void
.end method
