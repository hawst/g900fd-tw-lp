.class Lcom/google/b/c/dz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field

.field final b:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;[Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 814
    iput-object p1, p0, Lcom/google/b/c/dz;->a:Ljava/util/Comparator;

    .line 815
    iput-object p2, p0, Lcom/google/b/c/dz;->b:[Ljava/lang/Object;

    .line 816
    return-void
.end method


# virtual methods
.method readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 820
    new-instance v1, Lcom/google/b/c/dy;

    iget-object v0, p0, Lcom/google/b/c/dz;->a:Ljava/util/Comparator;

    invoke-direct {v1, v0}, Lcom/google/b/c/dy;-><init>(Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/b/c/dz;->b:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/google/b/c/dy;->b([Ljava/lang/Object;)Lcom/google/b/c/dy;

    move-result-object v1

    iget-object v0, v1, Lcom/google/b/c/dy;->a:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/b/c/dy;->c:Ljava/util/Comparator;

    iget v3, v1, Lcom/google/b/c/dy;->b:I

    invoke-static {v2, v3, v0}, Lcom/google/b/c/dx;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/google/b/c/dx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/dx;->size()I

    move-result v2

    iput v2, v1, Lcom/google/b/c/dy;->b:I

    return-object v0
.end method
