.class public Lcom/google/b/c/eu;
.super Ljava/util/AbstractSequentialList;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractSequentialList",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TF;>;"
        }
    .end annotation
.end field

.field final b:Lcom/google/b/a/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/aa",
            "<-TF;+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/b/a/aa;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TF;>;",
            "Lcom/google/b/a/aa",
            "<-TF;+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/util/AbstractSequentialList;-><init>()V

    .line 527
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/google/b/c/eu;->a:Ljava/util/List;

    .line 528
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/b/a/aa;

    iput-object p2, p0, Lcom/google/b/c/eu;->b:Lcom/google/b/a/aa;

    .line 529
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/b/c/eu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 539
    return-void
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 548
    new-instance v0, Lcom/google/b/c/ev;

    iget-object v1, p0, Lcom/google/b/c/eu;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/ev;-><init>(Lcom/google/b/c/eu;Ljava/util/ListIterator;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/b/c/eu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
