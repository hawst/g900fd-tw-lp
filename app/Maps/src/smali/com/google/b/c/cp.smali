.class Lcom/google/b/c/cp;
.super Lcom/google/b/c/dn;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/dn",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/co;


# direct methods
.method constructor <init>(Lcom/google/b/c/co;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/b/c/cp;->a:Lcom/google/b/c/co;

    invoke-direct {p0}, Lcom/google/b/c/dn;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lcom/google/b/c/lg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/lg",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/b/c/cp;->a:Lcom/google/b/c/co;

    iget-object v0, v0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/eg;->a(Ljava/util/Iterator;)Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/b/c/cp;->a:Lcom/google/b/c/co;

    iget-object v0, v0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/b/c/cp;->a:Lcom/google/b/c/co;

    iget-object v0, v0, Lcom/google/b/c/co;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/eg;->a(Ljava/util/Iterator;)Lcom/google/b/c/lg;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/b/c/cp;->a:Lcom/google/b/c/co;

    invoke-virtual {v0}, Lcom/google/b/c/co;->size()I

    move-result v0

    return v0
.end method
