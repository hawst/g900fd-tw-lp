.class final Lcom/google/b/c/jh;
.super Lcom/google/b/c/ds;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ds",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final transient b:Lcom/google/b/c/jk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/jk",
            "<TK;>;"
        }
    .end annotation
.end field

.field final transient c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/c/jk;Lcom/google/b/c/cv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/jk",
            "<TK;>;",
            "Lcom/google/b/c/cv",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/b/c/ds;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    .line 37
    iput-object p2, p0, Lcom/google/b/c/jh;->c:Lcom/google/b/c/cv;

    .line 38
    return-void
.end method

.method private a(II)Lcom/google/b/c/ds;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 99
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/b/c/jh;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 104
    :goto_0
    return-object p0

    .line 101
    :cond_0
    if-ne p1, p2, :cond_2

    .line 102
    invoke-virtual {p0}, Lcom/google/b/c/jh;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p0, Lcom/google/b/c/ds;->a:Lcom/google/b/c/ds;

    goto :goto_0

    :cond_1
    new-instance p0, Lcom/google/b/c/aw;

    invoke-direct {p0, v0}, Lcom/google/b/c/aw;-><init>(Ljava/util/Comparator;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    invoke-virtual {v0, p1, p2}, Lcom/google/b/c/jk;->a(II)Lcom/google/b/c/dx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/c/jh;->c:Lcom/google/b/c/cv;

    invoke-virtual {v1, p1, p2}, Lcom/google/b/c/cv;->a(II)Lcom/google/b/c/cv;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/b/c/dx;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/b/c/dx;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {}, Lcom/google/b/c/io;->d()Lcom/google/b/c/io;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/b/c/ds;->a:Lcom/google/b/c/ds;

    :goto_1
    move-object p0, v0

    goto :goto_0

    :cond_3
    new-instance p0, Lcom/google/b/c/aw;

    invoke-direct {p0, v0}, Lcom/google/b/c/aw;-><init>(Ljava/util/Comparator;)V

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/google/b/c/jh;

    check-cast v0, Lcom/google/b/c/jk;

    invoke-direct {v1, v0, v2}, Lcom/google/b/c/jh;-><init>(Lcom/google/b/c/jk;Lcom/google/b/c/cv;)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1, p1, p2}, Lcom/google/b/c/jk;->c(Ljava/lang/Object;Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/b/c/jh;->a(II)Lcom/google/b/c/ds;

    move-result-object v0

    return-object v0
.end method

.method public final aq_()Lcom/google/b/c/ci;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ci",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/b/c/jh;->c:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final as_()Lcom/google/b/c/dx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dx",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)Lcom/google/b/c/ds;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/b/c/ds",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/b/c/jk;->d(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/b/c/jh;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/b/c/jh;->a(II)Lcom/google/b/c/ds;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/google/b/c/ji;

    invoke-direct {v0, p0}, Lcom/google/b/c/ji;-><init>(Lcom/google/b/c/jh;)V

    return-object v0
.end method

.method public final bridge synthetic e()Lcom/google/b/c/dn;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    invoke-virtual {v0, p1}, Lcom/google/b/c/jk;->a(Ljava/lang/Object;)I

    move-result v0

    .line 95
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/b/c/jh;->c:Lcom/google/b/c/cv;

    invoke-virtual {v1, v0}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/b/c/jh;->b:Lcom/google/b/c/jk;

    return-object v0
.end method

.method public final bridge synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/b/c/jh;->c:Lcom/google/b/c/cv;

    return-object v0
.end method
