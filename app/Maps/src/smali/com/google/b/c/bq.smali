.class final Lcom/google/b/c/bq;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Lcom/google/b/c/ak;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<TV;TK;>;",
        "Lcom/google/b/c/ak",
        "<TV;TK;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/b/c/bl;


# direct methods
.method constructor <init>(Lcom/google/b/c/bl;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 542
    return-void
.end method


# virtual methods
.method public final ap_()Lcom/google/b/c/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ak",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-interface {v0}, Lcom/google/b/c/ak;->clear()V

    .line 500
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-interface {v0, p1}, Lcom/google/b/c/ak;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TV;TK;>;>;"
        }
    .end annotation

    .prologue
    .line 578
    new-instance v0, Lcom/google/b/c/br;

    invoke-direct {v0, p0}, Lcom/google/b/c/br;-><init>(Lcom/google/b/c/bq;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    .line 509
    iget-object v1, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    .line 510
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 509
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 510
    :cond_1
    iget-object v0, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 539
    new-instance v0, Lcom/google/b/c/bu;

    invoke-direct {v0, p0}, Lcom/google/b/c/bu;-><init>(Lcom/google/b/c/bq;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TK;)TK;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 515
    iget-object v2, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v3

    if-nez p2, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {v2, p1, v3}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v4

    if-eqz v4, :cond_4

    iget v5, v4, Lcom/google/b/c/bm;->b:I

    if-ne v0, v5, :cond_4

    iget-object v5, v4, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    if-eq p2, v5, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p2, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_4

    :goto_2
    return-object p2

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {v2, p2, v0}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v1

    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "value already present: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v2, v4}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bm;)V

    :cond_6
    new-instance v1, Lcom/google/b/c/bm;

    invoke-direct {v1, p2, v0, p1, v3}, Lcom/google/b/c/bm;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    invoke-virtual {v2, v1}, Lcom/google/b/c/bl;->b(Lcom/google/b/c/bm;)V

    invoke-virtual {v2}, Lcom/google/b/c/bl;->c()V

    if-nez v4, :cond_7

    const/4 p2, 0x0

    goto :goto_2

    :cond_7
    iget-object p2, v4, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    goto :goto_2
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    .line 524
    iget-object v1, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {v1, p1, v0}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    .line 525
    if-nez v0, :cond_1

    .line 526
    const/4 v0, 0x0

    .line 529
    :goto_1
    return-object v0

    .line 524
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-virtual {v1, v0}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bm;)V

    .line 529
    iget-object v0, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    iget v0, v0, Lcom/google/b/c/bl;->b:I

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-interface {v0}, Lcom/google/b/c/ak;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 635
    new-instance v0, Lcom/google/b/c/bw;

    iget-object v1, p0, Lcom/google/b/c/bq;->a:Lcom/google/b/c/bl;

    invoke-direct {v0, v1}, Lcom/google/b/c/bw;-><init>(Lcom/google/b/c/bl;)V

    return-object v0
.end method
