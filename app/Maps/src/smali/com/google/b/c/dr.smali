.class final Lcom/google/b/c/dr;
.super Lcom/google/b/c/ir;
.source "PG"

# interfaces
.implements Lcom/google/b/c/jy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ir",
        "<TE;>;",
        "Lcom/google/b/c/jy",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/b/c/dx;Lcom/google/b/c/cv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/dx",
            "<TE;>;",
            "Lcom/google/b/c/cv",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/b/c/ir;-><init>(Lcom/google/b/c/ci;Lcom/google/b/c/cv;)V

    .line 35
    return-void
.end method


# virtual methods
.method final b(II)Lcom/google/b/c/cv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/google/b/c/jk;

    invoke-super {p0, p1, p2}, Lcom/google/b/c/ir;->b(II)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/b/c/dr;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/b/c/jk;-><init>(Lcom/google/b/c/cv;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Lcom/google/b/c/jk;->c()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/b/c/ir;->e()Lcom/google/b/c/ci;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/dx;

    invoke-virtual {v0}, Lcom/google/b/c/dx;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/google/b/c/dr;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final bridge synthetic e()Lcom/google/b/c/ci;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/google/b/c/ir;->e()Lcom/google/b/c/ci;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/dx;

    return-object v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/b/c/ir;->e()Lcom/google/b/c/ci;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/dx;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dx;->a(Ljava/lang/Object;)I

    move-result v0

    .line 59
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/b/c/dr;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/google/b/c/dr;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
