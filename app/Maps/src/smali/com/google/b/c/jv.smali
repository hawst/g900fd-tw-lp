.class final Lcom/google/b/c/jv;
.super Lcom/google/b/c/cf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/cf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final transient a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field final transient b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field transient c:Lcom/google/b/c/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cf",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/b/c/cf;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    .line 39
    iput-object p2, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    .line 40
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/cf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Lcom/google/b/c/cf",
            "<TV;TK;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/b/c/cf;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    .line 44
    iput-object p2, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    .line 45
    iput-object p3, p0, Lcom/google/b/c/jv;->c:Lcom/google/b/c/cf;

    .line 46
    return-void
.end method

.method constructor <init>(Ljava/util/Map$Entry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/b/c/jv;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 50
    return-void
.end method


# virtual methods
.method final a()Lcom/google/b/c/dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ap_()Lcom/google/b/c/ak;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/b/c/jv;->b()Lcom/google/b/c/cf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/b/c/cf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cf",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/b/c/jv;->c:Lcom/google/b/c/cf;

    .line 91
    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/google/b/c/jv;

    iget-object v1, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/b/c/jv;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/c/cf;)V

    iput-object v0, p0, Lcom/google/b/c/jv;->c:Lcom/google/b/c/cf;

    .line 94
    :cond_0
    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final d()Lcom/google/b/c/dn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/b/c/hj;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/b/c/jv;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/b/c/jv;->b:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method
