.class public final Lcom/google/b/c/bl;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Lcom/google/b/c/ak;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Lcom/google/b/c/ak",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field transient a:[Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field transient b:I

.field transient c:I

.field private transient d:[Lcom/google/b/c/bm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient e:I

.field private transient f:Lcom/google/b/c/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ak",
            "<TV;TK;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 110
    invoke-direct {p0, p1}, Lcom/google/b/c/bl;->a(I)V

    .line 111
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expectedSize must be >= 0 but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 115
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p1, v0, v1}, Lcom/google/b/c/cc;->a(ID)I

    move-result v0

    .line 116
    new-array v1, v0, [Lcom/google/b/c/bm;

    iput-object v1, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    .line 117
    new-array v1, v0, [Lcom/google/b/c/bm;

    iput-object v1, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    .line 118
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/c/bl;->e:I

    .line 119
    iput v2, p0, Lcom/google/b/c/bl;->c:I

    .line 120
    iput v2, p0, Lcom/google/b/c/bl;->b:I

    .line 121
    return-void
.end method

.method static synthetic a(Lcom/google/b/c/bl;Lcom/google/b/c/bm;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bm;)V

    return-void
.end method

.method public static b()Lcom/google/b/c/bl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/b/c/bl",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 57
    const/16 v0, 0x10

    new-instance v1, Lcom/google/b/c/bl;

    invoke-direct {v1, v0}, Lcom/google/b/c/bl;-><init>(I)V

    return-object v1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 4

    .prologue
    .line 662
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 663
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 664
    invoke-direct {p0, v1}, Lcom/google/b/c/bl;->a(I)V

    .line 665
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 666
    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3

    .prologue
    .line 656
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 657
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 658
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Object;I)Lcom/google/b/c/bm;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    iget v1, p0, Lcom/google/b/c/bl;->e:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 179
    iget v1, v0, Lcom/google/b/c/bm;->b:I

    if-ne p2, v1, :cond_2

    iget-object v1, v0, Lcom/google/b/c/bm;->a:Ljava/lang/Object;

    if-eq p1, v1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 183
    :goto_2
    return-object v0

    .line 179
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 178
    :cond_2
    iget-object v0, v0, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    goto :goto_0

    .line 183
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method a(Lcom/google/b/c/bm;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128
    iget v0, p1, Lcom/google/b/c/bm;->b:I

    iget v2, p0, Lcom/google/b/c/bl;->e:I

    and-int v3, v0, v2

    .line 130
    iget-object v0, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    aget-object v0, v0, v3

    move-object v2, v1

    .line 131
    :goto_0
    if-ne v0, p1, :cond_1

    .line 132
    if-nez v2, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    iget-object v2, p1, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    aput-object v2, v0, v3

    .line 142
    :goto_1
    iget v0, p1, Lcom/google/b/c/bm;->d:I

    iget v2, p0, Lcom/google/b/c/bl;->e:I

    and-int/2addr v2, v0

    .line 144
    iget-object v0, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    aget-object v0, v0, v2

    .line 145
    :goto_2
    if-ne v0, p1, :cond_3

    .line 146
    if-nez v1, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    iget-object v1, p1, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    aput-object v1, v0, v2

    .line 156
    :goto_3
    iget v0, p0, Lcom/google/b/c/bl;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/c/bl;->b:I

    .line 157
    iget v0, p0, Lcom/google/b/c/bl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/bl;->c:I

    .line 158
    return-void

    .line 135
    :cond_0
    iget-object v0, p1, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    iput-object v0, v2, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    goto :goto_1

    .line 130
    :cond_1
    iget-object v2, v0, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p1, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    iput-object v0, v1, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    goto :goto_3

    .line 144
    :cond_3
    iget-object v1, v0, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2
.end method

.method public final ap_()Lcom/google/b/c/ak;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/ak",
            "<TV;TK;>;"
        }
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/b/c/bl;->f:Lcom/google/b/c/ak;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/b/c/bq;

    invoke-direct {v0, p0}, Lcom/google/b/c/bq;-><init>(Lcom/google/b/c/bl;)V

    iput-object v0, p0, Lcom/google/b/c/bl;->f:Lcom/google/b/c/ak;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/b/c/bl;->f:Lcom/google/b/c/ak;

    goto :goto_0
.end method

.method b(Ljava/lang/Object;I)Lcom/google/b/c/bm;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    iget v1, p0, Lcom/google/b/c/bl;->e:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 188
    iget v1, v0, Lcom/google/b/c/bm;->d:I

    if-ne p2, v1, :cond_2

    iget-object v1, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    if-eq p1, v1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 192
    :goto_2
    return-object v0

    .line 188
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 187
    :cond_2
    iget-object v0, v0, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    goto :goto_0

    .line 192
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method b(Lcom/google/b/c/bm;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/bm",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 161
    iget v0, p1, Lcom/google/b/c/bm;->b:I

    iget v1, p0, Lcom/google/b/c/bl;->e:I

    and-int/2addr v0, v1

    .line 162
    iget-object v1, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    aget-object v1, v1, v0

    iput-object v1, p1, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    .line 163
    iget-object v1, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    aput-object p1, v1, v0

    .line 165
    iget v0, p1, Lcom/google/b/c/bm;->d:I

    iget v1, p0, Lcom/google/b/c/bl;->e:I

    and-int/2addr v0, v1

    .line 166
    iget-object v1, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    aget-object v1, v1, v0

    iput-object v1, p1, Lcom/google/b/c/bm;->f:Lcom/google/b/c/bm;

    .line 167
    iget-object v1, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    aput-object p1, v1, v0

    .line 169
    iget v0, p0, Lcom/google/b/c/bl;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/bl;->b:I

    .line 170
    iget v0, p0, Lcom/google/b/c/bl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/bl;->c:I

    .line 171
    return-void
.end method

.method c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 279
    iget-object v3, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    .line 280
    iget v1, p0, Lcom/google/b/c/bl;->b:I

    array-length v2, v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v2, v4, v5}, Lcom/google/b/c/cc;->a(IID)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    array-length v1, v3

    shl-int/lit8 v1, v1, 0x1

    .line 283
    new-array v2, v1, [Lcom/google/b/c/bm;

    iput-object v2, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    .line 284
    new-array v2, v1, [Lcom/google/b/c/bm;

    iput-object v2, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    .line 285
    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/b/c/bl;->e:I

    .line 286
    iput v0, p0, Lcom/google/b/c/bl;->b:I

    .line 288
    :goto_0
    array-length v1, v3

    if-ge v0, v1, :cond_1

    .line 289
    aget-object v1, v3, v0

    .line 290
    :goto_1
    if-eqz v1, :cond_0

    .line 291
    iget-object v2, v1, Lcom/google/b/c/bm;->e:Lcom/google/b/c/bm;

    .line 292
    invoke-virtual {p0, v1}, Lcom/google/b/c/bl;->b(Lcom/google/b/c/bm;)V

    move-object v1, v2

    .line 294
    goto :goto_1

    .line 288
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296
    :cond_1
    iget v0, p0, Lcom/google/b/c/bl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/bl;->c:I

    .line 298
    :cond_2
    return-void
.end method

.method public final clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 318
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/c/bl;->b:I

    .line 319
    iget-object v0, p0, Lcom/google/b/c/bl;->a:[Lcom/google/b/c/bm;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 320
    iget-object v0, p0, Lcom/google/b/c/bl;->d:[Lcom/google/b/c/bm;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 321
    iget v0, p0, Lcom/google/b/c/bl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/bl;->c:I

    .line 322
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 197
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 202
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 421
    new-instance v0, Lcom/google/b/c/bn;

    invoke-direct {v0, p0}, Lcom/google/b/c/bn;-><init>(Lcom/google/b/c/bl;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 208
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    .line 209
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 208
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 209
    :cond_1
    iget-object v0, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 381
    new-instance v0, Lcom/google/b/c/by;

    invoke-direct {v0, p0}, Lcom/google/b/c/by;-><init>(Lcom/google/b/c/bl;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 214
    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v2

    if-nez p2, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v2}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v3

    if-eqz v3, :cond_4

    iget v4, v3, Lcom/google/b/c/bm;->d:I

    if-ne v0, v4, :cond_4

    iget-object v4, v3, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    if-eq p2, v4, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_4

    :goto_2
    return-object p2

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0, p2, v0}, Lcom/google/b/c/bl;->b(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v1

    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "value already present: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {p0, v3}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bm;)V

    :cond_6
    new-instance v1, Lcom/google/b/c/bm;

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/google/b/c/bm;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    invoke-virtual {p0, v1}, Lcom/google/b/c/bl;->b(Lcom/google/b/c/bm;)V

    invoke-virtual {p0}, Lcom/google/b/c/bl;->c()V

    if-nez v3, :cond_7

    const/4 p2, 0x0

    goto :goto_2

    :cond_7
    iget-object p2, v3, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    goto :goto_2
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 307
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/b/c/cc;->a(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/bl;->a(Ljava/lang/Object;I)Lcom/google/b/c/bm;

    move-result-object v0

    .line 308
    if-nez v0, :cond_1

    .line 309
    const/4 v0, 0x0

    .line 312
    :goto_1
    return-object v0

    .line 307
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 311
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/b/c/bl;->a(Lcom/google/b/c/bm;)V

    .line 312
    iget-object v0, v0, Lcom/google/b/c/bm;->c:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/google/b/c/bl;->b:I

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/b/c/bl;->ap_()Lcom/google/b/c/ak;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/c/ak;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
