.class final Lcom/google/b/c/gy;
.super Lcom/google/b/c/gv;
.source "PG"

# interfaces
.implements Lcom/google/b/c/gn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/gv",
        "<TK;TV;>;",
        "Lcom/google/b/c/gn",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile e:J

.field f:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field g:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field h:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field i:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/b/c/gn;)V
    .locals 2
    .param p3    # Lcom/google/b/c/gn;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1056
    invoke-direct {p0, p1, p2, p3}, Lcom/google/b/c/gv;-><init>(Ljava/lang/Object;ILcom/google/b/c/gn;)V

    .line 1061
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/b/c/gy;->e:J

    .line 1073
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gy;->f:Lcom/google/b/c/gn;

    .line 1086
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gy;->g:Lcom/google/b/c/gn;

    .line 1101
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gy;->h:Lcom/google/b/c/gn;

    .line 1114
    invoke-static {}, Lcom/google/b/c/fn;->c()Lcom/google/b/c/gn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/gy;->i:Lcom/google/b/c/gn;

    .line 1057
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 1070
    iput-wide p1, p0, Lcom/google/b/c/gy;->e:J

    .line 1071
    return-void
.end method

.method public final a(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/google/b/c/gy;->f:Lcom/google/b/c/gn;

    .line 1084
    return-void
.end method

.method public final b(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1096
    iput-object p1, p0, Lcom/google/b/c/gy;->g:Lcom/google/b/c/gn;

    .line 1097
    return-void
.end method

.method public final c(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1111
    iput-object p1, p0, Lcom/google/b/c/gy;->h:Lcom/google/b/c/gn;

    .line 1112
    return-void
.end method

.method public final d(Lcom/google/b/c/gn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1124
    iput-object p1, p0, Lcom/google/b/c/gy;->i:Lcom/google/b/c/gn;

    .line 1125
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1065
    iget-wide v0, p0, Lcom/google/b/c/gy;->e:J

    return-wide v0
.end method

.method public final f()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/google/b/c/gy;->f:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final g()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/google/b/c/gy;->g:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final h()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/google/b/c/gy;->h:Lcom/google/b/c/gn;

    return-object v0
.end method

.method public final i()Lcom/google/b/c/gn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/google/b/c/gy;->i:Lcom/google/b/c/gn;

    return-object v0
.end method
