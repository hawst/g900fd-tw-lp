.class public abstract Lcom/google/b/c/cf;
.super Lcom/google/b/c/dc;
.source "PG"

# interfaces
.implements Lcom/google/b/c/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/dc",
        "<TK;TV;>;",
        "Lcom/google/b/c/ak",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/b/c/dc;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Collection;)Lcom/google/b/c/cf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)",
            "Lcom/google/b/c/cf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 203
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 211
    new-instance v0, Lcom/google/b/c/is;

    invoke-direct {v0, p0}, Lcom/google/b/c/is;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    .line 205
    :pswitch_0
    sget-object v0, Lcom/google/b/c/at;->a:Lcom/google/b/c/at;

    goto :goto_0

    .line 207
    :pswitch_1
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/eg;->d(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 208
    new-instance v1, Lcom/google/b/c/jv;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/b/c/jv;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public synthetic ap_()Lcom/google/b/c/ak;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/b/c/cf;->b()Lcom/google/b/c/cf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aq_()Lcom/google/b/c/ci;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/b/c/cf;->b()Lcom/google/b/c/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cf;->e()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()Lcom/google/b/c/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cf",
            "<TV;TK;>;"
        }
    .end annotation
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/b/c/cf;->b()Lcom/google/b/c/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cf;->e()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/google/b/c/ch;

    invoke-direct {v0, p0}, Lcom/google/b/c/ch;-><init>(Lcom/google/b/c/cf;)V

    return-object v0
.end method
