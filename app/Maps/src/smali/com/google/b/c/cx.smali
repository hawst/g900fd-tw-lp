.class public final Lcom/google/b/c/cx;
.super Lcom/google/b/c/ck;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ck",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private a:[Ljava/lang/Object;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/b/c/cx;-><init>(I)V

    .line 653
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/google/b/c/ck;-><init>()V

    .line 657
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    .line 658
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/c/cx;->b:I

    .line 659
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Iterable;)Lcom/google/b/c/ck;
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0, p1}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Lcom/google/b/c/ck;
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0, p1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/util/Iterator;)Lcom/google/b/c/ck;
    .locals 0

    .prologue
    .line 643
    invoke-super {p0, p1}, Lcom/google/b/c/ck;->a(Ljava/util/Iterator;)Lcom/google/b/c/ck;

    return-object p0
.end method

.method public final a()Lcom/google/b/c/cv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 745
    iget v0, p0, Lcom/google/b/c/cx;->b:I

    packed-switch v0, :pswitch_data_0

    .line 754
    iget v0, p0, Lcom/google/b/c/cx;->b:I

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 756
    new-instance v0, Lcom/google/b/c/ja;

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/google/b/c/ja;-><init>([Ljava/lang/Object;)V

    .line 758
    :goto_0
    return-object v0

    .line 747
    :pswitch_0
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    .line 751
    :pswitch_1
    iget-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 752
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    .line 758
    :cond_0
    new-instance v0, Lcom/google/b/c/ja;

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/b/c/cx;->b:I

    invoke-static {v1, v2}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/c/ja;-><init>([Ljava/lang/Object;)V

    goto :goto_0

    .line 745
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final varargs a([Ljava/lang/Object;)Lcom/google/b/c/cx;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "Lcom/google/b/c/cx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 716
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 717
    aget-object v2, p1, v0

    invoke-static {v2, v0}, Lcom/google/b/c/in;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 716
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 719
    :cond_0
    iget v0, p0, Lcom/google/b/c/cx;->b:I

    array-length v2, p1

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v2, v0, :cond_1

    iget-object v2, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v3, v0}, Lcom/google/b/c/cx;->a(II)I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    .line 720
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/b/c/cx;->b:I

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 721
    iget v0, p0, Lcom/google/b/c/cx;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/c/cx;->b:I

    .line 722
    return-object p0
.end method

.method public final b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Lcom/google/b/c/cx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 698
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 699
    check-cast v0, Ljava/util/Collection;

    .line 700
    iget v1, p0, Lcom/google/b/c/cx;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2, v0}, Lcom/google/b/c/cx;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    .line 702
    :cond_0
    invoke-super {p0, p1}, Lcom/google/b/c/ck;->a(Ljava/lang/Iterable;)Lcom/google/b/c/ck;

    .line 703
    return-object p0
.end method

.method public final b(Ljava/lang/Object;)Lcom/google/b/c/cx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/google/b/c/cx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 682
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 683
    :cond_0
    iget v0, p0, Lcom/google/b/c/cx;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v1, v0, :cond_1

    iget-object v1, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2, v0}, Lcom/google/b/c/cx;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    .line 684
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/cx;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/b/c/cx;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/b/c/cx;->b:I

    aput-object p1, v0, v1

    .line 685
    return-object p0
.end method

.method public final b(Ljava/util/Iterator;)Lcom/google/b/c/cx;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "Lcom/google/b/c/cx",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 735
    invoke-super {p0, p1}, Lcom/google/b/c/ck;->a(Ljava/util/Iterator;)Lcom/google/b/c/ck;

    .line 736
    return-object p0
.end method
