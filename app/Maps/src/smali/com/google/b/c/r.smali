.class Lcom/google/b/c/r;
.super Lcom/google/b/c/p;
.source "PG"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/f",
        "<TK;TV;>.com/google/b/c/p;",
        "Ljava/util/List",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic f:Lcom/google/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/b/c/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/List",
            "<TV;>;",
            "Lcom/google/b/c/f",
            "<TK;TV;>.com/google/b/c/p;)V"
        }
    .end annotation

    .prologue
    .line 671
    iput-object p1, p0, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    .line 672
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/b/c/p;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/b/c/p;)V

    .line 673
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)V"
        }
    .end annotation

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 707
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    .line 708
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 709
    iget-object v0, p0, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    iget v2, v0, Lcom/google/b/c/f;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/b/c/f;->b:I

    .line 710
    if-eqz v1, :cond_0

    .line 711
    invoke-virtual {p0}, Lcom/google/b/c/r;->c()V

    .line 713
    :cond_0
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 680
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    const/4 v0, 0x0

    .line 692
    :cond_0
    :goto_0
    return v0

    .line 683
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/c/r;->size()I

    move-result v1

    .line 684
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 685
    if-eqz v0, :cond_0

    .line 686
    iget-object v2, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 687
    iget-object v3, p0, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    sub-int/2addr v2, v1

    iget v4, v3, Lcom/google/b/c/f;->b:I

    add-int/2addr v2, v4

    iput v2, v3, Lcom/google/b/c/f;->b:I

    .line 688
    if-nez v1, :cond_0

    .line 689
    invoke-virtual {p0}, Lcom/google/b/c/r;->c()V

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 696
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 697
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 725
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 730
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 734
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 735
    new-instance v0, Lcom/google/b/c/s;

    invoke-direct {v0, p0}, Lcom/google/b/c/s;-><init>(Lcom/google/b/c/r;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 740
    new-instance v0, Lcom/google/b/c/s;

    invoke-direct {v0, p0, p1}, Lcom/google/b/c/s;-><init>(Lcom/google/b/c/r;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 717
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 718
    iget-object v1, p0, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    iget v2, v1, Lcom/google/b/c/f;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/google/b/c/f;->b:I

    .line 719
    invoke-virtual {p0}, Lcom/google/b/c/r;->b()V

    .line 720
    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)TV;"
        }
    .end annotation

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 702
    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/google/b/c/r;->a()V

    .line 745
    iget-object v1, p0, Lcom/google/b/c/r;->f:Lcom/google/b/c/f;

    iget-object v2, p0, Lcom/google/b/c/p;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v1, v2, v0, p0}, Lcom/google/b/c/f;->a(Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object p0, p0, Lcom/google/b/c/p;->c:Lcom/google/b/c/p;

    goto :goto_0
.end method
