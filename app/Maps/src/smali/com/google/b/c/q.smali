.class Lcom/google/b/c/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field final synthetic c:Lcom/google/b/c/p;


# direct methods
.method constructor <init>(Lcom/google/b/c/p;)V
    .locals 2

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/b/c/q;->b:Ljava/util/Collection;

    .line 453
    iget-object v0, p1, Lcom/google/b/c/p;->e:Lcom/google/b/c/f;

    iget-object v0, p1, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    .line 454
    return-void

    .line 453
    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/b/c/p;Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/b/c/q;->b:Ljava/util/Collection;

    .line 457
    iput-object p2, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    .line 458
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-virtual {v0}, Lcom/google/b/c/p;->a()V

    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/b/c/q;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-virtual {v0}, Lcom/google/b/c/p;->a()V

    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->b:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/b/c/q;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/b/c/q;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 483
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    iget-object v0, v0, Lcom/google/b/c/p;->e:Lcom/google/b/c/f;

    iget v1, v0, Lcom/google/b/c/f;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/b/c/f;->b:I

    .line 484
    iget-object v0, p0, Lcom/google/b/c/q;->c:Lcom/google/b/c/p;

    invoke-virtual {v0}, Lcom/google/b/c/p;->b()V

    .line 485
    return-void
.end method
