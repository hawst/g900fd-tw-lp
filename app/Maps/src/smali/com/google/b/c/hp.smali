.class abstract Lcom/google/b/c/hp;
.super Ljava/util/AbstractMap;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 2417
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/google/b/c/hp;->a:Ljava/util/Set;

    .line 2430
    if-nez v0, :cond_0

    .line 2431
    invoke-virtual {p0}, Lcom/google/b/c/hp;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/hp;->a:Ljava/util/Set;

    .line 2433
    :cond_0
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 2440
    iget-object v0, p0, Lcom/google/b/c/hp;->b:Ljava/util/Set;

    .line 2441
    if-nez v0, :cond_0

    .line 2442
    new-instance v0, Lcom/google/b/c/hq;

    invoke-direct {v0, p0}, Lcom/google/b/c/hq;-><init>(Lcom/google/b/c/hp;)V

    iput-object v0, p0, Lcom/google/b/c/hp;->b:Ljava/util/Set;

    .line 2449
    :cond_0
    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 2456
    iget-object v0, p0, Lcom/google/b/c/hp;->c:Ljava/util/Collection;

    .line 2457
    if-nez v0, :cond_0

    .line 2458
    new-instance v0, Lcom/google/b/c/hr;

    invoke-direct {v0, p0}, Lcom/google/b/c/hr;-><init>(Lcom/google/b/c/hp;)V

    iput-object v0, p0, Lcom/google/b/c/hp;->c:Ljava/util/Collection;

    .line 2465
    :cond_0
    return-object v0
.end method
