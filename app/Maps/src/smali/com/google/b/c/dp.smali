.class public Lcom/google/b/c/dp;
.super Lcom/google/b/c/ck;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/ck",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:[Ljava/lang/Object;

.field b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/b/c/dp;-><init>(I)V

    .line 542
    return-void
.end method

.method private constructor <init>(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 544
    invoke-direct {p0}, Lcom/google/b/c/ck;-><init>()V

    .line 545
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "capacity must be >= 0 but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 546
    :cond_1
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    .line 547
    iput v2, p0, Lcom/google/b/c/dp;->b:I

    .line 548
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Iterable;)Lcom/google/b/c/ck;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lcom/google/b/c/ck;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/Iterator;)Lcom/google/b/c/ck;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lcom/google/b/c/dp;->b(Ljava/util/Iterator;)Lcom/google/b/c/dp;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/b/c/dn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/dn",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 638
    iget v0, p0, Lcom/google/b/c/dp;->b:I

    iget-object v1, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/b/c/dn;->a(I[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    .line 641
    invoke-virtual {v0}, Lcom/google/b/c/dn;->size()I

    move-result v1

    iput v1, p0, Lcom/google/b/c/dp;->b:I

    .line 642
    return-object v0
.end method

.method public varargs a([Ljava/lang/Object;)Lcom/google/b/c/dp;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "Lcom/google/b/c/dp",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 589
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 590
    aget-object v2, p1, v0

    invoke-static {v2, v0}, Lcom/google/b/c/in;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 592
    :cond_0
    iget v0, p0, Lcom/google/b/c/dp;->b:I

    array-length v2, p1

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v2, v0, :cond_1

    iget-object v2, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v3, v0}, Lcom/google/b/c/dp;->a(II)I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/b/c/dp;->b:I

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 594
    iget v0, p0, Lcom/google/b/c/dp;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/c/dp;->b:I

    .line 595
    return-object p0
.end method

.method public b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Lcom/google/b/c/dp",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 609
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 610
    check-cast v0, Ljava/util/Collection;

    .line 611
    iget v1, p0, Lcom/google/b/c/dp;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2, v0}, Lcom/google/b/c/dp;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    .line 613
    :cond_0
    invoke-super {p0, p1}, Lcom/google/b/c/ck;->a(Ljava/lang/Iterable;)Lcom/google/b/c/ck;

    .line 614
    return-object p0
.end method

.method public b(Ljava/lang/Object;)Lcom/google/b/c/dp;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/google/b/c/dp",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 573
    iget v0, p0, Lcom/google/b/c/dp;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v2, v0}, Lcom/google/b/c/dp;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/b/c/in;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/dp;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/b/c/dp;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/b/c/dp;->b:I

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    aput-object p1, v0, v1

    .line 575
    return-object p0
.end method

.method public b(Ljava/util/Iterator;)Lcom/google/b/c/dp;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "Lcom/google/b/c/dp",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 628
    invoke-super {p0, p1}, Lcom/google/b/c/ck;->a(Ljava/util/Iterator;)Lcom/google/b/c/ck;

    .line 629
    return-object p0
.end method
