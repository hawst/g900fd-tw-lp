.class abstract Lcom/google/b/c/f;
.super Lcom/google/b/c/aa;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/aa",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x21f766b1f568c81dL


# instance fields
.field transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field transient b:I


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/b/c/aa;-><init>()V

    .line 118
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 119
    :cond_0
    iput-object p1, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    .line 120
    return-void
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TV;>;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 268
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 269
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    .line 270
    :cond_0
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 271
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 272
    :cond_1
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 273
    check-cast p0, Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 275
    :cond_2
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Collection",
            "<TV;>;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 312
    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 313
    new-instance v0, Lcom/google/b/c/u;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/b/c/u;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/google/b/c/p;)V

    .line 319
    :goto_0
    return-object v0

    .line 314
    :cond_0
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 315
    new-instance v0, Lcom/google/b/c/t;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/b/c/t;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_0

    .line 316
    :cond_1
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 317
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/b/c/f;->a(Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_2
    new-instance v0, Lcom/google/b/c/p;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/b/c/p;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/b/c/p;)V

    goto :goto_0
.end method

.method a(Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/b/c/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/List",
            "<TV;>;",
            "Lcom/google/b/c/f",
            "<TK;TV;>.com/google/b/c/p;)",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 324
    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/b/c/m;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/b/c/m;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/r;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/b/c/r;-><init>(Lcom/google/b/c/f;Ljava/lang/Object;Ljava/util/List;Lcom/google/b/c/p;)V

    goto :goto_0
.end method

.method final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 124
    iput-object p1, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    .line 125
    iput v2, p0, Lcom/google/b/c/f;->b:I

    .line 126
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 127
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v2

    goto :goto_1

    .line 128
    :cond_1
    iget v1, p0, Lcom/google/b/c/f;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/c/f;->b:I

    goto :goto_0

    .line 130
    :cond_2
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 185
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 186
    if-nez v0, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/google/b/c/f;->c()Ljava/util/Collection;

    move-result-object v0

    .line 188
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    iget v2, p0, Lcom/google/b/c/f;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/c/f;->b:I

    .line 190
    iget-object v2, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 199
    :goto_0
    return v0

    .line 193
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "New Collection violated the Collection spec"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 195
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    iget v0, p0, Lcom/google/b/c/f;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/c/f;->b:I

    move v0, v1

    .line 197
    goto :goto_0

    .line 199
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract c()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 298
    if-nez v0, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/google/b/c/f;->c()Ljava/util/Collection;

    move-result-object v0

    .line 301
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/b/c/f;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method d()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/b/c/f;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/f;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 253
    if-nez v0, :cond_0

    .line 254
    invoke-virtual {p0}, Lcom/google/b/c/f;->d()Ljava/util/Collection;

    move-result-object v0

    .line 262
    :goto_0
    return-object v0

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/c/f;->c()Ljava/util/Collection;

    move-result-object v1

    .line 258
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 259
    iget v2, p0, Lcom/google/b/c/f;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/b/c/f;->b:I

    .line 260
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 262
    invoke-static {v1}, Lcom/google/b/c/f;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/google/b/c/f;->b:I

    return v0
.end method

.method public e(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 282
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 285
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/c/f;->b:I

    .line 286
    return-void
.end method

.method final g()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/b/c/o;

    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/o;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/k;

    iget-object v1, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/k;-><init>(Lcom/google/b/c/f;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public h()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 946
    invoke-super {p0}, Lcom/google/b/c/aa;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 968
    invoke-super {p0}, Lcom/google/b/c/aa;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final j()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 982
    new-instance v0, Lcom/google/b/c/j;

    invoke-direct {v0, p0}, Lcom/google/b/c/j;-><init>(Lcom/google/b/c/f;)V

    return-object v0
.end method

.method final k()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/b/c/n;

    iget-object v0, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lcom/google/b/c/n;-><init>(Lcom/google/b/c/f;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/c/g;

    iget-object v1, p0, Lcom/google/b/c/f;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/b/c/g;-><init>(Lcom/google/b/c/f;Ljava/util/Map;)V

    goto :goto_0
.end method
