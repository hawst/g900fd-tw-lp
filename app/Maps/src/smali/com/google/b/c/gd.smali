.class final Lcom/google/b/c/gd;
.super Ljava/util/AbstractQueue;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "Lcom/google/b/c/gn",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/c/gn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/gn",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3014
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3015
    new-instance v0, Lcom/google/b/c/ge;

    invoke-direct {v0, p0}, Lcom/google/b/c/ge;-><init>(Lcom/google/b/c/gd;)V

    iput-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 3105
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    .line 3106
    :goto_0
    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-eq v0, v1, :cond_0

    .line 3107
    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v1

    .line 3108
    invoke-static {v0}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;)V

    move-object v0, v1

    .line 3110
    goto :goto_0

    .line 3112
    :cond_0
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0, v1}, Lcom/google/b/c/gn;->c(Lcom/google/b/c/gn;)V

    .line 3113
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0, v1}, Lcom/google/b/c/gn;->d(Lcom/google/b/c/gn;)V

    .line 3114
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3085
    check-cast p1, Lcom/google/b/c/gn;

    .line 3086
    invoke-interface {p1}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    sget-object v1, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 3091
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/b/c/gn",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3118
    new-instance v1, Lcom/google/b/c/gf;

    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    iget-object v2, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-direct {v1, p0, v0}, Lcom/google/b/c/gf;-><init>(Lcom/google/b/c/gd;Lcom/google/b/c/gn;)V

    return-object v1
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3014
    check-cast p1, Lcom/google/b/c/gn;

    invoke-interface {p1}, Lcom/google/b/c/gn;->i()Lcom/google/b/c/gn;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->i()Lcom/google/b/c/gn;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-static {p1, v0}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/b/c/gd;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3073
    check-cast p1, Lcom/google/b/c/gn;

    .line 3074
    invoke-interface {p1}, Lcom/google/b/c/gn;->i()Lcom/google/b/c/gn;

    move-result-object v0

    .line 3075
    invoke-interface {p1}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v1

    .line 3076
    invoke-static {v0, v1}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;Lcom/google/b/c/gn;)V

    .line 3077
    invoke-static {p1}, Lcom/google/b/c/fn;->b(Lcom/google/b/c/gn;)V

    .line 3079
    sget-object v0, Lcom/google/b/c/gm;->a:Lcom/google/b/c/gm;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 3096
    const/4 v1, 0x0

    .line 3097
    iget-object v0, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/b/c/gd;->a:Lcom/google/b/c/gn;

    if-eq v0, v2, :cond_0

    .line 3098
    add-int/lit8 v1, v1, 0x1

    .line 3097
    invoke-interface {v0}, Lcom/google/b/c/gn;->h()Lcom/google/b/c/gn;

    move-result-object v0

    goto :goto_0

    .line 3100
    :cond_0
    return v1
.end method
