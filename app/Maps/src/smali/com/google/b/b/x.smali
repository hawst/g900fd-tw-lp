.class abstract enum Lcom/google/b/b/x;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/b/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/b/x;

.field public static final enum b:Lcom/google/b/b/x;

.field public static final enum c:Lcom/google/b/b/x;

.field public static final enum d:Lcom/google/b/b/x;

.field public static final enum e:Lcom/google/b/b/x;

.field public static final enum f:Lcom/google/b/b/x;

.field public static final enum g:Lcom/google/b/b/x;

.field public static final enum h:Lcom/google/b/b/x;

.field static final i:[Lcom/google/b/b/x;

.field private static final synthetic j:[Lcom/google/b/b/x;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 441
    new-instance v0, Lcom/google/b/b/y;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lcom/google/b/b/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->a:Lcom/google/b/b/x;

    .line 449
    new-instance v0, Lcom/google/b/b/z;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, Lcom/google/b/b/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->b:Lcom/google/b/b/x;

    .line 465
    new-instance v0, Lcom/google/b/b/aa;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, Lcom/google/b/b/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->c:Lcom/google/b/b/x;

    .line 481
    new-instance v0, Lcom/google/b/b/ab;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, Lcom/google/b/b/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->d:Lcom/google/b/b/x;

    .line 499
    new-instance v0, Lcom/google/b/b/ac;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lcom/google/b/b/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->e:Lcom/google/b/b/x;

    .line 507
    new-instance v0, Lcom/google/b/b/ad;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->f:Lcom/google/b/b/x;

    .line 523
    new-instance v0, Lcom/google/b/b/ae;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->g:Lcom/google/b/b/x;

    .line 539
    new-instance v0, Lcom/google/b/b/af;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/x;->h:Lcom/google/b/b/x;

    .line 440
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/b/b/x;

    sget-object v1, Lcom/google/b/b/x;->a:Lcom/google/b/b/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/b/x;->b:Lcom/google/b/b/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/b/x;->c:Lcom/google/b/b/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/b/x;->d:Lcom/google/b/b/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/b/x;->e:Lcom/google/b/b/x;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/b/b/x;->f:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/b/x;->g:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/b/x;->h:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/b/x;->j:[Lcom/google/b/b/x;

    .line 567
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/b/b/x;

    sget-object v1, Lcom/google/b/b/x;->a:Lcom/google/b/b/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/b/x;->b:Lcom/google/b/b/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/b/x;->c:Lcom/google/b/b/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/b/x;->d:Lcom/google/b/b/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/b/b/x;->e:Lcom/google/b/b/x;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/b/b/x;->f:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/b/b/x;->g:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/b/b/x;->h:Lcom/google/b/b/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/b/b/x;->i:[Lcom/google/b/b/x;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/google/b/b/av;ZZ)Lcom/google/b/b/x;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 572
    sget-object v1, Lcom/google/b/b/av;->c:Lcom/google/b/b/av;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 574
    sget-object v1, Lcom/google/b/b/x;->i:[Lcom/google/b/b/x;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    .line 572
    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method static a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 604
    invoke-interface {p0}, Lcom/google/b/b/ar;->e()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/b/b/ar;->a(J)V

    .line 606
    invoke-interface {p0}, Lcom/google/b/b/ar;->g()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    .line 607
    invoke-interface {p0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    .line 609
    invoke-static {p0}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;)V

    .line 610
    return-void
.end method

.method static b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 616
    invoke-interface {p0}, Lcom/google/b/b/ar;->h()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/google/b/b/ar;->b(J)V

    .line 618
    invoke-interface {p0}, Lcom/google/b/b/ar;->j()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/b/p;->b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    .line 619
    invoke-interface {p0}, Lcom/google/b/b/ar;->i()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/b/b/p;->b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    .line 621
    invoke-static {p0}, Lcom/google/b/b/p;->b(Lcom/google/b/b/ar;)V

    .line 622
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/b/x;
    .locals 1

    .prologue
    .line 440
    const-class v0, Lcom/google/b/b/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/x;

    return-object v0
.end method

.method public static values()[Lcom/google/b/b/x;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lcom/google/b/b/x;->j:[Lcom/google/b/b/x;

    invoke-virtual {v0}, [Lcom/google/b/b/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/b/x;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/b/b/as;Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/as",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 597
    invoke-interface {p2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/b/b/ar;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;
    .param p4    # Lcom/google/b/b/ar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/as",
            "<TK;TV;>;TK;I",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end method
