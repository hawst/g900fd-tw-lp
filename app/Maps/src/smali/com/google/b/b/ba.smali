.class final Lcom/google/b/b/ba;
.super Lcom/google/b/b/bb;
.source "PG"

# interfaces
.implements Lcom/google/b/b/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/b/bb",
        "<TK;TV;>;",
        "Lcom/google/b/b/ar",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile d:J

.field e:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/b/b/ar;)V
    .locals 4
    .param p3    # Lcom/google/b/b/ar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 1213
    invoke-direct {p0, p1, p2, p3}, Lcom/google/b/b/bb;-><init>(Ljava/lang/Object;ILcom/google/b/b/ar;)V

    .line 1218
    iput-wide v2, p0, Lcom/google/b/b/ba;->a:J

    .line 1230
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ba;->b:Lcom/google/b/b/ar;

    .line 1243
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ba;->c:Lcom/google/b/b/ar;

    .line 1258
    iput-wide v2, p0, Lcom/google/b/b/ba;->d:J

    .line 1270
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ba;->e:Lcom/google/b/b/ar;

    .line 1283
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ba;->f:Lcom/google/b/b/ar;

    .line 1214
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 1227
    iput-wide p1, p0, Lcom/google/b/b/ba;->a:J

    .line 1228
    return-void
.end method

.method public final a(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1240
    iput-object p1, p0, Lcom/google/b/b/ba;->b:Lcom/google/b/b/ar;

    .line 1241
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 1267
    iput-wide p1, p0, Lcom/google/b/b/ba;->d:J

    .line 1268
    return-void
.end method

.method public final b(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1253
    iput-object p1, p0, Lcom/google/b/b/ba;->c:Lcom/google/b/b/ar;

    .line 1254
    return-void
.end method

.method public final c(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1280
    iput-object p1, p0, Lcom/google/b/b/ba;->e:Lcom/google/b/b/ar;

    .line 1281
    return-void
.end method

.method public final d(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1293
    iput-object p1, p0, Lcom/google/b/b/ba;->f:Lcom/google/b/b/ar;

    .line 1294
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1222
    iget-wide v0, p0, Lcom/google/b/b/ba;->a:J

    return-wide v0
.end method

.method public final f()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1235
    iget-object v0, p0, Lcom/google/b/b/ba;->b:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final g()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/google/b/b/ba;->c:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1262
    iget-wide v0, p0, Lcom/google/b/b/ba;->d:J

    return-wide v0
.end method

.method public final i()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1275
    iget-object v0, p0, Lcom/google/b/b/ba;->e:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final j()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/google/b/b/ba;->f:Lcom/google/b/b/ar;

    return-object v0
.end method
