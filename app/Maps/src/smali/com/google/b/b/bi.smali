.class final Lcom/google/b/b/bi;
.super Lcom/google/b/b/bj;
.source "PG"

# interfaces
.implements Lcom/google/b/b/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/b/bj",
        "<TK;TV;>;",
        "Lcom/google/b/b/ar",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile d:J

.field e:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/b/b/ar;)V
    .locals 4
    .param p4    # Lcom/google/b/b/ar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 1482
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/b/b/bj;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/b/b/ar;)V

    .line 1487
    iput-wide v2, p0, Lcom/google/b/b/bi;->a:J

    .line 1499
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bi;->b:Lcom/google/b/b/ar;

    .line 1512
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bi;->c:Lcom/google/b/b/ar;

    .line 1527
    iput-wide v2, p0, Lcom/google/b/b/bi;->d:J

    .line 1539
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bi;->e:Lcom/google/b/b/ar;

    .line 1552
    invoke-static {}, Lcom/google/b/b/p;->d()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bi;->f:Lcom/google/b/b/ar;

    .line 1483
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 1496
    iput-wide p1, p0, Lcom/google/b/b/bi;->a:J

    .line 1497
    return-void
.end method

.method public final a(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1509
    iput-object p1, p0, Lcom/google/b/b/bi;->b:Lcom/google/b/b/ar;

    .line 1510
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 1536
    iput-wide p1, p0, Lcom/google/b/b/bi;->d:J

    .line 1537
    return-void
.end method

.method public final b(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1522
    iput-object p1, p0, Lcom/google/b/b/bi;->c:Lcom/google/b/b/ar;

    .line 1523
    return-void
.end method

.method public final c(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1549
    iput-object p1, p0, Lcom/google/b/b/bi;->e:Lcom/google/b/b/ar;

    .line 1550
    return-void
.end method

.method public final d(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1562
    iput-object p1, p0, Lcom/google/b/b/bi;->f:Lcom/google/b/b/ar;

    .line 1563
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1491
    iget-wide v0, p0, Lcom/google/b/b/bi;->a:J

    return-wide v0
.end method

.method public final f()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1504
    iget-object v0, p0, Lcom/google/b/b/bi;->b:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final g()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/google/b/b/bi;->c:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1531
    iget-wide v0, p0, Lcom/google/b/b/bi;->d:J

    return-wide v0
.end method

.method public final i()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1544
    iget-object v0, p0, Lcom/google/b/b/bi;->e:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public final j()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1557
    iget-object v0, p0, Lcom/google/b/b/bi;->f:Lcom/google/b/b/ar;

    return-object v0
.end method
