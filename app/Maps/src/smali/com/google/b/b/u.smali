.class final Lcom/google/b/b/u;
.super Ljava/util/AbstractQueue;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "Lcom/google/b/b/ar",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3672
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3673
    new-instance v0, Lcom/google/b/b/v;

    invoke-direct {v0, p0}, Lcom/google/b/b/v;-><init>(Lcom/google/b/b/u;)V

    iput-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 3772
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    .line 3773
    :goto_0
    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-eq v0, v1, :cond_0

    .line 3774
    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v1

    .line 3775
    invoke-static {v0}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;)V

    move-object v0, v1

    .line 3777
    goto :goto_0

    .line 3779
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0, v1}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/ar;)V

    .line 3780
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0, v1}, Lcom/google/b/b/ar;->b(Lcom/google/b/b/ar;)V

    .line 3781
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3751
    check-cast p1, Lcom/google/b/b/ar;

    .line 3752
    invoke-interface {p1}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    sget-object v1, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 3757
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3785
    new-instance v1, Lcom/google/b/b/w;

    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    iget-object v2, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-direct {v1, p0, v0}, Lcom/google/b/b/w;-><init>(Lcom/google/b/b/u;Lcom/google/b/b/ar;)V

    return-object v1
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3672
    check-cast p1, Lcom/google/b/b/ar;

    invoke-interface {p1}, Lcom/google/b/b/ar;->g()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->g()Lcom/google/b/b/ar;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-static {p1, v0}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3672
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3672
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/b/b/u;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3739
    check-cast p1, Lcom/google/b/b/ar;

    .line 3740
    invoke-interface {p1}, Lcom/google/b/b/ar;->g()Lcom/google/b/b/ar;

    move-result-object v0

    .line 3741
    invoke-interface {p1}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v1

    .line 3742
    invoke-static {v0, v1}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V

    .line 3743
    invoke-static {p1}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;)V

    .line 3745
    sget-object v0, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 3762
    const/4 v1, 0x0

    .line 3763
    iget-object v0, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/b/b/u;->a:Lcom/google/b/b/ar;

    if-eq v0, v2, :cond_0

    .line 3765
    add-int/lit8 v1, v1, 0x1

    .line 3763
    invoke-interface {v0}, Lcom/google/b/b/ar;->f()Lcom/google/b/b/ar;

    move-result-object v0

    goto :goto_0

    .line 3767
    :cond_0
    return v1
.end method
