.class public final Lcom/google/b/b/e;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<+",
            "Lcom/google/b/b/c;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/b/a/cb;

.field private static final s:Ljava/util/logging/Logger;


# instance fields
.field c:Z

.field d:I

.field e:I

.field f:J

.field g:J

.field h:Lcom/google/b/b/cn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cn",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field i:Lcom/google/b/b/av;

.field j:Lcom/google/b/b/av;

.field k:J

.field l:J

.field m:J

.field n:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field o:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field p:Lcom/google/b/b/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cf",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field q:Lcom/google/b/a/cb;

.field r:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<+",
            "Lcom/google/b/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 159
    new-instance v0, Lcom/google/b/b/f;

    invoke-direct {v0}, Lcom/google/b/b/f;-><init>()V

    new-instance v1, Lcom/google/b/a/bz;

    invoke-direct {v1, v0}, Lcom/google/b/a/bz;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/google/b/b/e;->a:Lcom/google/b/a/bw;

    .line 176
    new-instance v1, Lcom/google/b/b/m;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    move-wide v10, v2

    move-wide v12, v2

    invoke-direct/range {v1 .. v13}, Lcom/google/b/b/m;-><init>(JJJJJJ)V

    .line 178
    new-instance v0, Lcom/google/b/b/g;

    invoke-direct {v0}, Lcom/google/b/b/g;-><init>()V

    .line 199
    new-instance v0, Lcom/google/b/b/h;

    invoke-direct {v0}, Lcom/google/b/b/h;-><init>()V

    sput-object v0, Lcom/google/b/b/e;->b:Lcom/google/b/a/cb;

    .line 207
    const-class v0, Lcom/google/b/b/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/b/b/e;->s:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/b/b/e;->c:Z

    .line 213
    iput v1, p0, Lcom/google/b/b/e;->d:I

    .line 214
    iput v1, p0, Lcom/google/b/b/e;->e:I

    .line 215
    iput-wide v2, p0, Lcom/google/b/b/e;->f:J

    .line 216
    iput-wide v2, p0, Lcom/google/b/b/e;->g:J

    .line 222
    iput-wide v2, p0, Lcom/google/b/b/e;->k:J

    .line 223
    iput-wide v2, p0, Lcom/google/b/b/e;->l:J

    .line 224
    iput-wide v2, p0, Lcom/google/b/b/e;->m:J

    .line 232
    sget-object v0, Lcom/google/b/b/e;->a:Lcom/google/b/a/bw;

    iput-object v0, p0, Lcom/google/b/b/e;->r:Lcom/google/b/a/bw;

    .line 235
    return-void
.end method

.method public static a()Lcom/google/b/b/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/e",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    new-instance v0, Lcom/google/b/b/e;

    invoke-direct {v0}, Lcom/google/b/b/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/b/b/av;)Lcom/google/b/b/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/av;",
            ")",
            "Lcom/google/b/b/e",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 516
    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p1, Lcom/google/b/b/av;

    iput-object p1, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    .line 517
    return-object p0
.end method

.method final b()Lcom/google/b/b/av;
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    sget-object v1, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/b/b/av;

    return-object v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method final c()Lcom/google/b/b/av;
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    sget-object v1, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/b/b/av;

    return-object v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public d()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 802
    iget-object v2, p0, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    if-nez v2, :cond_1

    .line 803
    iget-wide v2, p0, Lcom/google/b/b/e;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    :goto_0
    const-string v1, "maximumWeight requires weigher"

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 805
    :cond_1
    iget-boolean v2, p0, Lcom/google/b/b/e;->c:Z

    if-eqz v2, :cond_3

    .line 806
    iget-wide v2, p0, Lcom/google/b/b/e;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    :goto_1
    const-string v1, "weigher requires maximumWeight"

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 808
    :cond_3
    iget-wide v0, p0, Lcom/google/b/b/e;->g:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    .line 809
    sget-object v0, Lcom/google/b/b/e;->s:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 813
    :cond_4
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, -0x1

    const-wide/16 v6, -0x1

    .line 822
    new-instance v0, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 823
    iget v1, p0, Lcom/google/b/b/e;->d:I

    if-eq v1, v3, :cond_0

    .line 824
    const-string v1, "initialCapacity"

    iget v2, p0, Lcom/google/b/b/e;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;I)Lcom/google/b/a/ak;

    .line 826
    :cond_0
    iget v1, p0, Lcom/google/b/b/e;->e:I

    if-eq v1, v3, :cond_1

    .line 827
    const-string v1, "concurrencyLevel"

    iget v2, p0, Lcom/google/b/b/e;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;I)Lcom/google/b/a/ak;

    .line 829
    :cond_1
    iget-wide v2, p0, Lcom/google/b/b/e;->f:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 830
    const-string v1, "maximumSize"

    iget-wide v2, p0, Lcom/google/b/b/e;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/b/a/ak;->a(Ljava/lang/String;J)Lcom/google/b/a/ak;

    .line 832
    :cond_2
    iget-wide v2, p0, Lcom/google/b/b/e;->g:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 833
    const-string v1, "maximumWeight"

    iget-wide v2, p0, Lcom/google/b/b/e;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/b/a/ak;->a(Ljava/lang/String;J)Lcom/google/b/a/ak;

    .line 835
    :cond_3
    iget-wide v2, p0, Lcom/google/b/b/e;->k:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 836
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/google/b/b/e;->k:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 838
    :cond_4
    iget-wide v2, p0, Lcom/google/b/b/e;->l:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    .line 839
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/google/b/b/e;->l:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 841
    :cond_5
    iget-object v1, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    if-eqz v1, :cond_6

    .line 842
    const-string v1, "keyStrength"

    iget-object v2, p0, Lcom/google/b/b/e;->i:Lcom/google/b/b/av;

    invoke-virtual {v2}, Lcom/google/b/b/av;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 844
    :cond_6
    iget-object v1, p0, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    if-eqz v1, :cond_7

    .line 845
    const-string v1, "valueStrength"

    iget-object v2, p0, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    invoke-virtual {v2}, Lcom/google/b/b/av;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ak;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 847
    :cond_7
    iget-object v1, p0, Lcom/google/b/b/e;->n:Lcom/google/b/a/x;

    if-eqz v1, :cond_8

    .line 848
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/b/a/ak;->a(Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 850
    :cond_8
    iget-object v1, p0, Lcom/google/b/b/e;->o:Lcom/google/b/a/x;

    if-eqz v1, :cond_9

    .line 851
    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/b/a/ak;->a(Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 853
    :cond_9
    iget-object v1, p0, Lcom/google/b/b/e;->p:Lcom/google/b/b/cf;

    if-eqz v1, :cond_a

    .line 854
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lcom/google/b/a/ak;->a(Ljava/lang/Object;)Lcom/google/b/a/ak;

    .line 856
    :cond_a
    invoke-virtual {v0}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
