.class final Lcom/google/b/b/by;
.super Lcom/google/b/b/ch;
.source "PG"

# interfaces
.implements Lcom/google/b/b/bt;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x6499de12a37d0a3dL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/b/b/ch;-><init>()V

    return-void
.end method

.method private b()J
    .locals 8

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/google/b/b/by;->d:J

    .line 110
    iget-object v3, p0, Lcom/google/b/b/by;->c:[Lcom/google/b/b/cj;

    .line 111
    if-eqz v3, :cond_1

    .line 112
    array-length v4, v3

    .line 113
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 114
    aget-object v5, v3, v2

    .line 115
    if-eqz v5, :cond_0

    .line 116
    iget-wide v6, v5, Lcom/google/b/b/cj;->a:J

    add-long/2addr v0, v6

    .line 113
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    :cond_1
    return-wide v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 213
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 214
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/b/b/by;->e:I

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/b/by;->c:[Lcom/google/b/b/cj;

    .line 216
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/b/b/by;->d:J

    .line 217
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 208
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 209
    return-void
.end method


# virtual methods
.method final a(JJ)J
    .locals 3

    .prologue
    .line 57
    add-long v0, p1, p3

    return-wide v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 89
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/b/b/by;->a(J)V

    .line 90
    return-void
.end method

.method public final a(J)V
    .locals 19

    .prologue
    .line 76
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/b/b/by;->c:[Lcom/google/b/b/cj;

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/b/b/by;->d:J

    add-long v10, v8, p1

    sget-object v4, Lcom/google/b/b/ch;->f:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/ch;->g:J

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v11}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v4

    if-nez v4, :cond_4

    .line 77
    :cond_0
    const/4 v6, 0x1

    .line 78
    sget-object v4, Lcom/google/b/b/by;->a:Lcom/google/b/b/cm;

    invoke-virtual {v4}, Lcom/google/b/b/cm;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lcom/google/b/b/cl;

    iget v4, v12, Lcom/google/b/b/cl;->b:I

    .line 79
    if-eqz v13, :cond_11

    array-length v5, v13

    if-lez v5, :cond_11

    add-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    aget-object v5, v13, v4

    if-eqz v5, :cond_11

    iget-wide v8, v5, Lcom/google/b/b/cj;->a:J

    add-long v10, v8, p1

    sget-object v4, Lcom/google/b/b/cj;->b:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/cj;->c:J

    invoke-virtual/range {v4 .. v11}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v4

    if-nez v4, :cond_4

    .line 81
    :goto_0
    iget v6, v12, Lcom/google/b/b/cl;->b:I

    const/4 v5, 0x0

    move v13, v5

    move v14, v6

    move v15, v4

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    move-object/from16 v16, v0

    if-eqz v16, :cond_d

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    if-lez v17, :cond_d

    add-int/lit8 v4, v17, -0x1

    and-int/2addr v4, v14

    aget-object v5, v16, v4

    if-nez v5, :cond_6

    new-instance v10, Lcom/google/b/b/cj;

    move-wide/from16 v0, p1

    invoke-direct {v10, v0, v1}, Lcom/google/b/b/cj;-><init>(J)V

    sget-object v4, Lcom/google/b/b/ch;->f:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/ch;->h:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    if-eqz v5, :cond_2

    array-length v6, v5

    if-lez v6, :cond_2

    add-int/lit8 v6, v6, -0x1

    and-int/2addr v6, v14

    aget-object v7, v5, v6

    if-nez v7, :cond_2

    aput-object v10, v5, v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x1

    :cond_2
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/b/b/ch;->e:I

    if-eqz v4, :cond_1

    :cond_3
    iput v14, v12, Lcom/google/b/b/cl;->b:I

    .line 83
    :cond_4
    return-void

    .line 81
    :catchall_0
    move-exception v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/b/b/ch;->e:I

    throw v4

    :cond_5
    const/4 v13, 0x0

    move v4, v13

    move v6, v15

    :goto_2
    shl-int/lit8 v5, v14, 0xd

    xor-int/2addr v5, v14

    ushr-int/lit8 v7, v5, 0x11

    xor-int/2addr v5, v7

    shl-int/lit8 v7, v5, 0x5

    xor-int/2addr v5, v7

    move v13, v4

    move v14, v5

    move v15, v6

    goto :goto_1

    :cond_6
    if-nez v15, :cond_7

    const/4 v15, 0x1

    move v4, v13

    move v6, v15

    goto :goto_2

    :cond_7
    iget-wide v8, v5, Lcom/google/b/b/cj;->a:J

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-virtual {v0, v8, v9, v1, v2}, Lcom/google/b/b/ch;->a(JJ)J

    move-result-wide v10

    sget-object v4, Lcom/google/b/b/cj;->b:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/cj;->c:J

    invoke-virtual/range {v4 .. v11}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v4

    if-nez v4, :cond_3

    sget v4, Lcom/google/b/b/ch;->b:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    move-object/from16 v0, v16

    if-eq v4, v0, :cond_9

    :cond_8
    const/4 v13, 0x0

    move v4, v13

    move v6, v15

    goto :goto_2

    :cond_9
    if-nez v13, :cond_a

    const/4 v13, 0x1

    move v4, v13

    move v6, v15

    goto :goto_2

    :cond_a
    sget-object v4, Lcom/google/b/b/ch;->f:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/ch;->h:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v4

    if-eqz v4, :cond_10

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_c

    shl-int/lit8 v4, v17, 0x1

    new-array v5, v4, [Lcom/google/b/b/cj;

    const/4 v4, 0x0

    :goto_3
    move/from16 v0, v17

    if-ge v4, v0, :cond_b

    aget-object v6, v16, v4

    aput-object v6, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_b
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_c
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/b/b/ch;->e:I

    const/4 v4, 0x0

    move v13, v4

    goto/16 :goto_1

    :catchall_1
    move-exception v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/b/b/ch;->e:I

    throw v4

    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_f

    sget-object v4, Lcom/google/b/b/ch;->f:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/ch;->h:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v4

    if-eqz v4, :cond_f

    const/4 v4, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;

    move-object/from16 v0, v16

    if-ne v5, v0, :cond_e

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/b/b/cj;

    and-int/lit8 v5, v14, 0x1

    new-instance v6, Lcom/google/b/b/cj;

    move-wide/from16 v0, p1

    invoke-direct {v6, v0, v1}, Lcom/google/b/b/cj;-><init>(J)V

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/b/b/ch;->c:[Lcom/google/b/b/cj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v4, 0x1

    :cond_e
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/b/b/ch;->e:I

    if-nez v4, :cond_3

    goto/16 :goto_1

    :catchall_2
    move-exception v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/b/b/ch;->e:I

    throw v4

    :cond_f
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/b/b/ch;->d:J

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-virtual {v0, v8, v9, v1, v2}, Lcom/google/b/b/ch;->a(JJ)J

    move-result-wide v10

    sget-object v4, Lcom/google/b/b/ch;->f:Lsun/misc/Unsafe;

    sget-wide v6, Lcom/google/b/b/ch;->g:J

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v11}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v4

    if-nez v4, :cond_3

    goto/16 :goto_1

    :cond_10
    move v4, v13

    move v6, v15

    goto/16 :goto_2

    :cond_11
    move v4, v6

    goto/16 :goto_0
.end method

.method public final doubleValue()D
    .locals 2

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public final floatValue()F
    .locals 2

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public final intValue()I
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .locals 2

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/b/b/by;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
