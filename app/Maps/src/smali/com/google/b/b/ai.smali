.class abstract Lcom/google/b/b/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/b/b/as;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/as",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field e:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/b/b/bs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/p",
            "<TK;TV;>.com/google/b/b/bs;"
        }
    .end annotation
.end field

.field g:Lcom/google/b/b/bs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/p",
            "<TK;TV;>.com/google/b/b/bs;"
        }
    .end annotation
.end field

.field final synthetic h:Lcom/google/b/b/p;


# direct methods
.method constructor <init>(Lcom/google/b/b/p;)V
    .locals 1

    .prologue
    .line 4194
    iput-object p1, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4195
    iget-object v0, p1, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/b/ai;->a:I

    .line 4196
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/b/ai;->b:I

    .line 4197
    invoke-virtual {p0}, Lcom/google/b/b/ai;->a()V

    .line 4198
    return-void
.end method

.method private a(Lcom/google/b/b/ar;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 4259
    :try_start_0
    iget-object v1, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    .line 4260
    invoke-interface {p1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v4

    .line 4261
    iget-object v5, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    invoke-interface {p1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 4262
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 4263
    new-instance v1, Lcom/google/b/b/bs;

    iget-object v2, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    invoke-direct {v1, v2, v4, v0}, Lcom/google/b/b/bs;-><init>(Lcom/google/b/b/p;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/b/b/ai;->f:Lcom/google/b/b/bs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4264
    iget-object v0, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    invoke-virtual {v0}, Lcom/google/b/b/as;->a()V

    const/4 v0, 0x1

    .line 4267
    :goto_1
    return v0

    .line 4261
    :cond_1
    :try_start_1
    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v5, p1, v2, v3}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 4267
    :cond_2
    iget-object v0, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    invoke-virtual {v0}, Lcom/google/b/b/as;->a()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    invoke-virtual {v1}, Lcom/google/b/b/as;->a()V

    throw v0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 4229
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    if-eqz v0, :cond_1

    .line 4230
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    :goto_0
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    if-eqz v0, :cond_1

    .line 4231
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    invoke-direct {p0, v0}, Lcom/google/b/b/ai;->a(Lcom/google/b/b/ar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4232
    const/4 v0, 0x1

    .line 4236
    :goto_1
    return v0

    .line 4230
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    goto :goto_0

    .line 4236
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 4243
    :cond_0
    iget v0, p0, Lcom/google/b/b/ai;->b:I

    if-ltz v0, :cond_2

    .line 4244
    iget-object v0, p0, Lcom/google/b/b/ai;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/google/b/b/ai;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/b/ai;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    iput-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    if-eqz v0, :cond_0

    .line 4245
    iget-object v0, p0, Lcom/google/b/b/ai;->e:Lcom/google/b/b/ar;

    invoke-direct {p0, v0}, Lcom/google/b/b/ai;->a(Lcom/google/b/b/ar;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/b/b/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4246
    :cond_1
    const/4 v0, 0x1

    .line 4250
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 4203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/b/ai;->f:Lcom/google/b/b/bs;

    .line 4205
    invoke-direct {p0}, Lcom/google/b/b/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4223
    :cond_0
    :goto_0
    return-void

    .line 4209
    :cond_1
    invoke-direct {p0}, Lcom/google/b/b/ai;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4213
    :cond_2
    iget v0, p0, Lcom/google/b/b/ai;->a:I

    if-ltz v0, :cond_0

    .line 4214
    iget-object v0, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v1, p0, Lcom/google/b/b/ai;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/b/ai;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    .line 4215
    iget-object v0, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    iget v0, v0, Lcom/google/b/b/as;->b:I

    if-eqz v0, :cond_2

    .line 4216
    iget-object v0, p0, Lcom/google/b/b/ai;->c:Lcom/google/b/b/as;

    iget-object v0, v0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lcom/google/b/b/ai;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 4217
    iget-object v0, p0, Lcom/google/b/b/ai;->d:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/b/b/ai;->b:I

    .line 4218
    invoke-direct {p0}, Lcom/google/b/b/ai;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 4275
    iget-object v0, p0, Lcom/google/b/b/ai;->f:Lcom/google/b/b/bs;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 4288
    iget-object v0, p0, Lcom/google/b/b/ai;->g:Lcom/google/b/b/bs;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4289
    :cond_1
    iget-object v0, p0, Lcom/google/b/b/ai;->h:Lcom/google/b/b/p;

    iget-object v1, p0, Lcom/google/b/b/ai;->g:Lcom/google/b/b/bs;

    invoke-virtual {v1}, Lcom/google/b/b/bs;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/b/b/p;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4290
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/b/ai;->g:Lcom/google/b/b/bs;

    .line 4291
    return-void
.end method
