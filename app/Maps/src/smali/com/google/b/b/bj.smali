.class Lcom/google/b/b/bj;
.super Ljava/lang/ref/WeakReference;
.source "PG"

# interfaces
.implements Lcom/google/b/b/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TK;>;",
        "Lcom/google/b/b/ar",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final g:I

.field final h:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile i:Lcom/google/b/b/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/b/b/ar;)V
    .locals 1
    .param p4    # Lcom/google/b/b/ar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1302
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1367
    invoke-static {}, Lcom/google/b/b/p;->c()Lcom/google/b/b/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bj;->i:Lcom/google/b/b/bf;

    .line 1303
    iput p3, p0, Lcom/google/b/b/bj;->g:I

    .line 1304
    iput-object p4, p0, Lcom/google/b/b/bj;->h:Lcom/google/b/b/ar;

    .line 1305
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/b/b/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/google/b/b/bj;->i:Lcom/google/b/b/bf;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/b/b/bf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/google/b/b/bj;->i:Lcom/google/b/b/bf;

    .line 1375
    return-void
.end method

.method public final b()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1382
    iget-object v0, p0, Lcom/google/b/b/bj;->h:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1378
    iget v0, p0, Lcom/google/b/b/bj;->g:I

    return v0
.end method

.method public c(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1308
    invoke-virtual {p0}, Lcom/google/b/b/bj;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1360
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    .prologue
    .line 1314
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1322
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1330
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .locals 1

    .prologue
    .line 1340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1348
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1356
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
