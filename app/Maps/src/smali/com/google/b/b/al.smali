.class final Lcom/google/b/b/al;
.super Lcom/google/b/b/ap;
.source "PG"

# interfaces
.implements Lcom/google/b/b/o;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/b/ap",
        "<TK;TV;>;",
        "Lcom/google/b/b/o",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field transient a:Lcom/google/b/b/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/o",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/b/b/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/p",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4581
    invoke-direct {p0, p1}, Lcom/google/b/b/ap;-><init>(Lcom/google/b/b/p;)V

    .line 4582
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 4585
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4586
    invoke-virtual {p0}, Lcom/google/b/b/al;->c()Lcom/google/b/b/e;

    move-result-object v0

    .line 4587
    iget-object v1, p0, Lcom/google/b/b/al;->m:Lcom/google/b/b/k;

    invoke-virtual {v0}, Lcom/google/b/b/e;->d()V

    new-instance v2, Lcom/google/b/b/an;

    invoke-direct {v2, v0, v1}, Lcom/google/b/b/an;-><init>(Lcom/google/b/b/e;Lcom/google/b/b/k;)V

    iput-object v2, p0, Lcom/google/b/b/al;->a:Lcom/google/b/b/o;

    .line 4588
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4611
    iget-object v0, p0, Lcom/google/b/b/al;->a:Lcom/google/b/b/o;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 4603
    iget-object v0, p0, Lcom/google/b/b/al;->a:Lcom/google/b/b/o;

    invoke-interface {v0, p1}, Lcom/google/b/b/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 4595
    iget-object v0, p0, Lcom/google/b/b/al;->a:Lcom/google/b/b/o;

    invoke-interface {v0, p1}, Lcom/google/b/b/o;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
