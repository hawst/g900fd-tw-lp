.class Lcom/google/b/b/ap;
.super Lcom/google/b/b/n;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/b/n",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final b:Lcom/google/b/b/av;

.field final c:Lcom/google/b/b/av;

.field final d:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final f:J

.field final g:J

.field final h:J

.field final i:Lcom/google/b/b/cn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final j:I

.field final k:Lcom/google/b/b/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cf",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field final l:Lcom/google/b/a/cb;

.field final m:Lcom/google/b/b/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/k",
            "<-TK;TV;>;"
        }
    .end annotation
.end field

.field transient n:Lcom/google/b/b/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/d",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/b/b/av;Lcom/google/b/b/av;Lcom/google/b/a/x;Lcom/google/b/a/x;JJJLcom/google/b/b/cn;ILcom/google/b/b/cf;Lcom/google/b/a/cb;Lcom/google/b/b/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/av;",
            "Lcom/google/b/b/av;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;JJJ",
            "Lcom/google/b/b/cn",
            "<TK;TV;>;I",
            "Lcom/google/b/b/cf",
            "<-TK;-TV;>;",
            "Lcom/google/b/a/cb;",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4507
    invoke-direct {p0}, Lcom/google/b/b/n;-><init>()V

    .line 4508
    iput-object p1, p0, Lcom/google/b/b/ap;->b:Lcom/google/b/b/av;

    .line 4509
    iput-object p2, p0, Lcom/google/b/b/ap;->c:Lcom/google/b/b/av;

    .line 4510
    iput-object p3, p0, Lcom/google/b/b/ap;->d:Lcom/google/b/a/x;

    .line 4511
    iput-object p4, p0, Lcom/google/b/b/ap;->e:Lcom/google/b/a/x;

    .line 4512
    iput-wide p5, p0, Lcom/google/b/b/ap;->f:J

    .line 4513
    iput-wide p7, p0, Lcom/google/b/b/ap;->g:J

    .line 4514
    iput-wide p9, p0, Lcom/google/b/b/ap;->h:J

    .line 4515
    iput-object p11, p0, Lcom/google/b/b/ap;->i:Lcom/google/b/b/cn;

    .line 4516
    iput p12, p0, Lcom/google/b/b/ap;->j:I

    .line 4517
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/b/b/ap;->k:Lcom/google/b/b/cf;

    .line 4518
    invoke-static {}, Lcom/google/b/a/cb;->b()Lcom/google/b/a/cb;

    move-result-object v1

    move-object/from16 v0, p14

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/b/b/e;->b:Lcom/google/b/a/cb;

    move-object/from16 v0, p14

    if-ne v0, v1, :cond_1

    :cond_0
    const/16 p14, 0x0

    :cond_1
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/b/b/ap;->l:Lcom/google/b/a/cb;

    .line 4519
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/b/b/ap;->m:Lcom/google/b/b/k;

    .line 4520
    return-void
.end method

.method constructor <init>(Lcom/google/b/b/p;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/p",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4496
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/b/b/p;->h:Lcom/google/b/a/x;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/google/b/b/p;->n:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/google/b/b/p;->m:J

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/google/b/b/p;->k:J

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/b/b/p;->l:Lcom/google/b/b/cn;

    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/b/b/p;->f:I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/b/b/p;->q:Lcom/google/b/b/cf;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/b/b/p;->t:Lcom/google/b/b/k;

    move-object/from16 v18, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v18}, Lcom/google/b/b/ap;-><init>(Lcom/google/b/b/av;Lcom/google/b/b/av;Lcom/google/b/a/x;Lcom/google/b/a/x;JJJLcom/google/b/b/cn;ILcom/google/b/b/cf;Lcom/google/b/a/cb;Lcom/google/b/b/k;)V

    .line 4500
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 6

    .prologue
    .line 4551
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4552
    invoke-virtual {p0}, Lcom/google/b/b/ap;->c()Lcom/google/b/b/e;

    move-result-object v1

    .line 4553
    invoke-virtual {v1}, Lcom/google/b/b/e;->d()V

    iget-wide v2, v1, Lcom/google/b/b/e;->m:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "refreshAfterWrite requires a LoadingCache"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/b/b/ao;

    invoke-direct {v0, v1}, Lcom/google/b/b/ao;-><init>(Lcom/google/b/b/e;)V

    iput-object v0, p0, Lcom/google/b/b/ap;->n:Lcom/google/b/b/d;

    .line 4554
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4557
    iget-object v0, p0, Lcom/google/b/b/ap;->n:Lcom/google/b/b/d;

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/b/b/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 4562
    iget-object v0, p0, Lcom/google/b/b/ap;->n:Lcom/google/b/b/d;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4477
    iget-object v0, p0, Lcom/google/b/b/ap;->n:Lcom/google/b/b/d;

    return-object v0
.end method

.method final c()Lcom/google/b/b/e;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/e",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 4523
    invoke-static {}, Lcom/google/b/b/e;->a()Lcom/google/b/b/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/b/b/ap;->b:Lcom/google/b/b/av;

    invoke-virtual {v0, v1}, Lcom/google/b/b/e;->a(Lcom/google/b/b/av;)Lcom/google/b/b/e;

    move-result-object v2

    iget-object v0, p0, Lcom/google/b/b/ap;->c:Lcom/google/b/b/av;

    iget-object v1, v2, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "Value strength was already set to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v2, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    aput-object v6, v4, v5

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/b/b/av;

    iput-object v0, v2, Lcom/google/b/b/e;->j:Lcom/google/b/b/av;

    iget-object v0, p0, Lcom/google/b/b/ap;->d:Lcom/google/b/a/x;

    iget-object v1, v2, Lcom/google/b/b/e;->n:Lcom/google/b/a/x;

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v3, "key equivalence was already set to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v2, Lcom/google/b/b/e;->n:Lcom/google/b/a/x;

    aput-object v6, v4, v5

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, v2, Lcom/google/b/b/e;->n:Lcom/google/b/a/x;

    iget-object v0, p0, Lcom/google/b/b/ap;->e:Lcom/google/b/a/x;

    iget-object v1, v2, Lcom/google/b/b/e;->o:Lcom/google/b/a/x;

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    const-string v3, "value equivalence was already set to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v2, Lcom/google/b/b/e;->o:Lcom/google/b/a/x;

    aput-object v6, v4, v5

    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, v2, Lcom/google/b/b/e;->o:Lcom/google/b/a/x;

    iget v1, p0, Lcom/google/b/b/ap;->j:I

    iget v0, v2, Lcom/google/b/b/e;->e:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_9

    const/4 v0, 0x1

    :goto_3
    const-string v3, "concurrency level was already set to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v2, Lcom/google/b/b/e;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    if-lez v1, :cond_b

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    iput v1, v2, Lcom/google/b/b/e;->e:I

    iget-object v0, p0, Lcom/google/b/b/ap;->k:Lcom/google/b/b/cf;

    iget-object v1, v2, Lcom/google/b/b/e;->p:Lcom/google/b/b/cf;

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_5
    if-nez v1, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_d
    const/4 v1, 0x0

    goto :goto_5

    :cond_e
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Lcom/google/b/b/cf;

    iput-object v0, v2, Lcom/google/b/b/e;->p:Lcom/google/b/b/cf;

    .line 4527
    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/google/b/b/e;->c:Z

    .line 4528
    iget-wide v0, p0, Lcom/google/b/b/ap;->f:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_14

    .line 4529
    iget-wide v4, p0, Lcom/google/b/b/ap;->f:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, v2, Lcom/google/b/b/e;->k:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_6
    const-string v3, "expireAfterWrite was already set to %s ns"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->k:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_6

    :cond_11
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_12

    const/4 v0, 0x1

    :goto_7
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_7

    :cond_13
    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/b/b/e;->k:J

    .line 4531
    :cond_14
    iget-wide v0, p0, Lcom/google/b/b/ap;->g:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_19

    .line 4532
    iget-wide v4, p0, Lcom/google/b/b/ap;->g:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, v2, Lcom/google/b/b/e;->l:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_8
    const-string v3, "expireAfterAccess was already set to %s ns"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->l:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_8

    :cond_16
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_17

    const/4 v0, 0x1

    :goto_9
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_9

    :cond_18
    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/b/b/e;->l:J

    .line 4534
    :cond_19
    iget-object v0, p0, Lcom/google/b/b/ap;->i:Lcom/google/b/b/cn;

    sget-object v1, Lcom/google/b/b/j;->a:Lcom/google/b/b/j;

    if-eq v0, v1, :cond_24

    .line 4535
    iget-object v0, p0, Lcom/google/b/b/ap;->i:Lcom/google/b/b/cn;

    iget-object v1, v2, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    if-nez v1, :cond_1a

    const/4 v1, 0x1

    :goto_a
    if-nez v1, :cond_1b

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1a
    const/4 v1, 0x0

    goto :goto_a

    :cond_1b
    iget-boolean v1, v2, Lcom/google/b/b/e;->c:Z

    if-eqz v1, :cond_1d

    iget-wide v4, v2, Lcom/google/b/b/e;->f:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_1c

    const/4 v1, 0x1

    :goto_b
    const-string v3, "weigher can not be combined with maximum size"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, v2, Lcom/google/b/b/e;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    if-nez v1, :cond_1d

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    const/4 v1, 0x0

    goto :goto_b

    :cond_1d
    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1e
    check-cast v0, Lcom/google/b/b/cn;

    iput-object v0, v2, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    .line 4536
    iget-wide v0, p0, Lcom/google/b/b/ap;->h:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2d

    .line 4537
    iget-wide v4, p0, Lcom/google/b/b/ap;->h:J

    iget-wide v0, v2, Lcom/google/b/b/e;->g:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-nez v0, :cond_1f

    const/4 v0, 0x1

    :goto_c
    const-string v1, "maximum weight was already set to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->g:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    if-nez v0, :cond_20

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_c

    :cond_20
    iget-wide v0, v2, Lcom/google/b/b/e;->f:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-nez v0, :cond_21

    const/4 v0, 0x1

    :goto_d
    const-string v1, "maximum size was already set to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    if-nez v0, :cond_22

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_d

    :cond_22
    iput-wide v4, v2, Lcom/google/b/b/e;->g:J

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-ltz v0, :cond_23

    const/4 v0, 0x1

    :goto_e
    const-string v1, "maximum weight must not be negative"

    if-nez v0, :cond_2d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_e

    .line 4540
    :cond_24
    iget-wide v0, p0, Lcom/google/b/b/ap;->h:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2d

    .line 4541
    iget-wide v4, p0, Lcom/google/b/b/ap;->h:J

    iget-wide v0, v2, Lcom/google/b/b/e;->f:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-nez v0, :cond_25

    const/4 v0, 0x1

    :goto_f
    const-string v1, "maximum size was already set to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    if-nez v0, :cond_26

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_f

    :cond_26
    iget-wide v0, v2, Lcom/google/b/b/e;->g:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-nez v0, :cond_27

    const/4 v0, 0x1

    :goto_10
    const-string v1, "maximum weight was already set to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, v2, Lcom/google/b/b/e;->g:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    if-nez v0, :cond_28

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1, v3}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_10

    :cond_28
    iget-object v0, v2, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    if-nez v0, :cond_29

    const/4 v0, 0x1

    :goto_11
    const-string v1, "maximum size can not be combined with weigher"

    if-nez v0, :cond_2a

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    const/4 v0, 0x0

    goto :goto_11

    :cond_2a
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-ltz v0, :cond_2b

    const/4 v0, 0x1

    :goto_12
    const-string v1, "maximum size must not be negative"

    if-nez v0, :cond_2c

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2b
    const/4 v0, 0x0

    goto :goto_12

    :cond_2c
    iput-wide v4, v2, Lcom/google/b/b/e;->f:J

    .line 4544
    :cond_2d
    iget-object v0, p0, Lcom/google/b/b/ap;->l:Lcom/google/b/a/cb;

    if-eqz v0, :cond_31

    .line 4545
    iget-object v0, p0, Lcom/google/b/b/ap;->l:Lcom/google/b/a/cb;

    iget-object v1, v2, Lcom/google/b/b/e;->q:Lcom/google/b/a/cb;

    if-nez v1, :cond_2e

    const/4 v1, 0x1

    :goto_13
    if-nez v1, :cond_2f

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2e
    const/4 v1, 0x0

    goto :goto_13

    :cond_2f
    if-nez v0, :cond_30

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_30
    check-cast v0, Lcom/google/b/a/cb;

    iput-object v0, v2, Lcom/google/b/b/e;->q:Lcom/google/b/a/cb;

    .line 4547
    :cond_31
    return-object v2
.end method
