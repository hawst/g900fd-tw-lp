.class Lcom/google/b/b/as;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/b/b/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/p",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:I

.field c:I

.field d:I

.field e:I

.field volatile f:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final g:J

.field final h:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;"
        }
    .end annotation
.end field

.field final i:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field

.field final j:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field final l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final m:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final n:Lcom/google/b/b/c;


# direct methods
.method constructor <init>(Lcom/google/b/b/p;IJLcom/google/b/b/c;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/p",
            "<TK;TV;>;IJ",
            "Lcom/google/b/b/c;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2046
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 2026
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v2, p0, Lcom/google/b/b/as;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2047
    iput-object p1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    .line 2048
    move-wide/from16 v0, p3

    iput-wide v0, p0, Lcom/google/b/b/as;->g:J

    .line 2049
    if-nez p5, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    check-cast p5, Lcom/google/b/b/c;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    .line 2050
    new-instance v6, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v6, p2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/b/b/as;->e:I

    iget-object v2, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v2, v2, Lcom/google/b/b/p;->l:Lcom/google/b/b/cn;

    sget-object v7, Lcom/google/b/b/j;->a:Lcom/google/b/b/j;

    if-eq v2, v7, :cond_3

    move v2, v4

    :goto_0
    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/b/b/as;->e:I

    int-to-long v8, v2

    iget-wide v10, p0, Lcom/google/b/b/as;->g:J

    cmp-long v2, v8, v10

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/b/b/as;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/b/as;->e:I

    :cond_1
    iput-object v6, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2052
    iget-object v2, p1, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    sget-object v6, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v2, v6, :cond_4

    move v2, v4

    :goto_1
    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_2
    iput-object v2, p0, Lcom/google/b/b/as;->h:Ljava/lang/ref/ReferenceQueue;

    .line 2054
    iget-object v2, p1, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    sget-object v6, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v2, v6, :cond_6

    move v2, v4

    :goto_3
    if-eqz v2, :cond_2

    new-instance v3, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v3}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_2
    iput-object v3, p0, Lcom/google/b/b/as;->i:Ljava/lang/ref/ReferenceQueue;

    .line 2056
    invoke-virtual {p1}, Lcom/google/b/b/p;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_4
    iput-object v2, p0, Lcom/google/b/b/as;->j:Ljava/util/Queue;

    .line 2059
    iget-wide v2, p1, Lcom/google/b/b/p;->n:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_8

    move v2, v4

    :goto_5
    if-eqz v2, :cond_9

    new-instance v2, Lcom/google/b/b/bp;

    invoke-direct {v2}, Lcom/google/b/b/bp;-><init>()V

    :goto_6
    iput-object v2, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    .line 2062
    invoke-virtual {p1}, Lcom/google/b/b/p;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, Lcom/google/b/b/u;

    invoke-direct {v2}, Lcom/google/b/b/u;-><init>()V

    :goto_7
    iput-object v2, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    .line 2064
    return-void

    :cond_3
    move v2, v5

    .line 2050
    goto :goto_0

    :cond_4
    move v2, v5

    .line 2052
    goto :goto_1

    :cond_5
    move-object v2, v3

    goto :goto_2

    :cond_6
    move v2, v5

    .line 2054
    goto :goto_3

    .line 2056
    :cond_7
    invoke-static {}, Lcom/google/b/b/p;->e()Ljava/util/Queue;

    move-result-object v2

    goto :goto_4

    :cond_8
    move v2, v5

    .line 2059
    goto :goto_5

    :cond_9
    invoke-static {}, Lcom/google/b/b/p;->e()Ljava/util/Queue;

    move-result-object v2

    goto :goto_6

    .line 2062
    :cond_a
    invoke-static {}, Lcom/google/b/b/p;->e()Ljava/util/Queue;

    move-result-object v2

    goto :goto_7
.end method

.method private a(Ljava/lang/Object;IZ)Lcom/google/b/b/am;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;IZ)",
            "Lcom/google/b/b/am",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2358
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 2361
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    .line 2362
    invoke-direct {p0, v2, v3}, Lcom/google/b/b/as;->b(J)V

    .line 2364
    iget-object v4, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2365
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2366
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    move-object v1, v0

    .line 2369
    :goto_0
    if-eqz v1, :cond_3

    .line 2370
    invoke-interface {v1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v6

    .line 2371
    invoke-interface {v1}, Lcom/google/b/b/ar;->c()I

    move-result v7

    if-ne v7, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v7, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v7, v7, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v7, p1, v6}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2375
    invoke-interface {v1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v4

    .line 2376
    invoke-interface {v4}, Lcom/google/b/b/bf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    invoke-interface {v1}, Lcom/google/b/b/ar;->h()J

    move-result-wide v6

    sub-long/2addr v2, v6

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v6, v0, Lcom/google/b/b/p;->o:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    .line 2381
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2401
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2385
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 2386
    new-instance v0, Lcom/google/b/b/am;

    invoke-direct {v0, v4}, Lcom/google/b/b/am;-><init>(Lcom/google/b/b/bf;)V

    .line 2388
    invoke-interface {v1, v0}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2400
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2401
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_1

    .line 2369
    :cond_2
    :try_start_2
    invoke-interface {v1}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v1

    goto :goto_0

    .line 2393
    :cond_3
    iget v1, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/b/as;->d:I

    .line 2394
    new-instance v1, Lcom/google/b/b/am;

    invoke-direct {v1}, Lcom/google/b/b/am;-><init>()V

    .line 2395
    iget-object v2, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v2, v2, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    if-nez p1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2400
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2401
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0

    .line 2395
    :cond_4
    :try_start_3
    invoke-virtual {v2, p0, p1, p2, v0}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 2396
    invoke-interface {v0, v1}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V

    .line 2397
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2400
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2401
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2090
    invoke-interface {p1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2104
    :cond_0
    :goto_0
    return-object v0

    .line 2095
    :cond_1
    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v1

    .line 2096
    invoke-interface {v1}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2097
    if-nez v2, :cond_2

    invoke-interface {v1}, Lcom/google/b/b/bf;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2102
    :cond_2
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 2103
    iget-object v3, p0, Lcom/google/b/b/as;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/b/b/bf;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/b/b/ar;)Lcom/google/b/b/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V

    goto :goto_0
.end method

.method private a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;TK;I",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;",
            "Lcom/google/b/b/bz;",
            ")",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3201
    invoke-virtual {p0, p3, p5, p6}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    .line 3202
    iget-object v0, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3203
    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3205
    invoke-interface {p5}, Lcom/google/b/b/bf;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3206
    const/4 v0, 0x0

    invoke-interface {p5, v0}, Lcom/google/b/b/bf;->a(Ljava/lang/Object;)V

    .line 3209
    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/b/b/as;->b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;IJ)Lcom/google/b/b/ar;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "IJ)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2671
    invoke-direct {p0, p1, p2}, Lcom/google/b/b/as;->d(Ljava/lang/Object;I)Lcom/google/b/b/ar;

    move-result-object v1

    .line 2672
    if-nez v1, :cond_1

    .line 2678
    :cond_0
    :goto_0
    return-object v0

    .line 2674
    :cond_1
    iget-object v2, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-virtual {v2, v1, p3, p4}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2675
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-direct {p0, p3, p4}, Lcom/google/b/b/as;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0

    :cond_2
    move-object v0, v1

    .line 2678
    goto :goto_0
.end method

.method private a(Lcom/google/b/b/ar;Ljava/lang/Object;ILjava/lang/Object;JLcom/google/b/b/k;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;TK;ITV;J",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2316
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v2, v0, Lcom/google/b/b/p;->o:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/b/b/ar;->h()J

    move-result-wide v2

    sub-long v2, p5, v2

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v4, v0, Lcom/google/b/b/p;->o:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/b/bf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2318
    invoke-direct {p0, p2, p3, p7, v1}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/k;Z)Ljava/lang/Object;

    move-result-object v0

    .line 2319
    if-eqz v0, :cond_0

    move-object p4, v0

    .line 2323
    :cond_0
    return-object p4

    .line 2316
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/b/b/ar;Ljava/lang/Object;Lcom/google/b/b/bf;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;TK;",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2245
    invoke-interface {p3}, Lcom/google/b/b/bf;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2246
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2249
    :cond_0
    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Recursive load"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2252
    :cond_2
    :try_start_0
    invoke-interface {p3}, Lcom/google/b/b/bf;->e()Ljava/lang/Object;

    move-result-object v0

    .line 2253
    if-nez v0, :cond_3

    .line 2254
    new-instance v0, Lcom/google/b/b/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/b/b/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2261
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    invoke-interface {v2, v1}, Lcom/google/b/b/c;->b(I)V

    throw v0

    .line 2257
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v3, v3, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v3}, Lcom/google/b/a/cb;->a()J

    move-result-wide v4

    .line 2258
    iget-object v3, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v6, v3, Lcom/google/b/b/p;->m:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_4

    move v2, v1

    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {p1, v4, v5}, Lcom/google/b/b/ar;->a(J)V

    :cond_5
    iget-object v2, p0, Lcom/google/b/b/as;->j:Ljava/util/Queue;

    invoke-interface {v2, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261
    iget-object v2, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    invoke-interface {v2, v1}, Lcom/google/b/b/c;->b(I)V

    return-object v0
.end method

.method private a(Ljava/lang/Object;ILcom/google/b/b/k;Z)Ljava/lang/Object;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2334
    invoke-direct {p0, p1, p2, p4}, Lcom/google/b/b/as;->a(Ljava/lang/Object;IZ)Lcom/google/b/b/am;

    move-result-object v4

    .line 2336
    if-nez v4, :cond_0

    move-object v0, v6

    .line 2348
    :goto_0
    return-object v0

    .line 2340
    :cond_0
    invoke-virtual {v4, p1, p3}, Lcom/google/b/b/am;->a(Ljava/lang/Object;Lcom/google/b/b/k;)Lcom/google/b/j/a/l;

    move-result-object v5

    new-instance v0, Lcom/google/b/b/at;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/at;-><init>(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/am;Lcom/google/b/j/a/l;)V

    sget-object v1, Lcom/google/b/b/p;->b:Lcom/google/b/j/a/n;

    invoke-interface {v5, v0, v1}, Lcom/google/b/j/a/l;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2341
    invoke-interface {v5}, Lcom/google/b/j/a/l;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2343
    :try_start_0
    invoke-static {v5}, Lcom/google/b/j/a/t;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v6

    .line 2348
    goto :goto_0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 2571
    invoke-direct {p0}, Lcom/google/b/b/as;->d()V

    .line 2574
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2575
    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v1

    sget-object v2, Lcom/google/b/b/bz;->d:Lcom/google/b/b/bz;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;ILcom/google/b/b/bz;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2576
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2579
    :cond_1
    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2580
    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v1

    sget-object v2, Lcom/google/b/b/bz;->d:Lcom/google/b/b/bz;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;ILcom/google/b/b/bz;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2581
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2584
    :cond_2
    return-void
.end method

.method private a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;TK;TV;J)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2112
    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v3

    .line 2113
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->l:Lcom/google/b/b/cn;

    invoke-interface {v0}, Lcom/google/b/b/cn;->a()I

    move-result v4

    .line 2114
    if-ltz v4, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "Weights must be non-negative"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 2116
    :cond_1
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    invoke-virtual {v0, p0, p1, p3, v4}, Lcom/google/b/b/av;->a(Lcom/google/b/b/as;Lcom/google/b/b/ar;Ljava/lang/Object;I)Lcom/google/b/b/bf;

    move-result-object v0

    .line 2118
    invoke-interface {p1, v0}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V

    .line 2119
    invoke-direct {p0}, Lcom/google/b/b/as;->d()V

    iget v0, p0, Lcom/google/b/b/as;->c:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/google/b/b/as;->c:I

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v4, v0, Lcom/google/b/b/p;->m:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {p1, p4, p5}, Lcom/google/b/b/ar;->a(J)V

    :cond_2
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-virtual {v0}, Lcom/google/b/b/p;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1, p4, p5}, Lcom/google/b/b/ar;->b(J)V

    :cond_3
    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2120
    invoke-interface {v3, p3}, Lcom/google/b/b/bf;->a(Ljava/lang/Object;)V

    .line 2121
    return-void

    :cond_4
    move v1, v2

    .line 2119
    goto :goto_1
.end method

.method private a(Lcom/google/b/b/ar;ILcom/google/b/b/bz;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;I",
            "Lcom/google/b/b/bz;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3340
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 3341
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3342
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 3343
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 3345
    :goto_0
    if-eqz v2, :cond_1

    .line 3346
    if-ne v2, p1, :cond_0

    .line 3347
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 3348
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    move-object v0, p0

    move v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3350
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3351
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3352
    iput v1, p0, Lcom/google/b/b/as;->b:I

    .line 3353
    const/4 v0, 0x1

    .line 3357
    :goto_1
    return v0

    .line 3345
    :cond_0
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v2

    goto :goto_0

    .line 3357
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;ILcom/google/b/b/am;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/am",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3307
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3309
    :try_start_0
    iget-object v3, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3310
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 3311
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    move-object v2, v0

    .line 3313
    :goto_0
    if-eqz v2, :cond_3

    .line 3314
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v5

    .line 3315
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v6, v6, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v6, p1, v5}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3317
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    .line 3318
    if-ne v5, p3, :cond_1

    .line 3319
    iget-object v1, p3, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v1}, Lcom/google/b/b/bf;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3320
    iget-object v0, p3, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v2, v0}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3325
    :goto_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3334
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 3322
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v2}, Lcom/google/b/b/as;->b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3323
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3333
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3334
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0

    .line 3327
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3334
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v0, v1

    goto :goto_2

    .line 3313
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3331
    :cond_3
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3334
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Object;ILcom/google/b/b/am;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/am",
            "<TK;TV;>;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 3063
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3065
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v4

    .line 3066
    invoke-direct {p0, v4, v5}, Lcom/google/b/b/as;->b(J)V

    .line 3068
    iget v0, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v7, v0, 0x1

    .line 3069
    iget v0, p0, Lcom/google/b/b/as;->e:I

    if-le v7, v0, :cond_0

    .line 3070
    invoke-direct {p0}, Lcom/google/b/b/as;->f()V

    .line 3071
    iget v0, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v7, v0, 0x1

    .line 3074
    :cond_0
    iget-object v8, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3075
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    .line 3076
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    move-object v1, v0

    .line 3078
    :goto_0
    if-eqz v1, :cond_6

    .line 3079
    invoke-interface {v1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 3080
    invoke-interface {v1}, Lcom/google/b/b/ar;->c()I

    move-result v10

    if-ne v10, p2, :cond_5

    if-eqz v3, :cond_5

    iget-object v10, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v10, v10, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v10, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3082
    invoke-interface {v1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v0

    .line 3083
    invoke-interface {v0}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v3

    .line 3086
    if-eq p3, v0, :cond_1

    if-nez v3, :cond_4

    sget-object v8, Lcom/google/b/b/p;->u:Lcom/google/b/b/bf;

    if-eq v0, v8, :cond_4

    .line 3088
    :cond_1
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 3089
    iget-object v0, p3, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v0}, Lcom/google/b/b/bf;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3090
    if-nez v3, :cond_3

    sget-object v0, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    .line 3092
    :goto_1
    invoke-virtual {p0, p1, p3, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    .line 3093
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    :cond_2
    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    .line 3095
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 3096
    iput v7, p0, Lcom/google/b/b/as;->b:I

    .line 3097
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3098
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3117
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v0, v6

    :goto_2
    return v0

    .line 3090
    :cond_3
    :try_start_1
    sget-object v0, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    goto :goto_1

    .line 3102
    :cond_4
    new-instance v0, Lcom/google/b/b/bn;

    const/4 v1, 0x0

    invoke-direct {v0, p4, v1}, Lcom/google/b/b/bn;-><init>(Ljava/lang/Object;I)V

    .line 3103
    sget-object v1, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3104
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3117
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v0, v2

    goto :goto_2

    .line 3078
    :cond_5
    :try_start_2
    invoke-interface {v1}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v1

    goto :goto_0

    .line 3108
    :cond_6
    iget v1, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/b/as;->d:I

    .line 3109
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    if-nez p1, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3116
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3117
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0

    .line 3109
    :cond_7
    :try_start_3
    invoke-virtual {v1, p0, p1, p2, v0}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    .line 3110
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 3111
    invoke-virtual {v8, v9, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3112
    iput v7, p0, Lcom/google/b/b/as;->b:I

    .line 3113
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3114
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3117
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v0, v6

    goto :goto_2
.end method

.method private b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3216
    iget v2, p0, Lcom/google/b/b/as;->b:I

    .line 3217
    invoke-interface {p2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v1

    .line 3218
    :goto_0
    if-eq p1, p2, :cond_1

    .line 3219
    invoke-direct {p0, p1, v1}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3220
    if-eqz v0, :cond_0

    move v1, v2

    .line 3218
    :goto_1
    invoke-interface {p1}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 3223
    :cond_0
    sget-object v0, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    invoke-interface {p1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/b/b/ar;->c()I

    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    iget-object v0, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3224
    add-int/lit8 v0, v2, -0x1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_1

    .line 3227
    :cond_1
    iput v2, p0, Lcom/google/b/b/as;->b:I

    .line 3228
    return-object v1
.end method

.method private b(Ljava/lang/Object;ILcom/google/b/b/k;)Ljava/lang/Object;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 2164
    const/4 v7, 0x0

    .line 2165
    const/4 v5, 0x0

    .line 2166
    const/4 v8, 0x1

    .line 2168
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->lock()V

    .line 2171
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v4, v4, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v4}, Lcom/google/b/a/cb;->a()J

    move-result-wide v10

    .line 2172
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/google/b/b/as;->b(J)V

    .line 2174
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v12, v4, -0x1

    .line 2175
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2176
    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    and-int v14, p2, v4

    .line 2177
    invoke-virtual {v13, v14}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/b/b/ar;

    move-object v6, v4

    .line 2179
    :goto_0
    if-eqz v6, :cond_a

    .line 2180
    invoke-interface {v6}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v15

    .line 2181
    invoke-interface {v6}, Lcom/google/b/b/ar;->c()I

    move-result v9

    move/from16 v0, p2

    if-ne v9, v0, :cond_5

    if-eqz v15, :cond_5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v9, v9, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v15}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2183
    invoke-interface {v6}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v9

    .line 2184
    invoke-interface {v9}, Lcom/google/b/b/bf;->c()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2185
    const/4 v7, 0x0

    move-object v8, v9

    .line 2210
    :goto_1
    if-eqz v7, :cond_9

    .line 2211
    new-instance v5, Lcom/google/b/b/am;

    invoke-direct {v5}, Lcom/google/b/b/am;-><init>()V

    .line 2213
    if-nez v6, :cond_7

    .line 2214
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v6, v6, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    if-nez p1, :cond_6

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222
    :catchall_0
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->unlock()V

    .line 2223
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->b()V

    throw v4

    .line 2187
    :cond_0
    :try_start_1
    invoke-interface {v9}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2188
    if-nez v7, :cond_1

    .line 2189
    sget-object v7, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v9, v7}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    .line 2202
    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2203
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2204
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/b/b/as;->b:I

    move v7, v8

    move-object v8, v9

    .line 2206
    goto :goto_1

    .line 2190
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v10, v11}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 2193
    sget-object v7, Lcom/google/b/b/bz;->d:Lcom/google/b/b/bz;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v9, v7}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    goto :goto_2

    .line 2195
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v4, v4, Lcom/google/b/b/p;->m:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-lez v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_3

    invoke-interface {v6, v10, v11}, Lcom/google/b/b/ar;->a(J)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v4, v6}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2196
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/b/b/c;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->unlock()V

    .line 2223
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->b()V

    move-object v4, v7

    .line 2239
    :goto_4
    return-object v4

    .line 2195
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 2179
    :cond_5
    :try_start_2
    invoke-interface {v6}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v6

    goto/16 :goto_0

    .line 2214
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v6, v0, v1, v2, v4}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v4

    .line 2215
    invoke-interface {v4, v5}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V

    .line 2216
    invoke-virtual {v13, v14, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v17, v5

    move-object v5, v4

    move-object/from16 v4, v17

    .line 2222
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->unlock()V

    .line 2223
    invoke-virtual/range {p0 .. p0}, Lcom/google/b/b/as;->b()V

    .line 2226
    if-eqz v7, :cond_8

    .line 2231
    :try_start_3
    monitor-enter v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2232
    :try_start_4
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/google/b/b/am;->a(Ljava/lang/Object;Lcom/google/b/b/k;)Lcom/google/b/j/a/l;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4, v6}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/am;Lcom/google/b/j/a/l;)Ljava/lang/Object;

    move-result-object v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2235
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/google/b/b/c;->b(I)V

    goto :goto_4

    .line 2218
    :cond_7
    :try_start_5
    invoke-interface {v6, v5}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/bf;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v4, v5

    move-object v5, v6

    goto :goto_5

    .line 2233
    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2235
    :catchall_2
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/google/b/b/c;->b(I)V

    throw v4

    .line 2239
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1, v8}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Lcom/google/b/b/bf;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_4

    :cond_9
    move-object v4, v5

    move-object v5, v6

    goto :goto_5

    :cond_a
    move/from16 v17, v8

    move-object v8, v7

    move/from16 v7, v17

    goto/16 :goto_1
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 3395
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3397
    :try_start_0
    invoke-direct {p0}, Lcom/google/b/b/as;->c()V

    .line 3398
    invoke-direct {p0, p1, p2}, Lcom/google/b/b/as;->a(J)V

    .line 3399
    iget-object v0, p0, Lcom/google/b/b/as;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3401
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3404
    :cond_0
    return-void

    .line 3401
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0
.end method

.method private b(Lcom/google/b/b/ar;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 2492
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v0, v0, Lcom/google/b/b/p;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2493
    invoke-interface {p1, p2, p3}, Lcom/google/b/b/ar;->a(J)V

    .line 2495
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/as;->j:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2496
    return-void

    .line 2492
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2426
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    sget-object v1, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    move v1, v2

    .line 2427
    :goto_1
    iget-object v0, p0, Lcom/google/b/b/as;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/google/b/b/ar;

    iget-object v4, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v5

    iget-object v6, v4, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v7, v4, Lcom/google/b/b/p;->d:I

    ushr-int v7, v5, v7

    iget v4, v4, Lcom/google/b/b/p;->c:I

    and-int/2addr v4, v7

    aget-object v4, v6, v4

    invoke-virtual {v4, v0, v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;I)Z

    add-int/lit8 v0, v1, 0x1

    if-eq v0, v8, :cond_1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2426
    goto :goto_0

    .line 2429
    :cond_1
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    sget-object v1, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 2430
    :cond_2
    iget-object v0, p0, Lcom/google/b/b/as;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lcom/google/b/b/bf;

    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-interface {v0}, Lcom/google/b/b/bf;->b()Lcom/google/b/b/ar;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/b/b/ar;->c()I

    move-result v4

    iget-object v5, v1, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v6, v1, Lcom/google/b/b/p;->d:I

    ushr-int v6, v4, v6

    iget v1, v1, Lcom/google/b/b/p;->c:I

    and-int/2addr v1, v6

    aget-object v1, v5, v1

    invoke-interface {v3}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/bf;)Z

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v8, :cond_2

    .line 2432
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 2429
    goto :goto_2
.end method

.method private c(Lcom/google/b/b/ar;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 2507
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v0, v0, Lcom/google/b/b/p;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2508
    invoke-interface {p1, p2, p3}, Lcom/google/b/b/ar;->a(J)V

    .line 2510
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2511
    return-void

    .line 2507
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Ljava/lang/Object;I)Lcom/google/b/b/ar;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2650
    iget-object v0, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    :goto_0
    if-eqz v0, :cond_2

    .line 2651
    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 2652
    invoke-interface {v0}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v1

    .line 2656
    if-nez v1, :cond_1

    .line 2657
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/google/b/b/as;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2650
    :cond_0
    invoke-interface {v0}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v0

    goto :goto_0

    .line 2657
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0

    .line 2661
    :cond_1
    iget-object v2, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v2, v2, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v2, p1, v1}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2666
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2542
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/b/b/as;->j:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    if-eqz v0, :cond_1

    .line 2547
    iget-object v1, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2548
    iget-object v1, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2551
    :cond_1
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2613
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v0, v0, Lcom/google/b/b/p;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 2624
    :cond_0
    return-void

    .line 2613
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2617
    :cond_2
    invoke-direct {p0}, Lcom/google/b/b/as;->d()V

    .line 2618
    :cond_3
    iget v0, p0, Lcom/google/b/b/as;->c:I

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/b/b/as;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2619
    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    invoke-interface {v0}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/b/b/bf;->a()I

    move-result v2

    if-lez v2, :cond_4

    .line 2620
    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v1

    sget-object v2, Lcom/google/b/b/bz;->e:Lcom/google/b/b/bz;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;ILcom/google/b/b/bz;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2621
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2619
    :cond_5
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private f()V
    .locals 11

    .prologue
    .line 2850
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2851
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 2852
    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_0

    .line 2915
    :goto_0
    return-void

    .line 2866
    :cond_0
    iget v1, p0, Lcom/google/b/b/as;->b:I

    .line 2867
    shl-int/lit8 v0, v8, 0x1

    new-instance v9, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    .line 2868
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/b/b/as;->e:I

    .line 2869
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 2870
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    .line 2873
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    .line 2875
    if-eqz v0, :cond_1

    .line 2876
    invoke-interface {v0}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v4

    .line 2877
    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    move-result v2

    and-int v3, v2, v10

    .line 2880
    if-nez v4, :cond_2

    .line 2881
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2870
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_2
    move-object v5, v0

    .line 2888
    :goto_2
    if-eqz v4, :cond_3

    .line 2889
    invoke-interface {v4}, Lcom/google/b/b/ar;->c()I

    move-result v2

    and-int/2addr v2, v10

    .line 2890
    if-eq v2, v3, :cond_6

    move-object v3, v4

    .line 2888
    :goto_3
    invoke-interface {v4}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v4

    move-object v5, v3

    move v3, v2

    goto :goto_2

    .line 2896
    :cond_3
    invoke-virtual {v9, v3, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    .line 2899
    :goto_4
    if-eq v2, v5, :cond_1

    .line 2900
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v0

    and-int v3, v0, v10

    .line 2901
    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    .line 2902
    invoke-direct {p0, v2, v0}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 2903
    if-eqz v0, :cond_4

    .line 2904
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    .line 2899
    :goto_5
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_4

    .line 2906
    :cond_4
    sget-object v0, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    iget-object v0, p0, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0, v2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0, v2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2907
    add-int/lit8 v0, v1, -0x1

    goto :goto_5

    .line 2913
    :cond_5
    iput-object v9, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2914
    iput v1, p0, Lcom/google/b/b/as;->b:I

    goto/16 :goto_0

    :cond_6
    move v2, v3

    move-object v3, v5

    goto :goto_3
.end method


# virtual methods
.method final a(Lcom/google/b/b/ar;J)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2686
    invoke-interface {p1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2687
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/google/b/b/as;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2700
    :cond_0
    :goto_0
    return-object v0

    .line 2687
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0

    .line 2690
    :cond_1
    invoke-interface {p1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v1

    .line 2691
    if-nez v1, :cond_2

    .line 2692
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/google/b/b/as;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0

    .line 2696
    :cond_2
    iget-object v2, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/b/b/p;->a(Lcom/google/b/b/ar;J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2697
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-direct {p0, p2, p3}, Lcom/google/b/b/as;->a(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    goto :goto_0

    :catchall_2
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0

    :cond_3
    move-object v0, v1

    .line 2700
    goto :goto_0
.end method

.method final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2706
    :try_start_0
    iget v1, p0, Lcom/google/b/b/as;->b:I

    if-eqz v1, :cond_2

    .line 2707
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v6

    .line 2708
    invoke-direct {p0, p1, p2, v6, v7}, Lcom/google/b/b/as;->a(Ljava/lang/Object;IJ)Lcom/google/b/b/ar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2709
    if-nez v2, :cond_0

    .line 2710
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    .line 2722
    :goto_0
    return-object v0

    .line 2713
    :cond_0
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v5

    .line 2714
    if-eqz v5, :cond_1

    .line 2715
    invoke-direct {p0, v2, v6, v7}, Lcom/google/b/b/as;->b(Lcom/google/b/b/ar;J)V

    .line 2716
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v8, v0, Lcom/google/b/b/p;->t:Lcom/google/b/b/k;

    move-object v1, p0

    move v4, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;ILjava/lang/Object;JLcom/google/b/b/k;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2722
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_0

    .line 2718
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/google/b/b/as;->tryLock()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-eqz v1, :cond_2

    :try_start_3
    invoke-direct {p0}, Lcom/google/b/b/as;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2720
    :cond_2
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_0

    .line 2718
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2720
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILcom/google/b/b/am;Lcom/google/b/j/a/l;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/am",
            "<TK;TV;>;",
            "Lcom/google/b/j/a/l",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 2297
    const/4 v1, 0x0

    .line 2299
    :try_start_0
    invoke-static {p4}, Lcom/google/b/j/a/t;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    .line 2300
    if-nez v1, :cond_1

    .line 2301
    new-instance v0, Lcom/google/b/b/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/b/b/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2307
    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    .line 2308
    iget-object v1, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    iget-object v2, p3, Lcom/google/b/b/am;->c:Lcom/google/b/a/bm;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/b/a/bm;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/b/b/c;->b(J)V

    .line 2309
    invoke-direct {p0, p1, p2, p3}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/am;)Z

    :cond_0
    throw v0

    .line 2303
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    iget-object v2, p3, Lcom/google/b/b/am;->c:Lcom/google/b/a/bm;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/b/a/bm;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/b/b/c;->a(J)V

    .line 2304
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/am;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2307
    if-nez v1, :cond_2

    .line 2308
    iget-object v0, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    iget-object v2, p3, Lcom/google/b/b/am;->c:Lcom/google/b/a/bm;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/b/a/bm;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/b/b/c;->b(J)V

    .line 2309
    invoke-direct {p0, p1, p2, p3}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/am;)Z

    :cond_2
    return-object v1
.end method

.method final a(Ljava/lang/Object;ILcom/google/b/b/k;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2126
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2127
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2129
    :cond_1
    :try_start_0
    iget v1, p0, Lcom/google/b/b/as;->b:I

    if-eqz v1, :cond_5

    .line 2131
    invoke-direct {p0, p1, p2}, Lcom/google/b/b/as;->d(Ljava/lang/Object;I)Lcom/google/b/b/ar;

    move-result-object v2

    .line 2132
    if-eqz v2, :cond_5

    .line 2133
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v6

    .line 2134
    invoke-virtual {p0, v2, v6, v7}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;J)Ljava/lang/Object;

    move-result-object v5

    .line 2135
    if-eqz v5, :cond_4

    .line 2136
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-wide v8, v1, Lcom/google/b/b/p;->m:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_3

    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {v2, v6, v7}, Lcom/google/b/b/ar;->a(J)V

    :cond_2
    iget-object v0, p0, Lcom/google/b/b/as;->j:Ljava/util/Queue;

    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2137
    iget-object v0, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/b/b/c;->a(I)V

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v8, p3

    .line 2138
    invoke-direct/range {v1 .. v8}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;ILjava/lang/Object;JLcom/google/b/b/k;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2158
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    :goto_1
    return-object v0

    .line 2136
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2140
    :cond_4
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v0

    .line 2141
    invoke-interface {v0}, Lcom/google/b/b/bf;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2142
    invoke-direct {p0, v2, p1, v0}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Lcom/google/b/b/bf;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2158
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_1

    .line 2148
    :cond_5
    :try_start_2
    invoke-direct {p0, p1, p2, p3}, Lcom/google/b/b/as;->b(Ljava/lang/Object;ILcom/google/b/b/k;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2158
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_1

    .line 2149
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2150
    :try_start_3
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 2151
    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_6

    .line 2152
    new-instance v1, Lcom/google/b/j/a/d;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lcom/google/b/j/a/d;-><init>(Ljava/lang/Error;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2158
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    throw v0

    .line 2153
    :cond_6
    :try_start_4
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_7

    .line 2154
    new-instance v1, Lcom/google/b/j/a/s;

    invoke-direct {v1, v0}, Lcom/google/b/j/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2156
    :cond_7
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2971
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 2973
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v8

    .line 2974
    invoke-direct {p0, v8, v9}, Lcom/google/b/b/as;->b(J)V

    .line 2976
    iget-object v10, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2977
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v11, p2, v0

    .line 2978
    invoke-virtual {v10, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 2980
    :goto_0
    if-eqz v2, :cond_3

    .line 2981
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 2982
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v0, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2984
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    .line 2985
    invoke-interface {v5}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2986
    if-nez v0, :cond_1

    .line 2987
    invoke-interface {v5}, Lcom/google/b/b/bf;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2989
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 2990
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 2991
    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 2993
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2994
    invoke-virtual {v10, v11, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2995
    iput v1, p0, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2997
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3011
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v7

    :goto_1
    return-object v0

    .line 3000
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/b/as;->d:I

    .line 3001
    sget-object v1, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    invoke-virtual {p0, p1, v5, v1}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-wide v5, v8

    .line 3002
    invoke-direct/range {v1 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 3003
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3010
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3011
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_1

    .line 2980
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3008
    :cond_3
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3011
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v7

    goto :goto_1

    .line 3010
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3011
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2775
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 2777
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v4

    .line 2778
    invoke-direct {p0, v4, v5}, Lcom/google/b/b/as;->b(J)V

    .line 2780
    iget v0, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2781
    iget v1, p0, Lcom/google/b/b/as;->e:I

    if-le v0, v1, :cond_0

    .line 2782
    invoke-direct {p0}, Lcom/google/b/b/as;->f()V

    .line 2783
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 2786
    :cond_0
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2787
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 2788
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    move-object v1, v0

    .line 2791
    :goto_0
    if-eqz v1, :cond_5

    .line 2792
    invoke-interface {v1}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v2

    .line 2793
    invoke-interface {v1}, Lcom/google/b/b/ar;->c()I

    move-result v3

    if-ne v3, p2, :cond_4

    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v3, v3, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v3, p1, v2}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2797
    invoke-interface {v1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v0

    .line 2798
    invoke-interface {v0}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2800
    if-nez v7, :cond_2

    .line 2801
    iget v2, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/b/as;->d:I

    .line 2802
    invoke-interface {v0}, Lcom/google/b/b/bf;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2803
    sget-object v2, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 2804
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 2805
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 2810
    :goto_1
    iput v0, p0, Lcom/google/b/b/as;->b:I

    .line 2811
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2812
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2841
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v6

    :goto_2
    return-object v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 2807
    :try_start_1
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 2808
    iget v0, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2813
    :cond_2
    if-eqz p4, :cond_3

    .line 2817
    invoke-direct {p0, v1, v4, v5}, Lcom/google/b/b/as;->c(Lcom/google/b/b/ar;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2840
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2841
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v7

    goto :goto_2

    .line 2821
    :cond_3
    :try_start_2
    iget v2, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/b/b/as;->d:I

    .line 2822
    sget-object v2, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 2823
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 2824
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2840
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2841
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v7

    goto :goto_2

    .line 2791
    :cond_4
    :try_start_3
    invoke-interface {v1}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v1

    goto :goto_0

    .line 2831
    :cond_5
    iget v1, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/b/b/as;->d:I

    .line 2832
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    if-nez p1, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2840
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2841
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0

    .line 2832
    :cond_6
    :try_start_4
    invoke-virtual {v1, p0, p1, p2, v0}, Lcom/google/b/b/x;->a(Lcom/google/b/b/as;Ljava/lang/Object;ILcom/google/b/b/ar;)Lcom/google/b/b/ar;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    .line 2833
    invoke-direct/range {v0 .. v5}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 2834
    invoke-virtual {v7, v8, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2835
    iget v0, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2836
    iput v0, p0, Lcom/google/b/b/as;->b:I

    .line 2837
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2838
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2841
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v6

    goto :goto_2
.end method

.method final a()V
    .locals 2

    .prologue
    .line 3365
    iget-object v0, p0, Lcom/google/b/b/as;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    .line 3366
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/b/b/as;->b(J)V

    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    .line 3368
    :cond_0
    return-void
.end method

.method final a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;",
            "Lcom/google/b/b/bz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2596
    iget v0, p0, Lcom/google/b/b/as;->c:I

    invoke-interface {p2}, Lcom/google/b/b/bf;->a()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/b/b/as;->c:I

    .line 2597
    invoke-virtual {p3}, Lcom/google/b/b/bz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    iget-object v0, p0, Lcom/google/b/b/as;->n:Lcom/google/b/b/c;

    invoke-interface {v0}, Lcom/google/b/b/c;->a()V

    .line 2600
    :cond_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->p:Ljava/util/Queue;

    sget-object v1, Lcom/google/b/b/p;->v:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    .line 2601
    invoke-interface {p2}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2602
    new-instance v1, Lcom/google/b/b/cg;

    invoke-direct {v1, p1, v0, p3}, Lcom/google/b/b/cg;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/b/b/bz;)V

    .line 2603
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->p:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2605
    :cond_1
    return-void
.end method

.method final a(Lcom/google/b/b/ar;I)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;I)Z"
        }
    .end annotation

    .prologue
    .line 3242
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3244
    :try_start_0
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 3245
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3246
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 3247
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 3249
    :goto_0
    if-eqz v2, :cond_1

    .line 3250
    if-ne v2, p1, :cond_0

    .line 3251
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 3252
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3254
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3255
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3256
    iput v1, p0, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3257
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3264
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 3249
    :cond_0
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3261
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3264
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x0

    goto :goto_1

    .line 3263
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3264
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILcom/google/b/b/bf;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3272
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3274
    :try_start_0
    iget v1, p0, Lcom/google/b/b/as;->b:I

    .line 3275
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3276
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    .line 3277
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 3279
    :goto_0
    if-eqz v2, :cond_4

    .line 3280
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 3281
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v4

    if-ne v4, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v4, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v4, v4, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v4, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3283
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v4

    .line 3284
    if-ne v4, p3, :cond_2

    .line 3285
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 3286
    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3288
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3289
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3290
    iput v1, p0, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3291
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lcom/google/b/b/as;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3301
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    .line 3293
    :cond_2
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lcom/google/b/b/as;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3301
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_1

    .line 3279
    :cond_3
    :try_start_1
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3297
    :cond_4
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lcom/google/b/b/as;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3301
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_1

    .line 3299
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lcom/google/b/b/as;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_5

    .line 3301
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    :cond_5
    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 2918
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 2920
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v8

    .line 2921
    invoke-direct {p0, v8, v9}, Lcom/google/b/b/as;->b(J)V

    .line 2923
    iget-object v7, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2924
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    .line 2925
    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 2927
    :goto_0
    if-eqz v2, :cond_4

    .line 2928
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 2929
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v0, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2931
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    .line 2932
    invoke-interface {v5}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2933
    if-nez v0, :cond_1

    .line 2934
    invoke-interface {v5}, Lcom/google/b/b/bf;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2936
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 2937
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 2938
    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 2940
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2941
    invoke-virtual {v7, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2942
    iput v1, p0, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2944
    :cond_0
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2965
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x0

    :goto_1
    return v0

    .line 2947
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->h:Lcom/google/b/a/x;

    invoke-virtual {v1, p3, v0}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2948
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    .line 2949
    sget-object v0, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    invoke-virtual {p0, p1, v5, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p4

    move-wide v5, v8

    .line 2950
    invoke-direct/range {v1 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Ljava/lang/Object;Ljava/lang/Object;J)V

    .line 2951
    invoke-direct {p0}, Lcom/google/b/b/as;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2952
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2965
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x1

    goto :goto_1

    .line 2956
    :cond_2
    :try_start_2
    invoke-direct {p0, v2, v8, v9}, Lcom/google/b/b/as;->c(Lcom/google/b/b/ar;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2957
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2965
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x0

    goto :goto_1

    .line 2927
    :cond_3
    :try_start_3
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2962
    :cond_4
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2965
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    const/4 v0, 0x0

    goto :goto_1

    .line 2964
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 2965
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0
.end method

.method final b()V
    .locals 5

    .prologue
    .line 3408
    invoke-virtual {p0}, Lcom/google/b/b/as;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3409
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    :goto_0
    iget-object v0, v1, Lcom/google/b/b/p;->p:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/cg;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, v1, Lcom/google/b/b/p;->q:Lcom/google/b/b/cf;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/b/b/p;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Exception thrown by removal listener"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 3411
    :cond_0
    return-void
.end method

.method final b(Ljava/lang/Object;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2728
    :try_start_0
    iget v1, p0, Lcom/google/b/b/as;->b:I

    if-eqz v1, :cond_2

    .line 2729
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    .line 2730
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/b/b/as;->a(Ljava/lang/Object;IJ)Lcom/google/b/b/ar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2731
    if-nez v1, :cond_0

    .line 2732
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    .line 2739
    :goto_0
    return v0

    .line 2734
    :cond_0
    :try_start_1
    invoke-interface {v1}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2739
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_0

    .line 2737
    :cond_2
    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->a()V

    throw v0
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 3122
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3124
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v0}, Lcom/google/b/a/cb;->a()J

    move-result-wide v0

    .line 3125
    invoke-direct {p0, v0, v1}, Lcom/google/b/b/as;->b(J)V

    .line 3127
    iget v0, p0, Lcom/google/b/b/as;->b:I

    .line 3128
    iget-object v8, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3129
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    .line 3130
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 3132
    :goto_0
    if-eqz v2, :cond_4

    .line 3133
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 3134
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v0, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3136
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    .line 3137
    invoke-interface {v5}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3140
    iget-object v4, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v4, v4, Lcom/google/b/b/p;->h:Lcom/google/b/a/x;

    invoke-virtual {v4, p3, v0}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3141
    sget-object v6, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;

    .line 3149
    :goto_1
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    move-object v0, p0

    move v4, p2

    .line 3150
    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3152
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3153
    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3154
    iput v1, p0, Lcom/google/b/b/as;->b:I

    .line 3155
    sget-object v0, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v6, v0, :cond_2

    const/4 v0, 0x1

    .line 3161
    :goto_2
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3162
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move v7, v0

    :goto_3
    return v7

    .line 3142
    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v5}, Lcom/google/b/b/bf;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3143
    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3146
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3162
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_3

    :cond_2
    move v0, v7

    .line 3155
    goto :goto_2

    .line 3132
    :cond_3
    :try_start_2
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3159
    :cond_4
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3162
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_3

    .line 3161
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3162
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0
.end method

.method final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 10
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3017
    invoke-virtual {p0}, Lcom/google/b/b/as;->lock()V

    .line 3019
    :try_start_0
    iget-object v1, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v1, v1, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v1}, Lcom/google/b/a/cb;->a()J

    move-result-wide v2

    .line 3020
    invoke-direct {p0, v2, v3}, Lcom/google/b/b/as;->b(J)V

    .line 3022
    iget v1, p0, Lcom/google/b/b/as;->b:I

    .line 3023
    iget-object v8, p0, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3024
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v9, p2, v1

    .line 3025
    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/b/ar;

    move-object v2, v1

    .line 3027
    :goto_0
    if-eqz v2, :cond_3

    .line 3028
    invoke-interface {v2}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v3

    .line 3029
    invoke-interface {v2}, Lcom/google/b/b/ar;->c()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v4, v4, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v4, p1, v3}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3031
    invoke-interface {v2}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v5

    .line 3032
    invoke-interface {v5}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v7

    .line 3035
    if-eqz v7, :cond_0

    .line 3036
    sget-object v6, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;

    .line 3044
    :goto_1
    iget v0, p0, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/b/b/as;->d:I

    move-object v0, p0

    move v4, p2

    .line 3045
    invoke-direct/range {v0 .. v6}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;Ljava/lang/Object;ILcom/google/b/b/bf;Lcom/google/b/b/bz;)Lcom/google/b/b/ar;

    move-result-object v0

    .line 3047
    iget v1, p0, Lcom/google/b/b/as;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3048
    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3049
    iput v1, p0, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3056
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3057
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    move-object v0, v7

    :goto_2
    return-object v0

    .line 3037
    :cond_0
    :try_start_1
    invoke-interface {v5}, Lcom/google/b/b/bf;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3038
    sget-object v6, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3041
    :cond_1
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3057
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_2

    .line 3027
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3054
    :cond_3
    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3057
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    goto :goto_2

    .line 3056
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/b/b/as;->unlock()V

    .line 3057
    invoke-virtual {p0}, Lcom/google/b/b/as;->b()V

    throw v0
.end method
