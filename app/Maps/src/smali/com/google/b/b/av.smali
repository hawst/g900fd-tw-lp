.class public abstract enum Lcom/google/b/b/av;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/b/av;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/b/av;

.field public static final enum b:Lcom/google/b/b/av;

.field public static final enum c:Lcom/google/b/b/av;

.field private static final synthetic d:[Lcom/google/b/b/av;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 376
    new-instance v0, Lcom/google/b/b/aw;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    .line 391
    new-instance v0, Lcom/google/b/b/ax;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Lcom/google/b/b/ax;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/av;->b:Lcom/google/b/b/av;

    .line 407
    new-instance v0, Lcom/google/b/b/ay;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lcom/google/b/b/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/av;->c:Lcom/google/b/b/av;

    .line 370
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/b/b/av;

    sget-object v1, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/b/av;->b:Lcom/google/b/b/av;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/b/av;->c:Lcom/google/b/b/av;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/b/b/av;->d:[Lcom/google/b/b/av;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/b/av;
    .locals 1

    .prologue
    .line 370
    const-class v0, Lcom/google/b/b/av;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/av;

    return-object v0
.end method

.method public static values()[Lcom/google/b/b/av;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lcom/google/b/b/av;->d:[Lcom/google/b/b/av;

    invoke-virtual {v0}, [Lcom/google/b/b/av;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/b/av;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(Lcom/google/b/b/as;Lcom/google/b/b/ar;Ljava/lang/Object;I)Lcom/google/b/b/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/as",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;TV;I)",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation
.end method
