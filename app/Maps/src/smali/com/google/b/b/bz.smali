.class public abstract enum Lcom/google/b/b/bz;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/b/bz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/b/bz;

.field public static final enum b:Lcom/google/b/b/bz;

.field public static final enum c:Lcom/google/b/b/bz;

.field public static final enum d:Lcom/google/b/b/bz;

.field public static final enum e:Lcom/google/b/b/bz;

.field private static final synthetic f:[Lcom/google/b/b/bz;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/google/b/b/ca;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;

    .line 54
    new-instance v0, Lcom/google/b/b/cb;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lcom/google/b/b/cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    .line 67
    new-instance v0, Lcom/google/b/b/cc;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lcom/google/b/b/cc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    .line 79
    new-instance v0, Lcom/google/b/b/cd;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lcom/google/b/b/cd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/bz;->d:Lcom/google/b/b/bz;

    .line 91
    new-instance v0, Lcom/google/b/b/ce;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lcom/google/b/b/ce;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/bz;->e:Lcom/google/b/b/bz;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/b/b/bz;

    sget-object v1, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/b/b/bz;->b:Lcom/google/b/b/bz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/b/b/bz;->c:Lcom/google/b/b/bz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/b/b/bz;->d:Lcom/google/b/b/bz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/b/b/bz;->e:Lcom/google/b/b/bz;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/b/b/bz;->f:[Lcom/google/b/b/bz;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/b/bz;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/b/b/bz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/bz;

    return-object v0
.end method

.method public static values()[Lcom/google/b/b/bz;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/b/b/bz;->f:[Lcom/google/b/b/bz;

    invoke-virtual {v0}, [Lcom/google/b/b/bz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/b/bz;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
