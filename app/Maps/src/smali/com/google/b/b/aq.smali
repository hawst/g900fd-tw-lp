.class final enum Lcom/google/b/b/aq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/b/b/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/b/b/aq;",
        ">;",
        "Lcom/google/b/b/ar",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/b/b/aq;

.field private static final synthetic b:[Lcom/google/b/b/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 849
    new-instance v0, Lcom/google/b/b/aq;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/b/b/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    .line 848
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/b/aq;

    sget-object v1, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/b/b/aq;->b:[Lcom/google/b/b/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 848
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/b/b/aq;
    .locals 1

    .prologue
    .line 848
    const-class v0, Lcom/google/b/b/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/aq;

    return-object v0
.end method

.method public static values()[Lcom/google/b/b/aq;
    .locals 1

    .prologue
    .line 848
    sget-object v0, Lcom/google/b/b/aq;->b:[Lcom/google/b/b/aq;

    invoke-virtual {v0}, [Lcom/google/b/b/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/b/b/aq;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/b/b/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/bf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 852
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 873
    return-void
.end method

.method public final a(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 879
    return-void
.end method

.method public final a(Lcom/google/b/b/bf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/bf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 855
    return-void
.end method

.method public final b()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 858
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 891
    return-void
.end method

.method public final b(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 885
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 862
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 866
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 903
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 870
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final f()Lcom/google/b/b/ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 876
    return-object p0
.end method

.method public final g()Lcom/google/b/b/ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 882
    return-object p0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 888
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final i()Lcom/google/b/b/ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 894
    return-object p0
.end method

.method public final j()Lcom/google/b/b/ar;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 900
    return-object p0
.end method
