.class Lcom/google/b/b/am;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/b/bf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/b/b/bf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:Lcom/google/b/b/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final b:Lcom/google/b/j/a/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/j/a/r",
            "<TV;>;"
        }
    .end annotation
.end field

.field final c:Lcom/google/b/a/bm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3423
    invoke-static {}, Lcom/google/b/b/p;->c()Lcom/google/b/b/bf;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/b/b/am;-><init>(Lcom/google/b/b/bf;)V

    .line 3424
    return-void
.end method

.method public constructor <init>(Lcom/google/b/b/bf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3419
    invoke-static {}, Lcom/google/b/j/a/r;->a()Lcom/google/b/j/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    .line 3420
    new-instance v0, Lcom/google/b/a/bm;

    invoke-direct {v0}, Lcom/google/b/a/bm;-><init>()V

    iput-object v0, p0, Lcom/google/b/b/am;->c:Lcom/google/b/a/bm;

    .line 3427
    iput-object p1, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    .line 3428
    return-void
.end method

.method static a(Lcom/google/b/j/a/r;Ljava/lang/Throwable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/j/a/r",
            "<*>;",
            "Ljava/lang/Throwable;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3452
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/b/j/a/r;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3455
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 3439
    iget-object v0, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v0}, Lcom/google/b/b/bf;->a()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/b/b/ar;)Lcom/google/b/b/bf;
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3520
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lcom/google/b/b/k;)Lcom/google/b/j/a/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)",
            "Lcom/google/b/j/a/l",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3479
    iget-object v0, p0, Lcom/google/b/b/am;->c:Lcom/google/b/a/bm;

    invoke-virtual {v0}, Lcom/google/b/a/bm;->a()Lcom/google/b/a/bm;

    .line 3480
    iget-object v0, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v0}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3482
    if-nez v0, :cond_2

    .line 3483
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/google/b/b/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3484
    iget-object v1, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    invoke-virtual {v1, v0}, Lcom/google/b/j/a/r;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    .line 3494
    :cond_0
    :goto_0
    return-object v0

    .line 3484
    :cond_1
    invoke-static {v0}, Lcom/google/b/j/a/g;->a(Ljava/lang/Object;)Lcom/google/b/j/a/l;

    move-result-object v0

    goto :goto_0

    .line 3486
    :cond_2
    if-nez p1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3490
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3491
    instance-of v0, v1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_3

    .line 3492
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 3494
    :cond_3
    iget-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    invoke-static {v0, v1}, Lcom/google/b/b/am;->a(Lcom/google/b/j/a/r;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    goto :goto_0

    .line 3486
    :cond_4
    if-nez v0, :cond_5

    :try_start_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    invoke-virtual {p2, p1}, Lcom/google/b/b/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/j/a/g;->a(Ljava/lang/Object;)Lcom/google/b/j/a/l;

    move-result-object v0

    .line 3488
    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/b/j/a/g;->a(Ljava/lang/Object;)Lcom/google/b/j/a/l;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 3494
    :cond_6
    invoke-static {}, Lcom/google/b/j/a/r;->a()Lcom/google/b/j/a/r;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/b/b/am;->a(Lcom/google/b/j/a/r;Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 3466
    if-eqz p1, :cond_0

    .line 3469
    iget-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    invoke-virtual {v0, p1}, Lcom/google/b/j/a/r;->a(Ljava/lang/Object;)Z

    .line 3476
    :goto_0
    return-void

    .line 3472
    :cond_0
    invoke-static {}, Lcom/google/b/b/p;->c()Lcom/google/b/b/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    goto :goto_0
.end method

.method public final b()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3515
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 3431
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 3435
    iget-object v0, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v0}, Lcom/google/b/b/bf;->d()Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 3503
    iget-object v0, p0, Lcom/google/b/b/am;->b:Lcom/google/b/j/a/r;

    invoke-static {v0}, Lcom/google/b/j/a/t;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 3507
    iget-object v0, p0, Lcom/google/b/b/am;->a:Lcom/google/b/b/bf;

    invoke-interface {v0}, Lcom/google/b/b/bf;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
