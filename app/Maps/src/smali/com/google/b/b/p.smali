.class Lcom/google/b/b/p;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:Ljava/util/logging/Logger;

.field static final b:Lcom/google/b/j/a/n;

.field static final u:Lcom/google/b/b/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/bf",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field static final v:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final c:I

.field final d:I

.field final e:[Lcom/google/b/b/as;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/b/b/as",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final f:I

.field final g:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/b/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/x",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final i:Lcom/google/b/b/av;

.field final j:Lcom/google/b/b/av;

.field final k:J

.field final l:Lcom/google/b/b/cn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cn",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final m:J

.field final n:J

.field final o:J

.field final p:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/b/cg",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final q:Lcom/google/b/b/cf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/cf",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final r:Lcom/google/b/a/cb;

.field final s:Lcom/google/b/b/x;

.field final t:Lcom/google/b/b/k;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/k",
            "<-TK;TV;>;"
        }
    .end annotation
.end field

.field w:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field x:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field y:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const-class v0, Lcom/google/b/b/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/b/b/p;->a:Ljava/util/logging/Logger;

    .line 157
    new-instance v0, Lcom/google/b/j/a/q;

    invoke-direct {v0}, Lcom/google/b/j/a/q;-><init>()V

    sput-object v0, Lcom/google/b/b/p;->b:Lcom/google/b/j/a/n;

    .line 690
    new-instance v0, Lcom/google/b/b/q;

    invoke-direct {v0}, Lcom/google/b/b/q;-><init>()V

    sput-object v0, Lcom/google/b/b/p;->u:Lcom/google/b/b/bf;

    .line 983
    new-instance v0, Lcom/google/b/b/r;

    invoke-direct {v0}, Lcom/google/b/b/r;-><init>()V

    sput-object v0, Lcom/google/b/b/p;->v:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/google/b/b/e;Lcom/google/b/b/k;)V
    .locals 10
    .param p2    # Lcom/google/b/b/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/e",
            "<-TK;-TV;>;",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 236
    iget v0, p1, Lcom/google/b/b/e;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x4

    :goto_0
    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/b/b/p;->f:I

    .line 238
    invoke-virtual {p1}, Lcom/google/b/b/e;->b()Lcom/google/b/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    .line 239
    invoke-virtual {p1}, Lcom/google/b/b/e;->c()Lcom/google/b/b/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    .line 241
    iget-object v0, p1, Lcom/google/b/b/e;->n:Lcom/google/b/a/x;

    invoke-virtual {p1}, Lcom/google/b/b/e;->b()Lcom/google/b/b/av;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/b/av;->a()Lcom/google/b/a/x;

    move-result-object v1

    if-eqz v0, :cond_a

    :goto_1
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, p0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    .line 242
    iget-object v0, p1, Lcom/google/b/b/e;->o:Lcom/google/b/a/x;

    invoke-virtual {p1}, Lcom/google/b/b/e;->c()Lcom/google/b/b/av;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/b/av;->a()Lcom/google/b/a/x;

    move-result-object v1

    if-eqz v0, :cond_c

    :goto_2
    check-cast v0, Lcom/google/b/a/x;

    iput-object v0, p0, Lcom/google/b/b/p;->h:Lcom/google/b/a/x;

    .line 244
    iget-wide v0, p1, Lcom/google/b/b/e;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/google/b/b/e;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    :cond_0
    const-wide/16 v0, 0x0

    :goto_3
    iput-wide v0, p0, Lcom/google/b/b/p;->k:J

    .line 245
    iget-object v0, p1, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    sget-object v1, Lcom/google/b/b/j;->a:Lcom/google/b/b/j;

    if-eqz v0, :cond_10

    :goto_4
    check-cast v0, Lcom/google/b/b/cn;

    iput-object v0, p0, Lcom/google/b/b/p;->l:Lcom/google/b/b/cn;

    .line 246
    iget-wide v0, p1, Lcom/google/b/b/e;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_12

    const-wide/16 v0, 0x0

    :goto_5
    iput-wide v0, p0, Lcom/google/b/b/p;->m:J

    .line 247
    iget-wide v0, p1, Lcom/google/b/b/e;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_13

    const-wide/16 v0, 0x0

    :goto_6
    iput-wide v0, p0, Lcom/google/b/b/p;->n:J

    .line 248
    iget-wide v0, p1, Lcom/google/b/b/e;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_14

    const-wide/16 v0, 0x0

    :goto_7
    iput-wide v0, p0, Lcom/google/b/b/p;->o:J

    .line 250
    iget-object v0, p1, Lcom/google/b/b/e;->p:Lcom/google/b/b/cf;

    sget-object v1, Lcom/google/b/b/i;->a:Lcom/google/b/b/i;

    if-eqz v0, :cond_15

    :goto_8
    check-cast v0, Lcom/google/b/b/cf;

    iput-object v0, p0, Lcom/google/b/b/p;->q:Lcom/google/b/b/cf;

    .line 251
    iget-object v0, p0, Lcom/google/b/b/p;->q:Lcom/google/b/b/cf;

    sget-object v1, Lcom/google/b/b/i;->a:Lcom/google/b/b/i;

    if-ne v0, v1, :cond_17

    sget-object v0, Lcom/google/b/b/p;->v:Ljava/util/Queue;

    :goto_9
    iput-object v0, p0, Lcom/google/b/b/p;->p:Ljava/util/Queue;

    .line 255
    iget-wide v0, p0, Lcom/google/b/b/p;->n:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, 0x1

    :goto_a
    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/b/b/p;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_19

    const/4 v0, 0x1

    :goto_b
    if-eqz v0, :cond_1a

    :cond_1
    const/4 v0, 0x1

    :goto_c
    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/b/b/p;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1b

    const/4 v0, 0x1

    :goto_d
    if-eqz v0, :cond_1c

    :cond_2
    const/4 v0, 0x1

    :goto_e
    iget-object v1, p1, Lcom/google/b/b/e;->q:Lcom/google/b/a/cb;

    if-eqz v1, :cond_1d

    iget-object v0, p1, Lcom/google/b/b/e;->q:Lcom/google/b/a/cb;

    :goto_f
    iput-object v0, p0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    .line 256
    iget-object v2, p0, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    iget-wide v0, p0, Lcom/google/b/b/p;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1f

    const/4 v0, 0x1

    :goto_10
    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/google/b/b/p;->k:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-ltz v0, :cond_20

    const/4 v0, 0x1

    :goto_11
    if-eqz v0, :cond_21

    :cond_3
    const/4 v0, 0x1

    :goto_12
    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/b/b/p;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_22

    const/4 v0, 0x1

    :goto_13
    if-eqz v0, :cond_23

    :cond_4
    const/4 v0, 0x1

    :goto_14
    iget-wide v4, p0, Lcom/google/b/b/p;->n:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_24

    const/4 v1, 0x1

    :goto_15
    if-nez v1, :cond_6

    iget-wide v4, p0, Lcom/google/b/b/p;->n:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_25

    const/4 v1, 0x1

    :goto_16
    if-nez v1, :cond_5

    iget-wide v4, p0, Lcom/google/b/b/p;->o:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_26

    const/4 v1, 0x1

    :goto_17
    if-eqz v1, :cond_27

    :cond_5
    const/4 v1, 0x1

    :goto_18
    if-eqz v1, :cond_28

    :cond_6
    const/4 v1, 0x1

    :goto_19
    invoke-static {v2, v0, v1}, Lcom/google/b/b/x;->a(Lcom/google/b/b/av;ZZ)Lcom/google/b/b/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/p;->s:Lcom/google/b/b/x;

    .line 257
    iget-object v0, p1, Lcom/google/b/b/e;->r:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    .line 258
    iput-object p2, p0, Lcom/google/b/b/p;->t:Lcom/google/b/b/k;

    .line 260
    iget v0, p1, Lcom/google/b/b/e;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_29

    const/16 v0, 0x10

    :goto_1a
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 261
    iget-wide v2, p0, Lcom/google/b/b/p;->k:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2a

    const/4 v1, 0x1

    :goto_1b
    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/b/b/p;->l:Lcom/google/b/b/cn;

    sget-object v2, Lcom/google/b/b/j;->a:Lcom/google/b/b/j;

    if-eq v1, v2, :cond_2b

    const/4 v1, 0x1

    :goto_1c
    if-nez v1, :cond_7

    .line 262
    iget-wide v2, p0, Lcom/google/b/b/p;->k:J

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 270
    :cond_7
    const/4 v2, 0x0

    .line 271
    const/4 v1, 0x1

    .line 272
    :goto_1d
    iget v3, p0, Lcom/google/b/b/p;->f:I

    if-ge v1, v3, :cond_2d

    iget-wide v4, p0, Lcom/google/b/b/p;->k:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2c

    const/4 v3, 0x1

    :goto_1e
    if-eqz v3, :cond_8

    mul-int/lit8 v3, v1, 0x14

    int-to-long v4, v3

    iget-wide v6, p0, Lcom/google/b/b/p;->k:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_2d

    .line 273
    :cond_8
    add-int/lit8 v2, v2, 0x1

    .line 274
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1d

    .line 236
    :cond_9
    iget v0, p1, Lcom/google/b/b/e;->e:I

    goto/16 :goto_0

    .line 241
    :cond_a
    if-nez v1, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move-object v0, v1

    goto/16 :goto_1

    .line 242
    :cond_c
    if-nez v1, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move-object v0, v1

    goto/16 :goto_2

    .line 244
    :cond_e
    iget-object v0, p1, Lcom/google/b/b/e;->h:Lcom/google/b/b/cn;

    if-nez v0, :cond_f

    iget-wide v0, p1, Lcom/google/b/b/e;->f:J

    goto/16 :goto_3

    :cond_f
    iget-wide v0, p1, Lcom/google/b/b/e;->g:J

    goto/16 :goto_3

    .line 245
    :cond_10
    if-nez v1, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move-object v0, v1

    goto/16 :goto_4

    .line 246
    :cond_12
    iget-wide v0, p1, Lcom/google/b/b/e;->l:J

    goto/16 :goto_5

    .line 247
    :cond_13
    iget-wide v0, p1, Lcom/google/b/b/e;->k:J

    goto/16 :goto_6

    .line 248
    :cond_14
    iget-wide v0, p1, Lcom/google/b/b/e;->m:J

    goto/16 :goto_7

    .line 250
    :cond_15
    if-nez v1, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    move-object v0, v1

    goto/16 :goto_8

    .line 251
    :cond_17
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto/16 :goto_9

    .line 255
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_d

    :cond_1c
    const/4 v0, 0x0

    goto/16 :goto_e

    :cond_1d
    if-eqz v0, :cond_1e

    invoke-static {}, Lcom/google/b/a/cb;->b()Lcom/google/b/a/cb;

    move-result-object v0

    goto/16 :goto_f

    :cond_1e
    sget-object v0, Lcom/google/b/b/e;->b:Lcom/google/b/a/cb;

    goto/16 :goto_f

    .line 256
    :cond_1f
    const/4 v0, 0x0

    goto/16 :goto_10

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_11

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_12

    :cond_22
    const/4 v0, 0x0

    goto/16 :goto_13

    :cond_23
    const/4 v0, 0x0

    goto/16 :goto_14

    :cond_24
    const/4 v1, 0x0

    goto/16 :goto_15

    :cond_25
    const/4 v1, 0x0

    goto/16 :goto_16

    :cond_26
    const/4 v1, 0x0

    goto/16 :goto_17

    :cond_27
    const/4 v1, 0x0

    goto/16 :goto_18

    :cond_28
    const/4 v1, 0x0

    goto/16 :goto_19

    .line 260
    :cond_29
    iget v0, p1, Lcom/google/b/b/e;->d:I

    goto/16 :goto_1a

    .line 261
    :cond_2a
    const/4 v1, 0x0

    goto/16 :goto_1b

    :cond_2b
    const/4 v1, 0x0

    goto/16 :goto_1c

    .line 272
    :cond_2c
    const/4 v3, 0x0

    goto/16 :goto_1e

    .line 276
    :cond_2d
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/b/b/p;->d:I

    .line 277
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/b/b/p;->c:I

    .line 279
    new-array v2, v1, [Lcom/google/b/b/as;

    iput-object v2, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    .line 281
    div-int v2, v0, v1

    .line 282
    mul-int v3, v2, v1

    if-ge v3, v0, :cond_33

    .line 283
    add-int/lit8 v0, v2, 0x1

    .line 286
    :goto_1f
    const/4 v3, 0x1

    .line 287
    :goto_20
    if-ge v3, v0, :cond_2e

    .line 288
    shl-int/lit8 v3, v3, 0x1

    goto :goto_20

    .line 291
    :cond_2e
    iget-wide v4, p0, Lcom/google/b/b/p;->k:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_30

    const/4 v0, 0x1

    :goto_21
    if-eqz v0, :cond_31

    .line 293
    iget-wide v4, p0, Lcom/google/b/b/p;->k:J

    int-to-long v6, v1

    div-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    .line 294
    iget-wide v6, p0, Lcom/google/b/b/p;->k:J

    int-to-long v0, v1

    rem-long v8, v6, v0

    .line 295
    const/4 v0, 0x0

    :goto_22
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    array-length v1, v1

    if-ge v0, v1, :cond_32

    .line 296
    int-to-long v6, v0

    cmp-long v1, v6, v8

    if-nez v1, :cond_2f

    .line 297
    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    .line 299
    :cond_2f
    iget-object v7, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget-object v1, p1, Lcom/google/b/b/e;->r:Lcom/google/b/a/bw;

    invoke-interface {v1}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/b/b/c;

    new-instance v1, Lcom/google/b/b/as;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/b/b/as;-><init>(Lcom/google/b/b/p;IJLcom/google/b/b/c;)V

    aput-object v1, v7, v0

    .line 295
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 291
    :cond_30
    const/4 v0, 0x0

    goto :goto_21

    .line 303
    :cond_31
    const/4 v0, 0x0

    :goto_23
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    array-length v1, v1

    if-ge v0, v1, :cond_32

    .line 304
    iget-object v7, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    const-wide/16 v4, -0x1

    iget-object v1, p1, Lcom/google/b/b/e;->r:Lcom/google/b/a/bw;

    invoke-interface {v1}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/b/b/c;

    new-instance v1, Lcom/google/b/b/as;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/b/b/as;-><init>(Lcom/google/b/b/p;IJLcom/google/b/b/c;)V

    aput-object v1, v7, v0

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 308
    :cond_32
    return-void

    :cond_33
    move v0, v2

    goto :goto_1f
.end method

.method static a(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1884
    sget-object v0, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    .line 1885
    invoke-interface {p0, v0}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/ar;)V

    .line 1886
    invoke-interface {p0, v0}, Lcom/google/b/b/ar;->b(Lcom/google/b/b/ar;)V

    .line 1887
    return-void
.end method

.method static a(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1878
    invoke-interface {p0, p1}, Lcom/google/b/b/ar;->a(Lcom/google/b/b/ar;)V

    .line 1879
    invoke-interface {p1, p0}, Lcom/google/b/b/ar;->b(Lcom/google/b/b/ar;)V

    .line 1880
    return-void
.end method

.method static b(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1897
    sget-object v0, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    .line 1898
    invoke-interface {p0, v0}, Lcom/google/b/b/ar;->c(Lcom/google/b/b/ar;)V

    .line 1899
    invoke-interface {p0, v0}, Lcom/google/b/b/ar;->d(Lcom/google/b/b/ar;)V

    .line 1900
    return-void
.end method

.method static b(Lcom/google/b/b/ar;Lcom/google/b/b/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1891
    invoke-interface {p0, p1}, Lcom/google/b/b/ar;->c(Lcom/google/b/b/ar;)V

    .line 1892
    invoke-interface {p1, p0}, Lcom/google/b/b/ar;->d(Lcom/google/b/b/ar;)V

    .line 1893
    return-void
.end method

.method static c()Lcom/google/b/b/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 730
    sget-object v0, Lcom/google/b/b/p;->u:Lcom/google/b/b/bf;

    return-object v0
.end method

.method static d()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 980
    sget-object v0, Lcom/google/b/b/aq;->a:Lcom/google/b/b/aq;

    return-object v0
.end method

.method static e()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1014
    sget-object v0, Lcom/google/b/b/p;->v:Ljava/util/Queue;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/google/b/b/p;->g:Lcom/google/b/a/x;

    invoke-virtual {v0, p1}, Lcom/google/b/a/x;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1798
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method final a()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 335
    iget-wide v2, p0, Lcom/google/b/b/p;->m:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/b/b/p;->k:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method final a(Lcom/google/b/b/ar;J)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1864
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1865
    :cond_0
    iget-wide v2, p0, Lcom/google/b/b/p;->m:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {p1}, Lcom/google/b/b/ar;->e()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/b/b/p;->m:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    .line 1871
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 1865
    goto :goto_0

    .line 1868
    :cond_3
    iget-wide v2, p0, Lcom/google/b/b/p;->n:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    invoke-interface {p1}, Lcom/google/b/b/ar;->h()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/b/b/p;->n:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_4
    move v0, v1

    .line 1871
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1868
    goto :goto_2
.end method

.method final b()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 343
    iget-wide v2, p0, Lcom/google/b/b/p;->n:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/b/b/p;->o:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public clear()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4142
    iget-object v5, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_b

    aget-object v7, v5, v4

    .line 4143
    iget v0, v7, Lcom/google/b/b/as;->b:I

    if-eqz v0, :cond_8

    invoke-virtual {v7}, Lcom/google/b/b/as;->lock()V

    :try_start_0
    iget-object v8, v7, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v3, v2

    :goto_1
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_2

    invoke-virtual {v8, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/b/ar;

    :goto_2
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/b/b/bf;->d()Z

    move-result v9

    if-eqz v9, :cond_0

    sget-object v9, Lcom/google/b/b/bz;->a:Lcom/google/b/b/bz;

    invoke-interface {v0}, Lcom/google/b/b/ar;->d()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v0}, Lcom/google/b/b/ar;->c()I

    invoke-interface {v0}, Lcom/google/b/b/ar;->a()Lcom/google/b/b/bf;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v9}, Lcom/google/b/b/as;->a(Ljava/lang/Object;Lcom/google/b/b/bf;Lcom/google/b/b/bz;)V

    :cond_0
    invoke-interface {v0}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v0

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_3
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, v7, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->i:Lcom/google/b/b/av;

    sget-object v3, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v0, v3, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, v7, Lcom/google/b/b/as;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_5
    iget-object v0, v7, Lcom/google/b/b/as;->a:Lcom/google/b/b/p;

    iget-object v0, v0, Lcom/google/b/b/p;->j:Lcom/google/b/b/av;

    sget-object v3, Lcom/google/b/b/av;->a:Lcom/google/b/b/av;

    if-eq v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, v7, Lcom/google/b/b/as;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_7
    iget-object v0, v7, Lcom/google/b/b/as;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, v7, Lcom/google/b/b/as;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, v7, Lcom/google/b/b/as;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, v7, Lcom/google/b/b/as;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v7, Lcom/google/b/b/as;->d:I

    const/4 v0, 0x0

    iput v0, v7, Lcom/google/b/b/as;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v7}, Lcom/google/b/b/as;->unlock()V

    invoke-virtual {v7}, Lcom/google/b/b/as;->b()V

    .line 4142
    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 4143
    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_5

    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lcom/google/b/b/as;->unlock()V

    invoke-virtual {v7}, Lcom/google/b/b/as;->b()V

    throw v0

    .line 4145
    :cond_b
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 4036
    if-nez p1, :cond_0

    .line 4037
    const/4 v0, 0x0

    .line 4040
    :goto_0
    return v0

    .line 4039
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4040
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/b/as;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 20
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 4046
    if-nez p1, :cond_0

    .line 4047
    const/4 v4, 0x0

    .line 4081
    :goto_0
    return v4

    .line 4055
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/b/b/p;->r:Lcom/google/b/a/cb;

    invoke-virtual {v4}, Lcom/google/b/a/cb;->a()J

    move-result-wide v14

    .line 4056
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    .line 4057
    const-wide/16 v8, -0x1

    .line 4058
    const/4 v4, 0x0

    move v10, v4

    move-wide v12, v8

    :goto_1
    const/4 v4, 0x3

    if-ge v10, v4, :cond_5

    .line 4059
    const-wide/16 v6, 0x0

    .line 4060
    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_4

    aget-object v7, v11, v6

    .line 4063
    iget v4, v7, Lcom/google/b/b/as;->b:I

    .line 4065
    iget-object v0, v7, Lcom/google/b/b/as;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v17, v0

    .line 4066
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 4067
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/b/b/ar;

    :goto_4
    if-eqz v4, :cond_2

    .line 4068
    invoke-virtual {v7, v4, v14, v15}, Lcom/google/b/b/as;->a(Lcom/google/b/b/ar;J)Ljava/lang/Object;

    move-result-object v18

    .line 4069
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/b/b/p;->h:Lcom/google/b/a/x;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/x;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 4070
    const/4 v4, 0x1

    goto :goto_0

    .line 4067
    :cond_1
    invoke-interface {v4}, Lcom/google/b/b/ar;->b()Lcom/google/b/b/ar;

    move-result-object v4

    goto :goto_4

    .line 4066
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 4074
    :cond_3
    iget v4, v7, Lcom/google/b/b/as;->d:I

    int-to-long v4, v4

    add-long/2addr v8, v4

    .line 4060
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    .line 4076
    :cond_4
    cmp-long v4, v8, v12

    if-eqz v4, :cond_5

    .line 4058
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v12, v8

    goto :goto_1

    .line 4081
    :cond_5
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 4178
    iget-object v0, p0, Lcom/google/b/b/p;->y:Ljava/util/Set;

    .line 4179
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/b/ah;

    invoke-direct {v0, p0, p0}, Lcom/google/b/b/ah;-><init>(Lcom/google/b/b/p;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/b/b/p;->y:Ljava/util/Set;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3855
    if-nez p1, :cond_0

    .line 3856
    const/4 v0, 0x0

    .line 3859
    :goto_0
    return-object v0

    .line 3858
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3859
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/b/as;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3815
    .line 3816
    iget-object v6, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    move v0, v1

    move-wide v2, v4

    .line 3817
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3818
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/b/as;->b:I

    if-eqz v7, :cond_1

    .line 3835
    :cond_0
    :goto_1
    return v1

    .line 3821
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/b/as;->d:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3817
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3824
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3825
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3826
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/b/as;->b:I

    if-nez v7, :cond_0

    .line 3829
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/b/b/as;->d:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3825
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3831
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3835
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 4159
    iget-object v0, p0, Lcom/google/b/b/p;->w:Ljava/util/Set;

    .line 4160
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/b/ak;

    invoke-direct {v0, p0, p0}, Lcom/google/b/b/ak;-><init>(Lcom/google/b/b/p;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/b/b/p;->w:Ljava/util/Set;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4086
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4087
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4088
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4089
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4101
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/b/b/p;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4104
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4093
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4094
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4095
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4096
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4108
    if-nez p1, :cond_0

    .line 4109
    const/4 v0, 0x0

    .line 4112
    :goto_0
    return-object v0

    .line 4111
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4112
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0}, Lcom/google/b/b/as;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 4116
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 4117
    :cond_0
    const/4 v0, 0x0

    .line 4120
    :goto_0
    return v0

    .line 4119
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4120
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/b/b/as;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4134
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4135
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4136
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4137
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 4124
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4125
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4126
    :cond_1
    if-nez p2, :cond_2

    .line 4127
    const/4 v0, 0x0

    .line 4130
    :goto_0
    return v0

    .line 4129
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4130
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v2, p0, Lcom/google/b/b/p;->d:I

    ushr-int v2, v0, v2

    iget v3, p0, Lcom/google/b/b/p;->c:I

    and-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 3849
    iget-object v1, p0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    aget-object v4, v1, v0

    iget v4, v4, Lcom/google/b/b/as;->b:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    const v0, 0x7fffffff

    :goto_1
    return v0

    :cond_1
    const-wide/32 v0, -0x80000000

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_2
    long-to-int v0, v2

    goto :goto_1
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 4168
    iget-object v0, p0, Lcom/google/b/b/p;->x:Ljava/util/Collection;

    .line 4169
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/b/b/bg;

    invoke-direct {v0, p0, p0}, Lcom/google/b/b/bg;-><init>(Lcom/google/b/b/p;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, Lcom/google/b/b/p;->x:Ljava/util/Collection;

    goto :goto_0
.end method
