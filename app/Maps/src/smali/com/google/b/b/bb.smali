.class Lcom/google/b/b/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/b/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/b/b/ar",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field final h:I

.field final i:Lcom/google/b/b/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile j:Lcom/google/b/b/bf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/b/b/ar;)V
    .locals 1
    .param p3    # Lcom/google/b/b/ar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1097
    invoke-static {}, Lcom/google/b/b/p;->c()Lcom/google/b/b/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/b/bb;->j:Lcom/google/b/b/bf;

    .line 1032
    iput-object p1, p0, Lcom/google/b/b/bb;->g:Ljava/lang/Object;

    .line 1033
    iput p2, p0, Lcom/google/b/b/bb;->h:I

    .line 1034
    iput-object p3, p0, Lcom/google/b/b/bb;->i:Lcom/google/b/b/ar;

    .line 1035
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/b/b/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/b/b/bb;->j:Lcom/google/b/b/bf;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1056
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/b/b/bf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/bf",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1104
    iput-object p1, p0, Lcom/google/b/b/bb;->j:Lcom/google/b/b/bf;

    .line 1105
    return-void
.end method

.method public final b()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/b/b/bb;->i:Lcom/google/b/b/ar;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1108
    iget v0, p0, Lcom/google/b/b/bb;->h:I

    return v0
.end method

.method public c(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1082
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/b/b/bb;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public d(Lcom/google/b/b/ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1090
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    .prologue
    .line 1044
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1052
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1060
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .locals 1

    .prologue
    .line 1070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1078
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()Lcom/google/b/b/ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/b/ar",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1086
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
