.class public Lcom/google/b/b/an;
.super Lcom/google/b/b/ao;
.source "PG"

# interfaces
.implements Lcom/google/b/b/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/b/ao",
        "<TK;TV;>;",
        "Lcom/google/b/b/o",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Lcom/google/b/b/e;Lcom/google/b/b/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/b/e",
            "<-TK;-TV;>;",
            "Lcom/google/b/b/k",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4702
    new-instance v0, Lcom/google/b/b/p;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/b/b/k;

    invoke-direct {v0, p1, p2}, Lcom/google/b/b/p;-><init>(Lcom/google/b/b/e;Lcom/google/b/b/k;)V

    invoke-direct {p0, v0}, Lcom/google/b/b/ao;-><init>(Lcom/google/b/b/p;)V

    .line 4703
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 4728
    invoke-virtual {p0, p1}, Lcom/google/b/b/an;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 4713
    :try_start_0
    iget-object v0, p0, Lcom/google/b/b/an;->a:Lcom/google/b/b/p;

    iget-object v1, v0, Lcom/google/b/b/p;->t:Lcom/google/b/b/k;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4714
    :catch_0
    move-exception v0

    .line 4715
    new-instance v1, Lcom/google/b/j/a/s;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/j/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 4713
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1}, Lcom/google/b/b/p;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, v0, Lcom/google/b/b/p;->e:[Lcom/google/b/b/as;

    iget v4, v0, Lcom/google/b/b/p;->d:I

    ushr-int v4, v2, v4

    iget v0, v0, Lcom/google/b/b/p;->c:I

    and-int/2addr v0, v4

    aget-object v0, v3, v0

    invoke-virtual {v0, p1, v2, v1}, Lcom/google/b/b/as;->a(Ljava/lang/Object;ILcom/google/b/b/k;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 4737
    new-instance v0, Lcom/google/b/b/al;

    iget-object v1, p0, Lcom/google/b/b/an;->a:Lcom/google/b/b/p;

    invoke-direct {v0, v1}, Lcom/google/b/b/al;-><init>(Lcom/google/b/b/p;)V

    return-object v0
.end method
