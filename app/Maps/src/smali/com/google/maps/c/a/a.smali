.class public final Lcom/google/maps/c/a/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/c/a/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/c/a/a;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/maps/c/a/a;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/maps/c/a/b;

    invoke-direct {v0}, Lcom/google/maps/c/a/b;-><init>()V

    sput-object v0, Lcom/google/maps/c/a/a;->PARSER:Lcom/google/n/ax;

    .line 203
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/c/a/a;->f:Lcom/google/n/aw;

    .line 359
    new-instance v0, Lcom/google/maps/c/a/a;

    invoke-direct {v0}, Lcom/google/maps/c/a/a;-><init>()V

    sput-object v0, Lcom/google/maps/c/a/a;->c:Lcom/google/maps/c/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 167
    iput-byte v0, p0, Lcom/google/maps/c/a/a;->d:B

    .line 186
    iput v0, p0, Lcom/google/maps/c/a/a;->e:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/c/a/a;->b:I

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/maps/c/a/a;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 30
    const/4 v0, 0x0

    .line 31
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 33
    sparse-switch v3, :sswitch_data_0

    .line 38
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 40
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    iget v3, p0, Lcom/google/maps/c/a/a;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/c/a/a;->a:I

    .line 46
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/c/a/a;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/c/a/a;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/c/a/a;->au:Lcom/google/n/bn;

    .line 58
    return-void

    .line 53
    :catch_1
    move-exception v0

    .line 54
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 55
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 167
    iput-byte v0, p0, Lcom/google/maps/c/a/a;->d:B

    .line 186
    iput v0, p0, Lcom/google/maps/c/a/a;->e:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Lcom/google/maps/c/a/a;->newBuilder()Lcom/google/maps/c/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/c/a/a;
    .locals 1

    .prologue
    .line 362
    sget-object v0, Lcom/google/maps/c/a/a;->c:Lcom/google/maps/c/a/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/c/a/c;
    .locals 1

    .prologue
    .line 265
    new-instance v0, Lcom/google/maps/c/a/c;

    invoke-direct {v0}, Lcom/google/maps/c/a/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/c/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/google/maps/c/a/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 179
    invoke-virtual {p0}, Lcom/google/maps/c/a/a;->c()I

    .line 180
    iget v0, p0, Lcom/google/maps/c/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 181
    iget v0, p0, Lcom/google/maps/c/a/a;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 183
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/c/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 184
    return-void

    .line 181
    :cond_1
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 169
    iget-byte v1, p0, Lcom/google/maps/c/a/a;->d:B

    .line 170
    if-ne v1, v0, :cond_0

    .line 174
    :goto_0
    return v0

    .line 171
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/c/a/a;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 188
    iget v1, p0, Lcom/google/maps/c/a/a;->e:I

    .line 189
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 198
    :goto_0
    return v0

    .line 192
    :cond_0
    iget v1, p0, Lcom/google/maps/c/a/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 193
    iget v1, p0, Lcom/google/maps/c/a/a;->b:I

    .line 194
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/google/maps/c/a/a;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    iput v0, p0, Lcom/google/maps/c/a/a;->e:I

    goto :goto_0

    .line 194
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/c/a/a;->newBuilder()Lcom/google/maps/c/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/c/a/a;->newBuilder()Lcom/google/maps/c/a/c;

    move-result-object v0

    return-object v0
.end method
