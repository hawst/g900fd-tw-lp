.class public final Lcom/google/maps/c/a/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/c/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/c/a/a;",
        "Lcom/google/maps/c/a/c;",
        ">;",
        "Lcom/google/maps/c/a/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 283
    sget-object v0, Lcom/google/maps/c/a/a;->c:Lcom/google/maps/c/a/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 284
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 309
    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 314
    :goto_0
    return-object p0

    .line 310
    :cond_0
    iget v1, p1, Lcom/google/maps/c/a/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 311
    iget v0, p1, Lcom/google/maps/c/a/a;->b:I

    iget v1, p0, Lcom/google/maps/c/a/c;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/c/a/c;->a:I

    iput v0, p0, Lcom/google/maps/c/a/c;->b:I

    .line 313
    :cond_1
    iget-object v0, p1, Lcom/google/maps/c/a/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 310
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/maps/c/a/c;->c()Lcom/google/maps/c/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 275
    check-cast p1, Lcom/google/maps/c/a/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/c/a/a;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 297
    new-instance v2, Lcom/google/maps/c/a/a;

    invoke-direct {v2, p0}, Lcom/google/maps/c/a/a;-><init>(Lcom/google/n/v;)V

    .line 298
    iget v3, p0, Lcom/google/maps/c/a/c;->a:I

    .line 299
    const/4 v1, 0x0

    .line 300
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 303
    :goto_0
    iget v1, p0, Lcom/google/maps/c/a/c;->b:I

    iput v1, v2, Lcom/google/maps/c/a/a;->b:I

    .line 304
    iput v0, v2, Lcom/google/maps/c/a/a;->a:I

    .line 305
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
