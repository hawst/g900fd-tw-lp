.class public final Lcom/google/maps/e/a/j;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/e/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/e/a/h;",
        "Lcom/google/maps/e/a/j;",
        ">;",
        "Lcom/google/maps/e/a/m;"
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:F

.field private G:F

.field private H:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:I

.field private O:D

.field private P:D

.field private Q:Z

.field private R:I

.field private S:F

.field private T:I

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private V:I

.field private W:Ljava/lang/Object;

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private Y:J

.field private a:I

.field private b:I

.field private c:D

.field private d:D

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:D

.field private j:D

.field private k:D

.field private l:D

.field private m:D

.field private n:D

.field private o:D

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/google/n/ao;

.field private s:D

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private u:D

.field private v:D

.field private w:D

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private y:D

.field private z:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5968
    sget-object v0, Lcom/google/maps/e/a/h;->Y:Lcom/google/maps/e/a/h;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6911
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    .line 7048
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    .line 7184
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/j;->r:Lcom/google/n/ao;

    .line 7276
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    .line 7509
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    .line 8221
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/e/a/j;->R:I

    .line 8321
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    .line 8419
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/e/a/j;->W:Ljava/lang/Object;

    .line 8495
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    .line 5969
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/e/a/h;)Lcom/google/maps/e/a/j;
    .locals 9

    .prologue
    const v8, 0x8000

    const/high16 v7, 0x200000

    const/high16 v6, 0x20000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6287
    invoke-static {}, Lcom/google/maps/e/a/h;->d()Lcom/google/maps/e/a/h;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6478
    :goto_0
    return-object p0

    .line 6288
    :cond_0
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6289
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->c:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->c:D

    .line 6291
    :cond_1
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 6292
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->d:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->d:D

    .line 6294
    :cond_2
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 6295
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->e:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->e:D

    .line 6297
    :cond_3
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 6298
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->f:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->f:D

    .line 6300
    :cond_4
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 6301
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->g:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->g:D

    .line 6303
    :cond_5
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 6304
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->h:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->h:D

    .line 6306
    :cond_6
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_30

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 6307
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->i:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->i:D

    .line 6309
    :cond_7
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_31

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 6310
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->j:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->j:D

    .line 6312
    :cond_8
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_32

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 6313
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->k:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->k:D

    .line 6315
    :cond_9
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_33

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 6316
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->l:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->l:D

    .line 6318
    :cond_a
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_34

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 6319
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->m:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->m:D

    .line 6321
    :cond_b
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_35

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 6322
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->n:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->n:D

    .line 6324
    :cond_c
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_36

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 6325
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->o:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->o:D

    .line 6327
    :cond_d
    iget-object v2, p1, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 6328
    iget-object v2, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_37

    .line 6329
    iget-object v2, p1, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    .line 6330
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v2, v2, -0x2001

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6337
    :cond_e
    :goto_e
    iget-object v2, p1, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 6338
    iget-object v2, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 6339
    iget-object v2, p1, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    .line 6340
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v2, v2, -0x4001

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6347
    :cond_f
    :goto_f
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_3b

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 6348
    iget-object v2, p0, Lcom/google/maps/e/a/j;->r:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6349
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/2addr v2, v8

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6351
    :cond_10
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_3c

    move v2, v0

    :goto_11
    if-eqz v2, :cond_11

    .line 6352
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->s:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v5, 0x10000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->s:D

    .line 6354
    :cond_11
    iget-object v2, p1, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_12

    .line 6355
    iget-object v2, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 6356
    iget-object v2, p1, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    .line 6357
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    const v3, -0x20001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6364
    :cond_12
    :goto_12
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/2addr v2, v8

    if-ne v2, v8, :cond_3f

    move v2, v0

    :goto_13
    if-eqz v2, :cond_13

    .line 6365
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->u:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v5, 0x40000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->u:D

    .line 6367
    :cond_13
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_40

    move v2, v0

    :goto_14
    if-eqz v2, :cond_14

    .line 6368
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->v:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v5, 0x80000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->v:D

    .line 6370
    :cond_14
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_41

    move v2, v0

    :goto_15
    if-eqz v2, :cond_15

    .line 6371
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->w:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v5, 0x100000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->w:D

    .line 6373
    :cond_15
    iget-object v2, p1, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_16

    .line 6374
    iget-object v2, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_42

    .line 6375
    iget-object v2, p1, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    .line 6376
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    const v3, -0x200001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6383
    :cond_16
    :goto_16
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_44

    move v2, v0

    :goto_17
    if-eqz v2, :cond_17

    .line 6384
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->y:D

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v5, 0x400000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/e/a/j;->a:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->y:D

    .line 6386
    :cond_17
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_45

    move v2, v0

    :goto_18
    if-eqz v2, :cond_18

    .line 6387
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->z:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x800000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->z:Z

    .line 6389
    :cond_18
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_46

    move v2, v0

    :goto_19
    if-eqz v2, :cond_19

    .line 6390
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->A:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x1000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->A:Z

    .line 6392
    :cond_19
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_47

    move v2, v0

    :goto_1a
    if-eqz v2, :cond_1a

    .line 6393
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->B:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x2000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->B:Z

    .line 6395
    :cond_1a
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_48

    move v2, v0

    :goto_1b
    if-eqz v2, :cond_1b

    .line 6396
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->C:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x4000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->C:Z

    .line 6398
    :cond_1b
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    const/high16 v3, 0x800000

    if-ne v2, v3, :cond_49

    move v2, v0

    :goto_1c
    if-eqz v2, :cond_1c

    .line 6399
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->D:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x8000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->D:Z

    .line 6401
    :cond_1c
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000000

    if-ne v2, v3, :cond_4a

    move v2, v0

    :goto_1d
    if-eqz v2, :cond_1d

    .line 6402
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->E:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->E:Z

    .line 6404
    :cond_1d
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_4b

    move v2, v0

    :goto_1e
    if-eqz v2, :cond_1e

    .line 6405
    iget v2, p1, Lcom/google/maps/e/a/h;->F:F

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput v2, p0, Lcom/google/maps/e/a/j;->F:F

    .line 6407
    :cond_1e
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_4c

    move v2, v0

    :goto_1f
    if-eqz v2, :cond_1f

    .line 6408
    iget v2, p1, Lcom/google/maps/e/a/h;->G:F

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput v2, p0, Lcom/google/maps/e/a/j;->G:F

    .line 6410
    :cond_1f
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_4d

    move v2, v0

    :goto_20
    if-eqz v2, :cond_20

    .line 6411
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->H:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v4, -0x80000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/e/a/j;->a:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->H:Z

    .line 6413
    :cond_20
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_4e

    move v2, v0

    :goto_21
    if-eqz v2, :cond_21

    .line 6414
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->I:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->J:Z

    .line 6416
    :cond_21
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_4f

    move v2, v0

    :goto_22
    if-eqz v2, :cond_22

    .line 6417
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->J:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->K:Z

    .line 6419
    :cond_22
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_50

    move v2, v0

    :goto_23
    if-eqz v2, :cond_23

    .line 6420
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->K:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->L:Z

    .line 6422
    :cond_23
    iget v2, p1, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_51

    move v2, v0

    :goto_24
    if-eqz v2, :cond_24

    .line 6423
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->L:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->M:Z

    .line 6425
    :cond_24
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_52

    move v2, v0

    :goto_25
    if-eqz v2, :cond_25

    .line 6426
    iget v2, p1, Lcom/google/maps/e/a/h;->M:I

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput v2, p0, Lcom/google/maps/e/a/j;->N:I

    .line 6428
    :cond_25
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_53

    move v2, v0

    :goto_26
    if-eqz v2, :cond_26

    .line 6429
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->N:D

    iget v4, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/e/a/j;->b:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->O:D

    .line 6431
    :cond_26
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_54

    move v2, v0

    :goto_27
    if-eqz v2, :cond_27

    .line 6432
    iget-wide v2, p1, Lcom/google/maps/e/a/h;->O:D

    iget v4, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/e/a/j;->b:I

    iput-wide v2, p0, Lcom/google/maps/e/a/j;->P:D

    .line 6434
    :cond_27
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_55

    move v2, v0

    :goto_28
    if-eqz v2, :cond_28

    .line 6435
    iget-boolean v2, p1, Lcom/google/maps/e/a/h;->P:Z

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput-boolean v2, p0, Lcom/google/maps/e/a/j;->Q:Z

    .line 6437
    :cond_28
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_56

    move v2, v0

    :goto_29
    if-eqz v2, :cond_58

    .line 6438
    iget v2, p1, Lcom/google/maps/e/a/h;->Q:I

    invoke-static {v2}, Lcom/google/maps/e/a/k;->a(I)Lcom/google/maps/e/a/k;

    move-result-object v2

    if-nez v2, :cond_29

    sget-object v2, Lcom/google/maps/e/a/k;->a:Lcom/google/maps/e/a/k;

    :cond_29
    if-nez v2, :cond_57

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2a
    move v2, v1

    .line 6288
    goto/16 :goto_1

    :cond_2b
    move v2, v1

    .line 6291
    goto/16 :goto_2

    :cond_2c
    move v2, v1

    .line 6294
    goto/16 :goto_3

    :cond_2d
    move v2, v1

    .line 6297
    goto/16 :goto_4

    :cond_2e
    move v2, v1

    .line 6300
    goto/16 :goto_5

    :cond_2f
    move v2, v1

    .line 6303
    goto/16 :goto_6

    :cond_30
    move v2, v1

    .line 6306
    goto/16 :goto_7

    :cond_31
    move v2, v1

    .line 6309
    goto/16 :goto_8

    :cond_32
    move v2, v1

    .line 6312
    goto/16 :goto_9

    :cond_33
    move v2, v1

    .line 6315
    goto/16 :goto_a

    :cond_34
    move v2, v1

    .line 6318
    goto/16 :goto_b

    :cond_35
    move v2, v1

    .line 6321
    goto/16 :goto_c

    :cond_36
    move v2, v1

    .line 6324
    goto/16 :goto_d

    .line 6332
    :cond_37
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-eq v2, v3, :cond_38

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6333
    :cond_38
    iget-object v2, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_e

    .line 6342
    :cond_39
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-eq v2, v3, :cond_3a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6343
    :cond_3a
    iget-object v2, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_f

    :cond_3b
    move v2, v1

    .line 6347
    goto/16 :goto_10

    :cond_3c
    move v2, v1

    .line 6351
    goto/16 :goto_11

    .line 6359
    :cond_3d
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/2addr v2, v6

    if-eq v2, v6, :cond_3e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6360
    :cond_3e
    iget-object v2, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_12

    :cond_3f
    move v2, v1

    .line 6364
    goto/16 :goto_13

    :cond_40
    move v2, v1

    .line 6367
    goto/16 :goto_14

    :cond_41
    move v2, v1

    .line 6370
    goto/16 :goto_15

    .line 6378
    :cond_42
    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/2addr v2, v7

    if-eq v2, v7, :cond_43

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->a:I

    or-int/2addr v2, v7

    iput v2, p0, Lcom/google/maps/e/a/j;->a:I

    .line 6379
    :cond_43
    iget-object v2, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_16

    :cond_44
    move v2, v1

    .line 6383
    goto/16 :goto_17

    :cond_45
    move v2, v1

    .line 6386
    goto/16 :goto_18

    :cond_46
    move v2, v1

    .line 6389
    goto/16 :goto_19

    :cond_47
    move v2, v1

    .line 6392
    goto/16 :goto_1a

    :cond_48
    move v2, v1

    .line 6395
    goto/16 :goto_1b

    :cond_49
    move v2, v1

    .line 6398
    goto/16 :goto_1c

    :cond_4a
    move v2, v1

    .line 6401
    goto/16 :goto_1d

    :cond_4b
    move v2, v1

    .line 6404
    goto/16 :goto_1e

    :cond_4c
    move v2, v1

    .line 6407
    goto/16 :goto_1f

    :cond_4d
    move v2, v1

    .line 6410
    goto/16 :goto_20

    :cond_4e
    move v2, v1

    .line 6413
    goto/16 :goto_21

    :cond_4f
    move v2, v1

    .line 6416
    goto/16 :goto_22

    :cond_50
    move v2, v1

    .line 6419
    goto/16 :goto_23

    :cond_51
    move v2, v1

    .line 6422
    goto/16 :goto_24

    :cond_52
    move v2, v1

    .line 6425
    goto/16 :goto_25

    :cond_53
    move v2, v1

    .line 6428
    goto/16 :goto_26

    :cond_54
    move v2, v1

    .line 6431
    goto/16 :goto_27

    :cond_55
    move v2, v1

    .line 6434
    goto/16 :goto_28

    :cond_56
    move v2, v1

    .line 6437
    goto/16 :goto_29

    .line 6438
    :cond_57
    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iget v2, v2, Lcom/google/maps/e/a/k;->e:I

    iput v2, p0, Lcom/google/maps/e/a/j;->R:I

    .line 6440
    :cond_58
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_60

    move v2, v0

    :goto_2a
    if-eqz v2, :cond_59

    .line 6441
    iget v2, p1, Lcom/google/maps/e/a/h;->R:F

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput v2, p0, Lcom/google/maps/e/a/j;->S:F

    .line 6443
    :cond_59
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_61

    move v2, v0

    :goto_2b
    if-eqz v2, :cond_5a

    .line 6444
    iget v2, p1, Lcom/google/maps/e/a/h;->S:I

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput v2, p0, Lcom/google/maps/e/a/j;->T:I

    .line 6446
    :cond_5a
    iget-object v2, p1, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5b

    .line 6447
    iget-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_62

    .line 6448
    iget-object v2, p1, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    .line 6449
    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    .line 6456
    :cond_5b
    :goto_2c
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_64

    move v2, v0

    :goto_2d
    if-eqz v2, :cond_5c

    .line 6457
    iget v2, p1, Lcom/google/maps/e/a/h;->U:I

    iget v3, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/e/a/j;->b:I

    iput v2, p0, Lcom/google/maps/e/a/j;->V:I

    .line 6459
    :cond_5c
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_65

    move v2, v0

    :goto_2e
    if-eqz v2, :cond_5d

    .line 6460
    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    .line 6461
    iget-object v2, p1, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->W:Ljava/lang/Object;

    .line 6464
    :cond_5d
    iget-object v2, p1, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5e

    .line 6465
    iget-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_66

    .line 6466
    iget-object v2, p1, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    .line 6467
    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, -0x4001

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    .line 6474
    :cond_5e
    :goto_2f
    iget v2, p1, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_68

    :goto_30
    if-eqz v0, :cond_5f

    .line 6475
    iget-wide v0, p1, Lcom/google/maps/e/a/h;->X:J

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/2addr v2, v8

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    iput-wide v0, p0, Lcom/google/maps/e/a/j;->Y:J

    .line 6477
    :cond_5f
    iget-object v0, p1, Lcom/google/maps/e/a/h;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_60
    move v2, v1

    .line 6440
    goto/16 :goto_2a

    :cond_61
    move v2, v1

    .line 6443
    goto/16 :goto_2b

    .line 6451
    :cond_62
    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-eq v2, v3, :cond_63

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    .line 6452
    :cond_63
    iget-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2c

    :cond_64
    move v2, v1

    .line 6456
    goto/16 :goto_2d

    :cond_65
    move v2, v1

    .line 6459
    goto :goto_2e

    .line 6469
    :cond_66
    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-eq v2, v3, :cond_67

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    .line 6470
    :cond_67
    iget-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2f

    :cond_68
    move v0, v1

    .line 6474
    goto :goto_30
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x10000

    const/4 v2, 0x0

    const/high16 v10, -0x80000000

    const/4 v1, 0x1

    const v9, 0x8000

    .line 5960
    new-instance v3, Lcom/google/maps/e/a/h;

    invoke-direct {v3, p0}, Lcom/google/maps/e/a/h;-><init>(Lcom/google/n/v;)V

    iget v4, p0, Lcom/google/maps/e/a/j;->a:I

    iget v5, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit8 v0, v4, 0x1

    if-ne v0, v1, :cond_2f

    move v0, v1

    :goto_0
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->c:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->c:D

    and-int/lit8 v6, v4, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->d:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->d:D

    and-int/lit8 v6, v4, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->e:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->e:D

    and-int/lit8 v6, v4, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->f:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->f:D

    and-int/lit8 v6, v4, 0x10

    const/16 v7, 0x10

    if-ne v6, v7, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->g:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->g:D

    and-int/lit8 v6, v4, 0x20

    const/16 v7, 0x20

    if-ne v6, v7, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->h:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->h:D

    and-int/lit8 v6, v4, 0x40

    const/16 v7, 0x40

    if-ne v6, v7, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->i:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->i:D

    and-int/lit16 v6, v4, 0x80

    const/16 v7, 0x80

    if-ne v6, v7, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->j:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->j:D

    and-int/lit16 v6, v4, 0x100

    const/16 v7, 0x100

    if-ne v6, v7, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->k:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->k:D

    and-int/lit16 v6, v4, 0x200

    const/16 v7, 0x200

    if-ne v6, v7, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->l:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->l:D

    and-int/lit16 v6, v4, 0x400

    const/16 v7, 0x400

    if-ne v6, v7, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->m:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->m:D

    and-int/lit16 v6, v4, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->n:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->n:D

    and-int/lit16 v6, v4, 0x1000

    const/16 v7, 0x1000

    if-ne v6, v7, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->o:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->o:D

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v6, v6, 0x2000

    const/16 v7, 0x2000

    if-ne v6, v7, :cond_c

    iget-object v6, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v6, v6, -0x2001

    iput v6, p0, Lcom/google/maps/e/a/j;->a:I

    :cond_c
    iget-object v6, p0, Lcom/google/maps/e/a/j;->p:Ljava/util/List;

    iput-object v6, v3, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v6, v6, 0x4000

    const/16 v7, 0x4000

    if-ne v6, v7, :cond_d

    iget-object v6, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    and-int/lit16 v6, v6, -0x4001

    iput v6, p0, Lcom/google/maps/e/a/j;->a:I

    :cond_d
    iget-object v6, p0, Lcom/google/maps/e/a/j;->q:Ljava/util/List;

    iput-object v6, v3, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    and-int v6, v4, v9

    if-ne v6, v9, :cond_e

    or-int/lit16 v0, v0, 0x2000

    :cond_e
    iget-object v6, v3, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/maps/e/a/j;->r:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/maps/e/a/j;->r:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int v6, v4, v11

    if-ne v6, v11, :cond_f

    or-int/lit16 v0, v0, 0x4000

    :cond_f
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->s:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->s:D

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v7, 0x20000

    and-int/2addr v6, v7

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_10

    iget-object v6, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    const v7, -0x20001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/maps/e/a/j;->a:I

    :cond_10
    iget-object v6, p0, Lcom/google/maps/e/a/j;->t:Ljava/util/List;

    iput-object v6, v3, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    const/high16 v6, 0x40000

    and-int/2addr v6, v4

    const/high16 v7, 0x40000

    if-ne v6, v7, :cond_11

    or-int/2addr v0, v9

    :cond_11
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->u:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->u:D

    const/high16 v6, 0x80000

    and-int/2addr v6, v4

    const/high16 v7, 0x80000

    if-ne v6, v7, :cond_12

    or-int/2addr v0, v11

    :cond_12
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->v:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->v:D

    const/high16 v6, 0x100000

    and-int/2addr v6, v4

    const/high16 v7, 0x100000

    if-ne v6, v7, :cond_13

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    :cond_13
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->w:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->w:D

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    const/high16 v7, 0x200000

    and-int/2addr v6, v7

    const/high16 v7, 0x200000

    if-ne v6, v7, :cond_14

    iget-object v6, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    iget v6, p0, Lcom/google/maps/e/a/j;->a:I

    const v7, -0x200001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/maps/e/a/j;->a:I

    :cond_14
    iget-object v6, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    iput-object v6, v3, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    const/high16 v6, 0x400000

    and-int/2addr v6, v4

    const/high16 v7, 0x400000

    if-ne v6, v7, :cond_15

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    :cond_15
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->y:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->y:D

    const/high16 v6, 0x800000

    and-int/2addr v6, v4

    const/high16 v7, 0x800000

    if-ne v6, v7, :cond_16

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    :cond_16
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->z:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->z:Z

    const/high16 v6, 0x1000000

    and-int/2addr v6, v4

    const/high16 v7, 0x1000000

    if-ne v6, v7, :cond_17

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    :cond_17
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->A:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->A:Z

    const/high16 v6, 0x2000000

    and-int/2addr v6, v4

    const/high16 v7, 0x2000000

    if-ne v6, v7, :cond_18

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    :cond_18
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->B:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->B:Z

    const/high16 v6, 0x4000000

    and-int/2addr v6, v4

    const/high16 v7, 0x4000000

    if-ne v6, v7, :cond_19

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    :cond_19
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->C:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->C:Z

    const/high16 v6, 0x8000000

    and-int/2addr v6, v4

    const/high16 v7, 0x8000000

    if-ne v6, v7, :cond_1a

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    :cond_1a
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->D:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->D:Z

    const/high16 v6, 0x10000000

    and-int/2addr v6, v4

    const/high16 v7, 0x10000000

    if-ne v6, v7, :cond_1b

    const/high16 v6, 0x1000000

    or-int/2addr v0, v6

    :cond_1b
    iget-boolean v6, p0, Lcom/google/maps/e/a/j;->E:Z

    iput-boolean v6, v3, Lcom/google/maps/e/a/h;->E:Z

    const/high16 v6, 0x20000000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000000

    if-ne v6, v7, :cond_1c

    const/high16 v6, 0x2000000

    or-int/2addr v0, v6

    :cond_1c
    iget v6, p0, Lcom/google/maps/e/a/j;->F:F

    iput v6, v3, Lcom/google/maps/e/a/h;->F:F

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v6, v7, :cond_1d

    const/high16 v6, 0x4000000

    or-int/2addr v0, v6

    :cond_1d
    iget v6, p0, Lcom/google/maps/e/a/j;->G:F

    iput v6, v3, Lcom/google/maps/e/a/h;->G:F

    and-int/2addr v4, v10

    if-ne v4, v10, :cond_1e

    const/high16 v4, 0x8000000

    or-int/2addr v0, v4

    :cond_1e
    iget-boolean v4, p0, Lcom/google/maps/e/a/j;->H:Z

    iput-boolean v4, v3, Lcom/google/maps/e/a/h;->H:Z

    and-int/lit8 v4, v5, 0x1

    if-ne v4, v1, :cond_1f

    const/high16 v4, 0x10000000

    or-int/2addr v0, v4

    :cond_1f
    iget-boolean v4, p0, Lcom/google/maps/e/a/j;->J:Z

    iput-boolean v4, v3, Lcom/google/maps/e/a/h;->I:Z

    and-int/lit8 v4, v5, 0x2

    const/4 v6, 0x2

    if-ne v4, v6, :cond_20

    const/high16 v4, 0x20000000

    or-int/2addr v0, v4

    :cond_20
    iget-boolean v4, p0, Lcom/google/maps/e/a/j;->K:Z

    iput-boolean v4, v3, Lcom/google/maps/e/a/h;->J:Z

    and-int/lit8 v4, v5, 0x4

    const/4 v6, 0x4

    if-ne v4, v6, :cond_21

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v0, v4

    :cond_21
    iget-boolean v4, p0, Lcom/google/maps/e/a/j;->L:Z

    iput-boolean v4, v3, Lcom/google/maps/e/a/h;->K:Z

    and-int/lit8 v4, v5, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_22

    or-int/2addr v0, v10

    :cond_22
    iget-boolean v4, p0, Lcom/google/maps/e/a/j;->M:Z

    iput-boolean v4, v3, Lcom/google/maps/e/a/h;->L:Z

    and-int/lit8 v4, v5, 0x10

    const/16 v6, 0x10

    if-ne v4, v6, :cond_2e

    :goto_1
    iget v2, p0, Lcom/google/maps/e/a/j;->N:I

    iput v2, v3, Lcom/google/maps/e/a/h;->M:I

    and-int/lit8 v2, v5, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_23

    or-int/lit8 v1, v1, 0x2

    :cond_23
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->O:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->N:D

    and-int/lit8 v2, v5, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_24

    or-int/lit8 v1, v1, 0x4

    :cond_24
    iget-wide v6, p0, Lcom/google/maps/e/a/j;->P:D

    iput-wide v6, v3, Lcom/google/maps/e/a/h;->O:D

    and-int/lit16 v2, v5, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_25

    or-int/lit8 v1, v1, 0x8

    :cond_25
    iget-boolean v2, p0, Lcom/google/maps/e/a/j;->Q:Z

    iput-boolean v2, v3, Lcom/google/maps/e/a/h;->P:Z

    and-int/lit16 v2, v5, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_26

    or-int/lit8 v1, v1, 0x10

    :cond_26
    iget v2, p0, Lcom/google/maps/e/a/j;->R:I

    iput v2, v3, Lcom/google/maps/e/a/h;->Q:I

    and-int/lit16 v2, v5, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_27

    or-int/lit8 v1, v1, 0x20

    :cond_27
    iget v2, p0, Lcom/google/maps/e/a/j;->S:F

    iput v2, v3, Lcom/google/maps/e/a/h;->R:F

    and-int/lit16 v2, v5, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_28

    or-int/lit8 v1, v1, 0x40

    :cond_28
    iget v2, p0, Lcom/google/maps/e/a/j;->T:I

    iput v2, v3, Lcom/google/maps/e/a/h;->S:I

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_29

    iget-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    :cond_29
    iget-object v2, p0, Lcom/google/maps/e/a/j;->U:Ljava/util/List;

    iput-object v2, v3, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    and-int/lit16 v2, v5, 0x1000

    const/16 v4, 0x1000

    if-ne v2, v4, :cond_2a

    or-int/lit16 v1, v1, 0x80

    :cond_2a
    iget v2, p0, Lcom/google/maps/e/a/j;->V:I

    iput v2, v3, Lcom/google/maps/e/a/h;->U:I

    and-int/lit16 v2, v5, 0x2000

    const/16 v4, 0x2000

    if-ne v2, v4, :cond_2b

    or-int/lit16 v1, v1, 0x100

    :cond_2b
    iget-object v2, p0, Lcom/google/maps/e/a/j;->W:Ljava/lang/Object;

    iput-object v2, v3, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v4, 0x4000

    if-ne v2, v4, :cond_2c

    iget-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/e/a/j;->b:I

    and-int/lit16 v2, v2, -0x4001

    iput v2, p0, Lcom/google/maps/e/a/j;->b:I

    :cond_2c
    iget-object v2, p0, Lcom/google/maps/e/a/j;->X:Ljava/util/List;

    iput-object v2, v3, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    and-int v2, v5, v9

    if-ne v2, v9, :cond_2d

    or-int/lit16 v1, v1, 0x200

    :cond_2d
    iget-wide v4, p0, Lcom/google/maps/e/a/j;->Y:J

    iput-wide v4, v3, Lcom/google/maps/e/a/h;->X:J

    iput v0, v3, Lcom/google/maps/e/a/h;->a:I

    iput v1, v3, Lcom/google/maps/e/a/h;->b:I

    return-object v3

    :cond_2e
    move v1, v2

    goto/16 :goto_1

    :cond_2f
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5960
    check-cast p1, Lcom/google/maps/e/a/h;

    invoke-virtual {p0, p1}, Lcom/google/maps/e/a/j;->a(Lcom/google/maps/e/a/h;)Lcom/google/maps/e/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6482
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 6483
    iget-object v0, p0, Lcom/google/maps/e/a/j;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/e/a/b;->d()Lcom/google/maps/e/a/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/b;

    invoke-virtual {v0}, Lcom/google/maps/e/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6488
    :goto_1
    return v2

    .line 6482
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 6488
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
