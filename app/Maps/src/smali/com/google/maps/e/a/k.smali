.class public final enum Lcom/google/maps/e/a/k;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/e/a/k;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/e/a/k;

.field public static final enum b:Lcom/google/maps/e/a/k;

.field public static final enum c:Lcom/google/maps/e/a/k;

.field public static final enum d:Lcom/google/maps/e/a/k;

.field private static final synthetic f:[Lcom/google/maps/e/a/k;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4562
    new-instance v0, Lcom/google/maps/e/a/k;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/e/a/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/k;->a:Lcom/google/maps/e/a/k;

    .line 4566
    new-instance v0, Lcom/google/maps/e/a/k;

    const-string v1, "OVERLAPS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/e/a/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/k;->b:Lcom/google/maps/e/a/k;

    .line 4570
    new-instance v0, Lcom/google/maps/e/a/k;

    const-string v1, "CONTAINS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/e/a/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/k;->c:Lcom/google/maps/e/a/k;

    .line 4574
    new-instance v0, Lcom/google/maps/e/a/k;

    const-string v1, "NEAR"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/maps/e/a/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/k;->d:Lcom/google/maps/e/a/k;

    .line 4557
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/e/a/k;

    sget-object v1, Lcom/google/maps/e/a/k;->a:Lcom/google/maps/e/a/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/e/a/k;->b:Lcom/google/maps/e/a/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/e/a/k;->c:Lcom/google/maps/e/a/k;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/e/a/k;->d:Lcom/google/maps/e/a/k;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/e/a/k;->f:[Lcom/google/maps/e/a/k;

    .line 4614
    new-instance v0, Lcom/google/maps/e/a/l;

    invoke-direct {v0}, Lcom/google/maps/e/a/l;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4623
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4624
    iput p3, p0, Lcom/google/maps/e/a/k;->e:I

    .line 4625
    return-void
.end method

.method public static a(I)Lcom/google/maps/e/a/k;
    .locals 1

    .prologue
    .line 4600
    sparse-switch p0, :sswitch_data_0

    .line 4605
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4601
    :sswitch_0
    sget-object v0, Lcom/google/maps/e/a/k;->a:Lcom/google/maps/e/a/k;

    goto :goto_0

    .line 4602
    :sswitch_1
    sget-object v0, Lcom/google/maps/e/a/k;->b:Lcom/google/maps/e/a/k;

    goto :goto_0

    .line 4603
    :sswitch_2
    sget-object v0, Lcom/google/maps/e/a/k;->c:Lcom/google/maps/e/a/k;

    goto :goto_0

    .line 4604
    :sswitch_3
    sget-object v0, Lcom/google/maps/e/a/k;->d:Lcom/google/maps/e/a/k;

    goto :goto_0

    .line 4600
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/e/a/k;
    .locals 1

    .prologue
    .line 4557
    const-class v0, Lcom/google/maps/e/a/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/k;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/e/a/k;
    .locals 1

    .prologue
    .line 4557
    sget-object v0, Lcom/google/maps/e/a/k;->f:[Lcom/google/maps/e/a/k;

    invoke-virtual {v0}, [Lcom/google/maps/e/a/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/e/a/k;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4596
    iget v0, p0, Lcom/google/maps/e/a/k;->e:I

    return v0
.end method
