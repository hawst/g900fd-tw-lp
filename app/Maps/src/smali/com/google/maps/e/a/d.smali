.class public final Lcom/google/maps/e/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/e/a/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/e/a/b;",
        "Lcom/google/maps/e/a/d;",
        ">;",
        "Lcom/google/maps/e/a/g;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:D


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 434
    sget-object v0, Lcom/google/maps/e/a/b;->d:Lcom/google/maps/e/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 491
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/e/a/d;->b:I

    .line 435
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/e/a/b;)Lcom/google/maps/e/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 466
    invoke-static {}, Lcom/google/maps/e/a/b;->d()Lcom/google/maps/e/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 474
    :goto_0
    return-object p0

    .line 467
    :cond_0
    iget v2, p1, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 468
    iget v2, p1, Lcom/google/maps/e/a/b;->b:I

    invoke-static {v2}, Lcom/google/maps/e/a/e;->a(I)Lcom/google/maps/e/a/e;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/e/a/e;->a:Lcom/google/maps/e/a/e;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 467
    goto :goto_1

    .line 468
    :cond_3
    iget v3, p0, Lcom/google/maps/e/a/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/e/a/d;->a:I

    iget v2, v2, Lcom/google/maps/e/a/e;->n:I

    iput v2, p0, Lcom/google/maps/e/a/d;->b:I

    .line 470
    :cond_4
    iget v2, p1, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 471
    iget-wide v0, p1, Lcom/google/maps/e/a/b;->c:D

    iget v2, p0, Lcom/google/maps/e/a/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/e/a/d;->a:I

    iput-wide v0, p0, Lcom/google/maps/e/a/d;->c:D

    .line 473
    :cond_5
    iget-object v0, p1, Lcom/google/maps/e/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 470
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 426
    new-instance v2, Lcom/google/maps/e/a/b;

    invoke-direct {v2, p0}, Lcom/google/maps/e/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/e/a/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/e/a/d;->b:I

    iput v1, v2, Lcom/google/maps/e/a/b;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/maps/e/a/d;->c:D

    iput-wide v4, v2, Lcom/google/maps/e/a/b;->c:D

    iput v0, v2, Lcom/google/maps/e/a/b;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 426
    check-cast p1, Lcom/google/maps/e/a/b;

    invoke-virtual {p0, p1}, Lcom/google/maps/e/a/d;->a(Lcom/google/maps/e/a/b;)Lcom/google/maps/e/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 478
    iget v2, p0, Lcom/google/maps/e/a/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 486
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 478
    goto :goto_0

    .line 482
    :cond_2
    iget v2, p0, Lcom/google/maps/e/a/d;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 486
    goto :goto_1

    :cond_3
    move v2, v0

    .line 482
    goto :goto_2
.end method
