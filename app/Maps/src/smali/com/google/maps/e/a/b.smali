.class public final Lcom/google/maps/e/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/e/a/g;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/e/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/e/a/b;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:D

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/google/maps/e/a/c;

    invoke-direct {v0}, Lcom/google/maps/e/a/c;-><init>()V

    sput-object v0, Lcom/google/maps/e/a/b;->PARSER:Lcom/google/n/ax;

    .line 354
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/e/a/b;->g:Lcom/google/n/aw;

    .line 563
    new-instance v0, Lcom/google/maps/e/a/b;

    invoke-direct {v0}, Lcom/google/maps/e/a/b;-><init>()V

    sput-object v0, Lcom/google/maps/e/a/b;->d:Lcom/google/maps/e/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 44
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 303
    iput-byte v0, p0, Lcom/google/maps/e/a/b;->e:B

    .line 333
    iput v0, p0, Lcom/google/maps/e/a/b;->f:I

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/e/a/b;->b:I

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/e/a/b;->c:D

    .line 47
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 53
    invoke-direct {p0}, Lcom/google/maps/e/a/b;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 58
    const/4 v0, 0x0

    .line 59
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 60
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 61
    sparse-switch v3, :sswitch_data_0

    .line 66
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 68
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 73
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 74
    invoke-static {v3}, Lcom/google/maps/e/a/e;->a(I)Lcom/google/maps/e/a/e;

    move-result-object v4

    .line 75
    if-nez v4, :cond_1

    .line 76
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/e/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 78
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/maps/e/a/b;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/e/a/b;->a:I

    .line 79
    iput v3, p0, Lcom/google/maps/e/a/b;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 94
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    :sswitch_2
    :try_start_4
    iget v3, p0, Lcom/google/maps/e/a/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/e/a/b;->a:I

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/e/a/b;->c:D
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/b;->au:Lcom/google/n/bn;

    .line 97
    return-void

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 42
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 303
    iput-byte v0, p0, Lcom/google/maps/e/a/b;->e:B

    .line 333
    iput v0, p0, Lcom/google/maps/e/a/b;->f:I

    .line 43
    return-void
.end method

.method public static d()Lcom/google/maps/e/a/b;
    .locals 1

    .prologue
    .line 566
    sget-object v0, Lcom/google/maps/e/a/b;->d:Lcom/google/maps/e/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/e/a/d;
    .locals 1

    .prologue
    .line 416
    new-instance v0, Lcom/google/maps/e/a/d;

    invoke-direct {v0}, Lcom/google/maps/e/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/e/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/google/maps/e/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 323
    invoke-virtual {p0}, Lcom/google/maps/e/a/b;->c()I

    .line 324
    iget v0, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 325
    iget v0, p0, Lcom/google/maps/e/a/b;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 327
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 328
    iget-wide v0, p0, Lcom/google/maps/e/a/b;->c:D

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/maps/e/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 331
    return-void

    .line 325
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 305
    iget-byte v2, p0, Lcom/google/maps/e/a/b;->e:B

    .line 306
    if-ne v2, v0, :cond_0

    .line 318
    :goto_0
    return v0

    .line 307
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 309
    :cond_1
    iget v2, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 310
    iput-byte v1, p0, Lcom/google/maps/e/a/b;->e:B

    move v0, v1

    .line 311
    goto :goto_0

    :cond_2
    move v2, v1

    .line 309
    goto :goto_1

    .line 313
    :cond_3
    iget v2, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 314
    iput-byte v1, p0, Lcom/google/maps/e/a/b;->e:B

    move v0, v1

    .line 315
    goto :goto_0

    :cond_4
    move v2, v1

    .line 313
    goto :goto_2

    .line 317
    :cond_5
    iput-byte v0, p0, Lcom/google/maps/e/a/b;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 335
    iget v0, p0, Lcom/google/maps/e/a/b;->f:I

    .line 336
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 349
    :goto_0
    return v0

    .line 339
    :cond_0
    iget v0, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 340
    iget v0, p0, Lcom/google/maps/e/a/b;->b:I

    .line 341
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 343
    :goto_2
    iget v2, p0, Lcom/google/maps/e/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 344
    iget-wide v2, p0, Lcom/google/maps/e/a/b;->c:D

    .line 345
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 347
    :cond_1
    iget-object v1, p0, Lcom/google/maps/e/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    iput v0, p0, Lcom/google/maps/e/a/b;->f:I

    goto :goto_0

    .line 341
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/maps/e/a/b;->newBuilder()Lcom/google/maps/e/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/e/a/d;->a(Lcom/google/maps/e/a/b;)Lcom/google/maps/e/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/maps/e/a/b;->newBuilder()Lcom/google/maps/e/a/d;

    move-result-object v0

    return-object v0
.end method
