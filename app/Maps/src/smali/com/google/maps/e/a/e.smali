.class public final enum Lcom/google/maps/e/a/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/e/a/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/e/a/e;

.field public static final enum b:Lcom/google/maps/e/a/e;

.field public static final enum c:Lcom/google/maps/e/a/e;

.field public static final enum d:Lcom/google/maps/e/a/e;

.field public static final enum e:Lcom/google/maps/e/a/e;

.field public static final enum f:Lcom/google/maps/e/a/e;

.field public static final enum g:Lcom/google/maps/e/a/e;

.field public static final enum h:Lcom/google/maps/e/a/e;

.field public static final enum i:Lcom/google/maps/e/a/e;

.field public static final enum j:Lcom/google/maps/e/a/e;

.field public static final enum k:Lcom/google/maps/e/a/e;

.field public static final enum l:Lcom/google/maps/e/a/e;

.field public static final enum m:Lcom/google/maps/e/a/e;

.field private static final synthetic o:[Lcom/google/maps/e/a/e;


# instance fields
.field final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 122
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_NON_PRECISE_MATCH"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->a:Lcom/google/maps/e/a/e;

    .line 126
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_FEATURE_IS_IN_DIFFERENT_COUNTRY"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->b:Lcom/google/maps/e/a/e;

    .line 130
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_COUNTY_OR_PROVINCE_MATCHES_BUT_CITY_DOES_NOT"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->c:Lcom/google/maps/e/a/e;

    .line 134
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_INTERPOLATED_SEGMENTS_DEMOTED"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->d:Lcom/google/maps/e/a/e;

    .line 138
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_INTERPOLATED_SEGMENTS_DEMOTED_IN_NZ_DEPRECATED"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->e:Lcom/google/maps/e/a/e;

    .line 142
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_GEOCODED_ADDRESS_DEMOTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->f:Lcom/google/maps/e/a/e;

    .line 146
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_TYPE_NEAR_MATCH"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->g:Lcom/google/maps/e/a/e;

    .line 150
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_UNLIKELY_LOCATION_NAME_MATCH"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->h:Lcom/google/maps/e/a/e;

    .line 154
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_HAS_MISMATCHED_STREET_AFFIX"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->i:Lcom/google/maps/e/a/e;

    .line 158
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_HAS_MISMATCHED_DIRECTIONAL_HIT"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->j:Lcom/google/maps/e/a/e;

    .line 162
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_LANGUAGE_MISMATCH"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->k:Lcom/google/maps/e/a/e;

    .line 166
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_HAS_LANDMARKS_WITHOUT_SUPPORTING_TERMS"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->l:Lcom/google/maps/e/a/e;

    .line 170
    new-instance v0, Lcom/google/maps/e/a/e;

    const-string v1, "PENALTY_HAS_NEAR_ROUTES_MATCH"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/e/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/e/a/e;->m:Lcom/google/maps/e/a/e;

    .line 117
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/maps/e/a/e;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/e/a/e;->a:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/e/a/e;->b:Lcom/google/maps/e/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/e/a/e;->c:Lcom/google/maps/e/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/e/a/e;->d:Lcom/google/maps/e/a/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/e/a/e;->e:Lcom/google/maps/e/a/e;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/e/a/e;->f:Lcom/google/maps/e/a/e;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/e/a/e;->g:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/e/a/e;->h:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/e/a/e;->i:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/e/a/e;->j:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/e/a/e;->k:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/e/a/e;->l:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/e/a/e;->m:Lcom/google/maps/e/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/e/a/e;->o:[Lcom/google/maps/e/a/e;

    .line 255
    new-instance v0, Lcom/google/maps/e/a/f;

    invoke-direct {v0}, Lcom/google/maps/e/a/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 265
    iput p3, p0, Lcom/google/maps/e/a/e;->n:I

    .line 266
    return-void
.end method

.method public static a(I)Lcom/google/maps/e/a/e;
    .locals 1

    .prologue
    .line 232
    packed-switch p0, :pswitch_data_0

    .line 246
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 233
    :pswitch_0
    sget-object v0, Lcom/google/maps/e/a/e;->a:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 234
    :pswitch_1
    sget-object v0, Lcom/google/maps/e/a/e;->b:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 235
    :pswitch_2
    sget-object v0, Lcom/google/maps/e/a/e;->c:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 236
    :pswitch_3
    sget-object v0, Lcom/google/maps/e/a/e;->d:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 237
    :pswitch_4
    sget-object v0, Lcom/google/maps/e/a/e;->e:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 238
    :pswitch_5
    sget-object v0, Lcom/google/maps/e/a/e;->f:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 239
    :pswitch_6
    sget-object v0, Lcom/google/maps/e/a/e;->g:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 240
    :pswitch_7
    sget-object v0, Lcom/google/maps/e/a/e;->h:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 241
    :pswitch_8
    sget-object v0, Lcom/google/maps/e/a/e;->i:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 242
    :pswitch_9
    sget-object v0, Lcom/google/maps/e/a/e;->j:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 243
    :pswitch_a
    sget-object v0, Lcom/google/maps/e/a/e;->k:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 244
    :pswitch_b
    sget-object v0, Lcom/google/maps/e/a/e;->l:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 245
    :pswitch_c
    sget-object v0, Lcom/google/maps/e/a/e;->m:Lcom/google/maps/e/a/e;

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/e/a/e;
    .locals 1

    .prologue
    .line 117
    const-class v0, Lcom/google/maps/e/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/e/a/e;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/maps/e/a/e;->o:[Lcom/google/maps/e/a/e;

    invoke-virtual {v0}, [Lcom/google/maps/e/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/e/a/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/maps/e/a/e;->n:I

    return v0
.end method
