.class public final Lcom/google/maps/e/a/h;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/e/a/m;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/e/a/h;",
            ">;"
        }
    .end annotation
.end field

.field static final Y:Lcom/google/maps/e/a/h;

.field private static volatile ab:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field A:Z

.field B:Z

.field C:Z

.field D:Z

.field E:Z

.field F:F

.field G:F

.field H:Z

.field I:Z

.field J:Z

.field K:Z

.field L:Z

.field M:I

.field N:D

.field O:D

.field P:Z

.field Q:I

.field R:F

.field S:I

.field T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field U:I

.field V:Ljava/lang/Object;

.field W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field X:J

.field private Z:B

.field a:I

.field private aa:I

.field b:I

.field c:D

.field d:D

.field e:D

.field f:D

.field g:D

.field h:D

.field i:D

.field j:D

.field k:D

.field l:D

.field m:D

.field n:D

.field o:D

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field r:Lcom/google/n/ao;

.field s:D

.field t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field u:D

.field v:D

.field w:D

.field x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field y:D

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4539
    new-instance v0, Lcom/google/maps/e/a/i;

    invoke-direct {v0}, Lcom/google/maps/e/a/i;-><init>()V

    sput-object v0, Lcom/google/maps/e/a/h;->PARSER:Lcom/google/n/ax;

    .line 5888
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/e/a/h;->ab:Lcom/google/n/aw;

    .line 8597
    new-instance v0, Lcom/google/maps/e/a/h;

    invoke-direct {v0}, Lcom/google/maps/e/a/h;-><init>()V

    sput-object v0, Lcom/google/maps/e/a/h;->Y:Lcom/google/maps/e/a/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 4136
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    .line 5507
    iput-byte v6, p0, Lcom/google/maps/e/a/h;->Z:B

    .line 5673
    iput v6, p0, Lcom/google/maps/e/a/h;->aa:I

    .line 4137
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->c:D

    .line 4138
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->d:D

    .line 4139
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->e:D

    .line 4140
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->f:D

    .line 4141
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->g:D

    .line 4142
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->h:D

    .line 4143
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->i:D

    .line 4144
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->j:D

    .line 4145
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->k:D

    .line 4146
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->l:D

    .line 4147
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->m:D

    .line 4148
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->n:D

    .line 4149
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->o:D

    .line 4150
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    .line 4151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    .line 4152
    iget-object v0, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4153
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->s:D

    .line 4154
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    .line 4155
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->u:D

    .line 4156
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->v:D

    .line 4157
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->w:D

    .line 4158
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    .line 4159
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->y:D

    .line 4160
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->z:Z

    .line 4161
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->A:Z

    .line 4162
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->B:Z

    .line 4163
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->C:Z

    .line 4164
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->D:Z

    .line 4165
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->E:Z

    .line 4166
    iput v5, p0, Lcom/google/maps/e/a/h;->F:F

    .line 4167
    iput v5, p0, Lcom/google/maps/e/a/h;->G:F

    .line 4168
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->H:Z

    .line 4169
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->I:Z

    .line 4170
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->J:Z

    .line 4171
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->K:Z

    .line 4172
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->L:Z

    .line 4173
    iput v4, p0, Lcom/google/maps/e/a/h;->M:I

    .line 4174
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->N:D

    .line 4175
    iput-wide v2, p0, Lcom/google/maps/e/a/h;->O:D

    .line 4176
    iput-boolean v4, p0, Lcom/google/maps/e/a/h;->P:Z

    .line 4177
    iput v4, p0, Lcom/google/maps/e/a/h;->Q:I

    .line 4178
    iput v5, p0, Lcom/google/maps/e/a/h;->R:F

    .line 4179
    iput v4, p0, Lcom/google/maps/e/a/h;->S:I

    .line 4180
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 4181
    iput v4, p0, Lcom/google/maps/e/a/h;->U:I

    .line 4182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    .line 4183
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 4184
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/e/a/h;->X:J

    .line 4185
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    .line 4191
    invoke-direct {p0}, Lcom/google/maps/e/a/h;-><init>()V

    .line 4192
    const/4 v2, 0x0

    .line 4193
    const/4 v1, 0x0

    .line 4195
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 4197
    const/4 v0, 0x0

    move v3, v0

    .line 4198
    :cond_0
    :goto_0
    if-nez v3, :cond_22

    .line 4199
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 4200
    sparse-switch v0, :sswitch_data_0

    .line 4205
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4207
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 4202
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 4203
    goto :goto_0

    .line 4212
    :sswitch_1
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4213
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->m:D
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4512
    :catch_0
    move-exception v0

    .line 4513
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4518
    :catchall_0
    move-exception v0

    const/high16 v3, 0x200000

    and-int/2addr v3, v2

    const/high16 v5, 0x200000

    if-ne v3, v5, :cond_1

    .line 4519
    iget-object v3, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    .line 4521
    :cond_1
    and-int/lit16 v3, v2, 0x2000

    const/16 v5, 0x2000

    if-ne v3, v5, :cond_2

    .line 4522
    iget-object v3, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    .line 4524
    :cond_2
    and-int/lit16 v3, v2, 0x4000

    const/16 v5, 0x4000

    if-ne v3, v5, :cond_3

    .line 4525
    iget-object v3, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    .line 4527
    :cond_3
    and-int/lit16 v3, v1, 0x4000

    const/16 v5, 0x4000

    if-ne v3, v5, :cond_4

    .line 4528
    iget-object v3, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 4530
    :cond_4
    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_5

    .line 4531
    iget-object v2, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    .line 4533
    :cond_5
    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_6

    .line 4534
    iget-object v1, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 4536
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/e/a/h;->au:Lcom/google/n/bn;

    throw v0

    .line 4217
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4218
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->n:D
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 4514
    :catch_1
    move-exception v0

    .line 4515
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 4516
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4222
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4223
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->o:D

    goto/16 :goto_0

    .line 4227
    :sswitch_4
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x10000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4228
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->v:D

    goto/16 :goto_0

    .line 4232
    :sswitch_5
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4233
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->w:D

    goto/16 :goto_0

    .line 4237
    :sswitch_6
    const/high16 v0, 0x200000

    and-int/2addr v0, v2

    const/high16 v5, 0x200000

    if-eq v0, v5, :cond_7

    .line 4238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    .line 4240
    const/high16 v0, 0x200000

    or-int/2addr v2, v0

    .line 4242
    :cond_7
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 4243
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 4242
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4247
    :sswitch_7
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x40000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4248
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->y:D

    goto/16 :goto_0

    .line 4252
    :sswitch_8
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x80000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4253
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->z:Z

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 4257
    :sswitch_9
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x100000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4258
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->A:Z

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 4262
    :sswitch_a
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x1000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4263
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->E:Z

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    .line 4267
    :sswitch_b
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x2000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4268
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->F:F

    goto/16 :goto_0

    .line 4272
    :sswitch_c
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4273
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->c:D

    goto/16 :goto_0

    .line 4277
    :sswitch_d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4278
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->d:D

    goto/16 :goto_0

    .line 4282
    :sswitch_e
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4283
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->e:D

    goto/16 :goto_0

    .line 4287
    :sswitch_f
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4288
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->g:D

    goto/16 :goto_0

    .line 4292
    :sswitch_10
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4293
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->l:D

    goto/16 :goto_0

    .line 4297
    :sswitch_11
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4298
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->P:Z

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    .line 4302
    :sswitch_12
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4303
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->j:D

    goto/16 :goto_0

    .line 4307
    :sswitch_13
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4308
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->k:D

    goto/16 :goto_0

    .line 4312
    :sswitch_14
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x800000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4313
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->D:Z

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    .line 4317
    :sswitch_15
    and-int/lit16 v0, v2, 0x2000

    const/16 v5, 0x2000

    if-eq v0, v5, :cond_d

    .line 4318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    .line 4320
    or-int/lit16 v2, v2, 0x2000

    .line 4322
    :cond_d
    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 4323
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 4322
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4327
    :sswitch_16
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x20000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4328
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->J:Z

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_6

    .line 4332
    :sswitch_17
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x40000000    # 2.0f

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4333
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->K:Z

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x0

    goto :goto_7

    .line 4337
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 4338
    invoke-static {v0}, Lcom/google/maps/e/a/k;->a(I)Lcom/google/maps/e/a/k;

    move-result-object v5

    .line 4339
    if-nez v5, :cond_10

    .line 4340
    const/16 v5, 0x18

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 4342
    :cond_10
    iget v5, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4343
    iput v0, p0, Lcom/google/maps/e/a/h;->Q:I

    goto/16 :goto_0

    .line 4348
    :sswitch_19
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4349
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->R:F

    goto/16 :goto_0

    .line 4353
    :sswitch_1a
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4354
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->S:I

    goto/16 :goto_0

    .line 4358
    :sswitch_1b
    and-int/lit16 v0, v2, 0x4000

    const/16 v5, 0x4000

    if-eq v0, v5, :cond_11

    .line 4359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    .line 4361
    or-int/lit16 v2, v2, 0x4000

    .line 4363
    :cond_11
    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 4364
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 4363
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4368
    :sswitch_1c
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4369
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->f:D

    goto/16 :goto_0

    .line 4373
    :sswitch_1d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4374
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->i:D

    goto/16 :goto_0

    .line 4378
    :sswitch_1e
    and-int/lit16 v0, v1, 0x4000

    const/16 v5, 0x4000

    if-eq v0, v5, :cond_12

    .line 4379
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 4380
    or-int/lit16 v1, v1, 0x4000

    .line 4382
    :cond_12
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4386
    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 4387
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 4388
    and-int/lit16 v0, v1, 0x4000

    const/16 v6, 0x4000

    if-eq v0, v6, :cond_13

    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_14

    const/4 v0, -0x1

    :goto_8
    if-lez v0, :cond_13

    .line 4389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 4390
    or-int/lit16 v1, v1, 0x4000

    .line 4392
    :cond_13
    :goto_9
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_15

    const/4 v0, -0x1

    :goto_a
    if-lez v0, :cond_16

    .line 4393
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 4388
    :cond_14
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_8

    .line 4392
    :cond_15
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_a

    .line 4395
    :cond_16
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 4399
    :sswitch_20
    iget-object v0, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 4400
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    goto/16 :goto_0

    .line 4404
    :sswitch_21
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x200000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4405
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->B:Z

    goto/16 :goto_0

    :cond_17
    const/4 v0, 0x0

    goto :goto_b

    .line 4409
    :sswitch_22
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, -0x80000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4410
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_c
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->L:Z

    goto/16 :goto_0

    :cond_18
    const/4 v0, 0x0

    goto :goto_c

    .line 4414
    :sswitch_23
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4415
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->s:D

    goto/16 :goto_0

    .line 4419
    :sswitch_24
    const/high16 v0, 0x20000

    and-int/2addr v0, v2

    const/high16 v5, 0x20000

    if-eq v0, v5, :cond_19

    .line 4420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    .line 4422
    const/high16 v0, 0x20000

    or-int/2addr v2, v0

    .line 4424
    :cond_19
    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 4425
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 4424
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4429
    :sswitch_25
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x400000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4430
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->C:Z

    goto/16 :goto_0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_d

    .line 4434
    :sswitch_26
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4435
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->u:D

    goto/16 :goto_0

    .line 4439
    :sswitch_27
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4440
    iget v5, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4441
    iput-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4445
    :sswitch_28
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x4000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4446
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->G:F

    goto/16 :goto_0

    .line 4450
    :sswitch_29
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4451
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->U:I

    goto/16 :goto_0

    .line 4455
    :sswitch_2a
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4456
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/e/a/h;->M:I

    goto/16 :goto_0

    .line 4460
    :sswitch_2b
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x8000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4461
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->H:Z

    goto/16 :goto_0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_e

    .line 4465
    :sswitch_2c
    and-int/lit16 v0, v1, 0x800

    const/16 v5, 0x800

    if-eq v0, v5, :cond_1c

    .line 4466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 4467
    or-int/lit16 v1, v1, 0x800

    .line 4469
    :cond_1c
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4473
    :sswitch_2d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 4474
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 4475
    and-int/lit16 v0, v1, 0x800

    const/16 v6, 0x800

    if-eq v0, v6, :cond_1d

    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_1e

    const/4 v0, -0x1

    :goto_f
    if-lez v0, :cond_1d

    .line 4476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 4477
    or-int/lit16 v1, v1, 0x800

    .line 4479
    :cond_1d
    :goto_10
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_1f

    const/4 v0, -0x1

    :goto_11
    if-lez v0, :cond_20

    .line 4480
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 4475
    :cond_1e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_f

    .line 4479
    :cond_1f
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_11

    .line 4482
    :cond_20
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 4486
    :sswitch_2e
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4487
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->X:J

    goto/16 :goto_0

    .line 4491
    :sswitch_2f
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v5, 0x10000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4492
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    :goto_12
    iput-boolean v0, p0, Lcom/google/maps/e/a/h;->I:Z

    goto/16 :goto_0

    :cond_21
    const/4 v0, 0x0

    goto :goto_12

    .line 4496
    :sswitch_30
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4497
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->N:D

    goto/16 :goto_0

    .line 4501
    :sswitch_31
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/e/a/h;->a:I

    .line 4502
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->h:D

    goto/16 :goto_0

    .line 4506
    :sswitch_32
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/e/a/h;->b:I

    .line 4507
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/maps/e/a/h;->O:D
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4518
    :cond_22
    const/high16 v0, 0x200000

    and-int/2addr v0, v2

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_23

    .line 4519
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    .line 4521
    :cond_23
    and-int/lit16 v0, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_24

    .line 4522
    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    .line 4524
    :cond_24
    and-int/lit16 v0, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_25

    .line 4525
    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    .line 4527
    :cond_25
    and-int/lit16 v0, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_26

    .line 4528
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 4530
    :cond_26
    const/high16 v0, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_27

    .line 4531
    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    .line 4533
    :cond_27
    and-int/lit16 v0, v1, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_28

    .line 4534
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 4536
    :cond_28
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->au:Lcom/google/n/bn;

    .line 4537
    return-void

    .line 4200
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x39 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5d -> :sswitch_b
        0x61 -> :sswitch_c
        0x69 -> :sswitch_d
        0x71 -> :sswitch_e
        0x79 -> :sswitch_f
        0x81 -> :sswitch_10
        0x88 -> :sswitch_11
        0x91 -> :sswitch_12
        0x99 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xcd -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe1 -> :sswitch_1c
        0xe9 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf2 -> :sswitch_1f
        0xfa -> :sswitch_20
        0x100 -> :sswitch_21
        0x108 -> :sswitch_22
        0x111 -> :sswitch_23
        0x11a -> :sswitch_24
        0x120 -> :sswitch_25
        0x129 -> :sswitch_26
        0x132 -> :sswitch_27
        0x13d -> :sswitch_28
        0x140 -> :sswitch_29
        0x148 -> :sswitch_2a
        0x150 -> :sswitch_2b
        0x168 -> :sswitch_2c
        0x16a -> :sswitch_2d
        0x170 -> :sswitch_2e
        0x178 -> :sswitch_2f
        0x181 -> :sswitch_30
        0x189 -> :sswitch_31
        0x191 -> :sswitch_32
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 4134
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    .line 5507
    iput-byte v1, p0, Lcom/google/maps/e/a/h;->Z:B

    .line 5673
    iput v1, p0, Lcom/google/maps/e/a/h;->aa:I

    .line 4135
    return-void
.end method

.method public static d()Lcom/google/maps/e/a/h;
    .locals 1

    .prologue
    .line 8600
    sget-object v0, Lcom/google/maps/e/a/h;->Y:Lcom/google/maps/e/a/h;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/e/a/j;
    .locals 1

    .prologue
    .line 5950
    new-instance v0, Lcom/google/maps/e/a/j;

    invoke-direct {v0}, Lcom/google/maps/e/a/j;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/e/a/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4551
    sget-object v0, Lcom/google/maps/e/a/h;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5525
    invoke-virtual {p0}, Lcom/google/maps/e/a/h;->c()I

    .line 5526
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    .line 5527
    iget-wide v0, p0, Lcom/google/maps/e/a/h;->m:D

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5529
    :cond_0
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_1

    .line 5530
    iget-wide v0, p0, Lcom/google/maps/e/a/h;->n:D

    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5532
    :cond_1
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_2

    .line 5533
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->o:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5535
    :cond_2
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_3

    .line 5536
    iget-wide v0, p0, Lcom/google/maps/e/a/h;->v:D

    invoke-static {v8, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5538
    :cond_3
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_4

    .line 5539
    iget-wide v0, p0, Lcom/google/maps/e/a/h;->w:D

    invoke-static {v7, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    :cond_4
    move v1, v2

    .line 5541
    :goto_0
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 5542
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5541
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5544
    :cond_5
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_6

    .line 5545
    const/4 v0, 0x7

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->y:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5547
    :cond_6
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_7

    .line 5548
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->z:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_14

    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5550
    :cond_7
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_8

    .line 5551
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->A:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_15

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5553
    :cond_8
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_9

    .line 5554
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->E:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_16

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5556
    :cond_9
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_a

    .line 5557
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/maps/e/a/h;->F:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 5559
    :cond_a
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 5560
    const/16 v0, 0xc

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->c:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5562
    :cond_b
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_c

    .line 5563
    const/16 v0, 0xd

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->d:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5565
    :cond_c
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_d

    .line 5566
    const/16 v0, 0xe

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->e:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5568
    :cond_d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_e

    .line 5569
    const/16 v0, 0xf

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->g:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5571
    :cond_e
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 5572
    const/16 v0, 0x10

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->l:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5574
    :cond_f
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_10

    .line 5575
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->P:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_17

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5577
    :cond_10
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_11

    .line 5578
    const/16 v0, 0x12

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->j:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5580
    :cond_11
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_12

    .line 5581
    const/16 v0, 0x13

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->k:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5583
    :cond_12
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_13

    .line 5584
    const/16 v0, 0x14

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->D:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_18

    move v0, v3

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_13
    move v1, v2

    .line 5586
    :goto_6
    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 5587
    const/16 v4, 0x15

    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5586
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_14
    move v0, v2

    .line 5548
    goto/16 :goto_1

    :cond_15
    move v0, v2

    .line 5551
    goto/16 :goto_2

    :cond_16
    move v0, v2

    .line 5554
    goto/16 :goto_3

    :cond_17
    move v0, v2

    .line 5575
    goto/16 :goto_4

    :cond_18
    move v0, v2

    .line 5584
    goto :goto_5

    .line 5589
    :cond_19
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_1a

    .line 5590
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->J:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1f

    move v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5592
    :cond_1a
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_1b

    .line 5593
    const/16 v0, 0x17

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->K:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_20

    move v0, v3

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5595
    :cond_1b
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1c

    .line 5596
    const/16 v0, 0x18

    iget v1, p0, Lcom/google/maps/e/a/h;->Q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_21

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 5598
    :cond_1c
    :goto_9
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1d

    .line 5599
    const/16 v0, 0x19

    iget v1, p0, Lcom/google/maps/e/a/h;->R:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 5601
    :cond_1d
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_1e

    .line 5602
    const/16 v0, 0x1a

    iget v1, p0, Lcom/google/maps/e/a/h;->S:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_22

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_1e
    :goto_a
    move v1, v2

    .line 5604
    :goto_b
    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_23

    .line 5605
    const/16 v4, 0x1b

    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5604
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_1f
    move v0, v2

    .line 5590
    goto/16 :goto_7

    :cond_20
    move v0, v2

    .line 5593
    goto :goto_8

    .line 5596
    :cond_21
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    .line 5602
    :cond_22
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a

    .line 5607
    :cond_23
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_24

    .line 5608
    const/16 v0, 0x1c

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->f:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5610
    :cond_24
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_25

    .line 5611
    const/16 v0, 0x1d

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->i:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    :cond_25
    move v1, v2

    .line 5613
    :goto_c
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_27

    .line 5614
    const/16 v4, 0x1e

    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_26

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 5613
    :goto_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 5614
    :cond_26
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_d

    .line 5616
    :cond_27
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_28

    .line 5617
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5619
    :cond_28
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_29

    .line 5620
    const/16 v0, 0x20

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->B:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_2c

    move v0, v3

    :goto_e
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5622
    :cond_29
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2a

    .line 5623
    const/16 v0, 0x21

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->L:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_2d

    move v0, v3

    :goto_f
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5625
    :cond_2a
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_2b

    .line 5626
    const/16 v0, 0x22

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->s:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    :cond_2b
    move v1, v2

    .line 5628
    :goto_10
    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2e

    .line 5629
    const/16 v4, 0x23

    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5628
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    :cond_2c
    move v0, v2

    .line 5620
    goto :goto_e

    :cond_2d
    move v0, v2

    .line 5623
    goto :goto_f

    .line 5631
    :cond_2e
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_2f

    .line 5632
    const/16 v0, 0x24

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->C:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_36

    move v0, v3

    :goto_11
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5634
    :cond_2f
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_30

    .line 5635
    const/16 v0, 0x25

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->u:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5637
    :cond_30
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_31

    .line 5638
    const/16 v1, 0x26

    iget-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_37

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    :goto_12
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5640
    :cond_31
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_32

    .line 5641
    const/16 v0, 0x27

    iget v1, p0, Lcom/google/maps/e/a/h;->G:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 5643
    :cond_32
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_33

    .line 5644
    const/16 v0, 0x28

    iget v1, p0, Lcom/google/maps/e/a/h;->U:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_38

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 5646
    :cond_33
    :goto_13
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_34

    .line 5647
    const/16 v0, 0x29

    iget v1, p0, Lcom/google/maps/e/a/h;->M:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_39

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 5649
    :cond_34
    :goto_14
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_35

    .line 5650
    const/16 v0, 0x2a

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->H:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_3a

    move v0, v3

    :goto_15
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_35
    move v1, v2

    .line 5652
    :goto_16
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3c

    .line 5653
    const/16 v4, 0x2d

    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 5652
    :goto_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    :cond_36
    move v0, v2

    .line 5632
    goto/16 :goto_11

    .line 5638
    :cond_37
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_12

    .line 5644
    :cond_38
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_13

    .line 5647
    :cond_39
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_14

    :cond_3a
    move v0, v2

    .line 5650
    goto :goto_15

    .line 5653
    :cond_3b
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_17

    .line 5655
    :cond_3c
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_3d

    .line 5656
    const/16 v0, 0x2e

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->X:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 5658
    :cond_3d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_3f

    .line 5659
    const/16 v0, 0x2f

    iget-boolean v1, p0, Lcom/google/maps/e/a/h;->I:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_3e

    move v2, v3

    :cond_3e
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 5661
    :cond_3f
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_40

    .line 5662
    const/16 v0, 0x30

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->N:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5664
    :cond_40
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_41

    .line 5665
    const/16 v0, 0x31

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->h:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5667
    :cond_41
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_42

    .line 5668
    const/16 v0, 0x32

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->O:D

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 5670
    :cond_42
    iget-object v0, p0, Lcom/google/maps/e/a/h;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5671
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5509
    iget-byte v0, p0, Lcom/google/maps/e/a/h;->Z:B

    .line 5510
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 5520
    :cond_0
    :goto_0
    return v2

    .line 5511
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 5513
    :goto_1
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 5514
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/e/a/b;->d()Lcom/google/maps/e/a/b;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/b;

    invoke-virtual {v0}, Lcom/google/maps/e/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5515
    iput-byte v2, p0, Lcom/google/maps/e/a/h;->Z:B

    goto :goto_0

    .line 5513
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 5519
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/e/a/h;->Z:B

    move v2, v3

    .line 5520
    goto :goto_0
.end method

.method public final c()I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 5675
    iget v0, p0, Lcom/google/maps/e/a/h;->aa:I

    .line 5676
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5883
    :goto_0
    return v0

    .line 5679
    :cond_0
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_37

    .line 5680
    iget-wide v2, p0, Lcom/google/maps/e/a/h;->m:D

    .line 5681
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 5683
    :goto_1
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1

    .line 5684
    iget-wide v2, p0, Lcom/google/maps/e/a/h;->n:D

    .line 5685
    invoke-static {v9, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 5687
    :cond_1
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_2

    .line 5688
    const/4 v2, 0x3

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->o:D

    .line 5689
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 5691
    :cond_2
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_3

    .line 5692
    iget-wide v2, p0, Lcom/google/maps/e/a/h;->v:D

    .line 5693
    invoke-static {v10, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 5695
    :cond_3
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_4

    .line 5696
    const/4 v2, 0x5

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->w:D

    .line 5697
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    :cond_4
    move v2, v1

    move v3, v0

    .line 5699
    :goto_2
    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 5700
    const/4 v5, 0x6

    iget-object v0, p0, Lcom/google/maps/e/a/h;->x:Ljava/util/List;

    .line 5701
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5699
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 5703
    :cond_5
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x40000

    and-int/2addr v0, v2

    const/high16 v2, 0x40000

    if-ne v0, v2, :cond_6

    .line 5704
    const/4 v0, 0x7

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->y:D

    .line 5705
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5707
    :cond_6
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x80000

    and-int/2addr v0, v2

    const/high16 v2, 0x80000

    if-ne v0, v2, :cond_7

    .line 5708
    const/16 v0, 0x8

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->z:Z

    .line 5709
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5711
    :cond_7
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x100000

    and-int/2addr v0, v2

    const/high16 v2, 0x100000

    if-ne v0, v2, :cond_8

    .line 5712
    const/16 v0, 0x9

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->A:Z

    .line 5713
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5715
    :cond_8
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x1000000

    and-int/2addr v0, v2

    const/high16 v2, 0x1000000

    if-ne v0, v2, :cond_9

    .line 5716
    iget-boolean v0, p0, Lcom/google/maps/e/a/h;->E:Z

    .line 5717
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5719
    :cond_9
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x2000000

    and-int/2addr v0, v2

    const/high16 v2, 0x2000000

    if-ne v0, v2, :cond_a

    .line 5720
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/maps/e/a/h;->F:F

    .line 5721
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 5723
    :cond_a
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v8, :cond_b

    .line 5724
    const/16 v0, 0xc

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->c:D

    .line 5725
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5727
    :cond_b
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v9, :cond_c

    .line 5728
    const/16 v0, 0xd

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->d:D

    .line 5729
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5731
    :cond_c
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v10, :cond_d

    .line 5732
    const/16 v0, 0xe

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->e:D

    .line 5733
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5735
    :cond_d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_e

    .line 5736
    const/16 v0, 0xf

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->g:D

    .line 5737
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5739
    :cond_e
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_f

    .line 5740
    const/16 v0, 0x10

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->l:D

    .line 5741
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5743
    :cond_f
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_10

    .line 5744
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->P:Z

    .line 5745
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5747
    :cond_10
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_11

    .line 5748
    const/16 v0, 0x12

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->j:D

    .line 5749
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5751
    :cond_11
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_12

    .line 5752
    const/16 v0, 0x13

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->k:D

    .line 5753
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5755
    :cond_12
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x800000

    and-int/2addr v0, v2

    const/high16 v2, 0x800000

    if-ne v0, v2, :cond_13

    .line 5756
    const/16 v0, 0x14

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->D:Z

    .line 5757
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_13
    move v2, v1

    .line 5759
    :goto_3
    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_14

    .line 5760
    const/16 v5, 0x15

    iget-object v0, p0, Lcom/google/maps/e/a/h;->p:Ljava/util/List;

    .line 5761
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5759
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 5763
    :cond_14
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x20000000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000000

    if-ne v0, v2, :cond_15

    .line 5764
    const/16 v0, 0x16

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->J:Z

    .line 5765
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5767
    :cond_15
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x40000000    # 2.0f

    and-int/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_16

    .line 5768
    const/16 v0, 0x17

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->K:Z

    .line 5769
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5771
    :cond_16
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_17

    .line 5772
    const/16 v0, 0x18

    iget v2, p0, Lcom/google/maps/e/a/h;->Q:I

    .line 5773
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_1a

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5775
    :cond_17
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_18

    .line 5776
    const/16 v0, 0x19

    iget v2, p0, Lcom/google/maps/e/a/h;->R:F

    .line 5777
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 5779
    :cond_18
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_19

    .line 5780
    const/16 v0, 0x1a

    iget v2, p0, Lcom/google/maps/e/a/h;->S:I

    .line 5781
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_1b

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    :cond_19
    move v2, v1

    .line 5783
    :goto_6
    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1c

    .line 5784
    const/16 v5, 0x1b

    iget-object v0, p0, Lcom/google/maps/e/a/h;->q:Ljava/util/List;

    .line 5785
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5783
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_1a
    move v0, v4

    .line 5773
    goto :goto_4

    :cond_1b
    move v0, v4

    .line 5781
    goto :goto_5

    .line 5787
    :cond_1c
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1d

    .line 5788
    const/16 v0, 0x1c

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->f:D

    .line 5789
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5791
    :cond_1d
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_1e

    .line 5792
    const/16 v0, 0x1d

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->i:D

    .line 5793
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    :cond_1e
    move v2, v1

    move v5, v1

    .line 5797
    :goto_7
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_20

    .line 5798
    iget-object v0, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    .line 5799
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1f

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v5, v0

    .line 5797
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_1f
    move v0, v4

    .line 5799
    goto :goto_8

    .line 5801
    :cond_20
    add-int v0, v3, v5

    .line 5802
    iget-object v2, p0, Lcom/google/maps/e/a/h;->W:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 5804
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_21

    .line 5805
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/maps/e/a/h;->r:Lcom/google/n/ao;

    .line 5806
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 5808
    :cond_21
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_22

    .line 5809
    const/16 v2, 0x20

    iget-boolean v3, p0, Lcom/google/maps/e/a/h;->B:Z

    .line 5810
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5812
    :cond_22
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_23

    .line 5813
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/maps/e/a/h;->L:Z

    .line 5814
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5816
    :cond_23
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_24

    .line 5817
    const/16 v2, 0x22

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->s:D

    .line 5818
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    :cond_24
    move v2, v1

    move v3, v0

    .line 5820
    :goto_9
    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_25

    .line 5821
    const/16 v5, 0x23

    iget-object v0, p0, Lcom/google/maps/e/a/h;->t:Ljava/util/List;

    .line 5822
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5820
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 5824
    :cond_25
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x400000

    and-int/2addr v0, v2

    const/high16 v2, 0x400000

    if-ne v0, v2, :cond_26

    .line 5825
    const/16 v0, 0x24

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->C:Z

    .line 5826
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 5828
    :cond_26
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_27

    .line 5829
    const/16 v0, 0x25

    iget-wide v6, p0, Lcom/google/maps/e/a/h;->u:D

    .line 5830
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v3, v0

    .line 5832
    :cond_27
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_28

    .line 5833
    const/16 v2, 0x26

    .line 5834
    iget-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_2d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/e/a/h;->V:Ljava/lang/Object;

    :goto_a
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 5836
    :cond_28
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x4000000

    and-int/2addr v0, v2

    const/high16 v2, 0x4000000

    if-ne v0, v2, :cond_29

    .line 5837
    const/16 v0, 0x27

    iget v2, p0, Lcom/google/maps/e/a/h;->G:F

    .line 5838
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 5840
    :cond_29
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_2a

    .line 5841
    const/16 v0, 0x28

    iget v2, p0, Lcom/google/maps/e/a/h;->U:I

    .line 5842
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_2e

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_b
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5844
    :cond_2a
    iget v0, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v8, :cond_2b

    .line 5845
    const/16 v0, 0x29

    iget v2, p0, Lcom/google/maps/e/a/h;->M:I

    .line 5846
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_2f

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 5848
    :cond_2b
    iget v0, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v2, 0x8000000

    and-int/2addr v0, v2

    const/high16 v2, 0x8000000

    if-ne v0, v2, :cond_2c

    .line 5849
    const/16 v0, 0x2a

    iget-boolean v2, p0, Lcom/google/maps/e/a/h;->H:Z

    .line 5850
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_2c
    move v2, v1

    move v5, v1

    .line 5854
    :goto_d
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_31

    .line 5855
    iget-object v0, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    .line 5856
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_30

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_e
    add-int/2addr v5, v0

    .line 5854
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 5834
    :cond_2d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    :cond_2e
    move v0, v4

    .line 5842
    goto :goto_b

    :cond_2f
    move v0, v4

    .line 5846
    goto :goto_c

    :cond_30
    move v0, v4

    .line 5856
    goto :goto_e

    .line 5858
    :cond_31
    add-int v0, v3, v5

    .line 5859
    iget-object v2, p0, Lcom/google/maps/e/a/h;->T:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 5861
    iget v2, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_32

    .line 5862
    const/16 v2, 0x2e

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->X:J

    .line 5863
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 5865
    :cond_32
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_33

    .line 5866
    const/16 v2, 0x2f

    iget-boolean v3, p0, Lcom/google/maps/e/a/h;->I:Z

    .line 5867
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5869
    :cond_33
    iget v2, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v9, :cond_34

    .line 5870
    const/16 v2, 0x30

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->N:D

    .line 5871
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 5873
    :cond_34
    iget v2, p0, Lcom/google/maps/e/a/h;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_35

    .line 5874
    const/16 v2, 0x31

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->h:D

    .line 5875
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 5877
    :cond_35
    iget v2, p0, Lcom/google/maps/e/a/h;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v10, :cond_36

    .line 5878
    const/16 v2, 0x32

    iget-wide v4, p0, Lcom/google/maps/e/a/h;->O:D

    .line 5879
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 5881
    :cond_36
    iget-object v1, p0, Lcom/google/maps/e/a/h;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5882
    iput v0, p0, Lcom/google/maps/e/a/h;->aa:I

    goto/16 :goto_0

    :cond_37
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4128
    invoke-static {}, Lcom/google/maps/e/a/h;->newBuilder()Lcom/google/maps/e/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/e/a/j;->a(Lcom/google/maps/e/a/h;)Lcom/google/maps/e/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4128
    invoke-static {}, Lcom/google/maps/e/a/h;->newBuilder()Lcom/google/maps/e/a/j;

    move-result-object v0

    return-object v0
.end method
