.class public final Lcom/google/maps/f/b/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/b/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/f/b/e;",
        "Lcom/google/maps/f/b/g;",
        ">;",
        "Lcom/google/maps/f/b/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:Z

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/google/n/ao;

.field private m:Ljava/lang/Object;

.field private n:Ljava/lang/Object;

.field private o:Lcom/google/n/ao;

.field private p:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 800
    sget-object v0, Lcom/google/maps/f/b/e;->q:Lcom/google/maps/f/b/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1014
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/g;->b:Ljava/lang/Object;

    .line 1090
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/g;->c:Ljava/lang/Object;

    .line 1166
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/g;->d:Ljava/lang/Object;

    .line 1306
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->g:Lcom/google/n/ao;

    .line 1365
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->h:Lcom/google/n/ao;

    .line 1424
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->i:Lcom/google/n/ao;

    .line 1483
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->j:Lcom/google/n/ao;

    .line 1543
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    .line 1679
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->l:Lcom/google/n/ao;

    .line 1738
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/g;->m:Ljava/lang/Object;

    .line 1814
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/g;->n:Ljava/lang/Object;

    .line 1890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/g;->o:Lcom/google/n/ao;

    .line 1949
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/f/b/g;->p:I

    .line 801
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/f/b/e;)Lcom/google/maps/f/b/g;
    .locals 5

    .prologue
    const/16 v4, 0x200

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 923
    invoke-static {}, Lcom/google/maps/f/b/e;->d()Lcom/google/maps/f/b/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 993
    :goto_0
    return-object p0

    .line 924
    :cond_0
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_10

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 925
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 926
    iget-object v2, p1, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->b:Ljava/lang/Object;

    .line 929
    :cond_1
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 930
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 931
    iget-object v2, p1, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->c:Ljava/lang/Object;

    .line 934
    :cond_2
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 935
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 936
    iget-object v2, p1, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->d:Ljava/lang/Object;

    .line 939
    :cond_3
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 940
    iget-boolean v2, p1, Lcom/google/maps/f/b/e;->e:Z

    iget v3, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/f/b/g;->a:I

    iput-boolean v2, p0, Lcom/google/maps/f/b/g;->e:Z

    .line 942
    :cond_4
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 943
    iget-boolean v2, p1, Lcom/google/maps/f/b/e;->f:Z

    iget v3, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/f/b/g;->a:I

    iput-boolean v2, p0, Lcom/google/maps/f/b/g;->f:Z

    .line 945
    :cond_5
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 946
    iget-object v2, p0, Lcom/google/maps/f/b/g;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 947
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 949
    :cond_6
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 950
    iget-object v2, p0, Lcom/google/maps/f/b/g;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 951
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 953
    :cond_7
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 954
    iget-object v2, p0, Lcom/google/maps/f/b/g;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 955
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 957
    :cond_8
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 958
    iget-object v2, p0, Lcom/google/maps/f/b/g;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 959
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 961
    :cond_9
    iget-object v2, p1, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 962
    iget-object v2, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 963
    iget-object v2, p1, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    .line 964
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 971
    :cond_a
    :goto_a
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x200

    if-ne v2, v4, :cond_1b

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 972
    iget-object v2, p0, Lcom/google/maps/f/b/g;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 973
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 975
    :cond_b
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 976
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 977
    iget-object v2, p1, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->m:Ljava/lang/Object;

    .line 980
    :cond_c
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 981
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 982
    iget-object v2, p1, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/g;->n:Ljava/lang/Object;

    .line 985
    :cond_d
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 986
    iget-object v2, p0, Lcom/google/maps/f/b/g;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 987
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 989
    :cond_e
    iget v2, p1, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_1f

    :goto_f
    if-eqz v0, :cond_21

    .line 990
    iget v0, p1, Lcom/google/maps/f/b/e;->p:I

    invoke-static {v0}, Lcom/google/maps/f/b/i;->a(I)Lcom/google/maps/f/b/i;

    move-result-object v0

    if-nez v0, :cond_f

    sget-object v0, Lcom/google/maps/f/b/i;->a:Lcom/google/maps/f/b/i;

    :cond_f
    if-nez v0, :cond_20

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    move v2, v1

    .line 924
    goto/16 :goto_1

    :cond_11
    move v2, v1

    .line 929
    goto/16 :goto_2

    :cond_12
    move v2, v1

    .line 934
    goto/16 :goto_3

    :cond_13
    move v2, v1

    .line 939
    goto/16 :goto_4

    :cond_14
    move v2, v1

    .line 942
    goto/16 :goto_5

    :cond_15
    move v2, v1

    .line 945
    goto/16 :goto_6

    :cond_16
    move v2, v1

    .line 949
    goto/16 :goto_7

    :cond_17
    move v2, v1

    .line 953
    goto/16 :goto_8

    :cond_18
    move v2, v1

    .line 957
    goto/16 :goto_9

    .line 966
    :cond_19
    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit16 v2, v2, 0x200

    if-eq v2, v4, :cond_1a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/f/b/g;->a:I

    .line 967
    :cond_1a
    iget-object v2, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_1b
    move v2, v1

    .line 971
    goto/16 :goto_b

    :cond_1c
    move v2, v1

    .line 975
    goto/16 :goto_c

    :cond_1d
    move v2, v1

    .line 980
    goto :goto_d

    :cond_1e
    move v2, v1

    .line 985
    goto :goto_e

    :cond_1f
    move v0, v1

    .line 989
    goto :goto_f

    .line 990
    :cond_20
    iget v1, p0, Lcom/google/maps/f/b/g;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/maps/f/b/g;->a:I

    iget v0, v0, Lcom/google/maps/f/b/i;->c:I

    iput v0, p0, Lcom/google/maps/f/b/g;->p:I

    .line 992
    :cond_21
    iget-object v0, p1, Lcom/google/maps/f/b/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 792
    new-instance v2, Lcom/google/maps/f/b/e;

    invoke-direct {v2, p0}, Lcom/google/maps/f/b/e;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_e

    :goto_0
    iget-object v4, p0, Lcom/google/maps/f/b/g;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/f/b/g;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/f/b/g;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/f/b/g;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/f/b/e;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v4, p0, Lcom/google/maps/f/b/g;->f:Z

    iput-boolean v4, v2, Lcom/google/maps/f/b/e;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/maps/f/b/g;->a:I

    :cond_8
    iget-object v4, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v4, v2, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v4, p0, Lcom/google/maps/f/b/g;->m:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget-object v4, p0, Lcom/google/maps/f/b/g;->n:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x1000

    :cond_c
    iget-object v4, v2, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/g;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/g;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_d

    or-int/lit16 v0, v0, 0x2000

    :cond_d
    iget v1, p0, Lcom/google/maps/f/b/g;->p:I

    iput v1, v2, Lcom/google/maps/f/b/e;->p:I

    iput v0, v2, Lcom/google/maps/f/b/e;->a:I

    return-object v2

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 792
    check-cast p1, Lcom/google/maps/f/b/e;

    invoke-virtual {p0, p1}, Lcom/google/maps/f/b/g;->a(Lcom/google/maps/f/b/e;)Lcom/google/maps/f/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 997
    iget v0, p0, Lcom/google/maps/f/b/g;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 998
    iget-object v0, p0, Lcom/google/maps/f/b/g;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1009
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 997
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1003
    :goto_2
    iget-object v0, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1004
    iget-object v0, p0, Lcom/google/maps/f/b/g;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/b/a;->d()Lcom/google/maps/f/b/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/b/a;

    invoke-virtual {v0}, Lcom/google/maps/f/b/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1003
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1009
    goto :goto_1
.end method
