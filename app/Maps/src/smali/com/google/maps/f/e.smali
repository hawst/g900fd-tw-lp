.class public final Lcom/google/maps/f/e;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/e;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Lcom/google/maps/f/e;

.field private static final serialVersionUID:J

.field private static volatile u:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Z

.field d:Ljava/lang/Object;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:I

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field l:Lcom/google/n/ao;

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field n:Ljava/lang/Object;

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field p:Lcom/google/n/ao;

.field q:Lcom/google/n/ao;

.field private s:B

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lcom/google/maps/f/f;

    invoke-direct {v0}, Lcom/google/maps/f/f;-><init>()V

    sput-object v0, Lcom/google/maps/f/e;->PARSER:Lcom/google/n/ax;

    .line 840
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/f/e;->u:Lcom/google/n/aw;

    .line 2269
    new-instance v0, Lcom/google/maps/f/e;

    invoke-direct {v0}, Lcom/google/maps/f/e;-><init>()V

    sput-object v0, Lcom/google/maps/f/e;->r:Lcom/google/maps/f/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 304
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    .line 377
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    .line 409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    .line 425
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    .line 441
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    .line 639
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    .line 670
    iput-byte v3, p0, Lcom/google/maps/f/e;->s:B

    .line 758
    iput v3, p0, Lcom/google/maps/f/e;->t:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/f/e;->c:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iput v2, p0, Lcom/google/maps/f/e;->j:I

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 28
    iget-object v0, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    .line 32
    iget-object v0, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 33
    iget-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v12, 0x2000

    const/16 v11, 0x800

    const/4 v2, 0x1

    const/16 v10, 0x200

    const/4 v3, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/maps/f/e;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 46
    :cond_0
    :goto_0
    if-nez v4, :cond_d

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 53
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 55
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 51
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 61
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/f/e;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x800

    if-ne v2, v11, :cond_1

    .line 182
    iget-object v2, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    .line 184
    :cond_1
    and-int/lit16 v2, v1, 0x2000

    if-ne v2, v12, :cond_2

    .line 185
    iget-object v2, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    .line 187
    :cond_2
    and-int/lit16 v1, v1, 0x200

    if-ne v1, v10, :cond_3

    .line 188
    iget-object v1, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 190
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/f/e;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/f/e;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 177
    :catch_1
    move-exception v0

    .line 178
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 179
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 71
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 75
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 76
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 80
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 81
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 85
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v6, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/f/e;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 91
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 92
    invoke-static {v0}, Lcom/google/maps/f/h;->a(I)Lcom/google/maps/f/h;

    move-result-object v6

    .line 93
    if-nez v6, :cond_4

    .line 94
    const/16 v6, 0xb

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 96
    :cond_4
    iget v6, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/maps/f/e;->a:I

    .line 97
    iput v0, p0, Lcom/google/maps/f/e;->j:I

    goto/16 :goto_0

    .line 102
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 103
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 107
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 108
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 112
    :sswitch_a
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/f/e;->c:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 117
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 118
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/f/e;->a:I

    goto/16 :goto_0

    .line 122
    :sswitch_c
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v11, :cond_6

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    .line 125
    or-int/lit16 v1, v1, 0x800

    .line 127
    :cond_6
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 128
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 127
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 132
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 133
    iget v6, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/f/e;->a:I

    .line 134
    iput-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 138
    :sswitch_e
    and-int/lit16 v0, v1, 0x2000

    if-eq v0, v12, :cond_7

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    .line 141
    or-int/lit16 v1, v1, 0x2000

    .line 143
    :cond_7
    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 144
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 143
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 148
    :sswitch_f
    and-int/lit16 v0, v1, 0x200

    if-eq v0, v10, :cond_8

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 150
    or-int/lit16 v1, v1, 0x200

    .line 152
    :cond_8
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 156
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 157
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 158
    and-int/lit16 v0, v1, 0x200

    if-eq v0, v10, :cond_9

    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_a

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_9

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 160
    or-int/lit16 v1, v1, 0x200

    .line 162
    :cond_9
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_b

    const/4 v0, -0x1

    :goto_4
    if-lez v0, :cond_c

    .line 163
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 158
    :cond_a
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 162
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 165
    :cond_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 169
    :sswitch_11
    iget-object v0, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 170
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/f/e;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 181
    :cond_d
    and-int/lit16 v0, v1, 0x800

    if-ne v0, v11, :cond_e

    .line 182
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    .line 184
    :cond_e
    and-int/lit16 v0, v1, 0x2000

    if-ne v0, v12, :cond_f

    .line 185
    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    .line 187
    :cond_f
    and-int/lit16 v0, v1, 0x200

    if-ne v0, v10, :cond_10

    .line 188
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 190
    :cond_10
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->au:Lcom/google/n/bn;

    .line 191
    return-void

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x42 -> :sswitch_5
        0x52 -> :sswitch_6
        0x58 -> :sswitch_7
        0x62 -> :sswitch_8
        0x6a -> :sswitch_9
        0x70 -> :sswitch_a
        0x7a -> :sswitch_b
        0x82 -> :sswitch_c
        0x8a -> :sswitch_d
        0x92 -> :sswitch_e
        0x98 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 304
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    .line 377
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    .line 409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    .line 425
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    .line 441
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    .line 639
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    .line 670
    iput-byte v1, p0, Lcom/google/maps/f/e;->s:B

    .line 758
    iput v1, p0, Lcom/google/maps/f/e;->t:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/f/e;
    .locals 1

    .prologue
    .line 2272
    sget-object v0, Lcom/google/maps/f/e;->r:Lcom/google/maps/f/e;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/f/g;
    .locals 1

    .prologue
    .line 902
    new-instance v0, Lcom/google/maps/f/g;

    invoke-direct {v0}, Lcom/google/maps/f/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lcom/google/maps/f/e;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 706
    invoke-virtual {p0}, Lcom/google/maps/f/e;->c()I

    .line 707
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_0

    .line 708
    iget-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 710
    :cond_0
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 711
    iget-object v0, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 713
    :cond_1
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    .line 714
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 716
    :cond_2
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 717
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 719
    :cond_3
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 720
    iget-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 722
    :cond_4
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    .line 723
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 725
    :cond_5
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_6

    .line 726
    const/16 v0, 0xb

    iget v3, p0, Lcom/google/maps/f/e;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_c

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 728
    :cond_6
    :goto_1
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_7

    .line 729
    const/16 v0, 0xc

    iget-object v3, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 731
    :cond_7
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    .line 732
    const/16 v0, 0xd

    iget-object v3, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 734
    :cond_8
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_9

    .line 735
    const/16 v0, 0xe

    iget-boolean v3, p0, Lcom/google/maps/f/e;->c:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_d

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 737
    :cond_9
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v8, :cond_a

    .line 738
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_a
    move v1, v2

    .line 740
    :goto_3
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 741
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 740
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 723
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 726
    :cond_c
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 735
    goto :goto_2

    .line 743
    :cond_e
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 744
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_f
    move v1, v2

    .line 746
    :goto_5
    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 747
    const/16 v3, 0x12

    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 746
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 744
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_11
    move v1, v2

    .line 749
    :goto_6
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 750
    const/16 v3, 0x13

    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 749
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 752
    :cond_12
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_13

    .line 753
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 755
    :cond_13
    iget-object v0, p0, Lcom/google/maps/f/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 756
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 672
    iget-byte v0, p0, Lcom/google/maps/f/e;->s:B

    .line 673
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 701
    :cond_0
    :goto_0
    return v2

    .line 674
    :cond_1
    if-eqz v0, :cond_0

    .line 676
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 677
    iget-object v0, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/b/e;->d()Lcom/google/maps/f/b/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/b/e;

    invoke-virtual {v0}, Lcom/google/maps/f/b/e;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 678
    iput-byte v2, p0, Lcom/google/maps/f/e;->s:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 676
    goto :goto_1

    .line 682
    :cond_3
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 683
    iget-object v0, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dw;

    invoke-virtual {v0}, Lcom/google/maps/g/dw;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 684
    iput-byte v2, p0, Lcom/google/maps/f/e;->s:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 682
    goto :goto_2

    :cond_5
    move v1, v2

    .line 688
    :goto_3
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 689
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/a;->d()Lcom/google/maps/f/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/a;

    invoke-virtual {v0}, Lcom/google/maps/f/a;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 690
    iput-byte v2, p0, Lcom/google/maps/f/e;->s:B

    goto :goto_0

    .line 688
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 694
    :cond_7
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 695
    iget-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    sget-object v1, Lcom/google/maps/f/e;->r:Lcom/google/maps/f/e;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 696
    iput-byte v2, p0, Lcom/google/maps/f/e;->s:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 694
    goto :goto_4

    .line 700
    :cond_9
    iput-byte v3, p0, Lcom/google/maps/f/e;->s:B

    move v2, v3

    .line 701
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v3, 0xa

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 760
    iget v0, p0, Lcom/google/maps/f/e;->t:I

    .line 761
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 835
    :goto_0
    return v0

    .line 764
    :cond_0
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_13

    .line 765
    iget-object v0, p0, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    .line 766
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 768
    :goto_1
    iget v2, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    .line 769
    iget-object v2, p0, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    .line 770
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 772
    :cond_1
    iget v2, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_2

    .line 773
    const/4 v2, 0x5

    iget-object v4, p0, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    .line 774
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 776
    :cond_2
    iget v2, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_3

    .line 777
    const/4 v2, 0x6

    iget-object v4, p0, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    .line 778
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 780
    :cond_3
    iget v2, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_12

    .line 781
    iget-object v2, p0, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    .line 782
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 784
    :goto_2
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_4

    .line 786
    iget-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 788
    :cond_4
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_5

    .line 789
    const/16 v0, 0xb

    iget v4, p0, Lcom/google/maps/f/e;->j:I

    .line 790
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 792
    :cond_5
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_6

    .line 793
    const/16 v0, 0xc

    iget-object v3, p0, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    .line 794
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 796
    :cond_6
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_7

    .line 797
    const/16 v0, 0xd

    iget-object v3, p0, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    .line 798
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 800
    :cond_7
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 801
    const/16 v0, 0xe

    iget-boolean v3, p0, Lcom/google/maps/f/e;->c:Z

    .line 802
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 804
    :cond_8
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_9

    .line 805
    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    .line 806
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_9
    move v3, v2

    move v2, v1

    .line 808
    :goto_5
    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 809
    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/maps/f/e;->m:Ljava/util/List;

    .line 810
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 808
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 786
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_b
    move v0, v3

    .line 790
    goto/16 :goto_4

    .line 812
    :cond_c
    iget v0, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 813
    const/16 v2, 0x11

    .line 814
    iget-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_d
    move v2, v1

    .line 816
    :goto_7
    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_f

    .line 817
    const/16 v4, 0x12

    iget-object v0, p0, Lcom/google/maps/f/e;->o:Ljava/util/List;

    .line 818
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 816
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 814
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_f
    move v2, v1

    move v4, v1

    .line 822
    :goto_8
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_10

    .line 823
    iget-object v0, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    .line 824
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 822
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 826
    :cond_10
    add-int v0, v3, v4

    .line 827
    iget-object v2, p0, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 829
    iget v2, p0, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_11

    .line 830
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    .line 831
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 833
    :cond_11
    iget-object v1, p0, Lcom/google/maps/f/e;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 834
    iput v0, p0, Lcom/google/maps/f/e;->t:I

    goto/16 :goto_0

    :cond_12
    move v2, v0

    goto/16 :goto_2

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/e;->newBuilder()Lcom/google/maps/f/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/f/g;->a(Lcom/google/maps/f/e;)Lcom/google/maps/f/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/e;->newBuilder()Lcom/google/maps/f/g;

    move-result-object v0

    return-object v0
.end method
