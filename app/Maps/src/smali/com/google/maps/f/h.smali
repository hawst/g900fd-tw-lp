.class public final enum Lcom/google/maps/f/h;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/f/h;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/f/h;

.field public static final enum b:Lcom/google/maps/f/h;

.field public static final enum c:Lcom/google/maps/f/h;

.field public static final enum d:Lcom/google/maps/f/h;

.field public static final enum e:Lcom/google/maps/f/h;

.field public static final enum f:Lcom/google/maps/f/h;

.field private static final synthetic h:[Lcom/google/maps/f/h;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 216
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_MOBILE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->a:Lcom/google/maps/f/h;

    .line 220
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_DESKTOP"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->b:Lcom/google/maps/f/h;

    .line 224
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_LU_RICHLIST_MAP"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->c:Lcom/google/maps/f/h;

    .line 228
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_LU_RICHLISTDESKTOP_FULLLIST_MAP"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->d:Lcom/google/maps/f/h;

    .line 232
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_IOS_GSA_LU"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->e:Lcom/google/maps/f/h;

    .line 236
    new-instance v0, Lcom/google/maps/f/h;

    const-string v1, "CLIENT_IOS_GSA_IMMERSIVE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/f/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/h;->f:Lcom/google/maps/f/h;

    .line 211
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/maps/f/h;

    sget-object v1, Lcom/google/maps/f/h;->a:Lcom/google/maps/f/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/f/h;->b:Lcom/google/maps/f/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/f/h;->c:Lcom/google/maps/f/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/f/h;->d:Lcom/google/maps/f/h;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/f/h;->e:Lcom/google/maps/f/h;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/f/h;->f:Lcom/google/maps/f/h;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/f/h;->h:[Lcom/google/maps/f/h;

    .line 286
    new-instance v0, Lcom/google/maps/f/i;

    invoke-direct {v0}, Lcom/google/maps/f/i;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 296
    iput p3, p0, Lcom/google/maps/f/h;->g:I

    .line 297
    return-void
.end method

.method public static a(I)Lcom/google/maps/f/h;
    .locals 1

    .prologue
    .line 270
    packed-switch p0, :pswitch_data_0

    .line 277
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 271
    :pswitch_0
    sget-object v0, Lcom/google/maps/f/h;->a:Lcom/google/maps/f/h;

    goto :goto_0

    .line 272
    :pswitch_1
    sget-object v0, Lcom/google/maps/f/h;->b:Lcom/google/maps/f/h;

    goto :goto_0

    .line 273
    :pswitch_2
    sget-object v0, Lcom/google/maps/f/h;->c:Lcom/google/maps/f/h;

    goto :goto_0

    .line 274
    :pswitch_3
    sget-object v0, Lcom/google/maps/f/h;->d:Lcom/google/maps/f/h;

    goto :goto_0

    .line 275
    :pswitch_4
    sget-object v0, Lcom/google/maps/f/h;->e:Lcom/google/maps/f/h;

    goto :goto_0

    .line 276
    :pswitch_5
    sget-object v0, Lcom/google/maps/f/h;->f:Lcom/google/maps/f/h;

    goto :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/f/h;
    .locals 1

    .prologue
    .line 211
    const-class v0, Lcom/google/maps/f/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/h;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/f/h;
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/google/maps/f/h;->h:[Lcom/google/maps/f/h;

    invoke-virtual {v0}, [Lcom/google/maps/f/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/f/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/google/maps/f/h;->g:I

    return v0
.end method
