.class public final Lcom/google/maps/f/b/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/b/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/b/a;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/f/b/a;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:F

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/maps/f/b/b;

    invoke-direct {v0}, Lcom/google/maps/f/b/b;-><init>()V

    sput-object v0, Lcom/google/maps/f/b/a;->PARSER:Lcom/google/n/ax;

    .line 246
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/f/b/a;->i:Lcom/google/n/aw;

    .line 607
    new-instance v0, Lcom/google/maps/f/b/a;

    invoke-direct {v0}, Lcom/google/maps/f/b/a;-><init>()V

    sput-object v0, Lcom/google/maps/f/b/a;->f:Lcom/google/maps/f/b/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 96
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    .line 183
    iput-byte v2, p0, Lcom/google/maps/f/b/a;->g:B

    .line 217
    iput v2, p0, Lcom/google/maps/f/b/a;->h:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/f/b/a;->d:F

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/f/b/a;->e:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/maps/f/b/a;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 49
    iget v3, p0, Lcom/google/maps/f/b/a;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/f/b/a;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/f/b/a;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/f/b/a;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/f/b/a;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/maps/f/b/a;->d:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    .line 73
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 74
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 59
    iget v4, p0, Lcom/google/maps/f/b/a;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/f/b/a;->a:I

    .line 60
    iput-object v3, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    goto :goto_0

    .line 64
    :sswitch_4
    iget v3, p0, Lcom/google/maps/f/b/a;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/f/b/a;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/f/b/a;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/a;->au:Lcom/google/n/bn;

    .line 77
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 96
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    .line 183
    iput-byte v1, p0, Lcom/google/maps/f/b/a;->g:B

    .line 217
    iput v1, p0, Lcom/google/maps/f/b/a;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/f/b/a;
    .locals 1

    .prologue
    .line 610
    sget-object v0, Lcom/google/maps/f/b/a;->f:Lcom/google/maps/f/b/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/f/b/c;
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/google/maps/f/b/c;

    invoke-direct {v0}, Lcom/google/maps/f/b/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/google/maps/f/b/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 201
    invoke-virtual {p0}, Lcom/google/maps/f/b/a;->c()I

    .line 202
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 205
    :cond_0
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 206
    iget v0, p0, Lcom/google/maps/f/b/a;->d:F

    const/4 v1, 0x5

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 208
    :cond_1
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 211
    :cond_2
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 212
    iget v0, p0, Lcom/google/maps/f/b/a;->e:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 214
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/maps/f/b/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 215
    return-void

    .line 209
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 212
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 185
    iget-byte v0, p0, Lcom/google/maps/f/b/a;->g:B

    .line 186
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 196
    :goto_0
    return v0

    .line 187
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 189
    :cond_1
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 191
    iput-byte v2, p0, Lcom/google/maps/f/b/a;->g:B

    move v0, v2

    .line 192
    goto :goto_0

    :cond_2
    move v0, v2

    .line 189
    goto :goto_1

    .line 195
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/f/b/a;->g:B

    move v0, v1

    .line 196
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 219
    iget v0, p0, Lcom/google/maps/f/b/a;->h:I

    .line 220
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 241
    :goto_0
    return v0

    .line 223
    :cond_0
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 224
    iget-object v0, p0, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    .line 225
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 227
    :goto_1
    iget v2, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_5

    .line 228
    iget v2, p0, Lcom/google/maps/f/b/a;->d:F

    .line 229
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    move v2, v0

    .line 231
    :goto_2
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 232
    const/4 v3, 0x3

    .line 233
    iget-object v0, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 235
    :cond_1
    iget v0, p0, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 236
    iget v0, p0, Lcom/google/maps/f/b/a;->e:I

    .line 237
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/google/maps/f/b/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 240
    iput v0, p0, Lcom/google/maps/f/b/a;->h:I

    goto :goto_0

    .line 233
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 237
    :cond_4
    const/16 v0, 0xa

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/b/a;->newBuilder()Lcom/google/maps/f/b/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/f/b/c;->a(Lcom/google/maps/f/b/a;)Lcom/google/maps/f/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/b/a;->newBuilder()Lcom/google/maps/f/b/c;

    move-result-object v0

    return-object v0
.end method
