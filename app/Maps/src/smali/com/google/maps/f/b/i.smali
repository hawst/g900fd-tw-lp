.class public final enum Lcom/google/maps/f/b/i;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/f/b/i;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/f/b/i;

.field public static final enum b:Lcom/google/maps/f/b/i;

.field private static final synthetic d:[Lcom/google/maps/f/b/i;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 14
    new-instance v0, Lcom/google/maps/f/b/i;

    const-string v1, "SEARCH_VERTICAL_NOT_SPECIFIED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/maps/f/b/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/b/i;->a:Lcom/google/maps/f/b/i;

    .line 18
    new-instance v0, Lcom/google/maps/f/b/i;

    const-string v1, "SEARCH_VERTICAL_INDOOR_LODGING"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/f/b/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/f/b/i;->b:Lcom/google/maps/f/b/i;

    .line 9
    new-array v0, v4, [Lcom/google/maps/f/b/i;

    sget-object v1, Lcom/google/maps/f/b/i;->a:Lcom/google/maps/f/b/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/f/b/i;->b:Lcom/google/maps/f/b/i;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/maps/f/b/i;->d:[Lcom/google/maps/f/b/i;

    .line 48
    new-instance v0, Lcom/google/maps/f/b/j;

    invoke-direct {v0}, Lcom/google/maps/f/b/j;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput p3, p0, Lcom/google/maps/f/b/i;->c:I

    .line 59
    return-void
.end method

.method public static a(I)Lcom/google/maps/f/b/i;
    .locals 1

    .prologue
    .line 36
    packed-switch p0, :pswitch_data_0

    .line 39
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 37
    :pswitch_0
    sget-object v0, Lcom/google/maps/f/b/i;->a:Lcom/google/maps/f/b/i;

    goto :goto_0

    .line 38
    :pswitch_1
    sget-object v0, Lcom/google/maps/f/b/i;->b:Lcom/google/maps/f/b/i;

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/f/b/i;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/f/b/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/b/i;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/f/b/i;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/f/b/i;->d:[Lcom/google/maps/f/b/i;

    invoke-virtual {v0}, [Lcom/google/maps/f/b/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/f/b/i;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/maps/f/b/i;->c:I

    return v0
.end method
