.class public final Lcom/google/maps/f/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/f/e;",
        "Lcom/google/maps/f/g;",
        ">;",
        "Lcom/google/maps/f/j;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Z

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/google/n/ao;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/Object;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/google/n/ao;

.field private q:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 920
    sget-object v0, Lcom/google/maps/f/e;->r:Lcom/google/maps/f/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1174
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->b:Lcom/google/n/ao;

    .line 1265
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/g;->d:Ljava/lang/Object;

    .line 1341
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->e:Lcom/google/n/ao;

    .line 1400
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->f:Lcom/google/n/ao;

    .line 1459
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->g:Lcom/google/n/ao;

    .line 1518
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->h:Lcom/google/n/ao;

    .line 1577
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->i:Lcom/google/n/ao;

    .line 1636
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/f/g;->j:I

    .line 1672
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    .line 1738
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->l:Lcom/google/n/ao;

    .line 1798
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    .line 1934
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/g;->n:Ljava/lang/Object;

    .line 2011
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    .line 2147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->p:Lcom/google/n/ao;

    .line 2206
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/g;->q:Lcom/google/n/ao;

    .line 921
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/f/e;)Lcom/google/maps/f/g;
    .locals 6

    .prologue
    const/16 v5, 0x800

    const/16 v4, 0x200

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1057
    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1141
    :goto_0
    return-object p0

    .line 1058
    :cond_0
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1059
    iget-object v2, p0, Lcom/google/maps/f/g;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1060
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1062
    :cond_1
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1063
    iget-boolean v2, p1, Lcom/google/maps/f/e;->c:Z

    iget v3, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/f/g;->a:I

    iput-boolean v2, p0, Lcom/google/maps/f/g;->c:Z

    .line 1065
    :cond_2
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1066
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1067
    iget-object v2, p1, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/g;->d:Ljava/lang/Object;

    .line 1070
    :cond_3
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1071
    iget-object v2, p0, Lcom/google/maps/f/g;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1072
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1074
    :cond_4
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1075
    iget-object v2, p0, Lcom/google/maps/f/g;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1076
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1078
    :cond_5
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1079
    iget-object v2, p0, Lcom/google/maps/f/g;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1080
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1082
    :cond_6
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1083
    iget-object v2, p0, Lcom/google/maps/f/g;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1084
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1086
    :cond_7
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1087
    iget-object v2, p0, Lcom/google/maps/f/g;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1088
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1090
    :cond_8
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_9
    if-eqz v2, :cond_14

    .line 1091
    iget v2, p1, Lcom/google/maps/f/e;->j:I

    invoke-static {v2}, Lcom/google/maps/f/h;->a(I)Lcom/google/maps/f/h;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/maps/f/h;->b:Lcom/google/maps/f/h;

    :cond_9
    if-nez v2, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 1058
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 1062
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 1065
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 1070
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 1074
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 1078
    goto :goto_6

    :cond_10
    move v2, v1

    .line 1082
    goto :goto_7

    :cond_11
    move v2, v1

    .line 1086
    goto :goto_8

    :cond_12
    move v2, v1

    .line 1090
    goto :goto_9

    .line 1091
    :cond_13
    iget v3, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/f/g;->a:I

    iget v2, v2, Lcom/google/maps/f/h;->g:I

    iput v2, p0, Lcom/google/maps/f/g;->j:I

    .line 1093
    :cond_14
    iget-object v2, p1, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_15

    .line 1094
    iget-object v2, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1095
    iget-object v2, p1, Lcom/google/maps/f/e;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    .line 1096
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1103
    :cond_15
    :goto_a
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x200

    if-ne v2, v4, :cond_1e

    move v2, v0

    :goto_b
    if-eqz v2, :cond_16

    .line 1104
    iget-object v2, p0, Lcom/google/maps/f/g;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1105
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1107
    :cond_16
    iget-object v2, p1, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_17

    .line 1108
    iget-object v2, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1109
    iget-object v2, p1, Lcom/google/maps/f/e;->m:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    .line 1110
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1117
    :cond_17
    :goto_c
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_d
    if-eqz v2, :cond_18

    .line 1118
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1119
    iget-object v2, p1, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/g;->n:Ljava/lang/Object;

    .line 1122
    :cond_18
    iget-object v2, p1, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_19

    .line 1123
    iget-object v2, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1124
    iget-object v2, p1, Lcom/google/maps/f/e;->o:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    .line 1125
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, -0x2001

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1132
    :cond_19
    :goto_e
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x800

    if-ne v2, v5, :cond_24

    move v2, v0

    :goto_f
    if-eqz v2, :cond_1a

    .line 1133
    iget-object v2, p0, Lcom/google/maps/f/g;->p:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1134
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1136
    :cond_1a
    iget v2, p1, Lcom/google/maps/f/e;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_25

    :goto_10
    if-eqz v0, :cond_1b

    .line 1137
    iget-object v0, p0, Lcom/google/maps/f/g;->q:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1138
    iget v0, p0, Lcom/google/maps/f/g;->a:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/f/g;->a:I

    .line 1140
    :cond_1b
    iget-object v0, p1, Lcom/google/maps/f/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1098
    :cond_1c
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, 0x200

    if-eq v2, v4, :cond_1d

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1099
    :cond_1d
    iget-object v2, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/f/e;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_1e
    move v2, v1

    .line 1103
    goto/16 :goto_b

    .line 1112
    :cond_1f
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, 0x800

    if-eq v2, v5, :cond_20

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1113
    :cond_20
    iget-object v2, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/f/e;->m:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c

    :cond_21
    move v2, v1

    .line 1117
    goto/16 :goto_d

    .line 1127
    :cond_22
    iget v2, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-eq v2, v3, :cond_23

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/f/g;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/f/g;->a:I

    .line 1128
    :cond_23
    iget-object v2, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/f/e;->o:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_e

    :cond_24
    move v2, v1

    .line 1132
    goto/16 :goto_f

    :cond_25
    move v0, v1

    .line 1136
    goto/16 :goto_10
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 8

    .prologue
    const v7, 0x8000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 912
    new-instance v2, Lcom/google/maps/f/e;

    invoke-direct {v2, p0}, Lcom/google/maps/f/e;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_f

    :goto_0
    iget-object v4, v2, Lcom/google/maps/f/e;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/maps/f/g;->c:Z

    iput-boolean v4, v2, Lcom/google/maps/f/e;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/f/g;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/e;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/f/e;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/f/e;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/f/e;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/f/e;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/f/e;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v4, p0, Lcom/google/maps/f/g;->j:I

    iput v4, v2, Lcom/google/maps/f/e;->j:I

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/maps/f/g;->a:I

    :cond_8
    iget-object v4, p0, Lcom/google/maps/f/g;->k:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/f/e;->k:Ljava/util/List;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v4, v2, Lcom/google/maps/f/e;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, -0x801

    iput v4, p0, Lcom/google/maps/f/g;->a:I

    :cond_a
    iget-object v4, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/f/e;->m:Ljava/util/List;

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-object v4, p0, Lcom/google/maps/f/g;->n:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/f/e;->n:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit16 v4, v4, -0x2001

    iput v4, p0, Lcom/google/maps/f/g;->a:I

    :cond_c
    iget-object v4, p0, Lcom/google/maps/f/g;->o:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/f/e;->o:Ljava/util/List;

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x800

    :cond_d
    iget-object v4, v2, Lcom/google/maps/f/e;->p:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/g;->p:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/g;->p:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/2addr v3, v7

    if-ne v3, v7, :cond_e

    or-int/lit16 v0, v0, 0x1000

    :cond_e
    iget-object v3, v2, Lcom/google/maps/f/e;->q:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/f/g;->q:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/f/g;->q:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/f/e;->a:I

    return-object v2

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 912
    check-cast p1, Lcom/google/maps/f/e;

    invoke-virtual {p0, p1}, Lcom/google/maps/f/g;->a(Lcom/google/maps/f/e;)Lcom/google/maps/f/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const v5, 0x8000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1145
    iget v0, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1146
    iget-object v0, p0, Lcom/google/maps/f/g;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/b/e;->d()Lcom/google/maps/f/b/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/b/e;

    invoke-virtual {v0}, Lcom/google/maps/f/b/e;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1169
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1145
    goto :goto_0

    .line 1151
    :cond_2
    iget v0, p0, Lcom/google/maps/f/g;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1152
    iget-object v0, p0, Lcom/google/maps/f/g;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dw;

    invoke-virtual {v0}, Lcom/google/maps/g/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 1157
    :goto_3
    iget-object v0, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1158
    iget-object v0, p0, Lcom/google/maps/f/g;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/a;->d()Lcom/google/maps/f/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/a;

    invoke-virtual {v0}, Lcom/google/maps/f/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1151
    goto :goto_2

    .line 1163
    :cond_5
    iget v0, p0, Lcom/google/maps/f/g;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_7

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 1164
    iget-object v0, p0, Lcom/google/maps/f/g;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_6
    move v2, v3

    .line 1169
    goto :goto_1

    :cond_7
    move v0, v2

    .line 1163
    goto :goto_4
.end method
