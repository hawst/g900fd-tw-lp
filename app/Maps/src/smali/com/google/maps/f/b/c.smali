.class public final Lcom/google/maps/f/b/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/b/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/f/b/a;",
        "Lcom/google/maps/f/b/c;",
        ">;",
        "Lcom/google/maps/f/b/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:F

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/google/maps/f/b/a;->f:Lcom/google/maps/f/b/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 404
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/c;->b:Lcom/google/n/ao;

    .line 463
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/c;->c:Ljava/lang/Object;

    .line 327
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/f/b/a;)Lcom/google/maps/f/b/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 372
    invoke-static {}, Lcom/google/maps/f/b/a;->d()Lcom/google/maps/f/b/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 389
    :goto_0
    return-object p0

    .line 373
    :cond_0
    iget v2, p1, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 374
    iget-object v2, p0, Lcom/google/maps/f/b/c;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 375
    iget v2, p0, Lcom/google/maps/f/b/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/f/b/c;->a:I

    .line 377
    :cond_1
    iget v2, p1, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 378
    iget v2, p0, Lcom/google/maps/f/b/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/f/b/c;->a:I

    .line 379
    iget-object v2, p1, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/f/b/c;->c:Ljava/lang/Object;

    .line 382
    :cond_2
    iget v2, p1, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 383
    iget v2, p1, Lcom/google/maps/f/b/a;->d:F

    iget v3, p0, Lcom/google/maps/f/b/c;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/f/b/c;->a:I

    iput v2, p0, Lcom/google/maps/f/b/c;->d:F

    .line 385
    :cond_3
    iget v2, p1, Lcom/google/maps/f/b/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 386
    iget v0, p1, Lcom/google/maps/f/b/a;->e:I

    iget v1, p0, Lcom/google/maps/f/b/c;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/f/b/c;->a:I

    iput v0, p0, Lcom/google/maps/f/b/c;->e:I

    .line 388
    :cond_4
    iget-object v0, p1, Lcom/google/maps/f/b/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 373
    goto :goto_1

    :cond_6
    move v2, v1

    .line 377
    goto :goto_2

    :cond_7
    move v2, v1

    .line 382
    goto :goto_3

    :cond_8
    move v0, v1

    .line 385
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318
    new-instance v2, Lcom/google/maps/f/b/a;

    invoke-direct {v2, p0}, Lcom/google/maps/f/b/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/f/b/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/maps/f/b/a;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/f/b/c;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/f/b/c;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/f/b/c;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/f/b/a;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/f/b/c;->d:F

    iput v1, v2, Lcom/google/maps/f/b/a;->d:F

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/f/b/c;->e:I

    iput v1, v2, Lcom/google/maps/f/b/a;->e:I

    iput v0, v2, Lcom/google/maps/f/b/a;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 318
    check-cast p1, Lcom/google/maps/f/b/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/f/b/c;->a(Lcom/google/maps/f/b/a;)Lcom/google/maps/f/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 393
    iget v0, p0, Lcom/google/maps/f/b/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 394
    iget-object v0, p0, Lcom/google/maps/f/b/c;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 399
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 393
    goto :goto_0

    :cond_1
    move v0, v2

    .line 399
    goto :goto_1
.end method
