.class public final Lcom/google/maps/f/b/e;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/f/b/h;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/b/e;",
            ">;"
        }
    .end annotation
.end field

.field static final q:Lcom/google/maps/f/b/e;

.field private static final serialVersionUID:J

.field private static volatile t:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Z

.field f:Z

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:Lcom/google/n/ao;

.field m:Ljava/lang/Object;

.field n:Ljava/lang/Object;

.field o:Lcom/google/n/ao;

.field p:I

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    new-instance v0, Lcom/google/maps/f/b/f;

    invoke-direct {v0}, Lcom/google/maps/f/b/f;-><init>()V

    sput-object v0, Lcom/google/maps/f/b/e;->PARSER:Lcom/google/n/ax;

    .line 720
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/f/b/e;->t:Lcom/google/n/aw;

    .line 1989
    new-instance v0, Lcom/google/maps/f/b/e;

    invoke-direct {v0}, Lcom/google/maps/f/b/e;-><init>()V

    sput-object v0, Lcom/google/maps/f/b/e;->q:Lcom/google/maps/f/b/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 336
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    .line 352
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    .line 368
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    .line 384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    .line 443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    .line 543
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    .line 574
    iput-byte v3, p0, Lcom/google/maps/f/b/e;->r:B

    .line 647
    iput v3, p0, Lcom/google/maps/f/b/e;->s:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    .line 21
    iput-boolean v4, p0, Lcom/google/maps/f/b/e;->e:Z

    .line 22
    iput-boolean v4, p0, Lcom/google/maps/f/b/e;->f:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    .line 28
    iget-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 32
    iput v2, p0, Lcom/google/maps/f/b/e;->p:I

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x200

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    invoke-direct {p0}, Lcom/google/maps/f/b/e;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 45
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 46
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 47
    sparse-switch v0, :sswitch_data_0

    .line 52
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 54
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 50
    goto :goto_0

    .line 59
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 60
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 61
    iput-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x200

    if-ne v1, v8, :cond_1

    .line 158
    iget-object v1, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    .line 160
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/f/b/e;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 153
    :catch_1
    move-exception v0

    .line 154
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 155
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    and-int/lit16 v0, v1, 0x200

    if-eq v0, v8, :cond_2

    .line 71
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    .line 73
    or-int/lit16 v1, v1, 0x200

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 75
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 81
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    goto/16 :goto_0

    .line 85
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 91
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 92
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    goto/16 :goto_0

    .line 96
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 97
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    goto/16 :goto_0

    .line 101
    :sswitch_8
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/f/b/e;->e:Z

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 106
    :sswitch_9
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/f/b/e;->f:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_2

    .line 111
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 113
    iput-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 117
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 118
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    goto/16 :goto_0

    .line 122
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 123
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 124
    iput-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 128
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 129
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 130
    iput-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 134
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 135
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/f/b/e;->a:I

    goto/16 :goto_0

    .line 139
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 140
    invoke-static {v0}, Lcom/google/maps/f/b/i;->a(I)Lcom/google/maps/f/b/i;

    move-result-object v6

    .line 141
    if-nez v6, :cond_5

    .line 142
    const/16 v6, 0x14

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 144
    :cond_5
    iget v6, p0, Lcom/google/maps/f/b/e;->a:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/maps/f/b/e;->a:I

    .line 145
    iput v0, p0, Lcom/google/maps/f/b/e;->p:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 157
    :cond_6
    and-int/lit16 v0, v1, 0x200

    if-ne v0, v8, :cond_7

    .line 158
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    .line 160
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->au:Lcom/google/n/bn;

    .line 161
    return-void

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x8a -> :sswitch_c
        0x92 -> :sswitch_d
        0x9a -> :sswitch_e
        0xa0 -> :sswitch_f
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 336
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    .line 352
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    .line 368
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    .line 384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    .line 443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    .line 543
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    .line 574
    iput-byte v1, p0, Lcom/google/maps/f/b/e;->r:B

    .line 647
    iput v1, p0, Lcom/google/maps/f/b/e;->s:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/f/b/e;
    .locals 1

    .prologue
    .line 1992
    sget-object v0, Lcom/google/maps/f/b/e;->q:Lcom/google/maps/f/b/e;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/f/b/g;
    .locals 1

    .prologue
    .line 782
    new-instance v0, Lcom/google/maps/f/b/g;

    invoke-direct {v0}, Lcom/google/maps/f/b/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/f/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    sget-object v0, Lcom/google/maps/f/b/e;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 598
    invoke-virtual {p0}, Lcom/google/maps/f/b/e;->c()I

    .line 599
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 600
    iget-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 602
    :cond_0
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1

    .line 603
    iget-object v0, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 605
    :goto_1
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 606
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 605
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 600
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 608
    :cond_3
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_4

    .line 609
    iget-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 611
    :cond_4
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    .line 612
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 614
    :cond_5
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 615
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 617
    :cond_6
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 618
    iget-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 620
    :cond_7
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_8

    .line 621
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/maps/f/b/e;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_11

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 623
    :cond_8
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 624
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/maps/f/b/e;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_12

    :goto_4
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 626
    :cond_9
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_a

    .line 627
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 629
    :cond_a
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 630
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 632
    :cond_b
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 633
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 635
    :cond_c
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 636
    const/16 v1, 0x12

    iget-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 638
    :cond_d
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_e

    .line 639
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 641
    :cond_e
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_f

    .line 642
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/maps/f/b/e;->p:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_16

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 644
    :cond_f
    :goto_8
    iget-object v0, p0, Lcom/google/maps/f/b/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 645
    return-void

    .line 612
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    :cond_11
    move v0, v2

    .line 621
    goto/16 :goto_3

    :cond_12
    move v3, v2

    .line 624
    goto/16 :goto_4

    .line 627
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 633
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 636
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 642
    :cond_16
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 576
    iget-byte v0, p0, Lcom/google/maps/f/b/e;->r:B

    .line 577
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 593
    :cond_0
    :goto_0
    return v2

    .line 578
    :cond_1
    if-eqz v0, :cond_0

    .line 580
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 581
    iget-object v0, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 582
    iput-byte v2, p0, Lcom/google/maps/f/b/e;->r:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 580
    goto :goto_1

    :cond_3
    move v1, v2

    .line 586
    :goto_2
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 587
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/b/a;->d()Lcom/google/maps/f/b/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/b/a;

    invoke-virtual {v0}, Lcom/google/maps/f/b/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 588
    iput-byte v2, p0, Lcom/google/maps/f/b/e;->r:B

    goto :goto_0

    .line 586
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 592
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/f/b/e;->r:B

    move v2, v3

    .line 593
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 649
    iget v0, p0, Lcom/google/maps/f/b/e;->s:I

    .line 650
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 715
    :goto_0
    return v0

    .line 653
    :cond_0
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_15

    .line 655
    iget-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 657
    :goto_2
    iget v2, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1

    .line 658
    iget-object v2, p0, Lcom/google/maps/f/b/e;->j:Lcom/google/n/ao;

    .line 659
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 661
    :goto_3
    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 662
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/f/b/e;->k:Ljava/util/List;

    .line 663
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 661
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 655
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 665
    :cond_3
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_4

    .line 666
    iget-object v0, p0, Lcom/google/maps/f/b/e;->l:Lcom/google/n/ao;

    .line 667
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 669
    :cond_4
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    .line 670
    const/4 v2, 0x5

    .line 671
    iget-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->c:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 673
    :cond_5
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 674
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/f/b/e;->h:Lcom/google/n/ao;

    .line 675
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 677
    :cond_6
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 678
    iget-object v0, p0, Lcom/google/maps/f/b/e;->g:Lcom/google/n/ao;

    .line 679
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 681
    :cond_7
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_8

    .line 682
    const/16 v0, 0xa

    iget-boolean v2, p0, Lcom/google/maps/f/b/e;->e:Z

    .line 683
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 685
    :cond_8
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_9

    .line 686
    const/16 v0, 0xb

    iget-boolean v2, p0, Lcom/google/maps/f/b/e;->f:Z

    .line 687
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 689
    :cond_9
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_a

    .line 690
    const/16 v2, 0xc

    .line 691
    iget-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 693
    :cond_a
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_b

    .line 694
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/maps/f/b/e;->i:Lcom/google/n/ao;

    .line 695
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 697
    :cond_b
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_c

    .line 698
    const/16 v2, 0x11

    .line 699
    iget-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->m:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 701
    :cond_c
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_d

    .line 702
    const/16 v2, 0x12

    .line 703
    iget-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/f/b/e;->n:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 705
    :cond_d
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_e

    .line 706
    const/16 v0, 0x13

    iget-object v2, p0, Lcom/google/maps/f/b/e;->o:Lcom/google/n/ao;

    .line 707
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 709
    :cond_e
    iget v0, p0, Lcom/google/maps/f/b/e;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_f

    .line 710
    const/16 v0, 0x14

    iget v2, p0, Lcom/google/maps/f/b/e;->p:I

    .line 711
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_14

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 713
    :cond_f
    iget-object v0, p0, Lcom/google/maps/f/b/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 714
    iput v0, p0, Lcom/google/maps/f/b/e;->s:I

    goto/16 :goto_0

    .line 671
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 691
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 699
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 703
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 711
    :cond_14
    const/16 v0, 0xa

    goto :goto_8

    :cond_15
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/b/e;->newBuilder()Lcom/google/maps/f/b/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/f/b/g;->a(Lcom/google/maps/f/b/e;)Lcom/google/maps/f/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/f/b/e;->newBuilder()Lcom/google/maps/f/b/g;

    move-result-object v0

    return-object v0
.end method
