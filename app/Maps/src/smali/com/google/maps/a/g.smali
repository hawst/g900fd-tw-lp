.class public final Lcom/google/maps/a/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/a/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/a/e;",
        "Lcom/google/maps/a/g;",
        ">;",
        "Lcom/google/maps/a/h;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:D

.field public c:D

.field public d:D


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/maps/a/e;->e:Lcom/google/maps/a/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 264
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 312
    :goto_0
    return-object p0

    .line 302
    :cond_0
    iget v2, p1, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 303
    iget-wide v2, p1, Lcom/google/maps/a/e;->b:D

    iget v4, p0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, p0, Lcom/google/maps/a/g;->b:D

    .line 305
    :cond_1
    iget v2, p1, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 306
    iget-wide v2, p1, Lcom/google/maps/a/e;->c:D

    iget v4, p0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, p0, Lcom/google/maps/a/g;->c:D

    .line 308
    :cond_2
    iget v2, p1, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 309
    iget-wide v0, p1, Lcom/google/maps/a/e;->d:D

    iget v2, p0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/a/g;->a:I

    iput-wide v0, p0, Lcom/google/maps/a/g;->d:D

    .line 311
    :cond_3
    iget-object v0, p1, Lcom/google/maps/a/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 302
    goto :goto_1

    :cond_5
    move v2, v1

    .line 305
    goto :goto_2

    :cond_6
    move v0, v1

    .line 308
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/maps/a/g;->c()Lcom/google/maps/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 255
    check-cast p1, Lcom/google/maps/a/e;

    invoke-virtual {p0, p1}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/a/e;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 281
    new-instance v2, Lcom/google/maps/a/e;

    invoke-direct {v2, p0}, Lcom/google/maps/a/e;-><init>(Lcom/google/n/v;)V

    .line 282
    iget v3, p0, Lcom/google/maps/a/g;->a:I

    .line 283
    const/4 v1, 0x0

    .line 284
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 287
    :goto_0
    iget-wide v4, p0, Lcom/google/maps/a/g;->b:D

    iput-wide v4, v2, Lcom/google/maps/a/e;->b:D

    .line 288
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 289
    or-int/lit8 v0, v0, 0x2

    .line 291
    :cond_0
    iget-wide v4, p0, Lcom/google/maps/a/g;->c:D

    iput-wide v4, v2, Lcom/google/maps/a/e;->c:D

    .line 292
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 293
    or-int/lit8 v0, v0, 0x4

    .line 295
    :cond_1
    iget-wide v4, p0, Lcom/google/maps/a/g;->d:D

    iput-wide v4, v2, Lcom/google/maps/a/e;->d:D

    .line 296
    iput v0, v2, Lcom/google/maps/a/e;->a:I

    .line 297
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
