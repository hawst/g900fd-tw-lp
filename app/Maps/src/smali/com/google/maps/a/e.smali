.class public final Lcom/google/maps/a/e;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/a/h;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/a/e;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/a/e;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:D

.field public c:D

.field public d:D

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/maps/a/f;

    invoke-direct {v0}, Lcom/google/maps/a/f;-><init>()V

    sput-object v0, Lcom/google/maps/a/e;->PARSER:Lcom/google/n/ax;

    .line 183
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/a/e;->h:Lcom/google/n/aw;

    .line 421
    new-instance v0, Lcom/google/maps/a/e;

    invoke-direct {v0}, Lcom/google/maps/a/e;-><init>()V

    sput-object v0, Lcom/google/maps/a/e;->e:Lcom/google/maps/a/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 133
    iput-byte v2, p0, Lcom/google/maps/a/e;->f:B

    .line 158
    iput v2, p0, Lcom/google/maps/a/e;->g:I

    .line 18
    iput-wide v0, p0, Lcom/google/maps/a/e;->b:D

    .line 19
    iput-wide v0, p0, Lcom/google/maps/a/e;->c:D

    .line 20
    iput-wide v0, p0, Lcom/google/maps/a/e;->d:D

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/a/e;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget v3, p0, Lcom/google/maps/a/e;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/a/e;->a:I

    .line 48
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/a/e;->d:D
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/a/e;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/a/e;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/a/e;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/a/e;->b:D
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/a/e;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/a/e;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/a/e;->c:D
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/a/e;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 133
    iput-byte v0, p0, Lcom/google/maps/a/e;->f:B

    .line 158
    iput v0, p0, Lcom/google/maps/a/e;->g:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;
    .locals 1

    .prologue
    .line 248
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/a/e;
    .locals 1

    .prologue
    .line 424
    sget-object v0, Lcom/google/maps/a/e;->e:Lcom/google/maps/a/e;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/a/g;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/maps/a/g;

    invoke-direct {v0}, Lcom/google/maps/a/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/a/e;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 145
    invoke-virtual {p0}, Lcom/google/maps/a/e;->c()I

    .line 146
    iget v0, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 147
    iget-wide v0, p0, Lcom/google/maps/a/e;->d:D

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 149
    :cond_0
    iget v0, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 150
    iget-wide v0, p0, Lcom/google/maps/a/e;->b:D

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 152
    :cond_1
    iget v0, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 153
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/maps/a/e;->c:D

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/google/maps/a/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 156
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 135
    iget-byte v1, p0, Lcom/google/maps/a/e;->f:B

    .line 136
    if-ne v1, v0, :cond_0

    .line 140
    :goto_0
    return v0

    .line 137
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/a/e;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 160
    iget v0, p0, Lcom/google/maps/a/e;->g:I

    .line 161
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 178
    :goto_0
    return v0

    .line 164
    :cond_0
    iget v0, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 165
    iget-wide v2, p0, Lcom/google/maps/a/e;->d:D

    .line 166
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 168
    :goto_1
    iget v2, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    .line 169
    iget-wide v2, p0, Lcom/google/maps/a/e;->b:D

    .line 170
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 172
    :cond_1
    iget v2, p0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    .line 173
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/maps/a/e;->c:D

    .line 174
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/google/maps/a/e;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    iput v0, p0, Lcom/google/maps/a/e;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v0

    return-object v0
.end method
