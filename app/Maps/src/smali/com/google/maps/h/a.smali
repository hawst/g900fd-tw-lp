.class public final Lcom/google/maps/h/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/h/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/h/a;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/h/a;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/maps/h/b;

    invoke-direct {v0}, Lcom/google/maps/h/b;-><init>()V

    sput-object v0, Lcom/google/maps/h/a;->PARSER:Lcom/google/n/ax;

    .line 218
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/h/a;->h:Lcom/google/n/aw;

    .line 534
    new-instance v0, Lcom/google/maps/h/a;

    invoke-direct {v0}, Lcom/google/maps/h/a;-><init>()V

    sput-object v0, Lcom/google/maps/h/a;->e:Lcom/google/maps/h/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 108
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    .line 160
    iput-byte v2, p0, Lcom/google/maps/h/a;->f:B

    .line 191
    iput v2, p0, Lcom/google/maps/h/a;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/h/a;->c:I

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v8, 0x4

    .line 27
    invoke-direct {p0}, Lcom/google/maps/h/a;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 35
    sparse-switch v1, :sswitch_data_0

    .line 40
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 42
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 48
    iget v1, p0, Lcom/google/maps/h/a;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/h/a;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 79
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 80
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    .line 88
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/h/a;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget v1, p0, Lcom/google/maps/h/a;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/h/a;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/maps/h/a;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 81
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 82
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_8

    .line 58
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 59
    or-int/lit8 v1, v0, 0x4

    .line 61
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 66
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 67
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_2

    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_3

    move v1, v2

    :goto_5
    if-lez v1, :cond_2

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    .line 69
    or-int/lit8 v0, v0, 0x4

    .line 71
    :cond_2
    :goto_6
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_4

    move v1, v2

    :goto_7
    if-lez v1, :cond_5

    .line 72
    iget-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 85
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 67
    :cond_3
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_5

    .line 71
    :cond_4
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_7

    .line 74
    :cond_5
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 85
    :cond_6
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_7

    .line 86
    iget-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    .line 88
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/h/a;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 81
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 79
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_4

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 108
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    .line 160
    iput-byte v1, p0, Lcom/google/maps/h/a;->f:B

    .line 191
    iput v1, p0, Lcom/google/maps/h/a;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/h/a;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/google/maps/h/a;->e:Lcom/google/maps/h/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/h/c;
    .locals 1

    .prologue
    .line 280
    new-instance v0, Lcom/google/maps/h/c;

    invoke-direct {v0}, Lcom/google/maps/h/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/h/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/maps/h/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    .line 178
    invoke-virtual {p0}, Lcom/google/maps/h/a;->c()I

    .line 179
    iget v1, p0, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 180
    iget-object v1, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 182
    :cond_0
    iget v1, p0, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 183
    iget v1, p0, Lcom/google/maps/h/a;->c:I

    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_1
    :goto_0
    move v1, v0

    .line 185
    :goto_1
    iget-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 186
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 183
    :cond_2
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 188
    :cond_3
    iget-object v0, p0, Lcom/google/maps/h/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 189
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 162
    iget-byte v0, p0, Lcom/google/maps/h/a;->f:B

    .line 163
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 173
    :goto_0
    return v0

    .line 164
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 166
    :cond_1
    iget v0, p0, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/h/e;

    invoke-virtual {v0}, Lcom/google/maps/h/e;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 168
    iput-byte v2, p0, Lcom/google/maps/h/a;->f:B

    move v0, v2

    .line 169
    goto :goto_0

    :cond_2
    move v0, v2

    .line 166
    goto :goto_1

    .line 172
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/h/a;->f:B

    move v0, v1

    .line 173
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 193
    iget v0, p0, Lcom/google/maps/h/a;->g:I

    .line 194
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 213
    :goto_0
    return v0

    .line 197
    :cond_0
    iget v0, p0, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 198
    iget-object v0, p0, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    .line 199
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 201
    :goto_1
    iget v2, p0, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 202
    iget v2, p0, Lcom/google/maps/h/a;->c:I

    .line 203
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 208
    add-int/2addr v0, v1

    .line 209
    iget-object v1, p0, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 211
    iget-object v1, p0, Lcom/google/maps/h/a;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    iput v0, p0, Lcom/google/maps/h/a;->g:I

    goto :goto_0

    .line 203
    :cond_2
    const/16 v1, 0xa

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/h/a;->newBuilder()Lcom/google/maps/h/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/h/c;->a(Lcom/google/maps/h/a;)Lcom/google/maps/h/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/h/a;->newBuilder()Lcom/google/maps/h/c;

    move-result-object v0

    return-object v0
.end method
