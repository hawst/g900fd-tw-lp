.class public final Lcom/google/maps/h/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/h/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/h/a;",
        "Lcom/google/maps/h/c;",
        ">;",
        "Lcom/google/maps/h/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/google/maps/h/a;->e:Lcom/google/maps/h/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 373
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/h/c;->b:Lcom/google/n/ao;

    .line 464
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    .line 299
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/h/a;)Lcom/google/maps/h/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 339
    invoke-static {}, Lcom/google/maps/h/a;->d()Lcom/google/maps/h/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 358
    :goto_0
    return-object p0

    .line 340
    :cond_0
    iget v2, p1, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 341
    iget-object v2, p0, Lcom/google/maps/h/c;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 342
    iget v2, p0, Lcom/google/maps/h/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/h/c;->a:I

    .line 344
    :cond_1
    iget v2, p1, Lcom/google/maps/h/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 345
    iget v0, p1, Lcom/google/maps/h/a;->c:I

    iget v1, p0, Lcom/google/maps/h/c;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/h/c;->a:I

    iput v0, p0, Lcom/google/maps/h/c;->c:I

    .line 347
    :cond_2
    iget-object v0, p1, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 348
    iget-object v0, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 349
    iget-object v0, p1, Lcom/google/maps/h/a;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    .line 350
    iget v0, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/maps/h/c;->a:I

    .line 357
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/maps/h/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 340
    goto :goto_1

    :cond_5
    move v0, v1

    .line 344
    goto :goto_2

    .line 352
    :cond_6
    iget v0, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/h/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/h/c;->a:I

    .line 353
    :cond_7
    iget-object v0, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/h/a;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 290
    new-instance v2, Lcom/google/maps/h/a;

    invoke-direct {v2, p0}, Lcom/google/maps/h/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/maps/h/a;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/h/c;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/h/c;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/h/c;->c:I

    iput v1, v2, Lcom/google/maps/h/a;->c:I

    iget v1, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/maps/h/c;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/h/c;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/h/a;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/h/a;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 290
    check-cast p1, Lcom/google/maps/h/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/h/c;->a(Lcom/google/maps/h/a;)Lcom/google/maps/h/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 362
    iget v0, p0, Lcom/google/maps/h/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/google/maps/h/c;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/h/e;

    invoke-virtual {v0}, Lcom/google/maps/h/e;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 368
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 362
    goto :goto_0

    :cond_1
    move v0, v2

    .line 368
    goto :goto_1
.end method
