.class public final Lcom/google/maps/g/qd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ql;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qd;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/qd;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/Object;

.field g:I

.field h:Lcom/google/n/ao;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/aq;

.field l:I

.field m:I

.field n:I

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/maps/g/qe;

    invoke-direct {v0}, Lcom/google/maps/g/qe;-><init>()V

    sput-object v0, Lcom/google/maps/g/qd;->PARSER:Lcom/google/n/ax;

    .line 789
    new-instance v0, Lcom/google/maps/g/qf;

    invoke-direct {v0}, Lcom/google/maps/g/qf;-><init>()V

    .line 852
    new-instance v0, Lcom/google/maps/g/qg;

    invoke-direct {v0}, Lcom/google/maps/g/qg;-><init>()V

    .line 957
    new-instance v0, Lcom/google/maps/g/qh;

    invoke-direct {v0}, Lcom/google/maps/g/qh;-><init>()V

    .line 1215
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/qd;->r:Lcom/google/n/aw;

    .line 2226
    new-instance v0, Lcom/google/maps/g/qd;

    invoke-direct {v0}, Lcom/google/maps/g/qd;-><init>()V

    sput-object v0, Lcom/google/maps/g/qd;->o:Lcom/google/maps/g/qd;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 818
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    .line 939
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    .line 986
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    .line 1075
    iput-byte v4, p0, Lcom/google/maps/g/qd;->p:B

    .line 1130
    iput v4, p0, Lcom/google/maps/g/qd;->q:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/maps/g/qd;->d:I

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    .line 23
    iput v2, p0, Lcom/google/maps/g/qd;->g:I

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 27
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    .line 28
    iput v2, p0, Lcom/google/maps/g/qd;->l:I

    .line 29
    iput v2, p0, Lcom/google/maps/g/qd;->m:I

    .line 30
    iput v2, p0, Lcom/google/maps/g/qd;->n:I

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/16 v9, 0x80

    const/16 v8, 0x8

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 37
    invoke-direct {p0}, Lcom/google/maps/g/qd;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 43
    :cond_0
    :goto_0
    if-nez v3, :cond_1a

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 52
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 58
    invoke-static {v0}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v6

    .line 59
    if-nez v6, :cond_5

    .line 60
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 224
    :catch_0
    move-exception v0

    .line 225
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 231
    iget-object v2, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 233
    :cond_1
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_2

    .line 234
    iget-object v2, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 236
    :cond_2
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v9, :cond_3

    .line 237
    iget-object v2, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 239
    :cond_3
    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_4

    .line 240
    iget-object v1, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    .line 242
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qd;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :cond_5
    and-int/lit8 v6, v1, 0x1

    if-eq v6, v4, :cond_6

    .line 63
    :try_start_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 64
    or-int/lit8 v1, v1, 0x1

    .line 66
    :cond_6
    iget-object v6, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 226
    :catch_1
    move-exception v0

    .line 227
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 228
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 71
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 72
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 73
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_7

    move v0, v2

    :goto_2
    if-lez v0, :cond_a

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 75
    invoke-static {v0}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v7

    .line 76
    if-nez v7, :cond_8

    .line 77
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 73
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 79
    :cond_8
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_9

    .line 80
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 81
    or-int/lit8 v1, v1, 0x1

    .line 83
    :cond_9
    iget-object v7, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 86
    :cond_a
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 90
    :sswitch_3
    iget-object v0, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 91
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    goto/16 :goto_0

    .line 95
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 96
    invoke-static {v0}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v6

    .line 97
    if-nez v6, :cond_b

    .line 98
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 100
    :cond_b
    iget v6, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/qd;->a:I

    .line 101
    iput v0, p0, Lcom/google/maps/g/qd;->d:I

    goto/16 :goto_0

    .line 106
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 107
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v6

    .line 108
    if-nez v6, :cond_c

    .line 109
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 111
    :cond_c
    and-int/lit8 v6, v1, 0x8

    if-eq v6, v8, :cond_d

    .line 112
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 113
    or-int/lit8 v1, v1, 0x8

    .line 115
    :cond_d
    iget-object v6, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 120
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 121
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 122
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_e

    move v0, v2

    :goto_4
    if-lez v0, :cond_11

    .line 123
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 124
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v7

    .line 125
    if-nez v7, :cond_f

    .line 126
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 122
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 128
    :cond_f
    and-int/lit8 v7, v1, 0x8

    if-eq v7, v8, :cond_10

    .line 129
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 130
    or-int/lit8 v1, v1, 0x8

    .line 132
    :cond_10
    iget-object v7, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 135
    :cond_11
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 139
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 140
    iget v6, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/qd;->a:I

    .line 141
    iput-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 145
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 146
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v6

    .line 147
    if-nez v6, :cond_12

    .line 148
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 150
    :cond_12
    iget v6, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/maps/g/qd;->a:I

    .line 151
    iput v0, p0, Lcom/google/maps/g/qd;->g:I

    goto/16 :goto_0

    .line 156
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 157
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    goto/16 :goto_0

    .line 161
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 162
    invoke-static {v0}, Lcom/google/maps/g/qj;->a(I)Lcom/google/maps/g/qj;

    move-result-object v6

    .line 163
    if-nez v6, :cond_13

    .line 164
    const/16 v6, 0xa

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 166
    :cond_13
    and-int/lit16 v6, v1, 0x80

    if-eq v6, v9, :cond_14

    .line 167
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 168
    or-int/lit16 v1, v1, 0x80

    .line 170
    :cond_14
    iget-object v6, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 175
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 176
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 177
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_15

    move v0, v2

    :goto_6
    if-lez v0, :cond_18

    .line 178
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 179
    invoke-static {v0}, Lcom/google/maps/g/qj;->a(I)Lcom/google/maps/g/qj;

    move-result-object v7

    .line 180
    if-nez v7, :cond_16

    .line 181
    const/16 v7, 0xa

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 177
    :cond_15
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_6

    .line 183
    :cond_16
    and-int/lit16 v7, v1, 0x80

    if-eq v7, v9, :cond_17

    .line 184
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 185
    or-int/lit16 v1, v1, 0x80

    .line 187
    :cond_17
    iget-object v7, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 190
    :cond_18
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 194
    :sswitch_c
    iget-object v0, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 195
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    goto/16 :goto_0

    .line 199
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 200
    and-int/lit16 v6, v1, 0x200

    const/16 v7, 0x200

    if-eq v6, v7, :cond_19

    .line 201
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    .line 202
    or-int/lit16 v1, v1, 0x200

    .line 204
    :cond_19
    iget-object v6, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 208
    :sswitch_e
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/qd;->l:I

    goto/16 :goto_0

    .line 213
    :sswitch_f
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    .line 214
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/qd;->m:I

    goto/16 :goto_0

    .line 218
    :sswitch_10
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/qd;->a:I

    .line 219
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/qd;->n:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 230
    :cond_1a
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_1b

    .line 231
    iget-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 233
    :cond_1b
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v8, :cond_1c

    .line 234
    iget-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 236
    :cond_1c
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v9, :cond_1d

    .line 237
    iget-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 239
    :cond_1d
    and-int/lit16 v0, v1, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1e

    .line 240
    iget-object v0, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    .line 242
    :cond_1e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->au:Lcom/google/n/bn;

    .line 243
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x68 -> :sswitch_e
        0x70 -> :sswitch_f
        0x78 -> :sswitch_10
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 818
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    .line 939
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    .line 986
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    .line 1075
    iput-byte v1, p0, Lcom/google/maps/g/qd;->p:B

    .line 1130
    iput v1, p0, Lcom/google/maps/g/qd;->q:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/qd;
    .locals 1

    .prologue
    .line 2229
    sget-object v0, Lcom/google/maps/g/qd;->o:Lcom/google/maps/g/qd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/qi;
    .locals 1

    .prologue
    .line 1277
    new-instance v0, Lcom/google/maps/g/qi;

    invoke-direct {v0}, Lcom/google/maps/g/qi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    sget-object v0, Lcom/google/maps/g/qd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 1087
    invoke-virtual {p0}, Lcom/google/maps/g/qd;->c()I

    move v1, v2

    .line 1088
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1089
    iget-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1088
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1089
    :cond_0
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 1091
    :cond_1
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_2

    .line 1092
    iget-object v0, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1094
    :cond_2
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 1095
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/qd;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_3
    :goto_2
    move v1, v2

    .line 1097
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1098
    iget-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1097
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1095
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 1098
    :cond_5
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 1100
    :cond_6
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_7

    .line 1101
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1103
    :cond_7
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v9, :cond_8

    .line 1104
    iget v0, p0, Lcom/google/maps/g/qd;->g:I

    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1106
    :cond_8
    :goto_6
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 1107
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_9
    move v1, v2

    .line 1109
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 1110
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1109
    :goto_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1101
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 1104
    :cond_b
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 1110
    :cond_c
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    .line 1112
    :cond_d
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_e

    .line 1113
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_e
    move v0, v2

    .line 1115
    :goto_9
    iget-object v1, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_f

    .line 1116
    const/16 v1, 0xc

    iget-object v3, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1115
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1118
    :cond_f
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_10

    .line 1119
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/g/qd;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_13

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1121
    :cond_10
    :goto_a
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_11

    .line 1122
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/maps/g/qd;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_14

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1124
    :cond_11
    :goto_b
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_12

    .line 1125
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/maps/g/qd;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_15

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1127
    :cond_12
    :goto_c
    iget-object v0, p0, Lcom/google/maps/g/qd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1128
    return-void

    .line 1119
    :cond_13
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a

    .line 1122
    :cond_14
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b

    .line 1125
    :cond_15
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_c
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1077
    iget-byte v1, p0, Lcom/google/maps/g/qd;->p:B

    .line 1078
    if-ne v1, v0, :cond_0

    .line 1082
    :goto_0
    return v0

    .line 1079
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1081
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/qd;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 1132
    iget v0, p0, Lcom/google/maps/g/qd;->q:I

    .line 1133
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1210
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1138
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1139
    iget-object v0, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    .line 1140
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 1138
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 1140
    goto :goto_2

    .line 1142
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 1143
    iget-object v1, p0, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1145
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 1146
    iget-object v1, p0, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    .line 1147
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1149
    :cond_3
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_16

    .line 1150
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/maps/g/qd;->d:I

    .line 1151
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    move v1, v0

    :goto_4
    move v3, v2

    move v5, v2

    .line 1155
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1156
    iget-object v0, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    .line 1157
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v5, v0

    .line 1155
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_4
    move v1, v4

    .line 1151
    goto :goto_3

    :cond_5
    move v0, v4

    .line 1157
    goto :goto_6

    .line 1159
    :cond_6
    add-int v0, v1, v5

    .line 1160
    iget-object v1, p0, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 1162
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_15

    .line 1163
    const/4 v3, 0x7

    .line 1164
    iget-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 1166
    :goto_8
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_7

    .line 1167
    iget v1, p0, Lcom/google/maps/g/qd;->g:I

    .line 1168
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v1, :cond_9

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_9
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1170
    :cond_7
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_14

    .line 1171
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    .line 1172
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    :goto_a
    move v3, v2

    move v5, v2

    .line 1176
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    .line 1177
    iget-object v0, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    .line 1178
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v5, v0

    .line 1176
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 1164
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_9
    move v1, v4

    .line 1168
    goto :goto_9

    :cond_a
    move v0, v4

    .line 1178
    goto :goto_c

    .line 1180
    :cond_b
    add-int v0, v1, v5

    .line 1181
    iget-object v1, p0, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1183
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_c

    .line 1184
    const/16 v1, 0xb

    iget-object v3, p0, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    .line 1185
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_c
    move v1, v2

    move v3, v2

    .line 1189
    :goto_d
    iget-object v5, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v1, v5, :cond_d

    .line 1190
    iget-object v5, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    .line 1191
    invoke-interface {v5, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 1189
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 1193
    :cond_d
    add-int/2addr v0, v3

    .line 1194
    iget-object v1, p0, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 1196
    iget v0, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_13

    .line 1197
    const/16 v0, 0xd

    iget v3, p0, Lcom/google/maps/g/qd;->l:I

    .line 1198
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_e
    add-int/2addr v0, v5

    add-int/2addr v0, v1

    .line 1200
    :goto_f
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_e

    .line 1201
    const/16 v1, 0xe

    iget v3, p0, Lcom/google/maps/g/qd;->m:I

    .line 1202
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_12

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_10
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 1204
    :cond_e
    iget v1, p0, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_10

    .line 1205
    const/16 v1, 0xf

    iget v3, p0, Lcom/google/maps/g/qd;->n:I

    .line 1206
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_f
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 1208
    :cond_10
    iget-object v1, p0, Lcom/google/maps/g/qd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1209
    iput v0, p0, Lcom/google/maps/g/qd;->q:I

    goto/16 :goto_0

    :cond_11
    move v0, v4

    .line 1198
    goto :goto_e

    :cond_12
    move v1, v4

    .line 1202
    goto :goto_10

    :cond_13
    move v0, v1

    goto :goto_f

    :cond_14
    move v1, v0

    goto/16 :goto_a

    :cond_15
    move v0, v1

    goto/16 :goto_8

    :cond_16
    move v1, v0

    goto/16 :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/qd;->newBuilder()Lcom/google/maps/g/qi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/qi;->a(Lcom/google/maps/g/qd;)Lcom/google/maps/g/qi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/qd;->newBuilder()Lcom/google/maps/g/qi;

    move-result-object v0

    return-object v0
.end method
