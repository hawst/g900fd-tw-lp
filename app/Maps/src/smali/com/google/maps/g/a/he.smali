.class public final enum Lcom/google/maps/g/a/he;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/he;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/he;

.field public static final enum b:Lcom/google/maps/g/a/he;

.field public static final enum c:Lcom/google/maps/g/a/he;

.field public static final enum d:Lcom/google/maps/g/a/he;

.field public static final enum e:Lcom/google/maps/g/a/he;

.field public static final enum f:Lcom/google/maps/g/a/he;

.field private static final synthetic h:[Lcom/google/maps/g/a/he;


# instance fields
.field public final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "SERVER_DEFINED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    .line 18
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "HIGHEST_SCORING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->b:Lcom/google/maps/g/a/he;

    .line 22
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "TYPICAL_PATTERNS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->c:Lcom/google/maps/g/a/he;

    .line 26
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "LEAVING_NOW_PATTERNS"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->d:Lcom/google/maps/g/a/he;

    .line 30
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "SCHEDULE_EXPLORATION"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->e:Lcom/google/maps/g/a/he;

    .line 34
    new-instance v0, Lcom/google/maps/g/a/he;

    const-string v1, "LAST_AVAILABLE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/he;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/he;->f:Lcom/google/maps/g/a/he;

    .line 9
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/maps/g/a/he;

    sget-object v1, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/he;->b:Lcom/google/maps/g/a/he;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/he;->c:Lcom/google/maps/g/a/he;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/he;->d:Lcom/google/maps/g/a/he;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/he;->e:Lcom/google/maps/g/a/he;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/he;->f:Lcom/google/maps/g/a/he;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/he;->h:[Lcom/google/maps/g/a/he;

    .line 84
    new-instance v0, Lcom/google/maps/g/a/hf;

    invoke-direct {v0}, Lcom/google/maps/g/a/hf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 94
    iput p3, p0, Lcom/google/maps/g/a/he;->g:I

    .line 95
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/he;
    .locals 1

    .prologue
    .line 68
    packed-switch p0, :pswitch_data_0

    .line 75
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 69
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 70
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/he;->b:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/he;->c:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 72
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/he;->d:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 73
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/he;->e:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 74
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/he;->f:Lcom/google/maps/g/a/he;

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/he;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/a/he;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/he;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/he;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/a/he;->h:[Lcom/google/maps/g/a/he;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/he;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/he;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/maps/g/a/he;->g:I

    return v0
.end method
