.class public final Lcom/google/maps/g/uf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/uo;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/uf;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/maps/g/uf;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field public e:I

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/google/maps/g/ug;

    invoke-direct {v0}, Lcom/google/maps/g/ug;-><init>()V

    sput-object v0, Lcom/google/maps/g/uf;->PARSER:Lcom/google/n/ax;

    .line 851
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/uf;->l:Lcom/google/n/aw;

    .line 1589
    new-instance v0, Lcom/google/maps/g/uf;

    invoke-direct {v0}, Lcom/google/maps/g/uf;-><init>()V

    sput-object v0, Lcom/google/maps/g/uf;->i:Lcom/google/maps/g/uf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 583
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    .line 773
    iput-byte v2, p0, Lcom/google/maps/g/uf;->j:B

    .line 810
    iput v2, p0, Lcom/google/maps/g/uf;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    .line 21
    iput v3, p0, Lcom/google/maps/g/uf;->e:I

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 24
    iput v3, p0, Lcom/google/maps/g/uf;->h:I

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x20

    const/16 v7, 0x10

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/maps/g/uf;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 37
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 39
    sparse-switch v4, :sswitch_data_0

    .line 44
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 46
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 52
    iget v4, p0, Lcom/google/maps/g/uf;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/uf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v7, :cond_1

    .line 111
    iget-object v2, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    .line 113
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v8, :cond_2

    .line 114
    iget-object v1, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 116
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/uf;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 57
    iget v5, p0, Lcom/google/maps/g/uf;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/uf;->a:I

    .line 58
    iput-object v4, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 106
    :catch_1
    move-exception v0

    .line 107
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/maps/g/uf;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/uf;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/uf;->e:I

    goto :goto_0

    .line 67
    :sswitch_4
    and-int/lit8 v4, v1, 0x10

    if-eq v4, v7, :cond_3

    .line 68
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    .line 70
    or-int/lit8 v1, v1, 0x10

    .line 72
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 72
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 77
    :sswitch_5
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v8, :cond_4

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 80
    or-int/lit8 v1, v1, 0x20

    .line 82
    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 82
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 87
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 88
    invoke-static {v4}, Lcom/google/maps/g/um;->a(I)Lcom/google/maps/g/um;

    move-result-object v5

    .line 89
    if-nez v5, :cond_5

    .line 90
    const/4 v5, 0x6

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_5
    iget v5, p0, Lcom/google/maps/g/uf;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/uf;->a:I

    .line 93
    iput v4, p0, Lcom/google/maps/g/uf;->h:I

    goto/16 :goto_0

    .line 98
    :sswitch_7
    iget-object v4, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 99
    iget v4, p0, Lcom/google/maps/g/uf;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/uf;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 110
    :cond_6
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_7

    .line 111
    iget-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    .line 113
    :cond_7
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v8, :cond_8

    .line 114
    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 116
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->au:Lcom/google/n/bn;

    .line 117
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 583
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    .line 773
    iput-byte v1, p0, Lcom/google/maps/g/uf;->j:B

    .line 810
    iput v1, p0, Lcom/google/maps/g/uf;->k:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/maps/g/uf;
    .locals 1

    .prologue
    .line 1592
    sget-object v0, Lcom/google/maps/g/uf;->i:Lcom/google/maps/g/uf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/uh;
    .locals 1

    .prologue
    .line 913
    new-instance v0, Lcom/google/maps/g/uh;

    invoke-direct {v0}, Lcom/google/maps/g/uh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/uf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcom/google/maps/g/uf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 785
    invoke-virtual {p0}, Lcom/google/maps/g/uf;->c()I

    .line 786
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 787
    iget-object v0, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 789
    :cond_0
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 790
    iget-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 792
    :cond_1
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 793
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/uf;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_2
    :goto_1
    move v1, v2

    .line 795
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 796
    iget-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 795
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 790
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 793
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_5
    move v1, v2

    .line 798
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 799
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 798
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 801
    :cond_6
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 802
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/g/uf;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 804
    :cond_7
    :goto_4
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_8

    .line 805
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 807
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/uf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 808
    return-void

    .line 802
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 775
    iget-byte v1, p0, Lcom/google/maps/g/uf;->j:B

    .line 776
    if-ne v1, v0, :cond_0

    .line 780
    :goto_0
    return v0

    .line 777
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 779
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/uf;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v3, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 812
    iget v0, p0, Lcom/google/maps/g/uf;->k:I

    .line 813
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 846
    :goto_0
    return v0

    .line 816
    :cond_0
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 817
    iget-object v0, p0, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    .line 818
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 820
    :goto_1
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 822
    iget-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 824
    :cond_1
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 825
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/maps/g/uf;->e:I

    .line 826
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    :cond_2
    move v4, v1

    move v1, v2

    .line 828
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 829
    iget-object v0, p0, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    .line 830
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 828
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 822
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_4
    move v0, v3

    .line 826
    goto :goto_3

    :cond_5
    move v1, v2

    .line 832
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 833
    const/4 v5, 0x5

    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 834
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 832
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 836
    :cond_6
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 837
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/g/uf;->h:I

    .line 838
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_7

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_7
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 840
    :cond_8
    iget v0, p0, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_9

    .line 841
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    .line 842
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 844
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/uf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 845
    iput v0, p0, Lcom/google/maps/g/uf;->k:I

    goto/16 :goto_0

    :cond_a
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    .line 627
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 628
    check-cast v0, Ljava/lang/String;

    .line 636
    :goto_0
    return-object v0

    .line 630
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 632
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 633
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 634
    iput-object v1, p0, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 636
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/uf;->newBuilder()Lcom/google/maps/g/uh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/uh;->a(Lcom/google/maps/g/uf;)Lcom/google/maps/g/uh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/uf;->newBuilder()Lcom/google/maps/g/uh;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/ui;",
            ">;"
        }
    .end annotation

    .prologue
    .line 721
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    .line 722
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 723
    iget-object v0, p0, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 724
    invoke-static {}, Lcom/google/maps/g/ui;->d()Lcom/google/maps/g/ui;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ui;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 726
    :cond_0
    return-object v1
.end method
