.class public final Lcom/google/maps/g/o;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/af;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/o;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/o;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/q;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/maps/g/xs;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/google/maps/g/p;

    invoke-direct {v0}, Lcom/google/maps/g/p;-><init>()V

    sput-object v0, Lcom/google/maps/g/o;->PARSER:Lcom/google/n/ax;

    .line 2542
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/o;->g:Lcom/google/n/aw;

    .line 2869
    new-instance v0, Lcom/google/maps/g/o;

    invoke-direct {v0}, Lcom/google/maps/g/o;-><init>()V

    sput-object v0, Lcom/google/maps/g/o;->d:Lcom/google/maps/g/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 161
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2499
    iput-byte v0, p0, Lcom/google/maps/g/o;->e:B

    .line 2521
    iput v0, p0, Lcom/google/maps/g/o;->f:I

    .line 162
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    .line 163
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 169
    invoke-direct {p0}, Lcom/google/maps/g/o;-><init>()V

    .line 172
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 175
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 176
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 177
    sparse-switch v0, :sswitch_data_0

    .line 182
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 184
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 180
    goto :goto_0

    .line 189
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_1

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    .line 191
    or-int/lit8 v1, v1, 0x1

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/q;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_2

    .line 218
    iget-object v1, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    .line 220
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/o;->au:Lcom/google/n/bn;

    throw v0

    .line 197
    :sswitch_2
    const/4 v0, 0x0

    .line 198
    :try_start_2
    iget v2, p0, Lcom/google/maps/g/o;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_6

    .line 199
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    invoke-static {v0}, Lcom/google/maps/g/xs;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v0

    move-object v2, v0

    .line 201
    :goto_1
    sget-object v0, Lcom/google/maps/g/xs;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/xs;

    iput-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    .line 202
    if-eqz v2, :cond_3

    .line 203
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/xu;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    .line 204
    invoke-virtual {v2}, Lcom/google/maps/g/xu;->c()Lcom/google/maps/g/xs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    .line 206
    :cond_3
    iget v0, p0, Lcom/google/maps/g/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/o;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 213
    :catch_1
    move-exception v0

    .line 214
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 215
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 217
    :cond_4
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_5

    .line 218
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    .line 220
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/o;->au:Lcom/google/n/bn;

    .line 221
    return-void

    :cond_6
    move-object v2, v0

    goto :goto_1

    .line 177
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 159
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2499
    iput-byte v0, p0, Lcom/google/maps/g/o;->e:B

    .line 2521
    iput v0, p0, Lcom/google/maps/g/o;->f:I

    .line 160
    return-void
.end method

.method public static d()Lcom/google/maps/g/o;
    .locals 1

    .prologue
    .line 2872
    sget-object v0, Lcom/google/maps/g/o;->d:Lcom/google/maps/g/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ae;
    .locals 1

    .prologue
    .line 2604
    new-instance v0, Lcom/google/maps/g/ae;

    invoke-direct {v0}, Lcom/google/maps/g/ae;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    sget-object v0, Lcom/google/maps/g/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 2511
    invoke-virtual {p0}, Lcom/google/maps/g/o;->c()I

    .line 2512
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2513
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2512
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2515
    :cond_0
    iget v0, p0, Lcom/google/maps/g/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 2516
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/xs;->d()Lcom/google/maps/g/xs;

    move-result-object v0

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2518
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2519
    return-void

    .line 2516
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2501
    iget-byte v1, p0, Lcom/google/maps/g/o;->e:B

    .line 2502
    if-ne v1, v0, :cond_0

    .line 2506
    :goto_0
    return v0

    .line 2503
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2505
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/o;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2523
    iget v0, p0, Lcom/google/maps/g/o;->f:I

    .line 2524
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2537
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2527
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2528
    iget-object v0, p0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    .line 2529
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2527
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2531
    :cond_1
    iget v0, p0, Lcom/google/maps/g/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 2532
    const/4 v1, 0x2

    .line 2533
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/xs;->d()Lcom/google/maps/g/xs;

    move-result-object v0

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2535
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2536
    iput v0, p0, Lcom/google/maps/g/o;->f:I

    goto :goto_0

    .line 2533
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/google/maps/g/o;->newBuilder()Lcom/google/maps/g/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ae;->a(Lcom/google/maps/g/o;)Lcom/google/maps/g/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/google/maps/g/o;->newBuilder()Lcom/google/maps/g/ae;

    move-result-object v0

    return-object v0
.end method
