.class public final Lcom/google/maps/g/hp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/hn;",
        "Lcom/google/maps/g/hp;",
        ">;",
        "Lcom/google/maps/g/hw;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/google/maps/a/a;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 777
    sget-object v0, Lcom/google/maps/g/hn;->g:Lcom/google/maps/g/hn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 853
    iput v1, p0, Lcom/google/maps/g/hp;->b:I

    .line 889
    iput v1, p0, Lcom/google/maps/g/hp;->c:I

    .line 925
    iput v1, p0, Lcom/google/maps/g/hp;->d:I

    .line 961
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    .line 1022
    iput v1, p0, Lcom/google/maps/g/hp;->f:I

    .line 778
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/hn;)Lcom/google/maps/g/hp;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 827
    invoke-static {}, Lcom/google/maps/g/hn;->d()Lcom/google/maps/g/hn;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 844
    :goto_0
    return-object p0

    .line 828
    :cond_0
    iget v0, p1, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 829
    iget v0, p1, Lcom/google/maps/g/hn;->b:I

    invoke-static {v0}, Lcom/google/maps/g/hs;->a(I)Lcom/google/maps/g/hs;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/hs;->a:Lcom/google/maps/g/hs;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 828
    goto :goto_1

    .line 829
    :cond_3
    iget v3, p0, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/hs;->n:I

    iput v0, p0, Lcom/google/maps/g/hp;->b:I

    .line 831
    :cond_4
    iget v0, p1, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 832
    iget v0, p1, Lcom/google/maps/g/hn;->c:I

    invoke-static {v0}, Lcom/google/maps/g/hq;->a(I)Lcom/google/maps/g/hq;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/hq;->a:Lcom/google/maps/g/hq;

    :cond_5
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    .line 831
    goto :goto_2

    .line 832
    :cond_7
    iget v3, p0, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/hq;->d:I

    iput v0, p0, Lcom/google/maps/g/hp;->c:I

    .line 834
    :cond_8
    iget v0, p1, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_c

    .line 835
    iget v0, p1, Lcom/google/maps/g/hn;->d:I

    invoke-static {v0}, Lcom/google/maps/g/hu;->a(I)Lcom/google/maps/g/hu;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/maps/g/hu;->a:Lcom/google/maps/g/hu;

    :cond_9
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v0, v2

    .line 834
    goto :goto_3

    .line 835
    :cond_b
    iget v3, p0, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/hu;->d:I

    iput v0, p0, Lcom/google/maps/g/hp;->d:I

    .line 837
    :cond_c
    iget v0, p1, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_f

    move v0, v1

    :goto_4
    if-eqz v0, :cond_d

    .line 838
    iget-object v0, p1, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/maps/g/hp;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v4, :cond_11

    iget-object v3, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_11

    iget-object v3, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    invoke-static {v3}, Lcom/google/maps/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/a/c;->c()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    :goto_6
    iget v0, p0, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/hp;->a:I

    .line 840
    :cond_d
    iget v0, p1, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_7
    if-eqz v0, :cond_14

    .line 841
    iget v0, p1, Lcom/google/maps/g/hn;->f:I

    invoke-static {v0}, Lcom/google/maps/g/op;->a(I)Lcom/google/maps/g/op;

    move-result-object v0

    if-nez v0, :cond_e

    sget-object v0, Lcom/google/maps/g/op;->a:Lcom/google/maps/g/op;

    :cond_e
    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move v0, v2

    .line 837
    goto :goto_4

    .line 838
    :cond_10
    iget-object v0, p1, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    goto :goto_5

    :cond_11
    iput-object v0, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    goto :goto_6

    :cond_12
    move v0, v2

    .line 840
    goto :goto_7

    .line 841
    :cond_13
    iget v1, p0, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/op;->e:I

    iput v0, p0, Lcom/google/maps/g/hp;->f:I

    .line 843
    :cond_14
    iget-object v0, p1, Lcom/google/maps/g/hn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 769
    new-instance v2, Lcom/google/maps/g/hn;

    invoke-direct {v2, p0}, Lcom/google/maps/g/hn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/hp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/maps/g/hp;->b:I

    iput v1, v2, Lcom/google/maps/g/hn;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/hp;->c:I

    iput v1, v2, Lcom/google/maps/g/hn;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/hp;->d:I

    iput v1, v2, Lcom/google/maps/g/hn;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    iput-object v1, v2, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/maps/g/hp;->f:I

    iput v1, v2, Lcom/google/maps/g/hn;->f:I

    iput v0, v2, Lcom/google/maps/g/hn;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 769
    check-cast p1, Lcom/google/maps/g/hn;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/hp;->a(Lcom/google/maps/g/hn;)Lcom/google/maps/g/hp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 848
    const/4 v0, 0x1

    return v0
.end method
