.class public final Lcom/google/maps/g/fx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ga;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fx;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/fx;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/maps/g/fc;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354
    new-instance v0, Lcom/google/maps/g/fy;

    invoke-direct {v0}, Lcom/google/maps/g/fy;-><init>()V

    sput-object v0, Lcom/google/maps/g/fx;->PARSER:Lcom/google/n/ax;

    .line 617
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/fx;->j:Lcom/google/n/aw;

    .line 1150
    new-instance v0, Lcom/google/maps/g/fx;

    invoke-direct {v0}, Lcom/google/maps/g/fx;-><init>()V

    sput-object v0, Lcom/google/maps/g/fx;->g:Lcom/google/maps/g/fx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 276
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 553
    iput-byte v0, p0, Lcom/google/maps/g/fx;->h:B

    .line 584
    iput v0, p0, Lcom/google/maps/g/fx;->i:I

    .line 277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    .line 278
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    .line 279
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    .line 280
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    .line 281
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 287
    invoke-direct {p0}, Lcom/google/maps/g/fx;-><init>()V

    .line 288
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 292
    const/4 v0, 0x0

    move v2, v0

    .line 293
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 294
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 295
    sparse-switch v0, :sswitch_data_0

    .line 300
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 302
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 298
    goto :goto_0

    .line 307
    :sswitch_1
    const/4 v0, 0x0

    .line 308
    iget v1, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 309
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    invoke-static {}, Lcom/google/maps/g/fc;->newBuilder()Lcom/google/maps/g/fe;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/fe;->a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;

    move-result-object v0

    move-object v1, v0

    .line 311
    :goto_1
    sget-object v0, Lcom/google/maps/g/fc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fc;

    iput-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    .line 312
    if-eqz v1, :cond_1

    .line 313
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/fe;->a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;

    .line 314
    invoke-virtual {v1}, Lcom/google/maps/g/fe;->c()Lcom/google/maps/g/fc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    .line 316
    :cond_1
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/fx;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fx;->au:Lcom/google/n/bn;

    throw v0

    .line 320
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 321
    iget v1, p0, Lcom/google/maps/g/fx;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/fx;->a:I

    .line 322
    iput-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 347
    :catch_1
    move-exception v0

    .line 348
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 349
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 326
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 327
    iget v1, p0, Lcom/google/maps/g/fx;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/fx;->a:I

    .line 328
    iput-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    goto :goto_0

    .line 332
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 333
    iget v1, p0, Lcom/google/maps/g/fx;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/fx;->a:I

    .line 334
    iput-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 338
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 339
    iget v1, p0, Lcom/google/maps/g/fx;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/fx;->a:I

    .line 340
    iput-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 351
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->au:Lcom/google/n/bn;

    .line 352
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 295
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 274
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 553
    iput-byte v0, p0, Lcom/google/maps/g/fx;->h:B

    .line 584
    iput v0, p0, Lcom/google/maps/g/fx;->i:I

    .line 275
    return-void
.end method

.method public static h()Lcom/google/maps/g/fx;
    .locals 1

    .prologue
    .line 1153
    sget-object v0, Lcom/google/maps/g/fx;->g:Lcom/google/maps/g/fx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/fz;
    .locals 1

    .prologue
    .line 679
    new-instance v0, Lcom/google/maps/g/fz;

    invoke-direct {v0}, Lcom/google/maps/g/fz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    sget-object v0, Lcom/google/maps/g/fx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 565
    invoke-virtual {p0}, Lcom/google/maps/g/fx;->c()I

    .line 566
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 567
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 569
    :cond_0
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 570
    iget-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 572
    :cond_1
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 573
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 575
    :cond_2
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 576
    iget-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 578
    :cond_3
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 579
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 581
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/fx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 582
    return-void

    .line 567
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto/16 :goto_0

    .line 570
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 573
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 576
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 579
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 555
    iget-byte v1, p0, Lcom/google/maps/g/fx;->h:B

    .line 556
    if-ne v1, v0, :cond_0

    .line 560
    :goto_0
    return v0

    .line 557
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 559
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/fx;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 586
    iget v0, p0, Lcom/google/maps/g/fx;->i:I

    .line 587
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 612
    :goto_0
    return v0

    .line 590
    :cond_0
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 592
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 594
    :goto_2
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 596
    iget-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 598
    :cond_1
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 599
    const/4 v3, 0x3

    .line 600
    iget-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 602
    :cond_2
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 604
    iget-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 606
    :cond_3
    iget v0, p0, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 607
    const/4 v3, 0x5

    .line 608
    iget-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 610
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/fx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 611
    iput v0, p0, Lcom/google/maps/g/fx;->i:I

    goto/16 :goto_0

    .line 592
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto/16 :goto_1

    .line 596
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 600
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 604
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 608
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    .line 398
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 399
    check-cast v0, Ljava/lang/String;

    .line 407
    :goto_0
    return-object v0

    .line 401
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 403
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 404
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 405
    iput-object v1, p0, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 407
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Lcom/google/maps/g/fx;->newBuilder()Lcom/google/maps/g/fz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/fz;->a(Lcom/google/maps/g/fx;)Lcom/google/maps/g/fz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Lcom/google/maps/g/fx;->newBuilder()Lcom/google/maps/g/fz;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    .line 440
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 441
    check-cast v0, Ljava/lang/String;

    .line 449
    :goto_0
    return-object v0

    .line 443
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 445
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 446
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    iput-object v1, p0, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 449
    goto :goto_0
.end method
