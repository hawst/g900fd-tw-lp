.class public final Lcom/google/maps/g/rv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ry;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rv;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/maps/g/rv;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2642
    new-instance v0, Lcom/google/maps/g/rw;

    invoke-direct {v0}, Lcom/google/maps/g/rw;-><init>()V

    sput-object v0, Lcom/google/maps/g/rv;->PARSER:Lcom/google/n/ax;

    .line 2686
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/rv;->d:Lcom/google/n/aw;

    .line 2797
    new-instance v0, Lcom/google/maps/g/rv;

    invoke-direct {v0}, Lcom/google/maps/g/rv;-><init>()V

    sput-object v0, Lcom/google/maps/g/rv;->a:Lcom/google/maps/g/rv;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2606
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2657
    iput-byte v0, p0, Lcom/google/maps/g/rv;->b:B

    .line 2673
    iput v0, p0, Lcom/google/maps/g/rv;->c:I

    .line 2607
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2613
    invoke-direct {p0}, Lcom/google/maps/g/rv;-><init>()V

    .line 2615
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2617
    const/4 v0, 0x0

    .line 2618
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2619
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2620
    packed-switch v3, :pswitch_data_0

    .line 2625
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2627
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2623
    goto :goto_0

    .line 2639
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rv;->au:Lcom/google/n/bn;

    .line 2640
    return-void

    .line 2633
    :catch_0
    move-exception v0

    .line 2634
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2639
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rv;->au:Lcom/google/n/bn;

    throw v0

    .line 2635
    :catch_1
    move-exception v0

    .line 2636
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2637
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2620
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2604
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2657
    iput-byte v0, p0, Lcom/google/maps/g/rv;->b:B

    .line 2673
    iput v0, p0, Lcom/google/maps/g/rv;->c:I

    .line 2605
    return-void
.end method

.method public static a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;
    .locals 1

    .prologue
    .line 2751
    invoke-static {}, Lcom/google/maps/g/rv;->newBuilder()Lcom/google/maps/g/rx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/rx;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/rv;
    .locals 1

    .prologue
    .line 2800
    sget-object v0, Lcom/google/maps/g/rv;->a:Lcom/google/maps/g/rv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/rx;
    .locals 1

    .prologue
    .line 2748
    new-instance v0, Lcom/google/maps/g/rx;

    invoke-direct {v0}, Lcom/google/maps/g/rx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2654
    sget-object v0, Lcom/google/maps/g/rv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 2669
    iget v0, p0, Lcom/google/maps/g/rv;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2670
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/rv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2671
    return-void

    .line 2669
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/maps/g/rv;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2659
    iget-byte v1, p0, Lcom/google/maps/g/rv;->b:B

    .line 2660
    if-ne v1, v0, :cond_0

    .line 2664
    :goto_0
    return v0

    .line 2661
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2663
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/rv;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2675
    iget v0, p0, Lcom/google/maps/g/rv;->c:I

    .line 2676
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2681
    :goto_0
    return v0

    .line 2678
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2680
    iput v0, p0, Lcom/google/maps/g/rv;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2598
    invoke-static {}, Lcom/google/maps/g/rv;->newBuilder()Lcom/google/maps/g/rx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/rx;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2598
    invoke-static {}, Lcom/google/maps/g/rv;->newBuilder()Lcom/google/maps/g/rx;

    move-result-object v0

    return-object v0
.end method
