.class public final Lcom/google/maps/g/ba;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/bh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ba;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/ba;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/google/maps/g/bb;

    invoke-direct {v0}, Lcom/google/maps/g/bb;-><init>()V

    sput-object v0, Lcom/google/maps/g/ba;->PARSER:Lcom/google/n/ax;

    .line 339
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ba;->h:Lcom/google/n/aw;

    .line 782
    new-instance v0, Lcom/google/maps/g/ba;

    invoke-direct {v0}, Lcom/google/maps/g/ba;-><init>()V

    sput-object v0, Lcom/google/maps/g/ba;->e:Lcom/google/maps/g/ba;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 176
    iput v1, p0, Lcom/google/maps/g/ba;->b:I

    .line 279
    iput-byte v0, p0, Lcom/google/maps/g/ba;->f:B

    .line 310
    iput v0, p0, Lcom/google/maps/g/ba;->g:I

    .line 18
    iput v1, p0, Lcom/google/maps/g/ba;->d:I

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/google/maps/g/ba;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 31
    :cond_0
    :goto_0
    if-nez v1, :cond_5

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 33
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 40
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-eq v0, v5, :cond_1

    .line 46
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 48
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 50
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ba;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ba;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-eq v0, v6, :cond_2

    .line 55
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    .line 57
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 57
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 59
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/g/ba;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 86
    :catch_1
    move-exception v0

    .line 87
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 88
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-eq v0, v7, :cond_3

    .line 64
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 66
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 68
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/ba;->b:I

    goto/16 :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 73
    invoke-static {v0}, Lcom/google/maps/g/bd;->a(I)Lcom/google/maps/g/bd;

    move-result-object v4

    .line 74
    if-nez v4, :cond_4

    .line 75
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 77
    :cond_4
    iget v4, p0, Lcom/google/maps/g/ba;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/ba;->a:I

    .line 78
    iput v0, p0, Lcom/google/maps/g/ba;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 90
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ba;->au:Lcom/google/n/bn;

    .line 91
    return-void

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ba;->b:I

    .line 279
    iput-byte v1, p0, Lcom/google/maps/g/ba;->f:B

    .line 310
    iput v1, p0, Lcom/google/maps/g/ba;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/ba;
    .locals 1

    .prologue
    .line 785
    sget-object v0, Lcom/google/maps/g/ba;->e:Lcom/google/maps/g/ba;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/bg;
    .locals 1

    .prologue
    .line 401
    new-instance v0, Lcom/google/maps/g/bg;

    invoke-direct {v0}, Lcom/google/maps/g/bg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/google/maps/g/ba;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 291
    invoke-virtual {p0}, Lcom/google/maps/g/ba;->c()I

    .line 292
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v2, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 294
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 293
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v3, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 298
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 297
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 300
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v4, :cond_2

    .line 301
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 302
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 301
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 304
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 305
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/ba;->d:I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 307
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 308
    return-void

    .line 305
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 281
    iget-byte v1, p0, Lcom/google/maps/g/ba;->f:B

    .line 282
    if-ne v1, v0, :cond_0

    .line 286
    :goto_0
    return v0

    .line 283
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 285
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/ba;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 312
    iget v0, p0, Lcom/google/maps/g/ba;->g:I

    .line 313
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 334
    :goto_0
    return v0

    .line 316
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v3, :cond_5

    .line 317
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 318
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 320
    :goto_1
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v4, :cond_1

    .line 321
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 322
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 324
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ba;->b:I

    if-ne v0, v5, :cond_2

    .line 325
    iget-object v0, p0, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 326
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 328
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 329
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/ba;->d:I

    .line 330
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 332
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 333
    iput v0, p0, Lcom/google/maps/g/ba;->g:I

    goto :goto_0

    .line 330
    :cond_4
    const/16 v0, 0xa

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ba;->newBuilder()Lcom/google/maps/g/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/bg;->a(Lcom/google/maps/g/ba;)Lcom/google/maps/g/bg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ba;->newBuilder()Lcom/google/maps/g/bg;

    move-result-object v0

    return-object v0
.end method
