.class public final Lcom/google/maps/g/m;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ah;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/m;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/m;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/maps/g/n;

    invoke-direct {v0}, Lcom/google/maps/g/n;-><init>()V

    sput-object v0, Lcom/google/maps/g/m;->PARSER:Lcom/google/n/ax;

    .line 3118
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/m;->m:Lcom/google/n/aw;

    .line 3769
    new-instance v0, Lcom/google/maps/g/m;

    invoke-direct {v0}, Lcom/google/maps/g/m;-><init>()V

    sput-object v0, Lcom/google/maps/g/m;->j:Lcom/google/maps/g/m;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    .line 2930
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    .line 2946
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    .line 2962
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    .line 2978
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    .line 2994
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    .line 3009
    iput-byte v3, p0, Lcom/google/maps/g/m;->k:B

    .line 3073
    iput v3, p0, Lcom/google/maps/g/m;->l:I

    .line 18
    iput v4, p0, Lcom/google/maps/g/m;->b:I

    .line 19
    iput v4, p0, Lcom/google/maps/g/m;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/m;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/m;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/m;->c:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/m;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 58
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/m;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 63
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/m;->a:I

    goto :goto_0

    .line 67
    :sswitch_4
    iget-object v3, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 68
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/m;->a:I

    goto :goto_0

    .line 72
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 73
    invoke-static {v3}, Lcom/google/o/f/j;->a(I)Lcom/google/o/f/j;

    move-result-object v4

    .line 74
    if-nez v4, :cond_1

    .line 75
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 77
    :cond_1
    iget v4, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/m;->a:I

    .line 78
    iput v3, p0, Lcom/google/maps/g/m;->b:I

    goto/16 :goto_0

    .line 83
    :sswitch_6
    iget-object v3, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 84
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/m;->a:I

    goto/16 :goto_0

    .line 88
    :sswitch_7
    iget-object v3, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 89
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/m;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_8
    iget-object v3, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/maps/g/m;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/m;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/m;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    .line 2930
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    .line 2946
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    .line 2962
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    .line 2978
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    .line 2994
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    .line 3009
    iput-byte v1, p0, Lcom/google/maps/g/m;->k:B

    .line 3073
    iput v1, p0, Lcom/google/maps/g/m;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/m;
    .locals 1

    .prologue
    .line 3772
    sget-object v0, Lcom/google/maps/g/m;->j:Lcom/google/maps/g/m;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ag;
    .locals 1

    .prologue
    .line 3180
    new-instance v0, Lcom/google/maps/g/ag;

    invoke-direct {v0}, Lcom/google/maps/g/ag;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/maps/g/m;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 3045
    invoke-virtual {p0}, Lcom/google/maps/g/m;->c()I

    .line 3046
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 3047
    iget v0, p0, Lcom/google/maps/g/m;->c:I

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 3049
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 3050
    iget-object v0, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3052
    :cond_1
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 3053
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3055
    :cond_2
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 3056
    iget-object v0, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3058
    :cond_3
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 3059
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/m;->b:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 3061
    :cond_4
    :goto_1
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3062
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3064
    :cond_5
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 3065
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3067
    :cond_6
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 3068
    iget-object v0, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3070
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/m;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3071
    return-void

    .line 3047
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 3059
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3011
    iget-byte v0, p0, Lcom/google/maps/g/m;->k:B

    .line 3012
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 3040
    :goto_0
    return v0

    .line 3013
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 3015
    :cond_1
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 3016
    iget-object v0, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/vb;->d()Lcom/google/maps/g/vb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vb;

    invoke-virtual {v0}, Lcom/google/maps/g/vb;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3017
    iput-byte v2, p0, Lcom/google/maps/g/m;->k:B

    move v0, v2

    .line 3018
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3015
    goto :goto_1

    .line 3021
    :cond_3
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 3022
    iget-object v0, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/vb;->d()Lcom/google/maps/g/vb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vb;

    invoke-virtual {v0}, Lcom/google/maps/g/vb;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3023
    iput-byte v2, p0, Lcom/google/maps/g/m;->k:B

    move v0, v2

    .line 3024
    goto :goto_0

    :cond_4
    move v0, v2

    .line 3021
    goto :goto_2

    .line 3027
    :cond_5
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 3028
    iget-object v0, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/dc;->d()Lcom/google/maps/g/dc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dc;

    invoke-virtual {v0}, Lcom/google/maps/g/dc;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3029
    iput-byte v2, p0, Lcom/google/maps/g/m;->k:B

    move v0, v2

    .line 3030
    goto :goto_0

    :cond_6
    move v0, v2

    .line 3027
    goto :goto_3

    .line 3033
    :cond_7
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 3034
    iget-object v0, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ms;->d()Lcom/google/maps/g/ms;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ms;

    invoke-virtual {v0}, Lcom/google/maps/g/ms;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 3035
    iput-byte v2, p0, Lcom/google/maps/g/m;->k:B

    move v0, v2

    .line 3036
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 3033
    goto :goto_4

    .line 3039
    :cond_9
    iput-byte v1, p0, Lcom/google/maps/g/m;->k:B

    move v0, v1

    .line 3040
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3075
    iget v0, p0, Lcom/google/maps/g/m;->l:I

    .line 3076
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3113
    :goto_0
    return v0

    .line 3079
    :cond_0
    iget v0, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_a

    .line 3080
    iget v0, p0, Lcom/google/maps/g/m;->c:I

    .line 3081
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3083
    :goto_2
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_1

    .line 3084
    iget-object v2, p0, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    .line 3085
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 3087
    :cond_1
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_2

    .line 3088
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    .line 3089
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 3091
    :cond_2
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 3092
    iget-object v2, p0, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    .line 3093
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 3095
    :cond_3
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_4

    .line 3096
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/maps/g/m;->b:I

    .line 3097
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 3099
    :cond_4
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 3100
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    .line 3101
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 3103
    :cond_5
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 3104
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    .line 3105
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 3107
    :cond_6
    iget v2, p0, Lcom/google/maps/g/m;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 3108
    iget-object v2, p0, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    .line 3109
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3111
    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/m;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3112
    iput v0, p0, Lcom/google/maps/g/m;->l:I

    goto/16 :goto_0

    .line 3081
    :cond_8
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 3097
    :cond_9
    const/16 v2, 0xa

    goto :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/m;->newBuilder()Lcom/google/maps/g/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ag;->a(Lcom/google/maps/g/m;)Lcom/google/maps/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/m;->newBuilder()Lcom/google/maps/g/ag;

    move-result-object v0

    return-object v0
.end method
