.class public final enum Lcom/google/maps/g/ot;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ot;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/maps/g/ot;

.field public static final enum B:Lcom/google/maps/g/ot;

.field public static final enum C:Lcom/google/maps/g/ot;

.field public static final enum D:Lcom/google/maps/g/ot;

.field public static final enum E:Lcom/google/maps/g/ot;

.field private static final synthetic G:[Lcom/google/maps/g/ot;

.field public static final enum a:Lcom/google/maps/g/ot;

.field public static final enum b:Lcom/google/maps/g/ot;

.field public static final enum c:Lcom/google/maps/g/ot;

.field public static final enum d:Lcom/google/maps/g/ot;

.field public static final enum e:Lcom/google/maps/g/ot;

.field public static final enum f:Lcom/google/maps/g/ot;

.field public static final enum g:Lcom/google/maps/g/ot;

.field public static final enum h:Lcom/google/maps/g/ot;

.field public static final enum i:Lcom/google/maps/g/ot;

.field public static final enum j:Lcom/google/maps/g/ot;

.field public static final enum k:Lcom/google/maps/g/ot;

.field public static final enum l:Lcom/google/maps/g/ot;

.field public static final enum m:Lcom/google/maps/g/ot;

.field public static final enum n:Lcom/google/maps/g/ot;

.field public static final enum o:Lcom/google/maps/g/ot;

.field public static final enum p:Lcom/google/maps/g/ot;

.field public static final enum q:Lcom/google/maps/g/ot;

.field public static final enum r:Lcom/google/maps/g/ot;

.field public static final enum s:Lcom/google/maps/g/ot;

.field public static final enum t:Lcom/google/maps/g/ot;

.field public static final enum u:Lcom/google/maps/g/ot;

.field public static final enum v:Lcom/google/maps/g/ot;

.field public static final enum w:Lcom/google/maps/g/ot;

.field public static final enum x:Lcom/google/maps/g/ot;

.field public static final enum y:Lcom/google/maps/g/ot;

.field public static final enum z:Lcom/google/maps/g/ot;


# instance fields
.field public final F:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->a:Lcom/google/maps/g/ot;

    .line 80
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->b:Lcom/google/maps/g/ot;

    .line 84
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->c:Lcom/google/maps/g/ot;

    .line 88
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "CATEGORY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->d:Lcom/google/maps/g/ot;

    .line 92
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "WEBSITE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->e:Lcom/google/maps/g/ot;

    .line 96
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "CLOSED"

    const/4 v2, 0x5

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->f:Lcom/google/maps/g/ot;

    .line 100
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "DOES_NOT_EXIST"

    const/4 v2, 0x6

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->g:Lcom/google/maps/g/ot;

    .line 104
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "PRIVATE"

    const/4 v2, 0x7

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->h:Lcom/google/maps/g/ot;

    .line 108
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "SPAM"

    const/16 v2, 0x8

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->i:Lcom/google/maps/g/ot;

    .line 112
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "MOVED"

    const/16 v2, 0x9

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->j:Lcom/google/maps/g/ot;

    .line 116
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "DUPLICATE"

    const/16 v2, 0xa

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->k:Lcom/google/maps/g/ot;

    .line 120
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "PHONE_NUMBER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v7}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->l:Lcom/google/maps/g/ot;

    .line 124
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "BUSINESS_HOURS"

    const/16 v2, 0xc

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->m:Lcom/google/maps/g/ot;

    .line 128
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ACCESS_POINT"

    const/16 v2, 0xd

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->n:Lcom/google/maps/g/ot;

    .line 132
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "POINT"

    const/16 v2, 0xe

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->o:Lcom/google/maps/g/ot;

    .line 136
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "POLYLINE"

    const/16 v2, 0xf

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->p:Lcom/google/maps/g/ot;

    .line 140
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "POLYGON"

    const/16 v2, 0x10

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->q:Lcom/google/maps/g/ot;

    .line 144
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ROAD_DIRECTIONALITY"

    const/16 v2, 0x11

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->r:Lcom/google/maps/g/ot;

    .line 148
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ROAD_NOT_PASSABLE"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->s:Lcom/google/maps/g/ot;

    .line 152
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ROAD_CONSTRUCTION"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->t:Lcom/google/maps/g/ot;

    .line 156
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "ROAD_RESTRICTION"

    const/16 v2, 0x14

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->u:Lcom/google/maps/g/ot;

    .line 160
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "TURN_RESTRICTION"

    const/16 v2, 0x15

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->v:Lcom/google/maps/g/ot;

    .line 164
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "TRANSIT_SCHEDULE"

    const/16 v2, 0x16

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->w:Lcom/google/maps/g/ot;

    .line 168
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "DIRECTION_SUB_OPTIMAL_PATH"

    const/16 v2, 0x17

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->x:Lcom/google/maps/g/ot;

    .line 172
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "SIGNAGE_REFERENCE"

    const/16 v2, 0x18

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->y:Lcom/google/maps/g/ot;

    .line 176
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "LANDMARK_REFERENCE"

    const/16 v2, 0x19

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->z:Lcom/google/maps/g/ot;

    .line 180
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "TEXT_TO_SPEECH_APPROACHING"

    const/16 v2, 0x1a

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->A:Lcom/google/maps/g/ot;

    .line 184
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "TEXT_TO_SPEECH_GUIDANCE"

    const/16 v2, 0x1b

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->B:Lcom/google/maps/g/ot;

    .line 188
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "LANE_GUIDANCE_INSTRUCTION"

    const/16 v2, 0x1c

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->C:Lcom/google/maps/g/ot;

    .line 192
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "LANE_CONFIGURATION"

    const/16 v2, 0x1d

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->D:Lcom/google/maps/g/ot;

    .line 196
    new-instance v0, Lcom/google/maps/g/ot;

    const-string v1, "OTHER"

    const/16 v2, 0x1e

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ot;->E:Lcom/google/maps/g/ot;

    .line 71
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/google/maps/g/ot;

    sget-object v1, Lcom/google/maps/g/ot;->a:Lcom/google/maps/g/ot;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/ot;->b:Lcom/google/maps/g/ot;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/ot;->c:Lcom/google/maps/g/ot;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/ot;->d:Lcom/google/maps/g/ot;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/ot;->e:Lcom/google/maps/g/ot;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/ot;->f:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/ot;->g:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/ot;->h:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/ot;->i:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/ot;->j:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/ot;->k:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/ot;->l:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/ot;->m:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/ot;->n:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/ot;->o:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/ot;->p:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/g/ot;->q:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/g/ot;->r:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/g/ot;->s:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/g/ot;->t:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/g/ot;->u:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/g/ot;->v:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/g/ot;->w:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/g/ot;->x:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/g/ot;->y:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/g/ot;->z:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/g/ot;->A:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/g/ot;->B:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/g/ot;->C:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/g/ot;->D:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/g/ot;->E:Lcom/google/maps/g/ot;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/ot;->G:[Lcom/google/maps/g/ot;

    .line 371
    new-instance v0, Lcom/google/maps/g/ou;

    invoke-direct {v0}, Lcom/google/maps/g/ou;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 381
    iput p3, p0, Lcom/google/maps/g/ot;->F:I

    .line 382
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ot;
    .locals 1

    .prologue
    .line 330
    packed-switch p0, :pswitch_data_0

    .line 362
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 331
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/ot;->a:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 332
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/ot;->b:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 333
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/ot;->c:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 334
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/ot;->d:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 335
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/ot;->e:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 336
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/ot;->f:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 337
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/ot;->g:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 338
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/ot;->h:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 339
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/ot;->i:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 340
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/ot;->j:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 341
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/ot;->k:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 342
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/ot;->l:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 343
    :pswitch_d
    sget-object v0, Lcom/google/maps/g/ot;->m:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 344
    :pswitch_e
    sget-object v0, Lcom/google/maps/g/ot;->n:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 345
    :pswitch_f
    sget-object v0, Lcom/google/maps/g/ot;->o:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 346
    :pswitch_10
    sget-object v0, Lcom/google/maps/g/ot;->p:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 347
    :pswitch_11
    sget-object v0, Lcom/google/maps/g/ot;->q:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 348
    :pswitch_12
    sget-object v0, Lcom/google/maps/g/ot;->r:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 349
    :pswitch_13
    sget-object v0, Lcom/google/maps/g/ot;->s:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 350
    :pswitch_14
    sget-object v0, Lcom/google/maps/g/ot;->t:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 351
    :pswitch_15
    sget-object v0, Lcom/google/maps/g/ot;->u:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 352
    :pswitch_16
    sget-object v0, Lcom/google/maps/g/ot;->v:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 353
    :pswitch_17
    sget-object v0, Lcom/google/maps/g/ot;->w:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 354
    :pswitch_18
    sget-object v0, Lcom/google/maps/g/ot;->x:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 355
    :pswitch_19
    sget-object v0, Lcom/google/maps/g/ot;->y:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 356
    :pswitch_1a
    sget-object v0, Lcom/google/maps/g/ot;->z:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 357
    :pswitch_1b
    sget-object v0, Lcom/google/maps/g/ot;->A:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 358
    :pswitch_1c
    sget-object v0, Lcom/google/maps/g/ot;->B:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 359
    :pswitch_1d
    sget-object v0, Lcom/google/maps/g/ot;->C:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 360
    :pswitch_1e
    sget-object v0, Lcom/google/maps/g/ot;->D:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 361
    :pswitch_1f
    sget-object v0, Lcom/google/maps/g/ot;->E:Lcom/google/maps/g/ot;

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_c
        :pswitch_5
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_6
        :pswitch_7
        :pswitch_12
        :pswitch_17
        :pswitch_1f
        :pswitch_4
        :pswitch_18
        :pswitch_15
        :pswitch_16
        :pswitch_8
        :pswitch_13
        :pswitch_14
        :pswitch_19
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_e
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ot;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/google/maps/g/ot;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ot;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ot;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/maps/g/ot;->G:[Lcom/google/maps/g/ot;

    invoke-virtual {v0}, [Lcom/google/maps/g/ot;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ot;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/google/maps/g/ot;->F:I

    return v0
.end method
