.class public final enum Lcom/google/maps/g/a/af;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/af;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/af;

.field public static final enum b:Lcom/google/maps/g/a/af;

.field public static final enum c:Lcom/google/maps/g/a/af;

.field public static final enum d:Lcom/google/maps/g/a/af;

.field public static final enum e:Lcom/google/maps/g/a/af;

.field public static final enum f:Lcom/google/maps/g/a/af;

.field public static final enum g:Lcom/google/maps/g/a/af;

.field public static final enum h:Lcom/google/maps/g/a/af;

.field public static final enum i:Lcom/google/maps/g/a/af;

.field public static final enum j:Lcom/google/maps/g/a/af;

.field private static final synthetic l:[Lcom/google/maps/g/a/af;


# instance fields
.field public final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 138
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_TO_ROAD_NAME"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    .line 142
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_TOWARD_NAME"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->b:Lcom/google/maps/g/a/af;

    .line 146
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_TOWARD_ROAD_NAME"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    .line 150
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_EXIT_NUMBER"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    .line 154
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_FOLLOW_ROAD_NAME"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->e:Lcom/google/maps/g/a/af;

    .line 158
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_FROM_ROAD_NAME"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->f:Lcom/google/maps/g/a/af;

    .line 162
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_TITLE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->g:Lcom/google/maps/g/a/af;

    .line 166
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_ADDRESS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->h:Lcom/google/maps/g/a/af;

    .line 170
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_AT_ROAD_NAME"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->i:Lcom/google/maps/g/a/af;

    .line 174
    new-instance v0, Lcom/google/maps/g/a/af;

    const-string v1, "TYPE_INTERSECTION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/af;->j:Lcom/google/maps/g/a/af;

    .line 133
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/maps/g/a/af;

    sget-object v1, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/af;->b:Lcom/google/maps/g/a/af;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/af;->e:Lcom/google/maps/g/a/af;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/af;->f:Lcom/google/maps/g/a/af;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/af;->g:Lcom/google/maps/g/a/af;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/af;->h:Lcom/google/maps/g/a/af;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/af;->i:Lcom/google/maps/g/a/af;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/a/af;->j:Lcom/google/maps/g/a/af;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/af;->l:[Lcom/google/maps/g/a/af;

    .line 244
    new-instance v0, Lcom/google/maps/g/a/ag;

    invoke-direct {v0}, Lcom/google/maps/g/a/ag;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 253
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 254
    iput p3, p0, Lcom/google/maps/g/a/af;->k:I

    .line 255
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/af;
    .locals 1

    .prologue
    .line 224
    packed-switch p0, :pswitch_data_0

    .line 235
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 225
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 226
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/af;->b:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 227
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 228
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 229
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/af;->e:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 230
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/af;->f:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 231
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/af;->g:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 232
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/af;->h:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 233
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/af;->i:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 234
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/a/af;->j:Lcom/google/maps/g/a/af;

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/af;
    .locals 1

    .prologue
    .line 133
    const-class v0, Lcom/google/maps/g/a/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/af;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/af;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/google/maps/g/a/af;->l:[Lcom/google/maps/g/a/af;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/af;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/google/maps/g/a/af;->k:I

    return v0
.end method
