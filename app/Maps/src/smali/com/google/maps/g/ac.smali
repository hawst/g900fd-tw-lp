.class public final Lcom/google/maps/g/ac;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ad;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/q;",
        "Lcom/google/maps/g/ac;",
        ">;",
        "Lcom/google/maps/g/ad;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/maps/g/ky;

.field private g:Lcom/google/n/f;

.field private h:Lcom/google/maps/g/u;

.field private i:Ljava/lang/Object;

.field private j:Lcom/google/maps/g/i;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1754
    sget-object v0, Lcom/google/maps/g/q;->k:Lcom/google/maps/g/q;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1874
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ac;->b:Ljava/lang/Object;

    .line 1950
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ac;->c:Ljava/lang/Object;

    .line 2026
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ac;->d:I

    .line 2062
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ac;->e:Ljava/lang/Object;

    .line 2138
    iput-object v1, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    .line 2199
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/ac;->g:Lcom/google/n/f;

    .line 2234
    iput-object v1, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    .line 2295
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ac;->i:Ljava/lang/Object;

    .line 2371
    iput-object v1, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    .line 1755
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/q;)Lcom/google/maps/g/ac;
    .locals 7

    .prologue
    const/16 v6, 0x100

    const/16 v5, 0x40

    const/16 v4, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1828
    invoke-static {}, Lcom/google/maps/g/q;->h()Lcom/google/maps/g/q;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1865
    :goto_0
    return-object p0

    .line 1829
    :cond_0
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1830
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1831
    iget-object v0, p1, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ac;->b:Ljava/lang/Object;

    .line 1834
    :cond_1
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 1835
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1836
    iget-object v0, p1, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ac;->c:Ljava/lang/Object;

    .line 1839
    :cond_2
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    .line 1840
    iget v0, p1, Lcom/google/maps/g/q;->d:I

    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    .line 1829
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1834
    goto :goto_2

    :cond_6
    move v0, v2

    .line 1839
    goto :goto_3

    .line 1840
    :cond_7
    iget v3, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/ac;->a:I

    iget v0, v0, Lcom/google/maps/g/s;->b:I

    iput v0, p0, Lcom/google/maps/g/ac;->d:I

    .line 1842
    :cond_8
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 1843
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1844
    iget-object v0, p1, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ac;->e:Ljava/lang/Object;

    .line 1847
    :cond_9
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v4, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 1848
    iget-object v0, p1, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v0

    :goto_6
    iget v3, p0, Lcom/google/maps/g/ac;->a:I

    and-int/lit8 v3, v3, 0x10

    if-ne v3, v4, :cond_e

    iget-object v3, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v4

    if-eq v3, v4, :cond_e

    iget-object v3, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    invoke-static {v3}, Lcom/google/maps/g/ky;->a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/la;->a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/la;->c()Lcom/google/maps/g/ky;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    :goto_7
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1850
    :cond_a
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_8
    if-eqz v0, :cond_11

    .line 1851
    iget-object v0, p1, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v0, v2

    .line 1842
    goto :goto_4

    :cond_c
    move v0, v2

    .line 1847
    goto :goto_5

    .line 1848
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    goto :goto_6

    :cond_e
    iput-object v0, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    goto :goto_7

    :cond_f
    move v0, v2

    .line 1850
    goto :goto_8

    .line 1851
    :cond_10
    iget v3, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/ac;->a:I

    iput-object v0, p0, Lcom/google/maps/g/ac;->g:Lcom/google/n/f;

    .line 1853
    :cond_11
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v5, :cond_15

    move v0, v1

    :goto_9
    if-eqz v0, :cond_12

    .line 1854
    iget-object v0, p1, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v0

    :goto_a
    iget v3, p0, Lcom/google/maps/g/ac;->a:I

    and-int/lit8 v3, v3, 0x40

    if-ne v3, v5, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v4

    if-eq v3, v4, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    invoke-static {v3}, Lcom/google/maps/g/u;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/w;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/w;->c()Lcom/google/maps/g/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    :goto_b
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1856
    :cond_12
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_c
    if-eqz v0, :cond_13

    .line 1857
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1858
    iget-object v0, p1, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ac;->i:Ljava/lang/Object;

    .line 1861
    :cond_13
    iget v0, p1, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x100

    if-ne v0, v6, :cond_19

    move v0, v1

    :goto_d
    if-eqz v0, :cond_14

    .line 1862
    iget-object v0, p1, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    if-nez v0, :cond_1a

    invoke-static {}, Lcom/google/maps/g/i;->d()Lcom/google/maps/g/i;

    move-result-object v0

    :goto_e
    iget v1, p0, Lcom/google/maps/g/ac;->a:I

    and-int/lit16 v1, v1, 0x100

    if-ne v1, v6, :cond_1b

    iget-object v1, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    invoke-static {}, Lcom/google/maps/g/i;->d()Lcom/google/maps/g/i;

    move-result-object v2

    if-eq v1, v2, :cond_1b

    iget-object v1, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    invoke-static {v1}, Lcom/google/maps/g/i;->a(Lcom/google/maps/g/i;)Lcom/google/maps/g/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/k;->a(Lcom/google/maps/g/i;)Lcom/google/maps/g/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/k;->c()Lcom/google/maps/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    :goto_f
    iget v0, p0, Lcom/google/maps/g/ac;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/ac;->a:I

    .line 1864
    :cond_14
    iget-object v0, p1, Lcom/google/maps/g/q;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_15
    move v0, v2

    .line 1853
    goto/16 :goto_9

    .line 1854
    :cond_16
    iget-object v0, p1, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    goto/16 :goto_a

    :cond_17
    iput-object v0, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    goto :goto_b

    :cond_18
    move v0, v2

    .line 1856
    goto :goto_c

    :cond_19
    move v0, v2

    .line 1861
    goto :goto_d

    .line 1862
    :cond_1a
    iget-object v0, p1, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    goto :goto_e

    :cond_1b
    iput-object v0, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    goto :goto_f
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1746
    new-instance v2, Lcom/google/maps/g/q;

    invoke-direct {v2, p0}, Lcom/google/maps/g/q;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ac;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/ac;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/ac;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/ac;->d:I

    iput v1, v2, Lcom/google/maps/g/q;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/ac;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/ac;->f:Lcom/google/maps/g/ky;

    iput-object v1, v2, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/ac;->g:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/ac;->h:Lcom/google/maps/g/u;

    iput-object v1, v2, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/ac;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/ac;->j:Lcom/google/maps/g/i;

    iput-object v1, v2, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    iput v0, v2, Lcom/google/maps/g/q;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1746
    check-cast p1, Lcom/google/maps/g/q;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ac;->a(Lcom/google/maps/g/q;)Lcom/google/maps/g/ac;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1869
    const/4 v0, 0x1

    return v0
.end method
