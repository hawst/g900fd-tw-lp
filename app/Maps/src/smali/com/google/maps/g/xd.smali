.class public final Lcom/google/maps/g/xd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/xd;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/xd;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/n/ao;

.field f:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/google/maps/g/xe;

    invoke-direct {v0}, Lcom/google/maps/g/xe;-><init>()V

    sput-object v0, Lcom/google/maps/g/xd;->PARSER:Lcom/google/n/ax;

    .line 166
    new-instance v0, Lcom/google/maps/g/xf;

    invoke-direct {v0}, Lcom/google/maps/g/xf;-><init>()V

    .line 300
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/xd;->j:Lcom/google/n/aw;

    .line 766
    new-instance v0, Lcom/google/maps/g/xd;

    invoke-direct {v0}, Lcom/google/maps/g/xd;-><init>()V

    sput-object v0, Lcom/google/maps/g/xd;->g:Lcom/google/maps/g/xd;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 132
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    .line 148
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    .line 225
    iput-byte v3, p0, Lcom/google/maps/g/xd;->h:B

    .line 262
    iput v3, p0, Lcom/google/maps/g/xd;->i:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/xd;->f:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v10, 0x4

    const/4 v3, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/xd;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 35
    :cond_0
    :goto_0
    if-nez v4, :cond_9

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 50
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/xd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v10, :cond_1

    .line 110
    iget-object v1, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 112
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/xd;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/xd;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 106
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 107
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 60
    invoke-static {v0}, Lcom/google/maps/b/ad;->a(I)Lcom/google/maps/b/ad;

    move-result-object v6

    .line 61
    if-nez v6, :cond_2

    .line 62
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 64
    :cond_2
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v10, :cond_3

    .line 65
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 66
    or-int/lit8 v1, v1, 0x4

    .line 68
    :cond_3
    iget-object v6, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 73
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 74
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 75
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_4

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_7

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 77
    invoke-static {v0}, Lcom/google/maps/b/ad;->a(I)Lcom/google/maps/b/ad;

    move-result-object v7

    .line 78
    if-nez v7, :cond_5

    .line 79
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 75
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 81
    :cond_5
    and-int/lit8 v7, v1, 0x4

    if-eq v7, v10, :cond_6

    .line 82
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 83
    or-int/lit8 v1, v1, 0x4

    .line 85
    :cond_6
    iget-object v7, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 88
    :cond_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 92
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 93
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/xd;->a:I

    goto/16 :goto_0

    .line 97
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/xd;->a:I

    .line 98
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/xd;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_3

    .line 109
    :cond_9
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v10, :cond_a

    .line 110
    iget-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 112
    :cond_a
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xd;->au:Lcom/google/n/bn;

    .line 113
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 132
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    .line 148
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    .line 225
    iput-byte v1, p0, Lcom/google/maps/g/xd;->h:B

    .line 262
    iput v1, p0, Lcom/google/maps/g/xd;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/xd;
    .locals 1

    .prologue
    .line 769
    sget-object v0, Lcom/google/maps/g/xd;->g:Lcom/google/maps/g/xd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/xg;
    .locals 1

    .prologue
    .line 362
    new-instance v0, Lcom/google/maps/g/xg;

    invoke-direct {v0}, Lcom/google/maps/g/xg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/xd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lcom/google/maps/g/xd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/google/maps/g/xd;->c()I

    .line 244
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 247
    :cond_0
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 248
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 250
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 251
    :cond_2
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 253
    :cond_3
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 254
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 256
    :cond_4
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 257
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/maps/g/xd;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_5

    move v2, v3

    :cond_5
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 259
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/xd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 260
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 227
    iget-byte v0, p0, Lcom/google/maps/g/xd;->h:B

    .line 228
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 238
    :goto_0
    return v0

    .line 229
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 231
    :cond_1
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 232
    iget-object v0, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 233
    iput-byte v2, p0, Lcom/google/maps/g/xd;->h:B

    move v0, v2

    .line 234
    goto :goto_0

    :cond_2
    move v0, v2

    .line 231
    goto :goto_1

    .line 237
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/g/xd;->h:B

    move v0, v1

    .line 238
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 264
    iget v0, p0, Lcom/google/maps/g/xd;->i:I

    .line 265
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 295
    :goto_0
    return v0

    .line 268
    :cond_0
    iget v0, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 269
    iget-object v0, p0, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    .line 270
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 272
    :goto_1
    iget v2, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 273
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    .line 274
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    move v4, v1

    .line 278
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    .line 280
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v4, v0

    .line 278
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 280
    :cond_1
    const/16 v0, 0xa

    goto :goto_4

    .line 282
    :cond_2
    add-int v0, v2, v4

    .line 283
    iget-object v2, p0, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 285
    iget v2, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 286
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    .line 287
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 289
    :cond_3
    iget v2, p0, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 290
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/xd;->f:Z

    .line 291
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 293
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/xd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    iput v0, p0, Lcom/google/maps/g/xd;->i:I

    goto/16 :goto_0

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/xd;->newBuilder()Lcom/google/maps/g/xg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/xg;->a(Lcom/google/maps/g/xd;)Lcom/google/maps/g/xg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/xd;->newBuilder()Lcom/google/maps/g/xg;

    move-result-object v0

    return-object v0
.end method
