.class public final Lcom/google/maps/g/vp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vp;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/vp;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/aq;

.field d:Ljava/lang/Object;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field g:Z

.field h:Z

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/google/maps/g/vq;

    invoke-direct {v0}, Lcom/google/maps/g/vq;-><init>()V

    sput-object v0, Lcom/google/maps/g/vp;->PARSER:Lcom/google/n/ax;

    .line 652
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/vp;->n:Lcom/google/n/aw;

    .line 1598
    new-instance v0, Lcom/google/maps/g/vp;

    invoke-direct {v0}, Lcom/google/maps/g/vp;-><init>()V

    sput-object v0, Lcom/google/maps/g/vp;->k:Lcom/google/maps/g/vp;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 555
    iput-byte v0, p0, Lcom/google/maps/g/vp;->l:B

    .line 598
    iput v0, p0, Lcom/google/maps/g/vp;->m:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    .line 19
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    .line 22
    iput v1, p0, Lcom/google/maps/g/vp;->f:I

    .line 23
    iput-boolean v1, p0, Lcom/google/maps/g/vp;->g:Z

    .line 24
    iput-boolean v1, p0, Lcom/google/maps/g/vp;->h:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    .line 26
    iput v1, p0, Lcom/google/maps/g/vp;->j:I

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/maps/g/vp;-><init>()V

    .line 34
    const/4 v1, 0x0

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 38
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 39
    :cond_0
    :goto_0
    if-nez v2, :cond_a

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 41
    sparse-switch v1, :sswitch_data_0

    .line 46
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 43
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 54
    and-int/lit8 v1, v0, 0x2

    const/4 v5, 0x2

    if-eq v1, v5, :cond_11

    .line 55
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 56
    or-int/lit8 v1, v0, 0x2

    .line 58
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 59
    goto :goto_0

    .line 62
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 63
    iget v4, p0, Lcom/google/maps/g/vp;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/vp;->a:I

    .line 64
    iput-object v1, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 132
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 138
    iget-object v2, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    .line 140
    :cond_1
    and-int/lit8 v2, v1, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    .line 141
    iget-object v2, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    .line 143
    :cond_2
    and-int/lit16 v2, v1, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_3

    .line 144
    iget-object v2, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    .line 146
    :cond_3
    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 147
    iget-object v1, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    .line 149
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vp;->au:Lcom/google/n/bn;

    throw v0

    .line 68
    :sswitch_3
    and-int/lit8 v1, v0, 0x8

    const/16 v4, 0x8

    if-eq v1, v4, :cond_10

    .line 69
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 71
    or-int/lit8 v1, v0, 0x8

    .line 73
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 73
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 75
    goto/16 :goto_0

    .line 78
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 79
    invoke-static {v1}, Lcom/google/maps/g/vu;->a(I)Lcom/google/maps/g/vu;

    move-result-object v4

    .line 80
    if-nez v4, :cond_5

    .line 81
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 133
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 134
    :goto_5
    :try_start_7
    new-instance v2, Lcom/google/n/ak;

    .line 135
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 83
    :cond_5
    :try_start_8
    iget v4, p0, Lcom/google/maps/g/vp;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/vp;->a:I

    .line 84
    iput v1, p0, Lcom/google/maps/g/vp;->f:I

    goto/16 :goto_0

    .line 137
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_3

    .line 89
    :sswitch_5
    iget v1, p0, Lcom/google/maps/g/vp;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/vp;->a:I

    .line 90
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_6
    iput-boolean v1, p0, Lcom/google/maps/g/vp;->g:Z

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_6

    .line 94
    :sswitch_6
    iget v1, p0, Lcom/google/maps/g/vp;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/vp;->a:I

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_7
    iput-boolean v1, p0, Lcom/google/maps/g/vp;->h:Z

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_7

    .line 99
    :sswitch_7
    and-int/lit16 v1, v0, 0x80

    const/16 v4, 0x80

    if-eq v1, v4, :cond_f

    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 102
    or-int/lit16 v1, v0, 0x80

    .line 104
    :goto_8
    :try_start_9
    iget-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 105
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 104
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 106
    goto/16 :goto_0

    .line 109
    :sswitch_8
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 110
    invoke-static {v1}, Lcom/google/maps/g/vs;->a(I)Lcom/google/maps/g/vs;

    move-result-object v4

    .line 111
    if-nez v4, :cond_8

    .line 112
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 114
    :cond_8
    iget v4, p0, Lcom/google/maps/g/vp;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/g/vp;->a:I

    .line 115
    iput v1, p0, Lcom/google/maps/g/vp;->j:I

    goto/16 :goto_0

    .line 120
    :sswitch_9
    and-int/lit8 v1, v0, 0x1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_9

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    .line 123
    or-int/lit8 v0, v0, 0x1

    .line 125
    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 126
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 125
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 137
    :cond_a
    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    .line 138
    iget-object v1, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    .line 140
    :cond_b
    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_c

    .line 141
    iget-object v1, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    .line 143
    :cond_c
    and-int/lit16 v1, v0, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_d

    .line 144
    iget-object v1, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    .line 146
    :cond_d
    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    .line 147
    iget-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    .line 149
    :cond_e
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->au:Lcom/google/n/bn;

    .line 150
    return-void

    .line 133
    :catch_2
    move-exception v0

    goto/16 :goto_5

    .line 131
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_f
    move v1, v0

    goto/16 :goto_8

    :cond_10
    move v1, v0

    goto/16 :goto_4

    :cond_11
    move v1, v0

    goto/16 :goto_1

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 555
    iput-byte v0, p0, Lcom/google/maps/g/vp;->l:B

    .line 598
    iput v0, p0, Lcom/google/maps/g/vp;->m:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/vp;
    .locals 1

    .prologue
    .line 1601
    sget-object v0, Lcom/google/maps/g/vp;->k:Lcom/google/maps/g/vp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/vr;
    .locals 1

    .prologue
    .line 714
    new-instance v0, Lcom/google/maps/g/vr;

    invoke-direct {v0}, Lcom/google/maps/g/vr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    sget-object v0, Lcom/google/maps/g/vp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 567
    invoke-virtual {p0}, Lcom/google/maps/g/vp;->c()I

    move v0, v1

    .line 568
    :goto_0
    iget-object v2, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 569
    iget-object v2, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 568
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 571
    :cond_0
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 572
    iget-object v0, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v2, v1

    .line 574
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 575
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 574
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 572
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 577
    :cond_3
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 578
    iget v0, p0, Lcom/google/maps/g/vp;->f:I

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 580
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_5

    .line 581
    const/4 v0, 0x5

    iget-boolean v2, p0, Lcom/google/maps/g/vp;->g:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_8

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 583
    :cond_5
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_6

    .line 584
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/maps/g/vp;->h:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_9

    :goto_5
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    :cond_6
    move v2, v1

    .line 586
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 587
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 586
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 578
    :cond_7
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    :cond_8
    move v0, v1

    .line 581
    goto :goto_4

    :cond_9
    move v3, v1

    .line 584
    goto :goto_5

    .line 589
    :cond_a
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_b

    .line 590
    iget v0, p0, Lcom/google/maps/g/vp;->j:I

    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 592
    :cond_b
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 593
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 592
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 590
    :cond_c
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 595
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/vp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 596
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 557
    iget-byte v1, p0, Lcom/google/maps/g/vp;->l:B

    .line 558
    if-ne v1, v0, :cond_0

    .line 562
    :goto_0
    return v0

    .line 559
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 561
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/vp;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v4, 0xa

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 600
    iget v0, p0, Lcom/google/maps/g/vp;->m:I

    .line 601
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 647
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 606
    :goto_1
    iget-object v3, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 607
    iget-object v3, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    .line 608
    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v5

    add-int/2addr v2, v3

    .line 606
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 610
    :cond_1
    add-int/lit8 v0, v2, 0x0

    .line 611
    iget-object v2, p0, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 613
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_c

    .line 615
    iget-object v0, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    :goto_3
    move v2, v1

    move v3, v0

    .line 617
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 618
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    .line 619
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 617
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 615
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 621
    :cond_3
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 622
    iget v0, p0, Lcom/google/maps/g/vp;->f:I

    .line 623
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 625
    :cond_4
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_5

    .line 626
    const/4 v0, 0x5

    iget-boolean v2, p0, Lcom/google/maps/g/vp;->g:Z

    .line 627
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 629
    :cond_5
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_6

    .line 630
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/maps/g/vp;->h:Z

    .line 631
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_6
    move v2, v1

    .line 633
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 634
    const/4 v5, 0x7

    iget-object v0, p0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    .line 635
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 633
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_7
    move v0, v4

    .line 623
    goto :goto_5

    .line 637
    :cond_8
    iget v0, p0, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_a

    .line 638
    iget v0, p0, Lcom/google/maps/g/vp;->j:I

    .line 639
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_9
    add-int v0, v2, v4

    add-int/2addr v3, v0

    :cond_a
    move v2, v1

    .line 641
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 642
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    .line 643
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 641
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 645
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/vp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 646
    iput v0, p0, Lcom/google/maps/g/vp;->m:I

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto/16 :goto_3
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/vx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    .line 415
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 416
    iget-object v0, p0, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 417
    invoke-static {}, Lcom/google/maps/g/vx;->i()Lcom/google/maps/g/vx;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vx;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 419
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vp;->newBuilder()Lcom/google/maps/g/vr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/vr;->a(Lcom/google/maps/g/vp;)Lcom/google/maps/g/vr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vp;->newBuilder()Lcom/google/maps/g/vr;

    move-result-object v0

    return-object v0
.end method
