.class public final Lcom/google/maps/g/tl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/to;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/tj;",
        "Lcom/google/maps/g/tl;",
        ">;",
        "Lcom/google/maps/g/to;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:F

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/google/maps/g/tj;->g:Lcom/google/maps/g/tj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 482
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/tl;->b:Lcom/google/n/ao;

    .line 541
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/maps/g/tl;->c:F

    .line 605
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/tl;->e:I

    .line 641
    const/16 v0, 0x58

    iput v0, p0, Lcom/google/maps/g/tl;->f:I

    .line 404
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/tj;)Lcom/google/maps/g/tl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 455
    invoke-static {}, Lcom/google/maps/g/tj;->d()Lcom/google/maps/g/tj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 473
    :goto_0
    return-object p0

    .line 456
    :cond_0
    iget v2, p1, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 457
    iget-object v2, p0, Lcom/google/maps/g/tl;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 458
    iget v2, p0, Lcom/google/maps/g/tl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/tl;->a:I

    .line 460
    :cond_1
    iget v2, p1, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 461
    iget v2, p1, Lcom/google/maps/g/tj;->c:F

    iget v3, p0, Lcom/google/maps/g/tl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/tl;->a:I

    iput v2, p0, Lcom/google/maps/g/tl;->c:F

    .line 463
    :cond_2
    iget v2, p1, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 464
    iget v2, p1, Lcom/google/maps/g/tj;->d:I

    iget v3, p0, Lcom/google/maps/g/tl;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/tl;->a:I

    iput v2, p0, Lcom/google/maps/g/tl;->d:I

    .line 466
    :cond_3
    iget v2, p1, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 467
    iget v2, p1, Lcom/google/maps/g/tj;->e:I

    invoke-static {v2}, Lcom/google/maps/g/tm;->a(I)Lcom/google/maps/g/tm;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/tm;->a:Lcom/google/maps/g/tm;

    :cond_4
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 456
    goto :goto_1

    :cond_6
    move v2, v1

    .line 460
    goto :goto_2

    :cond_7
    move v2, v1

    .line 463
    goto :goto_3

    :cond_8
    move v2, v1

    .line 466
    goto :goto_4

    .line 467
    :cond_9
    iget v3, p0, Lcom/google/maps/g/tl;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/tl;->a:I

    iget v2, v2, Lcom/google/maps/g/tm;->e:I

    iput v2, p0, Lcom/google/maps/g/tl;->e:I

    .line 469
    :cond_a
    iget v2, p1, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    :goto_5
    if-eqz v0, :cond_b

    .line 470
    iget v0, p1, Lcom/google/maps/g/tj;->f:I

    iget v1, p0, Lcom/google/maps/g/tl;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/tl;->a:I

    iput v0, p0, Lcom/google/maps/g/tl;->f:I

    .line 472
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/tj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 469
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 395
    new-instance v2, Lcom/google/maps/g/tj;

    invoke-direct {v2, p0}, Lcom/google/maps/g/tj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/tl;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/tl;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/tl;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/tl;->c:F

    iput v1, v2, Lcom/google/maps/g/tj;->c:F

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/tl;->d:I

    iput v1, v2, Lcom/google/maps/g/tj;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/tl;->e:I

    iput v1, v2, Lcom/google/maps/g/tj;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/maps/g/tl;->f:I

    iput v1, v2, Lcom/google/maps/g/tj;->f:I

    iput v0, v2, Lcom/google/maps/g/tj;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 395
    check-cast p1, Lcom/google/maps/g/tj;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/tl;->a(Lcom/google/maps/g/tj;)Lcom/google/maps/g/tl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x1

    return v0
.end method
