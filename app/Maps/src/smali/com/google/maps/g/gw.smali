.class public final Lcom/google/maps/g/gw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/gu;",
        "Lcom/google/maps/g/gw;",
        ">;",
        "Lcom/google/maps/g/gx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 407
    sget-object v0, Lcom/google/maps/g/gu;->i:Lcom/google/maps/g/gu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 515
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gw;->b:Lcom/google/n/ao;

    .line 574
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gw;->c:Lcom/google/n/ao;

    .line 633
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gw;->d:Ljava/lang/Object;

    .line 773
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gw;->g:Lcom/google/n/ao;

    .line 832
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gw;->h:Lcom/google/n/ao;

    .line 408
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/gu;)Lcom/google/maps/g/gw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 477
    invoke-static {}, Lcom/google/maps/g/gu;->d()Lcom/google/maps/g/gu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 506
    :goto_0
    return-object p0

    .line 478
    :cond_0
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 479
    iget-object v2, p0, Lcom/google/maps/g/gw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/gu;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 480
    iget v2, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/gw;->a:I

    .line 482
    :cond_1
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 483
    iget-object v2, p0, Lcom/google/maps/g/gw;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/gu;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 484
    iget v2, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/gw;->a:I

    .line 486
    :cond_2
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 487
    iget v2, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/gw;->a:I

    .line 488
    iget-object v2, p1, Lcom/google/maps/g/gu;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gw;->d:Ljava/lang/Object;

    .line 491
    :cond_3
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 492
    iget v2, p1, Lcom/google/maps/g/gu;->e:I

    iget v3, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/gw;->a:I

    iput v2, p0, Lcom/google/maps/g/gw;->e:I

    .line 494
    :cond_4
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 495
    iget v2, p1, Lcom/google/maps/g/gu;->f:I

    iget v3, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/gw;->a:I

    iput v2, p0, Lcom/google/maps/g/gw;->f:I

    .line 497
    :cond_5
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 498
    iget-object v2, p0, Lcom/google/maps/g/gw;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/gu;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 499
    iget v2, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/gw;->a:I

    .line 501
    :cond_6
    iget v2, p1, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    :goto_7
    if-eqz v0, :cond_7

    .line 502
    iget-object v0, p0, Lcom/google/maps/g/gw;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/gu;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 503
    iget v0, p0, Lcom/google/maps/g/gw;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/gw;->a:I

    .line 505
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/gu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 478
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 482
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 486
    goto :goto_3

    :cond_b
    move v2, v1

    .line 491
    goto :goto_4

    :cond_c
    move v2, v1

    .line 494
    goto :goto_5

    :cond_d
    move v2, v1

    .line 497
    goto :goto_6

    :cond_e
    move v0, v1

    .line 501
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 399
    new-instance v2, Lcom/google/maps/g/gu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/gu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/gw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/gu;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/gw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/gw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/gu;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/gw;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/gw;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/gw;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gu;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/maps/g/gw;->e:I

    iput v4, v2, Lcom/google/maps/g/gu;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/gw;->f:I

    iput v4, v2, Lcom/google/maps/g/gu;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/gu;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/gw;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/gw;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v3, v2, Lcom/google/maps/g/gu;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/gw;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/gw;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/gu;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 399
    check-cast p1, Lcom/google/maps/g/gu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/gw;->a(Lcom/google/maps/g/gu;)Lcom/google/maps/g/gw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x1

    return v0
.end method
