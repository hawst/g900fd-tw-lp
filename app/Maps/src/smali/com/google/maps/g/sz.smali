.class public final Lcom/google/maps/g/sz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/td;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/sx;",
        "Lcom/google/maps/g/sz;",
        ">;",
        "Lcom/google/maps/g/td;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 626
    sget-object v0, Lcom/google/maps/g/sx;->f:Lcom/google/maps/g/sx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 703
    iput v1, p0, Lcom/google/maps/g/sz;->a:I

    .line 847
    iput v1, p0, Lcom/google/maps/g/sz;->d:I

    .line 883
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/sz;->e:Ljava/lang/Object;

    .line 627
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/sx;)Lcom/google/maps/g/sz;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x4

    const/4 v2, 0x1

    .line 667
    invoke-static {}, Lcom/google/maps/g/sx;->d()Lcom/google/maps/g/sx;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 690
    :goto_0
    return-object p0

    .line 668
    :cond_0
    iget v1, p1, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 669
    iget v1, p1, Lcom/google/maps/g/sx;->d:I

    invoke-static {v1}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    :cond_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v1, v0

    .line 668
    goto :goto_1

    .line 669
    :cond_3
    iget v3, p0, Lcom/google/maps/g/sz;->c:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/sz;->c:I

    iget v1, v1, Lcom/google/maps/g/ta;->d:I

    iput v1, p0, Lcom/google/maps/g/sz;->d:I

    .line 671
    :cond_4
    iget v1, p1, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_5

    move v0, v2

    :cond_5
    if-eqz v0, :cond_6

    .line 672
    iget v0, p0, Lcom/google/maps/g/sz;->c:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/sz;->c:I

    .line 673
    iget-object v0, p1, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/sz;->e:Ljava/lang/Object;

    .line 676
    :cond_6
    sget-object v0, Lcom/google/maps/g/sv;->a:[I

    iget v1, p1, Lcom/google/maps/g/sx;->b:I

    invoke-static {v1}, Lcom/google/maps/g/tc;->a(I)Lcom/google/maps/g/tc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/tc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 683
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/sx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 678
    :pswitch_0
    iget v0, p1, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v2, :cond_7

    iget-object v0, p1, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    move-object v1, v0

    :goto_3
    iget v0, p0, Lcom/google/maps/g/sz;->a:I

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v3

    if-eq v0, v3, :cond_8

    iget-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    invoke-static {v0}, Lcom/google/maps/g/lc;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/maps/g/le;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/le;->c()Lcom/google/maps/g/lc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    :goto_4
    iput v2, p0, Lcom/google/maps/g/sz;->a:I

    goto :goto_2

    :cond_7
    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_8
    iput-object v1, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    goto :goto_4

    .line 682
    :pswitch_1
    iget v0, p1, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v4, :cond_9

    iget-object v0, p1, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    move-object v1, v0

    :goto_5
    iget v0, p0, Lcom/google/maps/g/sz;->a:I

    if-ne v0, v4, :cond_a

    iget-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    invoke-static {}, Lcom/google/maps/g/wj;->d()Lcom/google/maps/g/wj;

    move-result-object v2

    if-eq v0, v2, :cond_a

    iget-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    invoke-static {v0}, Lcom/google/maps/g/wj;->a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/maps/g/wl;->a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/wl;->c()Lcom/google/maps/g/wj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    :goto_6
    iput v4, p0, Lcom/google/maps/g/sz;->a:I

    goto :goto_2

    :cond_9
    invoke-static {}, Lcom/google/maps/g/wj;->d()Lcom/google/maps/g/wj;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    :cond_a
    iput-object v1, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    goto :goto_6

    .line 676
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x4

    .line 618
    new-instance v2, Lcom/google/maps/g/sx;

    invoke-direct {v2, p0}, Lcom/google/maps/g/sx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/sz;->c:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/sz;->a:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    :cond_0
    iget v4, p0, Lcom/google/maps/g/sz;->a:I

    if-ne v4, v0, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    :cond_1
    and-int/lit8 v4, v3, 0x4

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/maps/g/sz;->d:I

    iput v1, v2, Lcom/google/maps/g/sx;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/sz;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/sx;->a:I

    iget v0, p0, Lcom/google/maps/g/sz;->a:I

    iput v0, v2, Lcom/google/maps/g/sx;->b:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 618
    check-cast p1, Lcom/google/maps/g/sx;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/sz;->a(Lcom/google/maps/g/sx;)Lcom/google/maps/g/sz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 694
    iget v0, p0, Lcom/google/maps/g/sz;->a:I

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 695
    iget v0, p0, Lcom/google/maps/g/sz;->a:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/maps/g/sz;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/lc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 700
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 694
    goto :goto_0

    .line 695
    :cond_1
    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 700
    goto :goto_2
.end method
