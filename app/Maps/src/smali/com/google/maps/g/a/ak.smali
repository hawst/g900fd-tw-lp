.class public final Lcom/google/maps/g/a/ak;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/an;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ai;",
        "Lcom/google/maps/g/a/ak;",
        ">;",
        "Lcom/google/maps/g/a/an;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 374
    sget-object v0, Lcom/google/maps/g/a/ai;->e:Lcom/google/maps/g/a/ai;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 466
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ak;->d:Ljava/lang/Object;

    .line 542
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/ak;->c:I

    .line 375
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 412
    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 425
    :goto_0
    return-object p0

    .line 413
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 414
    iget v2, p1, Lcom/google/maps/g/a/ai;->b:I

    iget v3, p0, Lcom/google/maps/g/a/ak;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/ak;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ak;->b:I

    .line 416
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 417
    iget v2, p0, Lcom/google/maps/g/a/ak;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ak;->a:I

    .line 418
    iget-object v2, p1, Lcom/google/maps/g/a/ai;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ak;->d:Ljava/lang/Object;

    .line 421
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 422
    iget v0, p1, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 413
    goto :goto_1

    :cond_5
    move v2, v1

    .line 416
    goto :goto_2

    :cond_6
    move v0, v1

    .line 421
    goto :goto_3

    .line 422
    :cond_7
    iget v1, p0, Lcom/google/maps/g/a/ak;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/ak;->a:I

    iget v0, v0, Lcom/google/maps/g/a/al;->e:I

    iput v0, p0, Lcom/google/maps/g/a/ak;->c:I

    .line 424
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/a/ai;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/maps/g/a/ak;->c()Lcom/google/maps/g/a/ai;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 366
    check-cast p1, Lcom/google/maps/g/a/ai;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ak;->a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/ai;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 392
    new-instance v2, Lcom/google/maps/g/a/ai;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ai;-><init>(Lcom/google/n/v;)V

    .line 393
    iget v3, p0, Lcom/google/maps/g/a/ak;->a:I

    .line 394
    const/4 v1, 0x0

    .line 395
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 398
    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/ak;->b:I

    iput v1, v2, Lcom/google/maps/g/a/ai;->b:I

    .line 399
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 400
    or-int/lit8 v0, v0, 0x2

    .line 402
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/ak;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ai;->c:Ljava/lang/Object;

    .line 403
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 404
    or-int/lit8 v0, v0, 0x4

    .line 406
    :cond_1
    iget v1, p0, Lcom/google/maps/g/a/ak;->c:I

    iput v1, v2, Lcom/google/maps/g/a/ai;->d:I

    .line 407
    iput v0, v2, Lcom/google/maps/g/a/ai;->a:I

    .line 408
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
