.class public final Lcom/google/maps/g/nt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/nr;",
        "Lcom/google/maps/g/nt;",
        ">;",
        "Lcom/google/maps/g/nu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/aq;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/hg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 986
    sget-object v0, Lcom/google/maps/g/nr;->e:Lcom/google/maps/g/nr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1062
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nt;->b:Ljava/lang/Object;

    .line 1138
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    .line 1232
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    .line 987
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/nr;)Lcom/google/maps/g/nt;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1026
    invoke-static {}, Lcom/google/maps/g/nr;->d()Lcom/google/maps/g/nr;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1053
    :goto_0
    return-object p0

    .line 1027
    :cond_0
    iget v1, p1, Lcom/google/maps/g/nr;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    .line 1028
    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/nt;->a:I

    .line 1029
    iget-object v0, p1, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/nt;->b:Ljava/lang/Object;

    .line 1032
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1033
    iget-object v0, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1034
    iget-object v0, p1, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    .line 1035
    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/nt;->a:I

    .line 1042
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1043
    iget-object v0, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1044
    iget-object v0, p1, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    .line 1045
    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/maps/g/nt;->a:I

    .line 1052
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/maps/g/nr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1027
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1037
    :cond_5
    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/nt;->a:I

    .line 1038
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1047
    :cond_7
    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/nt;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/nt;->a:I

    .line 1048
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 978
    new-instance v2, Lcom/google/maps/g/nr;

    invoke-direct {v2, p0}, Lcom/google/maps/g/nr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/nt;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/nt;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/nt;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/nt;->c:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/nt;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/maps/g/nt;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/nt;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/nr;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 978
    check-cast p1, Lcom/google/maps/g/nr;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/nt;->a(Lcom/google/maps/g/nr;)Lcom/google/maps/g/nt;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1057
    const/4 v0, 0x1

    return v0
.end method
