.class public final enum Lcom/google/maps/g/a/dq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/dq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/dq;

.field public static final enum b:Lcom/google/maps/g/a/dq;

.field private static final synthetic d:[Lcom/google/maps/g/a/dq;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/google/maps/g/a/dq;

    const-string v1, "NO_ATTRIBUTION_REQUIRED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dq;->a:Lcom/google/maps/g/a/dq;

    .line 99
    new-instance v0, Lcom/google/maps/g/a/dq;

    const-string v1, "WAZE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dq;->b:Lcom/google/maps/g/a/dq;

    .line 90
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/maps/g/a/dq;

    sget-object v1, Lcom/google/maps/g/a/dq;->a:Lcom/google/maps/g/a/dq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/dq;->b:Lcom/google/maps/g/a/dq;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/a/dq;->d:[Lcom/google/maps/g/a/dq;

    .line 129
    new-instance v0, Lcom/google/maps/g/a/dr;

    invoke-direct {v0}, Lcom/google/maps/g/a/dr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    iput p3, p0, Lcom/google/maps/g/a/dq;->c:I

    .line 140
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/dq;
    .locals 1

    .prologue
    .line 117
    packed-switch p0, :pswitch_data_0

    .line 120
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 118
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/dq;->a:Lcom/google/maps/g/a/dq;

    goto :goto_0

    .line 119
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/dq;->b:Lcom/google/maps/g/a/dq;

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/dq;
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/google/maps/g/a/dq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dq;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/dq;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/maps/g/a/dq;->d:[Lcom/google/maps/g/a/dq;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/dq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/dq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/maps/g/a/dq;->c:I

    return v0
.end method
