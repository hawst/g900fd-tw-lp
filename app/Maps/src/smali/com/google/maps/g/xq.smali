.class public final Lcom/google/maps/g/xq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/xo;",
        "Lcom/google/maps/g/xq;",
        ">;",
        "Lcom/google/maps/g/xr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 431
    sget-object v0, Lcom/google/maps/g/xo;->g:Lcom/google/maps/g/xo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 515
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xq;->b:Ljava/lang/Object;

    .line 591
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xq;->c:Ljava/lang/Object;

    .line 667
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xq;->d:Ljava/lang/Object;

    .line 743
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xq;->e:Ljava/lang/Object;

    .line 432
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/xo;)Lcom/google/maps/g/xq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 481
    invoke-static {}, Lcom/google/maps/g/xo;->d()Lcom/google/maps/g/xo;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 506
    :goto_0
    return-object p0

    .line 482
    :cond_0
    iget v2, p1, Lcom/google/maps/g/xo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 483
    iget v2, p0, Lcom/google/maps/g/xq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/xq;->a:I

    .line 484
    iget-object v2, p1, Lcom/google/maps/g/xo;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xq;->b:Ljava/lang/Object;

    .line 487
    :cond_1
    iget v2, p1, Lcom/google/maps/g/xo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 488
    iget v2, p0, Lcom/google/maps/g/xq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/xq;->a:I

    .line 489
    iget-object v2, p1, Lcom/google/maps/g/xo;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xq;->c:Ljava/lang/Object;

    .line 492
    :cond_2
    iget v2, p1, Lcom/google/maps/g/xo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 493
    iget v2, p0, Lcom/google/maps/g/xq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/xq;->a:I

    .line 494
    iget-object v2, p1, Lcom/google/maps/g/xo;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xq;->d:Ljava/lang/Object;

    .line 497
    :cond_3
    iget v2, p1, Lcom/google/maps/g/xo;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 498
    iget v2, p0, Lcom/google/maps/g/xq;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/xq;->a:I

    .line 499
    iget-object v2, p1, Lcom/google/maps/g/xo;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xq;->e:Ljava/lang/Object;

    .line 502
    :cond_4
    iget v2, p1, Lcom/google/maps/g/xo;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 503
    iget-boolean v0, p1, Lcom/google/maps/g/xo;->f:Z

    iget v1, p0, Lcom/google/maps/g/xq;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/xq;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/xq;->f:Z

    .line 505
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/xo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 482
    goto :goto_1

    :cond_7
    move v2, v1

    .line 487
    goto :goto_2

    :cond_8
    move v2, v1

    .line 492
    goto :goto_3

    :cond_9
    move v2, v1

    .line 497
    goto :goto_4

    :cond_a
    move v0, v1

    .line 502
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 423
    new-instance v2, Lcom/google/maps/g/xo;

    invoke-direct {v2, p0}, Lcom/google/maps/g/xo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/xq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/xq;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xo;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/xq;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xo;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/xq;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xo;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/xq;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xo;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/maps/g/xq;->f:Z

    iput-boolean v1, v2, Lcom/google/maps/g/xo;->f:Z

    iput v0, v2, Lcom/google/maps/g/xo;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 423
    check-cast p1, Lcom/google/maps/g/xo;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/xq;->a(Lcom/google/maps/g/xo;)Lcom/google/maps/g/xq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x1

    return v0
.end method
