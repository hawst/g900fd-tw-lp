.class public final enum Lcom/google/maps/g/il;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/il;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/il;

.field public static final enum b:Lcom/google/maps/g/il;

.field public static final enum c:Lcom/google/maps/g/il;

.field private static final synthetic e:[Lcom/google/maps/g/il;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    new-instance v0, Lcom/google/maps/g/il;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/il;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/il;->a:Lcom/google/maps/g/il;

    .line 113
    new-instance v0, Lcom/google/maps/g/il;

    const-string v1, "BAD_REQUEST"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/il;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/il;->b:Lcom/google/maps/g/il;

    .line 117
    new-instance v0, Lcom/google/maps/g/il;

    const-string v1, "BACKEND_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/il;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/il;->c:Lcom/google/maps/g/il;

    .line 104
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/il;

    sget-object v1, Lcom/google/maps/g/il;->a:Lcom/google/maps/g/il;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/il;->b:Lcom/google/maps/g/il;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/il;->c:Lcom/google/maps/g/il;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/il;->e:[Lcom/google/maps/g/il;

    .line 152
    new-instance v0, Lcom/google/maps/g/im;

    invoke-direct {v0}, Lcom/google/maps/g/im;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput p3, p0, Lcom/google/maps/g/il;->d:I

    .line 163
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/il;
    .locals 1

    .prologue
    .line 139
    packed-switch p0, :pswitch_data_0

    .line 143
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 140
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/il;->a:Lcom/google/maps/g/il;

    goto :goto_0

    .line 141
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/il;->b:Lcom/google/maps/g/il;

    goto :goto_0

    .line 142
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/il;->c:Lcom/google/maps/g/il;

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/il;
    .locals 1

    .prologue
    .line 104
    const-class v0, Lcom/google/maps/g/il;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/il;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/il;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/google/maps/g/il;->e:[Lcom/google/maps/g/il;

    invoke-virtual {v0}, [Lcom/google/maps/g/il;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/il;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/maps/g/il;->d:I

    return v0
.end method
