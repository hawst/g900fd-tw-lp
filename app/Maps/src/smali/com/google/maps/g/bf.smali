.class public final enum Lcom/google/maps/g/bf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/bf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/bf;

.field public static final enum b:Lcom/google/maps/g/bf;

.field public static final enum c:Lcom/google/maps/g/bf;

.field public static final enum d:Lcom/google/maps/g/bf;

.field private static final synthetic f:[Lcom/google/maps/g/bf;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 180
    new-instance v0, Lcom/google/maps/g/bf;

    const-string v1, "MULTIPLE_CHOICE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/bf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bf;->a:Lcom/google/maps/g/bf;

    .line 181
    new-instance v0, Lcom/google/maps/g/bf;

    const-string v1, "FIVE_STAR_RATING"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/maps/g/bf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bf;->b:Lcom/google/maps/g/bf;

    .line 182
    new-instance v0, Lcom/google/maps/g/bf;

    const-string v1, "FREE_FORM"

    invoke-direct {v0, v1, v3, v6}, Lcom/google/maps/g/bf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bf;->c:Lcom/google/maps/g/bf;

    .line 183
    new-instance v0, Lcom/google/maps/g/bf;

    const-string v1, "ANSWERTYPE_NOT_SET"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/bf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bf;->d:Lcom/google/maps/g/bf;

    .line 178
    new-array v0, v6, [Lcom/google/maps/g/bf;

    sget-object v1, Lcom/google/maps/g/bf;->a:Lcom/google/maps/g/bf;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/bf;->b:Lcom/google/maps/g/bf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/bf;->c:Lcom/google/maps/g/bf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/bf;->d:Lcom/google/maps/g/bf;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/bf;->f:[Lcom/google/maps/g/bf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/bf;->e:I

    .line 186
    iput p3, p0, Lcom/google/maps/g/bf;->e:I

    .line 187
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/bf;
    .locals 2

    .prologue
    .line 189
    packed-switch p0, :pswitch_data_0

    .line 194
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/bf;->a:Lcom/google/maps/g/bf;

    .line 193
    :goto_0
    return-object v0

    .line 191
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/bf;->b:Lcom/google/maps/g/bf;

    goto :goto_0

    .line 192
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/bf;->c:Lcom/google/maps/g/bf;

    goto :goto_0

    .line 193
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/bf;->d:Lcom/google/maps/g/bf;

    goto :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/bf;
    .locals 1

    .prologue
    .line 178
    const-class v0, Lcom/google/maps/g/bf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/bf;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/bf;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/google/maps/g/bf;->f:[Lcom/google/maps/g/bf;

    invoke-virtual {v0}, [Lcom/google/maps/g/bf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/bf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/google/maps/g/bf;->e:I

    return v0
.end method
