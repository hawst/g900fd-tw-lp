.class public final Lcom/google/maps/g/nh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/nc;",
        "Lcom/google/maps/g/nh;",
        ">;",
        "Lcom/google/maps/g/nn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/f;

.field private f:Lcom/google/n/f;

.field private g:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 531
    sget-object v0, Lcom/google/maps/g/nc;->h:Lcom/google/maps/g/nc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 651
    iput v1, p0, Lcom/google/maps/g/nh;->a:I

    .line 667
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nh;->d:Ljava/lang/Object;

    .line 743
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nh;->e:Lcom/google/n/f;

    .line 778
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nh;->f:Lcom/google/n/f;

    .line 813
    iput v1, p0, Lcom/google/maps/g/nh;->g:I

    .line 532
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/nc;)Lcom/google/maps/g/nh;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 596
    invoke-static {}, Lcom/google/maps/g/nc;->d()Lcom/google/maps/g/nc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 644
    :goto_0
    return-object p0

    .line 597
    :cond_0
    iget v2, p1, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 598
    iget v2, p0, Lcom/google/maps/g/nh;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/nh;->c:I

    .line 599
    iget-object v2, p1, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/nh;->d:Ljava/lang/Object;

    .line 602
    :cond_1
    iget v2, p1, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 603
    iget-object v2, p1, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 597
    goto :goto_1

    :cond_3
    move v2, v1

    .line 602
    goto :goto_2

    .line 603
    :cond_4
    iget v3, p0, Lcom/google/maps/g/nh;->c:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/nh;->c:I

    iput-object v2, p0, Lcom/google/maps/g/nh;->e:Lcom/google/n/f;

    .line 605
    :cond_5
    iget v2, p1, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 606
    iget-object v2, p1, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 605
    goto :goto_3

    .line 606
    :cond_7
    iget v3, p0, Lcom/google/maps/g/nh;->c:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/nh;->c:I

    iput-object v2, p0, Lcom/google/maps/g/nh;->f:Lcom/google/n/f;

    .line 608
    :cond_8
    iget v2, p1, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_c

    .line 609
    iget v0, p1, Lcom/google/maps/g/nc;->g:I

    invoke-static {v0}, Lcom/google/maps/g/nf;->a(I)Lcom/google/maps/g/nf;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    :cond_9
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v0, v1

    .line 608
    goto :goto_4

    .line 609
    :cond_b
    iget v1, p0, Lcom/google/maps/g/nh;->c:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/nh;->c:I

    iget v0, v0, Lcom/google/maps/g/nf;->d:I

    iput v0, p0, Lcom/google/maps/g/nh;->g:I

    .line 611
    :cond_c
    sget-object v0, Lcom/google/maps/g/ne;->a:[I

    iget v1, p1, Lcom/google/maps/g/nc;->b:I

    invoke-static {v1}, Lcom/google/maps/g/ni;->a(I)Lcom/google/maps/g/ni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/ni;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 637
    :goto_5
    iget-object v0, p1, Lcom/google/maps/g/nc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 613
    :pswitch_0
    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    if-eq v0, v5, :cond_d

    .line 614
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    .line 616
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 617
    iget-object v1, p1, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 616
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 618
    iput v5, p0, Lcom/google/maps/g/nh;->a:I

    goto :goto_5

    .line 622
    :pswitch_1
    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    if-eq v0, v4, :cond_e

    .line 623
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    .line 625
    :cond_e
    iget-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 626
    iget-object v1, p1, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 625
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 627
    iput v4, p0, Lcom/google/maps/g/nh;->a:I

    goto :goto_5

    .line 631
    :pswitch_2
    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    if-eq v0, v6, :cond_f

    .line 632
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    .line 634
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 635
    iget-object v1, p1, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 634
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 636
    iput v6, p0, Lcom/google/maps/g/nh;->a:I

    goto :goto_5

    .line 611
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 523
    new-instance v4, Lcom/google/maps/g/nc;

    invoke-direct {v4, p0}, Lcom/google/maps/g/nc;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/maps/g/nh;->c:I

    and-int/lit8 v3, v1, 0x1

    if-ne v3, v0, :cond_6

    :goto_0
    iget-object v3, p0, Lcom/google/maps/g/nh;->d:Ljava/lang/Object;

    iput-object v3, v4, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, p0, Lcom/google/maps/g/nh;->e:Lcom/google/n/f;

    iput-object v3, v4, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    and-int/lit8 v3, v1, 0x4

    if-ne v3, v6, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, p0, Lcom/google/maps/g/nh;->f:Lcom/google/n/f;

    iput-object v3, v4, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x8

    move v3, v0

    :goto_1
    iget v0, p0, Lcom/google/maps/g/nh;->g:I

    iput v0, v4, Lcom/google/maps/g/nc;->g:I

    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    :cond_2
    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    if-ne v0, v6, :cond_3

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    :cond_3
    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/maps/g/nh;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    :cond_4
    iput v3, v4, Lcom/google/maps/g/nc;->a:I

    iget v0, p0, Lcom/google/maps/g/nh;->a:I

    iput v0, v4, Lcom/google/maps/g/nc;->b:I

    return-object v4

    :cond_5
    move v3, v0

    goto :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 523
    check-cast p1, Lcom/google/maps/g/nc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/nh;->a(Lcom/google/maps/g/nc;)Lcom/google/maps/g/nh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 648
    const/4 v0, 0x1

    return v0
.end method
