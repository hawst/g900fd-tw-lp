.class public final enum Lcom/google/maps/g/ni;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ni;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/ni;

.field public static final enum b:Lcom/google/maps/g/ni;

.field public static final enum c:Lcom/google/maps/g/ni;

.field public static final enum d:Lcom/google/maps/g/ni;

.field private static final synthetic f:[Lcom/google/maps/g/ni;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 199
    new-instance v0, Lcom/google/maps/g/ni;

    const-string v1, "MULTIPLE_CHOICE_QUESTION"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/ni;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ni;->a:Lcom/google/maps/g/ni;

    .line 200
    new-instance v0, Lcom/google/maps/g/ni;

    const-string v1, "FIVE_STAR_RATING_QUESTION"

    invoke-direct {v0, v1, v5, v7}, Lcom/google/maps/g/ni;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ni;->b:Lcom/google/maps/g/ni;

    .line 201
    new-instance v0, Lcom/google/maps/g/ni;

    const-string v1, "FREE_FORM_QUESTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/g/ni;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ni;->c:Lcom/google/maps/g/ni;

    .line 202
    new-instance v0, Lcom/google/maps/g/ni;

    const-string v1, "QUESTIONPRESENTATIONFORM_NOT_SET"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/maps/g/ni;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ni;->d:Lcom/google/maps/g/ni;

    .line 197
    new-array v0, v7, [Lcom/google/maps/g/ni;

    sget-object v1, Lcom/google/maps/g/ni;->a:Lcom/google/maps/g/ni;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/ni;->b:Lcom/google/maps/g/ni;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/ni;->c:Lcom/google/maps/g/ni;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/ni;->d:Lcom/google/maps/g/ni;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/ni;->f:[Lcom/google/maps/g/ni;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ni;->e:I

    .line 205
    iput p3, p0, Lcom/google/maps/g/ni;->e:I

    .line 206
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ni;
    .locals 2

    .prologue
    .line 208
    packed-switch p0, :pswitch_data_0

    .line 213
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/ni;->a:Lcom/google/maps/g/ni;

    .line 212
    :goto_0
    return-object v0

    .line 210
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/ni;->b:Lcom/google/maps/g/ni;

    goto :goto_0

    .line 211
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/ni;->c:Lcom/google/maps/g/ni;

    goto :goto_0

    .line 212
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/ni;->d:Lcom/google/maps/g/ni;

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ni;
    .locals 1

    .prologue
    .line 197
    const-class v0, Lcom/google/maps/g/ni;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ni;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ni;
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/google/maps/g/ni;->f:[Lcom/google/maps/g/ni;

    invoke-virtual {v0}, [Lcom/google/maps/g/ni;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ni;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/google/maps/g/ni;->e:I

    return v0
.end method
