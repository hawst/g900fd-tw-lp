.class public final Lcom/google/maps/g/lh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/lk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lh;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Lcom/google/maps/g/lh;

.field private static final serialVersionUID:J

.field private static volatile u:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Z

.field d:Z

.field e:Z

.field f:Lcom/google/n/ao;

.field g:Z

.field h:Z

.field i:Lcom/google/n/ao;

.field j:Z

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:I

.field n:Z

.field o:Lcom/google/n/ao;

.field p:Z

.field q:Z

.field private s:B

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/maps/g/li;

    invoke-direct {v0}, Lcom/google/maps/g/li;-><init>()V

    sput-object v0, Lcom/google/maps/g/lh;->PARSER:Lcom/google/n/ax;

    .line 933
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/lh;->u:Lcom/google/n/aw;

    .line 1884
    new-instance v0, Lcom/google/maps/g/lh;

    invoke-direct {v0}, Lcom/google/maps/g/lh;-><init>()V

    sput-object v0, Lcom/google/maps/g/lh;->r:Lcom/google/maps/g/lh;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 547
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    .line 608
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    .line 654
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    .line 685
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    .line 701
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    .line 747
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    .line 792
    iput-byte v4, p0, Lcom/google/maps/g/lh;->s:B

    .line 856
    iput v4, p0, Lcom/google/maps/g/lh;->t:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->c:Z

    .line 20
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->d:Z

    .line 21
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->e:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->g:Z

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->h:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iput-boolean v3, p0, Lcom/google/maps/g/lh;->j:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 29
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/lh;->m:I

    .line 30
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->n:Z

    .line 31
    iget-object v0, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 32
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->p:Z

    .line 33
    iput-boolean v2, p0, Lcom/google/maps/g/lh;->q:Z

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/maps/g/lh;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 46
    :cond_0
    :goto_0
    if-nez v3, :cond_a

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 53
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 55
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 51
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 61
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/lh;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/lh;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 145
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 66
    goto :goto_1

    .line 70
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->d:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 75
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->e:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 80
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 81
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    goto/16 :goto_0

    .line 85
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->g:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 90
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->h:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_5

    .line 95
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 96
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    goto/16 :goto_0

    .line 100
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 101
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->j:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_6

    .line 105
    :sswitch_a
    iget-object v0, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 106
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    goto/16 :goto_0

    .line 110
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 111
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    goto/16 :goto_0

    .line 115
    :sswitch_c
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 116
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/lh;->m:I

    goto/16 :goto_0

    .line 120
    :sswitch_d
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 121
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->n:Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_7

    .line 125
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 126
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    goto/16 :goto_0

    .line 130
    :sswitch_f
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 131
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->p:Z

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_8

    .line 135
    :sswitch_10
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/g/lh;->a:I

    .line 136
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/google/maps/g/lh;->q:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_9

    .line 147
    :cond_a
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lh;->au:Lcom/google/n/bn;

    .line 148
    return-void

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 547
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    .line 608
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    .line 654
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    .line 685
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    .line 701
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    .line 747
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    .line 792
    iput-byte v1, p0, Lcom/google/maps/g/lh;->s:B

    .line 856
    iput v1, p0, Lcom/google/maps/g/lh;->t:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/lh;
    .locals 1

    .prologue
    .line 1887
    sget-object v0, Lcom/google/maps/g/lh;->r:Lcom/google/maps/g/lh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/lj;
    .locals 1

    .prologue
    .line 995
    new-instance v0, Lcom/google/maps/g/lj;

    invoke-direct {v0}, Lcom/google/maps/g/lj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    sget-object v0, Lcom/google/maps/g/lh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 804
    invoke-virtual {p0}, Lcom/google/maps/g/lh;->c()I

    .line 805
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 806
    iget-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 808
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 809
    iget-boolean v0, p0, Lcom/google/maps/g/lh;->c:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_10

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 811
    :cond_1
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 812
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_11

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 814
    :cond_2
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 815
    iget-boolean v0, p0, Lcom/google/maps/g/lh;->e:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_12

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 817
    :cond_3
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 818
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 820
    :cond_4
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 821
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_13

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 823
    :cond_5
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 824
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_14

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 826
    :cond_6
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 827
    iget-object v0, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 829
    :cond_7
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 830
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_15

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 832
    :cond_8
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_9

    .line 833
    const/16 v0, 0xa

    iget-object v3, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 835
    :cond_9
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_a

    .line 836
    const/16 v0, 0xb

    iget-object v3, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 838
    :cond_a
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_b

    .line 839
    const/16 v0, 0xc

    iget v3, p0, Lcom/google/maps/g/lh;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_16

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 841
    :cond_b
    :goto_6
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_c

    .line 842
    const/16 v0, 0xd

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->n:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_17

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 844
    :cond_c
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_d

    .line 845
    const/16 v0, 0xe

    iget-object v3, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 847
    :cond_d
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_e

    .line 848
    const/16 v0, 0xf

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->p:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_18

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 850
    :cond_e
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_f

    .line 851
    const/16 v0, 0x10

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->q:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_19

    :goto_9
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 853
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/lh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 854
    return-void

    :cond_10
    move v0, v2

    .line 809
    goto/16 :goto_0

    :cond_11
    move v0, v2

    .line 812
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 815
    goto/16 :goto_2

    :cond_13
    move v0, v2

    .line 821
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 824
    goto/16 :goto_4

    :cond_15
    move v0, v2

    .line 830
    goto/16 :goto_5

    .line 839
    :cond_16
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    :cond_17
    move v0, v2

    .line 842
    goto :goto_7

    :cond_18
    move v0, v2

    .line 848
    goto :goto_8

    :cond_19
    move v1, v2

    .line 851
    goto :goto_9
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 794
    iget-byte v1, p0, Lcom/google/maps/g/lh;->s:B

    .line 795
    if-ne v1, v0, :cond_0

    .line 799
    :goto_0
    return v0

    .line 796
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 798
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/lh;->s:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 858
    iget v0, p0, Lcom/google/maps/g/lh;->t:I

    .line 859
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 928
    :goto_0
    return v0

    .line 862
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_11

    .line 863
    iget-object v0, p0, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    .line 864
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 866
    :goto_1
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 867
    iget-boolean v2, p0, Lcom/google/maps/g/lh;->c:Z

    .line 868
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 870
    :cond_1
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 871
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->d:Z

    .line 872
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 874
    :cond_2
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 875
    iget-boolean v2, p0, Lcom/google/maps/g/lh;->e:Z

    .line 876
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 878
    :cond_3
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 879
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    .line 880
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 882
    :cond_4
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 883
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->g:Z

    .line 884
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 886
    :cond_5
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 887
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->h:Z

    .line 888
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 890
    :cond_6
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 891
    iget-object v2, p0, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    .line 892
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 894
    :cond_7
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_8

    .line 895
    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->j:Z

    .line 896
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 898
    :cond_8
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_9

    .line 899
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    .line 900
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 902
    :cond_9
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_a

    .line 903
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    .line 904
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 906
    :cond_a
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_b

    .line 907
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/maps/g/lh;->m:I

    .line 908
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_10

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 910
    :cond_b
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_c

    .line 911
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->n:Z

    .line 912
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 914
    :cond_c
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_d

    .line 915
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    .line 916
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 918
    :cond_d
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_e

    .line 919
    const/16 v2, 0xf

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->p:Z

    .line 920
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 922
    :cond_e
    iget v2, p0, Lcom/google/maps/g/lh;->a:I

    const v3, 0x8000

    and-int/2addr v2, v3

    const v3, 0x8000

    if-ne v2, v3, :cond_f

    .line 923
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/maps/g/lh;->q:Z

    .line 924
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 926
    :cond_f
    iget-object v1, p0, Lcom/google/maps/g/lh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 927
    iput v0, p0, Lcom/google/maps/g/lh;->t:I

    goto/16 :goto_0

    .line 908
    :cond_10
    const/16 v2, 0xa

    goto :goto_2

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/lh;->newBuilder()Lcom/google/maps/g/lj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/lj;->a(Lcom/google/maps/g/lh;)Lcom/google/maps/g/lj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/lh;->newBuilder()Lcom/google/maps/g/lj;

    move-result-object v0

    return-object v0
.end method
