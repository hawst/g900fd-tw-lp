.class public final Lcom/google/maps/g/jn;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/js;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/jl;",
        "Lcom/google/maps/g/jn;",
        ">;",
        "Lcom/google/maps/g/js;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/jh;

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1218
    sget-object v0, Lcom/google/maps/g/jl;->g:Lcom/google/maps/g/jl;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    .line 1357
    iput v1, p0, Lcom/google/maps/g/jn;->c:I

    .line 1393
    iput v1, p0, Lcom/google/maps/g/jn;->d:I

    .line 1461
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/jn;->f:Ljava/lang/Object;

    .line 1219
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/jl;)Lcom/google/maps/g/jn;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1268
    invoke-static {}, Lcom/google/maps/g/jl;->d()Lcom/google/maps/g/jl;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1287
    :goto_0
    return-object p0

    .line 1269
    :cond_0
    iget v0, p1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1270
    iget-object v0, p1, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/jn;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    invoke-static {v3}, Lcom/google/maps/g/jh;->a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/jj;->a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/jj;->c()Lcom/google/maps/g/jh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/jn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/jn;->a:I

    .line 1272
    :cond_1
    iget v0, p1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    .line 1273
    iget v0, p1, Lcom/google/maps/g/jl;->c:I

    invoke-static {v0}, Lcom/google/maps/g/jo;->a(I)Lcom/google/maps/g/jo;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/jo;->a:Lcom/google/maps/g/jo;

    :cond_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 1269
    goto :goto_1

    .line 1270
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 1272
    goto :goto_4

    .line 1273
    :cond_7
    iget v3, p0, Lcom/google/maps/g/jn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/jn;->a:I

    iget v0, v0, Lcom/google/maps/g/jo;->c:I

    iput v0, p0, Lcom/google/maps/g/jn;->c:I

    .line 1275
    :cond_8
    iget v0, p1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_c

    .line 1276
    iget v0, p1, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_9
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v0, v2

    .line 1275
    goto :goto_5

    .line 1276
    :cond_b
    iget v3, p0, Lcom/google/maps/g/jn;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/jn;->a:I

    iget v0, v0, Lcom/google/maps/g/jq;->f:I

    iput v0, p0, Lcom/google/maps/g/jn;->d:I

    .line 1278
    :cond_c
    iget v0, p1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_6
    if-eqz v0, :cond_d

    .line 1279
    iget v0, p1, Lcom/google/maps/g/jl;->e:I

    iget v3, p0, Lcom/google/maps/g/jn;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/jn;->a:I

    iput v0, p0, Lcom/google/maps/g/jn;->e:I

    .line 1281
    :cond_d
    iget v0, p1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_7
    if-eqz v0, :cond_e

    .line 1282
    iget v0, p0, Lcom/google/maps/g/jn;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/jn;->a:I

    .line 1283
    iget-object v0, p1, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/jn;->f:Ljava/lang/Object;

    .line 1286
    :cond_e
    iget-object v0, p1, Lcom/google/maps/g/jl;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v0, v2

    .line 1278
    goto :goto_6

    :cond_10
    move v0, v2

    .line 1281
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1210
    new-instance v2, Lcom/google/maps/g/jl;

    invoke-direct {v2, p0}, Lcom/google/maps/g/jl;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/jn;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/jn;->b:Lcom/google/maps/g/jh;

    iput-object v1, v2, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/jn;->c:I

    iput v1, v2, Lcom/google/maps/g/jl;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/jn;->d:I

    iput v1, v2, Lcom/google/maps/g/jl;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/jn;->e:I

    iput v1, v2, Lcom/google/maps/g/jl;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/jn;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/jl;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1210
    check-cast p1, Lcom/google/maps/g/jl;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/jn;->a(Lcom/google/maps/g/jl;)Lcom/google/maps/g/jn;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1291
    const/4 v0, 0x1

    return v0
.end method
