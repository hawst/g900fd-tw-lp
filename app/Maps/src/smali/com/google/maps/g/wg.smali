.class public final Lcom/google/maps/g/wg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/we;",
        "Lcom/google/maps/g/wg;",
        ">;",
        "Lcom/google/maps/g/wh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/aq;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 401
    sget-object v0, Lcom/google/maps/g/we;->f:Lcom/google/maps/g/we;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 482
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wg;->b:Ljava/lang/Object;

    .line 558
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    .line 651
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wg;->d:Ljava/lang/Object;

    .line 727
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wg;->e:Ljava/lang/Object;

    .line 402
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/we;)Lcom/google/maps/g/wg;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 446
    invoke-static {}, Lcom/google/maps/g/we;->d()Lcom/google/maps/g/we;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 473
    :goto_0
    return-object p0

    .line 447
    :cond_0
    iget v2, p1, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 448
    iget v2, p0, Lcom/google/maps/g/wg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/wg;->a:I

    .line 449
    iget-object v2, p1, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/wg;->b:Ljava/lang/Object;

    .line 452
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 453
    iget-object v2, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 454
    iget-object v2, p1, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    .line 455
    iget v2, p0, Lcom/google/maps/g/wg;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/wg;->a:I

    .line 462
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 463
    iget v2, p0, Lcom/google/maps/g/wg;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/wg;->a:I

    .line 464
    iget-object v2, p1, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/wg;->d:Ljava/lang/Object;

    .line 467
    :cond_3
    iget v2, p1, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_4

    .line 468
    iget v0, p0, Lcom/google/maps/g/wg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/wg;->a:I

    .line 469
    iget-object v0, p1, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/wg;->e:Ljava/lang/Object;

    .line 472
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/we;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 447
    goto :goto_1

    .line 457
    :cond_6
    iget v2, p0, Lcom/google/maps/g/wg;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_7

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/wg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/wg;->a:I

    .line 458
    :cond_7
    iget-object v2, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    move v2, v1

    .line 462
    goto :goto_3

    :cond_9
    move v0, v1

    .line 467
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 393
    new-instance v2, Lcom/google/maps/g/we;

    invoke-direct {v2, p0}, Lcom/google/maps/g/we;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/wg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/wg;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/wg;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/wg;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/wg;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/wg;->c:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/wg;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/wg;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/we;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 393
    check-cast p1, Lcom/google/maps/g/we;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/wg;->a(Lcom/google/maps/g/we;)Lcom/google/maps/g/wg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x1

    return v0
.end method
