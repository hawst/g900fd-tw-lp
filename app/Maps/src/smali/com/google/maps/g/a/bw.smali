.class public final Lcom/google/maps/g/a/bw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/bu;",
        "Lcom/google/maps/g/a/bw;",
        ">;",
        "Lcom/google/maps/g/a/bz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 518
    sget-object v0, Lcom/google/maps/g/a/bu;->j:Lcom/google/maps/g/a/bu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 627
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/bw;->b:I

    .line 791
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bw;->g:Ljava/lang/Object;

    .line 519
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/bu;)Lcom/google/maps/g/a/bw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 586
    invoke-static {}, Lcom/google/maps/g/a/bu;->d()Lcom/google/maps/g/a/bu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 614
    :goto_0
    return-object p0

    .line 587
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 588
    iget v2, p1, Lcom/google/maps/g/a/bu;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/bx;->a(I)Lcom/google/maps/g/a/bx;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 587
    goto :goto_1

    .line 588
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iget v2, v2, Lcom/google/maps/g/a/bx;->e:I

    iput v2, p0, Lcom/google/maps/g/a/bw;->b:I

    .line 590
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 591
    iget v2, p1, Lcom/google/maps/g/a/bu;->c:I

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bw;->c:I

    .line 593
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 594
    iget v2, p1, Lcom/google/maps/g/a/bu;->d:I

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bw;->d:I

    .line 596
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 597
    iget v2, p1, Lcom/google/maps/g/a/bu;->e:I

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bw;->e:I

    .line 599
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 600
    iget-boolean v2, p1, Lcom/google/maps/g/a/bu;->f:Z

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/bw;->f:Z

    .line 602
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 603
    iget v2, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/bw;->a:I

    .line 604
    iget-object v2, p1, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/bw;->g:Ljava/lang/Object;

    .line 607
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 608
    iget v2, p1, Lcom/google/maps/g/a/bu;->h:I

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/bw;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bw;->h:I

    .line 610
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 611
    iget v0, p1, Lcom/google/maps/g/a/bu;->i:I

    iget v1, p0, Lcom/google/maps/g/a/bw;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/a/bw;->a:I

    iput v0, p0, Lcom/google/maps/g/a/bw;->i:I

    .line 613
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 590
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 593
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 596
    goto :goto_4

    :cond_f
    move v2, v1

    .line 599
    goto :goto_5

    :cond_10
    move v2, v1

    .line 602
    goto :goto_6

    :cond_11
    move v2, v1

    .line 607
    goto :goto_7

    :cond_12
    move v0, v1

    .line 610
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 510
    new-instance v2, Lcom/google/maps/g/a/bu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/bu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/bw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/bw;->b:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/a/bw;->c:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/a/bw;->d:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/a/bw;->e:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/maps/g/a/bw;->f:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/bu;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/bw;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/maps/g/a/bw;->h:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/maps/g/a/bw;->i:I

    iput v1, v2, Lcom/google/maps/g/a/bu;->i:I

    iput v0, v2, Lcom/google/maps/g/a/bu;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 510
    check-cast p1, Lcom/google/maps/g/a/bu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/bw;->a(Lcom/google/maps/g/a/bu;)Lcom/google/maps/g/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 618
    iget v2, p0, Lcom/google/maps/g/a/bw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 622
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 618
    goto :goto_0

    :cond_1
    move v0, v1

    .line 622
    goto :goto_1
.end method
