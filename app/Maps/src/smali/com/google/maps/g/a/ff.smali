.class public final Lcom/google/maps/g/a/ff;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ff;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/a/ff;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field public f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/google/maps/g/a/fg;

    invoke-direct {v0}, Lcom/google/maps/g/a/fg;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ff;->PARSER:Lcom/google/n/ax;

    .line 317
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ff;->j:Lcom/google/n/aw;

    .line 868
    new-instance v0, Lcom/google/maps/g/a/ff;

    invoke-direct {v0}, Lcom/google/maps/g/a/ff;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ff;->g:Lcom/google/maps/g/a/ff;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 152
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    .line 241
    iput-byte v2, p0, Lcom/google/maps/g/a/ff;->h:B

    .line 284
    iput v2, p0, Lcom/google/maps/g/a/ff;->i:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/a/ff;->e:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/a/ff;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 35
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 50
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/ff;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v10, :cond_1

    .line 88
    iget-object v1, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    .line 90
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ff;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v10, :cond_2

    .line 55
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    .line 57
    or-int/lit8 v1, v1, 0x4

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 59
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/ff;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/ff;->e:Z

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget v6, p0, Lcom/google/maps/g/a/ff;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/a/ff;->a:I

    .line 71
    iput-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 75
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 76
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/ff;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_4
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v10, :cond_5

    .line 88
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    .line 90
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->au:Lcom/google/n/bn;

    .line 91
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 152
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    .line 241
    iput-byte v1, p0, Lcom/google/maps/g/a/ff;->h:B

    .line 284
    iput v1, p0, Lcom/google/maps/g/a/ff;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/ff;
    .locals 1

    .prologue
    .line 871
    sget-object v0, Lcom/google/maps/g/a/ff;->g:Lcom/google/maps/g/a/ff;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/fh;
    .locals 1

    .prologue
    .line 379
    new-instance v0, Lcom/google/maps/g/a/fh;

    invoke-direct {v0}, Lcom/google/maps/g/a/fh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ff;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/google/maps/g/a/ff;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 265
    invoke-virtual {p0}, Lcom/google/maps/g/a/ff;->c()I

    .line 266
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 269
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 269
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 272
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 273
    iget-boolean v0, p0, Lcom/google/maps/g/a/ff;->e:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_2

    move v2, v3

    :cond_2
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 275
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 276
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 278
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 279
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 282
    return-void

    .line 276
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 243
    iget-byte v0, p0, Lcom/google/maps/g/a/ff;->h:B

    .line 244
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 260
    :cond_0
    :goto_0
    return v2

    .line 245
    :cond_1
    if-eqz v0, :cond_0

    .line 247
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 248
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 249
    iput-byte v2, p0, Lcom/google/maps/g/a/ff;->h:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 247
    goto :goto_1

    :cond_3
    move v1, v2

    .line 253
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 254
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/eu;->d()Lcom/google/maps/g/a/eu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/eu;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 255
    iput-byte v2, p0, Lcom/google/maps/g/a/ff;->h:B

    goto :goto_0

    .line 253
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 259
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/g/a/ff;->h:B

    move v2, v3

    .line 260
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 286
    iget v0, p0, Lcom/google/maps/g/a/ff;->i:I

    .line 287
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 312
    :goto_0
    return v0

    .line 290
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 291
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    .line 292
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 294
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    .line 296
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 294
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 298
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 299
    iget-boolean v0, p0, Lcom/google/maps/g/a/ff;->e:Z

    .line 300
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 302
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 303
    const/4 v2, 0x5

    .line 304
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 306
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 307
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    .line 308
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 310
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/a/ff;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 311
    iput v0, p0, Lcom/google/maps/g/a/ff;->i:I

    goto/16 :goto_0

    .line 304
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ff;->newBuilder()Lcom/google/maps/g/a/fh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/fh;->a(Lcom/google/maps/g/a/ff;)Lcom/google/maps/g/a/fh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ff;->newBuilder()Lcom/google/maps/g/a/fh;

    move-result-object v0

    return-object v0
.end method
