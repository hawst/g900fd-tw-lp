.class public final enum Lcom/google/maps/g/cu;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/cu;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/cu;

.field public static final enum b:Lcom/google/maps/g/cu;

.field public static final enum c:Lcom/google/maps/g/cu;

.field private static final synthetic e:[Lcom/google/maps/g/cu;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 168
    new-instance v0, Lcom/google/maps/g/cu;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/cu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/cu;->a:Lcom/google/maps/g/cu;

    .line 172
    new-instance v0, Lcom/google/maps/g/cu;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/cu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/cu;->b:Lcom/google/maps/g/cu;

    .line 176
    new-instance v0, Lcom/google/maps/g/cu;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/cu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/cu;->c:Lcom/google/maps/g/cu;

    .line 163
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/cu;

    sget-object v1, Lcom/google/maps/g/cu;->a:Lcom/google/maps/g/cu;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/cu;->b:Lcom/google/maps/g/cu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/cu;->c:Lcom/google/maps/g/cu;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/cu;->e:[Lcom/google/maps/g/cu;

    .line 211
    new-instance v0, Lcom/google/maps/g/cv;

    invoke-direct {v0}, Lcom/google/maps/g/cv;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 221
    iput p3, p0, Lcom/google/maps/g/cu;->d:I

    .line 222
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/cu;
    .locals 1

    .prologue
    .line 198
    packed-switch p0, :pswitch_data_0

    .line 202
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 199
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/cu;->a:Lcom/google/maps/g/cu;

    goto :goto_0

    .line 200
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/cu;->b:Lcom/google/maps/g/cu;

    goto :goto_0

    .line 201
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/cu;->c:Lcom/google/maps/g/cu;

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/cu;
    .locals 1

    .prologue
    .line 163
    const-class v0, Lcom/google/maps/g/cu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/cu;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/cu;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/google/maps/g/cu;->e:[Lcom/google/maps/g/cu;

    invoke-virtual {v0}, [Lcom/google/maps/g/cu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/cu;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/google/maps/g/cu;->d:I

    return v0
.end method
