.class public final enum Lcom/google/maps/g/a/dl;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/dl;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/dl;

.field public static final enum b:Lcom/google/maps/g/a/dl;

.field public static final enum c:Lcom/google/maps/g/a/dl;

.field public static final enum d:Lcom/google/maps/g/a/dl;

.field public static final enum e:Lcom/google/maps/g/a/dl;

.field public static final enum f:Lcom/google/maps/g/a/dl;

.field public static final enum g:Lcom/google/maps/g/a/dl;

.field public static final enum h:Lcom/google/maps/g/a/dl;

.field public static final enum i:Lcom/google/maps/g/a/dl;

.field public static final enum j:Lcom/google/maps/g/a/dl;

.field public static final enum k:Lcom/google/maps/g/a/dl;

.field public static final enum l:Lcom/google/maps/g/a/dl;

.field private static final synthetic n:[Lcom/google/maps/g/a/dl;


# instance fields
.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 280
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    .line 284
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "CONTINUE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->b:Lcom/google/maps/g/a/dl;

    .line 288
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "TOLL"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->c:Lcom/google/maps/g/a/dl;

    .line 292
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "PARTIAL_TOLL"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->d:Lcom/google/maps/g/a/dl;

    .line 296
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "SEASONAL_CLOSURE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->e:Lcom/google/maps/g/a/dl;

    .line 300
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "ROUNDABOUTS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->f:Lcom/google/maps/g/a/dl;

    .line 304
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "COUNTRY_BORDER"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->g:Lcom/google/maps/g/a/dl;

    .line 308
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "PROVINCE_BORDER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->h:Lcom/google/maps/g/a/dl;

    .line 312
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "SIDE_OF_ROAD"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->i:Lcom/google/maps/g/a/dl;

    .line 316
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "TOLL_ZONE_CROSSING"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->j:Lcom/google/maps/g/a/dl;

    .line 320
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "INCIDENT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->k:Lcom/google/maps/g/a/dl;

    .line 324
    new-instance v0, Lcom/google/maps/g/a/dl;

    const-string v1, "BETA"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/dl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    .line 275
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/maps/g/a/dl;

    sget-object v1, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/dl;->b:Lcom/google/maps/g/a/dl;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/dl;->c:Lcom/google/maps/g/a/dl;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/dl;->d:Lcom/google/maps/g/a/dl;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/dl;->e:Lcom/google/maps/g/a/dl;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/dl;->f:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/dl;->g:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/dl;->h:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/dl;->i:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/a/dl;->j:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/a/dl;->k:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/dl;->n:[Lcom/google/maps/g/a/dl;

    .line 404
    new-instance v0, Lcom/google/maps/g/a/dm;

    invoke-direct {v0}, Lcom/google/maps/g/a/dm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 413
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 414
    iput p3, p0, Lcom/google/maps/g/a/dl;->m:I

    .line 415
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/dl;
    .locals 1

    .prologue
    .line 382
    packed-switch p0, :pswitch_data_0

    .line 395
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 383
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 384
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/dl;->b:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 385
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/dl;->c:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 386
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/dl;->d:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 387
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/dl;->e:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 388
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/dl;->f:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 389
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/dl;->g:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 390
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/dl;->h:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 391
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/dl;->i:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 392
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/a/dl;->j:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 393
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/a/dl;->k:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 394
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    goto :goto_0

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/dl;
    .locals 1

    .prologue
    .line 275
    const-class v0, Lcom/google/maps/g/a/dl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dl;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/dl;
    .locals 1

    .prologue
    .line 275
    sget-object v0, Lcom/google/maps/g/a/dl;->n:[Lcom/google/maps/g/a/dl;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/dl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/dl;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 378
    iget v0, p0, Lcom/google/maps/g/a/dl;->m:I

    return v0
.end method
