.class public Lcom/google/maps/g/b/z;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/e/a/a/a/d;

.field public static final b:Lcom/google/e/a/a/a/d;

.field public static final c:Lcom/google/e/a/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x21b

    .line 26
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/z;->a:Lcom/google/e/a/a/a/d;

    .line 27
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/z;->b:Lcom/google/e/a/a/a/d;

    .line 28
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/z;->c:Lcom/google/e/a/a/a/d;

    .line 30
    sget-object v0, Lcom/google/maps/g/b/z;->a:Lcom/google/e/a/a/a/d;

    const/4 v1, 0x7

    sget-object v2, Lcom/google/geo/b/a/d;->a:Lcom/google/e/a/a/a/d;

    .line 31
    invoke-virtual {v0, v3, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x41e

    .line 34
    invoke-virtual {v0, v1, v5, v4}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x4

    .line 37
    invoke-virtual {v0, v1, v2, v4}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/google/maps/a/a/a;->d:Lcom/google/e/a/a/a/d;

    .line 40
    invoke-virtual {v0, v3, v6, v1}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/b/z;->b:Lcom/google/e/a/a/a/d;

    .line 43
    invoke-virtual {v0, v3, v7, v1}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/b/z;->c:Lcom/google/e/a/a/a/d;

    .line 46
    invoke-virtual {v0, v3, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x224

    const/4 v2, 0x5

    const-string v3, "unknown_client"

    .line 49
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 53
    sget-object v0, Lcom/google/maps/g/b/z;->b:Lcom/google/e/a/a/a/d;

    const/16 v1, 0x215

    const-wide/16 v2, 0x0

    .line 56
    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    .line 54
    invoke-virtual {v0, v1, v5, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x215

    const-wide/16 v2, 0xa

    .line 59
    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    .line 57
    invoke-virtual {v0, v1, v6, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x224

    .line 60
    invoke-virtual {v0, v1, v7, v4}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 64
    sget-object v0, Lcom/google/maps/g/b/z;->c:Lcom/google/e/a/a/a/d;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/maps/a/a/a;->d:Lcom/google/e/a/a/a/d;

    .line 65
    invoke-virtual {v0, v1, v5, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x218

    .line 68
    invoke-virtual {v0, v1, v6, v4}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 72
    return-void
.end method
