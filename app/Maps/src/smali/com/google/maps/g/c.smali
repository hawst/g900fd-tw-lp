.class public final Lcom/google/maps/g/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a;",
        "Lcom/google/maps/g/c;",
        ">;",
        "Lcom/google/maps/g/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/aq;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/google/n/aq;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/google/n/ao;

.field private o:Z

.field private p:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 792
    sget-object v0, Lcom/google/maps/g/a;->q:Lcom/google/maps/g/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1021
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->b:Lcom/google/n/ao;

    .line 1080
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->c:Lcom/google/n/ao;

    .line 1139
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c;->d:Ljava/lang/Object;

    .line 1215
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c;->e:Ljava/lang/Object;

    .line 1291
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    .line 1384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->g:Lcom/google/n/ao;

    .line 1443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->h:Lcom/google/n/ao;

    .line 1502
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->i:Lcom/google/n/ao;

    .line 1561
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->j:Lcom/google/n/ao;

    .line 1621
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    .line 1757
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    .line 1851
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    .line 1987
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c;->n:Lcom/google/n/ao;

    .line 793
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;
    .locals 7

    .prologue
    const/16 v6, 0x400

    const/16 v5, 0x200

    const/16 v4, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 920
    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1006
    :goto_0
    return-object p0

    .line 921
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_10

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 922
    iget-object v2, p0, Lcom/google/maps/g/c;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 923
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 925
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 926
    iget-object v2, p0, Lcom/google/maps/g/c;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 927
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 929
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 930
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 931
    iget-object v2, p1, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c;->d:Ljava/lang/Object;

    .line 934
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 935
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 936
    iget-object v2, p1, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c;->e:Ljava/lang/Object;

    .line 939
    :cond_4
    iget-object v2, p1, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 940
    iget-object v2, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 941
    iget-object v2, p1, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    .line 942
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 949
    :cond_5
    :goto_5
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v4, :cond_16

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 950
    iget-object v2, p0, Lcom/google/maps/g/c;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 951
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 953
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 954
    iget-object v2, p0, Lcom/google/maps/g/c;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 955
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 957
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 958
    iget-object v2, p0, Lcom/google/maps/g/c;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 959
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 961
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 962
    iget-object v2, p0, Lcom/google/maps/g/c;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 963
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 965
    :cond_9
    iget-object v2, p1, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 966
    iget-object v2, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 967
    iget-object v2, p1, Lcom/google/maps/g/a;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    .line 968
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 975
    :cond_a
    :goto_a
    iget-object v2, p1, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 976
    iget-object v2, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 977
    iget-object v2, p1, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    .line 978
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 985
    :cond_b
    :goto_b
    iget-object v2, p1, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 986
    iget-object v2, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 987
    iget-object v2, p1, Lcom/google/maps/g/a;->m:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    .line 988
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 995
    :cond_c
    :goto_c
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 996
    iget-object v2, p0, Lcom/google/maps/g/c;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 997
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 999
    :cond_d
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v2, v2, 0x200

    if-ne v2, v5, :cond_21

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 1000
    iget-boolean v2, p1, Lcom/google/maps/g/a;->o:Z

    iget v3, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/c;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/c;->o:Z

    .line 1002
    :cond_e
    iget v2, p1, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v2, v2, 0x400

    if-ne v2, v6, :cond_22

    :goto_f
    if-eqz v0, :cond_f

    .line 1003
    iget-boolean v0, p1, Lcom/google/maps/g/a;->p:Z

    iget v1, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/maps/g/c;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/c;->p:Z

    .line 1005
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v2, v1

    .line 921
    goto/16 :goto_1

    :cond_11
    move v2, v1

    .line 925
    goto/16 :goto_2

    :cond_12
    move v2, v1

    .line 929
    goto/16 :goto_3

    :cond_13
    move v2, v1

    .line 934
    goto/16 :goto_4

    .line 944
    :cond_14
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v4, :cond_15

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 945
    :cond_15
    iget-object v2, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_16
    move v2, v1

    .line 949
    goto/16 :goto_6

    :cond_17
    move v2, v1

    .line 953
    goto/16 :goto_7

    :cond_18
    move v2, v1

    .line 957
    goto/16 :goto_8

    :cond_19
    move v2, v1

    .line 961
    goto/16 :goto_9

    .line 970
    :cond_1a
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, 0x200

    if-eq v2, v5, :cond_1b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 971
    :cond_1b
    iget-object v2, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    .line 980
    :cond_1c
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, 0x400

    if-eq v2, v6, :cond_1d

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 981
    :cond_1d
    iget-object v2, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    .line 990
    :cond_1e
    iget v2, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-eq v2, v3, :cond_1f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/c;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/c;->a:I

    .line 991
    :cond_1f
    iget-object v2, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c

    :cond_20
    move v2, v1

    .line 995
    goto/16 :goto_d

    :cond_21
    move v2, v1

    .line 999
    goto/16 :goto_e

    :cond_22
    move v0, v1

    .line 1002
    goto/16 :goto_f
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 784
    invoke-virtual {p0}, Lcom/google/maps/g/c;->c()Lcom/google/maps/g/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 784
    check-cast p1, Lcom/google/maps/g/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1010
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1011
    iget-object v0, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1016
    :goto_1
    return v2

    .line 1010
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1016
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/a;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 834
    new-instance v2, Lcom/google/maps/g/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a;-><init>(Lcom/google/n/v;)V

    .line 835
    iget v3, p0, Lcom/google/maps/g/c;->a:I

    .line 837
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_e

    .line 840
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->b:Lcom/google/n/ao;

    .line 841
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->b:Lcom/google/n/ao;

    .line 842
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 840
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 843
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 844
    or-int/lit8 v0, v0, 0x2

    .line 846
    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->c:Lcom/google/n/ao;

    .line 847
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->c:Lcom/google/n/ao;

    .line 848
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 846
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 849
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 850
    or-int/lit8 v0, v0, 0x4

    .line 852
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/c;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    .line 853
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 854
    or-int/lit8 v0, v0, 0x8

    .line 856
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/c;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    .line 857
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 858
    iget-object v4, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    .line 859
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/maps/g/c;->a:I

    .line 861
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/c;->f:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 862
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 863
    or-int/lit8 v0, v0, 0x10

    .line 865
    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->g:Lcom/google/n/ao;

    .line 866
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->g:Lcom/google/n/ao;

    .line 867
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 865
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 868
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 869
    or-int/lit8 v0, v0, 0x20

    .line 871
    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->h:Lcom/google/n/ao;

    .line 872
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->h:Lcom/google/n/ao;

    .line 873
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 871
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 874
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 875
    or-int/lit8 v0, v0, 0x40

    .line 877
    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->i:Lcom/google/n/ao;

    .line 878
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->i:Lcom/google/n/ao;

    .line 879
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 877
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 880
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 881
    or-int/lit16 v0, v0, 0x80

    .line 883
    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->j:Lcom/google/n/ao;

    .line 884
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->j:Lcom/google/n/ao;

    .line 885
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 883
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 886
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 887
    iget-object v4, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    .line 888
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/maps/g/c;->a:I

    .line 890
    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/c;->k:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 891
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 892
    iget-object v4, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    .line 893
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/maps/g/c;->a:I

    .line 895
    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/c;->l:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 896
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 897
    iget-object v4, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    .line 898
    iget v4, p0, Lcom/google/maps/g/c;->a:I

    and-int/lit16 v4, v4, -0x801

    iput v4, p0, Lcom/google/maps/g/c;->a:I

    .line 900
    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/c;->m:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 901
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 902
    or-int/lit16 v0, v0, 0x100

    .line 904
    :cond_b
    iget-object v4, v2, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/c;->n:Lcom/google/n/ao;

    .line 905
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/c;->n:Lcom/google/n/ao;

    .line 906
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 904
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 907
    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    .line 908
    or-int/lit16 v0, v0, 0x200

    .line 910
    :cond_c
    iget-boolean v1, p0, Lcom/google/maps/g/c;->o:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a;->o:Z

    .line 911
    and-int/lit16 v1, v3, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_d

    .line 912
    or-int/lit16 v0, v0, 0x400

    .line 914
    :cond_d
    iget-boolean v1, p0, Lcom/google/maps/g/c;->p:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a;->p:Z

    .line 915
    iput v0, v2, Lcom/google/maps/g/a;->a:I

    .line 916
    return-object v2

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method
