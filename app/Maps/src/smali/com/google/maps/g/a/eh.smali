.class public final enum Lcom/google/maps/g/a/eh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/eh;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/eh;

.field public static final enum b:Lcom/google/maps/g/a/eh;

.field public static final enum c:Lcom/google/maps/g/a/eh;

.field public static final enum d:Lcom/google/maps/g/a/eh;

.field public static final enum e:Lcom/google/maps/g/a/eh;

.field public static final enum f:Lcom/google/maps/g/a/eh;

.field public static final enum g:Lcom/google/maps/g/a/eh;

.field public static final enum h:Lcom/google/maps/g/a/eh;

.field public static final enum i:Lcom/google/maps/g/a/eh;

.field public static final enum j:Lcom/google/maps/g/a/eh;

.field public static final enum k:Lcom/google/maps/g/a/eh;

.field public static final enum l:Lcom/google/maps/g/a/eh;

.field public static final enum m:Lcom/google/maps/g/a/eh;

.field public static final enum n:Lcom/google/maps/g/a/eh;

.field public static final enum o:Lcom/google/maps/g/a/eh;

.field public static final enum p:Lcom/google/maps/g/a/eh;

.field public static final enum q:Lcom/google/maps/g/a/eh;

.field public static final enum r:Lcom/google/maps/g/a/eh;

.field private static final synthetic t:[Lcom/google/maps/g/a/eh;


# instance fields
.field public final s:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 101
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "TRAVEL_MODE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    .line 105
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "RAW_TEXT"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->b:Lcom/google/maps/g/a/eh;

    .line 109
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "AGENCY"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->c:Lcom/google/maps/g/a/eh;

    .line 113
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "VEHICLE_TYPE"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->d:Lcom/google/maps/g/a/eh;

    .line 117
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "LINE"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    .line 121
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "TRANSIT_TRIP_NAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->f:Lcom/google/maps/g/a/eh;

    .line 125
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "HEADING"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->g:Lcom/google/maps/g/a/eh;

    .line 129
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "BLOCK_TRANSFER"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->h:Lcom/google/maps/g/a/eh;

    .line 133
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "TRANSIT_GROUP_SEPARATOR"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->i:Lcom/google/maps/g/a/eh;

    .line 137
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "ALTERNATE_LINE_SEPARATOR"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->j:Lcom/google/maps/g/a/eh;

    .line 141
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "EXPRESS_TYPE"

    const/16 v2, 0xa

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->k:Lcom/google/maps/g/a/eh;

    .line 145
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "DESTINATION"

    const/16 v2, 0xb

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->l:Lcom/google/maps/g/a/eh;

    .line 149
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "HEADSIGN"

    const/16 v2, 0xc

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->m:Lcom/google/maps/g/a/eh;

    .line 153
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "TIME"

    const/16 v2, 0xd

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->n:Lcom/google/maps/g/a/eh;

    .line 157
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "HIGHWAY"

    const/16 v2, 0xe

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->o:Lcom/google/maps/g/a/eh;

    .line 161
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "ROAD"

    const/16 v2, 0xf

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->p:Lcom/google/maps/g/a/eh;

    .line 165
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "VIA"

    const/16 v2, 0x10

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->q:Lcom/google/maps/g/a/eh;

    .line 169
    new-instance v0, Lcom/google/maps/g/a/eh;

    const-string v1, "TURN"

    const/16 v2, 0x11

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/eh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/eh;->r:Lcom/google/maps/g/a/eh;

    .line 96
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/maps/g/a/eh;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/a/eh;->b:Lcom/google/maps/g/a/eh;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/eh;->c:Lcom/google/maps/g/a/eh;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/eh;->d:Lcom/google/maps/g/a/eh;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/eh;->f:Lcom/google/maps/g/a/eh;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/eh;->g:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/eh;->h:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/eh;->i:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/a/eh;->j:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/a/eh;->k:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/a/eh;->l:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/a/eh;->m:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/a/eh;->n:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/a/eh;->o:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/a/eh;->p:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/g/a/eh;->q:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/g/a/eh;->r:Lcom/google/maps/g/a/eh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/eh;->t:[Lcom/google/maps/g/a/eh;

    .line 279
    new-instance v0, Lcom/google/maps/g/a/ei;

    invoke-direct {v0}, Lcom/google/maps/g/a/ei;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 288
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 289
    iput p3, p0, Lcom/google/maps/g/a/eh;->s:I

    .line 290
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/eh;
    .locals 1

    .prologue
    .line 251
    packed-switch p0, :pswitch_data_0

    .line 270
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 252
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 253
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/eh;->b:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 254
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/eh;->c:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 255
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/eh;->d:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 256
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 257
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/eh;->f:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 258
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/eh;->g:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 259
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/eh;->h:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 260
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/eh;->i:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 261
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/a/eh;->j:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 262
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/a/eh;->k:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 263
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/a/eh;->l:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 264
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/a/eh;->m:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 265
    :pswitch_d
    sget-object v0, Lcom/google/maps/g/a/eh;->n:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 266
    :pswitch_e
    sget-object v0, Lcom/google/maps/g/a/eh;->o:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 267
    :pswitch_f
    sget-object v0, Lcom/google/maps/g/a/eh;->p:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 268
    :pswitch_10
    sget-object v0, Lcom/google/maps/g/a/eh;->q:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 269
    :pswitch_11
    sget-object v0, Lcom/google/maps/g/a/eh;->r:Lcom/google/maps/g/a/eh;

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/eh;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/maps/g/a/eh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eh;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/eh;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/g/a/eh;->t:[Lcom/google/maps/g/a/eh;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/eh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/eh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/google/maps/g/a/eh;->s:I

    return v0
.end method
