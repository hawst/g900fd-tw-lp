.class public final Lcom/google/maps/g/g/o;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/g/k;",
        "Lcom/google/maps/g/g/o;",
        ">;",
        "Lcom/google/maps/g/g/t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 951
    sget-object v0, Lcom/google/maps/g/g/k;->j:Lcom/google/maps/g/g/k;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1064
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    .line 1137
    iput v1, p0, Lcom/google/maps/g/g/o;->c:I

    .line 1174
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    .line 1247
    iput v1, p0, Lcom/google/maps/g/g/o;->e:I

    .line 1283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g/o;->f:Lcom/google/n/ao;

    .line 1342
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    .line 952
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/g/k;)Lcom/google/maps/g/g/o;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1012
    invoke-static {}, Lcom/google/maps/g/g/k;->d()Lcom/google/maps/g/g/k;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1054
    :goto_0
    return-object p0

    .line 1013
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1014
    iget-object v2, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1015
    iget-object v2, p1, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    .line 1016
    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1023
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 1024
    iget v2, p1, Lcom/google/maps/g/g/k;->d:I

    invoke-static {v2}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1018
    :cond_3
    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1019
    :cond_4
    iget-object v2, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1023
    goto :goto_2

    .line 1024
    :cond_6
    iget v3, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/g/o;->a:I

    iget v2, v2, Lcom/google/maps/g/qb;->d:I

    iput v2, p0, Lcom/google/maps/g/g/o;->c:I

    .line 1026
    :cond_7
    iget-object v2, p1, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1027
    iget-object v2, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1028
    iget-object v2, p1, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    .line 1029
    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1036
    :cond_8
    :goto_3
    iget v2, p1, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_e

    .line 1037
    iget v2, p1, Lcom/google/maps/g/g/k;->g:I

    invoke-static {v2}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_9
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1031
    :cond_a
    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1032
    :cond_b
    iget-object v2, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_c
    move v2, v1

    .line 1036
    goto :goto_4

    .line 1037
    :cond_d
    iget v3, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/g/o;->a:I

    iget v2, v2, Lcom/google/maps/g/pz;->f:I

    iput v2, p0, Lcom/google/maps/g/g/o;->e:I

    .line 1039
    :cond_e
    iget v2, p1, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_11

    :goto_5
    if-eqz v0, :cond_f

    .line 1040
    iget-object v0, p0, Lcom/google/maps/g/g/o;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1041
    iget v0, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1043
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1044
    iget-object v0, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1045
    iget-object v0, p1, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    .line 1046
    iget v0, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1053
    :cond_10
    :goto_6
    iget-object v0, p1, Lcom/google/maps/g/g/k;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v0, v1

    .line 1039
    goto :goto_5

    .line 1048
    :cond_12
    iget v0, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_13

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/g/o;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/g/o;->a:I

    .line 1049
    :cond_13
    iget-object v0, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 943
    new-instance v2, Lcom/google/maps/g/g/k;

    invoke-direct {v2, p0}, Lcom/google/maps/g/g/k;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/g/o;->a:I

    iget v4, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/g/o;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/g/o;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    :goto_0
    iget v4, p0, Lcom/google/maps/g/g/o;->c:I

    iput v4, v2, Lcom/google/maps/g/g/k;->d:I

    iget v4, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/g/o;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/g/o;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v4, p0, Lcom/google/maps/g/g/o;->e:I

    iput v4, v2, Lcom/google/maps/g/g/k;->g:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/g/o;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/g/o;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/g/o;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/maps/g/g/o;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/g/o;->g:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/maps/g/g/k;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 943
    check-cast p1, Lcom/google/maps/g/g/k;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/g/o;->a(Lcom/google/maps/g/g/k;)Lcom/google/maps/g/g/o;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1058
    const/4 v0, 0x1

    return v0
.end method
