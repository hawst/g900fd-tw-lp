.class public final Lcom/google/maps/g/hg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hg;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/hg;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/google/maps/g/hh;

    invoke-direct {v0}, Lcom/google/maps/g/hh;-><init>()V

    sput-object v0, Lcom/google/maps/g/hg;->PARSER:Lcom/google/n/ax;

    .line 379
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/hg;->j:Lcom/google/n/aw;

    .line 929
    new-instance v0, Lcom/google/maps/g/hg;

    invoke-direct {v0}, Lcom/google/maps/g/hg;-><init>()V

    sput-object v0, Lcom/google/maps/g/hg;->g:Lcom/google/maps/g/hg;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 315
    iput-byte v0, p0, Lcom/google/maps/g/hg;->h:B

    .line 346
    iput v0, p0, Lcom/google/maps/g/hg;->i:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/hg;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    const/4 v0, 0x0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 50
    iget v4, p0, Lcom/google/maps/g/hg;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/hg;->a:I

    .line 51
    iput-object v3, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hg;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 56
    iget v4, p0, Lcom/google/maps/g/hg;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/hg;->a:I

    .line 57
    iput-object v3, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 82
    :catch_1
    move-exception v0

    .line 83
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 84
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 62
    iget v4, p0, Lcom/google/maps/g/hg;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/g/hg;->a:I

    .line 63
    iput-object v3, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 68
    iget v4, p0, Lcom/google/maps/g/hg;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/hg;->a:I

    .line 69
    iput-object v3, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    goto :goto_0

    .line 73
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 74
    iget v4, p0, Lcom/google/maps/g/hg;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/hg;->a:I

    .line 75
    iput-object v3, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 86
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->au:Lcom/google/n/bn;

    .line 87
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 315
    iput-byte v0, p0, Lcom/google/maps/g/hg;->h:B

    .line 346
    iput v0, p0, Lcom/google/maps/g/hg;->i:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;
    .locals 1

    .prologue
    .line 444
    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    return-object v0
.end method

.method public static j()Lcom/google/maps/g/hg;
    .locals 1

    .prologue
    .line 932
    sget-object v0, Lcom/google/maps/g/hg;->g:Lcom/google/maps/g/hg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/hi;
    .locals 1

    .prologue
    .line 441
    new-instance v0, Lcom/google/maps/g/hi;

    invoke-direct {v0}, Lcom/google/maps/g/hi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    sget-object v0, Lcom/google/maps/g/hg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 327
    invoke-virtual {p0}, Lcom/google/maps/g/hg;->c()I

    .line 328
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 331
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 332
    iget-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 334
    :cond_1
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 335
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 337
    :cond_2
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 338
    iget-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 340
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 341
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 343
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/hg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 344
    return-void

    .line 329
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 332
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 335
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 338
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 341
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 317
    iget-byte v1, p0, Lcom/google/maps/g/hg;->h:B

    .line 318
    if-ne v1, v0, :cond_0

    .line 322
    :goto_0
    return v0

    .line 319
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/hg;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 348
    iget v0, p0, Lcom/google/maps/g/hg;->i:I

    .line 349
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 374
    :goto_0
    return v0

    .line 352
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_a

    .line 354
    iget-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 356
    :goto_2
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    .line 358
    iget-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 360
    :cond_1
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 361
    const/4 v3, 0x3

    .line 362
    iget-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 364
    :cond_2
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 366
    iget-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 368
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 369
    const/4 v3, 0x5

    .line 370
    iget-object v0, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 372
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/hg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 373
    iput v0, p0, Lcom/google/maps/g/hg;->i:I

    goto/16 :goto_0

    .line 354
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 358
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 362
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 366
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 370
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    .line 118
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 119
    check-cast v0, Ljava/lang/String;

    .line 127
    :goto_0
    return-object v0

    .line 121
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 123
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iput-object v1, p0, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 127
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    .line 160
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    check-cast v0, Ljava/lang/String;

    .line 169
    :goto_0
    return-object v0

    .line 163
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 165
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iput-object v1, p0, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 169
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    .line 244
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 245
    check-cast v0, Ljava/lang/String;

    .line 253
    :goto_0
    return-object v0

    .line 247
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 249
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iput-object v1, p0, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 253
    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    .line 286
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 287
    check-cast v0, Ljava/lang/String;

    .line 295
    :goto_0
    return-object v0

    .line 289
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 291
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 292
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    iput-object v1, p0, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 295
    goto :goto_0
.end method
