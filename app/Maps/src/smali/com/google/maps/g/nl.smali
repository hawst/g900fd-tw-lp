.class public final Lcom/google/maps/g/nl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/nj;",
        "Lcom/google/maps/g/nl;",
        ">;",
        "Lcom/google/maps/g/nm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/f;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/google/maps/g/nj;->e:Lcom/google/maps/g/nj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 381
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nl;->b:Ljava/lang/Object;

    .line 457
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nl;->c:Lcom/google/n/f;

    .line 492
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nl;->d:Ljava/lang/Object;

    .line 320
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/nj;)Lcom/google/maps/g/nl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 357
    invoke-static {}, Lcom/google/maps/g/nj;->g()Lcom/google/maps/g/nj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 372
    :goto_0
    return-object p0

    .line 358
    :cond_0
    iget v2, p1, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 359
    iget v2, p0, Lcom/google/maps/g/nl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/nl;->a:I

    .line 360
    iget-object v2, p1, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/nl;->b:Ljava/lang/Object;

    .line 363
    :cond_1
    iget v2, p1, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 364
    iget-object v2, p1, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 358
    goto :goto_1

    :cond_3
    move v2, v1

    .line 363
    goto :goto_2

    .line 364
    :cond_4
    iget v3, p0, Lcom/google/maps/g/nl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/nl;->a:I

    iput-object v2, p0, Lcom/google/maps/g/nl;->c:Lcom/google/n/f;

    .line 366
    :cond_5
    iget v2, p1, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_6

    .line 367
    iget v0, p0, Lcom/google/maps/g/nl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/nl;->a:I

    .line 368
    iget-object v0, p1, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/nl;->d:Ljava/lang/Object;

    .line 371
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/nj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v0, v1

    .line 366
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 311
    new-instance v2, Lcom/google/maps/g/nj;

    invoke-direct {v2, p0}, Lcom/google/maps/g/nj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/nl;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/nl;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/nl;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/nl;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/nj;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 311
    check-cast p1, Lcom/google/maps/g/nj;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/nl;->a(Lcom/google/maps/g/nj;)Lcom/google/maps/g/nl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    return v0
.end method
