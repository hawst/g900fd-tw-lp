.class public final enum Lcom/google/maps/g/wq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/wq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/wq;

.field public static final enum b:Lcom/google/maps/g/wq;

.field public static final enum c:Lcom/google/maps/g/wq;

.field public static final enum d:Lcom/google/maps/g/wq;

.field public static final enum e:Lcom/google/maps/g/wq;

.field public static final enum f:Lcom/google/maps/g/wq;

.field public static final enum g:Lcom/google/maps/g/wq;

.field private static final synthetic i:[Lcom/google/maps/g/wq;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 89
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->a:Lcom/google/maps/g/wq;

    .line 93
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "REROUTE_FROM_NEW_LOCATION"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->b:Lcom/google/maps/g/wq;

    .line 97
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "REROUTE_AND_ALTERNATES_FROM_NEW_LOCATION"

    invoke-direct {v0, v1, v5, v8}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->c:Lcom/google/maps/g/wq;

    .line 101
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "TRAFFIC_UPDATE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->d:Lcom/google/maps/g/wq;

    .line 105
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "TRAFFIC_UPDATE_AND_BETTER_TRIP"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->e:Lcom/google/maps/g/wq;

    .line 109
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "TRAFFIC_UPDATE_AND_ALTERNATES_AND_BETTER_TRIP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->f:Lcom/google/maps/g/wq;

    .line 113
    new-instance v0, Lcom/google/maps/g/wq;

    const-string v1, "TRIP_UPDATE_AND_ALTERNATES"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/wq;->g:Lcom/google/maps/g/wq;

    .line 84
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/g/wq;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/g/wq;->a:Lcom/google/maps/g/wq;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/wq;->b:Lcom/google/maps/g/wq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/wq;->c:Lcom/google/maps/g/wq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/wq;->d:Lcom/google/maps/g/wq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/wq;->e:Lcom/google/maps/g/wq;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/wq;->f:Lcom/google/maps/g/wq;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/wq;->g:Lcom/google/maps/g/wq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/wq;->i:[Lcom/google/maps/g/wq;

    .line 168
    new-instance v0, Lcom/google/maps/g/wr;

    invoke-direct {v0}, Lcom/google/maps/g/wr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/google/maps/g/wq;->h:I

    .line 179
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/wq;
    .locals 1

    .prologue
    .line 151
    packed-switch p0, :pswitch_data_0

    .line 159
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/wq;->a:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 153
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/wq;->b:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 154
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/wq;->c:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 155
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/wq;->d:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 156
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/wq;->e:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 157
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/wq;->f:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 158
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/wq;->g:Lcom/google/maps/g/wq;

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/wq;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/google/maps/g/wq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wq;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/wq;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/g/wq;->i:[Lcom/google/maps/g/wq;

    invoke-virtual {v0}, [Lcom/google/maps/g/wq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/wq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/maps/g/wq;->h:I

    return v0
.end method
