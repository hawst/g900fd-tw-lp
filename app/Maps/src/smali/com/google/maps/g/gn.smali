.class public final Lcom/google/maps/g/gn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gn;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/gn;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field public c:I

.field public d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/maps/g/go;

    invoke-direct {v0}, Lcom/google/maps/g/go;-><init>()V

    sput-object v0, Lcom/google/maps/g/gn;->PARSER:Lcom/google/n/ax;

    .line 276
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/gn;->h:Lcom/google/n/aw;

    .line 564
    new-instance v0, Lcom/google/maps/g/gn;

    invoke-direct {v0}, Lcom/google/maps/g/gn;-><init>()V

    sput-object v0, Lcom/google/maps/g/gn;->e:Lcom/google/maps/g/gn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 226
    iput-byte v0, p0, Lcom/google/maps/g/gn;->f:B

    .line 251
    iput v0, p0, Lcom/google/maps/g/gn;->g:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/gn;->c:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/gn;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/gn;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 48
    iget v4, p0, Lcom/google/maps/g/gn;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/gn;->a:I

    .line 49
    iput-object v3, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/gn;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/gn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/gn;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/gn;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    .line 73
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 74
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 59
    invoke-static {v3}, Lcom/google/maps/g/gq;->a(I)Lcom/google/maps/g/gq;

    move-result-object v4

    .line 60
    if-nez v4, :cond_1

    .line 61
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 63
    :cond_1
    iget v4, p0, Lcom/google/maps/g/gn;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/gn;->a:I

    .line 64
    iput v3, p0, Lcom/google/maps/g/gn;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gn;->au:Lcom/google/n/bn;

    .line 77
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 226
    iput-byte v0, p0, Lcom/google/maps/g/gn;->f:B

    .line 251
    iput v0, p0, Lcom/google/maps/g/gn;->g:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/gn;
    .locals 1

    .prologue
    .line 567
    sget-object v0, Lcom/google/maps/g/gn;->e:Lcom/google/maps/g/gn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/gp;
    .locals 1

    .prologue
    .line 338
    new-instance v0, Lcom/google/maps/g/gp;

    invoke-direct {v0}, Lcom/google/maps/g/gp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/google/maps/g/gn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 238
    invoke-virtual {p0}, Lcom/google/maps/g/gn;->c()I

    .line 239
    iget v0, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 242
    :cond_0
    iget v0, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 243
    iget v0, p0, Lcom/google/maps/g/gn;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 245
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 246
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/gn;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 248
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/gn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 249
    return-void

    .line 240
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 243
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 246
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 228
    iget-byte v1, p0, Lcom/google/maps/g/gn;->f:B

    .line 229
    if-ne v1, v0, :cond_0

    .line 233
    :goto_0
    return v0

    .line 230
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 232
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/gn;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 253
    iget v0, p0, Lcom/google/maps/g/gn;->g:I

    .line 254
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 271
    :goto_0
    return v0

    .line 257
    :cond_0
    iget v0, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 259
    iget-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 261
    :goto_2
    iget v2, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 262
    iget v2, p0, Lcom/google/maps/g/gn;->c:I

    .line 263
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 265
    :cond_1
    iget v2, p0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    .line 266
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/g/gn;->d:I

    .line 267
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 269
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/gn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    iput v0, p0, Lcom/google/maps/g/gn;->g:I

    goto :goto_0

    .line 259
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_5
    move v2, v3

    .line 263
    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    .line 166
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 167
    check-cast v0, Ljava/lang/String;

    .line 175
    :goto_0
    return-object v0

    .line 169
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 171
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    iput-object v1, p0, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 175
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/gp;->a(Lcom/google/maps/g/gn;)Lcom/google/maps/g/gp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v0

    return-object v0
.end method
