.class public final Lcom/google/maps/g/rz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/sc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rz;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/maps/g/rz;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2424
    new-instance v0, Lcom/google/maps/g/sa;

    invoke-direct {v0}, Lcom/google/maps/g/sa;-><init>()V

    sput-object v0, Lcom/google/maps/g/rz;->PARSER:Lcom/google/n/ax;

    .line 2468
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/rz;->d:Lcom/google/n/aw;

    .line 2579
    new-instance v0, Lcom/google/maps/g/rz;

    invoke-direct {v0}, Lcom/google/maps/g/rz;-><init>()V

    sput-object v0, Lcom/google/maps/g/rz;->a:Lcom/google/maps/g/rz;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2388
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2439
    iput-byte v0, p0, Lcom/google/maps/g/rz;->b:B

    .line 2455
    iput v0, p0, Lcom/google/maps/g/rz;->c:I

    .line 2389
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2395
    invoke-direct {p0}, Lcom/google/maps/g/rz;-><init>()V

    .line 2397
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2399
    const/4 v0, 0x0

    .line 2400
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2401
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2402
    packed-switch v3, :pswitch_data_0

    .line 2407
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2409
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2405
    goto :goto_0

    .line 2421
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rz;->au:Lcom/google/n/bn;

    .line 2422
    return-void

    .line 2415
    :catch_0
    move-exception v0

    .line 2416
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2421
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rz;->au:Lcom/google/n/bn;

    throw v0

    .line 2417
    :catch_1
    move-exception v0

    .line 2418
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2419
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2402
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2386
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2439
    iput-byte v0, p0, Lcom/google/maps/g/rz;->b:B

    .line 2455
    iput v0, p0, Lcom/google/maps/g/rz;->c:I

    .line 2387
    return-void
.end method

.method public static a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;
    .locals 1

    .prologue
    .line 2533
    invoke-static {}, Lcom/google/maps/g/rz;->newBuilder()Lcom/google/maps/g/sb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/sb;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/rz;
    .locals 1

    .prologue
    .line 2582
    sget-object v0, Lcom/google/maps/g/rz;->a:Lcom/google/maps/g/rz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/sb;
    .locals 1

    .prologue
    .line 2530
    new-instance v0, Lcom/google/maps/g/sb;

    invoke-direct {v0}, Lcom/google/maps/g/sb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2436
    sget-object v0, Lcom/google/maps/g/rz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 2451
    iget v0, p0, Lcom/google/maps/g/rz;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2452
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/rz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2453
    return-void

    .line 2451
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/maps/g/rz;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2441
    iget-byte v1, p0, Lcom/google/maps/g/rz;->b:B

    .line 2442
    if-ne v1, v0, :cond_0

    .line 2446
    :goto_0
    return v0

    .line 2443
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2445
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/rz;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2457
    iget v0, p0, Lcom/google/maps/g/rz;->c:I

    .line 2458
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2463
    :goto_0
    return v0

    .line 2460
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2462
    iput v0, p0, Lcom/google/maps/g/rz;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2380
    invoke-static {}, Lcom/google/maps/g/rz;->newBuilder()Lcom/google/maps/g/sb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/sb;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2380
    invoke-static {}, Lcom/google/maps/g/rz;->newBuilder()Lcom/google/maps/g/sb;

    move-result-object v0

    return-object v0
.end method
