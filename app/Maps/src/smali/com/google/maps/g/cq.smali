.class public final Lcom/google/maps/g/cq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/cr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/co;",
        "Lcom/google/maps/g/cq;",
        ">;",
        "Lcom/google/maps/g/cr;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/google/maps/g/co;->h:Lcom/google/maps/g/co;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 348
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 403
    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 423
    :goto_0
    return-object p0

    .line 404
    :cond_0
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 405
    iget v2, p1, Lcom/google/maps/g/co;->b:I

    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/cq;->a:I

    iput v2, p0, Lcom/google/maps/g/cq;->b:I

    .line 407
    :cond_1
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 408
    iget v2, p1, Lcom/google/maps/g/co;->c:I

    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/cq;->a:I

    iput v2, p0, Lcom/google/maps/g/cq;->c:I

    .line 410
    :cond_2
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 411
    iget v2, p1, Lcom/google/maps/g/co;->d:I

    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/cq;->a:I

    iput v2, p0, Lcom/google/maps/g/cq;->d:I

    .line 413
    :cond_3
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 414
    iget v2, p1, Lcom/google/maps/g/co;->e:I

    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/cq;->a:I

    iput v2, p0, Lcom/google/maps/g/cq;->e:I

    .line 416
    :cond_4
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 417
    iget v2, p1, Lcom/google/maps/g/co;->f:I

    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/cq;->a:I

    iput v2, p0, Lcom/google/maps/g/cq;->f:I

    .line 419
    :cond_5
    iget v2, p1, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 420
    iget v0, p1, Lcom/google/maps/g/co;->g:I

    iget v1, p0, Lcom/google/maps/g/cq;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/maps/g/cq;->a:I

    iput v0, p0, Lcom/google/maps/g/cq;->g:I

    .line 422
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/co;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 404
    goto :goto_1

    :cond_8
    move v2, v1

    .line 407
    goto :goto_2

    :cond_9
    move v2, v1

    .line 410
    goto :goto_3

    :cond_a
    move v2, v1

    .line 413
    goto :goto_4

    :cond_b
    move v2, v1

    .line 416
    goto :goto_5

    :cond_c
    move v0, v1

    .line 419
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/maps/g/cq;->c()Lcom/google/maps/g/co;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 339
    check-cast p1, Lcom/google/maps/g/co;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/co;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 371
    new-instance v2, Lcom/google/maps/g/co;

    invoke-direct {v2, p0}, Lcom/google/maps/g/co;-><init>(Lcom/google/n/v;)V

    .line 372
    iget v3, p0, Lcom/google/maps/g/cq;->a:I

    .line 373
    const/4 v1, 0x0

    .line 374
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 377
    :goto_0
    iget v1, p0, Lcom/google/maps/g/cq;->b:I

    iput v1, v2, Lcom/google/maps/g/co;->b:I

    .line 378
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 379
    or-int/lit8 v0, v0, 0x2

    .line 381
    :cond_0
    iget v1, p0, Lcom/google/maps/g/cq;->c:I

    iput v1, v2, Lcom/google/maps/g/co;->c:I

    .line 382
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 383
    or-int/lit8 v0, v0, 0x4

    .line 385
    :cond_1
    iget v1, p0, Lcom/google/maps/g/cq;->d:I

    iput v1, v2, Lcom/google/maps/g/co;->d:I

    .line 386
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 387
    or-int/lit8 v0, v0, 0x8

    .line 389
    :cond_2
    iget v1, p0, Lcom/google/maps/g/cq;->e:I

    iput v1, v2, Lcom/google/maps/g/co;->e:I

    .line 390
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 391
    or-int/lit8 v0, v0, 0x10

    .line 393
    :cond_3
    iget v1, p0, Lcom/google/maps/g/cq;->f:I

    iput v1, v2, Lcom/google/maps/g/co;->f:I

    .line 394
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 395
    or-int/lit8 v0, v0, 0x20

    .line 397
    :cond_4
    iget v1, p0, Lcom/google/maps/g/cq;->g:I

    iput v1, v2, Lcom/google/maps/g/co;->g:I

    .line 398
    iput v0, v2, Lcom/google/maps/g/co;->a:I

    .line 399
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method
