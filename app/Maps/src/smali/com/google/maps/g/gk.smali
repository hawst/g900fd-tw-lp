.class public final Lcom/google/maps/g/gk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gk;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/maps/g/gk;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field j:Z

.field k:Ljava/lang/Object;

.field l:Ljava/lang/Object;

.field m:Ljava/lang/Object;

.field n:Ljava/lang/Object;

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field p:Lcom/google/n/ao;

.field q:Ljava/lang/Object;

.field r:Ljava/lang/Object;

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/google/maps/g/gl;

    invoke-direct {v0}, Lcom/google/maps/g/gl;-><init>()V

    sput-object v0, Lcom/google/maps/g/gk;->PARSER:Lcom/google/n/ax;

    .line 977
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/gk;->v:Lcom/google/n/aw;

    .line 2560
    new-instance v0, Lcom/google/maps/g/gk;

    invoke-direct {v0}, Lcom/google/maps/g/gk;-><init>()V

    sput-object v0, Lcom/google/maps/g/gk;->s:Lcom/google/maps/g/gk;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 320
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    .line 730
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    .line 829
    iput-byte v2, p0, Lcom/google/maps/g/gk;->t:B

    .line 896
    iput v2, p0, Lcom/google/maps/g/gk;->u:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/gk;->j:Z

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    .line 32
    iget-object v0, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x2000

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/maps/g/gk;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 47
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 48
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 54
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 56
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 52
    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 63
    iput-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x2000

    if-ne v1, v10, :cond_1

    .line 172
    iget-object v1, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    .line 174
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/gk;->au:Lcom/google/n/bn;

    throw v0

    .line 67
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 68
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 69
    iput-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 167
    :catch_1
    move-exception v0

    .line 168
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 169
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 74
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 75
    iput-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    goto :goto_0

    .line 79
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 81
    iput-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    goto :goto_0

    .line 85
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/gk;->a:I

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/gk;->j:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 90
    :sswitch_6
    and-int/lit16 v0, v1, 0x2000

    if-eq v0, v10, :cond_3

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    .line 93
    or-int/lit16 v1, v1, 0x2000

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 95
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 100
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 101
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 102
    iput-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 106
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 107
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/gk;->a:I

    goto/16 :goto_0

    .line 111
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 113
    iput-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 117
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 118
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 119
    iput-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 123
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 125
    iput-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 129
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 130
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 131
    iput-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 135
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 136
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 137
    iput-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 141
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 142
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/gk;->a:I

    goto/16 :goto_0

    .line 146
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 147
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 148
    iput-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 152
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 153
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 154
    iput-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 158
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 159
    iget v6, p0, Lcom/google/maps/g/gk;->a:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/maps/g/gk;->a:I

    .line 160
    iput-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 171
    :cond_4
    and-int/lit16 v0, v1, 0x2000

    if-ne v0, v10, :cond_5

    .line 172
    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    .line 174
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->au:Lcom/google/n/bn;

    .line 175
    return-void

    .line 49
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 320
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    .line 730
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    .line 829
    iput-byte v1, p0, Lcom/google/maps/g/gk;->t:B

    .line 896
    iput v1, p0, Lcom/google/maps/g/gk;->u:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/gk;
    .locals 1

    .prologue
    .line 2563
    sget-object v0, Lcom/google/maps/g/gk;->s:Lcom/google/maps/g/gk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/gm;
    .locals 1

    .prologue
    .line 1039
    new-instance v0, Lcom/google/maps/g/gm;

    invoke-direct {v0}, Lcom/google/maps/g/gm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    sget-object v0, Lcom/google/maps/g/gk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 841
    invoke-virtual {p0}, Lcom/google/maps/g/gk;->c()I

    .line 842
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    .line 843
    iget-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 845
    :cond_0
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1

    .line 846
    iget-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 848
    :cond_1
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    .line 849
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 851
    :cond_2
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 852
    iget-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 854
    :cond_3
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_4

    .line 855
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/gk;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_9

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 857
    :cond_4
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 858
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 857
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 843
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 846
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 849
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 852
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_9
    move v0, v2

    .line 855
    goto :goto_4

    .line 860
    :cond_a
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_b

    .line 861
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 863
    :cond_b
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_c

    .line 864
    iget-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 866
    :cond_c
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 867
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 869
    :cond_d
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_e

    .line 870
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    :goto_8
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 872
    :cond_e
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_f

    .line 873
    const/16 v2, 0xb

    iget-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    :goto_9
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 875
    :cond_f
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_10

    .line 876
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 878
    :cond_10
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_11

    .line 879
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    :goto_b
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 881
    :cond_11
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_12

    .line 882
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 884
    :cond_12
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_13

    .line 885
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 887
    :cond_13
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_14

    .line 888
    const/16 v1, 0x10

    iget-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    :goto_d
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 890
    :cond_14
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_15

    .line 891
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    :goto_e
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 893
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/gk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 894
    return-void

    .line 861
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 867
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 870
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 873
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 876
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 879
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_b

    .line 885
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    .line 888
    :cond_1d
    check-cast v0, Lcom/google/n/f;

    goto :goto_d

    .line 891
    :cond_1e
    check-cast v0, Lcom/google/n/f;

    goto :goto_e
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 831
    iget-byte v1, p0, Lcom/google/maps/g/gk;->t:B

    .line 832
    if-ne v1, v0, :cond_0

    .line 836
    :goto_0
    return v0

    .line 833
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 835
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/gk;->t:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 898
    iget v0, p0, Lcom/google/maps/g/gk;->u:I

    .line 899
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 972
    :goto_0
    return v0

    .line 902
    :cond_0
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1e

    .line 904
    iget-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 906
    :goto_2
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1

    .line 908
    iget-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 910
    :cond_1
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    .line 911
    const/4 v3, 0x3

    .line 912
    iget-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 914
    :cond_2
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 916
    iget-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 918
    :cond_3
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_4

    .line 919
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/gk;->j:Z

    .line 920
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_4
    move v3, v1

    move v1, v2

    .line 922
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 923
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    .line 924
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 922
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 904
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 908
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 912
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 916
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 926
    :cond_9
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 927
    const/4 v1, 0x7

    .line 928
    iget-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 930
    :cond_a
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_b

    .line 931
    iget-object v0, p0, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    .line 932
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 934
    :cond_b
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 935
    const/16 v1, 0x9

    .line 936
    iget-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 938
    :cond_c
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 939
    const/16 v1, 0xa

    .line 940
    iget-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 942
    :cond_d
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_e

    .line 943
    const/16 v1, 0xb

    .line 944
    iget-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 946
    :cond_e
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_f

    .line 947
    const/16 v1, 0xc

    .line 948
    iget-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    :goto_b
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 950
    :cond_f
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_10

    .line 951
    const/16 v1, 0xd

    .line 952
    iget-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 954
    :cond_10
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_11

    .line 955
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    .line 956
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 958
    :cond_11
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_12

    .line 959
    const/16 v1, 0xf

    .line 960
    iget-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    :goto_d
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 962
    :cond_12
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_13

    .line 963
    const/16 v1, 0x10

    .line 964
    iget-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    :goto_e
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 966
    :cond_13
    iget v0, p0, Lcom/google/maps/g/gk;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_14

    .line 967
    const/16 v1, 0x11

    .line 968
    iget-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    :goto_f
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 970
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/gk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 971
    iput v0, p0, Lcom/google/maps/g/gk;->u:I

    goto/16 :goto_0

    .line 928
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 936
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 940
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 944
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 948
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_b

    .line 952
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    .line 960
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_d

    .line 964
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto :goto_e

    .line 968
    :cond_1d
    check-cast v0, Lcom/google/n/f;

    goto :goto_f

    :cond_1e
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gk;->newBuilder()Lcom/google/maps/g/gm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/gm;->a(Lcom/google/maps/g/gk;)Lcom/google/maps/g/gm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gk;->newBuilder()Lcom/google/maps/g/gm;

    move-result-object v0

    return-object v0
.end method
