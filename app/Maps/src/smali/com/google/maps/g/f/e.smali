.class public final enum Lcom/google/maps/g/f/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/f/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/f/e;

.field public static final enum b:Lcom/google/maps/g/f/e;

.field public static final enum c:Lcom/google/maps/g/f/e;

.field public static final enum d:Lcom/google/maps/g/f/e;

.field public static final enum e:Lcom/google/maps/g/f/e;

.field private static final synthetic g:[Lcom/google/maps/g/f/e;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/f/e;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/f/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    .line 18
    new-instance v0, Lcom/google/maps/g/f/e;

    const-string v1, "PANO"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/f/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/e;->b:Lcom/google/maps/g/f/e;

    .line 22
    new-instance v0, Lcom/google/maps/g/f/e;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/f/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/e;->c:Lcom/google/maps/g/f/e;

    .line 26
    new-instance v0, Lcom/google/maps/g/f/e;

    const-string v1, "SATELLITE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/f/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    .line 30
    new-instance v0, Lcom/google/maps/g/f/e;

    const-string v1, "TOUR"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/f/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/e;->e:Lcom/google/maps/g/f/e;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/f/e;

    sget-object v1, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/f/e;->b:Lcom/google/maps/g/f/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/f/e;->c:Lcom/google/maps/g/f/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/f/e;->e:Lcom/google/maps/g/f/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/f/e;->g:[Lcom/google/maps/g/f/e;

    .line 75
    new-instance v0, Lcom/google/maps/g/f/f;

    invoke-direct {v0}, Lcom/google/maps/g/f/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput p3, p0, Lcom/google/maps/g/f/e;->f:I

    .line 86
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/f/e;
    .locals 1

    .prologue
    .line 60
    packed-switch p0, :pswitch_data_0

    .line 66
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    goto :goto_0

    .line 62
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/f/e;->b:Lcom/google/maps/g/f/e;

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/f/e;->c:Lcom/google/maps/g/f/e;

    goto :goto_0

    .line 64
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    goto :goto_0

    .line 65
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/f/e;->e:Lcom/google/maps/g/f/e;

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/f/e;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/f/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/f/e;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/f/e;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/f/e;->g:[Lcom/google/maps/g/f/e;

    invoke-virtual {v0}, [Lcom/google/maps/g/f/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/f/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/maps/g/f/e;->f:I

    return v0
.end method
