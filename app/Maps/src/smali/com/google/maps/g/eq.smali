.class public final Lcom/google/maps/g/eq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/et;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/eq;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/eq;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/maps/g/er;

    invoke-direct {v0}, Lcom/google/maps/g/er;-><init>()V

    sput-object v0, Lcom/google/maps/g/eq;->PARSER:Lcom/google/n/ax;

    .line 192
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/eq;->g:Lcom/google/n/aw;

    .line 532
    new-instance v0, Lcom/google/maps/g/eq;

    invoke-direct {v0}, Lcom/google/maps/g/eq;-><init>()V

    sput-object v0, Lcom/google/maps/g/eq;->d:Lcom/google/maps/g/eq;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 91
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    .line 149
    iput-byte v2, p0, Lcom/google/maps/g/eq;->e:B

    .line 171
    iput v2, p0, Lcom/google/maps/g/eq;->f:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/eq;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 34
    sparse-switch v4, :sswitch_data_0

    .line 39
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 41
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 47
    iget v4, p0, Lcom/google/maps/g/eq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/eq;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 62
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 63
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 69
    iget-object v1, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    .line 71
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/eq;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_2

    .line 52
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    .line 54
    or-int/lit8 v0, v0, 0x2

    .line 56
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 56
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 64
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 65
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 66
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_4

    .line 69
    iget-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    .line 71
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eq;->au:Lcom/google/n/bn;

    .line 72
    return-void

    .line 68
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 91
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    .line 149
    iput-byte v1, p0, Lcom/google/maps/g/eq;->e:B

    .line 171
    iput v1, p0, Lcom/google/maps/g/eq;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/eq;
    .locals 1

    .prologue
    .line 535
    sget-object v0, Lcom/google/maps/g/eq;->d:Lcom/google/maps/g/eq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/es;
    .locals 1

    .prologue
    .line 254
    new-instance v0, Lcom/google/maps/g/es;

    invoke-direct {v0}, Lcom/google/maps/g/es;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/eq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/google/maps/g/eq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 161
    invoke-virtual {p0}, Lcom/google/maps/g/eq;->c()I

    .line 162
    iget v0, p0, Lcom/google/maps/g/eq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 165
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 165
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/eq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 169
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 151
    iget-byte v1, p0, Lcom/google/maps/g/eq;->e:B

    .line 152
    if-ne v1, v0, :cond_0

    .line 156
    :goto_0
    return v0

    .line 153
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/eq;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 173
    iget v0, p0, Lcom/google/maps/g/eq;->f:I

    .line 174
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 187
    :goto_0
    return v0

    .line 177
    :cond_0
    iget v0, p0, Lcom/google/maps/g/eq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 178
    iget-object v0, p0, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    .line 179
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 181
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 182
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    .line 183
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 181
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/eq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 186
    iput v0, p0, Lcom/google/maps/g/eq;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/eq;->newBuilder()Lcom/google/maps/g/es;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/es;->a(Lcom/google/maps/g/eq;)Lcom/google/maps/g/es;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/eq;->newBuilder()Lcom/google/maps/g/es;

    move-result-object v0

    return-object v0
.end method
