.class public final Lcom/google/maps/g/xk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/xi;",
        "Lcom/google/maps/g/xk;",
        ">;",
        "Lcom/google/maps/g/xn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/google/maps/g/xi;->d:Lcom/google/maps/g/xi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 394
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    .line 530
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/xk;->c:I

    .line 337
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/xi;)Lcom/google/maps/g/xk;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 369
    invoke-static {}, Lcom/google/maps/g/xi;->g()Lcom/google/maps/g/xi;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 384
    :goto_0
    return-object p0

    .line 370
    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/xi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 372
    iget-object v1, p1, Lcom/google/maps/g/xi;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    .line 373
    iget v1, p0, Lcom/google/maps/g/xk;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/xk;->a:I

    .line 380
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/maps/g/xi;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_7

    .line 381
    iget v0, p1, Lcom/google/maps/g/xi;->c:I

    invoke-static {v0}, Lcom/google/maps/g/xl;->a(I)Lcom/google/maps/g/xl;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/xl;->a:Lcom/google/maps/g/xl;

    :cond_2
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 375
    :cond_3
    iget v1, p0, Lcom/google/maps/g/xk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/xk;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/xk;->a:I

    .line 376
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/xi;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 380
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 381
    :cond_6
    iget v1, p0, Lcom/google/maps/g/xk;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/xk;->a:I

    iget v0, v0, Lcom/google/maps/g/xl;->c:I

    iput v0, p0, Lcom/google/maps/g/xk;->c:I

    .line 383
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/xi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 328
    new-instance v2, Lcom/google/maps/g/xi;

    invoke-direct {v2, p0}, Lcom/google/maps/g/xi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/xk;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/xk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/xk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/xk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/xk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/xi;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/xk;->c:I

    iput v1, v2, Lcom/google/maps/g/xi;->c:I

    iput v0, v2, Lcom/google/maps/g/xi;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 328
    check-cast p1, Lcom/google/maps/g/xi;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/xk;->a(Lcom/google/maps/g/xi;)Lcom/google/maps/g/xk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x1

    return v0
.end method
