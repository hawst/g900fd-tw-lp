.class public final Lcom/google/maps/g/hy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ib;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hy;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/hy;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/ao;

.field public f:I

.field g:I

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field l:Lcom/google/n/ao;

.field m:I

.field n:Ljava/lang/Object;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcom/google/maps/g/hz;

    invoke-direct {v0}, Lcom/google/maps/g/hz;-><init>()V

    sput-object v0, Lcom/google/maps/g/hy;->PARSER:Lcom/google/n/ax;

    .line 702
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/hy;->r:Lcom/google/n/aw;

    .line 1823
    new-instance v0, Lcom/google/maps/g/hy;

    invoke-direct {v0}, Lcom/google/maps/g/hy;-><init>()V

    sput-object v0, Lcom/google/maps/g/hy;->o:Lcom/google/maps/g/hy;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 307
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    .line 497
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    .line 570
    iput-byte v3, p0, Lcom/google/maps/g/hy;->p:B

    .line 637
    iput v3, p0, Lcom/google/maps/g/hy;->q:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput v2, p0, Lcom/google/maps/g/hy;->f:I

    .line 23
    iput v2, p0, Lcom/google/maps/g/hy;->g:I

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    .line 25
    iput-boolean v2, p0, Lcom/google/maps/g/hy;->i:Z

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    .line 28
    iget-object v0, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iput v2, p0, Lcom/google/maps/g/hy;->m:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x40

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/maps/g/hy;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 43
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 52
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 58
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 59
    iput-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x40

    if-ne v1, v10, :cond_1

    .line 159
    iget-object v1, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    .line 161
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hy;->au:Lcom/google/n/bn;

    throw v0

    .line 63
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 65
    iput-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 154
    :catch_1
    move-exception v0

    .line 155
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 156
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 69
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/hy;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/hy;->i:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 74
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 75
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/hy;->a:I

    goto :goto_0

    .line 79
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 81
    iput-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 85
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 91
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 92
    invoke-static {v0}, Lcom/google/maps/g/ge;->a(I)Lcom/google/maps/g/ge;

    move-result-object v6

    .line 93
    if-nez v6, :cond_3

    .line 94
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 96
    :cond_3
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 97
    iput v0, p0, Lcom/google/maps/g/hy;->m:I

    goto/16 :goto_0

    .line 102
    :sswitch_8
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v10, :cond_4

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    .line 105
    or-int/lit8 v1, v1, 0x40

    .line 107
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 107
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 112
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 113
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 114
    iput-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 118
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 119
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 120
    iput-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 124
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/hy;->a:I

    goto/16 :goto_0

    .line 129
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 130
    invoke-static {v0}, Lcom/google/b/f/cj;->a(I)Lcom/google/b/f/cj;

    move-result-object v6

    .line 131
    if-nez v6, :cond_5

    .line 132
    const/16 v6, 0xc

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 134
    :cond_5
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 135
    iput v0, p0, Lcom/google/maps/g/hy;->f:I

    goto/16 :goto_0

    .line 140
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 141
    invoke-static {v0}, Lcom/google/b/f/ch;->a(I)Lcom/google/b/f/ch;

    move-result-object v6

    .line 142
    if-nez v6, :cond_6

    .line 143
    const/16 v6, 0xd

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 145
    :cond_6
    iget v6, p0, Lcom/google/maps/g/hy;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/hy;->a:I

    .line 146
    iput v0, p0, Lcom/google/maps/g/hy;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 158
    :cond_7
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v10, :cond_8

    .line 159
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    .line 161
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->au:Lcom/google/n/bn;

    .line 162
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 307
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    .line 497
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    .line 570
    iput-byte v1, p0, Lcom/google/maps/g/hy;->p:B

    .line 637
    iput v1, p0, Lcom/google/maps/g/hy;->q:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;
    .locals 1

    .prologue
    .line 767
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ia;->a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/hy;
    .locals 1

    .prologue
    .line 1826
    sget-object v0, Lcom/google/maps/g/hy;->o:Lcom/google/maps/g/hy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ia;
    .locals 1

    .prologue
    .line 764
    new-instance v0, Lcom/google/maps/g/ia;

    invoke-direct {v0}, Lcom/google/maps/g/ia;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    sget-object v0, Lcom/google/maps/g/hy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 594
    invoke-virtual {p0}, Lcom/google/maps/g/hy;->c()I

    .line 595
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_0

    .line 596
    iget-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 598
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 599
    iget-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 601
    :cond_1
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    .line 602
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/hy;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 604
    :cond_2
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 605
    iget-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 607
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    .line 608
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 610
    :cond_4
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_5

    .line 611
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 613
    :cond_5
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_6

    .line 614
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/maps/g/hy;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_c

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    :cond_6
    :goto_5
    move v3, v2

    .line 616
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_d

    .line 617
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 616
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 596
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 599
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 602
    goto/16 :goto_2

    .line 608
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 611
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 614
    :cond_c
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 619
    :cond_d
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_e

    .line 620
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 622
    :cond_e
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 623
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 625
    :cond_f
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_10

    .line 626
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 628
    :cond_10
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_11

    .line 629
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/maps/g/hy;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_15

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 631
    :cond_11
    :goto_9
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_12

    .line 632
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/g/hy;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_16

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 634
    :cond_12
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/hy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 635
    return-void

    .line 620
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 623
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 629
    :cond_15
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    .line 632
    :cond_16
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 572
    iget-byte v0, p0, Lcom/google/maps/g/hy;->p:B

    .line 573
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 589
    :cond_0
    :goto_0
    return v2

    .line 574
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 576
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 577
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 578
    iput-byte v2, p0, Lcom/google/maps/g/hy;->p:B

    goto :goto_0

    .line 576
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 582
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 583
    iget-object v0, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bf;->d()Lcom/google/b/f/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bf;

    invoke-virtual {v0}, Lcom/google/b/f/bf;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 584
    iput-byte v2, p0, Lcom/google/maps/g/hy;->p:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 582
    goto :goto_2

    .line 588
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/g/hy;->p:B

    move v2, v3

    .line 589
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 639
    iget v0, p0, Lcom/google/maps/g/hy;->q:I

    .line 640
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 697
    :goto_0
    return v0

    .line 643
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_16

    .line 645
    iget-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 647
    :goto_2
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 649
    iget-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 651
    :cond_1
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_2

    .line 652
    const/4 v0, 0x3

    iget-boolean v4, p0, Lcom/google/maps/g/hy;->i:Z

    .line 653
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 655
    :cond_2
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 656
    iget-object v0, p0, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    .line 657
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 659
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_4

    .line 660
    const/4 v4, 0x5

    .line 661
    iget-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 663
    :cond_4
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_5

    .line 664
    const/4 v4, 0x6

    .line 665
    iget-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 667
    :cond_5
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_6

    .line 668
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/maps/g/hy;->m:I

    .line 669
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    :cond_6
    move v4, v1

    move v1, v2

    .line 671
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 672
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    .line 673
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 671
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 645
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 649
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 661
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 665
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_b
    move v0, v3

    .line 669
    goto :goto_6

    .line 675
    :cond_c
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_d

    .line 676
    const/16 v1, 0x9

    .line 677
    iget-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 679
    :cond_d
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_e

    .line 681
    iget-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 683
    :cond_e
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 684
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    .line 685
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 687
    :cond_f
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_10

    .line 688
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/maps/g/hy;->f:I

    .line 689
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_15

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 691
    :cond_10
    iget v0, p0, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_12

    .line 692
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/g/hy;->g:I

    .line 693
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_11

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_11
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 695
    :cond_12
    iget-object v0, p0, Lcom/google/maps/g/hy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 696
    iput v0, p0, Lcom/google/maps/g/hy;->q:I

    goto/16 :goto_0

    .line 677
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 681
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_15
    move v0, v3

    .line 689
    goto :goto_a

    :cond_16
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ia;->a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    return-object v0
.end method
