.class public final Lcom/google/maps/g/us;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/uv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/us;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/us;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lcom/google/maps/g/ut;

    invoke-direct {v0}, Lcom/google/maps/g/ut;-><init>()V

    sput-object v0, Lcom/google/maps/g/us;->PARSER:Lcom/google/n/ax;

    .line 331
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/us;->h:Lcom/google/n/aw;

    .line 615
    new-instance v0, Lcom/google/maps/g/us;

    invoke-direct {v0}, Lcom/google/maps/g/us;-><init>()V

    sput-object v0, Lcom/google/maps/g/us;->e:Lcom/google/maps/g/us;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 137
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 281
    iput-byte v0, p0, Lcom/google/maps/g/us;->f:B

    .line 306
    iput v0, p0, Lcom/google/maps/g/us;->g:I

    .line 138
    iput v1, p0, Lcom/google/maps/g/us;->b:I

    .line 139
    iput-boolean v1, p0, Lcom/google/maps/g/us;->c:Z

    .line 140
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    .line 141
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 147
    invoke-direct {p0}, Lcom/google/maps/g/us;-><init>()V

    .line 148
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 153
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 154
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 155
    sparse-switch v0, :sswitch_data_0

    .line 160
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 162
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 158
    goto :goto_0

    .line 167
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/us;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/us;->a:I

    .line 168
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/us;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/us;->au:Lcom/google/n/bn;

    throw v0

    .line 172
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/us;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/us;->a:I

    .line 173
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/us;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 186
    :catch_1
    move-exception v0

    .line 187
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 188
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 173
    goto :goto_1

    .line 177
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 178
    iget v5, p0, Lcom/google/maps/g/us;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/us;->a:I

    .line 179
    iput-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/us;->au:Lcom/google/n/bn;

    .line 191
    return-void

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 135
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 281
    iput-byte v0, p0, Lcom/google/maps/g/us;->f:B

    .line 306
    iput v0, p0, Lcom/google/maps/g/us;->g:I

    .line 136
    return-void
.end method

.method public static d()Lcom/google/maps/g/us;
    .locals 1

    .prologue
    .line 618
    sget-object v0, Lcom/google/maps/g/us;->e:Lcom/google/maps/g/us;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/uu;
    .locals 1

    .prologue
    .line 393
    new-instance v0, Lcom/google/maps/g/uu;

    invoke-direct {v0}, Lcom/google/maps/g/uu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/us;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lcom/google/maps/g/us;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    invoke-virtual {p0}, Lcom/google/maps/g/us;->c()I

    .line 294
    iget v2, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 295
    iget v2, p0, Lcom/google/maps/g/us;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 297
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 298
    iget-boolean v2, p0, Lcom/google/maps/g/us;->c:Z

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_4

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 300
    :cond_1
    iget v0, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 301
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/us;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 304
    return-void

    .line 295
    :cond_3
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 298
    goto :goto_1

    .line 301
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 283
    iget-byte v1, p0, Lcom/google/maps/g/us;->f:B

    .line 284
    if-ne v1, v0, :cond_0

    .line 288
    :goto_0
    return v0

    .line 285
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 287
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/us;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 308
    iget v0, p0, Lcom/google/maps/g/us;->g:I

    .line 309
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 326
    :goto_0
    return v0

    .line 312
    :cond_0
    iget v0, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 313
    iget v0, p0, Lcom/google/maps/g/us;->b:I

    .line 314
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 316
    :goto_2
    iget v2, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_4

    .line 317
    iget-boolean v2, p0, Lcom/google/maps/g/us;->c:Z

    .line 318
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 320
    :goto_3
    iget v0, p0, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 321
    const/4 v3, 0x3

    .line 322
    iget-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/us;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 325
    iput v0, p0, Lcom/google/maps/g/us;->g:I

    goto :goto_0

    .line 314
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 322
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_4
    move v2, v0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/google/maps/g/us;->newBuilder()Lcom/google/maps/g/uu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/uu;->a(Lcom/google/maps/g/us;)Lcom/google/maps/g/uu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/google/maps/g/us;->newBuilder()Lcom/google/maps/g/uu;

    move-result-object v0

    return-object v0
.end method
