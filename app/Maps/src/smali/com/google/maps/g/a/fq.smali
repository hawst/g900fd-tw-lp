.class public final Lcom/google/maps/g/a/fq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ft;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/fo;",
        "Lcom/google/maps/g/a/fq;",
        ">;",
        "Lcom/google/maps/g/a/ft;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/google/maps/g/a/fo;->f:Lcom/google/maps/g/a/fo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 450
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/fq;->c:Ljava/lang/Object;

    .line 526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/fq;->d:Ljava/lang/Object;

    .line 348
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/fo;)Lcom/google/maps/g/a/fq;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 391
    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 409
    :goto_0
    return-object p0

    .line 392
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 393
    iget-wide v2, p1, Lcom/google/maps/g/a/fo;->b:J

    iget v4, p0, Lcom/google/maps/g/a/fq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/a/fq;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/a/fq;->b:J

    .line 395
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 396
    iget v2, p0, Lcom/google/maps/g/a/fq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/fq;->a:I

    .line 397
    iget-object v2, p1, Lcom/google/maps/g/a/fo;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/fq;->c:Ljava/lang/Object;

    .line 400
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 401
    iget v2, p0, Lcom/google/maps/g/a/fq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/fq;->a:I

    .line 402
    iget-object v2, p1, Lcom/google/maps/g/a/fo;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/fq;->d:Ljava/lang/Object;

    .line 405
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 406
    iget v0, p1, Lcom/google/maps/g/a/fo;->e:I

    iget v1, p0, Lcom/google/maps/g/a/fq;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/a/fq;->a:I

    iput v0, p0, Lcom/google/maps/g/a/fq;->e:I

    .line 408
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/a/fo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 392
    goto :goto_1

    :cond_6
    move v2, v1

    .line 395
    goto :goto_2

    :cond_7
    move v2, v1

    .line 400
    goto :goto_3

    :cond_8
    move v0, v1

    .line 405
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 339
    new-instance v2, Lcom/google/maps/g/a/fo;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/fo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/fq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-wide v4, p0, Lcom/google/maps/g/a/fq;->b:J

    iput-wide v4, v2, Lcom/google/maps/g/a/fo;->b:J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/fq;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/fo;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/fq;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/fo;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/a/fq;->e:I

    iput v1, v2, Lcom/google/maps/g/a/fo;->e:I

    iput v0, v2, Lcom/google/maps/g/a/fo;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 339
    check-cast p1, Lcom/google/maps/g/a/fo;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/fq;->a(Lcom/google/maps/g/a/fo;)Lcom/google/maps/g/a/fq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    return v0
.end method
