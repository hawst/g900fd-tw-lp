.class public final Lcom/google/maps/g/uz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/va;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ux;",
        "Lcom/google/maps/g/uz;",
        ">;",
        "Lcom/google/maps/g/va;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 819
    sget-object v0, Lcom/google/maps/g/ux;->o:Lcom/google/maps/g/ux;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1009
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uz;->b:Lcom/google/n/ao;

    .line 1068
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->c:Ljava/lang/Object;

    .line 1144
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->d:Ljava/lang/Object;

    .line 1220
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->e:Ljava/lang/Object;

    .line 1296
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->f:Ljava/lang/Object;

    .line 1372
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->g:Ljava/lang/Object;

    .line 1448
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->h:Ljava/lang/Object;

    .line 1524
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uz;->i:Ljava/lang/Object;

    .line 1601
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    .line 1802
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    .line 1938
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uz;->n:Lcom/google/n/ao;

    .line 820
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;
    .locals 5

    .prologue
    const/16 v4, 0x100

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 923
    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 994
    :goto_0
    return-object p0

    .line 924
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 925
    iget-object v2, p0, Lcom/google/maps/g/uz;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 926
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 928
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 929
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 930
    iget-object v2, p1, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->c:Ljava/lang/Object;

    .line 933
    :cond_2
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 934
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 935
    iget-object v2, p1, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->d:Ljava/lang/Object;

    .line 938
    :cond_3
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 939
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 940
    iget-object v2, p1, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->e:Ljava/lang/Object;

    .line 943
    :cond_4
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 944
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 945
    iget-object v2, p1, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->f:Ljava/lang/Object;

    .line 948
    :cond_5
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 949
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 950
    iget-object v2, p1, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->g:Ljava/lang/Object;

    .line 953
    :cond_6
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 954
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 955
    iget-object v2, p1, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->h:Ljava/lang/Object;

    .line 958
    :cond_7
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 959
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 960
    iget-object v2, p1, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uz;->i:Ljava/lang/Object;

    .line 963
    :cond_8
    iget-object v2, p1, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 964
    iget-object v2, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 965
    iget-object v2, p1, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    .line 966
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 973
    :cond_9
    :goto_9
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v2, v2, 0x100

    if-ne v2, v4, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 974
    iget-boolean v2, p1, Lcom/google/maps/g/ux;->k:Z

    iget v3, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/g/uz;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/uz;->k:Z

    .line 976
    :cond_a
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 977
    iget-boolean v2, p1, Lcom/google/maps/g/ux;->l:Z

    iget v3, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/g/uz;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/uz;->l:Z

    .line 979
    :cond_b
    iget-object v2, p1, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 980
    iget-object v2, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 981
    iget-object v2, p1, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    .line 982
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 989
    :cond_c
    :goto_c
    iget v2, p1, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1c

    :goto_d
    if-eqz v0, :cond_d

    .line 990
    iget-object v0, p0, Lcom/google/maps/g/uz;->n:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 991
    iget v0, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/uz;->a:I

    .line 993
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/ux;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 924
    goto/16 :goto_1

    :cond_f
    move v2, v1

    .line 928
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 933
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 938
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 943
    goto/16 :goto_5

    :cond_13
    move v2, v1

    .line 948
    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 953
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 958
    goto/16 :goto_8

    .line 968
    :cond_16
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v2, v2, 0x100

    if-eq v2, v4, :cond_17

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 969
    :cond_17
    iget-object v2, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 973
    goto/16 :goto_a

    :cond_19
    move v2, v1

    .line 976
    goto/16 :goto_b

    .line 984
    :cond_1a
    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-eq v2, v3, :cond_1b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/uz;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/uz;->a:I

    .line 985
    :cond_1b
    iget-object v2, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_c

    :cond_1c
    move v0, v1

    .line 989
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/google/maps/g/uz;->c()Lcom/google/maps/g/ux;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 811
    check-cast p1, Lcom/google/maps/g/ux;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/uz;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 998
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 999
    iget-object v0, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/e;->d()Lcom/google/maps/g/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/e;

    invoke-virtual {v0}, Lcom/google/maps/g/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1004
    :goto_1
    return v2

    .line 998
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1004
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/ux;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 857
    new-instance v2, Lcom/google/maps/g/ux;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ux;-><init>(Lcom/google/n/v;)V

    .line 858
    iget v3, p0, Lcom/google/maps/g/uz;->a:I

    .line 860
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    .line 863
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/uz;->b:Lcom/google/n/ao;

    .line 864
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/uz;->b:Lcom/google/n/ao;

    .line 865
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 863
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 866
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 867
    or-int/lit8 v0, v0, 0x2

    .line 869
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/uz;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    .line 870
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 871
    or-int/lit8 v0, v0, 0x4

    .line 873
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/uz;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    .line 874
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 875
    or-int/lit8 v0, v0, 0x8

    .line 877
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/uz;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    .line 878
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 879
    or-int/lit8 v0, v0, 0x10

    .line 881
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/uz;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    .line 882
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 883
    or-int/lit8 v0, v0, 0x20

    .line 885
    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/uz;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    .line 886
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 887
    or-int/lit8 v0, v0, 0x40

    .line 889
    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/uz;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    .line 890
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 891
    or-int/lit16 v0, v0, 0x80

    .line 893
    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/uz;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    .line 894
    iget v4, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 895
    iget-object v4, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    .line 896
    iget v4, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v4, v4, -0x101

    iput v4, p0, Lcom/google/maps/g/uz;->a:I

    .line 898
    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/uz;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 899
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 900
    or-int/lit16 v0, v0, 0x100

    .line 902
    :cond_8
    iget-boolean v4, p0, Lcom/google/maps/g/uz;->k:Z

    iput-boolean v4, v2, Lcom/google/maps/g/ux;->k:Z

    .line 903
    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 904
    or-int/lit16 v0, v0, 0x200

    .line 906
    :cond_9
    iget-boolean v4, p0, Lcom/google/maps/g/uz;->l:Z

    iput-boolean v4, v2, Lcom/google/maps/g/ux;->l:Z

    .line 907
    iget v4, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 908
    iget-object v4, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    .line 909
    iget v4, p0, Lcom/google/maps/g/uz;->a:I

    and-int/lit16 v4, v4, -0x801

    iput v4, p0, Lcom/google/maps/g/uz;->a:I

    .line 911
    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/uz;->m:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 912
    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_b

    .line 913
    or-int/lit16 v0, v0, 0x400

    .line 915
    :cond_b
    iget-object v3, v2, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/uz;->n:Lcom/google/n/ao;

    .line 916
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/uz;->n:Lcom/google/n/ao;

    .line 917
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 915
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 918
    iput v0, v2, Lcom/google/maps/g/ux;->a:I

    .line 919
    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method
