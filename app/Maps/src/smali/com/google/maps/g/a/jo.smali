.class public final Lcom/google/maps/g/a/jo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/jr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/jm;",
        "Lcom/google/maps/g/a/jo;",
        ">;",
        "Lcom/google/maps/g/a/jr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 533
    sget-object v0, Lcom/google/maps/g/a/jm;->h:Lcom/google/maps/g/a/jm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jo;->b:Lcom/google/n/ao;

    .line 696
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    .line 832
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/jo;->d:I

    .line 868
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jo;->e:Ljava/lang/Object;

    .line 944
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jo;->f:Ljava/lang/Object;

    .line 1020
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jo;->g:Lcom/google/n/ao;

    .line 534
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/jm;)Lcom/google/maps/g/a/jo;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 594
    invoke-static {}, Lcom/google/maps/g/a/jm;->g()Lcom/google/maps/g/a/jm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 627
    :goto_0
    return-object p0

    .line 595
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 596
    iget-object v2, p0, Lcom/google/maps/g/a/jo;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 597
    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 599
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 600
    iget-object v2, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 601
    iget-object v2, p1, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    .line 602
    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 609
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 610
    iget v2, p1, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    :cond_3
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 595
    goto :goto_1

    .line 604
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 605
    :cond_6
    iget-object v2, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v2, v1

    .line 609
    goto :goto_3

    .line 610
    :cond_8
    iget v3, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/jo;->a:I

    iget v2, v2, Lcom/google/maps/g/a/jp;->f:I

    iput v2, p0, Lcom/google/maps/g/a/jo;->d:I

    .line 612
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 613
    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 614
    iget-object v2, p1, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/jo;->e:Ljava/lang/Object;

    .line 617
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 618
    iget v2, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 619
    iget-object v2, p1, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/jo;->f:Ljava/lang/Object;

    .line 622
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    :goto_6
    if-eqz v0, :cond_c

    .line 623
    iget-object v0, p0, Lcom/google/maps/g/a/jo;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 624
    iget v0, p0, Lcom/google/maps/g/a/jo;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/jo;->a:I

    .line 626
    :cond_c
    iget-object v0, p1, Lcom/google/maps/g/a/jm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 612
    goto :goto_4

    :cond_e
    move v2, v1

    .line 617
    goto :goto_5

    :cond_f
    move v0, v1

    .line 622
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 525
    new-instance v2, Lcom/google/maps/g/a/jm;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/jm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/jo;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/jo;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/jo;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/a/jo;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/jo;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/a/jo;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/jo;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/jo;->d:I

    iput v4, v2, Lcom/google/maps/g/a/jm;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/jo;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/jo;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v3, v2, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/jo;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/jo;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/jm;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 525
    check-cast p1, Lcom/google/maps/g/a/jm;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/jo;->a(Lcom/google/maps/g/a/jm;)Lcom/google/maps/g/a/jo;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 631
    const/4 v0, 0x1

    return v0
.end method
