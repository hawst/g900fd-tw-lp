.class public final enum Lcom/google/maps/g/g/g;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/g/g;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/g/g;

.field public static final enum b:Lcom/google/maps/g/g/g;

.field private static final synthetic d:[Lcom/google/maps/g/g/g;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 183
    new-instance v0, Lcom/google/maps/g/g/g;

    const-string v1, "TRANSIT_SCHEDULE"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/maps/g/g/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    .line 187
    new-instance v0, Lcom/google/maps/g/g/g;

    const-string v1, "REVIEW_DETAILS"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/g/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/g/g;->b:Lcom/google/maps/g/g/g;

    .line 178
    new-array v0, v4, [Lcom/google/maps/g/g/g;

    sget-object v1, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/g/g;->b:Lcom/google/maps/g/g/g;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/maps/g/g/g;->d:[Lcom/google/maps/g/g/g;

    .line 217
    new-instance v0, Lcom/google/maps/g/g/h;

    invoke-direct {v0}, Lcom/google/maps/g/g/h;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 227
    iput p3, p0, Lcom/google/maps/g/g/g;->c:I

    .line 228
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/g/g;
    .locals 1

    .prologue
    .line 205
    packed-switch p0, :pswitch_data_0

    .line 208
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 206
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    goto :goto_0

    .line 207
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/g/g;->b:Lcom/google/maps/g/g/g;

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/g/g;
    .locals 1

    .prologue
    .line 178
    const-class v0, Lcom/google/maps/g/g/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/g;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/g/g;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/google/maps/g/g/g;->d:[Lcom/google/maps/g/g/g;

    invoke-virtual {v0}, [Lcom/google/maps/g/g/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/g/g;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/google/maps/g/g/g;->c:I

    return v0
.end method
