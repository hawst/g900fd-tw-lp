.class public final Lcom/google/maps/g/hc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hc;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/hc;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/maps/g/hd;

    invoke-direct {v0}, Lcom/google/maps/g/hd;-><init>()V

    sput-object v0, Lcom/google/maps/g/hc;->PARSER:Lcom/google/n/ax;

    .line 240
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/hc;->h:Lcom/google/n/aw;

    .line 600
    new-instance v0, Lcom/google/maps/g/hc;

    invoke-direct {v0}, Lcom/google/maps/g/hc;-><init>()V

    sput-object v0, Lcom/google/maps/g/hc;->e:Lcom/google/maps/g/hc;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 133
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    .line 190
    iput-byte v2, p0, Lcom/google/maps/g/hc;->f:B

    .line 215
    iput v2, p0, Lcom/google/maps/g/hc;->g:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/hc;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 48
    iget v4, p0, Lcom/google/maps/g/hc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/hc;->a:I

    .line 49
    iput-object v3, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hc;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 54
    iget v3, p0, Lcom/google/maps/g/hc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/hc;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 69
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 59
    iget v4, p0, Lcom/google/maps/g/hc;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/hc;->a:I

    .line 60
    iput-object v3, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hc;->au:Lcom/google/n/bn;

    .line 72
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 133
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    .line 190
    iput-byte v1, p0, Lcom/google/maps/g/hc;->f:B

    .line 215
    iput v1, p0, Lcom/google/maps/g/hc;->g:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/maps/g/hc;
    .locals 1

    .prologue
    .line 603
    sget-object v0, Lcom/google/maps/g/hc;->e:Lcom/google/maps/g/hc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/he;
    .locals 1

    .prologue
    .line 302
    new-instance v0, Lcom/google/maps/g/he;

    invoke-direct {v0}, Lcom/google/maps/g/he;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/google/maps/g/hc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 202
    invoke-virtual {p0}, Lcom/google/maps/g/hc;->c()I

    .line 203
    iget v0, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 206
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 209
    :cond_1
    iget v0, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 210
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/hc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 213
    return-void

    .line 204
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 210
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 192
    iget-byte v1, p0, Lcom/google/maps/g/hc;->f:B

    .line 193
    if-ne v1, v0, :cond_0

    .line 197
    :goto_0
    return v0

    .line 194
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 196
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/hc;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 217
    iget v0, p0, Lcom/google/maps/g/hc;->g:I

    .line 218
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 235
    :goto_0
    return v0

    .line 221
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 223
    iget-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 225
    :goto_2
    iget v2, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_4

    .line 226
    iget-object v2, p0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    .line 227
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 229
    :goto_3
    iget v0, p0, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 230
    const/4 v3, 0x3

    .line 231
    iget-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/hc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 234
    iput v0, p0, Lcom/google/maps/g/hc;->g:I

    goto :goto_0

    .line 223
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 231
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_4
    move v2, v0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    .line 103
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 104
    check-cast v0, Ljava/lang/String;

    .line 112
    :goto_0
    return-object v0

    .line 106
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 108
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iput-object v1, p0, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 112
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hc;->newBuilder()Lcom/google/maps/g/he;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/he;->a(Lcom/google/maps/g/hc;)Lcom/google/maps/g/he;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/hc;->newBuilder()Lcom/google/maps/g/he;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    .line 161
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 162
    check-cast v0, Ljava/lang/String;

    .line 170
    :goto_0
    return-object v0

    .line 164
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 166
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iput-object v1, p0, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 170
    goto :goto_0
.end method
