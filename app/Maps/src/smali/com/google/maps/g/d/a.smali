.class public final Lcom/google/maps/g/d/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/d/f;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/d/a;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/d/a;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Z

.field h:Z

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/maps/g/d/b;

    invoke-direct {v0}, Lcom/google/maps/g/d/b;-><init>()V

    sput-object v0, Lcom/google/maps/g/d/a;->PARSER:Lcom/google/n/ax;

    .line 399
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/d/a;->m:Lcom/google/n/aw;

    .line 1002
    new-instance v0, Lcom/google/maps/g/d/a;

    invoke-direct {v0}, Lcom/google/maps/g/d/a;-><init>()V

    sput-object v0, Lcom/google/maps/g/d/a;->j:Lcom/google/maps/g/d/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    .line 199
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    .line 215
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    .line 308
    iput-byte v4, p0, Lcom/google/maps/g/d/a;->k:B

    .line 354
    iput v4, p0, Lcom/google/maps/g/d/a;->l:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput-boolean v3, p0, Lcom/google/maps/g/d/a;->g:Z

    .line 24
    iput-boolean v3, p0, Lcom/google/maps/g/d/a;->h:Z

    .line 25
    iput v3, p0, Lcom/google/maps/g/d/a;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/d/a;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 53
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/d/a;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 58
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 63
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I

    goto :goto_0

    .line 67
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 68
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I

    goto :goto_0

    .line 72
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 73
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I

    goto/16 :goto_0

    .line 77
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/d/a;->g:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 82
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/d/a;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/d/a;->h:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 87
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 88
    invoke-static {v0}, Lcom/google/maps/g/d/d;->a(I)Lcom/google/maps/g/d/d;

    move-result-object v5

    .line 89
    if-nez v5, :cond_3

    .line 90
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_3
    iget v5, p0, Lcom/google/maps/g/d/a;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/maps/g/d/a;->a:I

    .line 93
    iput v0, p0, Lcom/google/maps/g/d/a;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/d/a;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    .line 199
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    .line 215
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    .line 308
    iput-byte v1, p0, Lcom/google/maps/g/d/a;->k:B

    .line 354
    iput v1, p0, Lcom/google/maps/g/d/a;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/d/a;
    .locals 1

    .prologue
    .line 1005
    sget-object v0, Lcom/google/maps/g/d/a;->j:Lcom/google/maps/g/d/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/d/c;
    .locals 1

    .prologue
    .line 461
    new-instance v0, Lcom/google/maps/g/d/c;

    invoke-direct {v0}, Lcom/google/maps/g/d/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/d/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/maps/g/d/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 326
    invoke-virtual {p0}, Lcom/google/maps/g/d/a;->c()I

    .line 327
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 330
    :cond_0
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 331
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 333
    :cond_1
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 334
    iget-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 336
    :cond_2
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 337
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 339
    :cond_3
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 340
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 342
    :cond_4
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 343
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/d/a;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_8

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 345
    :cond_5
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 346
    iget-boolean v0, p0, Lcom/google/maps/g/d/a;->h:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    :goto_1
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 348
    :cond_6
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 349
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/g/d/a;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 351
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/d/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 352
    return-void

    :cond_8
    move v0, v2

    .line 343
    goto :goto_0

    :cond_9
    move v1, v2

    .line 346
    goto :goto_1

    .line 349
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 310
    iget-byte v0, p0, Lcom/google/maps/g/d/a;->k:B

    .line 311
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 321
    :goto_0
    return v0

    .line 312
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 314
    :cond_1
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 315
    iget-object v0, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 316
    iput-byte v2, p0, Lcom/google/maps/g/d/a;->k:B

    move v0, v2

    .line 317
    goto :goto_0

    :cond_2
    move v0, v2

    .line 314
    goto :goto_1

    .line 320
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/g/d/a;->k:B

    move v0, v1

    .line 321
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 356
    iget v0, p0, Lcom/google/maps/g/d/a;->l:I

    .line 357
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 394
    :goto_0
    return v0

    .line 360
    :cond_0
    iget v0, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_9

    .line 361
    iget-object v0, p0, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    .line 362
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 364
    :goto_1
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_1

    .line 365
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    .line 366
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 368
    :cond_1
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_2

    .line 369
    iget-object v2, p0, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    .line 370
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 372
    :cond_2
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_3

    .line 373
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    .line 374
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 376
    :cond_3
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 377
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    .line 378
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 380
    :cond_4
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 381
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/d/a;->g:Z

    .line 382
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 384
    :cond_5
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 385
    iget-boolean v2, p0, Lcom/google/maps/g/d/a;->h:Z

    .line 386
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 388
    :cond_6
    iget v2, p0, Lcom/google/maps/g/d/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 389
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/maps/g/d/a;->i:I

    .line 390
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 392
    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/d/a;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    iput v0, p0, Lcom/google/maps/g/d/a;->l:I

    goto/16 :goto_0

    .line 390
    :cond_8
    const/16 v1, 0xa

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/d/a;->newBuilder()Lcom/google/maps/g/d/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/d/c;->a(Lcom/google/maps/g/d/a;)Lcom/google/maps/g/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/d/a;->newBuilder()Lcom/google/maps/g/d/c;

    move-result-object v0

    return-object v0
.end method
