.class public final Lcom/google/maps/g/a/cw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/cw;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/a/cw;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:D

.field c:D

.field public d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/maps/g/a/cx;

    invoke-direct {v0}, Lcom/google/maps/g/a/cx;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/cw;->PARSER:Lcom/google/n/ax;

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/cw;->i:Lcom/google/n/aw;

    .line 638
    new-instance v0, Lcom/google/maps/g/a/cw;

    invoke-direct {v0}, Lcom/google/maps/g/a/cw;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/cw;->f:Lcom/google/maps/g/a/cw;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 210
    iput-byte v2, p0, Lcom/google/maps/g/a/cw;->g:B

    .line 238
    iput v2, p0, Lcom/google/maps/g/a/cw;->h:I

    .line 18
    iput-wide v0, p0, Lcom/google/maps/g/a/cw;->b:D

    .line 19
    iput-wide v0, p0, Lcom/google/maps/g/a/cw;->c:D

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/a/cw;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    const/4 v0, 0x0

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/a/cw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/cw;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/g/a/cw;->b:D
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/cw;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/a/cw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/cw;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/g/a/cw;->c:D
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 75
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 59
    iget v4, p0, Lcom/google/maps/g/a/cw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/a/cw;->a:I

    .line 60
    iput-object v3, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    goto :goto_0

    .line 64
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 65
    iget v4, p0, Lcom/google/maps/g/a/cw;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/a/cw;->a:I

    .line 66
    iput-object v3, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->au:Lcom/google/n/bn;

    .line 78
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 210
    iput-byte v0, p0, Lcom/google/maps/g/a/cw;->g:B

    .line 238
    iput v0, p0, Lcom/google/maps/g/a/cw;->h:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;
    .locals 1

    .prologue
    .line 332
    invoke-static {}, Lcom/google/maps/g/a/cw;->newBuilder()Lcom/google/maps/g/a/cy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/cw;
    .locals 1

    .prologue
    .line 641
    sget-object v0, Lcom/google/maps/g/a/cw;->f:Lcom/google/maps/g/a/cw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/cy;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/google/maps/g/a/cy;

    invoke-direct {v0}, Lcom/google/maps/g/a/cy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/cw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/google/maps/g/a/cw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 222
    invoke-virtual {p0}, Lcom/google/maps/g/a/cw;->c()I

    .line 223
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 224
    iget-wide v0, p0, Lcom/google/maps/g/a/cw;->b:D

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 226
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 227
    iget-wide v0, p0, Lcom/google/maps/g/a/cw;->c:D

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 229
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 230
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 232
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 233
    iget-object v0, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/cw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 236
    return-void

    .line 230
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 233
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 212
    iget-byte v1, p0, Lcom/google/maps/g/a/cw;->g:B

    .line 213
    if-ne v1, v0, :cond_0

    .line 217
    :goto_0
    return v0

    .line 214
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 216
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/cw;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 240
    iget v0, p0, Lcom/google/maps/g/a/cw;->h:I

    .line 241
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 262
    :goto_0
    return v0

    .line 244
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 245
    iget-wide v2, p0, Lcom/google/maps/g/a/cw;->b:D

    .line 246
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 248
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_5

    .line 249
    iget-wide v2, p0, Lcom/google/maps/g/a/cw;->c:D

    .line 250
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    move v2, v0

    .line 252
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 253
    const/4 v3, 0x3

    .line 254
    iget-object v0, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 256
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 258
    iget-object v0, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 260
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/cw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 261
    iput v0, p0, Lcom/google/maps/g/a/cw;->h:I

    goto/16 :goto_0

    .line 254
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 258
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/cw;->newBuilder()Lcom/google/maps/g/a/cy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/cw;->newBuilder()Lcom/google/maps/g/a/cy;

    move-result-object v0

    return-object v0
.end method
