.class public final Lcom/google/maps/g/lw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/mf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/lu;",
        "Lcom/google/maps/g/lw;",
        ">;",
        "Lcom/google/maps/g/mf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2711
    sget-object v0, Lcom/google/maps/g/lu;->g:Lcom/google/maps/g/lu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lw;->b:Lcom/google/n/ao;

    .line 2864
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/lw;->c:I

    .line 2900
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lw;->d:Lcom/google/n/ao;

    .line 2959
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lw;->e:Lcom/google/n/ao;

    .line 3018
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lw;->f:Lcom/google/n/ao;

    .line 2712
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/lu;)Lcom/google/maps/g/lw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2769
    invoke-static {}, Lcom/google/maps/g/lu;->d()Lcom/google/maps/g/lu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2790
    :goto_0
    return-object p0

    .line 2770
    :cond_0
    iget v2, p1, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2771
    iget-object v2, p0, Lcom/google/maps/g/lw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2772
    iget v2, p0, Lcom/google/maps/g/lw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/lw;->a:I

    .line 2774
    :cond_1
    iget v2, p1, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 2775
    iget v2, p1, Lcom/google/maps/g/lu;->c:I

    invoke-static {v2}, Lcom/google/maps/g/lx;->a(I)Lcom/google/maps/g/lx;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/lx;->a:Lcom/google/maps/g/lx;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 2770
    goto :goto_1

    :cond_4
    move v2, v1

    .line 2774
    goto :goto_2

    .line 2775
    :cond_5
    iget v3, p0, Lcom/google/maps/g/lw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/lw;->a:I

    iget v2, v2, Lcom/google/maps/g/lx;->f:I

    iput v2, p0, Lcom/google/maps/g/lw;->c:I

    .line 2777
    :cond_6
    iget v2, p1, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 2778
    iget-object v2, p0, Lcom/google/maps/g/lw;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2779
    iget v2, p0, Lcom/google/maps/g/lw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/lw;->a:I

    .line 2781
    :cond_7
    iget v2, p1, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 2782
    iget-object v2, p0, Lcom/google/maps/g/lw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2783
    iget v2, p0, Lcom/google/maps/g/lw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/lw;->a:I

    .line 2785
    :cond_8
    iget v2, p1, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    :goto_5
    if-eqz v0, :cond_9

    .line 2786
    iget-object v0, p0, Lcom/google/maps/g/lw;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2787
    iget v0, p0, Lcom/google/maps/g/lw;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/lw;->a:I

    .line 2789
    :cond_9
    iget-object v0, p1, Lcom/google/maps/g/lu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 2777
    goto :goto_3

    :cond_b
    move v2, v1

    .line 2781
    goto :goto_4

    :cond_c
    move v0, v1

    .line 2785
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2703
    new-instance v2, Lcom/google/maps/g/lu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/lu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/lw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/maps/g/lw;->c:I

    iput v4, v2, Lcom/google/maps/g/lu;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lw;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lw;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lw;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lw;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/lw;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/lw;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/lu;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2703
    check-cast p1, Lcom/google/maps/g/lu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/lw;->a(Lcom/google/maps/g/lu;)Lcom/google/maps/g/lw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2794
    iget v0, p0, Lcom/google/maps/g/lw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 2795
    iget-object v0, p0, Lcom/google/maps/g/lw;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/lz;->d()Lcom/google/maps/g/lz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lz;

    invoke-virtual {v0}, Lcom/google/maps/g/lz;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2800
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2794
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2800
    goto :goto_1
.end method
