.class public final Lcom/google/maps/g/vm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/vk;",
        "Lcom/google/maps/g/vm;",
        ">;",
        "Lcom/google/maps/g/vn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 929
    sget-object v0, Lcom/google/maps/g/vk;->e:Lcom/google/maps/g/vk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1005
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vm;->b:Ljava/lang/Object;

    .line 1082
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    .line 1219
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    .line 930
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/vk;)Lcom/google/maps/g/vm;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 969
    invoke-static {}, Lcom/google/maps/g/vk;->h()Lcom/google/maps/g/vk;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 996
    :goto_0
    return-object p0

    .line 970
    :cond_0
    iget v1, p1, Lcom/google/maps/g/vk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    .line 971
    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/vm;->a:I

    .line 972
    iget-object v0, p1, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/vm;->b:Ljava/lang/Object;

    .line 975
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 976
    iget-object v0, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 977
    iget-object v0, p1, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    .line 978
    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/vm;->a:I

    .line 985
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 986
    iget-object v0, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 987
    iget-object v0, p1, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    .line 988
    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/maps/g/vm;->a:I

    .line 995
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/maps/g/vk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 970
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 980
    :cond_5
    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/vm;->a:I

    .line 981
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 990
    :cond_7
    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/vm;->a:I

    .line 991
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 921
    new-instance v2, Lcom/google/maps/g/vk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/vk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/vm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/vm;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/vm;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/vm;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vm;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/maps/g/vm;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/vm;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/vk;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 921
    check-cast p1, Lcom/google/maps/g/vk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/vm;->a(Lcom/google/maps/g/vk;)Lcom/google/maps/g/vm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1000
    const/4 v0, 0x1

    return v0
.end method
