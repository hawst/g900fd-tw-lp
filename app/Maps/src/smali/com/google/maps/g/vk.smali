.class public final Lcom/google/maps/g/vk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vk;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/vk;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/google/maps/g/vl;

    invoke-direct {v0}, Lcom/google/maps/g/vl;-><init>()V

    sput-object v0, Lcom/google/maps/g/vk;->PARSER:Lcom/google/n/ax;

    .line 849
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/vk;->h:Lcom/google/n/aw;

    .line 1359
    new-instance v0, Lcom/google/maps/g/vk;

    invoke-direct {v0}, Lcom/google/maps/g/vk;-><init>()V

    sput-object v0, Lcom/google/maps/g/vk;->e:Lcom/google/maps/g/vk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 799
    iput-byte v0, p0, Lcom/google/maps/g/vk;->f:B

    .line 824
    iput v0, p0, Lcom/google/maps/g/vk;->g:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/vk;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 35
    sparse-switch v1, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 42
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 48
    iget v5, p0, Lcom/google/maps/g/vk;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/vk;->a:I

    .line 49
    iput-object v1, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 75
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 81
    iget-object v2, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    .line 83
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_2

    .line 84
    iget-object v1, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    .line 86
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vk;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_7

    .line 54
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 56
    or-int/lit8 v1, v0, 0x2

    .line 58
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 58
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 60
    goto :goto_0

    .line 63
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_3

    .line 64
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    .line 66
    or-int/lit8 v0, v0, 0x4

    .line 68
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 68
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 76
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 77
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 78
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 80
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_5

    .line 81
    iget-object v1, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    .line 83
    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_6

    .line 84
    iget-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    .line 86
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->au:Lcom/google/n/bn;

    .line 87
    return-void

    .line 80
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto/16 :goto_2

    .line 76
    :catch_2
    move-exception v0

    goto :goto_4

    .line 74
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto :goto_3

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 799
    iput-byte v0, p0, Lcom/google/maps/g/vk;->f:B

    .line 824
    iput v0, p0, Lcom/google/maps/g/vk;->g:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/maps/g/vk;
    .locals 1

    .prologue
    .line 1362
    sget-object v0, Lcom/google/maps/g/vk;->e:Lcom/google/maps/g/vk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/vm;
    .locals 1

    .prologue
    .line 911
    new-instance v0, Lcom/google/maps/g/vm;

    invoke-direct {v0}, Lcom/google/maps/g/vm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    sget-object v0, Lcom/google/maps/g/vk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 811
    invoke-virtual {p0}, Lcom/google/maps/g/vk;->c()I

    .line 812
    iget v0, p0, Lcom/google/maps/g/vk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 813
    iget-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 815
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 816
    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 815
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 813
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 818
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 819
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 818
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 821
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/vk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 822
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 801
    iget-byte v1, p0, Lcom/google/maps/g/vk;->f:B

    .line 802
    if-ne v1, v0, :cond_0

    .line 806
    :goto_0
    return v0

    .line 803
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 805
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/vk;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 826
    iget v0, p0, Lcom/google/maps/g/vk;->g:I

    .line 827
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 844
    :goto_0
    return v0

    .line 830
    :cond_0
    iget v0, p0, Lcom/google/maps/g/vk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 832
    iget-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 834
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 835
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    .line 836
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 834
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 832
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v2, v1

    .line 838
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 839
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vk;->d:Ljava/util/List;

    .line 840
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 838
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 842
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/vk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 843
    iput v0, p0, Lcom/google/maps/g/vk;->g:I

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    .line 684
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 685
    check-cast v0, Ljava/lang/String;

    .line 693
    :goto_0
    return-object v0

    .line 687
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 689
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 690
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    iput-object v1, p0, Lcom/google/maps/g/vk;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 693
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vk;->newBuilder()Lcom/google/maps/g/vm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/vm;->a(Lcom/google/maps/g/vk;)Lcom/google/maps/g/vm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vk;->newBuilder()Lcom/google/maps/g/vm;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/vf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 720
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    .line 721
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 722
    iget-object v0, p0, Lcom/google/maps/g/vk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 723
    invoke-static {}, Lcom/google/maps/g/vf;->g()Lcom/google/maps/g/vf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vf;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 725
    :cond_0
    return-object v1
.end method
