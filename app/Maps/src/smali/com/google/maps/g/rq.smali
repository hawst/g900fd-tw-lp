.class public final Lcom/google/maps/g/rq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/sd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ro;",
        "Lcom/google/maps/g/rq;",
        ">;",
        "Lcom/google/maps/g/sd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/rr;

.field private c:Lcom/google/maps/g/rz;

.field private d:Lcom/google/maps/g/rv;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2985
    sget-object v0, Lcom/google/maps/g/ro;->e:Lcom/google/maps/g/ro;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3043
    iput-object v1, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    .line 3104
    iput-object v1, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    .line 3165
    iput-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    .line 2986
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ro;)Lcom/google/maps/g/rq;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3023
    invoke-static {}, Lcom/google/maps/g/ro;->d()Lcom/google/maps/g/ro;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3034
    :goto_0
    return-object p0

    .line 3024
    :cond_0
    iget v0, p1, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 3025
    iget-object v0, p1, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/rr;->d()Lcom/google/maps/g/rr;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/rq;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_6

    iget-object v3, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    invoke-static {}, Lcom/google/maps/g/rr;->d()Lcom/google/maps/g/rr;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    invoke-static {v3}, Lcom/google/maps/g/rr;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/rt;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    move-result-object v0

    new-instance v3, Lcom/google/maps/g/rr;

    invoke-direct {v3, v0}, Lcom/google/maps/g/rr;-><init>(Lcom/google/n/v;)V

    iput-object v3, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/rq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/rq;->a:I

    .line 3027
    :cond_1
    iget v0, p1, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_7

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 3028
    iget-object v0, p1, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/maps/g/rz;->d()Lcom/google/maps/g/rz;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/maps/g/rq;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_9

    iget-object v3, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    invoke-static {}, Lcom/google/maps/g/rz;->d()Lcom/google/maps/g/rz;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    invoke-static {v3}, Lcom/google/maps/g/rz;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/sb;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    move-result-object v0

    new-instance v3, Lcom/google/maps/g/rz;

    invoke-direct {v3, v0}, Lcom/google/maps/g/rz;-><init>(Lcom/google/n/v;)V

    iput-object v3, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    :goto_6
    iget v0, p0, Lcom/google/maps/g/rq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/rq;->a:I

    .line 3030
    :cond_2
    iget v0, p1, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_a

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 3031
    iget-object v0, p1, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/rv;->d()Lcom/google/maps/g/rv;

    move-result-object v0

    :goto_8
    iget v1, p0, Lcom/google/maps/g/rq;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_c

    iget-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    invoke-static {}, Lcom/google/maps/g/rv;->d()Lcom/google/maps/g/rv;

    move-result-object v2

    if-eq v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    invoke-static {v1}, Lcom/google/maps/g/rv;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/rx;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    move-result-object v0

    new-instance v1, Lcom/google/maps/g/rv;

    invoke-direct {v1, v0}, Lcom/google/maps/g/rv;-><init>(Lcom/google/n/v;)V

    iput-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    :goto_9
    iget v0, p0, Lcom/google/maps/g/rq;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/rq;->a:I

    .line 3033
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/ro;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 3024
    goto/16 :goto_1

    .line 3025
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    goto/16 :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    goto/16 :goto_3

    :cond_7
    move v0, v2

    .line 3027
    goto/16 :goto_4

    .line 3028
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    goto :goto_5

    :cond_9
    iput-object v0, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    goto :goto_6

    :cond_a
    move v0, v2

    .line 3030
    goto :goto_7

    .line 3031
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    goto :goto_8

    :cond_c
    iput-object v0, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2977
    new-instance v2, Lcom/google/maps/g/ro;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ro;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/rq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/rq;->b:Lcom/google/maps/g/rr;

    iput-object v1, v2, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/rq;->c:Lcom/google/maps/g/rz;

    iput-object v1, v2, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/rq;->d:Lcom/google/maps/g/rv;

    iput-object v1, v2, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    iput v0, v2, Lcom/google/maps/g/ro;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2977
    check-cast p1, Lcom/google/maps/g/ro;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/rq;->a(Lcom/google/maps/g/ro;)Lcom/google/maps/g/rq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3038
    const/4 v0, 0x1

    return v0
.end method
