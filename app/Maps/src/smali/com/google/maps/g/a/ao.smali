.class public final Lcom/google/maps/g/a/ao;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/at;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ao;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/a/ao;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Z

.field public c:Z

.field d:I

.field e:Z

.field f:Z

.field g:Z

.field h:Lcom/google/n/ao;

.field i:Z

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/maps/g/a/ap;

    invoke-direct {v0}, Lcom/google/maps/g/a/ap;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ao;->PARSER:Lcom/google/n/ax;

    .line 398
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ao;->m:Lcom/google/n/aw;

    .line 875
    new-instance v0, Lcom/google/maps/g/a/ao;

    invoke-direct {v0}, Lcom/google/maps/g/a/ao;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ao;->j:Lcom/google/maps/g/a/ao;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    .line 313
    iput-byte v4, p0, Lcom/google/maps/g/a/ao;->k:B

    .line 353
    iput v4, p0, Lcom/google/maps/g/a/ao;->l:I

    .line 18
    iput-boolean v2, p0, Lcom/google/maps/g/a/ao;->b:Z

    .line 19
    iput-boolean v2, p0, Lcom/google/maps/g/a/ao;->c:Z

    .line 20
    iput v1, p0, Lcom/google/maps/g/a/ao;->d:I

    .line 21
    iput-boolean v1, p0, Lcom/google/maps/g/a/ao;->e:Z

    .line 22
    iput-boolean v1, p0, Lcom/google/maps/g/a/ao;->f:Z

    .line 23
    iput-boolean v1, p0, Lcom/google/maps/g/a/ao;->g:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput-boolean v2, p0, Lcom/google/maps/g/a/ao;->i:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/a/ao;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ao;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 53
    goto :goto_1

    .line 57
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 58
    goto :goto_2

    .line 62
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->e:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 67
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->f:Z

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 72
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 73
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    goto/16 :goto_0

    .line 77
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_5

    .line 82
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/maps/g/a/ao;->i:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_6

    .line 87
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 88
    invoke-static {v0}, Lcom/google/maps/g/a/ar;->a(I)Lcom/google/maps/g/a/ar;

    move-result-object v5

    .line 89
    if-nez v5, :cond_7

    .line 90
    const/16 v5, 0x14

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_7
    iget v5, p0, Lcom/google/maps/g/a/ao;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/a/ao;->a:I

    .line 93
    iput v0, p0, Lcom/google/maps/g/a/ao;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_8
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ao;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0xa0 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    .line 313
    iput-byte v1, p0, Lcom/google/maps/g/a/ao;->k:B

    .line 353
    iput v1, p0, Lcom/google/maps/g/a/ao;->l:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;
    .locals 1

    .prologue
    .line 463
    invoke-static {}, Lcom/google/maps/g/a/ao;->newBuilder()Lcom/google/maps/g/a/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/aq;->a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/ao;
    .locals 1

    .prologue
    .line 878
    sget-object v0, Lcom/google/maps/g/a/ao;->j:Lcom/google/maps/g/a/ao;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/aq;
    .locals 1

    .prologue
    .line 460
    new-instance v0, Lcom/google/maps/g/a/aq;

    invoke-direct {v0}, Lcom/google/maps/g/a/aq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ao;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/maps/g/a/ao;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    invoke-virtual {p0}, Lcom/google/maps/g/a/ao;->c()I

    .line 326
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 327
    iget-boolean v0, p0, Lcom/google/maps/g/a/ao;->b:Z

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 329
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 330
    iget-boolean v0, p0, Lcom/google/maps/g/a/ao;->c:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 332
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 333
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 335
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 336
    iget-boolean v0, p0, Lcom/google/maps/g/a/ao;->f:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_b

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 338
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_4

    .line 339
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 341
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 342
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_c

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 344
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 345
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_d

    :goto_5
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 347
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_7

    .line 348
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/maps/g/a/ao;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 350
    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/ao;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 351
    return-void

    :cond_8
    move v0, v2

    .line 327
    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 330
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 333
    goto/16 :goto_2

    :cond_b
    move v0, v2

    .line 336
    goto :goto_3

    :cond_c
    move v0, v2

    .line 342
    goto :goto_4

    :cond_d
    move v1, v2

    .line 345
    goto :goto_5

    .line 348
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 315
    iget-byte v1, p0, Lcom/google/maps/g/a/ao;->k:B

    .line 316
    if-ne v1, v0, :cond_0

    .line 320
    :goto_0
    return v0

    .line 317
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 319
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/ao;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 355
    iget v0, p0, Lcom/google/maps/g/a/ao;->l:I

    .line 356
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 393
    :goto_0
    return v0

    .line 359
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 360
    iget-boolean v0, p0, Lcom/google/maps/g/a/ao;->b:Z

    .line 361
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 363
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 364
    iget-boolean v2, p0, Lcom/google/maps/g/a/ao;->c:Z

    .line 365
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 367
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 368
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->e:Z

    .line 369
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 371
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 372
    iget-boolean v2, p0, Lcom/google/maps/g/a/ao;->f:Z

    .line 373
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 375
    :cond_3
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_4

    .line 376
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    .line 377
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 379
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 380
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->g:Z

    .line 381
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 383
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_6

    .line 384
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/a/ao;->i:Z

    .line 385
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 387
    :cond_6
    iget v2, p0, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_7

    .line 388
    const/16 v2, 0x14

    iget v3, p0, Lcom/google/maps/g/a/ao;->d:I

    .line 389
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 391
    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/a/ao;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 392
    iput v0, p0, Lcom/google/maps/g/a/ao;->l:I

    goto/16 :goto_0

    .line 389
    :cond_8
    const/16 v1, 0xa

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ao;->newBuilder()Lcom/google/maps/g/a/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/aq;->a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ao;->newBuilder()Lcom/google/maps/g/a/aq;

    move-result-object v0

    return-object v0
.end method
