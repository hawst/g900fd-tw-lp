.class public final Lcom/google/maps/g/e/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/e/a;",
        "Lcom/google/maps/g/e/c;",
        ">;",
        "Lcom/google/maps/g/e/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 586
    sget-object v0, Lcom/google/maps/g/e/a;->j:Lcom/google/maps/g/e/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 705
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e/c;->b:Ljava/lang/Object;

    .line 781
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e/c;->c:Ljava/lang/Object;

    .line 921
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/e/c;->f:I

    .line 957
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e/c;->g:Lcom/google/n/ao;

    .line 1016
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e/c;->h:Lcom/google/n/ao;

    .line 1075
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/e/c;->i:Lcom/google/n/f;

    .line 587
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/e/a;)Lcom/google/maps/g/e/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 658
    invoke-static {}, Lcom/google/maps/g/e/a;->d()Lcom/google/maps/g/e/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 690
    :goto_0
    return-object p0

    .line 659
    :cond_0
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 660
    iget v2, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/e/c;->a:I

    .line 661
    iget-object v2, p1, Lcom/google/maps/g/e/a;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/e/c;->b:Ljava/lang/Object;

    .line 664
    :cond_1
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 665
    iget v2, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/e/c;->a:I

    .line 666
    iget-object v2, p1, Lcom/google/maps/g/e/a;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/e/c;->c:Ljava/lang/Object;

    .line 669
    :cond_2
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 670
    iget v2, p1, Lcom/google/maps/g/e/a;->d:I

    iget v3, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/e/c;->a:I

    iput v2, p0, Lcom/google/maps/g/e/c;->d:I

    .line 672
    :cond_3
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 673
    iget v2, p1, Lcom/google/maps/g/e/a;->e:I

    iget v3, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/e/c;->a:I

    iput v2, p0, Lcom/google/maps/g/e/c;->e:I

    .line 675
    :cond_4
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 676
    iget v2, p1, Lcom/google/maps/g/e/a;->f:I

    invoke-static {v2}, Lcom/google/maps/g/e/d;->a(I)Lcom/google/maps/g/e/d;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/e/d;->a:Lcom/google/maps/g/e/d;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 659
    goto :goto_1

    :cond_7
    move v2, v1

    .line 664
    goto :goto_2

    :cond_8
    move v2, v1

    .line 669
    goto :goto_3

    :cond_9
    move v2, v1

    .line 672
    goto :goto_4

    :cond_a
    move v2, v1

    .line 675
    goto :goto_5

    .line 676
    :cond_b
    iget v3, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/e/c;->a:I

    iget v2, v2, Lcom/google/maps/g/e/d;->i:I

    iput v2, p0, Lcom/google/maps/g/e/c;->f:I

    .line 678
    :cond_c
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 679
    iget-object v2, p0, Lcom/google/maps/g/e/c;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/e/a;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 680
    iget v2, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/e/c;->a:I

    .line 682
    :cond_d
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 683
    iget-object v2, p0, Lcom/google/maps/g/e/c;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/e/a;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 684
    iget v2, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/e/c;->a:I

    .line 686
    :cond_e
    iget v2, p1, Lcom/google/maps/g/e/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_13

    .line 687
    iget-object v0, p1, Lcom/google/maps/g/e/a;->i:Lcom/google/n/f;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move v2, v1

    .line 678
    goto :goto_6

    :cond_10
    move v2, v1

    .line 682
    goto :goto_7

    :cond_11
    move v0, v1

    .line 686
    goto :goto_8

    .line 687
    :cond_12
    iget v1, p0, Lcom/google/maps/g/e/c;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/e/c;->a:I

    iput-object v0, p0, Lcom/google/maps/g/e/c;->i:Lcom/google/n/f;

    .line 689
    :cond_13
    iget-object v0, p1, Lcom/google/maps/g/e/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 578
    new-instance v2, Lcom/google/maps/g/e/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/e/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/e/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/e/c;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e/a;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/e/c;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e/a;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/g/e/c;->d:I

    iput v4, v2, Lcom/google/maps/g/e/a;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/maps/g/e/c;->e:I

    iput v4, v2, Lcom/google/maps/g/e/a;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/e/c;->f:I

    iput v4, v2, Lcom/google/maps/g/e/a;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/e/a;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/e/c;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/e/c;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/e/a;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/e/c;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/e/c;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/e/c;->i:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/e/a;->i:Lcom/google/n/f;

    iput v0, v2, Lcom/google/maps/g/e/a;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 578
    check-cast p1, Lcom/google/maps/g/e/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/e/c;->a(Lcom/google/maps/g/e/a;)Lcom/google/maps/g/e/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 694
    iget v0, p0, Lcom/google/maps/g/e/c;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 695
    iget-object v0, p0, Lcom/google/maps/g/e/c;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 700
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 694
    goto :goto_0

    :cond_1
    move v0, v2

    .line 700
    goto :goto_1
.end method
