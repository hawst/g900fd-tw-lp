.class public final Lcom/google/maps/g/kk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ki;",
        "Lcom/google/maps/g/kk;",
        ">;",
        "Lcom/google/maps/g/kl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 620
    sget-object v0, Lcom/google/maps/g/ki;->j:Lcom/google/maps/g/ki;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 807
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    .line 944
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    .line 1081
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    .line 1218
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    .line 1354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/kk;->h:Lcom/google/n/ao;

    .line 1414
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    .line 1551
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    .line 621
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ki;)Lcom/google/maps/g/kk;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 696
    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 765
    :goto_0
    return-object p0

    .line 697
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ki;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 698
    iget-boolean v2, p1, Lcom/google/maps/g/ki;->b:Z

    iget v3, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/kk;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/kk;->b:Z

    .line 700
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 701
    iget-object v2, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 702
    iget-object v2, p1, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    .line 703
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 710
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 711
    iget-object v2, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 712
    iget-object v2, p1, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    .line 713
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 720
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 721
    iget-object v2, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 722
    iget-object v2, p1, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    .line 723
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 730
    :cond_4
    :goto_4
    iget-object v2, p1, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 731
    iget-object v2, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 732
    iget-object v2, p1, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    .line 733
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 740
    :cond_5
    :goto_5
    iget v2, p1, Lcom/google/maps/g/ki;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_12

    :goto_6
    if-eqz v0, :cond_6

    .line 741
    iget-object v0, p0, Lcom/google/maps/g/kk;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ki;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 742
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/kk;->a:I

    .line 744
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 745
    iget-object v0, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 746
    iget-object v0, p1, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    .line 747
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/maps/g/kk;->a:I

    .line 754
    :cond_7
    :goto_7
    iget-object v0, p1, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 755
    iget-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 756
    iget-object v0, p1, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    .line 757
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/maps/g/kk;->a:I

    .line 764
    :cond_8
    :goto_8
    iget-object v0, p1, Lcom/google/maps/g/ki;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 697
    goto/16 :goto_1

    .line 705
    :cond_a
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 706
    :cond_b
    iget-object v2, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 715
    :cond_c
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_d

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 716
    :cond_d
    iget-object v2, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 725
    :cond_e
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-eq v2, v3, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 726
    :cond_f
    iget-object v2, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 735
    :cond_10
    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-eq v2, v3, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/kk;->a:I

    .line 736
    :cond_11
    iget-object v2, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_12
    move v0, v1

    .line 740
    goto/16 :goto_6

    .line 749
    :cond_13
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_14

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/kk;->a:I

    .line 750
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 759
    :cond_15
    invoke-virtual {p0}, Lcom/google/maps/g/kk;->c()V

    .line 760
    iget-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 612
    new-instance v2, Lcom/google/maps/g/ki;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ki;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-boolean v4, p0, Lcom/google/maps/g/kk;->b:Z

    iput-boolean v4, v2, Lcom/google/maps/g/ki;->b:Z

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/kk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/kk;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/kk;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/kk;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/maps/g/kk;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/kk;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/maps/g/kk;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/kk;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x2

    :cond_4
    iget-object v3, v2, Lcom/google/maps/g/ki;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/kk;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/kk;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/maps/g/kk;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/kk;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/g/kk;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/ki;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 612
    check-cast p1, Lcom/google/maps/g/ki;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/kk;->a(Lcom/google/maps/g/ki;)Lcom/google/maps/g/kk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 769
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1553
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 1554
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    .line 1557
    iget v0, p0, Lcom/google/maps/g/kk;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/kk;->a:I

    .line 1559
    :cond_0
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/tp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1565
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    .line 1566
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1567
    iget-object v0, p0, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1568
    invoke-static {}, Lcom/google/maps/g/tp;->d()Lcom/google/maps/g/tp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tp;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1570
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
