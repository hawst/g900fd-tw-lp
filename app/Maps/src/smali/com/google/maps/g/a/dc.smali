.class public final Lcom/google/maps/g/a/dc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/da;",
        "Lcom/google/maps/g/a/dc;",
        ">;",
        "Lcom/google/maps/g/a/dt;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:Z

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:Ljava/lang/Object;

.field private o:I

.field private p:Lcom/google/n/ao;

.field private q:Lcom/google/n/ao;

.field private r:I

.field private s:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1572
    sget-object v0, Lcom/google/maps/g/a/da;->t:Lcom/google/maps/g/a/da;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1800
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/dc;->b:I

    .line 1836
    iput v1, p0, Lcom/google/maps/g/a/dc;->c:I

    .line 1872
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->d:Ljava/lang/Object;

    .line 1948
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->e:Ljava/lang/Object;

    .line 2024
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->f:Ljava/lang/Object;

    .line 2100
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->g:Lcom/google/n/ao;

    .line 2191
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->i:Lcom/google/n/ao;

    .line 2250
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->j:Lcom/google/n/ao;

    .line 2309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->k:Lcom/google/n/ao;

    .line 2368
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->l:Lcom/google/n/ao;

    .line 2427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->m:Lcom/google/n/ao;

    .line 2486
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->n:Ljava/lang/Object;

    .line 2562
    const/16 v0, 0x7f

    iput v0, p0, Lcom/google/maps/g/a/dc;->o:I

    .line 2598
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->p:Lcom/google/n/ao;

    .line 2657
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->q:Lcom/google/n/ao;

    .line 2716
    iput v1, p0, Lcom/google/maps/g/a/dc;->r:I

    .line 2752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dc;->s:Lcom/google/n/ao;

    .line 1573
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;
    .locals 7

    .prologue
    const/high16 v6, 0x20000

    const/high16 v5, 0x10000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1718
    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1791
    :goto_0
    return-object p0

    .line 1719
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1720
    iget v2, p1, Lcom/google/maps/g/a/da;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/dd;->a(I)Lcom/google/maps/g/a/dd;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1719
    goto :goto_1

    .line 1720
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/dc;->a:I

    iget v2, v2, Lcom/google/maps/g/a/dd;->d:I

    iput v2, p0, Lcom/google/maps/g/a/dc;->b:I

    .line 1722
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 1723
    iget v2, p1, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1722
    goto :goto_2

    .line 1723
    :cond_7
    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/dc;->a:I

    iget v2, v2, Lcom/google/maps/g/a/dl;->m:I

    iput v2, p0, Lcom/google/maps/g/a/dc;->c:I

    .line 1725
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 1726
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1727
    iget-object v2, p1, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/dc;->d:Ljava/lang/Object;

    .line 1730
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 1731
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1732
    iget-object v2, p1, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/dc;->e:Ljava/lang/Object;

    .line 1735
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 1736
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1737
    iget-object v2, p1, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/dc;->f:Ljava/lang/Object;

    .line 1740
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    .line 1741
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1742
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1744
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_7
    if-eqz v2, :cond_d

    .line 1745
    iget-boolean v2, p1, Lcom/google/maps/g/a/da;->h:Z

    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/dc;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dc;->h:Z

    .line 1747
    :cond_d
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_8
    if-eqz v2, :cond_e

    .line 1748
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1749
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1751
    :cond_e
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_9
    if-eqz v2, :cond_f

    .line 1752
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1753
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1755
    :cond_f
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_a
    if-eqz v2, :cond_10

    .line 1756
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1757
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1759
    :cond_10
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_b
    if-eqz v2, :cond_11

    .line 1760
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1761
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1763
    :cond_11
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_c
    if-eqz v2, :cond_12

    .line 1764
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1765
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1767
    :cond_12
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_d
    if-eqz v2, :cond_13

    .line 1768
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1769
    iget-object v2, p1, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/dc;->n:Ljava/lang/Object;

    .line 1772
    :cond_13
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_e
    if-eqz v2, :cond_22

    .line 1773
    iget v2, p1, Lcom/google/maps/g/a/da;->o:I

    invoke-static {v2}, Lcom/google/maps/g/a/ca;->a(I)Lcom/google/maps/g/a/ca;

    move-result-object v2

    if-nez v2, :cond_14

    sget-object v2, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    :cond_14
    if-nez v2, :cond_21

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    move v2, v1

    .line 1725
    goto/16 :goto_3

    :cond_16
    move v2, v1

    .line 1730
    goto/16 :goto_4

    :cond_17
    move v2, v1

    .line 1735
    goto/16 :goto_5

    :cond_18
    move v2, v1

    .line 1740
    goto/16 :goto_6

    :cond_19
    move v2, v1

    .line 1744
    goto/16 :goto_7

    :cond_1a
    move v2, v1

    .line 1747
    goto/16 :goto_8

    :cond_1b
    move v2, v1

    .line 1751
    goto/16 :goto_9

    :cond_1c
    move v2, v1

    .line 1755
    goto/16 :goto_a

    :cond_1d
    move v2, v1

    .line 1759
    goto :goto_b

    :cond_1e
    move v2, v1

    .line 1763
    goto :goto_c

    :cond_1f
    move v2, v1

    .line 1767
    goto :goto_d

    :cond_20
    move v2, v1

    .line 1772
    goto :goto_e

    .line 1773
    :cond_21
    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/a/dc;->a:I

    iget v2, v2, Lcom/google/maps/g/a/ca;->f:I

    iput v2, p0, Lcom/google/maps/g/a/dc;->o:I

    .line 1775
    :cond_22
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_f
    if-eqz v2, :cond_23

    .line 1776
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->p:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1777
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1779
    :cond_23
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_27

    move v2, v0

    :goto_10
    if-eqz v2, :cond_24

    .line 1780
    iget-object v2, p0, Lcom/google/maps/g/a/dc;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1781
    iget v2, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1783
    :cond_24
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_28

    move v2, v0

    :goto_11
    if-eqz v2, :cond_2a

    .line 1784
    iget v2, p1, Lcom/google/maps/g/a/da;->r:I

    invoke-static {v2}, Lcom/google/maps/g/a/df;->a(I)Lcom/google/maps/g/a/df;

    move-result-object v2

    if-nez v2, :cond_25

    sget-object v2, Lcom/google/maps/g/a/df;->a:Lcom/google/maps/g/a/df;

    :cond_25
    if-nez v2, :cond_29

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_26
    move v2, v1

    .line 1775
    goto :goto_f

    :cond_27
    move v2, v1

    .line 1779
    goto :goto_10

    :cond_28
    move v2, v1

    .line 1783
    goto :goto_11

    .line 1784
    :cond_29
    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/maps/g/a/dc;->a:I

    iget v2, v2, Lcom/google/maps/g/a/df;->d:I

    iput v2, p0, Lcom/google/maps/g/a/dc;->r:I

    .line 1786
    :cond_2a
    iget v2, p1, Lcom/google/maps/g/a/da;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2c

    :goto_12
    if-eqz v0, :cond_2b

    .line 1787
    iget-object v0, p0, Lcom/google/maps/g/a/dc;->s:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1788
    iget v0, p0, Lcom/google/maps/g/a/dc;->a:I

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1790
    :cond_2b
    iget-object v0, p1, Lcom/google/maps/g/a/da;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_2c
    move v0, v1

    .line 1786
    goto :goto_12
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1564
    invoke-virtual {p0}, Lcom/google/maps/g/a/dc;->c()Lcom/google/maps/g/a/da;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1564
    check-cast p1, Lcom/google/maps/g/a/da;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1795
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/da;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1620
    new-instance v2, Lcom/google/maps/g/a/da;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/da;-><init>(Lcom/google/n/v;)V

    .line 1621
    iget v3, p0, Lcom/google/maps/g/a/dc;->a:I

    .line 1623
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_11

    .line 1626
    :goto_0
    iget v4, p0, Lcom/google/maps/g/a/dc;->b:I

    iput v4, v2, Lcom/google/maps/g/a/da;->b:I

    .line 1627
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1628
    or-int/lit8 v0, v0, 0x2

    .line 1630
    :cond_0
    iget v4, p0, Lcom/google/maps/g/a/dc;->c:I

    iput v4, v2, Lcom/google/maps/g/a/da;->c:I

    .line 1631
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1632
    or-int/lit8 v0, v0, 0x4

    .line 1634
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/dc;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    .line 1635
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1636
    or-int/lit8 v0, v0, 0x8

    .line 1638
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/dc;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    .line 1639
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 1640
    or-int/lit8 v0, v0, 0x10

    .line 1642
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/dc;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    .line 1643
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 1644
    or-int/lit8 v0, v0, 0x20

    .line 1646
    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->g:Lcom/google/n/ao;

    .line 1647
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->g:Lcom/google/n/ao;

    .line 1648
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1646
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1649
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 1650
    or-int/lit8 v0, v0, 0x40

    .line 1652
    :cond_5
    iget-boolean v4, p0, Lcom/google/maps/g/a/dc;->h:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/da;->h:Z

    .line 1653
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 1654
    or-int/lit16 v0, v0, 0x80

    .line 1656
    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->i:Lcom/google/n/ao;

    .line 1657
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->i:Lcom/google/n/ao;

    .line 1658
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1656
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1659
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 1660
    or-int/lit16 v0, v0, 0x100

    .line 1662
    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->j:Lcom/google/n/ao;

    .line 1663
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->j:Lcom/google/n/ao;

    .line 1664
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1662
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1665
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 1666
    or-int/lit16 v0, v0, 0x200

    .line 1668
    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->k:Lcom/google/n/ao;

    .line 1669
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->k:Lcom/google/n/ao;

    .line 1670
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1668
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1671
    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 1672
    or-int/lit16 v0, v0, 0x400

    .line 1674
    :cond_9
    iget-object v4, v2, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->l:Lcom/google/n/ao;

    .line 1675
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->l:Lcom/google/n/ao;

    .line 1676
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1674
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1677
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 1678
    or-int/lit16 v0, v0, 0x800

    .line 1680
    :cond_a
    iget-object v4, v2, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->m:Lcom/google/n/ao;

    .line 1681
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->m:Lcom/google/n/ao;

    .line 1682
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1680
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1683
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 1684
    or-int/lit16 v0, v0, 0x1000

    .line 1686
    :cond_b
    iget-object v4, p0, Lcom/google/maps/g/a/dc;->n:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    .line 1687
    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    .line 1688
    or-int/lit16 v0, v0, 0x2000

    .line 1690
    :cond_c
    iget v4, p0, Lcom/google/maps/g/a/dc;->o:I

    iput v4, v2, Lcom/google/maps/g/a/da;->o:I

    .line 1691
    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    .line 1692
    or-int/lit16 v0, v0, 0x4000

    .line 1694
    :cond_d
    iget-object v4, v2, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->p:Lcom/google/n/ao;

    .line 1695
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->p:Lcom/google/n/ao;

    .line 1696
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1694
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1697
    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    .line 1698
    or-int/2addr v0, v7

    .line 1700
    :cond_e
    iget-object v4, v2, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->q:Lcom/google/n/ao;

    .line 1701
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dc;->q:Lcom/google/n/ao;

    .line 1702
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1700
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1703
    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    .line 1704
    or-int/2addr v0, v8

    .line 1706
    :cond_f
    iget v4, p0, Lcom/google/maps/g/a/dc;->r:I

    iput v4, v2, Lcom/google/maps/g/a/da;->r:I

    .line 1707
    and-int/2addr v3, v9

    if-ne v3, v9, :cond_10

    .line 1708
    or-int/2addr v0, v9

    .line 1710
    :cond_10
    iget-object v3, v2, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/dc;->s:Lcom/google/n/ao;

    .line 1711
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/dc;->s:Lcom/google/n/ao;

    .line 1712
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1710
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 1713
    iput v0, v2, Lcom/google/maps/g/a/da;->a:I

    .line 1714
    return-object v2

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method
