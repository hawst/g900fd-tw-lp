.class public final Lcom/google/maps/g/mx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/my;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/mv;",
        "Lcom/google/maps/g/mx;",
        ">;",
        "Lcom/google/maps/g/my;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/maps/g/lc;

.field private e:Ljava/lang/Object;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 564
    sget-object v0, Lcom/google/maps/g/mv;->g:Lcom/google/maps/g/mv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 652
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mx;->b:Ljava/lang/Object;

    .line 728
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mx;->c:Ljava/lang/Object;

    .line 804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    .line 865
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mx;->e:Ljava/lang/Object;

    .line 941
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/mx;->f:I

    .line 565
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/mv;)Lcom/google/maps/g/mx;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 614
    invoke-static {}, Lcom/google/maps/g/mv;->d()Lcom/google/maps/g/mv;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 637
    :goto_0
    return-object p0

    .line 615
    :cond_0
    iget v0, p1, Lcom/google/maps/g/mv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 616
    iget v0, p0, Lcom/google/maps/g/mx;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/mx;->a:I

    .line 617
    iget-object v0, p1, Lcom/google/maps/g/mv;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/mx;->b:Ljava/lang/Object;

    .line 620
    :cond_1
    iget v0, p1, Lcom/google/maps/g/mv;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 621
    iget v0, p0, Lcom/google/maps/g/mx;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/mx;->a:I

    .line 622
    iget-object v0, p1, Lcom/google/maps/g/mv;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/mx;->c:Ljava/lang/Object;

    .line 625
    :cond_2
    iget v0, p1, Lcom/google/maps/g/mv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 626
    iget-object v0, p1, Lcom/google/maps/g/mv;->d:Lcom/google/maps/g/lc;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v0

    :goto_4
    iget v3, p0, Lcom/google/maps/g/mx;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v4, :cond_a

    iget-object v3, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    invoke-static {v3}, Lcom/google/maps/g/lc;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/le;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/le;->c()Lcom/google/maps/g/lc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    :goto_5
    iget v0, p0, Lcom/google/maps/g/mx;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/mx;->a:I

    .line 628
    :cond_3
    iget v0, p1, Lcom/google/maps/g/mv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_6
    if-eqz v0, :cond_4

    .line 629
    iget v0, p0, Lcom/google/maps/g/mx;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/mx;->a:I

    .line 630
    iget-object v0, p1, Lcom/google/maps/g/mv;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/mx;->e:Ljava/lang/Object;

    .line 633
    :cond_4
    iget v0, p1, Lcom/google/maps/g/mv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_7
    if-eqz v0, :cond_e

    .line 634
    iget v0, p1, Lcom/google/maps/g/mv;->f:I

    invoke-static {v0}, Lcom/google/maps/g/na;->a(I)Lcom/google/maps/g/na;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/na;->a:Lcom/google/maps/g/na;

    :cond_5
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    .line 615
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 620
    goto :goto_2

    :cond_8
    move v0, v2

    .line 625
    goto :goto_3

    .line 626
    :cond_9
    iget-object v0, p1, Lcom/google/maps/g/mv;->d:Lcom/google/maps/g/lc;

    goto :goto_4

    :cond_a
    iput-object v0, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    goto :goto_5

    :cond_b
    move v0, v2

    .line 628
    goto :goto_6

    :cond_c
    move v0, v2

    .line 633
    goto :goto_7

    .line 634
    :cond_d
    iget v1, p0, Lcom/google/maps/g/mx;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/mx;->a:I

    iget v0, v0, Lcom/google/maps/g/na;->d:I

    iput v0, p0, Lcom/google/maps/g/mx;->f:I

    .line 636
    :cond_e
    iget-object v0, p1, Lcom/google/maps/g/mv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 556
    new-instance v2, Lcom/google/maps/g/mv;

    invoke-direct {v2, p0}, Lcom/google/maps/g/mv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/mx;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/mx;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/mv;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/mx;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/mv;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    iput-object v1, v2, Lcom/google/maps/g/mv;->d:Lcom/google/maps/g/lc;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/mx;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/mv;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/maps/g/mx;->f:I

    iput v1, v2, Lcom/google/maps/g/mv;->f:I

    iput v0, v2, Lcom/google/maps/g/mv;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 556
    check-cast p1, Lcom/google/maps/g/mv;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/mx;->a(Lcom/google/maps/g/mv;)Lcom/google/maps/g/mx;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 641
    iget v0, p0, Lcom/google/maps/g/mx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 642
    iget-object v0, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/lc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 647
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 641
    goto :goto_0

    .line 642
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/mx;->d:Lcom/google/maps/g/lc;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 647
    goto :goto_2
.end method
