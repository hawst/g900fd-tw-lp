.class public final Lcom/google/maps/g/tz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ua;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/tx;",
        "Lcom/google/maps/g/tz;",
        ">;",
        "Lcom/google/maps/g/ua;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 642
    sget-object v0, Lcom/google/maps/g/tx;->e:Lcom/google/maps/g/tx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 706
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tz;->b:Ljava/lang/Object;

    .line 782
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tz;->c:Ljava/lang/Object;

    .line 858
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tz;->d:Ljava/lang/Object;

    .line 643
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 680
    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 697
    :goto_0
    return-object p0

    .line 681
    :cond_0
    iget v2, p1, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 682
    iget v2, p0, Lcom/google/maps/g/tz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/tz;->a:I

    .line 683
    iget-object v2, p1, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/tz;->b:Ljava/lang/Object;

    .line 686
    :cond_1
    iget v2, p1, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 687
    iget v2, p0, Lcom/google/maps/g/tz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/tz;->a:I

    .line 688
    iget-object v2, p1, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/tz;->c:Ljava/lang/Object;

    .line 691
    :cond_2
    iget v2, p1, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 692
    iget v0, p0, Lcom/google/maps/g/tz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/tz;->a:I

    .line 693
    iget-object v0, p1, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/tz;->d:Ljava/lang/Object;

    .line 696
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/tx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 681
    goto :goto_1

    :cond_5
    move v2, v1

    .line 686
    goto :goto_2

    :cond_6
    move v0, v1

    .line 691
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 634
    invoke-virtual {p0}, Lcom/google/maps/g/tz;->c()Lcom/google/maps/g/tx;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 634
    check-cast p1, Lcom/google/maps/g/tx;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/tz;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/tx;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 660
    new-instance v2, Lcom/google/maps/g/tx;

    invoke-direct {v2, p0}, Lcom/google/maps/g/tx;-><init>(Lcom/google/n/v;)V

    .line 661
    iget v3, p0, Lcom/google/maps/g/tz;->a:I

    .line 662
    const/4 v1, 0x0

    .line 663
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 666
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/tz;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    .line 667
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 668
    or-int/lit8 v0, v0, 0x2

    .line 670
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/tz;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    .line 671
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 672
    or-int/lit8 v0, v0, 0x4

    .line 674
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/tz;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    .line 675
    iput v0, v2, Lcom/google/maps/g/tx;->a:I

    .line 676
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
