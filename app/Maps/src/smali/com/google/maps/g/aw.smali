.class public final Lcom/google/maps/g/aw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/az;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/aw;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/aw;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/google/maps/g/ax;

    invoke-direct {v0}, Lcom/google/maps/g/ax;-><init>()V

    sput-object v0, Lcom/google/maps/g/aw;->PARSER:Lcom/google/n/ax;

    .line 295
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/aw;->i:Lcom/google/n/aw;

    .line 712
    new-instance v0, Lcom/google/maps/g/aw;

    invoke-direct {v0}, Lcom/google/maps/g/aw;-><init>()V

    sput-object v0, Lcom/google/maps/g/aw;->f:Lcom/google/maps/g/aw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 238
    iput-byte v0, p0, Lcom/google/maps/g/aw;->g:B

    .line 266
    iput v0, p0, Lcom/google/maps/g/aw;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/aw;->e:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/aw;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    const/4 v0, 0x0

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 49
    iget v4, p0, Lcom/google/maps/g/aw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/aw;->a:I

    .line 50
    iput-object v3, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/aw;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 55
    iget v4, p0, Lcom/google/maps/g/aw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/aw;->a:I

    .line 56
    iput-object v3, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    .line 75
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 61
    iget v4, p0, Lcom/google/maps/g/aw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/aw;->a:I

    .line 62
    iput-object v3, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_4
    iget v3, p0, Lcom/google/maps/g/aw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/aw;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/aw;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->au:Lcom/google/n/bn;

    .line 79
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 238
    iput-byte v0, p0, Lcom/google/maps/g/aw;->g:B

    .line 266
    iput v0, p0, Lcom/google/maps/g/aw;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/aw;
    .locals 1

    .prologue
    .line 715
    sget-object v0, Lcom/google/maps/g/aw;->f:Lcom/google/maps/g/aw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ay;
    .locals 1

    .prologue
    .line 357
    new-instance v0, Lcom/google/maps/g/ay;

    invoke-direct {v0}, Lcom/google/maps/g/ay;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/aw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcom/google/maps/g/aw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 250
    invoke-virtual {p0}, Lcom/google/maps/g/aw;->c()I

    .line 251
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 254
    :cond_0
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 255
    iget-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 257
    :cond_1
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 258
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 260
    :cond_2
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 261
    iget v0, p0, Lcom/google/maps/g/aw;->e:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 263
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/aw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 264
    return-void

    .line 252
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 255
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 258
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 261
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240
    iget-byte v1, p0, Lcom/google/maps/g/aw;->g:B

    .line 241
    if-ne v1, v0, :cond_0

    .line 245
    :goto_0
    return v0

    .line 242
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/aw;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 268
    iget v0, p0, Lcom/google/maps/g/aw;->h:I

    .line 269
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 290
    :goto_0
    return v0

    .line 272
    :cond_0
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 274
    iget-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 276
    :goto_2
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 278
    iget-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 280
    :cond_1
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 281
    const/4 v3, 0x3

    .line 282
    iget-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 284
    :cond_2
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 285
    iget v0, p0, Lcom/google/maps/g/aw;->e:I

    .line 286
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/aw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 289
    iput v0, p0, Lcom/google/maps/g/aw;->h:I

    goto/16 :goto_0

    .line 274
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 278
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 282
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 286
    :cond_7
    const/16 v0, 0xa

    goto :goto_5

    :cond_8
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/aw;->newBuilder()Lcom/google/maps/g/ay;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ay;->a(Lcom/google/maps/g/aw;)Lcom/google/maps/g/ay;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/aw;->newBuilder()Lcom/google/maps/g/ay;

    move-result-object v0

    return-object v0
.end method
