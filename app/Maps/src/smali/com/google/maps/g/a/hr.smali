.class public final enum Lcom/google/maps/g/a/hr;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/hr;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/hr;

.field public static final enum b:Lcom/google/maps/g/a/hr;

.field public static final enum c:Lcom/google/maps/g/a/hr;

.field private static final synthetic e:[Lcom/google/maps/g/a/hr;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 107
    new-instance v0, Lcom/google/maps/g/a/hr;

    const-string v1, "BLENDED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/a/hr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    .line 111
    new-instance v0, Lcom/google/maps/g/a/hr;

    const-string v1, "UNIFORM"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/hr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hr;->b:Lcom/google/maps/g/a/hr;

    .line 115
    new-instance v0, Lcom/google/maps/g/a/hr;

    const-string v1, "STRICT"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/g/a/hr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    .line 102
    new-array v0, v5, [Lcom/google/maps/g/a/hr;

    sget-object v1, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/hr;->b:Lcom/google/maps/g/a/hr;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/a/hr;->e:[Lcom/google/maps/g/a/hr;

    .line 150
    new-instance v0, Lcom/google/maps/g/a/hs;

    invoke-direct {v0}, Lcom/google/maps/g/a/hs;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 160
    iput p3, p0, Lcom/google/maps/g/a/hr;->d:I

    .line 161
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/hr;
    .locals 1

    .prologue
    .line 137
    packed-switch p0, :pswitch_data_0

    .line 141
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 138
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    goto :goto_0

    .line 139
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/hr;->b:Lcom/google/maps/g/a/hr;

    goto :goto_0

    .line 140
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/hr;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/google/maps/g/a/hr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hr;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/hr;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/a/hr;->e:[Lcom/google/maps/g/a/hr;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/hr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/hr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/maps/g/a/hr;->d:I

    return v0
.end method
