.class public final Lcom/google/maps/g/c/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/c/f;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/c/a;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/c/a;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/aq;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/aq;

.field h:Ljava/lang/Object;

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/maps/g/c/b;

    invoke-direct {v0}, Lcom/google/maps/g/c/b;-><init>()V

    sput-object v0, Lcom/google/maps/g/c/a;->PARSER:Lcom/google/n/ax;

    .line 988
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/c/a;->m:Lcom/google/n/aw;

    .line 1787
    new-instance v0, Lcom/google/maps/g/c/a;

    invoke-direct {v0}, Lcom/google/maps/g/c/a;-><init>()V

    sput-object v0, Lcom/google/maps/g/c/a;->j:Lcom/google/maps/g/c/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 878
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    .line 893
    iput-byte v1, p0, Lcom/google/maps/g/c/a;->k:B

    .line 933
    iput v1, p0, Lcom/google/maps/g/c/a;->l:I

    .line 18
    iput v1, p0, Lcom/google/maps/g/c/a;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x20

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/c/a;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 40
    sparse-switch v4, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 53
    invoke-static {v4}, Lcom/google/maps/g/c/d;->a(I)Lcom/google/maps/g/c/d;

    move-result-object v5

    .line 54
    if-nez v5, :cond_3

    .line 55
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x4

    if-ne v2, v6, :cond_1

    .line 118
    iget-object v2, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    .line 120
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v7, :cond_2

    .line 121
    iget-object v1, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    .line 123
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/c/a;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_3
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/c/a;->a:I

    .line 58
    iput v4, p0, Lcom/google/maps/g/c/a;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 115
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 64
    iget v5, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/c/a;->a:I

    .line 65
    iput-object v4, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    goto :goto_0

    .line 69
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 70
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v6, :cond_4

    .line 71
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    .line 72
    or-int/lit8 v1, v1, 0x4

    .line 74
    :cond_4
    iget-object v5, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 79
    iget v5, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/c/a;->a:I

    .line 80
    iput-object v4, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 84
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 85
    and-int/lit8 v5, v1, 0x20

    if-eq v5, v7, :cond_5

    .line 86
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    .line 87
    or-int/lit8 v1, v1, 0x20

    .line 89
    :cond_5
    iget-object v5, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 93
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 94
    iget v5, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/c/a;->a:I

    .line 95
    iput-object v4, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 99
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 100
    iget v5, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/c/a;->a:I

    .line 101
    iput-object v4, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 105
    :sswitch_8
    iget-object v4, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 106
    iget v4, p0, Lcom/google/maps/g/c/a;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/g/c/a;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 117
    :cond_6
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v6, :cond_7

    .line 118
    iget-object v0, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    .line 120
    :cond_7
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v7, :cond_8

    .line 121
    iget-object v0, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    .line 123
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->au:Lcom/google/n/bn;

    .line 124
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 878
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    .line 893
    iput-byte v1, p0, Lcom/google/maps/g/c/a;->k:B

    .line 933
    iput v1, p0, Lcom/google/maps/g/c/a;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/c/a;
    .locals 1

    .prologue
    .line 1790
    sget-object v0, Lcom/google/maps/g/c/a;->j:Lcom/google/maps/g/c/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/c/c;
    .locals 1

    .prologue
    .line 1050
    new-instance v0, Lcom/google/maps/g/c/c;

    invoke-direct {v0}, Lcom/google/maps/g/c/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/c/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v0, Lcom/google/maps/g/c/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 905
    invoke-virtual {p0}, Lcom/google/maps/g/c/a;->c()I

    .line 906
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 907
    iget v0, p0, Lcom/google/maps/g/c/a;->b:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 909
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 910
    iget-object v0, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v0, v1

    .line 912
    :goto_2
    iget-object v2, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 913
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 912
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 907
    :cond_2
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 910
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 915
    :cond_4
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 916
    iget-object v0, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 918
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 919
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v2, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 918
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 916
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 921
    :cond_7
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 922
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 924
    :cond_8
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_9

    .line 925
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 927
    :cond_9
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 928
    iget-object v0, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 930
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/c/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 931
    return-void

    .line 922
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 925
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 895
    iget-byte v1, p0, Lcom/google/maps/g/c/a;->k:B

    .line 896
    if-ne v1, v0, :cond_0

    .line 900
    :goto_0
    return v0

    .line 897
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 899
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/c/a;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 935
    iget v0, p0, Lcom/google/maps/g/c/a;->l:I

    .line 936
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 983
    :goto_0
    return v0

    .line 939
    :cond_0
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 940
    iget v0, p0, Lcom/google/maps/g/c/a;->b:I

    .line 941
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 943
    :goto_2
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 945
    iget-object v0, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v0, v2

    move v3, v2

    .line 949
    :goto_4
    iget-object v4, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 950
    iget-object v4, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    .line 951
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 941
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 945
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 953
    :cond_4
    add-int v0, v1, v3

    .line 954
    iget-object v1, p0, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 956
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_c

    .line 958
    iget-object v0, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    :goto_6
    move v1, v2

    move v3, v2

    .line 962
    :goto_7
    iget-object v4, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 963
    iget-object v4, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    .line 964
    invoke-interface {v4, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 962
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 958
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 966
    :cond_6
    add-int/2addr v0, v3

    .line 967
    iget-object v1, p0, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 969
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_7

    .line 970
    const/4 v3, 0x6

    .line 971
    iget-object v0, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 973
    :cond_7
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_8

    .line 974
    const/4 v3, 0x7

    .line 975
    iget-object v0, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 977
    :cond_8
    iget v0, p0, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_9

    .line 978
    iget-object v0, p0, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    .line 979
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 981
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/c/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 982
    iput v0, p0, Lcom/google/maps/g/c/a;->l:I

    goto/16 :goto_0

    .line 971
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 975
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_c
    move v0, v1

    goto/16 :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/c/a;->newBuilder()Lcom/google/maps/g/c/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/c/c;->a(Lcom/google/maps/g/c/a;)Lcom/google/maps/g/c/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/c/a;->newBuilder()Lcom/google/maps/g/c/c;

    move-result-object v0

    return-object v0
.end method
