.class public final Lcom/google/maps/g/ag;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ah;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/m;",
        "Lcom/google/maps/g/ag;",
        ">;",
        "Lcom/google/maps/g/ah;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3198
    sget-object v0, Lcom/google/maps/g/m;->j:Lcom/google/maps/g/m;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3343
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ag;->b:I

    .line 3411
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->d:Lcom/google/n/ao;

    .line 3470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->e:Lcom/google/n/ao;

    .line 3529
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->f:Lcom/google/n/ao;

    .line 3588
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->g:Lcom/google/n/ao;

    .line 3647
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->h:Lcom/google/n/ao;

    .line 3706
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ag;->i:Lcom/google/n/ao;

    .line 3199
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/m;)Lcom/google/maps/g/ag;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3278
    invoke-static {}, Lcom/google/maps/g/m;->d()Lcom/google/maps/g/m;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3310
    :goto_0
    return-object p0

    .line 3279
    :cond_0
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 3280
    iget v2, p1, Lcom/google/maps/g/m;->b:I

    invoke-static {v2}, Lcom/google/o/f/j;->a(I)Lcom/google/o/f/j;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/f/j;->a:Lcom/google/o/f/j;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 3279
    goto :goto_1

    .line 3280
    :cond_3
    iget v3, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ag;->a:I

    iget v2, v2, Lcom/google/o/f/j;->e:I

    iput v2, p0, Lcom/google/maps/g/ag;->b:I

    .line 3282
    :cond_4
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 3283
    iget v2, p1, Lcom/google/maps/g/m;->c:I

    iget v3, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/ag;->a:I

    iput v2, p0, Lcom/google/maps/g/ag;->c:I

    .line 3285
    :cond_5
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 3286
    iget-object v2, p0, Lcom/google/maps/g/ag;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3287
    iget v2, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ag;->a:I

    .line 3289
    :cond_6
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 3290
    iget-object v2, p0, Lcom/google/maps/g/ag;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3291
    iget v2, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ag;->a:I

    .line 3293
    :cond_7
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 3294
    iget-object v2, p0, Lcom/google/maps/g/ag;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3295
    iget v2, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/ag;->a:I

    .line 3297
    :cond_8
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 3298
    iget-object v2, p0, Lcom/google/maps/g/ag;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3299
    iget v2, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/ag;->a:I

    .line 3301
    :cond_9
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 3302
    iget-object v2, p0, Lcom/google/maps/g/ag;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3303
    iget v2, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/ag;->a:I

    .line 3305
    :cond_a
    iget v2, p1, Lcom/google/maps/g/m;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 3306
    iget-object v0, p0, Lcom/google/maps/g/ag;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3307
    iget v0, p0, Lcom/google/maps/g/ag;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/ag;->a:I

    .line 3309
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/m;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 3282
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 3285
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 3289
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 3293
    goto :goto_5

    :cond_10
    move v2, v1

    .line 3297
    goto :goto_6

    :cond_11
    move v2, v1

    .line 3301
    goto :goto_7

    :cond_12
    move v0, v1

    .line 3305
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3190
    new-instance v2, Lcom/google/maps/g/m;

    invoke-direct {v2, p0}, Lcom/google/maps/g/m;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ag;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/maps/g/ag;->b:I

    iput v4, v2, Lcom/google/maps/g/m;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/maps/g/ag;->c:I

    iput v4, v2, Lcom/google/maps/g/m;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/m;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ag;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ag;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ag;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ag;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/m;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ag;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ag;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/m;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ag;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ag;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/m;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ag;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ag;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/maps/g/m;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/ag;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/ag;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/m;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3190
    check-cast p1, Lcom/google/maps/g/m;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ag;->a(Lcom/google/maps/g/m;)Lcom/google/maps/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3314
    iget v0, p0, Lcom/google/maps/g/ag;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 3315
    iget-object v0, p0, Lcom/google/maps/g/ag;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/vb;->d()Lcom/google/maps/g/vb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vb;

    invoke-virtual {v0}, Lcom/google/maps/g/vb;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 3338
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 3314
    goto :goto_0

    .line 3320
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ag;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 3321
    iget-object v0, p0, Lcom/google/maps/g/ag;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/vb;->d()Lcom/google/maps/g/vb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vb;

    invoke-virtual {v0}, Lcom/google/maps/g/vb;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 3323
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3320
    goto :goto_2

    .line 3326
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ag;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 3327
    iget-object v0, p0, Lcom/google/maps/g/ag;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/dc;->d()Lcom/google/maps/g/dc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dc;

    invoke-virtual {v0}, Lcom/google/maps/g/dc;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 3329
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3326
    goto :goto_3

    .line 3332
    :cond_5
    iget v0, p0, Lcom/google/maps/g/ag;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 3333
    iget-object v0, p0, Lcom/google/maps/g/ag;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ms;->d()Lcom/google/maps/g/ms;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ms;

    invoke-virtual {v0}, Lcom/google/maps/g/ms;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 3335
    goto :goto_1

    :cond_6
    move v0, v1

    .line 3332
    goto :goto_4

    :cond_7
    move v0, v2

    .line 3338
    goto :goto_1
.end method
