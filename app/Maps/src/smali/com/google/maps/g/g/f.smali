.class public final Lcom/google/maps/g/g/f;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/g/d;",
        "Lcom/google/maps/g/g/f;",
        ">;",
        "Lcom/google/maps/g/g/i;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lcom/google/maps/g/g/d;->c:Lcom/google/maps/g/g/d;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 406
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/g/f;->b:I

    .line 367
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/g/d;)Lcom/google/maps/g/g/f;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 392
    invoke-static {}, Lcom/google/maps/g/g/d;->d()Lcom/google/maps/g/g/d;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 397
    :goto_0
    return-object p0

    .line 393
    :cond_0
    iget v1, p1, Lcom/google/maps/g/g/d;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 394
    iget v0, p1, Lcom/google/maps/g/g/d;->b:I

    invoke-static {v0}, Lcom/google/maps/g/g/g;->a(I)Lcom/google/maps/g/g/g;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 393
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 394
    :cond_3
    iget v1, p0, Lcom/google/maps/g/g/f;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/g/f;->a:I

    iget v0, v0, Lcom/google/maps/g/g/g;->c:I

    iput v0, p0, Lcom/google/maps/g/g/f;->b:I

    .line 396
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/g/d;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 358
    new-instance v2, Lcom/google/maps/g/g/d;

    invoke-direct {v2, p0}, Lcom/google/maps/g/g/d;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/g/f;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget v1, p0, Lcom/google/maps/g/g/f;->b:I

    iput v1, v2, Lcom/google/maps/g/g/d;->b:I

    iput v0, v2, Lcom/google/maps/g/g/d;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 358
    check-cast p1, Lcom/google/maps/g/g/d;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/g/f;->a(Lcom/google/maps/g/g/d;)Lcom/google/maps/g/g/f;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x1

    return v0
.end method
