.class public final Lcom/google/maps/g/w;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/u;",
        "Lcom/google/maps/g/w;",
        ">;",
        "Lcom/google/maps/g/ab;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/x;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1220
    sget-object v0, Lcom/google/maps/g/u;->c:Lcom/google/maps/g/u;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    .line 1221
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1246
    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1251
    :goto_0
    return-object p0

    .line 1247
    :cond_0
    iget v0, p1, Lcom/google/maps/g/u;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1248
    iget-object v0, p1, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/x;->d()Lcom/google/maps/g/x;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/maps/g/w;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    invoke-static {}, Lcom/google/maps/g/x;->d()Lcom/google/maps/g/x;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    invoke-static {v1}, Lcom/google/maps/g/x;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/z;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/z;->c()Lcom/google/maps/g/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/w;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/w;->a:I

    .line 1250
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/u;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1247
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1248
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    goto :goto_2

    :cond_4
    iput-object v0, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1212
    invoke-virtual {p0}, Lcom/google/maps/g/w;->c()Lcom/google/maps/g/u;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1212
    check-cast p1, Lcom/google/maps/g/u;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/w;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1255
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/u;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1234
    new-instance v2, Lcom/google/maps/g/u;

    invoke-direct {v2, p0}, Lcom/google/maps/g/u;-><init>(Lcom/google/n/v;)V

    .line 1235
    iget v3, p0, Lcom/google/maps/g/w;->a:I

    .line 1236
    const/4 v1, 0x0

    .line 1237
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 1240
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/w;->b:Lcom/google/maps/g/x;

    iput-object v1, v2, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    .line 1241
    iput v0, v2, Lcom/google/maps/g/u;->a:I

    .line 1242
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
