.class public final Lcom/google/maps/g/a/fw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/fw;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/a/fw;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/maps/g/a/fx;

    invoke-direct {v0}, Lcom/google/maps/g/a/fx;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/fw;->PARSER:Lcom/google/n/ax;

    .line 318
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/fw;->h:Lcom/google/n/aw;

    .line 762
    new-instance v0, Lcom/google/maps/g/a/fw;

    invoke-direct {v0}, Lcom/google/maps/g/a/fw;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/fw;->e:Lcom/google/maps/g/a/fw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 268
    iput-byte v0, p0, Lcom/google/maps/g/a/fw;->f:B

    .line 293
    iput v0, p0, Lcom/google/maps/g/a/fw;->g:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/fw;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/a/fw;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 35
    sparse-switch v4, :sswitch_data_0

    .line 40
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 48
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 49
    or-int/lit8 v1, v1, 0x1

    .line 51
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    sget-object v5, Lcom/google/maps/g/a/eo;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 82
    iget-object v2, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 84
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_3

    .line 85
    iget-object v1, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 87
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fw;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v6, :cond_4

    .line 56
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 57
    or-int/lit8 v1, v1, 0x2

    .line 59
    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    sget-object v5, Lcom/google/maps/g/a/da;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 64
    invoke-static {v4}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v5

    .line 65
    if-nez v5, :cond_5

    .line 66
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 68
    :cond_5
    iget v5, p0, Lcom/google/maps/g/a/fw;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/fw;->a:I

    .line 69
    iput v4, p0, Lcom/google/maps/g/a/fw;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 81
    :cond_6
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_7

    .line 82
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 84
    :cond_7
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_8

    .line 85
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 87
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fw;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 268
    iput-byte v0, p0, Lcom/google/maps/g/a/fw;->f:B

    .line 293
    iput v0, p0, Lcom/google/maps/g/a/fw;->g:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;
    .locals 1

    .prologue
    .line 383
    invoke-static {}, Lcom/google/maps/g/a/fw;->newBuilder()Lcom/google/maps/g/a/fy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/fw;
    .locals 1

    .prologue
    .line 765
    sget-object v0, Lcom/google/maps/g/a/fw;->e:Lcom/google/maps/g/a/fw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/fy;
    .locals 1

    .prologue
    .line 380
    new-instance v0, Lcom/google/maps/g/a/fy;

    invoke-direct {v0}, Lcom/google/maps/g/a/fy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/a/fw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 280
    invoke-virtual {p0}, Lcom/google/maps/g/a/fw;->c()I

    move v1, v2

    .line 281
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 284
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 284
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 287
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/fw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 288
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 290
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 291
    return-void

    .line 288
    :cond_3
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 270
    iget-byte v1, p0, Lcom/google/maps/g/a/fw;->f:B

    .line 271
    if-ne v1, v0, :cond_0

    .line 275
    :goto_0
    return v0

    .line 272
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 274
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/fw;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 295
    iget v0, p0, Lcom/google/maps/g/a/fw;->g:I

    .line 296
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 313
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 299
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 301
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 303
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 304
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 305
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 303
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 307
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/fw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3

    .line 308
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/fw;->d:I

    .line 309
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/fw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 312
    iput v0, p0, Lcom/google/maps/g/a/fw;->g:I

    goto :goto_0

    .line 309
    :cond_4
    const/16 v0, 0xa

    goto :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/fw;->newBuilder()Lcom/google/maps/g/a/fy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/fw;->newBuilder()Lcom/google/maps/g/a/fy;

    move-result-object v0

    return-object v0
.end method
