.class public final Lcom/google/maps/g/jl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/js;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/jl;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/jl;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/maps/g/jh;

.field c:I

.field public d:I

.field public e:I

.field public f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 811
    new-instance v0, Lcom/google/maps/g/jm;

    invoke-direct {v0}, Lcom/google/maps/g/jm;-><init>()V

    sput-object v0, Lcom/google/maps/g/jl;->PARSER:Lcom/google/n/ax;

    .line 1138
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/jl;->j:Lcom/google/n/aw;

    .line 1541
    new-instance v0, Lcom/google/maps/g/jl;

    invoke-direct {v0}, Lcom/google/maps/g/jl;-><init>()V

    sput-object v0, Lcom/google/maps/g/jl;->g:Lcom/google/maps/g/jl;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 724
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1074
    iput-byte v0, p0, Lcom/google/maps/g/jl;->h:B

    .line 1105
    iput v0, p0, Lcom/google/maps/g/jl;->i:I

    .line 725
    iput v1, p0, Lcom/google/maps/g/jl;->c:I

    .line 726
    iput v1, p0, Lcom/google/maps/g/jl;->d:I

    .line 727
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/jl;->e:I

    .line 728
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    .line 729
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 735
    invoke-direct {p0}, Lcom/google/maps/g/jl;-><init>()V

    .line 736
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 740
    const/4 v0, 0x0

    move v2, v0

    .line 741
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 742
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 743
    sparse-switch v0, :sswitch_data_0

    .line 748
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 750
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 746
    goto :goto_0

    .line 755
    :sswitch_1
    const/4 v0, 0x0

    .line 756
    iget v1, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_5

    .line 757
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    invoke-static {}, Lcom/google/maps/g/jh;->newBuilder()Lcom/google/maps/g/jj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/jj;->a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;

    move-result-object v0

    move-object v1, v0

    .line 759
    :goto_1
    sget-object v0, Lcom/google/maps/g/jh;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jh;

    iput-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    .line 760
    if-eqz v1, :cond_1

    .line 761
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/jj;->a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;

    .line 762
    invoke-virtual {v1}, Lcom/google/maps/g/jj;->c()Lcom/google/maps/g/jh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    .line 764
    :cond_1
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/jl;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 802
    :catch_0
    move-exception v0

    .line 803
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 808
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/jl;->au:Lcom/google/n/bn;

    throw v0

    .line 768
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 769
    invoke-static {v0}, Lcom/google/maps/g/jo;->a(I)Lcom/google/maps/g/jo;

    move-result-object v1

    .line 770
    if-nez v1, :cond_2

    .line 771
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 804
    :catch_1
    move-exception v0

    .line 805
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 806
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 773
    :cond_2
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/jl;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/jl;->a:I

    .line 774
    iput v0, p0, Lcom/google/maps/g/jl;->c:I

    goto :goto_0

    .line 779
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 780
    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v1

    .line 781
    if-nez v1, :cond_3

    .line 782
    const/4 v1, 0x3

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 784
    :cond_3
    iget v1, p0, Lcom/google/maps/g/jl;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/jl;->a:I

    .line 785
    iput v0, p0, Lcom/google/maps/g/jl;->d:I

    goto/16 :goto_0

    .line 790
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/jl;->a:I

    .line 791
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/jl;->e:I

    goto/16 :goto_0

    .line 795
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 796
    iget v1, p0, Lcom/google/maps/g/jl;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/jl;->a:I

    .line 797
    iput-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 808
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jl;->au:Lcom/google/n/bn;

    .line 809
    return-void

    :cond_5
    move-object v1, v0

    goto/16 :goto_1

    .line 743
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 722
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1074
    iput-byte v0, p0, Lcom/google/maps/g/jl;->h:B

    .line 1105
    iput v0, p0, Lcom/google/maps/g/jl;->i:I

    .line 723
    return-void
.end method

.method public static d()Lcom/google/maps/g/jl;
    .locals 1

    .prologue
    .line 1544
    sget-object v0, Lcom/google/maps/g/jl;->g:Lcom/google/maps/g/jl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/jn;
    .locals 1

    .prologue
    .line 1200
    new-instance v0, Lcom/google/maps/g/jn;

    invoke-direct {v0}, Lcom/google/maps/g/jn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/jl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 823
    sget-object v0, Lcom/google/maps/g/jl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 1086
    invoke-virtual {p0}, Lcom/google/maps/g/jl;->c()I

    .line 1087
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v0

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1090
    :cond_0
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1091
    iget v0, p0, Lcom/google/maps/g/jl;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1093
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 1094
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1096
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1097
    iget v0, p0, Lcom/google/maps/g/jl;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1099
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1100
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1102
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/jl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1103
    return-void

    .line 1088
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    goto/16 :goto_0

    .line 1091
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 1094
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 1097
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 1100
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1076
    iget-byte v1, p0, Lcom/google/maps/g/jl;->h:B

    .line 1077
    if-ne v1, v0, :cond_0

    .line 1081
    :goto_0
    return v0

    .line 1078
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1080
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/jl;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1107
    iget v0, p0, Lcom/google/maps/g/jl;->i:I

    .line 1108
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1133
    :goto_0
    return v0

    .line 1111
    :cond_0
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 1113
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v0

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1115
    :goto_2
    iget v2, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1116
    iget v2, p0, Lcom/google/maps/g/jl;->c:I

    .line 1117
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1119
    :cond_1
    iget v2, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 1120
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/g/jl;->d:I

    .line 1121
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1123
    :cond_2
    iget v2, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_9

    .line 1124
    iget v2, p0, Lcom/google/maps/g/jl;->e:I

    .line 1125
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int v2, v4, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1127
    :goto_5
    iget v0, p0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1128
    const/4 v3, 0x5

    .line 1129
    iget-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1131
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/jl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1132
    iput v0, p0, Lcom/google/maps/g/jl;->i:I

    goto/16 :goto_0

    .line 1113
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    goto/16 :goto_1

    :cond_6
    move v2, v3

    .line 1117
    goto :goto_3

    :cond_7
    move v2, v3

    .line 1121
    goto :goto_4

    .line 1129
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_9
    move v2, v0

    goto :goto_5

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 716
    invoke-static {}, Lcom/google/maps/g/jl;->newBuilder()Lcom/google/maps/g/jn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/jn;->a(Lcom/google/maps/g/jl;)Lcom/google/maps/g/jn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 716
    invoke-static {}, Lcom/google/maps/g/jl;->newBuilder()Lcom/google/maps/g/jn;

    move-result-object v0

    return-object v0
.end method
