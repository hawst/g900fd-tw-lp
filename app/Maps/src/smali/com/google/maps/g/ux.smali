.class public final Lcom/google/maps/g/ux;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/va;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ux;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/ux;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:Z

.field l:Z

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/google/n/ao;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/google/maps/g/uy;

    invoke-direct {v0}, Lcom/google/maps/g/uy;-><init>()V

    sput-object v0, Lcom/google/maps/g/ux;->PARSER:Lcom/google/n/ax;

    .line 739
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ux;->r:Lcom/google/n/aw;

    .line 2001
    new-instance v0, Lcom/google/maps/g/ux;

    invoke-direct {v0}, Lcom/google/maps/g/ux;-><init>()V

    sput-object v0, Lcom/google/maps/g/ux;->o:Lcom/google/maps/g/ux;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    .line 598
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    .line 613
    iput-byte v2, p0, Lcom/google/maps/g/ux;->p:B

    .line 674
    iput v2, p0, Lcom/google/maps/g/ux;->q:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 27
    iput-boolean v3, p0, Lcom/google/maps/g/ux;->k:Z

    .line 28
    iput-boolean v3, p0, Lcom/google/maps/g/ux;->l:Z

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x800

    const/16 v8, 0x100

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/maps/g/ux;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 43
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 52
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 58
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 59
    iput-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x100

    if-ne v2, v8, :cond_1

    .line 147
    iget-object v2, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 149
    :cond_1
    and-int/lit16 v1, v1, 0x800

    if-ne v1, v9, :cond_2

    .line 150
    iget-object v1, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 152
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ux;->au:Lcom/google/n/bn;

    throw v0

    .line 63
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 65
    iput-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 142
    :catch_1
    move-exception v0

    .line 143
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 144
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 69
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 71
    iput-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    goto :goto_0

    .line 75
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 77
    iput-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    goto :goto_0

    .line 81
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 82
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 83
    iput-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 87
    :sswitch_6
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v8, :cond_3

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 90
    or-int/lit16 v1, v1, 0x100

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 92
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 97
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/ux;->a:I

    .line 98
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/ux;->k:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 102
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/ux;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/ux;->l:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 107
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 108
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 109
    iput-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 113
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget v6, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/ux;->a:I

    .line 115
    iput-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 119
    :sswitch_b
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v9, :cond_6

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 122
    or-int/lit16 v1, v1, 0x800

    .line 124
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 124
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 129
    :sswitch_c
    iget-object v0, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 130
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/ux;->a:I

    goto/16 :goto_0

    .line 134
    :sswitch_d
    iget-object v0, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 135
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ux;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 146
    :cond_7
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v8, :cond_8

    .line 147
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 149
    :cond_8
    and-int/lit16 v0, v1, 0x800

    if-ne v0, v9, :cond_9

    .line 150
    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 152
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->au:Lcom/google/n/bn;

    .line 153
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    .line 598
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    .line 613
    iput-byte v1, p0, Lcom/google/maps/g/ux;->p:B

    .line 674
    iput v1, p0, Lcom/google/maps/g/ux;->q:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;
    .locals 1

    .prologue
    .line 804
    invoke-static {}, Lcom/google/maps/g/ux;->newBuilder()Lcom/google/maps/g/uz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/uz;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/ux;
    .locals 1

    .prologue
    .line 2004
    sget-object v0, Lcom/google/maps/g/ux;->o:Lcom/google/maps/g/ux;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/uz;
    .locals 1

    .prologue
    .line 801
    new-instance v0, Lcom/google/maps/g/uz;

    invoke-direct {v0}, Lcom/google/maps/g/uz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ux;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    sget-object v0, Lcom/google/maps/g/ux;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 631
    invoke-virtual {p0}, Lcom/google/maps/g/ux;->c()I

    .line 632
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 633
    iget-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 635
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 636
    iget-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 638
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    .line 639
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 641
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_3

    .line 642
    iget-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 644
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_4

    .line 645
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_4
    move v1, v2

    .line 647
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 648
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 647
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 633
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 636
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 639
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 642
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 645
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 650
    :cond_a
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 651
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/maps/g/ux;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_f

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 653
    :cond_b
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 654
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/g/ux;->l:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_10

    move v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 656
    :cond_c
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_d

    .line 657
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 659
    :cond_d
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_e

    .line 660
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 662
    :cond_e
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_13

    .line 663
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 662
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_f
    move v0, v2

    .line 651
    goto/16 :goto_6

    :cond_10
    move v0, v2

    .line 654
    goto :goto_7

    .line 657
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 660
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 665
    :cond_13
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_14

    .line 666
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 668
    :cond_14
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_15

    .line 669
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 671
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/ux;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 672
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 615
    iget-byte v0, p0, Lcom/google/maps/g/ux;->p:B

    .line 616
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 626
    :cond_0
    :goto_0
    return v2

    .line 617
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 619
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 620
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/e;->d()Lcom/google/maps/g/e;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/e;

    invoke-virtual {v0}, Lcom/google/maps/g/e;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 621
    iput-byte v2, p0, Lcom/google/maps/g/ux;->p:B

    goto :goto_0

    .line 619
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 625
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/g/ux;->p:B

    move v2, v3

    .line 626
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 676
    iget v0, p0, Lcom/google/maps/g/ux;->q:I

    .line 677
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 734
    :goto_0
    return v0

    .line 680
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_14

    .line 682
    iget-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 684
    :goto_2
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 686
    iget-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 688
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    .line 689
    const/4 v3, 0x3

    .line 690
    iget-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 692
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_3

    .line 694
    iget-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 696
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    .line 697
    const/4 v3, 0x5

    .line 698
    iget-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_4
    move v3, v1

    move v1, v2

    .line 700
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 701
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/ux;->j:Ljava/util/List;

    .line 702
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 682
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 686
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 690
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 694
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 698
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 704
    :cond_a
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 705
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/maps/g/ux;->k:Z

    .line 706
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 708
    :cond_b
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 709
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/g/ux;->l:Z

    .line 710
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 712
    :cond_c
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_d

    .line 713
    const/16 v1, 0xa

    .line 714
    iget-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 716
    :cond_d
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_e

    .line 717
    const/16 v1, 0xb

    .line 718
    iget-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ux;->d:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_e
    move v1, v2

    .line 720
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 721
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/maps/g/ux;->m:Ljava/util/List;

    .line 722
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 720
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 714
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 718
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 724
    :cond_11
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_12

    .line 725
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/ux;->n:Lcom/google/n/ao;

    .line 726
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 728
    :cond_12
    iget v0, p0, Lcom/google/maps/g/ux;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_13

    .line 729
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/ux;->b:Lcom/google/n/ao;

    .line 730
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 732
    :cond_13
    iget-object v0, p0, Lcom/google/maps/g/ux;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 733
    iput v0, p0, Lcom/google/maps/g/ux;->q:I

    goto/16 :goto_0

    :cond_14
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ux;->newBuilder()Lcom/google/maps/g/uz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/uz;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ux;->newBuilder()Lcom/google/maps/g/uz;

    move-result-object v0

    return-object v0
.end method
