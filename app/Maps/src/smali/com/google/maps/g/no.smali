.class public final Lcom/google/maps/g/no;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/no;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/no;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/google/maps/g/np;

    invoke-direct {v0}, Lcom/google/maps/g/np;-><init>()V

    sput-object v0, Lcom/google/maps/g/no;->PARSER:Lcom/google/n/ax;

    .line 1525
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/no;->h:Lcom/google/n/aw;

    .line 2019
    new-instance v0, Lcom/google/maps/g/no;

    invoke-direct {v0}, Lcom/google/maps/g/no;-><init>()V

    sput-object v0, Lcom/google/maps/g/no;->e:Lcom/google/maps/g/no;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1460
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    .line 1475
    iput-byte v2, p0, Lcom/google/maps/g/no;->f:B

    .line 1500
    iput v2, p0, Lcom/google/maps/g/no;->g:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/no;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 35
    sparse-switch v1, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 42
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_7

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 50
    or-int/lit8 v1, v0, 0x2

    .line 52
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 52
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 54
    goto :goto_0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 58
    iget v1, p0, Lcom/google/maps/g/no;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/no;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 74
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 79
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 80
    iget-object v2, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    .line 82
    :cond_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_2

    .line 83
    iget-object v1, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 85
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/no;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :sswitch_3
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_3

    .line 63
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 65
    or-int/lit8 v0, v0, 0x1

    .line 67
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 67
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 75
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 76
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 77
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 79
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_5

    .line 80
    iget-object v1, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    .line 82
    :cond_5
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 83
    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 85
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/no;->au:Lcom/google/n/bn;

    .line 86
    return-void

    .line 79
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 75
    :catch_2
    move-exception v0

    goto :goto_4

    .line 73
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1460
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    .line 1475
    iput-byte v1, p0, Lcom/google/maps/g/no;->f:B

    .line 1500
    iput v1, p0, Lcom/google/maps/g/no;->g:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/maps/g/no;
    .locals 1

    .prologue
    .line 2022
    sget-object v0, Lcom/google/maps/g/no;->e:Lcom/google/maps/g/no;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/nq;
    .locals 1

    .prologue
    .line 1587
    new-instance v0, Lcom/google/maps/g/nq;

    invoke-direct {v0}, Lcom/google/maps/g/nq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/no;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lcom/google/maps/g/no;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 1487
    invoke-virtual {p0}, Lcom/google/maps/g/no;->c()I

    move v1, v2

    .line 1488
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1489
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1488
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1491
    :cond_0
    iget v0, p0, Lcom/google/maps/g/no;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_1

    .line 1492
    iget-object v0, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1494
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1495
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1494
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1497
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/no;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1498
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1477
    iget-byte v1, p0, Lcom/google/maps/g/no;->f:B

    .line 1478
    if-ne v1, v0, :cond_0

    .line 1482
    :goto_0
    return v0

    .line 1479
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1481
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/no;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1502
    iget v0, p0, Lcom/google/maps/g/no;->g:I

    .line 1503
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1520
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1506
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1507
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    .line 1508
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1510
    :cond_1
    iget v0, p0, Lcom/google/maps/g/no;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 1511
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/maps/g/no;->d:Lcom/google/n/ao;

    .line 1512
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_2
    move v1, v2

    .line 1514
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1515
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 1516
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1514
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1518
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/no;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1519
    iput v0, p0, Lcom/google/maps/g/no;->g:I

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/nv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1380
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    .line 1381
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1382
    iget-object v0, p0, Lcom/google/maps/g/no;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1383
    invoke-static {}, Lcom/google/maps/g/nv;->d()Lcom/google/maps/g/nv;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/nv;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1385
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/no;->newBuilder()Lcom/google/maps/g/nq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/nq;->a(Lcom/google/maps/g/no;)Lcom/google/maps/g/nq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/no;->newBuilder()Lcom/google/maps/g/nq;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/nr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1423
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    .line 1424
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1425
    iget-object v0, p0, Lcom/google/maps/g/no;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1426
    invoke-static {}, Lcom/google/maps/g/nr;->d()Lcom/google/maps/g/nr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/nr;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1428
    :cond_0
    return-object v1
.end method
