.class public final Lcom/google/maps/g/ia;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ib;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/hy;",
        "Lcom/google/maps/g/ia;",
        ">;",
        "Lcom/google/maps/g/ib;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/ao;

.field public f:Z

.field public g:Lcom/google/n/ao;

.field private h:I

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/Object;

.field private l:Ljava/lang/Object;

.field private m:I

.field private n:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 782
    sget-object v0, Lcom/google/maps/g/hy;->o:Lcom/google/maps/g/hy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 968
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->b:Ljava/lang/Object;

    .line 1044
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 1120
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->d:Ljava/lang/Object;

    .line 1196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ia;->e:Lcom/google/n/ao;

    .line 1255
    iput v1, p0, Lcom/google/maps/g/ia;->h:I

    .line 1291
    iput v1, p0, Lcom/google/maps/g/ia;->i:I

    .line 1328
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    .line 1496
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->k:Ljava/lang/Object;

    .line 1572
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->l:Ljava/lang/Object;

    .line 1648
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    .line 1707
    iput v1, p0, Lcom/google/maps/g/ia;->m:I

    .line 1743
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ia;->n:Ljava/lang/Object;

    .line 783
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/cj;)Lcom/google/maps/g/ia;
    .locals 1

    .prologue
    .line 1273
    if-nez p1, :cond_0

    .line 1274
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1276
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 1277
    iget v0, p1, Lcom/google/b/f/cj;->I:I

    iput v0, p0, Lcom/google/maps/g/ia;->h:I

    .line 1279
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 885
    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 947
    :goto_0
    return-object p0

    .line 886
    :cond_0
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 887
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 888
    iget-object v0, p1, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->b:Ljava/lang/Object;

    .line 891
    :cond_1
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 892
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 893
    iget-object v0, p1, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 896
    :cond_2
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 897
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 898
    iget-object v0, p1, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->d:Ljava/lang/Object;

    .line 901
    :cond_3
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 902
    iget-object v0, p0, Lcom/google/maps/g/ia;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 903
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 905
    :cond_4
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_6

    .line 906
    iget v0, p1, Lcom/google/maps/g/hy;->f:I

    invoke-static {v0}, Lcom/google/b/f/cj;->a(I)Lcom/google/b/f/cj;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    :cond_5
    invoke-virtual {p0, v0}, Lcom/google/maps/g/ia;->a(Lcom/google/b/f/cj;)Lcom/google/maps/g/ia;

    .line 908
    :cond_6
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_d

    move v0, v1

    :goto_6
    if-eqz v0, :cond_f

    .line 909
    iget v0, p1, Lcom/google/maps/g/hy;->g:I

    invoke-static {v0}, Lcom/google/b/f/ch;->a(I)Lcom/google/b/f/ch;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    :cond_7
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v2

    .line 886
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 891
    goto :goto_2

    :cond_a
    move v0, v2

    .line 896
    goto :goto_3

    :cond_b
    move v0, v2

    .line 901
    goto :goto_4

    :cond_c
    move v0, v2

    .line 905
    goto :goto_5

    :cond_d
    move v0, v2

    .line 908
    goto :goto_6

    .line 909
    :cond_e
    iget v3, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/ia;->a:I

    iget v0, v0, Lcom/google/b/f/ch;->f:I

    iput v0, p0, Lcom/google/maps/g/ia;->i:I

    .line 911
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 912
    iget-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 913
    iget-object v0, p1, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    .line 914
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 921
    :cond_10
    :goto_7
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v4, :cond_18

    move v0, v1

    :goto_8
    if-eqz v0, :cond_11

    .line 922
    iget-boolean v0, p1, Lcom/google/maps/g/hy;->i:Z

    iget v3, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/ia;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/ia;->f:Z

    .line 924
    :cond_11
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_9
    if-eqz v0, :cond_12

    .line 925
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 926
    iget-object v0, p1, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->k:Ljava/lang/Object;

    .line 929
    :cond_12
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_a
    if-eqz v0, :cond_13

    .line 930
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 931
    iget-object v0, p1, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->l:Ljava/lang/Object;

    .line 934
    :cond_13
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_b
    if-eqz v0, :cond_14

    .line 935
    iget-object v0, p0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 936
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 938
    :cond_14
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_c
    if-eqz v0, :cond_1e

    .line 939
    iget v0, p1, Lcom/google/maps/g/hy;->m:I

    invoke-static {v0}, Lcom/google/maps/g/ge;->a(I)Lcom/google/maps/g/ge;

    move-result-object v0

    if-nez v0, :cond_15

    sget-object v0, Lcom/google/maps/g/ge;->a:Lcom/google/maps/g/ge;

    :cond_15
    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 916
    :cond_16
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit8 v0, v0, 0x40

    if-eq v0, v4, :cond_17

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 917
    :cond_17
    iget-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 921
    goto/16 :goto_8

    :cond_19
    move v0, v2

    .line 924
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 929
    goto :goto_a

    :cond_1b
    move v0, v2

    .line 934
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 938
    goto :goto_c

    .line 939
    :cond_1d
    iget v3, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/ia;->a:I

    iget v0, v0, Lcom/google/maps/g/ge;->q:I

    iput v0, p0, Lcom/google/maps/g/ia;->m:I

    .line 941
    :cond_1e
    iget v0, p1, Lcom/google/maps/g/hy;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_20

    move v0, v1

    :goto_d
    if-eqz v0, :cond_1f

    .line 942
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/ia;->a:I

    .line 943
    iget-object v0, p1, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ia;->n:Ljava/lang/Object;

    .line 946
    :cond_1f
    iget-object v0, p1, Lcom/google/maps/g/hy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_20
    move v0, v2

    .line 941
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 774
    new-instance v2, Lcom/google/maps/g/hy;

    invoke-direct {v2, p0}, Lcom/google/maps/g/hy;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/ia;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/ia;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ia;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ia;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/ia;->h:I

    iput v4, v2, Lcom/google/maps/g/hy;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/maps/g/ia;->i:I

    iput v4, v2, Lcom/google/maps/g/hy;->g:I

    iget v4, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/maps/g/ia;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/hy;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-boolean v4, p0, Lcom/google/maps/g/ia;->f:Z

    iput-boolean v4, v2, Lcom/google/maps/g/hy;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/ia;->k:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hy;->j:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/ia;->l:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hy;->k:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v4, v2, Lcom/google/maps/g/hy;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget v1, p0, Lcom/google/maps/g/ia;->m:I

    iput v1, v2, Lcom/google/maps/g/hy;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget-object v1, p0, Lcom/google/maps/g/ia;->n:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hy;->n:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/hy;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 774
    check-cast p1, Lcom/google/maps/g/hy;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ia;->a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 951
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 952
    iget-object v0, p0, Lcom/google/maps/g/ia;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 963
    :cond_0
    :goto_1
    return v2

    .line 951
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 957
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ia;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 958
    iget-object v0, p0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/bf;->d()Lcom/google/b/f/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bf;

    invoke-virtual {v0}, Lcom/google/b/f/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 963
    goto :goto_1

    :cond_4
    move v0, v2

    .line 957
    goto :goto_2
.end method
