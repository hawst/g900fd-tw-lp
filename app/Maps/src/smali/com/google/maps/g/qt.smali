.class public final Lcom/google/maps/g/qt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/qw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qt;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/qt;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3868
    new-instance v0, Lcom/google/maps/g/qu;

    invoke-direct {v0}, Lcom/google/maps/g/qu;-><init>()V

    sput-object v0, Lcom/google/maps/g/qt;->PARSER:Lcom/google/n/ax;

    .line 3984
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/qt;->g:Lcom/google/n/aw;

    .line 4227
    new-instance v0, Lcom/google/maps/g/qt;

    invoke-direct {v0}, Lcom/google/maps/g/qt;-><init>()V

    sput-object v0, Lcom/google/maps/g/qt;->d:Lcom/google/maps/g/qt;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3818
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3941
    iput-byte v0, p0, Lcom/google/maps/g/qt;->e:B

    .line 3963
    iput v0, p0, Lcom/google/maps/g/qt;->f:I

    .line 3819
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/qt;->b:I

    .line 3820
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    .line 3821
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3827
    invoke-direct {p0}, Lcom/google/maps/g/qt;-><init>()V

    .line 3828
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3832
    const/4 v0, 0x0

    .line 3833
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3834
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3835
    sparse-switch v3, :sswitch_data_0

    .line 3840
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3842
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3838
    goto :goto_0

    .line 3847
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/qt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/qt;->a:I

    .line 3848
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/qt;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3859
    :catch_0
    move-exception v0

    .line 3860
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3865
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qt;->au:Lcom/google/n/bn;

    throw v0

    .line 3852
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3853
    iget v4, p0, Lcom/google/maps/g/qt;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/qt;->a:I

    .line 3854
    iput-object v3, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3861
    :catch_1
    move-exception v0

    .line 3862
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3863
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3865
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qt;->au:Lcom/google/n/bn;

    .line 3866
    return-void

    .line 3835
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3816
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3941
    iput-byte v0, p0, Lcom/google/maps/g/qt;->e:B

    .line 3963
    iput v0, p0, Lcom/google/maps/g/qt;->f:I

    .line 3817
    return-void
.end method

.method public static g()Lcom/google/maps/g/qt;
    .locals 1

    .prologue
    .line 4230
    sget-object v0, Lcom/google/maps/g/qt;->d:Lcom/google/maps/g/qt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/qv;
    .locals 1

    .prologue
    .line 4046
    new-instance v0, Lcom/google/maps/g/qv;

    invoke-direct {v0}, Lcom/google/maps/g/qv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3880
    sget-object v0, Lcom/google/maps/g/qt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 3953
    invoke-virtual {p0}, Lcom/google/maps/g/qt;->c()I

    .line 3954
    iget v0, p0, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3955
    iget v0, p0, Lcom/google/maps/g/qt;->b:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 3957
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3958
    iget-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3960
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/qt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3961
    return-void

    .line 3955
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 3958
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3943
    iget-byte v1, p0, Lcom/google/maps/g/qt;->e:B

    .line 3944
    if-ne v1, v0, :cond_0

    .line 3948
    :goto_0
    return v0

    .line 3945
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3947
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/qt;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3965
    iget v0, p0, Lcom/google/maps/g/qt;->f:I

    .line 3966
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3979
    :goto_0
    return v0

    .line 3969
    :cond_0
    iget v0, p0, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 3970
    iget v0, p0, Lcom/google/maps/g/qt;->b:I

    .line 3971
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 3973
    :goto_2
    iget v0, p0, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 3975
    iget-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 3977
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/qt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 3978
    iput v0, p0, Lcom/google/maps/g/qt;->f:I

    goto :goto_0

    .line 3971
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 3975
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3911
    iget-object v0, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    .line 3912
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3913
    check-cast v0, Ljava/lang/String;

    .line 3921
    :goto_0
    return-object v0

    .line 3915
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 3917
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 3918
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3919
    iput-object v1, p0, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3921
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3810
    invoke-static {}, Lcom/google/maps/g/qt;->newBuilder()Lcom/google/maps/g/qv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/qv;->a(Lcom/google/maps/g/qt;)Lcom/google/maps/g/qv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3810
    invoke-static {}, Lcom/google/maps/g/qt;->newBuilder()Lcom/google/maps/g/qv;

    move-result-object v0

    return-object v0
.end method
