.class public final Lcom/google/maps/g/a/dw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/du;",
        "Lcom/google/maps/g/a/dw;",
        ">;",
        "Lcom/google/maps/g/a/dz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/google/n/ao;

.field private i:Z

.field private j:Z

.field private k:Lcom/google/n/ao;

.field private l:I

.field private m:Z

.field private n:Lcom/google/n/ao;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Lcom/google/n/ao;

.field private u:Lcom/google/n/ao;

.field private v:Lcom/google/n/ao;

.field private w:Lcom/google/n/ao;

.field private x:Z

.field private y:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 974
    sget-object v0, Lcom/google/maps/g/a/du;->z:Lcom/google/maps/g/a/du;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1256
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->b:Lcom/google/n/ao;

    .line 1315
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->c:Lcom/google/n/ao;

    .line 1406
    iput-boolean v1, p0, Lcom/google/maps/g/a/dw;->e:Z

    .line 1438
    iput-boolean v1, p0, Lcom/google/maps/g/a/dw;->f:Z

    .line 1502
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->h:Lcom/google/n/ao;

    .line 1625
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->k:Lcom/google/n/ao;

    .line 1684
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/dw;->l:I

    .line 1752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->n:Lcom/google/n/ao;

    .line 1811
    iput-boolean v1, p0, Lcom/google/maps/g/a/dw;->o:Z

    .line 1843
    iput-boolean v1, p0, Lcom/google/maps/g/a/dw;->p:Z

    .line 1907
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    .line 2005
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->t:Lcom/google/n/ao;

    .line 2064
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->u:Lcom/google/n/ao;

    .line 2123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->v:Lcom/google/n/ao;

    .line 2182
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dw;->w:Lcom/google/n/ao;

    .line 975
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/du;)Lcom/google/maps/g/a/dw;
    .locals 7

    .prologue
    const/high16 v6, 0x20000

    const v5, 0x8000

    const/high16 v4, 0x10000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1157
    invoke-static {}, Lcom/google/maps/g/a/du;->d()Lcom/google/maps/g/a/du;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1247
    :goto_0
    return-object p0

    .line 1158
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1159
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1160
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1162
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1163
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1164
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1166
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1167
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->d:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->d:Z

    .line 1169
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1170
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->e:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->e:Z

    .line 1172
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1173
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->f:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->f:Z

    .line 1175
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1176
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->g:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->g:Z

    .line 1178
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1179
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1180
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1182
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1183
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->i:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->i:Z

    .line 1185
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1186
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->j:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->j:Z

    .line 1188
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1189
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1190
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1192
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_b
    if-eqz v2, :cond_18

    .line 1193
    iget v2, p1, Lcom/google/maps/g/a/du;->l:I

    invoke-static {v2}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v2

    if-nez v2, :cond_b

    sget-object v2, Lcom/google/maps/g/a/dx;->b:Lcom/google/maps/g/a/dx;

    :cond_b
    if-nez v2, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    move v2, v1

    .line 1158
    goto/16 :goto_1

    :cond_d
    move v2, v1

    .line 1162
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 1166
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 1169
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 1172
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 1175
    goto/16 :goto_6

    :cond_12
    move v2, v1

    .line 1178
    goto/16 :goto_7

    :cond_13
    move v2, v1

    .line 1182
    goto :goto_8

    :cond_14
    move v2, v1

    .line 1185
    goto :goto_9

    :cond_15
    move v2, v1

    .line 1188
    goto :goto_a

    :cond_16
    move v2, v1

    .line 1192
    goto :goto_b

    .line 1193
    :cond_17
    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iget v2, v2, Lcom/google/maps/g/a/dx;->e:I

    iput v2, p0, Lcom/google/maps/g/a/dw;->l:I

    .line 1195
    :cond_18
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_c
    if-eqz v2, :cond_19

    .line 1196
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->m:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->m:Z

    .line 1198
    :cond_19
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_27

    move v2, v0

    :goto_d
    if-eqz v2, :cond_1a

    .line 1199
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1200
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1202
    :cond_1a
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_e
    if-eqz v2, :cond_1b

    .line 1203
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->o:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->o:Z

    .line 1205
    :cond_1b
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_f
    if-eqz v2, :cond_1c

    .line 1206
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->p:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->p:Z

    .line 1208
    :cond_1c
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2a

    move v2, v0

    :goto_10
    if-eqz v2, :cond_1d

    .line 1209
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->q:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->q:Z

    .line 1211
    :cond_1d
    iget-object v2, p1, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1e

    .line 1212
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1213
    iget-object v2, p1, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    .line 1214
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    const v3, -0x10001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1221
    :cond_1e
    :goto_11
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_2d

    move v2, v0

    :goto_12
    if-eqz v2, :cond_1f

    .line 1222
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->s:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->s:Z

    .line 1224
    :cond_1f
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2e

    move v2, v0

    :goto_13
    if-eqz v2, :cond_20

    .line 1225
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->t:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1226
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1228
    :cond_20
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_2f

    move v2, v0

    :goto_14
    if-eqz v2, :cond_21

    .line 1229
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->u:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1230
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1232
    :cond_21
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_30

    move v2, v0

    :goto_15
    if-eqz v2, :cond_22

    .line 1233
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->v:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1234
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v3, 0x100000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1236
    :cond_22
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_31

    move v2, v0

    :goto_16
    if-eqz v2, :cond_23

    .line 1237
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->w:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1238
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v3, 0x200000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1240
    :cond_23
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_32

    move v2, v0

    :goto_17
    if-eqz v2, :cond_24

    .line 1241
    iget-boolean v2, p1, Lcom/google/maps/g/a/du;->x:Z

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v4, 0x400000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/dw;->x:Z

    .line 1243
    :cond_24
    iget v2, p1, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_33

    :goto_18
    if-eqz v0, :cond_25

    .line 1244
    iget-boolean v0, p1, Lcom/google/maps/g/a/du;->y:Z

    iget v1, p0, Lcom/google/maps/g/a/dw;->a:I

    const/high16 v2, 0x800000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/a/dw;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/dw;->y:Z

    .line 1246
    :cond_25
    iget-object v0, p1, Lcom/google/maps/g/a/du;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_26
    move v2, v1

    .line 1195
    goto/16 :goto_c

    :cond_27
    move v2, v1

    .line 1198
    goto/16 :goto_d

    :cond_28
    move v2, v1

    .line 1202
    goto/16 :goto_e

    :cond_29
    move v2, v1

    .line 1205
    goto/16 :goto_f

    :cond_2a
    move v2, v1

    .line 1208
    goto/16 :goto_10

    .line 1216
    :cond_2b
    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    and-int/2addr v2, v4

    if-eq v2, v4, :cond_2c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/dw;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/a/dw;->a:I

    .line 1217
    :cond_2c
    iget-object v2, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_11

    :cond_2d
    move v2, v1

    .line 1221
    goto/16 :goto_12

    :cond_2e
    move v2, v1

    .line 1224
    goto/16 :goto_13

    :cond_2f
    move v2, v1

    .line 1228
    goto/16 :goto_14

    :cond_30
    move v2, v1

    .line 1232
    goto/16 :goto_15

    :cond_31
    move v2, v1

    .line 1236
    goto/16 :goto_16

    :cond_32
    move v2, v1

    .line 1240
    goto :goto_17

    :cond_33
    move v0, v1

    .line 1243
    goto :goto_18
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 11

    .prologue
    const/high16 v10, 0x40000

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 966
    new-instance v2, Lcom/google/maps/g/a/du;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/du;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/dw;->a:I

    and-int/lit8 v0, v3, 0x1

    const/4 v4, 0x1

    if-ne v0, v4, :cond_17

    const/4 v0, 0x1

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->d:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->f:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->g:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->i:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->j:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v4, p0, Lcom/google/maps/g/a/dw;->l:I

    iput v4, v2, Lcom/google/maps/g/a/du;->l:I

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->m:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->m:Z

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-object v4, v2, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->o:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->o:Z

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->p:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->p:Z

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/2addr v0, v7

    :cond_e
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->q:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->q:Z

    iget v4, p0, Lcom/google/maps/g/a/dw;->a:I

    and-int/2addr v4, v8

    if-ne v4, v8, :cond_f

    iget-object v4, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/dw;->a:I

    const v5, -0x10001

    and-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/g/a/dw;->a:I

    :cond_f
    iget-object v4, p0, Lcom/google/maps/g/a/dw;->r:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    or-int/2addr v0, v8

    :cond_10
    iget-boolean v4, p0, Lcom/google/maps/g/a/dw;->s:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/du;->s:Z

    and-int v4, v3, v10

    if-ne v4, v10, :cond_11

    or-int/2addr v0, v9

    :cond_11
    iget-object v4, v2, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->t:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->t:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x80000

    and-int/2addr v4, v3

    const/high16 v5, 0x80000

    if-ne v4, v5, :cond_12

    or-int/2addr v0, v10

    :cond_12
    iget-object v4, v2, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->u:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->u:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x100000

    and-int/2addr v4, v3

    const/high16 v5, 0x100000

    if-ne v4, v5, :cond_13

    const/high16 v4, 0x80000

    or-int/2addr v0, v4

    :cond_13
    iget-object v4, v2, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->v:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->v:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x200000

    and-int/2addr v4, v3

    const/high16 v5, 0x200000

    if-ne v4, v5, :cond_14

    const/high16 v4, 0x100000

    or-int/2addr v0, v4

    :cond_14
    iget-object v4, v2, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/dw;->w:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/dw;->w:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v1, 0x400000

    and-int/2addr v1, v3

    const/high16 v4, 0x400000

    if-ne v1, v4, :cond_15

    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    :cond_15
    iget-boolean v1, p0, Lcom/google/maps/g/a/dw;->x:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/du;->x:Z

    const/high16 v1, 0x800000

    and-int/2addr v1, v3

    const/high16 v3, 0x800000

    if-ne v1, v3, :cond_16

    const/high16 v1, 0x400000

    or-int/2addr v0, v1

    :cond_16
    iget-boolean v1, p0, Lcom/google/maps/g/a/dw;->y:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/du;->y:Z

    iput v0, v2, Lcom/google/maps/g/a/du;->a:I

    return-object v2

    :cond_17
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 966
    check-cast p1, Lcom/google/maps/g/a/du;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/dw;->a(Lcom/google/maps/g/a/du;)Lcom/google/maps/g/a/dw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1251
    const/4 v0, 0x1

    return v0
.end method
