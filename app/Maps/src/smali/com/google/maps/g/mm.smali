.class public final Lcom/google/maps/g/mm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/mn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/mk;",
        "Lcom/google/maps/g/mm;",
        ">;",
        "Lcom/google/maps/g/mn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1506
    sget-object v0, Lcom/google/maps/g/mk;->j:Lcom/google/maps/g/mk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1627
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mm;->b:Lcom/google/n/ao;

    .line 1686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mm;->c:Ljava/lang/Object;

    .line 1762
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mm;->d:Ljava/lang/Object;

    .line 1902
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mm;->g:Lcom/google/n/ao;

    .line 1994
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    .line 1507
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/mk;)Lcom/google/maps/g/mm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1579
    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1618
    :goto_0
    return-object p0

    .line 1580
    :cond_0
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1581
    iget-object v2, p0, Lcom/google/maps/g/mm;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1582
    iget v2, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/mm;->a:I

    .line 1584
    :cond_1
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1585
    iget v2, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/mm;->a:I

    .line 1586
    iget-object v2, p1, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/mm;->c:Ljava/lang/Object;

    .line 1589
    :cond_2
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1590
    iget v2, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/mm;->a:I

    .line 1591
    iget-object v2, p1, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/mm;->d:Ljava/lang/Object;

    .line 1594
    :cond_3
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1595
    iget-boolean v2, p1, Lcom/google/maps/g/mk;->e:Z

    iget v3, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/mm;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/mm;->e:Z

    .line 1597
    :cond_4
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1598
    iget v2, p1, Lcom/google/maps/g/mk;->f:I

    iget v3, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/mm;->a:I

    iput v2, p0, Lcom/google/maps/g/mm;->f:I

    .line 1600
    :cond_5
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1601
    iget-object v2, p0, Lcom/google/maps/g/mm;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1602
    iget v2, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/mm;->a:I

    .line 1604
    :cond_6
    iget v2, p1, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    :goto_7
    if-eqz v0, :cond_7

    .line 1605
    iget-boolean v0, p1, Lcom/google/maps/g/mk;->h:Z

    iget v1, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/maps/g/mm;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/mm;->h:Z

    .line 1607
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1608
    iget-object v0, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1609
    iget-object v0, p1, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    .line 1610
    iget v0, p0, Lcom/google/maps/g/mm;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/maps/g/mm;->a:I

    .line 1617
    :cond_8
    :goto_8
    iget-object v0, p1, Lcom/google/maps/g/mk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 1580
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 1584
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 1589
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 1594
    goto :goto_4

    :cond_d
    move v2, v1

    .line 1597
    goto :goto_5

    :cond_e
    move v2, v1

    .line 1600
    goto :goto_6

    :cond_f
    move v0, v1

    .line 1604
    goto :goto_7

    .line 1612
    :cond_10
    iget v0, p0, Lcom/google/maps/g/mm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/mm;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/mm;->a:I

    .line 1613
    :cond_11
    iget-object v0, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1498
    new-instance v2, Lcom/google/maps/g/mk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/mk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/mm;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/mm;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/mm;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/mm;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/mm;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/mm;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/mk;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/mm;->f:I

    iput v4, v2, Lcom/google/maps/g/mk;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/mm;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/mm;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/mm;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/mk;->h:Z

    iget v1, p0, Lcom/google/maps/g/mm;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/mm;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/g/mm;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/mm;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/mk;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1498
    check-cast p1, Lcom/google/maps/g/mk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/mm;->a(Lcom/google/maps/g/mk;)Lcom/google/maps/g/mm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1622
    const/4 v0, 0x1

    return v0
.end method
