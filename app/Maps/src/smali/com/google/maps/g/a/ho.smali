.class public final Lcom/google/maps/g/a/ho;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ht;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ho;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/a/ho;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/maps/g/a/hp;

    invoke-direct {v0}, Lcom/google/maps/g/a/hp;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ho;->PARSER:Lcom/google/n/ax;

    .line 264
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ho;->h:Lcom/google/n/aw;

    .line 510
    new-instance v0, Lcom/google/maps/g/a/ho;

    invoke-direct {v0}, Lcom/google/maps/g/a/ho;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ho;->e:Lcom/google/maps/g/a/ho;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 214
    iput-byte v0, p0, Lcom/google/maps/g/a/ho;->f:B

    .line 239
    iput v0, p0, Lcom/google/maps/g/a/ho;->g:I

    .line 18
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/maps/g/a/ho;->b:I

    .line 19
    iput v1, p0, Lcom/google/maps/g/a/ho;->c:I

    .line 20
    iput-boolean v1, p0, Lcom/google/maps/g/a/ho;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/a/ho;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 33
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 35
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 48
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v5

    .line 49
    if-nez v5, :cond_1

    .line 50
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ho;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/a/ho;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/ho;->a:I

    .line 53
    iput v0, p0, Lcom/google/maps/g/a/ho;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 59
    invoke-static {v0}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v5

    .line 60
    if-nez v5, :cond_2

    .line 61
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 63
    :cond_2
    iget v5, p0, Lcom/google/maps/g/a/ho;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/ho;->a:I

    .line 64
    iput v0, p0, Lcom/google/maps/g/a/ho;->c:I

    goto :goto_0

    .line 69
    :sswitch_3
    iget v0, p0, Lcom/google/maps/g/a/ho;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/ho;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/ho;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 81
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ho;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 214
    iput-byte v0, p0, Lcom/google/maps/g/a/ho;->f:B

    .line 239
    iput v0, p0, Lcom/google/maps/g/a/ho;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/ho;
    .locals 1

    .prologue
    .line 513
    sget-object v0, Lcom/google/maps/g/a/ho;->e:Lcom/google/maps/g/a/ho;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/hq;
    .locals 1

    .prologue
    .line 326
    new-instance v0, Lcom/google/maps/g/a/hq;

    invoke-direct {v0}, Lcom/google/maps/g/a/hq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ho;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/g/a/ho;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 226
    invoke-virtual {p0}, Lcom/google/maps/g/a/ho;->c()I

    .line 227
    iget v2, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 228
    iget v2, p0, Lcom/google/maps/g/a/ho;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 230
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 231
    iget v2, p0, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 233
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 234
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/ho;->d:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 236
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/ho;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 237
    return-void

    .line 228
    :cond_3
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 231
    :cond_4
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 234
    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 216
    iget-byte v1, p0, Lcom/google/maps/g/a/ho;->f:B

    .line 217
    if-ne v1, v0, :cond_0

    .line 221
    :goto_0
    return v0

    .line 218
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 220
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/ho;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 241
    iget v0, p0, Lcom/google/maps/g/a/ho;->g:I

    .line 242
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 259
    :goto_0
    return v0

    .line 245
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 246
    iget v0, p0, Lcom/google/maps/g/a/ho;->b:I

    .line 247
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 249
    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 250
    iget v3, p0, Lcom/google/maps/g/a/ho;->c:I

    .line 251
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 253
    :cond_2
    iget v1, p0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 254
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/ho;->d:Z

    .line 255
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 257
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/ho;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    iput v0, p0, Lcom/google/maps/g/a/ho;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 247
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/hq;->a(Lcom/google/maps/g/a/ho;)Lcom/google/maps/g/a/hq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v0

    return-object v0
.end method
