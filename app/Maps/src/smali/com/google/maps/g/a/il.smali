.class public final Lcom/google/maps/g/a/il;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/im;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ij;",
        "Lcom/google/maps/g/a/il;",
        ">;",
        "Lcom/google/maps/g/a/im;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/a/be;

.field private c:Lcom/google/maps/g/a/cw;

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1730
    sget-object v0, Lcom/google/maps/g/a/ij;->g:Lcom/google/maps/g/a/ij;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1810
    iput-object v1, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    .line 1871
    iput-object v1, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    .line 1932
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/il;->d:Ljava/lang/Object;

    .line 2040
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/il;->f:Ljava/lang/Object;

    .line 1731
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1780
    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1801
    :goto_0
    return-object p0

    .line 1781
    :cond_0
    iget v0, p1, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1782
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/il;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    invoke-static {v3}, Lcom/google/maps/g/a/be;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/il;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/il;->a:I

    .line 1784
    :cond_1
    iget v0, p1, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 1785
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/maps/g/a/il;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_b

    iget-object v3, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    invoke-static {v3}, Lcom/google/maps/g/a/cw;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/cy;->c()Lcom/google/maps/g/a/cw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    :goto_6
    iget v0, p0, Lcom/google/maps/g/a/il;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/il;->a:I

    .line 1787
    :cond_2
    iget v0, p1, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 1788
    iget v0, p0, Lcom/google/maps/g/a/il;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/il;->a:I

    .line 1789
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/il;->d:Ljava/lang/Object;

    .line 1792
    :cond_3
    iget v0, p1, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_d

    move v0, v1

    :goto_8
    if-eqz v0, :cond_4

    .line 1793
    iget-boolean v0, p1, Lcom/google/maps/g/a/ij;->e:Z

    iget v3, p0, Lcom/google/maps/g/a/il;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/il;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/il;->e:Z

    .line 1795
    :cond_4
    iget v0, p1, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_9
    if-eqz v0, :cond_5

    .line 1796
    iget v0, p0, Lcom/google/maps/g/a/il;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/il;->a:I

    .line 1797
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/il;->f:Ljava/lang/Object;

    .line 1800
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1781
    goto/16 :goto_1

    .line 1782
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    goto/16 :goto_2

    :cond_8
    iput-object v0, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    goto/16 :goto_3

    :cond_9
    move v0, v2

    .line 1784
    goto/16 :goto_4

    .line 1785
    :cond_a
    iget-object v0, p1, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    goto/16 :goto_5

    :cond_b
    iput-object v0, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    goto :goto_6

    :cond_c
    move v0, v2

    .line 1787
    goto :goto_7

    :cond_d
    move v0, v2

    .line 1792
    goto :goto_8

    :cond_e
    move v0, v2

    .line 1795
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1722
    invoke-virtual {p0}, Lcom/google/maps/g/a/il;->c()Lcom/google/maps/g/a/ij;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1722
    check-cast p1, Lcom/google/maps/g/a/ij;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/il;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1805
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/ij;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1752
    new-instance v2, Lcom/google/maps/g/a/ij;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ij;-><init>(Lcom/google/n/v;)V

    .line 1753
    iget v3, p0, Lcom/google/maps/g/a/il;->a:I

    .line 1754
    const/4 v1, 0x0

    .line 1755
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 1758
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/il;->b:Lcom/google/maps/g/a/be;

    iput-object v1, v2, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    .line 1759
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1760
    or-int/lit8 v0, v0, 0x2

    .line 1762
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/il;->c:Lcom/google/maps/g/a/cw;

    iput-object v1, v2, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    .line 1763
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1764
    or-int/lit8 v0, v0, 0x4

    .line 1766
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/il;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    .line 1767
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1768
    or-int/lit8 v0, v0, 0x8

    .line 1770
    :cond_2
    iget-boolean v1, p0, Lcom/google/maps/g/a/il;->e:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/ij;->e:Z

    .line 1771
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 1772
    or-int/lit8 v0, v0, 0x10

    .line 1774
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/il;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    .line 1775
    iput v0, v2, Lcom/google/maps/g/a/ij;->a:I

    .line 1776
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
