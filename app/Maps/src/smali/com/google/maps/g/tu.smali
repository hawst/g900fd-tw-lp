.class public final Lcom/google/maps/g/tu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ub;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tu;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/tu;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/maps/g/tx;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/google/maps/g/tv;

    invoke-direct {v0}, Lcom/google/maps/g/tv;-><init>()V

    sput-object v0, Lcom/google/maps/g/tu;->PARSER:Lcom/google/n/ax;

    .line 1051
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/tu;->g:Lcom/google/n/aw;

    .line 1323
    new-instance v0, Lcom/google/maps/g/tu;

    invoke-direct {v0}, Lcom/google/maps/g/tu;-><init>()V

    sput-object v0, Lcom/google/maps/g/tu;->d:Lcom/google/maps/g/tu;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 183
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1008
    iput-byte v0, p0, Lcom/google/maps/g/tu;->e:B

    .line 1030
    iput v0, p0, Lcom/google/maps/g/tu;->f:I

    .line 184
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    .line 185
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 191
    invoke-direct {p0}, Lcom/google/maps/g/tu;-><init>()V

    .line 192
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 196
    const/4 v0, 0x0

    move v2, v0

    .line 197
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 198
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 199
    sparse-switch v0, :sswitch_data_0

    .line 204
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 206
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 202
    goto :goto_0

    .line 211
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 212
    iget v1, p0, Lcom/google/maps/g/tu;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/tu;->a:I

    .line 213
    iput-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/tu;->au:Lcom/google/n/bn;

    throw v0

    .line 217
    :sswitch_2
    const/4 v0, 0x0

    .line 218
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    .line 219
    iget-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    invoke-static {v0}, Lcom/google/maps/g/tx;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v0

    move-object v1, v0

    .line 221
    :goto_1
    sget-object v0, Lcom/google/maps/g/tx;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tx;

    iput-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    .line 222
    if-eqz v1, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/tz;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    .line 224
    invoke-virtual {v1}, Lcom/google/maps/g/tz;->c()Lcom/google/maps/g/tx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    .line 226
    :cond_1
    iget v0, p0, Lcom/google/maps/g/tu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/tu;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 233
    :catch_1
    move-exception v0

    .line 234
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 235
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 237
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tu;->au:Lcom/google/n/bn;

    .line 238
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 199
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 181
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1008
    iput-byte v0, p0, Lcom/google/maps/g/tu;->e:B

    .line 1030
    iput v0, p0, Lcom/google/maps/g/tu;->f:I

    .line 182
    return-void
.end method

.method public static d()Lcom/google/maps/g/tu;
    .locals 1

    .prologue
    .line 1326
    sget-object v0, Lcom/google/maps/g/tu;->d:Lcom/google/maps/g/tu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/tw;
    .locals 1

    .prologue
    .line 1113
    new-instance v0, Lcom/google/maps/g/tw;

    invoke-direct {v0}, Lcom/google/maps/g/tw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    sget-object v0, Lcom/google/maps/g/tu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 1020
    invoke-virtual {p0}, Lcom/google/maps/g/tu;->c()I

    .line 1021
    iget v0, p0, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1024
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1025
    iget-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v0

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1027
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/tu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1028
    return-void

    .line 1022
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1025
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1010
    iget-byte v1, p0, Lcom/google/maps/g/tu;->e:B

    .line 1011
    if-ne v1, v0, :cond_0

    .line 1015
    :goto_0
    return v0

    .line 1012
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1014
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/tu;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1032
    iget v0, p0, Lcom/google/maps/g/tu;->f:I

    .line 1033
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1046
    :goto_0
    return v0

    .line 1036
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1038
    iget-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1040
    :goto_2
    iget v2, p0, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1042
    iget-object v2, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/tu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    iput v0, p0, Lcom/google/maps/g/tu;->f:I

    goto :goto_0

    .line 1038
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1042
    :cond_3
    iget-object v2, p0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/maps/g/tu;->newBuilder()Lcom/google/maps/g/tw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/tw;->a(Lcom/google/maps/g/tu;)Lcom/google/maps/g/tw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/maps/g/tu;->newBuilder()Lcom/google/maps/g/tw;

    move-result-object v0

    return-object v0
.end method
