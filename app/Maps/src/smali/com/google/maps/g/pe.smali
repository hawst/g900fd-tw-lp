.class public final Lcom/google/maps/g/pe;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/pc;",
        "Lcom/google/maps/g/pe;",
        ">;",
        "Lcom/google/maps/g/pn;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field public d:I

.field public e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/n/ao;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/Object;

.field private l:Ljava/lang/Object;

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 752
    sget-object v0, Lcom/google/maps/g/pc;->o:Lcom/google/maps/g/pc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 935
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->g:Ljava/lang/Object;

    .line 1011
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->h:Ljava/lang/Object;

    .line 1087
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    .line 1146
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pe;->i:Lcom/google/n/ao;

    .line 1205
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->c:Ljava/lang/Object;

    .line 1282
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    .line 1450
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->e:Ljava/lang/Object;

    .line 1526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->k:Ljava/lang/Object;

    .line 1602
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pe;->l:Ljava/lang/Object;

    .line 1678
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pe;->m:Lcom/google/n/ao;

    .line 1737
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pe;->n:Lcom/google/n/ao;

    .line 1796
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pe;->f:Lcom/google/n/ao;

    .line 753
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/pc;)Lcom/google/maps/g/pe;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 861
    invoke-static {}, Lcom/google/maps/g/pc;->i()Lcom/google/maps/g/pc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 926
    :goto_0
    return-object p0

    .line 862
    :cond_0
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 863
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 864
    iget-object v2, p1, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->g:Ljava/lang/Object;

    .line 867
    :cond_1
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 868
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 869
    iget-object v2, p1, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->h:Ljava/lang/Object;

    .line 872
    :cond_2
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 873
    iget-object v2, p0, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 874
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 876
    :cond_3
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 877
    iget-object v2, p0, Lcom/google/maps/g/pe;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 878
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 880
    :cond_4
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 881
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 882
    iget-object v2, p1, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->c:Ljava/lang/Object;

    .line 885
    :cond_5
    iget-object v2, p1, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 886
    iget-object v2, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 887
    iget-object v2, p1, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    .line 888
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 895
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 896
    iget v2, p1, Lcom/google/maps/g/pc;->h:I

    iget v3, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/pe;->a:I

    iput v2, p0, Lcom/google/maps/g/pe;->d:I

    .line 898
    :cond_7
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 899
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 900
    iget-object v2, p1, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->e:Ljava/lang/Object;

    .line 903
    :cond_8
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 904
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 905
    iget-object v2, p1, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->k:Ljava/lang/Object;

    .line 908
    :cond_9
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 909
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 910
    iget-object v2, p1, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pe;->l:Ljava/lang/Object;

    .line 913
    :cond_a
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 914
    iget-object v2, p0, Lcom/google/maps/g/pe;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 915
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 917
    :cond_b
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 918
    iget-object v2, p0, Lcom/google/maps/g/pe;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 919
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 921
    :cond_c
    iget v2, p1, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1b

    :goto_d
    if-eqz v0, :cond_d

    .line 922
    iget-object v0, p0, Lcom/google/maps/g/pe;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 923
    iget v0, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/pe;->a:I

    .line 925
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/pc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 862
    goto/16 :goto_1

    :cond_f
    move v2, v1

    .line 867
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 872
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 876
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 880
    goto/16 :goto_5

    .line 890
    :cond_13
    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/pe;->a:I

    .line 891
    :cond_14
    iget-object v2, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 895
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 898
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 903
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 908
    goto/16 :goto_a

    :cond_19
    move v2, v1

    .line 913
    goto/16 :goto_b

    :cond_1a
    move v2, v1

    .line 917
    goto :goto_c

    :cond_1b
    move v0, v1

    .line 921
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 744
    new-instance v2, Lcom/google/maps/g/pc;

    invoke-direct {v2, p0}, Lcom/google/maps/g/pc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/pe;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/pe;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/pe;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/pe;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/pe;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/pe;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/pe;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/pe;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/g/pe;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/pe;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v4, p0, Lcom/google/maps/g/pe;->d:I

    iput v4, v2, Lcom/google/maps/g/pc;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/pe;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/pe;->k:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/pe;->l:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v4, v2, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/pe;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/pe;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v4, v2, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/pe;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/pe;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget-object v3, v2, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/pe;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/pe;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/pc;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 744
    check-cast p1, Lcom/google/maps/g/pc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/pe;->a(Lcom/google/maps/g/pc;)Lcom/google/maps/g/pe;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 930
    const/4 v0, 0x1

    return v0
.end method
