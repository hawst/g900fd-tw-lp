.class public final enum Lcom/google/maps/g/a/er;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/er;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/er;

.field public static final enum b:Lcom/google/maps/g/a/er;

.field public static final enum c:Lcom/google/maps/g/a/er;

.field public static final enum d:Lcom/google/maps/g/a/er;

.field public static final enum e:Lcom/google/maps/g/a/er;

.field private static final synthetic g:[Lcom/google/maps/g/a/er;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, Lcom/google/maps/g/a/er;

    const-string v1, "SPEED_NODATA"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/er;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    .line 111
    new-instance v0, Lcom/google/maps/g/a/er;

    const-string v1, "SPEED_STOPPED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/er;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/er;->b:Lcom/google/maps/g/a/er;

    .line 115
    new-instance v0, Lcom/google/maps/g/a/er;

    const-string v1, "SPEED_STOP_AND_GO"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/er;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/er;->c:Lcom/google/maps/g/a/er;

    .line 119
    new-instance v0, Lcom/google/maps/g/a/er;

    const-string v1, "SPEED_SLOW"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/er;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/er;->d:Lcom/google/maps/g/a/er;

    .line 123
    new-instance v0, Lcom/google/maps/g/a/er;

    const-string v1, "SPEED_NORMAL"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/er;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/er;->e:Lcom/google/maps/g/a/er;

    .line 102
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/a/er;

    sget-object v1, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/er;->b:Lcom/google/maps/g/a/er;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/er;->c:Lcom/google/maps/g/a/er;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/er;->d:Lcom/google/maps/g/a/er;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/er;->e:Lcom/google/maps/g/a/er;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/a/er;->g:[Lcom/google/maps/g/a/er;

    .line 168
    new-instance v0, Lcom/google/maps/g/a/es;

    invoke-direct {v0}, Lcom/google/maps/g/a/es;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/google/maps/g/a/er;->f:I

    .line 179
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/er;
    .locals 1

    .prologue
    .line 153
    packed-switch p0, :pswitch_data_0

    .line 159
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    goto :goto_0

    .line 155
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/er;->b:Lcom/google/maps/g/a/er;

    goto :goto_0

    .line 156
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/er;->c:Lcom/google/maps/g/a/er;

    goto :goto_0

    .line 157
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/er;->d:Lcom/google/maps/g/a/er;

    goto :goto_0

    .line 158
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/er;->e:Lcom/google/maps/g/a/er;

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/er;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/google/maps/g/a/er;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/er;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/er;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/a/er;->g:[Lcom/google/maps/g/a/er;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/er;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/er;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/google/maps/g/a/er;->f:I

    return v0
.end method
