.class public final enum Lcom/google/maps/g/a/l;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/l;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/l;

.field public static final enum b:Lcom/google/maps/g/a/l;

.field public static final enum c:Lcom/google/maps/g/a/l;

.field public static final enum d:Lcom/google/maps/g/a/l;

.field private static final synthetic f:[Lcom/google/maps/g/a/l;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 123
    new-instance v0, Lcom/google/maps/g/a/l;

    const-string v1, "ROAD_SHIELD"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/g/a/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    .line 127
    new-instance v0, Lcom/google/maps/g/a/l;

    const-string v1, "TURN"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/l;->b:Lcom/google/maps/g/a/l;

    .line 131
    new-instance v0, Lcom/google/maps/g/a/l;

    const-string v1, "TRANSIT_ICON"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/a/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    .line 135
    new-instance v0, Lcom/google/maps/g/a/l;

    const-string v1, "INCIDENT"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/maps/g/a/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/l;->d:Lcom/google/maps/g/a/l;

    .line 118
    new-array v0, v6, [Lcom/google/maps/g/a/l;

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/l;->b:Lcom/google/maps/g/a/l;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/l;->d:Lcom/google/maps/g/a/l;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/a/l;->f:[Lcom/google/maps/g/a/l;

    .line 175
    new-instance v0, Lcom/google/maps/g/a/m;

    invoke-direct {v0}, Lcom/google/maps/g/a/m;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 185
    iput p3, p0, Lcom/google/maps/g/a/l;->e:I

    .line 186
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/l;
    .locals 1

    .prologue
    .line 161
    packed-switch p0, :pswitch_data_0

    .line 166
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    goto :goto_0

    .line 163
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/l;->b:Lcom/google/maps/g/a/l;

    goto :goto_0

    .line 164
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    goto :goto_0

    .line 165
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/l;->d:Lcom/google/maps/g/a/l;

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/l;
    .locals 1

    .prologue
    .line 118
    const-class v0, Lcom/google/maps/g/a/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/l;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/l;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/google/maps/g/a/l;->f:[Lcom/google/maps/g/a/l;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/l;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/maps/g/a/l;->e:I

    return v0
.end method
