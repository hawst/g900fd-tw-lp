.class public final Lcom/google/maps/g/ds;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/dq;",
        "Lcom/google/maps/g/ds;",
        ">;",
        "Lcom/google/maps/g/dt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lcom/google/maps/g/dq;->e:Lcom/google/maps/g/dq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 613
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ds;->c:Ljava/lang/Object;

    .line 522
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 559
    invoke-static {}, Lcom/google/maps/g/dq;->d()Lcom/google/maps/g/dq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 572
    :goto_0
    return-object p0

    .line 560
    :cond_0
    iget v2, p1, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 561
    iget v2, p1, Lcom/google/maps/g/dq;->b:I

    iget v3, p0, Lcom/google/maps/g/ds;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ds;->a:I

    iput v2, p0, Lcom/google/maps/g/ds;->b:I

    .line 563
    :cond_1
    iget v2, p1, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 564
    iget v2, p0, Lcom/google/maps/g/ds;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ds;->a:I

    .line 565
    iget-object v2, p1, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ds;->c:Ljava/lang/Object;

    .line 568
    :cond_2
    iget v2, p1, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 569
    iget v0, p1, Lcom/google/maps/g/dq;->d:I

    iget v1, p0, Lcom/google/maps/g/ds;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/ds;->a:I

    iput v0, p0, Lcom/google/maps/g/ds;->d:I

    .line 571
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/dq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 560
    goto :goto_1

    :cond_5
    move v2, v1

    .line 563
    goto :goto_2

    :cond_6
    move v0, v1

    .line 568
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/maps/g/ds;->c()Lcom/google/maps/g/dq;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 513
    check-cast p1, Lcom/google/maps/g/dq;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ds;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 576
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/dq;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 539
    new-instance v2, Lcom/google/maps/g/dq;

    invoke-direct {v2, p0}, Lcom/google/maps/g/dq;-><init>(Lcom/google/n/v;)V

    .line 540
    iget v3, p0, Lcom/google/maps/g/ds;->a:I

    .line 541
    const/4 v1, 0x0

    .line 542
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 545
    :goto_0
    iget v1, p0, Lcom/google/maps/g/ds;->b:I

    iput v1, v2, Lcom/google/maps/g/dq;->b:I

    .line 546
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 547
    or-int/lit8 v0, v0, 0x2

    .line 549
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/ds;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    .line 550
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 551
    or-int/lit8 v0, v0, 0x4

    .line 553
    :cond_1
    iget v1, p0, Lcom/google/maps/g/ds;->d:I

    iput v1, v2, Lcom/google/maps/g/dq;->d:I

    .line 554
    iput v0, v2, Lcom/google/maps/g/dq;->a:I

    .line 555
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
