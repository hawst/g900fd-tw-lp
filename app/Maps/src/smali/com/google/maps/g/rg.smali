.class public final Lcom/google/maps/g/rg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/rj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rg;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/rg;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4327
    new-instance v0, Lcom/google/maps/g/rh;

    invoke-direct {v0}, Lcom/google/maps/g/rh;-><init>()V

    sput-object v0, Lcom/google/maps/g/rg;->PARSER:Lcom/google/n/ax;

    .line 4443
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/rg;->g:Lcom/google/n/aw;

    .line 4686
    new-instance v0, Lcom/google/maps/g/rg;

    invoke-direct {v0}, Lcom/google/maps/g/rg;-><init>()V

    sput-object v0, Lcom/google/maps/g/rg;->d:Lcom/google/maps/g/rg;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4277
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4400
    iput-byte v0, p0, Lcom/google/maps/g/rg;->e:B

    .line 4422
    iput v0, p0, Lcom/google/maps/g/rg;->f:I

    .line 4278
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/rg;->b:I

    .line 4279
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    .line 4280
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 4286
    invoke-direct {p0}, Lcom/google/maps/g/rg;-><init>()V

    .line 4287
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4291
    const/4 v0, 0x0

    .line 4292
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4293
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4294
    sparse-switch v3, :sswitch_data_0

    .line 4299
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4301
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4297
    goto :goto_0

    .line 4306
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/rg;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/rg;->a:I

    .line 4307
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/rg;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4318
    :catch_0
    move-exception v0

    .line 4319
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4324
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rg;->au:Lcom/google/n/bn;

    throw v0

    .line 4311
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 4312
    iget v4, p0, Lcom/google/maps/g/rg;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/rg;->a:I

    .line 4313
    iput-object v3, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4320
    :catch_1
    move-exception v0

    .line 4321
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4322
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4324
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rg;->au:Lcom/google/n/bn;

    .line 4325
    return-void

    .line 4294
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4275
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4400
    iput-byte v0, p0, Lcom/google/maps/g/rg;->e:B

    .line 4422
    iput v0, p0, Lcom/google/maps/g/rg;->f:I

    .line 4276
    return-void
.end method

.method public static g()Lcom/google/maps/g/rg;
    .locals 1

    .prologue
    .line 4689
    sget-object v0, Lcom/google/maps/g/rg;->d:Lcom/google/maps/g/rg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ri;
    .locals 1

    .prologue
    .line 4505
    new-instance v0, Lcom/google/maps/g/ri;

    invoke-direct {v0}, Lcom/google/maps/g/ri;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4339
    sget-object v0, Lcom/google/maps/g/rg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 4412
    invoke-virtual {p0}, Lcom/google/maps/g/rg;->c()I

    .line 4413
    iget v0, p0, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 4414
    iget v0, p0, Lcom/google/maps/g/rg;->b:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 4416
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4417
    iget-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4419
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/rg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4420
    return-void

    .line 4414
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 4417
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4402
    iget-byte v1, p0, Lcom/google/maps/g/rg;->e:B

    .line 4403
    if-ne v1, v0, :cond_0

    .line 4407
    :goto_0
    return v0

    .line 4404
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4406
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/rg;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4424
    iget v0, p0, Lcom/google/maps/g/rg;->f:I

    .line 4425
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4438
    :goto_0
    return v0

    .line 4428
    :cond_0
    iget v0, p0, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 4429
    iget v0, p0, Lcom/google/maps/g/rg;->b:I

    .line 4430
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 4432
    :goto_2
    iget v0, p0, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4434
    iget-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 4436
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/rg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 4437
    iput v0, p0, Lcom/google/maps/g/rg;->f:I

    goto :goto_0

    .line 4430
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 4434
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4370
    iget-object v0, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    .line 4371
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4372
    check-cast v0, Ljava/lang/String;

    .line 4380
    :goto_0
    return-object v0

    .line 4374
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 4376
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 4377
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4378
    iput-object v1, p0, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 4380
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4269
    invoke-static {}, Lcom/google/maps/g/rg;->newBuilder()Lcom/google/maps/g/ri;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ri;->a(Lcom/google/maps/g/rg;)Lcom/google/maps/g/ri;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4269
    invoke-static {}, Lcom/google/maps/g/rg;->newBuilder()Lcom/google/maps/g/ri;

    move-result-object v0

    return-object v0
.end method
