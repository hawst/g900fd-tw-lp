.class public final Lcom/google/maps/g/a/ce;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/cc;",
        "Lcom/google/maps/g/a/ce;",
        ">;",
        "Lcom/google/maps/g/a/cf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 359
    sget-object v0, Lcom/google/maps/g/a/cc;->h:Lcom/google/maps/g/a/cc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 468
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->b:Lcom/google/n/ao;

    .line 527
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->c:Lcom/google/n/ao;

    .line 586
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->d:Lcom/google/n/ao;

    .line 645
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->e:Lcom/google/n/ao;

    .line 704
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->f:Lcom/google/n/ao;

    .line 763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ce;->g:Lcom/google/n/ao;

    .line 360
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 427
    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 453
    :goto_0
    return-object p0

    .line 428
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 429
    iget-object v2, p0, Lcom/google/maps/g/a/ce;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/cc;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 430
    iget v2, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 432
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 433
    iget-object v2, p0, Lcom/google/maps/g/a/ce;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/cc;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 434
    iget v2, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 436
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 437
    iget-object v2, p0, Lcom/google/maps/g/a/ce;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/cc;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 438
    iget v2, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 440
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 441
    iget-object v2, p0, Lcom/google/maps/g/a/ce;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/cc;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 442
    iget v2, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 444
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 445
    iget-object v2, p0, Lcom/google/maps/g/a/ce;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/cc;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 446
    iget v2, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 448
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/cc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 449
    iget-object v0, p0, Lcom/google/maps/g/a/ce;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/cc;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 450
    iget v0, p0, Lcom/google/maps/g/a/ce;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 452
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/a/cc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 428
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 432
    goto :goto_2

    :cond_9
    move v2, v1

    .line 436
    goto :goto_3

    :cond_a
    move v2, v1

    .line 440
    goto :goto_4

    :cond_b
    move v2, v1

    .line 444
    goto :goto_5

    :cond_c
    move v0, v1

    .line 448
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/google/maps/g/a/ce;->c()Lcom/google/maps/g/a/cc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 351
    check-cast p1, Lcom/google/maps/g/a/cc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ce;->a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 457
    iget v0, p0, Lcom/google/maps/g/a/ce;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/google/maps/g/a/ce;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 463
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 457
    goto :goto_0

    :cond_1
    move v0, v2

    .line 463
    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/a/cc;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 383
    new-instance v2, Lcom/google/maps/g/a/cc;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/cc;-><init>(Lcom/google/n/v;)V

    .line 384
    iget v3, p0, Lcom/google/maps/g/a/ce;->a:I

    .line 386
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 389
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/cc;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->b:Lcom/google/n/ao;

    .line 390
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ce;->b:Lcom/google/n/ao;

    .line 391
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 389
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 392
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 393
    or-int/lit8 v0, v0, 0x2

    .line 395
    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/cc;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->c:Lcom/google/n/ao;

    .line 396
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ce;->c:Lcom/google/n/ao;

    .line 397
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 395
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 398
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 399
    or-int/lit8 v0, v0, 0x4

    .line 401
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/cc;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->d:Lcom/google/n/ao;

    .line 402
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ce;->d:Lcom/google/n/ao;

    .line 403
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 401
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 404
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 405
    or-int/lit8 v0, v0, 0x8

    .line 407
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/cc;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->e:Lcom/google/n/ao;

    .line 408
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ce;->e:Lcom/google/n/ao;

    .line 409
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 407
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 410
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 411
    or-int/lit8 v0, v0, 0x10

    .line 413
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/cc;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->f:Lcom/google/n/ao;

    .line 414
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ce;->f:Lcom/google/n/ao;

    .line 415
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 413
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 416
    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    .line 417
    or-int/lit8 v0, v0, 0x20

    .line 419
    :cond_4
    iget-object v3, v2, Lcom/google/maps/g/a/cc;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/ce;->g:Lcom/google/n/ao;

    .line 420
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/ce;->g:Lcom/google/n/ao;

    .line 421
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 419
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 422
    iput v0, v2, Lcom/google/maps/g/a/cc;->a:I

    .line 423
    return-object v2

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method
