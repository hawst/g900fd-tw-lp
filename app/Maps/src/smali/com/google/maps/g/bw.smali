.class public final Lcom/google/maps/g/bw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/bx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/bu;",
        "Lcom/google/maps/g/bw;",
        ">;",
        "Lcom/google/maps/g/bx;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Z

.field public d:Lcom/google/n/ao;

.field private e:I

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/google/maps/g/bu;->g:Lcom/google/maps/g/bu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 567
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    .line 675
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    .line 766
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bw;->f:Lcom/google/n/ao;

    .line 484
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/bu;)Lcom/google/maps/g/bw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 537
    invoke-static {}, Lcom/google/maps/g/bu;->d()Lcom/google/maps/g/bu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 558
    :goto_0
    return-object p0

    .line 538
    :cond_0
    iget v2, p1, Lcom/google/maps/g/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 539
    iget v2, p0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/bw;->a:I

    .line 540
    iget-object v2, p1, Lcom/google/maps/g/bu;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    .line 543
    :cond_1
    iget v2, p1, Lcom/google/maps/g/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 544
    iget-boolean v2, p1, Lcom/google/maps/g/bu;->c:Z

    iget v3, p0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/bw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/bw;->c:Z

    .line 546
    :cond_2
    iget v2, p1, Lcom/google/maps/g/bu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 547
    iget-object v2, p0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/bu;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 548
    iget v2, p0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/bw;->a:I

    .line 550
    :cond_3
    iget v2, p1, Lcom/google/maps/g/bu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 551
    iget v2, p1, Lcom/google/maps/g/bu;->e:I

    iget v3, p0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/bw;->a:I

    iput v2, p0, Lcom/google/maps/g/bw;->e:I

    .line 553
    :cond_4
    iget v2, p1, Lcom/google/maps/g/bu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 554
    iget-object v0, p0, Lcom/google/maps/g/bw;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/bu;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 555
    iget v0, p0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/bw;->a:I

    .line 557
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/bu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 538
    goto :goto_1

    :cond_7
    move v2, v1

    .line 543
    goto :goto_2

    :cond_8
    move v2, v1

    .line 546
    goto :goto_3

    :cond_9
    move v2, v1

    .line 550
    goto :goto_4

    :cond_a
    move v0, v1

    .line 553
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 475
    new-instance v2, Lcom/google/maps/g/bu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/bu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/bw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/bu;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/maps/g/bw;->c:Z

    iput-boolean v4, v2, Lcom/google/maps/g/bu;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/bu;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/maps/g/bw;->e:I

    iput v4, v2, Lcom/google/maps/g/bu;->e:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/bu;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/bw;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/bw;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/bu;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 475
    check-cast p1, Lcom/google/maps/g/bu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/bw;->a(Lcom/google/maps/g/bu;)Lcom/google/maps/g/bw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 562
    const/4 v0, 0x1

    return v0
.end method
