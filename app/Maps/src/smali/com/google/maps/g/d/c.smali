.class public final Lcom/google/maps/g/d/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/d/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/d/a;",
        "Lcom/google/maps/g/d/c;",
        ">;",
        "Lcom/google/maps/g/d/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Z

.field private h:Z

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 479
    sget-object v0, Lcom/google/maps/g/d/a;->j:Lcom/google/maps/g/d/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 603
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/c;->b:Lcom/google/n/ao;

    .line 662
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/c;->c:Lcom/google/n/ao;

    .line 721
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/c;->d:Lcom/google/n/ao;

    .line 780
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/c;->e:Lcom/google/n/ao;

    .line 839
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/d/c;->f:Lcom/google/n/ao;

    .line 962
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/d/c;->i:I

    .line 480
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/d/a;)Lcom/google/maps/g/d/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 557
    invoke-static {}, Lcom/google/maps/g/d/a;->d()Lcom/google/maps/g/d/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 588
    :goto_0
    return-object p0

    .line 558
    :cond_0
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 559
    iget-object v2, p0, Lcom/google/maps/g/d/c;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 560
    iget v2, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/d/c;->a:I

    .line 562
    :cond_1
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 563
    iget-object v2, p0, Lcom/google/maps/g/d/c;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 564
    iget v2, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/d/c;->a:I

    .line 566
    :cond_2
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 567
    iget-object v2, p0, Lcom/google/maps/g/d/c;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 568
    iget v2, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/d/c;->a:I

    .line 570
    :cond_3
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 571
    iget-object v2, p0, Lcom/google/maps/g/d/c;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 572
    iget v2, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/d/c;->a:I

    .line 574
    :cond_4
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 575
    iget-object v2, p0, Lcom/google/maps/g/d/c;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 576
    iget v2, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/d/c;->a:I

    .line 578
    :cond_5
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 579
    iget-boolean v2, p1, Lcom/google/maps/g/d/a;->g:Z

    iget v3, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/d/c;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/d/c;->g:Z

    .line 581
    :cond_6
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 582
    iget-boolean v2, p1, Lcom/google/maps/g/d/a;->h:Z

    iget v3, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/d/c;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/d/c;->h:Z

    .line 584
    :cond_7
    iget v2, p1, Lcom/google/maps/g/d/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_12

    .line 585
    iget v0, p1, Lcom/google/maps/g/d/a;->i:I

    invoke-static {v0}, Lcom/google/maps/g/d/d;->a(I)Lcom/google/maps/g/d/d;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/d/d;->a:Lcom/google/maps/g/d/d;

    :cond_8
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v2, v1

    .line 558
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 562
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 566
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 570
    goto :goto_4

    :cond_d
    move v2, v1

    .line 574
    goto :goto_5

    :cond_e
    move v2, v1

    .line 578
    goto :goto_6

    :cond_f
    move v2, v1

    .line 581
    goto :goto_7

    :cond_10
    move v0, v1

    .line 584
    goto :goto_8

    .line 585
    :cond_11
    iget v1, p0, Lcom/google/maps/g/d/c;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/d/c;->a:I

    iget v0, v0, Lcom/google/maps/g/d/d;->c:I

    iput v0, p0, Lcom/google/maps/g/d/c;->i:I

    .line 587
    :cond_12
    iget-object v0, p1, Lcom/google/maps/g/d/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 471
    new-instance v2, Lcom/google/maps/g/d/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/d/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/d/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/d/a;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/d/c;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/d/c;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/d/a;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/d/c;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/d/c;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/d/a;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/d/c;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/d/c;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/d/a;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/d/c;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/d/c;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/d/a;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/d/c;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/d/c;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/maps/g/d/c;->g:Z

    iput-boolean v1, v2, Lcom/google/maps/g/d/a;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/d/c;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/d/a;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/maps/g/d/c;->i:I

    iput v1, v2, Lcom/google/maps/g/d/a;->i:I

    iput v0, v2, Lcom/google/maps/g/d/a;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 471
    check-cast p1, Lcom/google/maps/g/d/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/d/c;->a(Lcom/google/maps/g/d/a;)Lcom/google/maps/g/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 592
    iget v0, p0, Lcom/google/maps/g/d/c;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 593
    iget-object v0, p0, Lcom/google/maps/g/d/c;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 598
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 592
    goto :goto_0

    :cond_1
    move v0, v2

    .line 598
    goto :goto_1
.end method
