.class public final enum Lcom/google/maps/g/s;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/s;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/s;

.field private static final synthetic c:[Lcom/google/maps/g/s;


# instance fields
.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 499
    new-instance v0, Lcom/google/maps/g/s;

    const-string v1, "RESTAURANT_RESERVATION"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/s;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    .line 494
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/maps/g/s;

    sget-object v1, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/maps/g/s;->c:[Lcom/google/maps/g/s;

    .line 524
    new-instance v0, Lcom/google/maps/g/t;

    invoke-direct {v0}, Lcom/google/maps/g/t;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 533
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 534
    iput p3, p0, Lcom/google/maps/g/s;->b:I

    .line 535
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/s;
    .locals 1

    .prologue
    .line 513
    packed-switch p0, :pswitch_data_0

    .line 515
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 514
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    goto :goto_0

    .line 513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/s;
    .locals 1

    .prologue
    .line 494
    const-class v0, Lcom/google/maps/g/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/s;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/s;
    .locals 1

    .prologue
    .line 494
    sget-object v0, Lcom/google/maps/g/s;->c:[Lcom/google/maps/g/s;

    invoke-virtual {v0}, [Lcom/google/maps/g/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/s;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 509
    iget v0, p0, Lcom/google/maps/g/s;->b:I

    return v0
.end method
