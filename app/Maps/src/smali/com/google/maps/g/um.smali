.class public final enum Lcom/google/maps/g/um;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/um;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/um;

.field public static final enum b:Lcom/google/maps/g/um;

.field public static final enum c:Lcom/google/maps/g/um;

.field private static final synthetic e:[Lcom/google/maps/g/um;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    new-instance v0, Lcom/google/maps/g/um;

    const-string v1, "SUMMARY_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/um;->a:Lcom/google/maps/g/um;

    .line 146
    new-instance v0, Lcom/google/maps/g/um;

    const-string v1, "EDITORIAL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/um;->b:Lcom/google/maps/g/um;

    .line 150
    new-instance v0, Lcom/google/maps/g/um;

    const-string v1, "USER_REVIEW"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/um;->c:Lcom/google/maps/g/um;

    .line 137
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/um;

    sget-object v1, Lcom/google/maps/g/um;->a:Lcom/google/maps/g/um;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/um;->b:Lcom/google/maps/g/um;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/um;->c:Lcom/google/maps/g/um;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/um;->e:[Lcom/google/maps/g/um;

    .line 185
    new-instance v0, Lcom/google/maps/g/un;

    invoke-direct {v0}, Lcom/google/maps/g/un;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 195
    iput p3, p0, Lcom/google/maps/g/um;->d:I

    .line 196
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/um;
    .locals 1

    .prologue
    .line 172
    packed-switch p0, :pswitch_data_0

    .line 176
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 173
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/um;->a:Lcom/google/maps/g/um;

    goto :goto_0

    .line 174
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/um;->b:Lcom/google/maps/g/um;

    goto :goto_0

    .line 175
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/um;->c:Lcom/google/maps/g/um;

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/um;
    .locals 1

    .prologue
    .line 137
    const-class v0, Lcom/google/maps/g/um;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/um;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/um;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/maps/g/um;->e:[Lcom/google/maps/g/um;

    invoke-virtual {v0}, [Lcom/google/maps/g/um;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/um;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/maps/g/um;->d:I

    return v0
.end method
