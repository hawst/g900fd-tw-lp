.class public final enum Lcom/google/maps/g/a/jp;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/jp;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/jp;

.field public static final enum b:Lcom/google/maps/g/a/jp;

.field public static final enum c:Lcom/google/maps/g/a/jp;

.field public static final enum d:Lcom/google/maps/g/a/jp;

.field public static final enum e:Lcom/google/maps/g/a/jp;

.field private static final synthetic g:[Lcom/google/maps/g/a/jp;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    new-instance v0, Lcom/google/maps/g/a/jp;

    const-string v1, "WAYPOINT_FOUND"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/jp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    .line 133
    new-instance v0, Lcom/google/maps/g/a/jp;

    const-string v1, "WAYPOINT_FOUND_WITH_REFINEMENTS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/jp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jp;->b:Lcom/google/maps/g/a/jp;

    .line 137
    new-instance v0, Lcom/google/maps/g/a/jp;

    const-string v1, "WAYPOINT_REFINEMENTS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/jp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jp;->c:Lcom/google/maps/g/a/jp;

    .line 141
    new-instance v0, Lcom/google/maps/g/a/jp;

    const-string v1, "WAYPOINT_NOT_FOUND"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/jp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jp;->d:Lcom/google/maps/g/a/jp;

    .line 145
    new-instance v0, Lcom/google/maps/g/a/jp;

    const-string v1, "WAYPOINT_FROM_REQUEST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/jp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jp;->e:Lcom/google/maps/g/a/jp;

    .line 124
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/a/jp;

    sget-object v1, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/jp;->b:Lcom/google/maps/g/a/jp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/jp;->c:Lcom/google/maps/g/a/jp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/jp;->d:Lcom/google/maps/g/a/jp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/jp;->e:Lcom/google/maps/g/a/jp;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/a/jp;->g:[Lcom/google/maps/g/a/jp;

    .line 190
    new-instance v0, Lcom/google/maps/g/a/jq;

    invoke-direct {v0}, Lcom/google/maps/g/a/jq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 200
    iput p3, p0, Lcom/google/maps/g/a/jp;->f:I

    .line 201
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/jp;
    .locals 1

    .prologue
    .line 175
    packed-switch p0, :pswitch_data_0

    .line 181
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 176
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    goto :goto_0

    .line 177
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/jp;->b:Lcom/google/maps/g/a/jp;

    goto :goto_0

    .line 178
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/jp;->c:Lcom/google/maps/g/a/jp;

    goto :goto_0

    .line 179
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/jp;->d:Lcom/google/maps/g/a/jp;

    goto :goto_0

    .line 180
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/jp;->e:Lcom/google/maps/g/a/jp;

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/jp;
    .locals 1

    .prologue
    .line 124
    const-class v0, Lcom/google/maps/g/a/jp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jp;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/jp;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/google/maps/g/a/jp;->g:[Lcom/google/maps/g/a/jp;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/jp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/jp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/google/maps/g/a/jp;->f:I

    return v0
.end method
