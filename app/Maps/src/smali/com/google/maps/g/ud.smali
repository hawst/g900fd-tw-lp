.class public final enum Lcom/google/maps/g/ud;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ud;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/ud;

.field public static final enum b:Lcom/google/maps/g/ud;

.field public static final enum c:Lcom/google/maps/g/ud;

.field public static final enum d:Lcom/google/maps/g/ud;

.field public static final enum e:Lcom/google/maps/g/ud;

.field public static final enum f:Lcom/google/maps/g/ud;

.field public static final enum g:Lcom/google/maps/g/ud;

.field public static final enum h:Lcom/google/maps/g/ud;

.field public static final enum i:Lcom/google/maps/g/ud;

.field public static final enum j:Lcom/google/maps/g/ud;

.field public static final enum k:Lcom/google/maps/g/ud;

.field public static final enum l:Lcom/google/maps/g/ud;

.field public static final enum m:Lcom/google/maps/g/ud;

.field public static final enum n:Lcom/google/maps/g/ud;

.field public static final enum o:Lcom/google/maps/g/ud;

.field public static final enum p:Lcom/google/maps/g/ud;

.field public static final enum q:Lcom/google/maps/g/ud;

.field public static final enum r:Lcom/google/maps/g/ud;

.field public static final enum s:Lcom/google/maps/g/ud;

.field public static final enum t:Lcom/google/maps/g/ud;

.field public static final enum u:Lcom/google/maps/g/ud;

.field public static final enum v:Lcom/google/maps/g/ud;

.field private static final synthetic x:[Lcom/google/maps/g/ud;


# instance fields
.field public final w:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_SEARCH"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->a:Lcom/google/maps/g/ud;

    .line 18
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_LOCATION"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->b:Lcom/google/maps/g/ud;

    .line 22
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_HISTORY"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->c:Lcom/google/maps/g/ud;

    .line 26
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_DIRECTIONS"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->d:Lcom/google/maps/g/ud;

    .line 30
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_TRAFFIC"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->e:Lcom/google/maps/g/ud;

    .line 34
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_TRANSIT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->f:Lcom/google/maps/g/ud;

    .line 38
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_BIKING"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->g:Lcom/google/maps/g/ud;

    .line 42
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_AD"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->h:Lcom/google/maps/g/ud;

    .line 46
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_NO_LAYERS"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->i:Lcom/google/maps/g/ud;

    .line 50
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_HOME"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->j:Lcom/google/maps/g/ud;

    .line 54
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_WORK"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->k:Lcom/google/maps/g/ud;

    .line 58
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_CONTACT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->l:Lcom/google/maps/g/ud;

    .line 62
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_ACTIVITY"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->m:Lcom/google/maps/g/ud;

    .line 66
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_TERRAIN"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->n:Lcom/google/maps/g/ud;

    .line 70
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_SEARCH_NEARBY"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->o:Lcom/google/maps/g/ud;

    .line 74
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_STARRED"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->p:Lcom/google/maps/g/ud;

    .line 78
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_REVIEWED"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->q:Lcom/google/maps/g/ud;

    .line 82
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_SHARED"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->r:Lcom/google/maps/g/ud;

    .line 86
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_TRAIN_STATION"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->s:Lcom/google/maps/g/ud;

    .line 90
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_BUS_STATION"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->t:Lcom/google/maps/g/ud;

    .line 94
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_PLACE_PIN"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->u:Lcom/google/maps/g/ud;

    .line 98
    new-instance v0, Lcom/google/maps/g/ud;

    const-string v1, "SUGGEST_ICON_WEB"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ud;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ud;->v:Lcom/google/maps/g/ud;

    .line 9
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/google/maps/g/ud;

    sget-object v1, Lcom/google/maps/g/ud;->a:Lcom/google/maps/g/ud;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/ud;->b:Lcom/google/maps/g/ud;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/ud;->c:Lcom/google/maps/g/ud;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/ud;->d:Lcom/google/maps/g/ud;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/ud;->e:Lcom/google/maps/g/ud;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/ud;->f:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/ud;->g:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/ud;->h:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/ud;->i:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/ud;->j:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/ud;->k:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/ud;->l:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/ud;->m:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/ud;->n:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/ud;->o:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/ud;->p:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/g/ud;->q:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/g/ud;->r:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/g/ud;->s:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/g/ud;->t:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/g/ud;->u:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/g/ud;->v:Lcom/google/maps/g/ud;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/ud;->x:[Lcom/google/maps/g/ud;

    .line 228
    new-instance v0, Lcom/google/maps/g/ue;

    invoke-direct {v0}, Lcom/google/maps/g/ue;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 238
    iput p3, p0, Lcom/google/maps/g/ud;->w:I

    .line 239
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ud;
    .locals 1

    .prologue
    .line 196
    packed-switch p0, :pswitch_data_0

    .line 219
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 197
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/ud;->a:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 198
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/ud;->b:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 199
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/ud;->c:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 200
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/ud;->d:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 201
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/ud;->e:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 202
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/ud;->f:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 203
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/ud;->g:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 204
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/ud;->h:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 205
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/ud;->i:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 206
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/ud;->j:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 207
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/ud;->k:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 208
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/ud;->l:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 209
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/ud;->m:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 210
    :pswitch_d
    sget-object v0, Lcom/google/maps/g/ud;->n:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 211
    :pswitch_e
    sget-object v0, Lcom/google/maps/g/ud;->o:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 212
    :pswitch_f
    sget-object v0, Lcom/google/maps/g/ud;->p:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 213
    :pswitch_10
    sget-object v0, Lcom/google/maps/g/ud;->q:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 214
    :pswitch_11
    sget-object v0, Lcom/google/maps/g/ud;->r:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 215
    :pswitch_12
    sget-object v0, Lcom/google/maps/g/ud;->s:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 216
    :pswitch_13
    sget-object v0, Lcom/google/maps/g/ud;->t:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 217
    :pswitch_14
    sget-object v0, Lcom/google/maps/g/ud;->u:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 218
    :pswitch_15
    sget-object v0, Lcom/google/maps/g/ud;->v:Lcom/google/maps/g/ud;

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ud;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/ud;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ud;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ud;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/ud;->x:[Lcom/google/maps/g/ud;

    invoke-virtual {v0}, [Lcom/google/maps/g/ud;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ud;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/google/maps/g/ud;->w:I

    return v0
.end method
