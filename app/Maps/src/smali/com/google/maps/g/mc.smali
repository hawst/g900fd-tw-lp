.class public final enum Lcom/google/maps/g/mc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/mc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/mc;

.field public static final enum b:Lcom/google/maps/g/mc;

.field public static final enum c:Lcom/google/maps/g/mc;

.field public static final enum d:Lcom/google/maps/g/mc;

.field private static final synthetic f:[Lcom/google/maps/g/mc;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 458
    new-instance v0, Lcom/google/maps/g/mc;

    const-string v1, "ULTRA_LOW_CONFIDENCE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/mc;->a:Lcom/google/maps/g/mc;

    .line 462
    new-instance v0, Lcom/google/maps/g/mc;

    const-string v1, "LOW_CONFIDENCE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/mc;->b:Lcom/google/maps/g/mc;

    .line 466
    new-instance v0, Lcom/google/maps/g/mc;

    const-string v1, "HIGH_CONFIDENCE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/mc;->c:Lcom/google/maps/g/mc;

    .line 470
    new-instance v0, Lcom/google/maps/g/mc;

    const-string v1, "CONFIRMED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/mc;->d:Lcom/google/maps/g/mc;

    .line 453
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/mc;

    sget-object v1, Lcom/google/maps/g/mc;->a:Lcom/google/maps/g/mc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/mc;->b:Lcom/google/maps/g/mc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/mc;->c:Lcom/google/maps/g/mc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/mc;->d:Lcom/google/maps/g/mc;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/mc;->f:[Lcom/google/maps/g/mc;

    .line 510
    new-instance v0, Lcom/google/maps/g/md;

    invoke-direct {v0}, Lcom/google/maps/g/md;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 520
    iput p3, p0, Lcom/google/maps/g/mc;->e:I

    .line 521
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/mc;
    .locals 1

    .prologue
    .line 496
    packed-switch p0, :pswitch_data_0

    .line 501
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 497
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/mc;->a:Lcom/google/maps/g/mc;

    goto :goto_0

    .line 498
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/mc;->b:Lcom/google/maps/g/mc;

    goto :goto_0

    .line 499
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/mc;->c:Lcom/google/maps/g/mc;

    goto :goto_0

    .line 500
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/mc;->d:Lcom/google/maps/g/mc;

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/mc;
    .locals 1

    .prologue
    .line 453
    const-class v0, Lcom/google/maps/g/mc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mc;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/mc;
    .locals 1

    .prologue
    .line 453
    sget-object v0, Lcom/google/maps/g/mc;->f:[Lcom/google/maps/g/mc;

    invoke-virtual {v0}, [Lcom/google/maps/g/mc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/mc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/google/maps/g/mc;->e:I

    return v0
.end method
