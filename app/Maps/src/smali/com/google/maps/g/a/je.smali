.class public final Lcom/google/maps/g/a/je;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/jl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/je;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/a/je;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field public d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field public g:I

.field public h:I

.field public i:Lcom/google/n/ao;

.field j:Ljava/lang/Object;

.field k:Z

.field public l:Ljava/lang/Object;

.field m:Z

.field public n:Ljava/lang/Object;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/google/maps/g/a/jf;

    invoke-direct {v0}, Lcom/google/maps/g/a/jf;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/je;->PARSER:Lcom/google/n/ax;

    .line 836
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/je;->r:Lcom/google/n/aw;

    .line 1874
    new-instance v0, Lcom/google/maps/g/a/je;

    invoke-direct {v0}, Lcom/google/maps/g/a/je;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/je;->o:Lcom/google/maps/g/a/je;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 413
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    .line 545
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    .line 716
    iput-byte v3, p0, Lcom/google/maps/g/a/je;->p:B

    .line 771
    iput v3, p0, Lcom/google/maps/g/a/je;->q:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    .line 23
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/je;->g:I

    .line 24
    iput v2, p0, Lcom/google/maps/g/a/je;->h:I

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    .line 27
    iput-boolean v2, p0, Lcom/google/maps/g/a/je;->k:Z

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    .line 29
    iput-boolean v2, p0, Lcom/google/maps/g/a/je;->m:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/maps/g/a/je;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 43
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 52
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 58
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 59
    iput-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/je;->au:Lcom/google/n/bn;

    throw v0

    .line 63
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 65
    iput-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 144
    :catch_1
    move-exception v0

    .line 145
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 146
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 69
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 70
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/je;->a:I

    goto :goto_0

    .line 74
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 75
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 76
    iput-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    goto :goto_0

    .line 80
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 81
    invoke-static {v0}, Lcom/google/maps/g/a/jh;->a(I)Lcom/google/maps/g/a/jh;

    move-result-object v5

    .line 82
    if-nez v5, :cond_1

    .line 83
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 85
    :cond_1
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 86
    iput v0, p0, Lcom/google/maps/g/a/je;->g:I

    goto :goto_0

    .line 91
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 92
    invoke-static {v0}, Lcom/google/maps/g/a/jj;->a(I)Lcom/google/maps/g/a/jj;

    move-result-object v5

    .line 93
    if-nez v5, :cond_2

    .line 94
    const/4 v5, 0x6

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 96
    :cond_2
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 97
    iput v0, p0, Lcom/google/maps/g/a/je;->h:I

    goto/16 :goto_0

    .line 102
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 103
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/je;->a:I

    goto/16 :goto_0

    .line 107
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 108
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 109
    iput-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 113
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/je;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/je;->k:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 118
    :sswitch_a
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/je;->a:I

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/je;->m:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    .line 123
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 125
    iput-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 129
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 130
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 131
    iput-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 135
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 136
    iget v5, p0, Lcom/google/maps/g/a/je;->a:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/maps/g/a/je;->a:I

    .line 137
    iput-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 148
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->au:Lcom/google/n/bn;

    .line 149
    return-void

    .line 45
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 413
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    .line 545
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    .line 716
    iput-byte v1, p0, Lcom/google/maps/g/a/je;->p:B

    .line 771
    iput v1, p0, Lcom/google/maps/g/a/je;->q:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;
    .locals 1

    .prologue
    .line 901
    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;

    move-result-object v0

    return-object v0
.end method

.method public static i()Lcom/google/maps/g/a/je;
    .locals 1

    .prologue
    .line 1877
    sget-object v0, Lcom/google/maps/g/a/je;->o:Lcom/google/maps/g/a/je;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/jg;
    .locals 1

    .prologue
    .line 898
    new-instance v0, Lcom/google/maps/g/a/jg;

    invoke-direct {v0}, Lcom/google/maps/g/a/jg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/je;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    sget-object v0, Lcom/google/maps/g/a/je;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 728
    invoke-virtual {p0}, Lcom/google/maps/g/a/je;->c()I

    .line 729
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 730
    iget-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 732
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 733
    iget-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 735
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 736
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 738
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 739
    iget-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 741
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 742
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/a/je;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_10

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 744
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 745
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/maps/g/a/je;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_11

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 747
    :cond_5
    :goto_4
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 748
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 750
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_7

    .line 751
    iget-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 753
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    .line 754
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/a/je;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_13

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 756
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_9

    .line 757
    const/16 v0, 0xb

    iget-boolean v3, p0, Lcom/google/maps/g/a/je;->m:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_14

    :goto_7
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 759
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_a

    .line 760
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 762
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 763
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 765
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 766
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 768
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/a/je;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 769
    return-void

    .line 730
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 733
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 739
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 742
    :cond_10
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 745
    :cond_11
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 751
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_13
    move v0, v2

    .line 754
    goto/16 :goto_6

    :cond_14
    move v1, v2

    .line 757
    goto/16 :goto_7

    .line 760
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 763
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 766
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto :goto_a
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 718
    iget-byte v1, p0, Lcom/google/maps/g/a/je;->p:B

    .line 719
    if-ne v1, v0, :cond_0

    .line 723
    :goto_0
    return v0

    .line 720
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 722
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/je;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 773
    iget v0, p0, Lcom/google/maps/g/a/je;->q:I

    .line 774
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 831
    :goto_0
    return v0

    .line 777
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_16

    .line 779
    iget-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 781
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 783
    iget-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 785
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 786
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    .line 787
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 789
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 791
    iget-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 793
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 794
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/a/je;->g:I

    .line 795
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_10

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 797
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 798
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/maps/g/a/je;->h:I

    .line 799
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 801
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 802
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    .line 803
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 805
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_7

    .line 807
    iget-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    :goto_7
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 809
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    .line 810
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/a/je;->k:Z

    .line 811
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 813
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_9

    .line 814
    const/16 v0, 0xb

    iget-boolean v3, p0, Lcom/google/maps/g/a/je;->m:Z

    .line 815
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 817
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_a

    .line 818
    const/16 v3, 0xc

    .line 819
    iget-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 821
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_b

    .line 822
    const/16 v3, 0xd

    .line 823
    iget-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 825
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_c

    .line 826
    const/16 v3, 0xe

    .line 827
    iget-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 829
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/a/je;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 830
    iput v0, p0, Lcom/google/maps/g/a/je;->q:I

    goto/16 :goto_0

    .line 779
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 783
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 791
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 795
    :cond_10
    const/16 v0, 0xa

    goto/16 :goto_5

    .line 799
    :cond_11
    const/16 v0, 0xa

    goto/16 :goto_6

    .line 807
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 819
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 823
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 827
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    :cond_16
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    .line 341
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 342
    check-cast v0, Ljava/lang/String;

    .line 350
    :goto_0
    return-object v0

    .line 344
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 346
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iput-object v1, p0, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 350
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    .line 383
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 384
    check-cast v0, Ljava/lang/String;

    .line 392
    :goto_0
    return-object v0

    .line 386
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 388
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 389
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iput-object v1, p0, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 392
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    .line 483
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 484
    check-cast v0, Ljava/lang/String;

    .line 492
    :goto_0
    return-object v0

    .line 486
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 488
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 489
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 490
    iput-object v1, p0, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 492
    goto :goto_0
.end method
