.class public final Lcom/google/maps/g/de;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/dc;",
        "Lcom/google/maps/g/de;",
        ">;",
        "Lcom/google/maps/g/dj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1183
    sget-object v0, Lcom/google/maps/g/dc;->b:Lcom/google/maps/g/dc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1236
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    .line 1184
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/dc;)Lcom/google/maps/g/de;
    .locals 2

    .prologue
    .line 1208
    invoke-static {}, Lcom/google/maps/g/dc;->d()Lcom/google/maps/g/dc;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1220
    :goto_0
    return-object p0

    .line 1209
    :cond_0
    iget-object v0, p1, Lcom/google/maps/g/dc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1210
    iget-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1211
    iget-object v0, p1, Lcom/google/maps/g/dc;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    .line 1212
    iget v0, p0, Lcom/google/maps/g/de;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/maps/g/de;->a:I

    .line 1219
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/maps/g/dc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1214
    :cond_2
    iget v0, p0, Lcom/google/maps/g/de;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/de;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/de;->a:I

    .line 1215
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/dc;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 1175
    new-instance v0, Lcom/google/maps/g/dc;

    invoke-direct {v0, p0}, Lcom/google/maps/g/dc;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/maps/g/de;->a:I

    iget v1, p0, Lcom/google/maps/g/de;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/de;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/de;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/g/dc;->a:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1175
    check-cast p1, Lcom/google/maps/g/dc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/de;->a(Lcom/google/maps/g/dc;)Lcom/google/maps/g/de;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1224
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1225
    iget-object v0, p0, Lcom/google/maps/g/de;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/df;->d()Lcom/google/maps/g/df;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/df;

    invoke-virtual {v0}, Lcom/google/maps/g/df;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1230
    :goto_1
    return v2

    .line 1224
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1230
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
