.class public final Lcom/google/maps/g/c/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/c/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/c/a;",
        "Lcom/google/maps/g/c/c;",
        ">;",
        "Lcom/google/maps/g/c/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/aq;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/aq;

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1068
    sget-object v0, Lcom/google/maps/g/c/a;->j:Lcom/google/maps/g/c/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1198
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/maps/g/c/c;->b:I

    .line 1234
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/c;->c:Ljava/lang/Object;

    .line 1310
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    .line 1403
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/c;->e:Ljava/lang/Object;

    .line 1479
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/c;->f:Ljava/lang/Object;

    .line 1555
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    .line 1648
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/c/c;->h:Ljava/lang/Object;

    .line 1724
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/c/c;->i:Lcom/google/n/ao;

    .line 1069
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/c/a;)Lcom/google/maps/g/c/c;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1140
    invoke-static {}, Lcom/google/maps/g/c/a;->d()Lcom/google/maps/g/c/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1189
    :goto_0
    return-object p0

    .line 1141
    :cond_0
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1142
    iget v2, p1, Lcom/google/maps/g/c/a;->b:I

    invoke-static {v2}, Lcom/google/maps/g/c/d;->a(I)Lcom/google/maps/g/c/d;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/c/d;->a:Lcom/google/maps/g/c/d;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1141
    goto :goto_1

    .line 1142
    :cond_3
    iget v3, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/c/c;->a:I

    iget v2, v2, Lcom/google/maps/g/c/d;->i:I

    iput v2, p0, Lcom/google/maps/g/c/c;->b:I

    .line 1144
    :cond_4
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1145
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1146
    iget-object v2, p1, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->c:Ljava/lang/Object;

    .line 1149
    :cond_5
    iget-object v2, p1, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1150
    iget-object v2, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1151
    iget-object v2, p1, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    .line 1152
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1159
    :cond_6
    :goto_3
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 1160
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1161
    iget-object v2, p1, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->e:Ljava/lang/Object;

    .line 1164
    :cond_7
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 1165
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1166
    iget-object v2, p1, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->f:Ljava/lang/Object;

    .line 1169
    :cond_8
    iget-object v2, p1, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1170
    iget-object v2, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1171
    iget-object v2, p1, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    .line 1172
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1179
    :cond_9
    :goto_6
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 1180
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1181
    iget-object v2, p1, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/c/c;->h:Ljava/lang/Object;

    .line 1184
    :cond_a
    iget v2, p1, Lcom/google/maps/g/c/a;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v5, :cond_14

    :goto_8
    if-eqz v0, :cond_b

    .line 1185
    iget-object v0, p0, Lcom/google/maps/g/c/c;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1186
    iget v0, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1188
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/c/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 1144
    goto/16 :goto_2

    .line 1154
    :cond_d
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_e

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1155
    :cond_e
    iget-object v2, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 1159
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 1164
    goto/16 :goto_5

    .line 1174
    :cond_11
    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v5, :cond_12

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/c/c;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/c/c;->a:I

    .line 1175
    :cond_12
    iget-object v2, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_13
    move v2, v1

    .line 1179
    goto :goto_7

    :cond_14
    move v0, v1

    .line 1184
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1060
    new-instance v2, Lcom/google/maps/g/c/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/c/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/maps/g/c/c;->b:I

    iput v4, v2, Lcom/google/maps/g/c/a;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/c/c;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->c:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/c/c;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/c/c;->d:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->d:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/c/c;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/c/c;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->f:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/c/c;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/g/c/c;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/c/c;->g:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->g:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/c/c;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/c/a;->h:Ljava/lang/Object;

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v3, v2, Lcom/google/maps/g/c/a;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/c/c;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/c/c;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/c/a;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1060
    check-cast p1, Lcom/google/maps/g/c/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/c/c;->a(Lcom/google/maps/g/c/a;)Lcom/google/maps/g/c/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1193
    const/4 v0, 0x1

    return v0
.end method
