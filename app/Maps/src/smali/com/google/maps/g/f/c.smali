.class public final Lcom/google/maps/g/f/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/f/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/f/a;",
        "Lcom/google/maps/g/f/c;",
        ">;",
        "Lcom/google/maps/g/f/d;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:I

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 461
    sget-object v0, Lcom/google/maps/g/f/a;->j:Lcom/google/maps/g/f/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 590
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/f/c;->b:I

    .line 626
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/c;->c:Lcom/google/n/ao;

    .line 685
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/c;->d:Lcom/google/n/ao;

    .line 744
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/c;->e:Lcom/google/n/ao;

    .line 803
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/f/c;->f:Ljava/lang/Object;

    .line 911
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/f/c;->h:I

    .line 947
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/c;->i:Lcom/google/n/ao;

    .line 462
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/f/a;)Lcom/google/maps/g/f/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 537
    invoke-static {}, Lcom/google/maps/g/f/a;->d()Lcom/google/maps/g/f/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 569
    :goto_0
    return-object p0

    .line 538
    :cond_0
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 539
    iget v2, p1, Lcom/google/maps/g/f/a;->b:I

    invoke-static {v2}, Lcom/google/maps/g/f/e;->a(I)Lcom/google/maps/g/f/e;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 538
    goto :goto_1

    .line 539
    :cond_3
    iget v3, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/f/c;->a:I

    iget v2, v2, Lcom/google/maps/g/f/e;->f:I

    iput v2, p0, Lcom/google/maps/g/f/c;->b:I

    .line 541
    :cond_4
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 542
    iget-object v2, p0, Lcom/google/maps/g/f/c;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 543
    iget v2, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/f/c;->a:I

    .line 545
    :cond_5
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 546
    iget-object v2, p0, Lcom/google/maps/g/f/c;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 547
    iget v2, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/f/c;->a:I

    .line 549
    :cond_6
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 550
    iget-object v2, p0, Lcom/google/maps/g/f/c;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 551
    iget v2, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/f/c;->a:I

    .line 553
    :cond_7
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 554
    iget v2, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/f/c;->a:I

    .line 555
    iget-object v2, p1, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/f/c;->f:Ljava/lang/Object;

    .line 558
    :cond_8
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 559
    iget-boolean v2, p1, Lcom/google/maps/g/f/a;->g:Z

    iget v3, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/f/c;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/f/c;->g:Z

    .line 561
    :cond_9
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 562
    iget v2, p1, Lcom/google/maps/g/f/a;->h:I

    invoke-static {v2}, Lcom/google/maps/g/f/k;->a(I)Lcom/google/maps/g/f/k;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, Lcom/google/maps/g/f/k;->a:Lcom/google/maps/g/f/k;

    :cond_a
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v2, v1

    .line 541
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 545
    goto :goto_3

    :cond_d
    move v2, v1

    .line 549
    goto :goto_4

    :cond_e
    move v2, v1

    .line 553
    goto :goto_5

    :cond_f
    move v2, v1

    .line 558
    goto :goto_6

    :cond_10
    move v2, v1

    .line 561
    goto :goto_7

    .line 562
    :cond_11
    iget v3, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/f/c;->a:I

    iget v2, v2, Lcom/google/maps/g/f/k;->d:I

    iput v2, p0, Lcom/google/maps/g/f/c;->h:I

    .line 564
    :cond_12
    iget v2, p1, Lcom/google/maps/g/f/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    :goto_8
    if-eqz v0, :cond_13

    .line 565
    iget-object v0, p0, Lcom/google/maps/g/f/c;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 566
    iget v0, p0, Lcom/google/maps/g/f/c;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/f/c;->a:I

    .line 568
    :cond_13
    iget-object v0, p1, Lcom/google/maps/g/f/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_14
    move v0, v1

    .line 564
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 453
    new-instance v2, Lcom/google/maps/g/f/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/f/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/f/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/maps/g/f/c;->b:I

    iput v4, v2, Lcom/google/maps/g/f/a;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/f/c;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/f/c;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/f/c;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/f/c;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/f/c;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/f/c;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/f/c;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/maps/g/f/c;->g:Z

    iput-boolean v4, v2, Lcom/google/maps/g/f/a;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/maps/g/f/c;->h:I

    iput v4, v2, Lcom/google/maps/g/f/a;->h:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/f/c;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/f/c;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/f/a;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 453
    check-cast p1, Lcom/google/maps/g/f/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/f/c;->a(Lcom/google/maps/g/f/a;)Lcom/google/maps/g/f/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 573
    iget v0, p0, Lcom/google/maps/g/f/c;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/google/maps/g/f/c;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 585
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 573
    goto :goto_0

    .line 579
    :cond_1
    iget v0, p0, Lcom/google/maps/g/f/c;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 580
    iget-object v0, p0, Lcom/google/maps/g/f/c;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 582
    goto :goto_1

    :cond_2
    move v0, v1

    .line 579
    goto :goto_2

    :cond_3
    move v0, v2

    .line 585
    goto :goto_1
.end method
