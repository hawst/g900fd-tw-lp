.class public final Lcom/google/maps/g/nr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nr;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/nr;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/aq;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/hg;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 729
    new-instance v0, Lcom/google/maps/g/ns;

    invoke-direct {v0}, Lcom/google/maps/g/ns;-><init>()V

    sput-object v0, Lcom/google/maps/g/nr;->PARSER:Lcom/google/n/ax;

    .line 906
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/nr;->h:Lcom/google/n/aw;

    .line 1360
    new-instance v0, Lcom/google/maps/g/nr;

    invoke-direct {v0}, Lcom/google/maps/g/nr;-><init>()V

    sput-object v0, Lcom/google/maps/g/nr;->e:Lcom/google/maps/g/nr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 660
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 851
    iput-byte v0, p0, Lcom/google/maps/g/nr;->f:B

    .line 876
    iput v0, p0, Lcom/google/maps/g/nr;->g:I

    .line 661
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    .line 662
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    .line 663
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    .line 664
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    .line 670
    invoke-direct {p0}, Lcom/google/maps/g/nr;-><init>()V

    .line 673
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 676
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 677
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 678
    sparse-switch v1, :sswitch_data_0

    .line 683
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 685
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 681
    goto :goto_0

    .line 690
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 691
    iget v5, p0, Lcom/google/maps/g/nr;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/nr;->a:I

    .line 692
    iput-object v1, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 714
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 715
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 720
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v6, :cond_1

    .line 721
    iget-object v2, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    .line 723
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_2

    .line 724
    iget-object v1, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    .line 726
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nr;->au:Lcom/google/n/bn;

    throw v0

    .line 696
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v5

    .line 697
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v6, :cond_7

    .line 698
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 699
    or-int/lit8 v1, v0, 0x2

    .line 701
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v0, v5}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 702
    goto :goto_0

    .line 705
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_3

    .line 706
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    .line 707
    or-int/lit8 v0, v0, 0x4

    .line 709
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    sget-object v5, Lcom/google/maps/g/hg;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 716
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 717
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 718
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 720
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v6, :cond_5

    .line 721
    iget-object v1, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    .line 723
    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_6

    .line 724
    iget-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    .line 726
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nr;->au:Lcom/google/n/bn;

    .line 727
    return-void

    .line 720
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 716
    :catch_2
    move-exception v0

    goto :goto_4

    .line 714
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto :goto_3

    .line 678
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 658
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 851
    iput-byte v0, p0, Lcom/google/maps/g/nr;->f:B

    .line 876
    iput v0, p0, Lcom/google/maps/g/nr;->g:I

    .line 659
    return-void
.end method

.method public static d()Lcom/google/maps/g/nr;
    .locals 1

    .prologue
    .line 1363
    sget-object v0, Lcom/google/maps/g/nr;->e:Lcom/google/maps/g/nr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/nt;
    .locals 1

    .prologue
    .line 968
    new-instance v0, Lcom/google/maps/g/nt;

    invoke-direct {v0}, Lcom/google/maps/g/nt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741
    sget-object v0, Lcom/google/maps/g/nr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 863
    invoke-virtual {p0}, Lcom/google/maps/g/nr;->c()I

    .line 864
    iget v0, p0, Lcom/google/maps/g/nr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 865
    iget-object v0, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v0, v1

    .line 867
    :goto_1
    iget-object v2, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 868
    iget-object v2, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 867
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 865
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 870
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 871
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 870
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 873
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/nr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 874
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 853
    iget-byte v1, p0, Lcom/google/maps/g/nr;->f:B

    .line 854
    if-ne v1, v0, :cond_0

    .line 858
    :goto_0
    return v0

    .line 855
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 857
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/nr;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 878
    iget v0, p0, Lcom/google/maps/g/nr;->g:I

    .line 879
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 901
    :goto_0
    return v0

    .line 882
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 884
    iget-object v0, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nr;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v1

    .line 888
    :goto_3
    iget-object v4, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 889
    iget-object v4, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    .line 890
    invoke-interface {v4, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 888
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 884
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 892
    :cond_2
    add-int/2addr v0, v3

    .line 893
    iget-object v2, p0, Lcom/google/maps/g/nr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 895
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 896
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/nr;->d:Ljava/util/List;

    .line 897
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 895
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 899
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/nr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 900
    iput v0, p0, Lcom/google/maps/g/nr;->g:I

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 652
    invoke-static {}, Lcom/google/maps/g/nr;->newBuilder()Lcom/google/maps/g/nt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/nt;->a(Lcom/google/maps/g/nr;)Lcom/google/maps/g/nt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 652
    invoke-static {}, Lcom/google/maps/g/nr;->newBuilder()Lcom/google/maps/g/nt;

    move-result-object v0

    return-object v0
.end method
