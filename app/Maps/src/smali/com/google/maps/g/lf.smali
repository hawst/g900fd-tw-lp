.class public final enum Lcom/google/maps/g/lf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/lf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/lf;

.field public static final enum b:Lcom/google/maps/g/lf;

.field public static final enum c:Lcom/google/maps/g/lf;

.field public static final enum d:Lcom/google/maps/g/lf;

.field private static final synthetic f:[Lcom/google/maps/g/lf;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 577
    new-instance v0, Lcom/google/maps/g/lf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/lf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/lf;->a:Lcom/google/maps/g/lf;

    .line 581
    new-instance v0, Lcom/google/maps/g/lf;

    const-string v1, "EASY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/lf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/lf;->b:Lcom/google/maps/g/lf;

    .line 585
    new-instance v0, Lcom/google/maps/g/lf;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/lf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/lf;->c:Lcom/google/maps/g/lf;

    .line 589
    new-instance v0, Lcom/google/maps/g/lf;

    const-string v1, "HARD"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/lf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/lf;->d:Lcom/google/maps/g/lf;

    .line 572
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/lf;

    sget-object v1, Lcom/google/maps/g/lf;->a:Lcom/google/maps/g/lf;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/lf;->b:Lcom/google/maps/g/lf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/lf;->c:Lcom/google/maps/g/lf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/lf;->d:Lcom/google/maps/g/lf;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/lf;->f:[Lcom/google/maps/g/lf;

    .line 629
    new-instance v0, Lcom/google/maps/g/lg;

    invoke-direct {v0}, Lcom/google/maps/g/lg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 638
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 639
    iput p3, p0, Lcom/google/maps/g/lf;->e:I

    .line 640
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/lf;
    .locals 1

    .prologue
    .line 615
    packed-switch p0, :pswitch_data_0

    .line 620
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 616
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/lf;->a:Lcom/google/maps/g/lf;

    goto :goto_0

    .line 617
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/lf;->b:Lcom/google/maps/g/lf;

    goto :goto_0

    .line 618
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/lf;->c:Lcom/google/maps/g/lf;

    goto :goto_0

    .line 619
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/lf;->d:Lcom/google/maps/g/lf;

    goto :goto_0

    .line 615
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/lf;
    .locals 1

    .prologue
    .line 572
    const-class v0, Lcom/google/maps/g/lf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lf;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/lf;
    .locals 1

    .prologue
    .line 572
    sget-object v0, Lcom/google/maps/g/lf;->f:[Lcom/google/maps/g/lf;

    invoke-virtual {v0}, [Lcom/google/maps/g/lf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/lf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/google/maps/g/lf;->e:I

    return v0
.end method
