.class public final Lcom/google/maps/g/jw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/jx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ju;",
        "Lcom/google/maps/g/jw;",
        ">;",
        "Lcom/google/maps/g/jx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcom/google/maps/g/ju;->e:Lcom/google/maps/g/ju;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 385
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/jw;->b:Ljava/lang/Object;

    .line 461
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/jw;->c:Ljava/lang/Object;

    .line 537
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/jw;->d:Lcom/google/n/ao;

    .line 321
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ju;)Lcom/google/maps/g/jw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 360
    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 376
    :goto_0
    return-object p0

    .line 361
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ju;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 362
    iget v2, p0, Lcom/google/maps/g/jw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/jw;->a:I

    .line 363
    iget-object v2, p1, Lcom/google/maps/g/ju;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/jw;->b:Ljava/lang/Object;

    .line 366
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ju;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 367
    iget v2, p0, Lcom/google/maps/g/jw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/jw;->a:I

    .line 368
    iget-object v2, p1, Lcom/google/maps/g/ju;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/jw;->c:Ljava/lang/Object;

    .line 371
    :cond_2
    iget v2, p1, Lcom/google/maps/g/ju;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 372
    iget-object v0, p0, Lcom/google/maps/g/jw;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ju;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 373
    iget v0, p0, Lcom/google/maps/g/jw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/jw;->a:I

    .line 375
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/ju;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 361
    goto :goto_1

    :cond_5
    move v2, v1

    .line 366
    goto :goto_2

    :cond_6
    move v0, v1

    .line 371
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 312
    new-instance v2, Lcom/google/maps/g/ju;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ju;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/jw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/jw;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ju;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/jw;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ju;->c:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/ju;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/jw;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/jw;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/ju;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 312
    check-cast p1, Lcom/google/maps/g/ju;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/jw;->a(Lcom/google/maps/g/ju;)Lcom/google/maps/g/jw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x1

    return v0
.end method
