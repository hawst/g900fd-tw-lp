.class public final Lcom/google/maps/g/a/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/a;",
        "Lcom/google/maps/g/a/c;",
        ">;",
        "Lcom/google/maps/g/a/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/aq;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/google/maps/g/a/a;->e:Lcom/google/maps/g/a/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 415
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    .line 508
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/c;->c:Ljava/lang/Object;

    .line 584
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/c;->d:Ljava/lang/Object;

    .line 346
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/a;)Lcom/google/maps/g/a/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 384
    invoke-static {}, Lcom/google/maps/g/a/a;->d()Lcom/google/maps/g/a/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 406
    :goto_0
    return-object p0

    .line 385
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 386
    iget-object v2, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 387
    iget-object v2, p1, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    .line 388
    iget v2, p0, Lcom/google/maps/g/a/c;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/a/c;->a:I

    .line 395
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/a/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 396
    iget v2, p0, Lcom/google/maps/g/a/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/c;->a:I

    .line 397
    iget-object v2, p1, Lcom/google/maps/g/a/a;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/c;->c:Ljava/lang/Object;

    .line 400
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 401
    iget v0, p0, Lcom/google/maps/g/a/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/c;->a:I

    .line 402
    iget-object v0, p1, Lcom/google/maps/g/a/a;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/c;->d:Ljava/lang/Object;

    .line 405
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/a/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 390
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/c;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/a/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/c;->a:I

    .line 391
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 395
    goto :goto_2

    :cond_7
    move v0, v1

    .line 400
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 337
    new-instance v2, Lcom/google/maps/g/a/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/c;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/a/c;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/a/c;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/a/c;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/c;->b:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/c;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/a;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/c;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/a;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/a/a;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 337
    check-cast p1, Lcom/google/maps/g/a/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/c;->a(Lcom/google/maps/g/a/a;)Lcom/google/maps/g/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x1

    return v0
.end method
