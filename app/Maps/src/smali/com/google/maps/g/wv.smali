.class public final Lcom/google/maps/g/wv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ww;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/wt;",
        "Lcom/google/maps/g/wv;",
        ">;",
        "Lcom/google/maps/g/ww;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/google/maps/g/wt;->c:Lcom/google/maps/g/wt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 370
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    .line 507
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    .line 307
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/wt;)Lcom/google/maps/g/wv;
    .locals 2

    .prologue
    .line 338
    invoke-static {}, Lcom/google/maps/g/wt;->d()Lcom/google/maps/g/wt;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 360
    :goto_0
    return-object p0

    .line 339
    :cond_0
    iget-object v0, p1, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341
    iget-object v0, p1, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    .line 342
    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/maps/g/wv;->a:I

    .line 349
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 350
    iget-object v0, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 351
    iget-object v0, p1, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    .line 352
    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/wv;->a:I

    .line 359
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/wt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/wv;->a:I

    .line 345
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 354
    :cond_5
    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/wv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/wv;->a:I

    .line 355
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 298
    new-instance v0, Lcom/google/maps/g/wt;

    invoke-direct {v0, p0}, Lcom/google/maps/g/wt;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/maps/g/wv;->a:I

    iget v1, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/wv;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/wv;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/wv;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/wv;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/wv;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 298
    check-cast p1, Lcom/google/maps/g/wt;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/wv;->a(Lcom/google/maps/g/wt;)Lcom/google/maps/g/wv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x1

    return v0
.end method
