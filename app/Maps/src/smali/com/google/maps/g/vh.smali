.class public final Lcom/google/maps/g/vh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/vf;",
        "Lcom/google/maps/g/vh;",
        ">;",
        "Lcom/google/maps/g/vo;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 691
    sget-object v0, Lcom/google/maps/g/vf;->l:Lcom/google/maps/g/vf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 840
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vh;->b:Lcom/google/n/ao;

    .line 899
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vh;->c:Ljava/lang/Object;

    .line 1039
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/vh;->f:I

    .line 1076
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    .line 1212
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vh;->h:Lcom/google/n/ao;

    .line 1271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vh;->i:Ljava/lang/Object;

    .line 1347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vh;->j:Ljava/lang/Object;

    .line 1424
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    .line 692
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/vf;)Lcom/google/maps/g/vh;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 777
    invoke-static {}, Lcom/google/maps/g/vf;->g()Lcom/google/maps/g/vf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 831
    :goto_0
    return-object p0

    .line 778
    :cond_0
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 779
    iget-object v2, p0, Lcom/google/maps/g/vh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 780
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 782
    :cond_1
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 783
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 784
    iget-object v2, p1, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/vh;->c:Ljava/lang/Object;

    .line 787
    :cond_2
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 788
    iget-boolean v2, p1, Lcom/google/maps/g/vf;->d:Z

    iget v3, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/vh;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/vh;->d:Z

    .line 790
    :cond_3
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 791
    iget-boolean v2, p1, Lcom/google/maps/g/vf;->e:Z

    iget v3, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/vh;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/vh;->e:Z

    .line 793
    :cond_4
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 794
    iget v2, p1, Lcom/google/maps/g/vf;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/vi;->a:Lcom/google/maps/g/vi;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 778
    goto :goto_1

    :cond_7
    move v2, v1

    .line 782
    goto :goto_2

    :cond_8
    move v2, v1

    .line 787
    goto :goto_3

    :cond_9
    move v2, v1

    .line 790
    goto :goto_4

    :cond_a
    move v2, v1

    .line 793
    goto :goto_5

    .line 794
    :cond_b
    iget v3, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/vh;->a:I

    iget v2, v2, Lcom/google/maps/g/vi;->d:I

    iput v2, p0, Lcom/google/maps/g/vh;->f:I

    .line 796
    :cond_c
    iget-object v2, p1, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 797
    iget-object v2, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 798
    iget-object v2, p1, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    .line 799
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 806
    :cond_d
    :goto_6
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 807
    iget-object v2, p0, Lcom/google/maps/g/vh;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 808
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 810
    :cond_e
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_f

    .line 811
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 812
    iget-object v2, p1, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/vh;->i:Ljava/lang/Object;

    .line 815
    :cond_f
    iget v2, p1, Lcom/google/maps/g/vf;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    :goto_9
    if-eqz v0, :cond_10

    .line 816
    iget v0, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/vh;->a:I

    .line 817
    iget-object v0, p1, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/vh;->j:Ljava/lang/Object;

    .line 820
    :cond_10
    iget-object v0, p1, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 821
    iget-object v0, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 822
    iget-object v0, p1, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    .line 823
    iget v0, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/maps/g/vh;->a:I

    .line 830
    :cond_11
    :goto_a
    iget-object v0, p1, Lcom/google/maps/g/vf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 801
    :cond_12
    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/vh;->a:I

    .line 802
    :cond_13
    iget-object v2, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 806
    goto :goto_7

    :cond_15
    move v2, v1

    .line 810
    goto :goto_8

    :cond_16
    move v0, v1

    .line 815
    goto :goto_9

    .line 825
    :cond_17
    iget v0, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_18

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vh;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/vh;->a:I

    .line 826
    :cond_18
    iget-object v0, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 683
    new-instance v2, Lcom/google/maps/g/vf;

    invoke-direct {v2, p0}, Lcom/google/maps/g/vf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/vh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/vh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/vh;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/maps/g/vh;->d:Z

    iput-boolean v4, v2, Lcom/google/maps/g/vf;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/vh;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/vf;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/vh;->f:I

    iput v4, v2, Lcom/google/maps/g/vf;->f:I

    iget v4, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/g/vh;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/vh;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/vh;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/vh;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/vh;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/vh;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vh;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/g/vh;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/vh;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/vf;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 683
    check-cast p1, Lcom/google/maps/g/vf;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/vh;->a(Lcom/google/maps/g/vf;)Lcom/google/maps/g/vh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 835
    const/4 v0, 0x1

    return v0
.end method
