.class public final enum Lcom/google/maps/g/a/ca;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/ca;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/ca;

.field public static final enum b:Lcom/google/maps/g/a/ca;

.field public static final enum c:Lcom/google/maps/g/a/ca;

.field public static final enum d:Lcom/google/maps/g/a/ca;

.field public static final enum e:Lcom/google/maps/g/a/ca;

.field private static final synthetic g:[Lcom/google/maps/g/a/ca;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 14
    new-instance v0, Lcom/google/maps/g/a/ca;

    const-string v1, "INCIDENT_ROAD_CLOSED"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/maps/g/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    .line 18
    new-instance v0, Lcom/google/maps/g/a/ca;

    const-string v1, "INCIDENT_ACCIDENT"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    .line 22
    new-instance v0, Lcom/google/maps/g/a/ca;

    const-string v1, "INCIDENT_CONSTRUCTION"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    .line 26
    new-instance v0, Lcom/google/maps/g/a/ca;

    const-string v1, "INCIDENT_JAM"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/g/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    .line 30
    new-instance v0, Lcom/google/maps/g/a/ca;

    const-string v1, "INCIDENT_OTHER"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/g/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/a/ca;

    sget-object v1, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/a/ca;->g:[Lcom/google/maps/g/a/ca;

    .line 75
    new-instance v0, Lcom/google/maps/g/a/cb;

    invoke-direct {v0}, Lcom/google/maps/g/a/cb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput p3, p0, Lcom/google/maps/g/a/ca;->f:I

    .line 86
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/ca;
    .locals 1

    .prologue
    .line 60
    sparse-switch p0, :sswitch_data_0

    .line 66
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 61
    :sswitch_0
    sget-object v0, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    goto :goto_0

    .line 62
    :sswitch_1
    sget-object v0, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    goto :goto_0

    .line 63
    :sswitch_2
    sget-object v0, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    goto :goto_0

    .line 64
    :sswitch_3
    sget-object v0, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    goto :goto_0

    .line 65
    :sswitch_4
    sget-object v0, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    goto :goto_0

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/ca;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/a/ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ca;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/ca;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/a/ca;->g:[Lcom/google/maps/g/a/ca;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/ca;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/ca;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/maps/g/a/ca;->f:I

    return v0
.end method
