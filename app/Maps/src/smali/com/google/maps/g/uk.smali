.class public final Lcom/google/maps/g/uk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ul;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ui;",
        "Lcom/google/maps/g/uk;",
        ">;",
        "Lcom/google/maps/g/ul;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 452
    sget-object v0, Lcom/google/maps/g/ui;->d:Lcom/google/maps/g/ui;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 453
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ui;)Lcom/google/maps/g/uk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 484
    invoke-static {}, Lcom/google/maps/g/ui;->d()Lcom/google/maps/g/ui;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 492
    :goto_0
    return-object p0

    .line 485
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 486
    iget v2, p1, Lcom/google/maps/g/ui;->b:I

    iget v3, p0, Lcom/google/maps/g/uk;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/uk;->a:I

    iput v2, p0, Lcom/google/maps/g/uk;->b:I

    .line 488
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 489
    iget v0, p1, Lcom/google/maps/g/ui;->c:I

    iget v1, p0, Lcom/google/maps/g/uk;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/uk;->a:I

    iput v0, p0, Lcom/google/maps/g/uk;->c:I

    .line 491
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/ui;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 485
    goto :goto_1

    :cond_4
    move v0, v1

    .line 488
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 444
    new-instance v2, Lcom/google/maps/g/ui;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ui;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/uk;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/uk;->b:I

    iput v1, v2, Lcom/google/maps/g/ui;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/uk;->c:I

    iput v1, v2, Lcom/google/maps/g/ui;->c:I

    iput v0, v2, Lcom/google/maps/g/ui;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 444
    check-cast p1, Lcom/google/maps/g/ui;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/uk;->a(Lcom/google/maps/g/ui;)Lcom/google/maps/g/uk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x1

    return v0
.end method
