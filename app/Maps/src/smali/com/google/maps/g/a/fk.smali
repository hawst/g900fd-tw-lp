.class public final Lcom/google/maps/g/a/fk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/fk;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/a/fk;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field c:Ljava/lang/Object;

.field public d:Lcom/google/maps/g/a/ai;

.field public e:Lcom/google/maps/g/a/be;

.field public f:Lcom/google/maps/g/a/ek;

.field public g:Lcom/google/maps/g/a/gy;

.field h:Lcom/google/maps/g/a/cc;

.field public i:Z

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/maps/g/a/au;

.field l:Lcom/google/maps/g/a/bq;

.field public m:Lcom/google/maps/g/a/cs;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/google/maps/g/a/fl;

    invoke-direct {v0}, Lcom/google/maps/g/a/fl;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/fk;->PARSER:Lcom/google/n/ax;

    .line 612
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/fk;->r:Lcom/google/n/aw;

    .line 1750
    new-instance v0, Lcom/google/maps/g/a/fk;

    invoke-direct {v0}, Lcom/google/maps/g/a/fk;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/fk;->o:Lcom/google/maps/g/a/fk;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 486
    iput-byte v0, p0, Lcom/google/maps/g/a/fk;->p:B

    .line 547
    iput v0, p0, Lcom/google/maps/g/a/fk;->q:I

    .line 18
    iput v1, p0, Lcom/google/maps/g/a/fk;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    .line 20
    iput-boolean v1, p0, Lcom/google/maps/g/a/fk;->i:Z

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/a/fk;-><init>()V

    .line 30
    const/4 v1, 0x0

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 34
    const/4 v0, 0x0

    move v3, v0

    .line 35
    :goto_0
    if-nez v3, :cond_d

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 44
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 39
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 50
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    .line 51
    if-nez v2, :cond_2

    .line 52
    const/4 v2, 0x1

    invoke-virtual {v4, v2, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit16 v2, v1, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_0

    .line 199
    iget-object v2, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 201
    :cond_0
    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 202
    iget-object v1, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    .line 204
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fk;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :cond_2
    :try_start_2
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/fk;->a:I

    .line 55
    iput v0, p0, Lcom/google/maps/g/a/fk;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 194
    :catch_1
    move-exception v0

    .line 195
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 196
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 61
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/fk;->a:I

    .line 62
    iput-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_3
    const/4 v0, 0x0

    .line 67
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v5, 0x4

    if-ne v2, v5, :cond_18

    .line 68
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    invoke-static {}, Lcom/google/maps/g/a/ai;->newBuilder()Lcom/google/maps/g/a/ak;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ak;->a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;

    move-result-object v0

    move-object v2, v0

    .line 70
    :goto_4
    sget-object v0, Lcom/google/maps/g/a/ai;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    .line 71
    if-eqz v2, :cond_3

    .line 72
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ak;->a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;

    .line 73
    invoke-virtual {v2}, Lcom/google/maps/g/a/ak;->c()Lcom/google/maps/g/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    .line 75
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 79
    :sswitch_4
    const/4 v0, 0x0

    .line 80
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v5, 0x8

    if-ne v2, v5, :cond_17

    .line 81
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->newBuilder()Lcom/google/maps/g/a/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    move-object v2, v0

    .line 83
    :goto_5
    sget-object v0, Lcom/google/maps/g/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    .line 84
    if-eqz v2, :cond_4

    .line 85
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    .line 86
    invoke-virtual {v2}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    .line 88
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 92
    :sswitch_5
    const/4 v0, 0x0

    .line 93
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v5, 0x10

    if-ne v2, v5, :cond_16

    .line 94
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    invoke-static {}, Lcom/google/maps/g/a/ek;->newBuilder()Lcom/google/maps/g/a/em;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/em;->a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;

    move-result-object v0

    move-object v2, v0

    .line 96
    :goto_6
    sget-object v0, Lcom/google/maps/g/a/ek;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ek;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    .line 97
    if-eqz v2, :cond_5

    .line 98
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/em;->a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;

    .line 99
    invoke-virtual {v2}, Lcom/google/maps/g/a/em;->c()Lcom/google/maps/g/a/ek;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    .line 101
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 105
    :sswitch_6
    const/4 v0, 0x0

    .line 106
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v5, 0x20

    if-ne v2, v5, :cond_15

    .line 107
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    invoke-static {v0}, Lcom/google/maps/g/a/gy;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v0

    move-object v2, v0

    .line 109
    :goto_7
    sget-object v0, Lcom/google/maps/g/a/gy;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gy;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    .line 110
    if-eqz v2, :cond_6

    .line 111
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ha;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    .line 112
    invoke-virtual {v2}, Lcom/google/maps/g/a/ha;->c()Lcom/google/maps/g/a/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    .line 114
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 118
    :sswitch_7
    const/4 v0, 0x0

    .line 119
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v5, 0x40

    if-ne v2, v5, :cond_14

    .line 120
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    invoke-static {}, Lcom/google/maps/g/a/cc;->newBuilder()Lcom/google/maps/g/a/ce;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ce;->a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;

    move-result-object v0

    move-object v2, v0

    .line 122
    :goto_8
    sget-object v0, Lcom/google/maps/g/a/cc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cc;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    .line 123
    if-eqz v2, :cond_7

    .line 124
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ce;->a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;

    .line 125
    invoke-virtual {v2}, Lcom/google/maps/g/a/ce;->c()Lcom/google/maps/g/a/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    .line 127
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 131
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    .line 132
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lcom/google/maps/g/a/fk;->i:Z

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_9

    .line 136
    :sswitch_9
    and-int/lit16 v0, v1, 0x100

    const/16 v2, 0x100

    if-eq v0, v2, :cond_9

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 138
    or-int/lit16 v1, v1, 0x100

    .line 140
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/da;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 144
    :sswitch_a
    const/4 v0, 0x0

    .line 145
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v5, 0x100

    if-ne v2, v5, :cond_13

    .line 146
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    invoke-static {}, Lcom/google/maps/g/a/au;->newBuilder()Lcom/google/maps/g/a/aw;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v0

    move-object v2, v0

    .line 148
    :goto_a
    sget-object v0, Lcom/google/maps/g/a/au;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/au;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    .line 149
    if-eqz v2, :cond_a

    .line 150
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    .line 151
    invoke-virtual {v2}, Lcom/google/maps/g/a/aw;->c()Lcom/google/maps/g/a/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    .line 153
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 157
    :sswitch_b
    const/4 v0, 0x0

    .line 158
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v5, 0x400

    if-ne v2, v5, :cond_12

    .line 159
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    invoke-static {}, Lcom/google/maps/g/a/cs;->newBuilder()Lcom/google/maps/g/a/cu;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/cu;->a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;

    move-result-object v0

    move-object v2, v0

    .line 161
    :goto_b
    sget-object v0, Lcom/google/maps/g/a/cs;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cs;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    .line 162
    if-eqz v2, :cond_b

    .line 163
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/cu;->a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;

    .line 164
    invoke-virtual {v2}, Lcom/google/maps/g/a/cu;->c()Lcom/google/maps/g/a/cs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    .line 166
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 170
    :sswitch_c
    const/4 v0, 0x0

    .line 171
    iget v2, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v5, 0x200

    if-ne v2, v5, :cond_11

    .line 172
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    invoke-static {}, Lcom/google/maps/g/a/bq;->newBuilder()Lcom/google/maps/g/a/bs;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v0

    move-object v2, v0

    .line 174
    :goto_c
    sget-object v0, Lcom/google/maps/g/a/bq;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/bq;

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    .line 175
    if-eqz v2, :cond_c

    .line 176
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    .line 177
    invoke-virtual {v2}, Lcom/google/maps/g/a/bs;->c()Lcom/google/maps/g/a/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    .line 179
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/fk;->a:I

    goto/16 :goto_0

    .line 183
    :sswitch_d
    and-int/lit16 v0, v1, 0x1000

    const/16 v2, 0x1000

    if-eq v0, v2, :cond_10

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 185
    or-int/lit16 v0, v1, 0x1000

    .line 187
    :goto_d
    :try_start_5
    iget-object v1, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/ee;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_e
    move v1, v0

    .line 191
    goto/16 :goto_0

    .line 198
    :cond_d
    and-int/lit16 v0, v1, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_e

    .line 199
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 201
    :cond_e
    and-int/lit16 v0, v1, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_f

    .line 202
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    .line 204
    :cond_f
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->au:Lcom/google/n/bn;

    .line 205
    return-void

    .line 198
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 194
    :catch_2
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_3

    .line 192
    :catch_3
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_1

    :cond_10
    move v0, v1

    goto :goto_d

    :cond_11
    move-object v2, v0

    goto :goto_c

    :cond_12
    move-object v2, v0

    goto/16 :goto_b

    :cond_13
    move-object v2, v0

    goto/16 :goto_a

    :cond_14
    move-object v2, v0

    goto/16 :goto_8

    :cond_15
    move-object v2, v0

    goto/16 :goto_7

    :cond_16
    move-object v2, v0

    goto/16 :goto_6

    :cond_17
    move-object v2, v0

    goto/16 :goto_5

    :cond_18
    move-object v2, v0

    goto/16 :goto_4

    :cond_19
    move v0, v1

    goto :goto_e

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 486
    iput-byte v0, p0, Lcom/google/maps/g/a/fk;->p:B

    .line 547
    iput v0, p0, Lcom/google/maps/g/a/fk;->q:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;
    .locals 1

    .prologue
    .line 677
    invoke-static {}, Lcom/google/maps/g/a/fk;->newBuilder()Lcom/google/maps/g/a/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/maps/g/a/fk;
    .locals 1

    .prologue
    .line 1753
    sget-object v0, Lcom/google/maps/g/a/fk;->o:Lcom/google/maps/g/a/fk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/fm;
    .locals 1

    .prologue
    .line 674
    new-instance v0, Lcom/google/maps/g/a/fm;

    invoke-direct {v0}, Lcom/google/maps/g/a/fm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/fk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    sget-object v0, Lcom/google/maps/g/a/fk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 504
    invoke-virtual {p0}, Lcom/google/maps/g/a/fk;->c()I

    .line 505
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 506
    iget v0, p0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 508
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 511
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 512
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_2
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 514
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_3

    .line 515
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_3
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 517
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 518
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v0

    :goto_4
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 520
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 521
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    :goto_5
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 523
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 524
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v0

    :goto_6
    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 526
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 527
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/a/fk;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_f

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_7
    move v1, v2

    .line 529
    :goto_8
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 530
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 529
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 506
    :cond_8
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 509
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 512
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_2

    .line 515
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_3

    .line 518
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_4

    .line 521
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto/16 :goto_5

    .line 524
    :cond_e
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    goto :goto_6

    :cond_f
    move v0, v2

    .line 527
    goto :goto_7

    .line 532
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_11

    .line 533
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v0, :cond_14

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_9
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 535
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_12

    .line 536
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v0

    :goto_a
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 538
    :cond_12
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_13

    .line 539
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/maps/g/a/bq;->d()Lcom/google/maps/g/a/bq;

    move-result-object v0

    :goto_b
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 541
    :cond_13
    :goto_c
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 542
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 541
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 533
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_9

    .line 536
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    goto :goto_a

    .line 539
    :cond_16
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    goto :goto_b

    .line 544
    :cond_17
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 545
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 488
    iget-byte v0, p0, Lcom/google/maps/g/a/fk;->p:B

    .line 489
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 499
    :goto_0
    return v0

    .line 490
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 492
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 493
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/a/cc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 494
    iput-byte v2, p0, Lcom/google/maps/g/a/fk;->p:B

    move v0, v2

    .line 495
    goto :goto_0

    :cond_2
    move v0, v2

    .line 492
    goto :goto_1

    .line 493
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    goto :goto_2

    .line 498
    :cond_4
    iput-byte v1, p0, Lcom/google/maps/g/a/fk;->p:B

    move v0, v1

    .line 499
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 549
    iget v0, p0, Lcom/google/maps/g/a/fk;->q:I

    .line 550
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 607
    :goto_0
    return v0

    .line 553
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_17

    .line 554
    iget v0, p0, Lcom/google/maps/g/a/fk;->b:I

    .line 555
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 557
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 559
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 561
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 562
    const/4 v3, 0x3

    .line 563
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 565
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 567
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 569
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 570
    const/4 v3, 0x6

    .line 571
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v0

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 573
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 574
    const/4 v3, 0x7

    .line 575
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 577
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 579
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v0

    :goto_8
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 581
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 582
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/a/fk;->i:Z

    .line 583
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_7
    move v3, v1

    move v1, v2

    .line 585
    :goto_9
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 586
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 587
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 585
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 555
    :cond_8
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 559
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 563
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_4

    .line 567
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_5

    .line 571
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_6

    .line 575
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto/16 :goto_7

    .line 579
    :cond_e
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    goto :goto_8

    .line 589
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_10

    .line 590
    const/16 v1, 0xb

    .line 591
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_a
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 593
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_11

    .line 594
    const/16 v1, 0xc

    .line 595
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    if-nez v0, :cond_14

    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v0

    :goto_b
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 597
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_12

    .line 598
    const/16 v1, 0xe

    .line 599
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/maps/g/a/bq;->d()Lcom/google/maps/g/a/bq;

    move-result-object v0

    :goto_c
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_12
    move v1, v2

    .line 601
    :goto_d
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 602
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    .line 603
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 601
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 591
    :cond_13
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto/16 :goto_a

    .line 595
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    goto :goto_b

    .line 599
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    goto :goto_c

    .line 605
    :cond_16
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 606
    iput v0, p0, Lcom/google/maps/g/a/fk;->q:I

    goto/16 :goto_0

    :cond_17
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    .line 252
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 253
    check-cast v0, Ljava/lang/String;

    .line 261
    :goto_0
    return-object v0

    .line 255
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 257
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 258
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iput-object v1, p0, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 261
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/fk;->newBuilder()Lcom/google/maps/g/a/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/fk;->newBuilder()Lcom/google/maps/g/a/fm;

    move-result-object v0

    return-object v0
.end method
