.class public final Lcom/google/maps/g/a/eu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/eu;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Lcom/google/maps/g/a/eu;

.field private static final serialVersionUID:J

.field private static volatile u:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field public f:I

.field public g:I

.field public h:I

.field i:I

.field public j:I

.field public k:I

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/google/n/ao;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public p:I

.field q:Z

.field private s:B

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lcom/google/maps/g/a/ev;

    invoke-direct {v0}, Lcom/google/maps/g/a/ev;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/eu;->PARSER:Lcom/google/n/ax;

    .line 1355
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/eu;->u:Lcom/google/n/aw;

    .line 2659
    new-instance v0, Lcom/google/maps/g/a/eu;

    invoke-direct {v0}, Lcom/google/maps/g/a/eu;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/eu;->r:Lcom/google/maps/g/a/eu;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 886
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    .line 902
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    .line 918
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    .line 1114
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    .line 1202
    iput-byte v4, p0, Lcom/google/maps/g/a/eu;->s:B

    .line 1278
    iput v4, p0, Lcom/google/maps/g/a/eu;->t:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput v2, p0, Lcom/google/maps/g/a/eu;->f:I

    .line 23
    iput v2, p0, Lcom/google/maps/g/a/eu;->g:I

    .line 24
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/g/a/eu;->h:I

    .line 25
    iput v2, p0, Lcom/google/maps/g/a/eu;->i:I

    .line 26
    iput v2, p0, Lcom/google/maps/g/a/eu;->j:I

    .line 27
    iput v2, p0, Lcom/google/maps/g/a/eu;->k:I

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    .line 32
    iput v2, p0, Lcom/google/maps/g/a/eu;->p:I

    .line 33
    iput-boolean v2, p0, Lcom/google/maps/g/a/eu;->q:Z

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v12, 0x2000

    const/16 v11, 0x800

    const/16 v10, 0x400

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/maps/g/a/eu;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 46
    :cond_0
    :goto_0
    if-nez v4, :cond_c

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 53
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 55
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 51
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 61
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x400

    if-ne v2, v10, :cond_1

    .line 188
    iget-object v2, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    .line 190
    :cond_1
    and-int/lit16 v2, v1, 0x800

    if-ne v2, v11, :cond_2

    .line 191
    iget-object v2, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    .line 193
    :cond_2
    and-int/lit16 v1, v1, 0x2000

    if-ne v1, v12, :cond_3

    .line 194
    iget-object v1, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    .line 196
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/eu;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 183
    :catch_1
    move-exception v0

    .line 184
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 185
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 71
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    goto/16 :goto_0

    .line 75
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    iget v6, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 77
    iput-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 81
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 82
    invoke-static {v0}, Lcom/google/maps/g/a/ez;->a(I)Lcom/google/maps/g/a/ez;

    move-result-object v6

    .line 83
    if-nez v6, :cond_4

    .line 84
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 86
    :cond_4
    iget v6, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 87
    iput v0, p0, Lcom/google/maps/g/a/eu;->f:I

    goto/16 :goto_0

    .line 92
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 93
    invoke-static {v0}, Lcom/google/maps/g/a/fd;->a(I)Lcom/google/maps/g/a/fd;

    move-result-object v6

    .line 94
    if-nez v6, :cond_5

    .line 95
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 97
    :cond_5
    iget v6, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 98
    iput v0, p0, Lcom/google/maps/g/a/eu;->g:I

    goto/16 :goto_0

    .line 103
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 104
    invoke-static {v0}, Lcom/google/maps/g/a/fb;->a(I)Lcom/google/maps/g/a/fb;

    move-result-object v6

    .line 105
    if-nez v6, :cond_6

    .line 106
    const/16 v6, 0x9

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 108
    :cond_6
    iget v6, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 109
    iput v0, p0, Lcom/google/maps/g/a/eu;->h:I

    goto/16 :goto_0

    .line 114
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 115
    invoke-static {v0}, Lcom/google/maps/g/a/ex;->a(I)Lcom/google/maps/g/a/ex;

    move-result-object v6

    .line 116
    if-nez v6, :cond_7

    .line 117
    const/16 v6, 0xa

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 119
    :cond_7
    iget v6, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 120
    iput v0, p0, Lcom/google/maps/g/a/eu;->i:I

    goto/16 :goto_0

    .line 125
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 126
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/eu;->j:I

    goto/16 :goto_0

    .line 130
    :sswitch_a
    and-int/lit16 v0, v1, 0x400

    if-eq v0, v10, :cond_8

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    .line 133
    or-int/lit16 v1, v1, 0x400

    .line 135
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 136
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 135
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 140
    :sswitch_b
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v11, :cond_9

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    .line 143
    or-int/lit16 v1, v1, 0x800

    .line 145
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 146
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 145
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 150
    :sswitch_c
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 151
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    goto/16 :goto_0

    .line 155
    :sswitch_d
    and-int/lit16 v0, v1, 0x2000

    if-eq v0, v12, :cond_a

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    .line 158
    or-int/lit16 v1, v1, 0x2000

    .line 160
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 161
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 160
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 165
    :sswitch_e
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 166
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/eu;->p:I

    goto/16 :goto_0

    .line 170
    :sswitch_f
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 171
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_b

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/eu;->q:Z

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_1

    .line 175
    :sswitch_10
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/eu;->a:I

    .line 176
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/eu;->k:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 187
    :cond_c
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v10, :cond_d

    .line 188
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    .line 190
    :cond_d
    and-int/lit16 v0, v1, 0x800

    if-ne v0, v11, :cond_e

    .line 191
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    .line 193
    :cond_e
    and-int/lit16 v0, v1, 0x2000

    if-ne v0, v12, :cond_f

    .line 194
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    .line 196
    :cond_f
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->au:Lcom/google/n/bn;

    .line 197
    return-void

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
        0x80 -> :sswitch_e
        0x88 -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 886
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    .line 902
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    .line 918
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    .line 1114
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    .line 1202
    iput-byte v1, p0, Lcom/google/maps/g/a/eu;->s:B

    .line 1278
    iput v1, p0, Lcom/google/maps/g/a/eu;->t:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/eu;
    .locals 1

    .prologue
    .line 2662
    sget-object v0, Lcom/google/maps/g/a/eu;->r:Lcom/google/maps/g/a/eu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/ew;
    .locals 1

    .prologue
    .line 1417
    new-instance v0, Lcom/google/maps/g/a/ew;

    invoke-direct {v0}, Lcom/google/maps/g/a/ew;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/eu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    sget-object v0, Lcom/google/maps/g/a/eu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 1226
    invoke-virtual {p0}, Lcom/google/maps/g/a/eu;->c()I

    .line 1227
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_0

    .line 1228
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1230
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1231
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1233
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 1234
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1236
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1237
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1239
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_4

    .line 1240
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/maps/g/a/eu;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1242
    :cond_4
    :goto_1
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1243
    iget v0, p0, Lcom/google/maps/g/a/eu;->g:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1245
    :cond_5
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 1246
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/g/a/eu;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_c

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1248
    :cond_6
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 1249
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/maps/g/a/eu;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1251
    :cond_7
    :goto_4
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 1252
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/maps/g/a/eu;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_8
    :goto_5
    move v1, v2

    .line 1254
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 1255
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1254
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1237
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1240
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 1243
    :cond_b
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 1246
    :cond_c
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 1249
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 1252
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    :cond_f
    move v1, v2

    .line 1257
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 1258
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1260
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_11

    .line 1261
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_11
    move v1, v2

    .line 1263
    :goto_8
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 1264
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1263
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 1266
    :cond_12
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_13

    .line 1267
    iget v0, p0, Lcom/google/maps/g/a/eu;->p:I

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_16

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1269
    :cond_13
    :goto_9
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_14

    .line 1270
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/maps/g/a/eu;->q:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_17

    move v0, v3

    :goto_a
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1272
    :cond_14
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_15

    .line 1273
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/maps/g/a/eu;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_18

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1275
    :cond_15
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1276
    return-void

    .line 1267
    :cond_16
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    :cond_17
    move v0, v2

    .line 1270
    goto :goto_a

    .line 1273
    :cond_18
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1204
    iget-byte v0, p0, Lcom/google/maps/g/a/eu;->s:B

    .line 1205
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1221
    :cond_0
    :goto_0
    return v2

    .line 1206
    :cond_1
    if-eqz v0, :cond_0

    .line 1208
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 1209
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1210
    iput-byte v2, p0, Lcom/google/maps/g/a/eu;->s:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1208
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1214
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1215
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/bu;->d()Lcom/google/maps/g/a/bu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/bu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1216
    iput-byte v2, p0, Lcom/google/maps/g/a/eu;->s:B

    goto :goto_0

    .line 1214
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1220
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/g/a/eu;->s:B

    move v2, v3

    .line 1221
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1280
    iget v0, p0, Lcom/google/maps/g/a/eu;->t:I

    .line 1281
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1350
    :goto_0
    return v0

    .line 1284
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_18

    .line 1285
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    .line 1286
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1288
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_1

    .line 1289
    const/4 v2, 0x3

    iget-object v4, p0, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    .line 1290
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1292
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_17

    .line 1293
    const/4 v2, 0x5

    iget-object v4, p0, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    .line 1294
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 1296
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 1297
    const/4 v4, 0x6

    .line 1298
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1300
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_3

    .line 1301
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/maps/g/a/eu;->f:I

    .line 1302
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1304
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 1305
    iget v0, p0, Lcom/google/maps/g/a/eu;->g:I

    .line 1306
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1308
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 1309
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/maps/g/a/eu;->h:I

    .line 1310
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1312
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_6

    .line 1313
    iget v0, p0, Lcom/google/maps/g/a/eu;->i:I

    .line 1314
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1316
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_7

    .line 1317
    const/16 v0, 0xb

    iget v4, p0, Lcom/google/maps/g/a/eu;->j:I

    .line 1318
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    :cond_7
    move v4, v2

    move v2, v1

    .line 1320
    :goto_9
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 1321
    const/16 v5, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    .line 1322
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1320
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 1298
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_9
    move v0, v3

    .line 1302
    goto/16 :goto_4

    :cond_a
    move v0, v3

    .line 1306
    goto/16 :goto_5

    :cond_b
    move v0, v3

    .line 1310
    goto :goto_6

    :cond_c
    move v0, v3

    .line 1314
    goto :goto_7

    :cond_d
    move v0, v3

    .line 1318
    goto :goto_8

    :cond_e
    move v2, v1

    .line 1324
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_f

    .line 1325
    const/16 v5, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    .line 1326
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1324
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    .line 1328
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_10

    .line 1329
    const/16 v0, 0xe

    iget-object v2, p0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    .line 1330
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    :cond_10
    move v2, v1

    .line 1332
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_11

    .line 1333
    const/16 v5, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    .line 1334
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1332
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 1336
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_12

    .line 1337
    iget v0, p0, Lcom/google/maps/g/a/eu;->p:I

    .line 1338
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_16

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1340
    :cond_12
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_13

    .line 1341
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/maps/g/a/eu;->q:Z

    .line 1342
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 1344
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_15

    .line 1345
    const/16 v0, 0x12

    iget v2, p0, Lcom/google/maps/g/a/eu;->k:I

    .line 1346
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_14

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_14
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 1348
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1349
    iput v0, p0, Lcom/google/maps/g/a/eu;->t:I

    goto/16 :goto_0

    :cond_16
    move v0, v3

    .line 1338
    goto :goto_c

    :cond_17
    move v2, v0

    goto/16 :goto_2

    :cond_18
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/eu;->newBuilder()Lcom/google/maps/g/a/ew;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/ew;->a(Lcom/google/maps/g/a/eu;)Lcom/google/maps/g/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/eu;->newBuilder()Lcom/google/maps/g/a/ew;

    move-result-object v0

    return-object v0
.end method
