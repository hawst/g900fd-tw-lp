.class public final Lcom/google/maps/g/a/du;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dz;


# static fields
.field private static volatile C:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/du;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final z:Lcom/google/maps/g/a/du;


# instance fields
.field private A:B

.field private B:I

.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Lcom/google/n/ao;

.field i:Z

.field j:Z

.field k:Lcom/google/n/ao;

.field l:I

.field m:Z

.field n:Lcom/google/n/ao;

.field o:Z

.field p:Z

.field q:Z

.field r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field s:Z

.field t:Lcom/google/n/ao;

.field u:Lcom/google/n/ao;

.field v:Lcom/google/n/ao;

.field w:Lcom/google/n/ao;

.field x:Z

.field y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/google/maps/g/a/dv;

    invoke-direct {v0}, Lcom/google/maps/g/a/dv;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/du;->PARSER:Lcom/google/n/ax;

    .line 894
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/du;->C:Lcom/google/n/aw;

    .line 2309
    new-instance v0, Lcom/google/maps/g/a/du;

    invoke-direct {v0}, Lcom/google/maps/g/a/du;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/du;->z:Lcom/google/maps/g/a/du;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 316
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    .line 332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    .line 408
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    .line 454
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    .line 501
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    .line 615
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    .line 631
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    .line 647
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    .line 692
    iput-byte v4, p0, Lcom/google/maps/g/a/du;->A:B

    .line 780
    iput v4, p0, Lcom/google/maps/g/a/du;->B:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->d:Z

    .line 21
    iput-boolean v2, p0, Lcom/google/maps/g/a/du;->e:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/maps/g/a/du;->f:Z

    .line 23
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->g:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->i:Z

    .line 26
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->j:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/du;->l:I

    .line 29
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->m:Z

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iput-boolean v2, p0, Lcom/google/maps/g/a/du;->o:Z

    .line 32
    iput-boolean v2, p0, Lcom/google/maps/g/a/du;->p:Z

    .line 33
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->q:Z

    .line 34
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 35
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->s:Z

    .line 36
    iget-object v0, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 37
    iget-object v0, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 38
    iget-object v0, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 39
    iget-object v0, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 40
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->x:Z

    .line 41
    iput-boolean v3, p0, Lcom/google/maps/g/a/du;->y:Z

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/4 v4, -0x1

    const/high16 v12, 0x10000

    const-wide/16 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/maps/g/a/du;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v5, v3

    move v1, v3

    .line 54
    :cond_0
    :goto_0
    if-nez v5, :cond_15

    .line 55
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 61
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v5, v2

    .line 63
    goto :goto_0

    :sswitch_0
    move v5, v2

    .line 59
    goto :goto_0

    .line 68
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 69
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    :catchall_0
    move-exception v0

    and-int/2addr v1, v12

    if-ne v1, v12, :cond_1

    .line 218
    iget-object v1, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 220
    :cond_1
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/du;->au:Lcom/google/n/bn;

    throw v0

    .line 73
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 74
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 213
    :catch_1
    move-exception v0

    .line 214
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 215
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 78
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->e:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 83
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->f:Z

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_2

    .line 88
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->g:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_3

    .line 93
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 94
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 98
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->d:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_4

    .line 103
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->i:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_5

    .line 108
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 109
    invoke-static {v0}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v7

    .line 110
    if-nez v7, :cond_7

    .line 111
    const/16 v7, 0x9

    invoke-virtual {v6, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 113
    :cond_7
    iget v7, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v7, v7, 0x400

    iput v7, p0, Lcom/google/maps/g/a/du;->a:I

    .line 114
    iput v0, p0, Lcom/google/maps/g/a/du;->l:I

    goto/16 :goto_0

    .line 119
    :sswitch_a
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_8

    move v0, v2

    :goto_6
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->m:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_6

    .line 124
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 129
    :sswitch_c
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_9

    move v0, v2

    :goto_7
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->o:Z

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_7

    .line 134
    :sswitch_d
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_a

    move v0, v2

    :goto_8
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->p:Z

    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto :goto_8

    .line 139
    :sswitch_e
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const v7, 0x8000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_b

    move v0, v2

    :goto_9
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->q:Z

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_9

    .line 144
    :sswitch_f
    and-int v0, v1, v12

    if-eq v0, v12, :cond_c

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 146
    or-int/2addr v1, v12

    .line 148
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 152
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 153
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v7

    .line 154
    and-int v0, v1, v12

    if-eq v0, v12, :cond_d

    iget v0, p1, Lcom/google/n/j;->f:I

    const v8, 0x7fffffff

    if-ne v0, v8, :cond_e

    move v0, v4

    :goto_a
    if-lez v0, :cond_d

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 156
    or-int/2addr v1, v12

    .line 158
    :cond_d
    :goto_b
    iget v0, p1, Lcom/google/n/j;->f:I

    const v8, 0x7fffffff

    if-ne v0, v8, :cond_f

    move v0, v4

    :goto_c
    if-lez v0, :cond_10

    .line 159
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 154
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v8, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v8

    iget v8, p1, Lcom/google/n/j;->f:I

    sub-int v0, v8, v0

    goto :goto_a

    .line 158
    :cond_f
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v8, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v8

    iget v8, p1, Lcom/google/n/j;->f:I

    sub-int v0, v8, v0

    goto :goto_c

    .line 161
    :cond_10
    iput v7, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 165
    :sswitch_11
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/2addr v0, v12

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 166
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_11

    move v0, v2

    :goto_d
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->s:Z

    goto/16 :goto_0

    :cond_11
    move v0, v3

    goto :goto_d

    .line 170
    :sswitch_12
    iget-object v0, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 171
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x20000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 175
    :sswitch_13
    iget-object v0, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 176
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x40000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 180
    :sswitch_14
    iget-object v0, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 181
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x80000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 185
    :sswitch_15
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 186
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_12

    move v0, v2

    :goto_e
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->j:Z

    goto/16 :goto_0

    :cond_12
    move v0, v3

    goto :goto_e

    .line 190
    :sswitch_16
    iget-object v0, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 191
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 195
    :sswitch_17
    iget-object v0, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    .line 196
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x100000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    goto/16 :goto_0

    .line 200
    :sswitch_18
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x200000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_13

    move v0, v2

    :goto_f
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->x:Z

    goto/16 :goto_0

    :cond_13
    move v0, v3

    goto :goto_f

    .line 205
    :sswitch_19
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v7, 0x400000

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/du;->a:I

    .line 206
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-eqz v0, :cond_14

    move v0, v2

    :goto_10
    iput-boolean v0, p0, Lcom/google/maps/g/a/du;->y:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_14
    move v0, v3

    goto :goto_10

    .line 217
    :cond_15
    and-int v0, v1, v12

    if-ne v0, v12, :cond_16

    .line 218
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 220
    :cond_16
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/du;->au:Lcom/google/n/bn;

    .line 221
    return-void

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x7a -> :sswitch_10
        0x80 -> :sswitch_11
        0x8a -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 316
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    .line 332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    .line 408
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    .line 454
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    .line 501
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    .line 615
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    .line 631
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    .line 647
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    .line 692
    iput-byte v1, p0, Lcom/google/maps/g/a/du;->A:B

    .line 780
    iput v1, p0, Lcom/google/maps/g/a/du;->B:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/du;
    .locals 1

    .prologue
    .line 2312
    sget-object v0, Lcom/google/maps/g/a/du;->z:Lcom/google/maps/g/a/du;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/dw;
    .locals 1

    .prologue
    .line 956
    new-instance v0, Lcom/google/maps/g/a/dw;

    invoke-direct {v0}, Lcom/google/maps/g/a/dw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/du;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    sget-object v0, Lcom/google/maps/g/a/du;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 704
    invoke-virtual {p0}, Lcom/google/maps/g/a/du;->c()I

    .line 705
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 706
    iget-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 708
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 709
    iget-object v0, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 711
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 712
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_e

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 714
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 715
    iget-boolean v0, p0, Lcom/google/maps/g/a/du;->f:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_f

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 717
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 718
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_10

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 720
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 721
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 723
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_6

    .line 724
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_11

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 726
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 727
    iget-boolean v0, p0, Lcom/google/maps/g/a/du;->i:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_12

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 729
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_8

    .line 730
    const/16 v0, 0x9

    iget v3, p0, Lcom/google/maps/g/a/du;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_13

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 732
    :cond_8
    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_9

    .line 733
    const/16 v0, 0xa

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->m:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_14

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 735
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_a

    .line 736
    const/16 v0, 0xb

    iget-object v3, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 738
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_b

    .line 739
    const/16 v0, 0xc

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->o:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_15

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 741
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_c

    .line 742
    const/16 v0, 0xd

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->p:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_16

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 744
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_d

    .line 745
    const/16 v0, 0xe

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->q:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_17

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_d
    move v3, v2

    .line 747
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_19

    .line 748
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_18

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 747
    :goto_b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    :cond_e
    move v0, v2

    .line 712
    goto/16 :goto_0

    :cond_f
    move v0, v2

    .line 715
    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 718
    goto/16 :goto_2

    :cond_11
    move v0, v2

    .line 724
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 727
    goto/16 :goto_4

    .line 730
    :cond_13
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    :cond_14
    move v0, v2

    .line 733
    goto/16 :goto_6

    :cond_15
    move v0, v2

    .line 739
    goto :goto_7

    :cond_16
    move v0, v2

    .line 742
    goto :goto_8

    :cond_17
    move v0, v2

    .line 745
    goto :goto_9

    .line 748
    :cond_18
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_b

    .line 750
    :cond_19
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_1a

    .line 751
    const/16 v0, 0x10

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->s:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_23

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 753
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_1b

    .line 754
    const/16 v0, 0x11

    iget-object v3, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 756
    :cond_1b
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_1c

    .line 757
    const/16 v0, 0x13

    iget-object v3, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 759
    :cond_1c
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_1d

    .line 760
    const/16 v0, 0x14

    iget-object v3, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 762
    :cond_1d
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_1e

    .line 763
    const/16 v0, 0x15

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_24

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 765
    :cond_1e
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1f

    .line 766
    const/16 v0, 0x16

    iget-object v3, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 768
    :cond_1f
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v0, v3

    const/high16 v3, 0x100000

    if-ne v0, v3, :cond_20

    .line 769
    const/16 v0, 0x17

    iget-object v3, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 771
    :cond_20
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v0, v3

    const/high16 v3, 0x200000

    if-ne v0, v3, :cond_21

    .line 772
    const/16 v0, 0x18

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->x:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_25

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 774
    :cond_21
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v0, v3

    const/high16 v3, 0x400000

    if-ne v0, v3, :cond_22

    .line 775
    const/16 v0, 0x19

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->y:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_26

    :goto_f
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 777
    :cond_22
    iget-object v0, p0, Lcom/google/maps/g/a/du;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 778
    return-void

    :cond_23
    move v0, v2

    .line 751
    goto/16 :goto_c

    :cond_24
    move v0, v2

    .line 763
    goto/16 :goto_d

    :cond_25
    move v0, v2

    .line 772
    goto :goto_e

    :cond_26
    move v1, v2

    .line 775
    goto :goto_f
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 694
    iget-byte v1, p0, Lcom/google/maps/g/a/du;->A:B

    .line 695
    if-ne v1, v0, :cond_0

    .line 699
    :goto_0
    return v0

    .line 696
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 698
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/du;->A:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 782
    iget v0, p0, Lcom/google/maps/g/a/du;->B:I

    .line 783
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 889
    :goto_0
    return v0

    .line 786
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1a

    .line 787
    iget-object v0, p0, Lcom/google/maps/g/a/du;->b:Lcom/google/n/ao;

    .line 788
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 790
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 791
    iget-object v2, p0, Lcom/google/maps/g/a/du;->c:Lcom/google/n/ao;

    .line 792
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 794
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    .line 795
    const/4 v2, 0x3

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->e:Z

    .line 796
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 798
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_3

    .line 799
    iget-boolean v2, p0, Lcom/google/maps/g/a/du;->f:Z

    .line 800
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 802
    :cond_3
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_4

    .line 803
    const/4 v2, 0x5

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->g:Z

    .line 804
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 806
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_5

    .line 807
    const/4 v2, 0x6

    iget-object v4, p0, Lcom/google/maps/g/a/du;->h:Lcom/google/n/ao;

    .line 808
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 810
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_6

    .line 811
    const/4 v2, 0x7

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->d:Z

    .line 812
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 814
    :cond_6
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_7

    .line 815
    const/16 v2, 0x8

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->i:Z

    .line 816
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 818
    :cond_7
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_8

    .line 819
    const/16 v2, 0x9

    iget v4, p0, Lcom/google/maps/g/a/du;->l:I

    .line 820
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 822
    :cond_8
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_9

    .line 823
    iget-boolean v2, p0, Lcom/google/maps/g/a/du;->m:Z

    .line 824
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 826
    :cond_9
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v4, 0x1000

    if-ne v2, v4, :cond_a

    .line 827
    const/16 v2, 0xb

    iget-object v4, p0, Lcom/google/maps/g/a/du;->n:Lcom/google/n/ao;

    .line 828
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 830
    :cond_a
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v4, 0x2000

    if-ne v2, v4, :cond_b

    .line 831
    const/16 v2, 0xc

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->o:Z

    .line 832
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 834
    :cond_b
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v4, 0x4000

    if-ne v2, v4, :cond_c

    .line 835
    const/16 v2, 0xd

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->p:Z

    .line 836
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 838
    :cond_c
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const v4, 0x8000

    and-int/2addr v2, v4

    const v4, 0x8000

    if-ne v2, v4, :cond_19

    .line 839
    const/16 v2, 0xe

    iget-boolean v4, p0, Lcom/google/maps/g/a/du;->q:Z

    .line 840
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    :goto_3
    move v4, v1

    move v5, v1

    .line 844
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_f

    .line 845
    iget-object v0, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    .line 846
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v5, v0

    .line 844
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_d
    move v2, v3

    .line 820
    goto/16 :goto_2

    :cond_e
    move v0, v3

    .line 846
    goto :goto_5

    .line 848
    :cond_f
    add-int v0, v2, v5

    .line 849
    iget-object v2, p0, Lcom/google/maps/g/a/du;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 851
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_10

    .line 852
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->s:Z

    .line 853
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 855
    :cond_10
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_11

    .line 856
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/maps/g/a/du;->t:Lcom/google/n/ao;

    .line 857
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 859
    :cond_11
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_12

    .line 860
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/maps/g/a/du;->u:Lcom/google/n/ao;

    .line 861
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 863
    :cond_12
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_13

    .line 864
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/maps/g/a/du;->v:Lcom/google/n/ao;

    .line 865
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 867
    :cond_13
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    .line 868
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->j:Z

    .line 869
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 871
    :cond_14
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_15

    .line 872
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/maps/g/a/du;->k:Lcom/google/n/ao;

    .line 873
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 875
    :cond_15
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_16

    .line 876
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/maps/g/a/du;->w:Lcom/google/n/ao;

    .line 877
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 879
    :cond_16
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_17

    .line 880
    const/16 v2, 0x18

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->x:Z

    .line 881
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 883
    :cond_17
    iget v2, p0, Lcom/google/maps/g/a/du;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_18

    .line 884
    const/16 v2, 0x19

    iget-boolean v3, p0, Lcom/google/maps/g/a/du;->y:Z

    .line 885
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 887
    :cond_18
    iget-object v1, p0, Lcom/google/maps/g/a/du;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 888
    iput v0, p0, Lcom/google/maps/g/a/du;->B:I

    goto/16 :goto_0

    :cond_19
    move v2, v0

    goto/16 :goto_3

    :cond_1a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/du;->newBuilder()Lcom/google/maps/g/a/dw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/dw;->a(Lcom/google/maps/g/a/du;)Lcom/google/maps/g/a/dw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/du;->newBuilder()Lcom/google/maps/g/a/dw;

    move-result-object v0

    return-object v0
.end method
