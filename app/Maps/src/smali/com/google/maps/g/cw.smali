.class public final Lcom/google/maps/g/cw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/cx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/cs;",
        "Lcom/google/maps/g/cw;",
        ">;",
        "Lcom/google/maps/g/cx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2235
    sget-object v0, Lcom/google/maps/g/cs;->l:Lcom/google/maps/g/cs;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2394
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/cw;->b:Ljava/lang/Object;

    .line 2470
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/cw;->c:I

    .line 2506
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cw;->d:Lcom/google/n/ao;

    .line 2565
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cw;->e:Lcom/google/n/ao;

    .line 2625
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    .line 2762
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    .line 2898
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cw;->h:Lcom/google/n/ao;

    .line 2957
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cw;->i:Lcom/google/n/ao;

    .line 3017
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    .line 2236
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/cs;)Lcom/google/maps/g/cw;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/16 v4, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2326
    invoke-static {}, Lcom/google/maps/g/cs;->g()Lcom/google/maps/g/cs;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2385
    :goto_0
    return-object p0

    .line 2327
    :cond_0
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2328
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2329
    iget-object v2, p1, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/cw;->b:Ljava/lang/Object;

    .line 2332
    :cond_1
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 2333
    iget v2, p1, Lcom/google/maps/g/cs;->c:I

    invoke-static {v2}, Lcom/google/maps/g/cu;->a(I)Lcom/google/maps/g/cu;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/cu;->a:Lcom/google/maps/g/cu;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 2327
    goto :goto_1

    :cond_4
    move v2, v1

    .line 2332
    goto :goto_2

    .line 2333
    :cond_5
    iget v3, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/cw;->a:I

    iget v2, v2, Lcom/google/maps/g/cu;->d:I

    iput v2, p0, Lcom/google/maps/g/cw;->c:I

    .line 2335
    :cond_6
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 2336
    iget-object v2, p0, Lcom/google/maps/g/cw;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2337
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2339
    :cond_7
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 2340
    iget-object v2, p0, Lcom/google/maps/g/cw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2341
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2343
    :cond_8
    iget-object v2, p1, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 2344
    iget-object v2, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2345
    iget-object v2, p1, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    .line 2346
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2353
    :cond_9
    :goto_5
    iget-object v2, p1, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 2354
    iget-object v2, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2355
    iget-object v2, p1, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    .line 2356
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2363
    :cond_a
    :goto_6
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_b

    .line 2364
    iget-object v2, p0, Lcom/google/maps/g/cw;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2365
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2367
    :cond_b
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v5, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_c

    .line 2368
    iget-object v2, p0, Lcom/google/maps/g/cw;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2369
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2371
    :cond_c
    iget-object v2, p1, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 2372
    iget-object v2, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2373
    iget-object v2, p1, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    .line 2374
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2381
    :cond_d
    :goto_9
    iget v2, p1, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_19

    :goto_a
    if-eqz v0, :cond_e

    .line 2382
    iget-wide v0, p1, Lcom/google/maps/g/cs;->k:J

    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    iput-wide v0, p0, Lcom/google/maps/g/cw;->k:J

    .line 2384
    :cond_e
    iget-object v0, p1, Lcom/google/maps/g/cs;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 2335
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 2339
    goto/16 :goto_4

    .line 2348
    :cond_11
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v4, :cond_12

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2349
    :cond_12
    iget-object v2, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    .line 2358
    :cond_13
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v5, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2359
    :cond_14
    iget-object v2, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 2363
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 2367
    goto/16 :goto_8

    .line 2376
    :cond_17
    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/cw;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/cw;->a:I

    .line 2377
    :cond_18
    iget-object v2, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    :cond_19
    move v0, v1

    .line 2381
    goto/16 :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2227
    new-instance v2, Lcom/google/maps/g/cs;

    invoke-direct {v2, p0}, Lcom/google/maps/g/cs;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/cw;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/maps/g/cw;->c:I

    iput v4, v2, Lcom/google/maps/g/cs;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/cw;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/cw;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/cw;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/cw;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/maps/g/cw;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/cw;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/g/cw;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/cw;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/cw;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/cw;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/cw;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/cw;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/cw;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/maps/g/cw;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/cw;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget-wide v4, p0, Lcom/google/maps/g/cw;->k:J

    iput-wide v4, v2, Lcom/google/maps/g/cs;->k:J

    iput v0, v2, Lcom/google/maps/g/cs;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2227
    check-cast p1, Lcom/google/maps/g/cs;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/cw;->a(Lcom/google/maps/g/cs;)Lcom/google/maps/g/cw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2389
    const/4 v0, 0x1

    return v0
.end method
