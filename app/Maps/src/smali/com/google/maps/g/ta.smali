.class public final enum Lcom/google/maps/g/ta;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ta;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/ta;

.field public static final enum b:Lcom/google/maps/g/ta;

.field public static final enum c:Lcom/google/maps/g/ta;

.field private static final synthetic e:[Lcom/google/maps/g/ta;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300
    new-instance v0, Lcom/google/maps/g/ta;

    const-string v1, "NO_CONFIDENCE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/ta;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    .line 304
    new-instance v0, Lcom/google/maps/g/ta;

    const-string v1, "LOW_CONFIDENCE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/ta;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ta;->b:Lcom/google/maps/g/ta;

    .line 308
    new-instance v0, Lcom/google/maps/g/ta;

    const-string v1, "HIGH_CONFIDENCE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/ta;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ta;->c:Lcom/google/maps/g/ta;

    .line 295
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/ta;

    sget-object v1, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/ta;->b:Lcom/google/maps/g/ta;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/ta;->c:Lcom/google/maps/g/ta;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/ta;->e:[Lcom/google/maps/g/ta;

    .line 343
    new-instance v0, Lcom/google/maps/g/tb;

    invoke-direct {v0}, Lcom/google/maps/g/tb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 353
    iput p3, p0, Lcom/google/maps/g/ta;->d:I

    .line 354
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ta;
    .locals 1

    .prologue
    .line 330
    packed-switch p0, :pswitch_data_0

    .line 334
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 331
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    goto :goto_0

    .line 332
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/ta;->b:Lcom/google/maps/g/ta;

    goto :goto_0

    .line 333
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/ta;->c:Lcom/google/maps/g/ta;

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ta;
    .locals 1

    .prologue
    .line 295
    const-class v0, Lcom/google/maps/g/ta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ta;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ta;
    .locals 1

    .prologue
    .line 295
    sget-object v0, Lcom/google/maps/g/ta;->e:[Lcom/google/maps/g/ta;

    invoke-virtual {v0}, [Lcom/google/maps/g/ta;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ta;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/google/maps/g/ta;->d:I

    return v0
.end method
