.class public final Lcom/google/maps/g/st;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/te;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/st;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/maps/g/st;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Z

.field j:Z

.field k:Lcom/google/n/f;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/maps/g/su;

    invoke-direct {v0}, Lcom/google/maps/g/su;-><init>()V

    sput-object v0, Lcom/google/maps/g/st;->PARSER:Lcom/google/n/ax;

    .line 1329
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/st;->o:Lcom/google/n/aw;

    .line 2200
    new-instance v0, Lcom/google/maps/g/st;

    invoke-direct {v0}, Lcom/google/maps/g/st;-><init>()V

    sput-object v0, Lcom/google/maps/g/st;->l:Lcom/google/maps/g/st;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1020
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    .line 1036
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    .line 1052
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    .line 1068
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    .line 1212
    iput-byte v3, p0, Lcom/google/maps/g/st;->m:B

    .line 1276
    iput v3, p0, Lcom/google/maps/g/st;->n:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    .line 25
    iput-boolean v4, p0, Lcom/google/maps/g/st;->i:Z

    .line 26
    iput-boolean v4, p0, Lcom/google/maps/g/st;->j:Z

    .line 27
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/st;->k:Lcom/google/n/f;

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/maps/g/st;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 40
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 49
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/st;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 119
    iget-object v1, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    .line 121
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/st;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 60
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/st;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 114
    :catch_1
    move-exception v0

    .line 115
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 116
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 65
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/st;->a:I

    goto :goto_0

    .line 69
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 70
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/st;->a:I

    goto/16 :goto_0

    .line 74
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 75
    iget v6, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/st;->a:I

    .line 76
    iput-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 80
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 81
    iget v6, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/st;->a:I

    .line 82
    iput-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 86
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/st;->a:I

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/st;->i:Z

    goto/16 :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 91
    :sswitch_8
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_3

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    .line 94
    or-int/lit8 v1, v1, 0x1

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 96
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 101
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/st;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/st;->j:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_2

    .line 106
    :sswitch_a
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/st;->a:I

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->k:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 118
    :cond_5
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_6

    .line 119
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    .line 121
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->au:Lcom/google/n/bn;

    .line 122
    return-void

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1020
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    .line 1036
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    .line 1052
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    .line 1068
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    .line 1212
    iput-byte v1, p0, Lcom/google/maps/g/st;->m:B

    .line 1276
    iput v1, p0, Lcom/google/maps/g/st;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/st;
    .locals 1

    .prologue
    .line 2203
    sget-object v0, Lcom/google/maps/g/st;->l:Lcom/google/maps/g/st;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/sw;
    .locals 1

    .prologue
    .line 1391
    new-instance v0, Lcom/google/maps/g/sw;

    invoke-direct {v0}, Lcom/google/maps/g/sw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/st;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/maps/g/st;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 1242
    invoke-virtual {p0}, Lcom/google/maps/g/st;->c()I

    .line 1243
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1246
    :cond_0
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1247
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1249
    :cond_1
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 1250
    iget-object v0, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1252
    :cond_2
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 1253
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1255
    :cond_3
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1256
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1258
    :cond_4
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1259
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1261
    :cond_5
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 1262
    iget-boolean v0, p0, Lcom/google/maps/g/st;->i:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_6
    move v3, v2

    .line 1264
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 1265
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1264
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1256
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1259
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_9
    move v0, v2

    .line 1262
    goto :goto_2

    .line 1267
    :cond_a
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_b

    .line 1268
    const/16 v0, 0xb

    iget-boolean v3, p0, Lcom/google/maps/g/st;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_d

    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 1270
    :cond_b
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_c

    .line 1271
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/maps/g/st;->k:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1273
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/st;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1274
    return-void

    :cond_d
    move v1, v2

    .line 1268
    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1214
    iget-byte v0, p0, Lcom/google/maps/g/st;->m:B

    .line 1215
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1237
    :cond_0
    :goto_0
    return v2

    .line 1216
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1218
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1219
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sx;->d()Lcom/google/maps/g/sx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sx;

    invoke-virtual {v0}, Lcom/google/maps/g/sx;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1220
    iput-byte v2, p0, Lcom/google/maps/g/st;->m:B

    goto :goto_0

    .line 1218
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1224
    :cond_3
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1225
    iget-object v0, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1226
    iput-byte v2, p0, Lcom/google/maps/g/st;->m:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1224
    goto :goto_2

    .line 1230
    :cond_5
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 1231
    iget-object v0, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/cf;->d()Lcom/google/maps/b/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/cf;

    invoke-virtual {v0}, Lcom/google/maps/b/cf;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1232
    iput-byte v2, p0, Lcom/google/maps/g/st;->m:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1230
    goto :goto_3

    .line 1236
    :cond_7
    iput-byte v3, p0, Lcom/google/maps/g/st;->m:B

    move v2, v3

    .line 1237
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 1278
    iget v0, p0, Lcom/google/maps/g/st;->n:I

    .line 1279
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1324
    :goto_0
    return v0

    .line 1282
    :cond_0
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_c

    .line 1283
    iget-object v0, p0, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    .line 1284
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1286
    :goto_1
    iget v2, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_1

    .line 1287
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    .line 1288
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1290
    :cond_1
    iget v2, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_2

    .line 1291
    iget-object v2, p0, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    .line 1292
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1294
    :cond_2
    iget v2, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_b

    .line 1295
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    .line 1296
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1298
    :goto_2
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 1299
    const/4 v3, 0x6

    .line 1300
    iget-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1302
    :cond_3
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 1303
    const/4 v3, 0x7

    .line 1304
    iget-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1306
    :cond_4
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 1307
    iget-boolean v0, p0, Lcom/google/maps/g/st;->i:Z

    .line 1308
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    :cond_5
    move v3, v2

    move v2, v1

    .line 1310
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 1311
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/st;->b:Ljava/util/List;

    .line 1312
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1310
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1300
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1304
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 1314
    :cond_8
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 1315
    const/16 v0, 0xb

    iget-boolean v2, p0, Lcom/google/maps/g/st;->j:Z

    .line 1316
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 1318
    :cond_9
    iget v0, p0, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_a

    .line 1319
    const/16 v0, 0xc

    iget-object v2, p0, Lcom/google/maps/g/st;->k:Lcom/google/n/f;

    .line 1320
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1322
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/st;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1323
    iput v0, p0, Lcom/google/maps/g/st;->n:I

    goto/16 :goto_0

    :cond_b
    move v2, v0

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/st;->newBuilder()Lcom/google/maps/g/sw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/sw;->a(Lcom/google/maps/g/st;)Lcom/google/maps/g/sw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/st;->newBuilder()Lcom/google/maps/g/sw;

    move-result-object v0

    return-object v0
.end method
