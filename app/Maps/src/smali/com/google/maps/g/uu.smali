.class public final Lcom/google/maps/g/uu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/uv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/us;",
        "Lcom/google/maps/g/uu;",
        ">;",
        "Lcom/google/maps/g/uv;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:I

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 411
    sget-object v0, Lcom/google/maps/g/us;->e:Lcom/google/maps/g/us;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 535
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uu;->b:Ljava/lang/Object;

    .line 412
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/us;)Lcom/google/maps/g/uu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 449
    invoke-static {}, Lcom/google/maps/g/us;->d()Lcom/google/maps/g/us;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 462
    :goto_0
    return-object p0

    .line 450
    :cond_0
    iget v2, p1, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 451
    iget v2, p1, Lcom/google/maps/g/us;->b:I

    iget v3, p0, Lcom/google/maps/g/uu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/uu;->a:I

    iput v2, p0, Lcom/google/maps/g/uu;->c:I

    .line 453
    :cond_1
    iget v2, p1, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 454
    iget-boolean v2, p1, Lcom/google/maps/g/us;->c:Z

    iget v3, p0, Lcom/google/maps/g/uu;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/uu;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/uu;->d:Z

    .line 456
    :cond_2
    iget v2, p1, Lcom/google/maps/g/us;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 457
    iget v0, p0, Lcom/google/maps/g/uu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/uu;->a:I

    .line 458
    iget-object v0, p1, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/uu;->b:Ljava/lang/Object;

    .line 461
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/us;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 450
    goto :goto_1

    :cond_5
    move v2, v1

    .line 453
    goto :goto_2

    :cond_6
    move v0, v1

    .line 456
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 403
    new-instance v2, Lcom/google/maps/g/us;

    invoke-direct {v2, p0}, Lcom/google/maps/g/us;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/uu;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/maps/g/uu;->c:I

    iput v1, v2, Lcom/google/maps/g/us;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/maps/g/uu;->d:Z

    iput-boolean v1, v2, Lcom/google/maps/g/us;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/uu;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/us;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/us;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 403
    check-cast p1, Lcom/google/maps/g/us;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/uu;->a(Lcom/google/maps/g/us;)Lcom/google/maps/g/uu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x1

    return v0
.end method
