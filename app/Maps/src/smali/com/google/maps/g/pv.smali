.class public final enum Lcom/google/maps/g/pv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/pv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/pv;

.field public static final enum b:Lcom/google/maps/g/pv;

.field public static final enum c:Lcom/google/maps/g/pv;

.field public static final enum d:Lcom/google/maps/g/pv;

.field private static final synthetic f:[Lcom/google/maps/g/pv;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 162
    new-instance v0, Lcom/google/maps/g/pv;

    const-string v1, "RATING_16"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v3, v2}, Lcom/google/maps/g/pv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    .line 166
    new-instance v0, Lcom/google/maps/g/pv;

    const-string v1, "RATING_20"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/pv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pv;->b:Lcom/google/maps/g/pv;

    .line 170
    new-instance v0, Lcom/google/maps/g/pv;

    const-string v1, "RATING_23"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/g/pv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pv;->c:Lcom/google/maps/g/pv;

    .line 174
    new-instance v0, Lcom/google/maps/g/pv;

    const-string v1, "RATING_26"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/g/pv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pv;->d:Lcom/google/maps/g/pv;

    .line 157
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/pv;

    sget-object v1, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/pv;->b:Lcom/google/maps/g/pv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/pv;->c:Lcom/google/maps/g/pv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/pv;->d:Lcom/google/maps/g/pv;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/pv;->f:[Lcom/google/maps/g/pv;

    .line 214
    new-instance v0, Lcom/google/maps/g/pw;

    invoke-direct {v0}, Lcom/google/maps/g/pw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 224
    iput p3, p0, Lcom/google/maps/g/pv;->e:I

    .line 225
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/pv;
    .locals 1

    .prologue
    .line 200
    sparse-switch p0, :sswitch_data_0

    .line 205
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 201
    :sswitch_0
    sget-object v0, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    goto :goto_0

    .line 202
    :sswitch_1
    sget-object v0, Lcom/google/maps/g/pv;->b:Lcom/google/maps/g/pv;

    goto :goto_0

    .line 203
    :sswitch_2
    sget-object v0, Lcom/google/maps/g/pv;->c:Lcom/google/maps/g/pv;

    goto :goto_0

    .line 204
    :sswitch_3
    sget-object v0, Lcom/google/maps/g/pv;->d:Lcom/google/maps/g/pv;

    goto :goto_0

    .line 200
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x14 -> :sswitch_1
        0x17 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/pv;
    .locals 1

    .prologue
    .line 157
    const-class v0, Lcom/google/maps/g/pv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pv;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/pv;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/google/maps/g/pv;->f:[Lcom/google/maps/g/pv;

    invoke-virtual {v0}, [Lcom/google/maps/g/pv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/pv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/google/maps/g/pv;->e:I

    return v0
.end method
