.class public final enum Lcom/google/maps/g/vs;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/vs;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/vs;

.field public static final enum b:Lcom/google/maps/g/vs;

.field private static final synthetic d:[Lcom/google/maps/g/vs;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 242
    new-instance v0, Lcom/google/maps/g/vs;

    const-string v1, "SHORT_DISTANCE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/vs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    .line 246
    new-instance v0, Lcom/google/maps/g/vs;

    const-string v1, "LONG_DISTANCE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/vs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/vs;->b:Lcom/google/maps/g/vs;

    .line 237
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/maps/g/vs;

    sget-object v1, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/vs;->b:Lcom/google/maps/g/vs;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/vs;->d:[Lcom/google/maps/g/vs;

    .line 276
    new-instance v0, Lcom/google/maps/g/vt;

    invoke-direct {v0}, Lcom/google/maps/g/vt;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 286
    iput p3, p0, Lcom/google/maps/g/vs;->c:I

    .line 287
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/vs;
    .locals 1

    .prologue
    .line 264
    packed-switch p0, :pswitch_data_0

    .line 267
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 265
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    goto :goto_0

    .line 266
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/vs;->b:Lcom/google/maps/g/vs;

    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/vs;
    .locals 1

    .prologue
    .line 237
    const-class v0, Lcom/google/maps/g/vs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vs;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/vs;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/google/maps/g/vs;->d:[Lcom/google/maps/g/vs;

    invoke-virtual {v0}, [Lcom/google/maps/g/vs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/vs;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/google/maps/g/vs;->c:I

    return v0
.end method
