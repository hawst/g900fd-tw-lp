.class public final Lcom/google/maps/g/a/ha;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/hb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/gy;",
        "Lcom/google/maps/g/a/ha;",
        ">;",
        "Lcom/google/maps/g/a/hb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Z

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;

.field private l:Lcom/google/n/ao;

.field private m:I

.field private n:Lcom/google/n/ao;

.field private o:Lcom/google/n/ao;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/n/ao;

.field private r:Lcom/google/n/ao;

.field private s:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 956
    sget-object v0, Lcom/google/maps/g/a/gy;->t:Lcom/google/maps/g/a/gy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1194
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->b:Ljava/lang/Object;

    .line 1270
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->c:Ljava/lang/Object;

    .line 1378
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->e:Ljava/lang/Object;

    .line 1455
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    .line 1591
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->g:Ljava/lang/Object;

    .line 1667
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->h:Ljava/lang/Object;

    .line 1775
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->j:Ljava/lang/Object;

    .line 1851
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->k:Ljava/lang/Object;

    .line 1927
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->l:Lcom/google/n/ao;

    .line 2018
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->n:Lcom/google/n/ao;

    .line 2077
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->o:Lcom/google/n/ao;

    .line 2137
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    .line 2273
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->q:Lcom/google/n/ao;

    .line 2332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ha;->r:Lcom/google/n/ao;

    .line 957
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;
    .locals 7

    .prologue
    const/16 v6, 0x4000

    const/16 v5, 0x10

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1096
    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1185
    :goto_0
    return-object p0

    .line 1097
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_13

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1098
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1099
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->b:Ljava/lang/Object;

    .line 1102
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1103
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1104
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->c:Ljava/lang/Object;

    .line 1107
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1108
    iget v2, p1, Lcom/google/maps/g/a/gy;->d:I

    iget v3, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/ha;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ha;->d:I

    .line 1110
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1111
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1112
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->e:Ljava/lang/Object;

    .line 1115
    :cond_4
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1116
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1117
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    .line 1118
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1125
    :cond_5
    :goto_5
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_19

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1126
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1127
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->g:Ljava/lang/Object;

    .line 1130
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1131
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1132
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->h:Ljava/lang/Object;

    .line 1135
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1136
    iget-boolean v2, p1, Lcom/google/maps/g/a/gy;->i:Z

    iget v3, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/ha;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/ha;->i:Z

    .line 1138
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1139
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1140
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->j:Ljava/lang/Object;

    .line 1143
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1144
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1145
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->k:Ljava/lang/Object;

    .line 1148
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1149
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1150
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1152
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1153
    iget v2, p1, Lcom/google/maps/g/a/gy;->m:I

    iget v3, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/ha;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ha;->m:I

    .line 1155
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 1156
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1157
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1159
    :cond_d
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 1160
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1161
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1163
    :cond_e
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 1164
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1165
    iget-object v2, p1, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    .line 1166
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit16 v2, v2, -0x4001

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1173
    :cond_f
    :goto_f
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 1174
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1175
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1177
    :cond_10
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v2, v2, 0x4000

    if-ne v2, v6, :cond_25

    move v2, v0

    :goto_11
    if-eqz v2, :cond_11

    .line 1178
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->r:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1179
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1181
    :cond_11
    iget v2, p1, Lcom/google/maps/g/a/gy;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_26

    :goto_12
    if-eqz v0, :cond_12

    .line 1182
    iget-boolean v0, p1, Lcom/google/maps/g/a/gy;->s:Z

    iget v1, p0, Lcom/google/maps/g/a/ha;->a:I

    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/a/ha;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/ha;->s:Z

    .line 1184
    :cond_12
    iget-object v0, p1, Lcom/google/maps/g/a/gy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 1097
    goto/16 :goto_1

    :cond_14
    move v2, v1

    .line 1102
    goto/16 :goto_2

    :cond_15
    move v2, v1

    .line 1107
    goto/16 :goto_3

    :cond_16
    move v2, v1

    .line 1110
    goto/16 :goto_4

    .line 1120
    :cond_17
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v5, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1121
    :cond_18
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_19
    move v2, v1

    .line 1125
    goto/16 :goto_6

    :cond_1a
    move v2, v1

    .line 1130
    goto/16 :goto_7

    :cond_1b
    move v2, v1

    .line 1135
    goto/16 :goto_8

    :cond_1c
    move v2, v1

    .line 1138
    goto/16 :goto_9

    :cond_1d
    move v2, v1

    .line 1143
    goto/16 :goto_a

    :cond_1e
    move v2, v1

    .line 1148
    goto/16 :goto_b

    :cond_1f
    move v2, v1

    .line 1152
    goto/16 :goto_c

    :cond_20
    move v2, v1

    .line 1155
    goto/16 :goto_d

    :cond_21
    move v2, v1

    .line 1159
    goto/16 :goto_e

    .line 1168
    :cond_22
    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit16 v2, v2, 0x4000

    if-eq v2, v6, :cond_23

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ha;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1169
    :cond_23
    iget-object v2, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_f

    :cond_24
    move v2, v1

    .line 1173
    goto/16 :goto_10

    :cond_25
    move v2, v1

    .line 1177
    goto/16 :goto_11

    :cond_26
    move v0, v1

    .line 1181
    goto/16 :goto_12
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 948
    invoke-virtual {p0}, Lcom/google/maps/g/a/ha;->c()Lcom/google/maps/g/a/gy;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 948
    check-cast p1, Lcom/google/maps/g/a/gy;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ha;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1189
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/gy;
    .locals 10

    .prologue
    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1004
    new-instance v2, Lcom/google/maps/g/a/gy;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/gy;-><init>(Lcom/google/n/v;)V

    .line 1005
    iget v3, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1007
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_11

    .line 1010
    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    .line 1011
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1012
    or-int/lit8 v0, v0, 0x2

    .line 1014
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    .line 1015
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1016
    or-int/lit8 v0, v0, 0x4

    .line 1018
    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/ha;->d:I

    iput v4, v2, Lcom/google/maps/g/a/gy;->d:I

    .line 1019
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1020
    or-int/lit8 v0, v0, 0x8

    .line 1022
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    .line 1023
    iget v4, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 1024
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    .line 1025
    iget v4, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1027
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 1028
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 1029
    or-int/lit8 v0, v0, 0x10

    .line 1031
    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    .line 1032
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 1033
    or-int/lit8 v0, v0, 0x20

    .line 1035
    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    .line 1036
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 1037
    or-int/lit8 v0, v0, 0x40

    .line 1039
    :cond_6
    iget-boolean v4, p0, Lcom/google/maps/g/a/ha;->i:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/gy;->i:Z

    .line 1040
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 1041
    or-int/lit16 v0, v0, 0x80

    .line 1043
    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    .line 1044
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 1045
    or-int/lit16 v0, v0, 0x100

    .line 1047
    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->k:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    .line 1048
    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 1049
    or-int/lit16 v0, v0, 0x200

    .line 1051
    :cond_9
    iget-object v4, v2, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ha;->l:Lcom/google/n/ao;

    .line 1052
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ha;->l:Lcom/google/n/ao;

    .line 1053
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1051
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1054
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 1055
    or-int/lit16 v0, v0, 0x400

    .line 1057
    :cond_a
    iget v4, p0, Lcom/google/maps/g/a/ha;->m:I

    iput v4, v2, Lcom/google/maps/g/a/gy;->m:I

    .line 1058
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 1059
    or-int/lit16 v0, v0, 0x800

    .line 1061
    :cond_b
    iget-object v4, v2, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ha;->n:Lcom/google/n/ao;

    .line 1062
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ha;->n:Lcom/google/n/ao;

    .line 1063
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1061
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1064
    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    .line 1065
    or-int/lit16 v0, v0, 0x1000

    .line 1067
    :cond_c
    iget-object v4, v2, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ha;->o:Lcom/google/n/ao;

    .line 1068
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ha;->o:Lcom/google/n/ao;

    .line 1069
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1067
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1070
    iget v4, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit16 v4, v4, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    .line 1071
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    .line 1072
    iget v4, p0, Lcom/google/maps/g/a/ha;->a:I

    and-int/lit16 v4, v4, -0x4001

    iput v4, p0, Lcom/google/maps/g/a/ha;->a:I

    .line 1074
    :cond_d
    iget-object v4, p0, Lcom/google/maps/g/a/ha;->p:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 1075
    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    .line 1076
    or-int/lit16 v0, v0, 0x2000

    .line 1078
    :cond_e
    iget-object v4, v2, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ha;->q:Lcom/google/n/ao;

    .line 1079
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ha;->q:Lcom/google/n/ao;

    .line 1080
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1078
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1081
    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    .line 1082
    or-int/lit16 v0, v0, 0x4000

    .line 1084
    :cond_f
    iget-object v4, v2, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ha;->r:Lcom/google/n/ao;

    .line 1085
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ha;->r:Lcom/google/n/ao;

    .line 1086
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1084
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1087
    and-int v1, v3, v9

    if-ne v1, v9, :cond_10

    .line 1088
    or-int/2addr v0, v7

    .line 1090
    :cond_10
    iget-boolean v1, p0, Lcom/google/maps/g/a/ha;->s:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/gy;->s:Z

    .line 1091
    iput v0, v2, Lcom/google/maps/g/a/gy;->a:I

    .line 1092
    return-object v2

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method
