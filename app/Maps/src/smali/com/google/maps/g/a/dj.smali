.class public final Lcom/google/maps/g/a/dj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/dh;",
        "Lcom/google/maps/g/a/dj;",
        ">;",
        "Lcom/google/maps/g/a/dk;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 775
    sget-object v0, Lcom/google/maps/g/a/dh;->e:Lcom/google/maps/g/a/dh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 776
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/dh;)Lcom/google/maps/g/a/dj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 813
    invoke-static {}, Lcom/google/maps/g/a/dh;->d()Lcom/google/maps/g/a/dh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 824
    :goto_0
    return-object p0

    .line 814
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 815
    iget v2, p1, Lcom/google/maps/g/a/dh;->b:I

    iget v3, p0, Lcom/google/maps/g/a/dj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/dj;->a:I

    iput v2, p0, Lcom/google/maps/g/a/dj;->b:I

    .line 817
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 818
    iget v2, p1, Lcom/google/maps/g/a/dh;->c:I

    iget v3, p0, Lcom/google/maps/g/a/dj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/dj;->a:I

    iput v2, p0, Lcom/google/maps/g/a/dj;->c:I

    .line 820
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 821
    iget-boolean v0, p1, Lcom/google/maps/g/a/dh;->d:Z

    iget v1, p0, Lcom/google/maps/g/a/dj;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/dj;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/dj;->d:Z

    .line 823
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/a/dh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 814
    goto :goto_1

    :cond_5
    move v2, v1

    .line 817
    goto :goto_2

    :cond_6
    move v0, v1

    .line 820
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 767
    new-instance v2, Lcom/google/maps/g/a/dh;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/dh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/dj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/dj;->b:I

    iput v1, v2, Lcom/google/maps/g/a/dh;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/a/dj;->c:I

    iput v1, v2, Lcom/google/maps/g/a/dh;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/maps/g/a/dj;->d:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/dh;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/dh;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 767
    check-cast p1, Lcom/google/maps/g/a/dh;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/dj;->a(Lcom/google/maps/g/a/dh;)Lcom/google/maps/g/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 828
    const/4 v0, 0x1

    return v0
.end method
