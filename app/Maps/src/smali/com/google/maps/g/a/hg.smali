.class public final Lcom/google/maps/g/a/hg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/hl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hg;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Lcom/google/maps/g/a/hg;

.field private static final serialVersionUID:J

.field private static volatile u:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field d:I

.field public e:Ljava/lang/Object;

.field f:Z

.field public g:Lcom/google/n/ao;

.field h:I

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field public l:Z

.field m:Z

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/lang/Object;

.field p:Ljava/lang/Object;

.field q:I

.field private s:B

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/google/maps/g/a/hh;

    invoke-direct {v0}, Lcom/google/maps/g/a/hh;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hg;->PARSER:Lcom/google/n/ax;

    .line 782
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/hg;->u:Lcom/google/n/aw;

    .line 2071
    new-instance v0, Lcom/google/maps/g/a/hg;

    invoke-direct {v0}, Lcom/google/maps/g/a/hg;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hg;->r:Lcom/google/maps/g/a/hg;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    .line 275
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    .line 363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    .line 437
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    .line 453
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    .line 641
    iput-byte v4, p0, Lcom/google/maps/g/a/hg;->s:B

    .line 705
    iput v4, p0, Lcom/google/maps/g/a/hg;->t:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput v2, p0, Lcom/google/maps/g/a/hg;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    .line 22
    iput-boolean v2, p0, Lcom/google/maps/g/a/hg;->f:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput v2, p0, Lcom/google/maps/g/a/hg;->h:I

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iput-boolean v2, p0, Lcom/google/maps/g/a/hg;->l:Z

    .line 29
    iput-boolean v2, p0, Lcom/google/maps/g/a/hg;->m:Z

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    .line 33
    iput v2, p0, Lcom/google/maps/g/a/hg;->q:I

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x1000

    const/16 v8, 0x80

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/maps/g/a/hg;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 46
    :cond_0
    :goto_0
    if-nez v4, :cond_9

    .line 47
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 53
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 55
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 51
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 61
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x80

    if-ne v2, v8, :cond_1

    .line 167
    iget-object v2, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 169
    :cond_1
    and-int/lit16 v1, v1, 0x1000

    if-ne v1, v9, :cond_2

    .line 170
    iget-object v1, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    .line 172
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hg;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 162
    :catch_1
    move-exception v0

    .line 163
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 164
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/hg;->d:I

    goto :goto_0

    .line 75
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    iget v6, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 77
    iput-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 81
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/hg;->f:Z

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 86
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 87
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    goto/16 :goto_0

    .line 91
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/hg;->h:I

    goto/16 :goto_0

    .line 96
    :sswitch_8
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v8, :cond_4

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 99
    or-int/lit16 v1, v1, 0x80

    .line 101
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 101
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 106
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 107
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    goto/16 :goto_0

    .line 111
    :sswitch_a
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 112
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    goto/16 :goto_0

    .line 116
    :sswitch_b
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 117
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/hg;->l:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 121
    :sswitch_c
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 122
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/a/hg;->m:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_3

    .line 126
    :sswitch_d
    and-int/lit16 v0, v1, 0x1000

    if-eq v0, v9, :cond_7

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    .line 129
    or-int/lit16 v1, v1, 0x1000

    .line 131
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 132
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 131
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 136
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 137
    iget v6, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 138
    iput-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 142
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 143
    iget v6, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 144
    iput-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 148
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 149
    invoke-static {v0}, Lcom/google/maps/g/a/hj;->a(I)Lcom/google/maps/g/a/hj;

    move-result-object v6

    .line 150
    if-nez v6, :cond_8

    .line 151
    const/16 v6, 0x10

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 153
    :cond_8
    iget v6, p0, Lcom/google/maps/g/a/hg;->a:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/maps/g/a/hg;->a:I

    .line 154
    iput v0, p0, Lcom/google/maps/g/a/hg;->q:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 166
    :cond_9
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v8, :cond_a

    .line 167
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 169
    :cond_a
    and-int/lit16 v0, v1, 0x1000

    if-ne v0, v9, :cond_b

    .line 170
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    .line 172
    :cond_b
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->au:Lcom/google/n/bn;

    .line 173
    return-void

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    .line 275
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    .line 363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    .line 437
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    .line 453
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    .line 641
    iput-byte v1, p0, Lcom/google/maps/g/a/hg;->s:B

    .line 705
    iput v1, p0, Lcom/google/maps/g/a/hg;->t:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/a/hg;
    .locals 1

    .prologue
    .line 2074
    sget-object v0, Lcom/google/maps/g/a/hg;->r:Lcom/google/maps/g/a/hg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/hi;
    .locals 1

    .prologue
    .line 844
    new-instance v0, Lcom/google/maps/g/a/hi;

    invoke-direct {v0}, Lcom/google/maps/g/a/hi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    sget-object v0, Lcom/google/maps/g/a/hg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 653
    invoke-virtual {p0}, Lcom/google/maps/g/a/hg;->c()I

    .line 654
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 655
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 657
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 658
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 660
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 661
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/maps/g/a/hg;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_7

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 663
    :cond_2
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_3

    .line 664
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 666
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 667
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/a/hg;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 669
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 670
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 672
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 673
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/maps/g/a/hg;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_a

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    :cond_6
    :goto_3
    move v3, v2

    .line 675
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    .line 676
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 675
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 661
    :cond_7
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 664
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 667
    goto :goto_2

    .line 673
    :cond_a
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 678
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_c

    .line 679
    const/16 v0, 0x9

    iget-object v3, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 681
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_d

    .line 682
    const/16 v0, 0xa

    iget-object v3, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 684
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_e

    .line 685
    const/16 v0, 0xb

    iget-boolean v3, p0, Lcom/google/maps/g/a/hg;->l:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_10

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 687
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_f

    .line 688
    const/16 v0, 0xc

    iget-boolean v3, p0, Lcom/google/maps/g/a/hg;->m:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_11

    :goto_6
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    :cond_f
    move v1, v2

    .line 690
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 691
    const/16 v3, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_10
    move v0, v2

    .line 685
    goto :goto_5

    :cond_11
    move v1, v2

    .line 688
    goto :goto_6

    .line 693
    :cond_12
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_13

    .line 694
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 696
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_14

    .line 697
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 699
    :cond_14
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_15

    .line 700
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/maps/g/a/hg;->q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_18

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 702
    :cond_15
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 703
    return-void

    .line 694
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 697
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 700
    :cond_18
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 643
    iget-byte v1, p0, Lcom/google/maps/g/a/hg;->s:B

    .line 644
    if-ne v1, v0, :cond_0

    .line 648
    :goto_0
    return v0

    .line 645
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 647
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/hg;->s:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 707
    iget v0, p0, Lcom/google/maps/g/a/hg;->t:I

    .line 708
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 777
    :goto_0
    return v0

    .line 711
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_16

    .line 712
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    .line 713
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 715
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 716
    iget-object v2, p0, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    .line 717
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 719
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_15

    .line 720
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/g/a/hg;->d:I

    .line 721
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    move v2, v0

    .line 723
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 725
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 727
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 728
    const/4 v0, 0x5

    iget-boolean v4, p0, Lcom/google/maps/g/a/hg;->f:Z

    .line 729
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 731
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 732
    const/4 v0, 0x6

    iget-object v4, p0, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    .line 733
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 735
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 736
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/maps/g/a/hg;->h:I

    .line 737
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    :cond_5
    move v4, v2

    move v2, v1

    .line 739
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 740
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 741
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 739
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_6
    move v2, v3

    .line 721
    goto/16 :goto_2

    .line 725
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_8
    move v0, v3

    .line 737
    goto :goto_5

    .line 743
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_a

    .line 744
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    .line 745
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 747
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_b

    .line 748
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    .line 749
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 751
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_c

    .line 752
    const/16 v0, 0xb

    iget-boolean v2, p0, Lcom/google/maps/g/a/hg;->l:Z

    .line 753
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 755
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 756
    const/16 v0, 0xc

    iget-boolean v2, p0, Lcom/google/maps/g/a/hg;->m:Z

    .line 757
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    :cond_d
    move v2, v1

    .line 759
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 760
    const/16 v5, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    .line 761
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 759
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 763
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_f

    .line 764
    const/16 v2, 0xe

    .line 765
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    :goto_8
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 767
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_10

    .line 768
    const/16 v2, 0xf

    .line 769
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    :goto_9
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 771
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_12

    .line 772
    const/16 v0, 0x10

    iget v2, p0, Lcom/google/maps/g/a/hg;->q:I

    .line 773
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_11

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_11
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 775
    :cond_12
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 776
    iput v0, p0, Lcom/google/maps/g/a/hg;->t:I

    goto/16 :goto_0

    .line 765
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 769
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_15
    move v2, v0

    goto/16 :goto_3

    :cond_16
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    .line 401
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 402
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 403
    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 405
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/hg;->newBuilder()Lcom/google/maps/g/a/hi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/hi;->a(Lcom/google/maps/g/a/hg;)Lcom/google/maps/g/a/hi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/hg;->newBuilder()Lcom/google/maps/g/a/hi;

    move-result-object v0

    return-object v0
.end method
