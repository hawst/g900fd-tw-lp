.class public final Lcom/google/maps/g/sx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/td;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/sx;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/sx;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 277
    new-instance v0, Lcom/google/maps/g/sy;

    invoke-direct {v0}, Lcom/google/maps/g/sy;-><init>()V

    sput-object v0, Lcom/google/maps/g/sx;->PARSER:Lcom/google/n/ax;

    .line 546
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/sx;->i:Lcom/google/n/aw;

    .line 963
    new-instance v0, Lcom/google/maps/g/sx;

    invoke-direct {v0}, Lcom/google/maps/g/sx;-><init>()V

    sput-object v0, Lcom/google/maps/g/sx;->f:Lcom/google/maps/g/sx;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 195
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 360
    iput v1, p0, Lcom/google/maps/g/sx;->b:I

    .line 483
    iput-byte v0, p0, Lcom/google/maps/g/sx;->g:B

    .line 517
    iput v0, p0, Lcom/google/maps/g/sx;->h:I

    .line 196
    iput v1, p0, Lcom/google/maps/g/sx;->d:I

    .line 197
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    .line 198
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x4

    const/4 v4, 0x1

    .line 204
    invoke-direct {p0}, Lcom/google/maps/g/sx;-><init>()V

    .line 205
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 209
    const/4 v0, 0x0

    move v3, v0

    .line 210
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 211
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 212
    sparse-switch v0, :sswitch_data_0

    .line 217
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 219
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 215
    goto :goto_0

    .line 225
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v4, :cond_6

    .line 226
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    invoke-static {}, Lcom/google/maps/g/lc;->newBuilder()Lcom/google/maps/g/le;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/le;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    move-result-object v0

    move-object v1, v0

    .line 228
    :goto_1
    sget-object v0, Lcom/google/maps/g/lc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    .line 229
    if-eqz v1, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/le;->a(Lcom/google/maps/g/lc;)Lcom/google/maps/g/le;

    .line 231
    invoke-virtual {v1}, Lcom/google/maps/g/le;->c()Lcom/google/maps/g/lc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    .line 233
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/sx;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 269
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/sx;->au:Lcom/google/n/bn;

    throw v0

    .line 237
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 238
    invoke-static {v0}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v1

    .line 239
    if-nez v1, :cond_2

    .line 240
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 270
    :catch_1
    move-exception v0

    .line 271
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 272
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 242
    :cond_2
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/sx;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/sx;->a:I

    .line 243
    iput v0, p0, Lcom/google/maps/g/sx;->d:I

    goto :goto_0

    .line 248
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 249
    iget v1, p0, Lcom/google/maps/g/sx;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/sx;->a:I

    .line 250
    iput-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    goto :goto_0

    .line 255
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v6, :cond_5

    .line 256
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    invoke-static {v0}, Lcom/google/maps/g/wj;->a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;

    move-result-object v0

    move-object v1, v0

    .line 258
    :goto_2
    sget-object v0, Lcom/google/maps/g/wj;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    .line 259
    if-eqz v1, :cond_3

    .line 260
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/wl;->a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;

    .line 261
    invoke-virtual {v1}, Lcom/google/maps/g/wl;->c()Lcom/google/maps/g/wj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    .line 263
    :cond_3
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/sx;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 274
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->au:Lcom/google/n/bn;

    .line 275
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_2

    :cond_6
    move-object v1, v2

    goto/16 :goto_1

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 193
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 360
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/sx;->b:I

    .line 483
    iput-byte v1, p0, Lcom/google/maps/g/sx;->g:B

    .line 517
    iput v1, p0, Lcom/google/maps/g/sx;->h:I

    .line 194
    return-void
.end method

.method public static d()Lcom/google/maps/g/sx;
    .locals 1

    .prologue
    .line 966
    sget-object v0, Lcom/google/maps/g/sx;->f:Lcom/google/maps/g/sx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/sz;
    .locals 1

    .prologue
    .line 608
    new-instance v0, Lcom/google/maps/g/sz;

    invoke-direct {v0}, Lcom/google/maps/g/sz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/sx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    sget-object v0, Lcom/google/maps/g/sx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x2

    .line 501
    invoke-virtual {p0}, Lcom/google/maps/g/sx;->c()I

    .line 502
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 505
    :cond_0
    iget v0, p0, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 506
    iget v0, p0, Lcom/google/maps/g/sx;->d:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 508
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 509
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 511
    :cond_2
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v4, :cond_3

    .line 512
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 514
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/sx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 515
    return-void

    .line 506
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 509
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 485
    iget-byte v0, p0, Lcom/google/maps/g/sx;->g:B

    .line 486
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 496
    :goto_0
    return v0

    .line 487
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 489
    :cond_1
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 490
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/lc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 491
    iput-byte v2, p0, Lcom/google/maps/g/sx;->g:B

    move v0, v2

    .line 492
    goto :goto_0

    :cond_2
    move v0, v2

    .line 489
    goto :goto_1

    .line 490
    :cond_3
    invoke-static {}, Lcom/google/maps/g/lc;->d()Lcom/google/maps/g/lc;

    move-result-object v0

    goto :goto_2

    .line 495
    :cond_4
    iput-byte v1, p0, Lcom/google/maps/g/sx;->g:B

    move v0, v1

    .line 496
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x4

    const/4 v1, 0x0

    .line 519
    iget v0, p0, Lcom/google/maps/g/sx;->h:I

    .line 520
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 541
    :goto_0
    return v0

    .line 523
    :cond_0
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v3, :cond_6

    .line 524
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/lc;

    .line 525
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 527
    :goto_1
    iget v2, p0, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_5

    .line 528
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/maps/g/sx;->d:I

    .line 529
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 531
    :goto_3
    iget v0, p0, Lcom/google/maps/g/sx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    .line 532
    const/4 v3, 0x3

    .line 533
    iget-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sx;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 535
    :cond_1
    iget v0, p0, Lcom/google/maps/g/sx;->b:I

    if-ne v0, v5, :cond_2

    .line 536
    iget-object v0, p0, Lcom/google/maps/g/sx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/maps/g/wj;

    .line 537
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 539
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/sx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 540
    iput v0, p0, Lcom/google/maps/g/sx;->h:I

    goto/16 :goto_0

    .line 529
    :cond_3
    const/16 v2, 0xa

    goto :goto_2

    .line 533
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/maps/g/sx;->newBuilder()Lcom/google/maps/g/sz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/sz;->a(Lcom/google/maps/g/sx;)Lcom/google/maps/g/sz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/maps/g/sx;->newBuilder()Lcom/google/maps/g/sz;

    move-result-object v0

    return-object v0
.end method
