.class public final Lcom/google/maps/g/gg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gg;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/gg;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/aq;

.field public c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/maps/g/gh;

    invoke-direct {v0}, Lcom/google/maps/g/gh;-><init>()V

    sput-object v0, Lcom/google/maps/g/gg;->PARSER:Lcom/google/n/ax;

    .line 209
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/gg;->g:Lcom/google/n/aw;

    .line 521
    new-instance v0, Lcom/google/maps/g/gg;

    invoke-direct {v0}, Lcom/google/maps/g/gg;-><init>()V

    sput-object v0, Lcom/google/maps/g/gg;->d:Lcom/google/maps/g/gg;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 161
    iput-byte v0, p0, Lcom/google/maps/g/gg;->e:B

    .line 183
    iput v0, p0, Lcom/google/maps/g/gg;->f:I

    .line 18
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/gg;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 34
    sparse-switch v4, :sswitch_data_0

    .line 39
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 47
    and-int/lit8 v5, v1, 0x1

    if-eq v5, v2, :cond_1

    .line 48
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 49
    or-int/lit8 v1, v1, 0x1

    .line 51
    :cond_1
    iget-object v5, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 69
    iget-object v1, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 71
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/gg;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 56
    iget v5, p0, Lcom/google/maps/g/gg;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/gg;->a:I

    .line 57
    iput-object v4, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 65
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 66
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 69
    iget-object v0, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 71
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gg;->au:Lcom/google/n/bn;

    .line 72
    return-void

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 161
    iput-byte v0, p0, Lcom/google/maps/g/gg;->e:B

    .line 183
    iput v0, p0, Lcom/google/maps/g/gg;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/gg;
    .locals 1

    .prologue
    .line 524
    sget-object v0, Lcom/google/maps/g/gg;->d:Lcom/google/maps/g/gg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/gi;
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/google/maps/g/gi;

    invoke-direct {v0}, Lcom/google/maps/g/gi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/gg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/google/maps/g/gg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 173
    invoke-virtual {p0}, Lcom/google/maps/g/gg;->c()I

    .line 174
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v1, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    iget v0, p0, Lcom/google/maps/g/gg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/gg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 181
    return-void

    .line 178
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 163
    iget-byte v1, p0, Lcom/google/maps/g/gg;->e:B

    .line 164
    if-ne v1, v0, :cond_0

    .line 168
    :goto_0
    return v0

    .line 165
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/gg;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 185
    iget v0, p0, Lcom/google/maps/g/gg;->f:I

    .line 186
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 204
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 191
    :goto_1
    iget-object v3, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 192
    iget-object v3, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 193
    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_1
    add-int/lit8 v0, v2, 0x0

    .line 196
    iget-object v2, p0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 198
    iget v0, p0, Lcom/google/maps/g/gg;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 199
    const/4 v3, 0x2

    .line 200
    iget-object v0, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 202
    :goto_3
    iget-object v1, p0, Lcom/google/maps/g/gg;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    iput v0, p0, Lcom/google/maps/g/gg;->f:I

    goto :goto_0

    .line 200
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gg;->newBuilder()Lcom/google/maps/g/gi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/gi;->a(Lcom/google/maps/g/gg;)Lcom/google/maps/g/gi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/gg;->newBuilder()Lcom/google/maps/g/gi;

    move-result-object v0

    return-object v0
.end method
