.class public final Lcom/google/maps/g/lu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/mf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lu;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/lu;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/maps/g/lv;

    invoke-direct {v0}, Lcom/google/maps/g/lv;-><init>()V

    sput-object v0, Lcom/google/maps/g/lu;->PARSER:Lcom/google/n/ax;

    .line 2631
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/lu;->j:Lcom/google/n/aw;

    .line 3081
    new-instance v0, Lcom/google/maps/g/lu;

    invoke-direct {v0}, Lcom/google/maps/g/lu;-><init>()V

    sput-object v0, Lcom/google/maps/g/lu;->g:Lcom/google/maps/g/lu;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2482
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    .line 2514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    .line 2530
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    .line 2546
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    .line 2561
    iput-byte v3, p0, Lcom/google/maps/g/lu;->h:B

    .line 2598
    iput v3, p0, Lcom/google/maps/g/lu;->i:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/lu;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/lu;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 50
    iget v3, p0, Lcom/google/maps/g/lu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/lu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/lu;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 55
    invoke-static {v3}, Lcom/google/maps/g/lx;->a(I)Lcom/google/maps/g/lx;

    move-result-object v4

    .line 56
    if-nez v4, :cond_1

    .line 57
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/maps/g/lu;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/lu;->a:I

    .line 60
    iput v3, p0, Lcom/google/maps/g/lu;->c:I

    goto :goto_0

    .line 65
    :sswitch_3
    iget-object v3, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 66
    iget v3, p0, Lcom/google/maps/g/lu;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/lu;->a:I

    goto :goto_0

    .line 70
    :sswitch_4
    iget-object v3, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 71
    iget v3, p0, Lcom/google/maps/g/lu;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/lu;->a:I

    goto :goto_0

    .line 75
    :sswitch_5
    iget-object v3, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 76
    iget v3, p0, Lcom/google/maps/g/lu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/lu;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lu;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2482
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    .line 2514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    .line 2530
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    .line 2546
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    .line 2561
    iput-byte v1, p0, Lcom/google/maps/g/lu;->h:B

    .line 2598
    iput v1, p0, Lcom/google/maps/g/lu;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/lu;
    .locals 1

    .prologue
    .line 3084
    sget-object v0, Lcom/google/maps/g/lu;->g:Lcom/google/maps/g/lu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/lw;
    .locals 1

    .prologue
    .line 2693
    new-instance v0, Lcom/google/maps/g/lw;

    invoke-direct {v0}, Lcom/google/maps/g/lw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/lu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 2579
    invoke-virtual {p0}, Lcom/google/maps/g/lu;->c()I

    .line 2580
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_0

    .line 2581
    iget-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2583
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2584
    iget v0, p0, Lcom/google/maps/g/lu;->c:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2586
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 2587
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2589
    :cond_2
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 2590
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2592
    :cond_3
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 2593
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2595
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/lu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2596
    return-void

    .line 2584
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2563
    iget-byte v0, p0, Lcom/google/maps/g/lu;->h:B

    .line 2564
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2574
    :goto_0
    return v0

    .line 2565
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 2567
    :cond_1
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 2568
    iget-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/lz;->d()Lcom/google/maps/g/lz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lz;

    invoke-virtual {v0}, Lcom/google/maps/g/lz;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2569
    iput-byte v2, p0, Lcom/google/maps/g/lu;->h:B

    move v0, v2

    .line 2570
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2567
    goto :goto_1

    .line 2573
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/g/lu;->h:B

    move v0, v1

    .line 2574
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2600
    iget v0, p0, Lcom/google/maps/g/lu;->i:I

    .line 2601
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2626
    :goto_0
    return v0

    .line 2604
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_6

    .line 2605
    iget-object v0, p0, Lcom/google/maps/g/lu;->d:Lcom/google/n/ao;

    .line 2606
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2608
    :goto_1
    iget v2, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2609
    iget v2, p0, Lcom/google/maps/g/lu;->c:I

    .line 2610
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2612
    :cond_1
    iget v2, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 2613
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/lu;->e:Lcom/google/n/ao;

    .line 2614
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2616
    :cond_2
    iget v2, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 2617
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/lu;->f:Lcom/google/n/ao;

    .line 2618
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2620
    :cond_3
    iget v2, p0, Lcom/google/maps/g/lu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_4

    .line 2621
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/maps/g/lu;->b:Lcom/google/n/ao;

    .line 2622
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2624
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/lu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2625
    iput v0, p0, Lcom/google/maps/g/lu;->i:I

    goto/16 :goto_0

    .line 2610
    :cond_5
    const/16 v2, 0xa

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/lu;->newBuilder()Lcom/google/maps/g/lw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/lw;->a(Lcom/google/maps/g/lu;)Lcom/google/maps/g/lw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/lu;->newBuilder()Lcom/google/maps/g/lw;

    move-result-object v0

    return-object v0
.end method
