.class public final Lcom/google/maps/g/a/ew;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/eu;",
        "Lcom/google/maps/g/a/ew;",
        ">;",
        "Lcom/google/maps/g/a/fj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/google/n/ao;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1435
    sget-object v0, Lcom/google/maps/g/a/eu;->r:Lcom/google/maps/g/a/eu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1660
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->b:Ljava/lang/Object;

    .line 1736
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->c:Lcom/google/n/ao;

    .line 1795
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->d:Lcom/google/n/ao;

    .line 1854
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->e:Lcom/google/n/ao;

    .line 1913
    iput v1, p0, Lcom/google/maps/g/a/ew;->f:I

    .line 1949
    iput v1, p0, Lcom/google/maps/g/a/ew;->g:I

    .line 1985
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/g/a/ew;->h:I

    .line 2021
    iput v1, p0, Lcom/google/maps/g/a/ew;->i:I

    .line 2122
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    .line 2259
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    .line 2395
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->n:Lcom/google/n/ao;

    .line 2455
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    .line 1436
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/eu;)Lcom/google/maps/g/a/ew;
    .locals 6

    .prologue
    const/16 v5, 0x800

    const/16 v4, 0x400

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1562
    invoke-static {}, Lcom/google/maps/g/a/eu;->d()Lcom/google/maps/g/a/eu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1639
    :goto_0
    return-object p0

    .line 1563
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1564
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1565
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->b:Ljava/lang/Object;

    .line 1568
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1569
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1570
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1572
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1573
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1574
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1576
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1577
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1578
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1580
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1581
    iget v2, p1, Lcom/google/maps/g/a/eu;->f:I

    invoke-static {v2}, Lcom/google/maps/g/a/ez;->a(I)Lcom/google/maps/g/a/ez;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1563
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1568
    goto :goto_2

    :cond_8
    move v2, v1

    .line 1572
    goto :goto_3

    :cond_9
    move v2, v1

    .line 1576
    goto :goto_4

    :cond_a
    move v2, v1

    .line 1580
    goto :goto_5

    .line 1581
    :cond_b
    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iget v2, v2, Lcom/google/maps/g/a/ez;->F:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->f:I

    .line 1583
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 1584
    iget v2, p1, Lcom/google/maps/g/a/eu;->g:I

    invoke-static {v2}, Lcom/google/maps/g/a/fd;->a(I)Lcom/google/maps/g/a/fd;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    :cond_d
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 1583
    goto :goto_6

    .line 1584
    :cond_f
    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iget v2, v2, Lcom/google/maps/g/a/fd;->j:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->g:I

    .line 1586
    :cond_10
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_14

    .line 1587
    iget v2, p1, Lcom/google/maps/g/a/eu;->h:I

    invoke-static {v2}, Lcom/google/maps/g/a/fb;->a(I)Lcom/google/maps/g/a/fb;

    move-result-object v2

    if-nez v2, :cond_11

    sget-object v2, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    :cond_11
    if-nez v2, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    move v2, v1

    .line 1586
    goto :goto_7

    .line 1587
    :cond_13
    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iget v2, v2, Lcom/google/maps/g/a/fb;->d:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->h:I

    .line 1589
    :cond_14
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_18

    .line 1590
    iget v2, p1, Lcom/google/maps/g/a/eu;->i:I

    invoke-static {v2}, Lcom/google/maps/g/a/ex;->a(I)Lcom/google/maps/g/a/ex;

    move-result-object v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/google/maps/g/a/ex;->a:Lcom/google/maps/g/a/ex;

    :cond_15
    if-nez v2, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    move v2, v1

    .line 1589
    goto :goto_8

    .line 1590
    :cond_17
    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iget v2, v2, Lcom/google/maps/g/a/ex;->j:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->i:I

    .line 1592
    :cond_18
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_9
    if-eqz v2, :cond_19

    .line 1593
    iget v2, p1, Lcom/google/maps/g/a/eu;->j:I

    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->j:I

    .line 1595
    :cond_19
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_a
    if-eqz v2, :cond_1a

    .line 1596
    iget v2, p1, Lcom/google/maps/g/a/eu;->k:I

    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->k:I

    .line 1598
    :cond_1a
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1599
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1600
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    .line 1601
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1608
    :cond_1b
    :goto_b
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1609
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1610
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    .line 1611
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1618
    :cond_1c
    :goto_c
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x400

    if-ne v2, v4, :cond_27

    move v2, v0

    :goto_d
    if-eqz v2, :cond_1d

    .line 1619
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1620
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1622
    :cond_1d
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1e

    .line 1623
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_28

    .line 1624
    iget-object v2, p1, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    .line 1625
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, -0x2001

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1632
    :cond_1e
    :goto_e
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x800

    if-ne v2, v5, :cond_2a

    move v2, v0

    :goto_f
    if-eqz v2, :cond_1f

    .line 1633
    iget v2, p1, Lcom/google/maps/g/a/eu;->p:I

    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/maps/g/a/ew;->a:I

    iput v2, p0, Lcom/google/maps/g/a/ew;->p:I

    .line 1635
    :cond_1f
    iget v2, p1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_2b

    :goto_10
    if-eqz v0, :cond_20

    .line 1636
    iget-boolean v0, p1, Lcom/google/maps/g/a/eu;->q:Z

    iget v1, p0, Lcom/google/maps/g/a/ew;->a:I

    const v2, 0x8000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/a/ew;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/ew;->q:Z

    .line 1638
    :cond_20
    iget-object v0, p1, Lcom/google/maps/g/a/eu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_21
    move v2, v1

    .line 1592
    goto/16 :goto_9

    :cond_22
    move v2, v1

    .line 1595
    goto/16 :goto_a

    .line 1603
    :cond_23
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, 0x400

    if-eq v2, v4, :cond_24

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1604
    :cond_24
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    .line 1613
    :cond_25
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, 0x800

    if-eq v2, v5, :cond_26

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1614
    :cond_26
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c

    :cond_27
    move v2, v1

    .line 1618
    goto/16 :goto_d

    .line 1627
    :cond_28
    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-eq v2, v3, :cond_29

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ew;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/g/a/ew;->a:I

    .line 1628
    :cond_29
    iget-object v2, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_e

    :cond_2a
    move v2, v1

    .line 1632
    goto/16 :goto_f

    :cond_2b
    move v0, v1

    .line 1635
    goto/16 :goto_10
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 8

    .prologue
    const v7, 0x8000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1427
    new-instance v2, Lcom/google/maps/g/a/eu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/eu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_f

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/ew;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ew;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ew;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/eu;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ew;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ew;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/eu;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ew;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ew;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/a/ew;->f:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/maps/g/a/ew;->g:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/maps/g/a/ew;->h:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/maps/g/a/ew;->i:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v4, p0, Lcom/google/maps/g/a/ew;->j:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->j:I

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v4, p0, Lcom/google/maps/g/a/ew;->k:I

    iput v4, v2, Lcom/google/maps/g/a/eu;->k:I

    iget v4, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/maps/g/a/ew;->a:I

    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/a/ew;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v4, v4, -0x801

    iput v4, p0, Lcom/google/maps/g/a/ew;->a:I

    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/a/ew;->m:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-object v4, v2, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ew;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ew;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    iget-object v1, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit16 v1, v1, -0x2001

    iput v1, p0, Lcom/google/maps/g/a/ew;->a:I

    :cond_c
    iget-object v1, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x800

    :cond_d
    iget v1, p0, Lcom/google/maps/g/a/ew;->p:I

    iput v1, v2, Lcom/google/maps/g/a/eu;->p:I

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/lit16 v0, v0, 0x1000

    :cond_e
    iget-boolean v1, p0, Lcom/google/maps/g/a/ew;->q:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/eu;->q:Z

    iput v0, v2, Lcom/google/maps/g/a/eu;->a:I

    return-object v2

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1427
    check-cast p1, Lcom/google/maps/g/a/eu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ew;->a(Lcom/google/maps/g/a/eu;)Lcom/google/maps/g/a/ew;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1643
    iget v0, p0, Lcom/google/maps/g/a/ew;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1644
    iget-object v0, p0, Lcom/google/maps/g/a/ew;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1655
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1643
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1649
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1650
    iget-object v0, p0, Lcom/google/maps/g/a/ew;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/bu;->d()Lcom/google/maps/g/a/bu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/bu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1655
    goto :goto_1
.end method
