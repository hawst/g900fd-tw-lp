.class public final enum Lcom/google/maps/g/d/d;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/d/d;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/d/d;

.field public static final enum b:Lcom/google/maps/g/d/d;

.field private static final synthetic d:[Lcom/google/maps/g/d/d;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    new-instance v0, Lcom/google/maps/g/d/d;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/d/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/d/d;->a:Lcom/google/maps/g/d/d;

    .line 135
    new-instance v0, Lcom/google/maps/g/d/d;

    const-string v1, "GET_ADDRESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/d/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/d/d;->b:Lcom/google/maps/g/d/d;

    .line 126
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/maps/g/d/d;

    sget-object v1, Lcom/google/maps/g/d/d;->a:Lcom/google/maps/g/d/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/d/d;->b:Lcom/google/maps/g/d/d;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/d/d;->d:[Lcom/google/maps/g/d/d;

    .line 165
    new-instance v0, Lcom/google/maps/g/d/e;

    invoke-direct {v0}, Lcom/google/maps/g/d/e;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 175
    iput p3, p0, Lcom/google/maps/g/d/d;->c:I

    .line 176
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/d/d;
    .locals 1

    .prologue
    .line 153
    packed-switch p0, :pswitch_data_0

    .line 156
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/d/d;->a:Lcom/google/maps/g/d/d;

    goto :goto_0

    .line 155
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/d/d;->b:Lcom/google/maps/g/d/d;

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/d/d;
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/maps/g/d/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/d/d;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/d/d;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/maps/g/d/d;->d:[Lcom/google/maps/g/d/d;

    invoke-virtual {v0}, [Lcom/google/maps/g/d/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/d/d;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/google/maps/g/d/d;->c:I

    return v0
.end method
