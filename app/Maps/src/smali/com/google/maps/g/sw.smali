.class public final Lcom/google/maps/g/sw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/te;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/st;",
        "Lcom/google/maps/g/sw;",
        ">;",
        "Lcom/google/maps/g/te;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Z

.field private j:Z

.field private k:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1409
    sget-object v0, Lcom/google/maps/g/st;->l:Lcom/google/maps/g/st;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1573
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    .line 1709
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/sw;->c:Lcom/google/n/ao;

    .line 1768
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/sw;->d:Lcom/google/n/ao;

    .line 1827
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/sw;->e:Lcom/google/n/ao;

    .line 1886
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/sw;->f:Lcom/google/n/ao;

    .line 1945
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/sw;->g:Ljava/lang/Object;

    .line 2021
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/sw;->h:Ljava/lang/Object;

    .line 2161
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/sw;->k:Lcom/google/n/f;

    .line 1410
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/st;)Lcom/google/maps/g/sw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1498
    invoke-static {}, Lcom/google/maps/g/st;->d()Lcom/google/maps/g/st;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1545
    :goto_0
    return-object p0

    .line 1499
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1500
    iget-object v2, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1501
    iget-object v2, p1, Lcom/google/maps/g/st;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    .line 1502
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1509
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1510
    iget-object v2, p0, Lcom/google/maps/g/sw;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1511
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1513
    :cond_2
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1514
    iget-object v2, p0, Lcom/google/maps/g/sw;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1515
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1517
    :cond_3
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1518
    iget-object v2, p0, Lcom/google/maps/g/sw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1519
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1521
    :cond_4
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1522
    iget-object v2, p0, Lcom/google/maps/g/sw;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1523
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1525
    :cond_5
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1526
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1527
    iget-object v2, p1, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/sw;->g:Ljava/lang/Object;

    .line 1530
    :cond_6
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1531
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1532
    iget-object v2, p1, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/sw;->h:Ljava/lang/Object;

    .line 1535
    :cond_7
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1536
    iget-boolean v2, p1, Lcom/google/maps/g/st;->i:Z

    iget v3, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/sw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/sw;->i:Z

    .line 1538
    :cond_8
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1539
    iget-boolean v2, p1, Lcom/google/maps/g/st;->j:Z

    iget v3, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/sw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/sw;->j:Z

    .line 1541
    :cond_9
    iget v2, p1, Lcom/google/maps/g/st;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_16

    .line 1542
    iget-object v0, p1, Lcom/google/maps/g/st;->k:Lcom/google/n/f;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1504
    :cond_a
    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/sw;->a:I

    .line 1505
    :cond_b
    iget-object v2, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/st;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 1509
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 1513
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 1517
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 1521
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 1525
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 1530
    goto :goto_7

    :cond_12
    move v2, v1

    .line 1535
    goto :goto_8

    :cond_13
    move v2, v1

    .line 1538
    goto :goto_9

    :cond_14
    move v0, v1

    .line 1541
    goto :goto_a

    .line 1542
    :cond_15
    iget v1, p0, Lcom/google/maps/g/sw;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/maps/g/sw;->a:I

    iput-object v0, p0, Lcom/google/maps/g/sw;->k:Lcom/google/n/f;

    .line 1544
    :cond_16
    iget-object v0, p1, Lcom/google/maps/g/st;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1401
    new-instance v2, Lcom/google/maps/g/st;

    invoke-direct {v2, p0}, Lcom/google/maps/g/st;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/sw;->a:I

    iget v4, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/sw;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/st;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/st;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/sw;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/sw;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/st;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/sw;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/sw;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/st;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/sw;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/sw;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/st;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/sw;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/sw;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/sw;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/st;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/sw;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/st;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-boolean v1, p0, Lcom/google/maps/g/sw;->i:Z

    iput-boolean v1, v2, Lcom/google/maps/g/st;->i:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-boolean v1, p0, Lcom/google/maps/g/sw;->j:Z

    iput-boolean v1, v2, Lcom/google/maps/g/st;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/sw;->k:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/st;->k:Lcom/google/n/f;

    iput v0, v2, Lcom/google/maps/g/st;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1401
    check-cast p1, Lcom/google/maps/g/st;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/sw;->a(Lcom/google/maps/g/st;)Lcom/google/maps/g/sw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1549
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1550
    iget-object v0, p0, Lcom/google/maps/g/sw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sx;->d()Lcom/google/maps/g/sx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sx;

    invoke-virtual {v0}, Lcom/google/maps/g/sx;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1567
    :cond_0
    :goto_1
    return v2

    .line 1549
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1555
    :cond_2
    iget v0, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1556
    iget-object v0, p0, Lcom/google/maps/g/sw;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1561
    :cond_3
    iget v0, p0, Lcom/google/maps/g/sw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1562
    iget-object v0, p0, Lcom/google/maps/g/sw;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/cf;->d()Lcom/google/maps/b/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/cf;

    invoke-virtual {v0}, Lcom/google/maps/b/cf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 1567
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1555
    goto :goto_2

    :cond_6
    move v0, v2

    .line 1561
    goto :goto_3
.end method
