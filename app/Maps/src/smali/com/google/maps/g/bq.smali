.class public final Lcom/google/maps/g/bq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/bt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/bq;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/maps/g/br;

    invoke-direct {v0}, Lcom/google/maps/g/br;-><init>()V

    sput-object v0, Lcom/google/maps/g/bq;->PARSER:Lcom/google/n/ax;

    .line 241
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/bq;->i:Lcom/google/n/aw;

    .line 626
    new-instance v0, Lcom/google/maps/g/bq;

    invoke-direct {v0}, Lcom/google/maps/g/bq;-><init>()V

    sput-object v0, Lcom/google/maps/g/bq;->f:Lcom/google/maps/g/bq;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 111
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    .line 184
    iput-byte v2, p0, Lcom/google/maps/g/bq;->g:B

    .line 212
    iput v2, p0, Lcom/google/maps/g/bq;->h:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/bq;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/bq;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/bq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/bq;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/bq;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/bq;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 54
    iget v3, p0, Lcom/google/maps/g/bq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/bq;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    .line 73
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 74
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 59
    iget v3, p0, Lcom/google/maps/g/bq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/bq;->a:I

    goto :goto_0

    .line 63
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 64
    iget v4, p0, Lcom/google/maps/g/bq;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/bq;->a:I

    .line 65
    iput-object v3, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/bq;->au:Lcom/google/n/bn;

    .line 77
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 111
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    .line 184
    iput-byte v1, p0, Lcom/google/maps/g/bq;->g:B

    .line 212
    iput v1, p0, Lcom/google/maps/g/bq;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/bq;
    .locals 1

    .prologue
    .line 629
    sget-object v0, Lcom/google/maps/g/bq;->f:Lcom/google/maps/g/bq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/bs;
    .locals 1

    .prologue
    .line 303
    new-instance v0, Lcom/google/maps/g/bs;

    invoke-direct {v0}, Lcom/google/maps/g/bs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/google/maps/g/bq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 196
    invoke-virtual {p0}, Lcom/google/maps/g/bq;->c()I

    .line 197
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 198
    iget v0, p0, Lcom/google/maps/g/bq;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 200
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 203
    :cond_1
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 204
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 206
    :cond_2
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 207
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/bq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 210
    return-void

    .line 198
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 207
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 186
    iget-byte v1, p0, Lcom/google/maps/g/bq;->g:B

    .line 187
    if-ne v1, v0, :cond_0

    .line 191
    :goto_0
    return v0

    .line 188
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 190
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/bq;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 214
    iget v0, p0, Lcom/google/maps/g/bq;->h:I

    .line 215
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 236
    :goto_0
    return v0

    .line 218
    :cond_0
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 219
    iget v0, p0, Lcom/google/maps/g/bq;->b:I

    .line 220
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 222
    :goto_2
    iget v2, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 223
    iget-object v2, p0, Lcom/google/maps/g/bq;->c:Lcom/google/n/ao;

    .line 224
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 226
    :cond_1
    iget v2, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    .line 227
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/bq;->d:Lcom/google/n/ao;

    .line 228
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 230
    :goto_3
    iget v0, p0, Lcom/google/maps/g/bq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 231
    const/4 v3, 0x5

    .line 232
    iget-object v0, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/bq;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 234
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/bq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 235
    iput v0, p0, Lcom/google/maps/g/bq;->h:I

    goto/16 :goto_0

    .line 220
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 232
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/bs;->a(Lcom/google/maps/g/bq;)Lcom/google/maps/g/bs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    return-object v0
.end method
