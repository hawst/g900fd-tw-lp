.class public final enum Lcom/google/maps/g/e/d;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/e/d;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/e/d;

.field public static final enum b:Lcom/google/maps/g/e/d;

.field public static final enum c:Lcom/google/maps/g/e/d;

.field public static final enum d:Lcom/google/maps/g/e/d;

.field public static final enum e:Lcom/google/maps/g/e/d;

.field public static final enum f:Lcom/google/maps/g/e/d;

.field public static final enum g:Lcom/google/maps/g/e/d;

.field public static final enum h:Lcom/google/maps/g/e/d;

.field private static final synthetic j:[Lcom/google/maps/g/e/d;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 133
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "ORIGINAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->a:Lcom/google/maps/g/e/d;

    .line 137
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "BEST_SNIPPETS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->b:Lcom/google/maps/g/e/d;

    .line 141
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "USER_LANGUAGE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->c:Lcom/google/maps/g/e/d;

    .line 145
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "NEWEST_FIRST"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->d:Lcom/google/maps/g/e/d;

    .line 149
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "QUALITY_SCORE"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->e:Lcom/google/maps/g/e/d;

    .line 153
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "STAR_RATING_HIGH_THEN_QUALITY"

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->f:Lcom/google/maps/g/e/d;

    .line 157
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "STAR_RATING_LOW_THEN_QUALITY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->g:Lcom/google/maps/g/e/d;

    .line 161
    new-instance v0, Lcom/google/maps/g/e/d;

    const-string v1, "QUALITY_SCORE_SOCIAL_BOOSTED"

    const/4 v2, 0x7

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/e/d;->h:Lcom/google/maps/g/e/d;

    .line 128
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/maps/g/e/d;

    sget-object v1, Lcom/google/maps/g/e/d;->a:Lcom/google/maps/g/e/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/e/d;->b:Lcom/google/maps/g/e/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/e/d;->c:Lcom/google/maps/g/e/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/e/d;->d:Lcom/google/maps/g/e/d;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/google/maps/g/e/d;->e:Lcom/google/maps/g/e/d;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/e/d;->f:Lcom/google/maps/g/e/d;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/e/d;->g:Lcom/google/maps/g/e/d;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/e/d;->h:Lcom/google/maps/g/e/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/e/d;->j:[Lcom/google/maps/g/e/d;

    .line 221
    new-instance v0, Lcom/google/maps/g/e/e;

    invoke-direct {v0}, Lcom/google/maps/g/e/e;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 231
    iput p3, p0, Lcom/google/maps/g/e/d;->i:I

    .line 232
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/e/d;
    .locals 1

    .prologue
    .line 203
    packed-switch p0, :pswitch_data_0

    .line 212
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 204
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/e/d;->a:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 205
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/e/d;->b:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 206
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/e/d;->c:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 207
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/e/d;->d:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 208
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/e/d;->e:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 209
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/e/d;->f:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 210
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/e/d;->g:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 211
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/e/d;->h:Lcom/google/maps/g/e/d;

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/e/d;
    .locals 1

    .prologue
    .line 128
    const-class v0, Lcom/google/maps/g/e/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/e/d;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/e/d;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/maps/g/e/d;->j:[Lcom/google/maps/g/e/d;

    invoke-virtual {v0}, [Lcom/google/maps/g/e/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/e/d;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/google/maps/g/e/d;->i:I

    return v0
.end method
