.class public final enum Lcom/google/maps/g/a/hm;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/hm;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/hm;

.field public static final enum b:Lcom/google/maps/g/a/hm;

.field public static final enum c:Lcom/google/maps/g/a/hm;

.field public static final enum d:Lcom/google/maps/g/a/hm;

.field public static final enum e:Lcom/google/maps/g/a/hm;

.field public static final enum f:Lcom/google/maps/g/a/hm;

.field public static final enum g:Lcom/google/maps/g/a/hm;

.field private static final synthetic i:[Lcom/google/maps/g/a/hm;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "DRIVE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 18
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "BICYCLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    .line 22
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "WALK"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    .line 26
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    .line 30
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "FLY"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->e:Lcom/google/maps/g/a/hm;

    .line 34
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "MIXED"

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    .line 38
    new-instance v0, Lcom/google/maps/g/a/hm;

    const-string v1, "TAXI"

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/hm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    .line 9
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/hm;->e:Lcom/google/maps/g/a/hm;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/hm;->i:[Lcom/google/maps/g/a/hm;

    .line 93
    new-instance v0, Lcom/google/maps/g/a/hn;

    invoke-direct {v0}, Lcom/google/maps/g/a/hn;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 103
    iput p3, p0, Lcom/google/maps/g/a/hm;->h:I

    .line 104
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/hm;
    .locals 1

    .prologue
    .line 76
    packed-switch p0, :pswitch_data_0

    .line 84
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 77
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 78
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 79
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 80
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 81
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/hm;->e:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 82
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 83
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/hm;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/a/hm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hm;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/hm;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/a/hm;->i:[Lcom/google/maps/g/a/hm;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/hm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/hm;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/maps/g/a/hm;->h:I

    return v0
.end method
