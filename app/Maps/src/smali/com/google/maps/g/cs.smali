.class public final Lcom/google/maps/g/cs;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/cx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/cs;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/maps/g/cs;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:I

.field public d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:J

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/google/maps/g/ct;

    invoke-direct {v0}, Lcom/google/maps/g/ct;-><init>()V

    sput-object v0, Lcom/google/maps/g/cs;->PARSER:Lcom/google/n/ax;

    .line 2155
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/cs;->o:Lcom/google/n/aw;

    .line 3189
    new-instance v0, Lcom/google/maps/g/cs;

    invoke-direct {v0}, Lcom/google/maps/g/cs;-><init>()V

    sput-object v0, Lcom/google/maps/g/cs;->l:Lcom/google/maps/g/cs;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    .line 1865
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    .line 1967
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    .line 1983
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    .line 2056
    iput-byte v3, p0, Lcom/google/maps/g/cs;->m:B

    .line 2102
    iput v3, p0, Lcom/google/maps/g/cs;->n:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/cs;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    .line 27
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/g/cs;->k:J

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/16 v9, 0x100

    const/16 v8, 0x20

    const/16 v7, 0x10

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/maps/g/cs;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_8

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 42
    sparse-switch v4, :sswitch_data_0

    .line 47
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 55
    iget v5, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/cs;->a:I

    .line 56
    iput-object v4, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v7, :cond_1

    .line 134
    iget-object v2, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    .line 136
    :cond_1
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v8, :cond_2

    .line 137
    iget-object v2, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    .line 139
    :cond_2
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v9, :cond_3

    .line 140
    iget-object v1, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    .line 142
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/cs;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 61
    invoke-static {v4}, Lcom/google/maps/g/cu;->a(I)Lcom/google/maps/g/cu;

    move-result-object v5

    .line 62
    if-nez v5, :cond_4

    .line 63
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 129
    :catch_1
    move-exception v0

    .line 130
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 131
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :cond_4
    :try_start_4
    iget v5, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/cs;->a:I

    .line 66
    iput v4, p0, Lcom/google/maps/g/cs;->c:I

    goto :goto_0

    .line 71
    :sswitch_3
    iget-object v4, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 72
    iget v4, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/cs;->a:I

    goto/16 :goto_0

    .line 76
    :sswitch_4
    iget-object v4, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 77
    iget v4, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/cs;->a:I

    goto/16 :goto_0

    .line 81
    :sswitch_5
    and-int/lit8 v4, v1, 0x10

    if-eq v4, v7, :cond_5

    .line 82
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    .line 84
    or-int/lit8 v1, v1, 0x10

    .line 86
    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 86
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 91
    :sswitch_6
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v8, :cond_6

    .line 92
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    .line 94
    or-int/lit8 v1, v1, 0x20

    .line 96
    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 96
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 101
    :sswitch_7
    iget-object v4, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 102
    iget v4, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/g/cs;->a:I

    goto/16 :goto_0

    .line 106
    :sswitch_8
    iget-object v4, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 107
    iget v4, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/g/cs;->a:I

    goto/16 :goto_0

    .line 111
    :sswitch_9
    and-int/lit16 v4, v1, 0x100

    if-eq v4, v9, :cond_7

    .line 112
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    .line 114
    or-int/lit16 v1, v1, 0x100

    .line 116
    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 117
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 116
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 121
    :sswitch_a
    iget v4, p0, Lcom/google/maps/g/cs;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/g/cs;->a:I

    .line 122
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/g/cs;->k:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 133
    :cond_8
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_9

    .line 134
    iget-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    .line 136
    :cond_9
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v8, :cond_a

    .line 137
    iget-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    .line 139
    :cond_a
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v9, :cond_b

    .line 140
    iget-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    .line 142
    :cond_b
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->au:Lcom/google/n/bn;

    .line 143
    return-void

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    .line 1865
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    .line 1967
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    .line 1983
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    .line 2056
    iput-byte v1, p0, Lcom/google/maps/g/cs;->m:B

    .line 2102
    iput v1, p0, Lcom/google/maps/g/cs;->n:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/cs;
    .locals 1

    .prologue
    .line 3192
    sget-object v0, Lcom/google/maps/g/cs;->l:Lcom/google/maps/g/cs;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/cw;
    .locals 1

    .prologue
    .line 2217
    new-instance v0, Lcom/google/maps/g/cw;

    invoke-direct {v0}, Lcom/google/maps/g/cw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/cs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    sget-object v0, Lcom/google/maps/g/cs;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 2068
    invoke-virtual {p0}, Lcom/google/maps/g/cs;->c()I

    .line 2069
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 2070
    iget-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2072
    :cond_0
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2073
    iget v0, p0, Lcom/google/maps/g/cs;->c:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2075
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 2076
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2078
    :cond_2
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 2079
    iget-object v0, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 2081
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2082
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2081
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2070
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 2073
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_6
    move v1, v2

    .line 2084
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 2085
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2087
    :cond_7
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 2088
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2090
    :cond_8
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 2091
    iget-object v0, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_9
    move v1, v2

    .line 2093
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 2094
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2093
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2096
    :cond_a
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 2097
    const/16 v0, 0xa

    iget-wide v4, p0, Lcom/google/maps/g/cs;->k:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 2099
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/cs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2100
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2058
    iget-byte v1, p0, Lcom/google/maps/g/cs;->m:B

    .line 2059
    if-ne v1, v0, :cond_0

    .line 2063
    :goto_0
    return v0

    .line 2060
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2062
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/cs;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2104
    iget v0, p0, Lcom/google/maps/g/cs;->n:I

    .line 2105
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2150
    :goto_0
    return v0

    .line 2108
    :cond_0
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 2110
    iget-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2112
    :goto_2
    iget v2, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2113
    iget v2, p0, Lcom/google/maps/g/cs;->c:I

    .line 2114
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2116
    :cond_1
    iget v2, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2117
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    .line 2118
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2120
    :cond_2
    iget v2, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 2121
    iget-object v2, p0, Lcom/google/maps/g/cs;->e:Lcom/google/n/ao;

    .line 2122
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v0

    .line 2124
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2125
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/maps/g/cs;->f:Ljava/util/List;

    .line 2126
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2124
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2110
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 2114
    :cond_5
    const/16 v2, 0xa

    goto :goto_3

    :cond_6
    move v2, v1

    .line 2128
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 2129
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/cs;->g:Ljava/util/List;

    .line 2130
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2128
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2132
    :cond_7
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_8

    .line 2133
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/maps/g/cs;->h:Lcom/google/n/ao;

    .line 2134
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 2136
    :cond_8
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_9

    .line 2137
    iget-object v0, p0, Lcom/google/maps/g/cs;->i:Lcom/google/n/ao;

    .line 2138
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_9
    move v2, v1

    .line 2140
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 2141
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/maps/g/cs;->j:Ljava/util/List;

    .line 2142
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2140
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 2144
    :cond_a
    iget v0, p0, Lcom/google/maps/g/cs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_b

    .line 2145
    const/16 v0, 0xa

    iget-wide v4, p0, Lcom/google/maps/g/cs;->k:J

    .line 2146
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2148
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/cs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2149
    iput v0, p0, Lcom/google/maps/g/cs;->n:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    .line 1803
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1804
    check-cast v0, Ljava/lang/String;

    .line 1812
    :goto_0
    return-object v0

    .line 1806
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1808
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1809
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1810
    iput-object v1, p0, Lcom/google/maps/g/cs;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1812
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/cs;->newBuilder()Lcom/google/maps/g/cw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/cw;->a(Lcom/google/maps/g/cs;)Lcom/google/maps/g/cw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/cs;->newBuilder()Lcom/google/maps/g/cw;

    move-result-object v0

    return-object v0
.end method
