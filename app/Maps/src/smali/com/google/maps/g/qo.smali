.class public final Lcom/google/maps/g/qo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/so;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/qm;",
        "Lcom/google/maps/g/qo;",
        ">;",
        "Lcom/google/maps/g/so;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/n/ao;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5119
    sget-object v0, Lcom/google/maps/g/qm;->l:Lcom/google/maps/g/qm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5285
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->b:Lcom/google/n/ao;

    .line 5344
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->c:Lcom/google/n/ao;

    .line 5403
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->d:Lcom/google/n/ao;

    .line 5462
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->e:Lcom/google/n/ao;

    .line 5521
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->f:Lcom/google/n/ao;

    .line 5580
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->i:Lcom/google/n/ao;

    .line 5639
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qo;->g:Lcom/google/n/ao;

    .line 5699
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    .line 5836
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    .line 5973
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    .line 5120
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 5838
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 5839
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    .line 5842
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5844
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 5975
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 5976
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    .line 5979
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5981
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qm;)Lcom/google/maps/g/qo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5216
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 5276
    :goto_0
    return-object p0

    .line 5217
    :cond_0
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 5218
    iget-object v2, p0, Lcom/google/maps/g/qo;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5219
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5221
    :cond_1
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 5222
    iget-object v2, p0, Lcom/google/maps/g/qo;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5223
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5225
    :cond_2
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 5226
    iget-object v2, p0, Lcom/google/maps/g/qo;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5227
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5229
    :cond_3
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 5230
    iget-object v2, p0, Lcom/google/maps/g/qo;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5231
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5233
    :cond_4
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 5234
    iget-object v2, p0, Lcom/google/maps/g/qo;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5235
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5237
    :cond_5
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 5238
    iget-object v2, p0, Lcom/google/maps/g/qo;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qm;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5239
    iget v2, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/qo;->a:I

    .line 5241
    :cond_6
    iget v2, p1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    :goto_7
    if-eqz v0, :cond_7

    .line 5242
    iget-object v0, p0, Lcom/google/maps/g/qo;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/qm;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5243
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5245
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/qm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 5246
    iget-object v0, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 5247
    iget-object v0, p1, Lcom/google/maps/g/qm;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    .line 5248
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5255
    :cond_8
    :goto_8
    iget-object v0, p1, Lcom/google/maps/g/qm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 5256
    iget-object v0, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 5257
    iget-object v0, p1, Lcom/google/maps/g/qm;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    .line 5258
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5265
    :cond_9
    :goto_9
    iget-object v0, p1, Lcom/google/maps/g/qm;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 5266
    iget-object v0, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 5267
    iget-object v0, p1, Lcom/google/maps/g/qm;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    .line 5268
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5275
    :cond_a
    :goto_a
    iget-object v0, p1, Lcom/google/maps/g/qm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 5217
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 5221
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 5225
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 5229
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 5233
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 5237
    goto/16 :goto_6

    :cond_11
    move v0, v1

    .line 5241
    goto :goto_7

    .line 5250
    :cond_12
    invoke-virtual {p0}, Lcom/google/maps/g/qo;->c()V

    .line 5251
    iget-object v0, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/qm;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    .line 5260
    :cond_13
    invoke-direct {p0}, Lcom/google/maps/g/qo;->d()V

    .line 5261
    iget-object v0, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/qm;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    .line 5270
    :cond_14
    invoke-direct {p0}, Lcom/google/maps/g/qo;->i()V

    .line 5271
    iget-object v0, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/qm;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5111
    new-instance v2, Lcom/google/maps/g/qm;

    invoke-direct {v2, p0}, Lcom/google/maps/g/qm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/qm;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/qm;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/qm;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/qm;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/qm;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/qm;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qo;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qo;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v3, v2, Lcom/google/maps/g/qm;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/qo;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/qo;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/g/qo;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/qm;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/maps/g/qo;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/qo;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/qm;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/g/qo;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/qo;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/qm;->k:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/qm;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5111
    check-cast p1, Lcom/google/maps/g/qm;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/qo;->a(Lcom/google/maps/g/qm;)Lcom/google/maps/g/qo;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 5280
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 5701
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 5702
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    .line 5705
    iget v0, p0, Lcom/google/maps/g/qo;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/qo;->a:I

    .line 5707
    :cond_0
    return-void
.end method
