.class public final enum Lcom/google/maps/g/a/ez;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/ez;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/maps/g/a/ez;

.field public static final enum B:Lcom/google/maps/g/a/ez;

.field public static final enum C:Lcom/google/maps/g/a/ez;

.field public static final enum D:Lcom/google/maps/g/a/ez;

.field public static final enum E:Lcom/google/maps/g/a/ez;

.field private static final synthetic G:[Lcom/google/maps/g/a/ez;

.field public static final enum a:Lcom/google/maps/g/a/ez;

.field public static final enum b:Lcom/google/maps/g/a/ez;

.field public static final enum c:Lcom/google/maps/g/a/ez;

.field public static final enum d:Lcom/google/maps/g/a/ez;

.field public static final enum e:Lcom/google/maps/g/a/ez;

.field public static final enum f:Lcom/google/maps/g/a/ez;

.field public static final enum g:Lcom/google/maps/g/a/ez;

.field public static final enum h:Lcom/google/maps/g/a/ez;

.field public static final enum i:Lcom/google/maps/g/a/ez;

.field public static final enum j:Lcom/google/maps/g/a/ez;

.field public static final enum k:Lcom/google/maps/g/a/ez;

.field public static final enum l:Lcom/google/maps/g/a/ez;

.field public static final enum m:Lcom/google/maps/g/a/ez;

.field public static final enum n:Lcom/google/maps/g/a/ez;

.field public static final enum o:Lcom/google/maps/g/a/ez;

.field public static final enum p:Lcom/google/maps/g/a/ez;

.field public static final enum q:Lcom/google/maps/g/a/ez;

.field public static final enum r:Lcom/google/maps/g/a/ez;

.field public static final enum s:Lcom/google/maps/g/a/ez;

.field public static final enum t:Lcom/google/maps/g/a/ez;

.field public static final enum u:Lcom/google/maps/g/a/ez;

.field public static final enum v:Lcom/google/maps/g/a/ez;

.field public static final enum w:Lcom/google/maps/g/a/ez;

.field public static final enum x:Lcom/google/maps/g/a/ez;

.field public static final enum y:Lcom/google/maps/g/a/ez;

.field public static final enum z:Lcom/google/maps/g/a/ez;


# instance fields
.field public final F:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 222
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "MANEUVER_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    .line 226
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "DEPART"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    .line 230
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "NAME_CHANGE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    .line 234
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "STRAIGHT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    .line 238
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "LANE_CHANGE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->e:Lcom/google/maps/g/a/ez;

    .line 242
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "TURN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v7}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    .line 246
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "COUNTED_TURN"

    const/4 v2, 0x6

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->g:Lcom/google/maps/g/a/ez;

    .line 250
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "UTURN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v8}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    .line 254
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "ON_RAMP"

    const/16 v2, 0x8

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    .line 258
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "OFF_RAMP"

    const/16 v2, 0x9

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    .line 262
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "FORK"

    const/16 v2, 0xa

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    .line 266
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "MERGE"

    const/16 v2, 0xb

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    .line 270
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "FERRY"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->m:Lcom/google/maps/g/a/ez;

    .line 274
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "FERRY_TRAIN"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->n:Lcom/google/maps/g/a/ez;

    .line 278
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "EASTER_EGG"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->o:Lcom/google/maps/g/a/ez;

    .line 282
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "ROUNDABOUT_ENTER"

    const/16 v2, 0xf

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->p:Lcom/google/maps/g/a/ez;

    .line 286
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "ROUNDABOUT_EXIT"

    const/16 v2, 0x10

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->q:Lcom/google/maps/g/a/ez;

    .line 290
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "ROUNDABOUT_ENTER_AND_EXIT"

    const/16 v2, 0x11

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    .line 294
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_WALKWAY"

    const/16 v2, 0x12

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->s:Lcom/google/maps/g/a/ez;

    .line 298
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_CROSSING"

    const/16 v2, 0x13

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->t:Lcom/google/maps/g/a/ez;

    .line 302
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_OVERPASS_UNDERPASS"

    const/16 v2, 0x14

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->u:Lcom/google/maps/g/a/ez;

    .line 306
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_STATION_PATH"

    const/16 v2, 0x15

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->v:Lcom/google/maps/g/a/ez;

    .line 310
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_ACCESS_PATH"

    const/16 v2, 0x16

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->w:Lcom/google/maps/g/a/ez;

    .line 314
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "SIMPLE_PATH"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->x:Lcom/google/maps/g/a/ez;

    .line 318
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_STAIRWAY"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->y:Lcom/google/maps/g/a/ez;

    .line 322
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_ELEVATOR"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->z:Lcom/google/maps/g/a/ez;

    .line 326
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_ESCALATOR"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->A:Lcom/google/maps/g/a/ez;

    .line 330
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_SLOPEWAY"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->B:Lcom/google/maps/g/a/ez;

    .line 334
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "PEDESTRIAN_LEVEL_CHANGE"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->C:Lcom/google/maps/g/a/ez;

    .line 338
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "DESTINATION"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    .line 342
    new-instance v0, Lcom/google/maps/g/a/ez;

    const-string v1, "ERROR"

    const/16 v2, 0x1e

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ez;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ez;->E:Lcom/google/maps/g/a/ez;

    .line 217
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/google/maps/g/a/ez;

    sget-object v1, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/ez;->e:Lcom/google/maps/g/a/ez;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/ez;->g:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/a/ez;->m:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/a/ez;->n:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/a/ez;->o:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/a/ez;->p:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/g/a/ez;->q:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/g/a/ez;->s:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/g/a/ez;->t:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/g/a/ez;->u:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/g/a/ez;->v:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/g/a/ez;->w:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/g/a/ez;->x:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/g/a/ez;->y:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/g/a/ez;->z:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/g/a/ez;->A:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/g/a/ez;->B:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/g/a/ez;->C:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/g/a/ez;->E:Lcom/google/maps/g/a/ez;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/ez;->G:[Lcom/google/maps/g/a/ez;

    .line 517
    new-instance v0, Lcom/google/maps/g/a/fa;

    invoke-direct {v0}, Lcom/google/maps/g/a/fa;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 527
    iput p3, p0, Lcom/google/maps/g/a/ez;->F:I

    .line 528
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 476
    packed-switch p0, :pswitch_data_0

    .line 508
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 477
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 478
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 479
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 480
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 481
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/ez;->e:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 482
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 483
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/ez;->g:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 484
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 485
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 486
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 487
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 488
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 489
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/a/ez;->m:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 490
    :pswitch_d
    sget-object v0, Lcom/google/maps/g/a/ez;->n:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 491
    :pswitch_e
    sget-object v0, Lcom/google/maps/g/a/ez;->o:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 492
    :pswitch_f
    sget-object v0, Lcom/google/maps/g/a/ez;->p:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 493
    :pswitch_10
    sget-object v0, Lcom/google/maps/g/a/ez;->q:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 494
    :pswitch_11
    sget-object v0, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 495
    :pswitch_12
    sget-object v0, Lcom/google/maps/g/a/ez;->s:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 496
    :pswitch_13
    sget-object v0, Lcom/google/maps/g/a/ez;->t:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 497
    :pswitch_14
    sget-object v0, Lcom/google/maps/g/a/ez;->u:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 498
    :pswitch_15
    sget-object v0, Lcom/google/maps/g/a/ez;->v:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 499
    :pswitch_16
    sget-object v0, Lcom/google/maps/g/a/ez;->w:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 500
    :pswitch_17
    sget-object v0, Lcom/google/maps/g/a/ez;->x:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 501
    :pswitch_18
    sget-object v0, Lcom/google/maps/g/a/ez;->y:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 502
    :pswitch_19
    sget-object v0, Lcom/google/maps/g/a/ez;->z:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 503
    :pswitch_1a
    sget-object v0, Lcom/google/maps/g/a/ez;->A:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 504
    :pswitch_1b
    sget-object v0, Lcom/google/maps/g/a/ez;->B:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 505
    :pswitch_1c
    sget-object v0, Lcom/google/maps/g/a/ez;->C:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 506
    :pswitch_1d
    sget-object v0, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 507
    :pswitch_1e
    sget-object v0, Lcom/google/maps/g/a/ez;->E:Lcom/google/maps/g/a/ez;

    goto :goto_0

    .line 476
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_3
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_6
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_1e
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_4
        :pswitch_1d
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 217
    const-class v0, Lcom/google/maps/g/a/ez;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ez;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/google/maps/g/a/ez;->G:[Lcom/google/maps/g/a/ez;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/ez;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/ez;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/google/maps/g/a/ez;->F:I

    return v0
.end method
