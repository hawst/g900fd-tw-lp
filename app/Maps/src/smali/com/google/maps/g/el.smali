.class public final enum Lcom/google/maps/g/el;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/el;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/el;

.field public static final enum b:Lcom/google/maps/g/el;

.field public static final enum c:Lcom/google/maps/g/el;

.field public static final enum d:Lcom/google/maps/g/el;

.field public static final enum e:Lcom/google/maps/g/el;

.field public static final enum f:Lcom/google/maps/g/el;

.field public static final enum g:Lcom/google/maps/g/el;

.field public static final enum h:Lcom/google/maps/g/el;

.field private static final synthetic j:[Lcom/google/maps/g/el;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 139
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "POINT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->a:Lcom/google/maps/g/el;

    .line 143
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "AREA"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->b:Lcom/google/maps/g/el;

    .line 147
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "NATURAL_FEATURE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->c:Lcom/google/maps/g/el;

    .line 151
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "ROUTE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->d:Lcom/google/maps/g/el;

    .line 155
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "SEARCH_RESULT"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->e:Lcom/google/maps/g/el;

    .line 159
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "PURE_SERVICE_AREA_BUSINESS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->f:Lcom/google/maps/g/el;

    .line 163
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "BUILDING"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->g:Lcom/google/maps/g/el;

    .line 167
    new-instance v0, Lcom/google/maps/g/el;

    const-string v1, "CONTEXTUAL_POINT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/el;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/el;->h:Lcom/google/maps/g/el;

    .line 134
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/maps/g/el;

    sget-object v1, Lcom/google/maps/g/el;->a:Lcom/google/maps/g/el;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/el;->b:Lcom/google/maps/g/el;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/el;->c:Lcom/google/maps/g/el;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/el;->d:Lcom/google/maps/g/el;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/el;->e:Lcom/google/maps/g/el;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/el;->f:Lcom/google/maps/g/el;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/el;->g:Lcom/google/maps/g/el;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/el;->h:Lcom/google/maps/g/el;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/el;->j:[Lcom/google/maps/g/el;

    .line 227
    new-instance v0, Lcom/google/maps/g/em;

    invoke-direct {v0}, Lcom/google/maps/g/em;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 237
    iput p3, p0, Lcom/google/maps/g/el;->i:I

    .line 238
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/el;
    .locals 1

    .prologue
    .line 209
    packed-switch p0, :pswitch_data_0

    .line 218
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 210
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/el;->a:Lcom/google/maps/g/el;

    goto :goto_0

    .line 211
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/el;->b:Lcom/google/maps/g/el;

    goto :goto_0

    .line 212
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/el;->c:Lcom/google/maps/g/el;

    goto :goto_0

    .line 213
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/el;->d:Lcom/google/maps/g/el;

    goto :goto_0

    .line 214
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/el;->e:Lcom/google/maps/g/el;

    goto :goto_0

    .line 215
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/el;->f:Lcom/google/maps/g/el;

    goto :goto_0

    .line 216
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/el;->g:Lcom/google/maps/g/el;

    goto :goto_0

    .line 217
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/el;->h:Lcom/google/maps/g/el;

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/el;
    .locals 1

    .prologue
    .line 134
    const-class v0, Lcom/google/maps/g/el;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/el;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/el;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/google/maps/g/el;->j:[Lcom/google/maps/g/el;

    invoke-virtual {v0}, [Lcom/google/maps/g/el;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/el;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/google/maps/g/el;->i:I

    return v0
.end method
