.class public final Lcom/google/maps/g/a/i;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/n;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/i;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/a/i;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/google/maps/g/a/j;

    invoke-direct {v0}, Lcom/google/maps/g/a/j;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/i;->PARSER:Lcom/google/n/ax;

    .line 1696
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/i;->j:Lcom/google/n/aw;

    .line 2255
    new-instance v0, Lcom/google/maps/g/a/i;

    invoke-direct {v0}, Lcom/google/maps/g/a/i;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/i;->g:Lcom/google/maps/g/a/i;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1532
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    .line 1632
    iput-byte v2, p0, Lcom/google/maps/g/a/i;->h:B

    .line 1663
    iput v2, p0, Lcom/google/maps/g/a/i;->i:I

    .line 18
    iput v3, p0, Lcom/google/maps/g/a/i;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/a/i;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 37
    sparse-switch v4, :sswitch_data_0

    .line 42
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 50
    invoke-static {v4}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v5

    .line 51
    if-nez v5, :cond_2

    .line 52
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 88
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 89
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 95
    iget-object v1, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 97
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/i;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/a/i;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/i;->a:I

    .line 55
    iput v4, p0, Lcom/google/maps/g/a/i;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 90
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 91
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 92
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 61
    iget v5, p0, Lcom/google/maps/g/a/i;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/i;->a:I

    .line 62
    iput-object v4, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    goto :goto_0

    .line 94
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 66
    :sswitch_3
    iget-object v4, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 67
    iget v4, p0, Lcom/google/maps/g/a/i;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/a/i;->a:I

    goto :goto_0

    .line 71
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 72
    iget v5, p0, Lcom/google/maps/g/a/i;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/a/i;->a:I

    .line 73
    iput-object v4, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 77
    :sswitch_5
    and-int/lit8 v4, v0, 0x8

    if-eq v4, v7, :cond_3

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 80
    or-int/lit8 v0, v0, 0x8

    .line 82
    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 82
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 94
    :cond_4
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_5

    .line 95
    iget-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 97
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->au:Lcom/google/n/bn;

    .line 98
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1532
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    .line 1632
    iput-byte v1, p0, Lcom/google/maps/g/a/i;->h:B

    .line 1663
    iput v1, p0, Lcom/google/maps/g/a/i;->i:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;
    .locals 1

    .prologue
    .line 1761
    invoke-static {}, Lcom/google/maps/g/a/i;->newBuilder()Lcom/google/maps/g/a/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/k;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/maps/g/a/i;
    .locals 1

    .prologue
    .line 2258
    sget-object v0, Lcom/google/maps/g/a/i;->g:Lcom/google/maps/g/a/i;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/k;
    .locals 1

    .prologue
    .line 1758
    new-instance v0, Lcom/google/maps/g/a/k;

    invoke-direct {v0}, Lcom/google/maps/g/a/k;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    sget-object v0, Lcom/google/maps/g/a/i;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 1644
    invoke-virtual {p0}, Lcom/google/maps/g/a/i;->c()I

    .line 1645
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1646
    iget v0, p0, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1648
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1649
    iget-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1651
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1652
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1654
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 1655
    iget-object v0, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1657
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1658
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1646
    :cond_4
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 1649
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1655
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 1660
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/a/i;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1661
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1634
    iget-byte v1, p0, Lcom/google/maps/g/a/i;->h:B

    .line 1635
    if-ne v1, v0, :cond_0

    .line 1639
    :goto_0
    return v0

    .line 1636
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1638
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/i;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1665
    iget v0, p0, Lcom/google/maps/g/a/i;->i:I

    .line 1666
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1691
    :goto_0
    return v0

    .line 1669
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 1670
    iget v0, p0, Lcom/google/maps/g/a/i;->b:I

    .line 1671
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1673
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1675
    iget-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1677
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1678
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    .line 1679
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1681
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1683
    iget-object v0, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_3
    move v3, v1

    move v1, v2

    .line 1685
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1686
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 1687
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1685
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1671
    :cond_4
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 1675
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1683
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 1689
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/a/i;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1690
    iput v0, p0, Lcom/google/maps/g/a/i;->i:I

    goto/16 :goto_0

    :cond_8
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    .line 1502
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1503
    check-cast v0, Ljava/lang/String;

    .line 1511
    :goto_0
    return-object v0

    .line 1505
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1507
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1508
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1509
    iput-object v1, p0, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1511
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/i;->newBuilder()Lcom/google/maps/g/a/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/k;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/i;->newBuilder()Lcom/google/maps/g/a/k;

    move-result-object v0

    return-object v0
.end method
