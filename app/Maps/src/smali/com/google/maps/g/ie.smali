.class public final Lcom/google/maps/g/ie;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ih;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ic;",
        "Lcom/google/maps/g/ie;",
        ">;",
        "Lcom/google/maps/g/ih;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 325
    sget-object v0, Lcom/google/maps/g/ic;->d:Lcom/google/maps/g/ic;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 374
    iput v1, p0, Lcom/google/maps/g/ie;->b:I

    .line 410
    iput v1, p0, Lcom/google/maps/g/ie;->c:I

    .line 326
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ic;)Lcom/google/maps/g/ie;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 357
    invoke-static {}, Lcom/google/maps/g/ic;->d()Lcom/google/maps/g/ic;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 365
    :goto_0
    return-object p0

    .line 358
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ic;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 359
    iget v2, p1, Lcom/google/maps/g/ic;->b:I

    invoke-static {v2}, Lcom/google/maps/g/if;->a(I)Lcom/google/maps/g/if;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/if;->a:Lcom/google/maps/g/if;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 358
    goto :goto_1

    .line 359
    :cond_3
    iget v3, p0, Lcom/google/maps/g/ie;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ie;->a:I

    iget v2, v2, Lcom/google/maps/g/if;->e:I

    iput v2, p0, Lcom/google/maps/g/ie;->b:I

    .line 361
    :cond_4
    iget v2, p1, Lcom/google/maps/g/ic;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_8

    .line 362
    iget v0, p1, Lcom/google/maps/g/ic;->c:I

    invoke-static {v0}, Lcom/google/maps/g/if;->a(I)Lcom/google/maps/g/if;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/if;->a:Lcom/google/maps/g/if;

    :cond_5
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v1

    .line 361
    goto :goto_2

    .line 362
    :cond_7
    iget v1, p0, Lcom/google/maps/g/ie;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/ie;->a:I

    iget v0, v0, Lcom/google/maps/g/if;->e:I

    iput v0, p0, Lcom/google/maps/g/ie;->c:I

    .line 364
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/ic;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 317
    new-instance v2, Lcom/google/maps/g/ic;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ic;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ie;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/ie;->b:I

    iput v1, v2, Lcom/google/maps/g/ic;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/ie;->c:I

    iput v1, v2, Lcom/google/maps/g/ic;->c:I

    iput v0, v2, Lcom/google/maps/g/ic;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 317
    check-cast p1, Lcom/google/maps/g/ic;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ie;->a(Lcom/google/maps/g/ic;)Lcom/google/maps/g/ie;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x1

    return v0
.end method
