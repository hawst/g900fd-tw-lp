.class public final enum Lcom/google/maps/g/a/ib;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/ib;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/ib;

.field public static final enum b:Lcom/google/maps/g/a/ib;

.field public static final enum c:Lcom/google/maps/g/a/ib;

.field private static final synthetic e:[Lcom/google/maps/g/a/ib;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 328
    new-instance v0, Lcom/google/maps/g/a/ib;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/ib;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ib;->a:Lcom/google/maps/g/a/ib;

    .line 332
    new-instance v0, Lcom/google/maps/g/a/ib;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/ib;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ib;->b:Lcom/google/maps/g/a/ib;

    .line 336
    new-instance v0, Lcom/google/maps/g/a/ib;

    const-string v1, "LIKELY_MISS_TRANSFER"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/ib;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ib;->c:Lcom/google/maps/g/a/ib;

    .line 323
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/a/ib;

    sget-object v1, Lcom/google/maps/g/a/ib;->a:Lcom/google/maps/g/a/ib;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/ib;->b:Lcom/google/maps/g/a/ib;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/ib;->c:Lcom/google/maps/g/a/ib;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/a/ib;->e:[Lcom/google/maps/g/a/ib;

    .line 371
    new-instance v0, Lcom/google/maps/g/a/ic;

    invoke-direct {v0}, Lcom/google/maps/g/a/ic;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 381
    iput p3, p0, Lcom/google/maps/g/a/ib;->d:I

    .line 382
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/ib;
    .locals 1

    .prologue
    .line 358
    packed-switch p0, :pswitch_data_0

    .line 362
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 359
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/ib;->a:Lcom/google/maps/g/a/ib;

    goto :goto_0

    .line 360
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/ib;->b:Lcom/google/maps/g/a/ib;

    goto :goto_0

    .line 361
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/ib;->c:Lcom/google/maps/g/a/ib;

    goto :goto_0

    .line 358
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/ib;
    .locals 1

    .prologue
    .line 323
    const-class v0, Lcom/google/maps/g/a/ib;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ib;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/ib;
    .locals 1

    .prologue
    .line 323
    sget-object v0, Lcom/google/maps/g/a/ib;->e:[Lcom/google/maps/g/a/ib;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/ib;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/ib;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/google/maps/g/a/ib;->d:I

    return v0
.end method
