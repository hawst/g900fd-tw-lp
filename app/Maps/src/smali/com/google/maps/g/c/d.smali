.class public final enum Lcom/google/maps/g/c/d;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/c/d;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/c/d;

.field public static final enum b:Lcom/google/maps/g/c/d;

.field public static final enum c:Lcom/google/maps/g/c/d;

.field public static final enum d:Lcom/google/maps/g/c/d;

.field public static final enum e:Lcom/google/maps/g/c/d;

.field public static final enum f:Lcom/google/maps/g/c/d;

.field public static final enum g:Lcom/google/maps/g/c/d;

.field public static final enum h:Lcom/google/maps/g/c/d;

.field private static final synthetic j:[Lcom/google/maps/g/c/d;


# instance fields
.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 149
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "UNKNOWN_STATUS"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->a:Lcom/google/maps/g/c/d;

    .line 153
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->b:Lcom/google/maps/g/c/d;

    .line 157
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "VALIDATION_ERROR"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->c:Lcom/google/maps/g/c/d;

    .line 161
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "REQUEST_ERROR"

    invoke-direct {v0, v1, v7, v6}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->d:Lcom/google/maps/g/c/d;

    .line 165
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v8, v7}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->e:Lcom/google/maps/g/c/d;

    .line 169
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "DEADLINE_EXCEEDED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->f:Lcom/google/maps/g/c/d;

    .line 173
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "INVALID_USER_ERROR"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->g:Lcom/google/maps/g/c/d;

    .line 177
    new-instance v0, Lcom/google/maps/g/c/d;

    const-string v1, "XSRF_VALIDATION_ERROR"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/c/d;->h:Lcom/google/maps/g/c/d;

    .line 144
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/maps/g/c/d;

    sget-object v1, Lcom/google/maps/g/c/d;->a:Lcom/google/maps/g/c/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/c/d;->b:Lcom/google/maps/g/c/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/c/d;->c:Lcom/google/maps/g/c/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/c/d;->d:Lcom/google/maps/g/c/d;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/c/d;->e:Lcom/google/maps/g/c/d;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/c/d;->f:Lcom/google/maps/g/c/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/c/d;->g:Lcom/google/maps/g/c/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/c/d;->h:Lcom/google/maps/g/c/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/c/d;->j:[Lcom/google/maps/g/c/d;

    .line 237
    new-instance v0, Lcom/google/maps/g/c/e;

    invoke-direct {v0}, Lcom/google/maps/g/c/e;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 247
    iput p3, p0, Lcom/google/maps/g/c/d;->i:I

    .line 248
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/c/d;
    .locals 1

    .prologue
    .line 219
    packed-switch p0, :pswitch_data_0

    .line 228
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 220
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/c/d;->a:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 221
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/c/d;->b:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 222
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/c/d;->c:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 223
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/c/d;->d:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 224
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/c/d;->e:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 225
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/c/d;->f:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 226
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/c/d;->g:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 227
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/c/d;->h:Lcom/google/maps/g/c/d;

    goto :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/c/d;
    .locals 1

    .prologue
    .line 144
    const-class v0, Lcom/google/maps/g/c/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/c/d;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/c/d;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/maps/g/c/d;->j:[Lcom/google/maps/g/c/d;

    invoke-virtual {v0}, [Lcom/google/maps/g/c/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/c/d;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/google/maps/g/c/d;->i:I

    return v0
.end method
