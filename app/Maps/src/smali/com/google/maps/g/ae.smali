.class public final Lcom/google/maps/g/ae;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/o;",
        "Lcom/google/maps/g/ae;",
        ">;",
        "Lcom/google/maps/g/af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/q;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/maps/g/xs;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2622
    sget-object v0, Lcom/google/maps/g/o;->d:Lcom/google/maps/g/o;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2680
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    .line 2804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    .line 2623
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/o;)Lcom/google/maps/g/ae;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2655
    invoke-static {}, Lcom/google/maps/g/o;->d()Lcom/google/maps/g/o;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2670
    :goto_0
    return-object p0

    .line 2656
    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2657
    iget-object v1, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2658
    iget-object v1, p1, Lcom/google/maps/g/o;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    .line 2659
    iget v1, p0, Lcom/google/maps/g/ae;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/ae;->a:I

    .line 2666
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/maps/g/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 2667
    iget-object v0, p1, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/xs;->d()Lcom/google/maps/g/xs;

    move-result-object v0

    :goto_3
    iget v1, p0, Lcom/google/maps/g/ae;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    invoke-static {}, Lcom/google/maps/g/xs;->d()Lcom/google/maps/g/xs;

    move-result-object v2

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    invoke-static {v1}, Lcom/google/maps/g/xs;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/xu;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/xu;->c()Lcom/google/maps/g/xs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    :goto_4
    iget v0, p0, Lcom/google/maps/g/ae;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ae;->a:I

    .line 2669
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/o;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2661
    :cond_3
    iget v1, p0, Lcom/google/maps/g/ae;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/ae;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/ae;->a:I

    .line 2662
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2666
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2667
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    goto :goto_3

    :cond_7
    iput-object v0, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2614
    new-instance v2, Lcom/google/maps/g/o;

    invoke-direct {v2, p0}, Lcom/google/maps/g/o;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ae;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/ae;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ae;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/ae;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ae;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/o;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/ae;->c:Lcom/google/maps/g/xs;

    iput-object v1, v2, Lcom/google/maps/g/o;->c:Lcom/google/maps/g/xs;

    iput v0, v2, Lcom/google/maps/g/o;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2614
    check-cast p1, Lcom/google/maps/g/o;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ae;->a(Lcom/google/maps/g/o;)Lcom/google/maps/g/ae;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2674
    const/4 v0, 0x1

    return v0
.end method
