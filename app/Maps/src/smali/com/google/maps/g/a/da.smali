.class public final Lcom/google/maps/g/a/da;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final t:Lcom/google/maps/g/a/da;

.field private static volatile w:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field h:Z

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field public k:Lcom/google/n/ao;

.field public l:Lcom/google/n/ao;

.field public m:Lcom/google/n/ao;

.field public n:Ljava/lang/Object;

.field public o:I

.field public p:Lcom/google/n/ao;

.field public q:Lcom/google/n/ao;

.field public r:I

.field public s:Lcom/google/n/ao;

.field private u:B

.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lcom/google/maps/g/a/db;

    invoke-direct {v0}, Lcom/google/maps/g/a/db;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/da;->PARSER:Lcom/google/n/ax;

    .line 1492
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/da;->w:Lcom/google/n/aw;

    .line 2815
    new-instance v0, Lcom/google/maps/g/a/da;

    invoke-direct {v0}, Lcom/google/maps/g/a/da;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/da;->t:Lcom/google/maps/g/a/da;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    .line 1136
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    .line 1152
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    .line 1168
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    .line 1184
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    .line 1200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    .line 1274
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    .line 1290
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    .line 1322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    .line 1337
    iput-byte v4, p0, Lcom/google/maps/g/a/da;->u:B

    .line 1407
    iput v4, p0, Lcom/google/maps/g/a/da;->v:I

    .line 18
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/da;->b:I

    .line 19
    iput v3, p0, Lcom/google/maps/g/a/da;->c:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput-boolean v3, p0, Lcom/google/maps/g/a/da;->h:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iget-object v0, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    .line 31
    const/16 v0, 0x7f

    iput v0, p0, Lcom/google/maps/g/a/da;->o:I

    .line 32
    iget-object v0, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 33
    iget-object v0, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    iput v3, p0, Lcom/google/maps/g/a/da;->r:I

    .line 35
    iget-object v0, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 36
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/maps/g/a/da;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 48
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 49
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 50
    sparse-switch v0, :sswitch_data_0

    .line 55
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 57
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 53
    goto :goto_0

    .line 62
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 63
    invoke-static {v0}, Lcom/google/maps/g/a/dd;->a(I)Lcom/google/maps/g/a/dd;

    move-result-object v5

    .line 64
    if-nez v5, :cond_1

    .line 65
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/da;->au:Lcom/google/n/bn;

    throw v0

    .line 67
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 68
    iput v0, p0, Lcom/google/maps/g/a/da;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 183
    :catch_1
    move-exception v0

    .line 184
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 185
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 74
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 75
    iput-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    goto :goto_0

    .line 79
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 81
    iput-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    goto :goto_0

    .line 85
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    goto :goto_0

    .line 91
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 92
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto :goto_0

    .line 96
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/da;->h:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 101
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 102
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 106
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 107
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 111
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 112
    invoke-static {v0}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v5

    .line 113
    if-nez v5, :cond_3

    .line 114
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 116
    :cond_3
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 117
    iput v0, p0, Lcom/google/maps/g/a/da;->c:I

    goto/16 :goto_0

    .line 122
    :sswitch_a
    iget-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 123
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 127
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 128
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 132
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 133
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 134
    iput-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 138
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 139
    invoke-static {v0}, Lcom/google/maps/g/a/ca;->a(I)Lcom/google/maps/g/a/ca;

    move-result-object v5

    .line 140
    if-nez v5, :cond_4

    .line 141
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 143
    :cond_4
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 144
    iput v0, p0, Lcom/google/maps/g/a/da;->o:I

    goto/16 :goto_0

    .line 149
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 150
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 154
    :sswitch_f
    iget-object v0, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 155
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 159
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 160
    invoke-static {v0}, Lcom/google/maps/g/a/df;->a(I)Lcom/google/maps/g/a/df;

    move-result-object v5

    .line 161
    if-nez v5, :cond_5

    .line 162
    const/16 v5, 0x10

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 164
    :cond_5
    iget v5, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/maps/g/a/da;->a:I

    .line 165
    iput v0, p0, Lcom/google/maps/g/a/da;->r:I

    goto/16 :goto_0

    .line 170
    :sswitch_11
    iget-object v0, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 171
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I

    goto/16 :goto_0

    .line 175
    :sswitch_12
    iget-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 176
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/da;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 187
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->au:Lcom/google/n/bn;

    .line 188
    return-void

    .line 50
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    .line 1136
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    .line 1152
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    .line 1168
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    .line 1184
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    .line 1200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    .line 1274
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    .line 1290
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    .line 1322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    .line 1337
    iput-byte v1, p0, Lcom/google/maps/g/a/da;->u:B

    .line 1407
    iput v1, p0, Lcom/google/maps/g/a/da;->v:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;
    .locals 1

    .prologue
    .line 1557
    invoke-static {}, Lcom/google/maps/g/a/da;->newBuilder()Lcom/google/maps/g/a/dc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v0

    return-object v0
.end method

.method public static i()Lcom/google/maps/g/a/da;
    .locals 1

    .prologue
    .line 2818
    sget-object v0, Lcom/google/maps/g/a/da;->t:Lcom/google/maps/g/a/da;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/dc;
    .locals 1

    .prologue
    .line 1554
    new-instance v0, Lcom/google/maps/g/a/dc;

    invoke-direct {v0}, Lcom/google/maps/g/a/dc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    sget-object v0, Lcom/google/maps/g/a/da;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 1349
    invoke-virtual {p0}, Lcom/google/maps/g/a/da;->c()I

    .line 1350
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1351
    iget v0, p0, Lcom/google/maps/g/a/da;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_12

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1353
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 1354
    iget-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1356
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_2

    .line 1357
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1359
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 1360
    iget-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1362
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 1363
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1365
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 1366
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/a/da;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_16

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1368
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 1369
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1371
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 1372
    iget-object v0, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1374
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 1375
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_17

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1377
    :cond_8
    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 1378
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1380
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    .line 1381
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1383
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_b

    .line 1384
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1386
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_c

    .line 1387
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/g/a/da;->o:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_19

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1389
    :cond_c
    :goto_7
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_d

    .line 1390
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1392
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_e

    .line 1393
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1395
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_f

    .line 1396
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/maps/g/a/da;->r:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_1a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1398
    :cond_f
    :goto_8
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_10

    .line 1399
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1401
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_11

    .line 1402
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1404
    :cond_11
    iget-object v0, p0, Lcom/google/maps/g/a/da;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1405
    return-void

    .line 1351
    :cond_12
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 1354
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1357
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 1360
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_16
    move v0, v2

    .line 1366
    goto/16 :goto_4

    .line 1375
    :cond_17
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 1384
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1387
    :cond_19
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_7

    .line 1396
    :cond_1a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1339
    iget-byte v1, p0, Lcom/google/maps/g/a/da;->u:B

    .line 1340
    if-ne v1, v0, :cond_0

    .line 1344
    :goto_0
    return v0

    .line 1341
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1343
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/da;->u:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 1409
    iget v0, p0, Lcom/google/maps/g/a/da;->v:I

    .line 1410
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1487
    :goto_0
    return v0

    .line 1413
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1a

    .line 1414
    iget v0, p0, Lcom/google/maps/g/a/da;->b:I

    .line 1415
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_13

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 1417
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 1419
    iget-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1421
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 1422
    const/4 v4, 0x3

    .line 1423
    iget-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1425
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 1427
    iget-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1429
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 1430
    const/4 v0, 0x5

    iget-object v4, p0, Lcom/google/maps/g/a/da;->g:Lcom/google/n/ao;

    .line 1431
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1433
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 1434
    const/4 v0, 0x6

    iget-boolean v4, p0, Lcom/google/maps/g/a/da;->h:Z

    .line 1435
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 1437
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_6

    .line 1438
    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/maps/g/a/da;->i:Lcom/google/n/ao;

    .line 1439
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1441
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_7

    .line 1442
    const/16 v0, 0x8

    iget-object v4, p0, Lcom/google/maps/g/a/da;->j:Lcom/google/n/ao;

    .line 1443
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1445
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 1446
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/maps/g/a/da;->c:I

    .line 1447
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1449
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_9

    .line 1450
    iget-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    .line 1451
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1453
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_a

    .line 1454
    const/16 v0, 0xb

    iget-object v4, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    .line 1455
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1457
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_b

    .line 1458
    const/16 v4, 0xc

    .line 1459
    iget-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    :goto_7
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1461
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_c

    .line 1462
    const/16 v0, 0xd

    iget v4, p0, Lcom/google/maps/g/a/da;->o:I

    .line 1463
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_19

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1465
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_d

    .line 1466
    const/16 v0, 0xe

    iget-object v4, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    .line 1467
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1469
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_e

    .line 1470
    const/16 v0, 0xf

    iget-object v4, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    .line 1471
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1473
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_10

    .line 1474
    const/16 v0, 0x10

    iget v4, p0, Lcom/google/maps/g/a/da;->r:I

    .line 1475
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_f
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1477
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_11

    .line 1478
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    .line 1479
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1481
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_12

    .line 1482
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    .line 1483
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1485
    :cond_12
    iget-object v0, p0, Lcom/google/maps/g/a/da;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1486
    iput v0, p0, Lcom/google/maps/g/a/da;->v:I

    goto/16 :goto_0

    :cond_13
    move v0, v1

    .line 1415
    goto/16 :goto_1

    .line 1419
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1423
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 1427
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_17
    move v0, v1

    .line 1447
    goto/16 :goto_6

    .line 1459
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    :cond_19
    move v0, v1

    .line 1463
    goto/16 :goto_8

    :cond_1a
    move v2, v3

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 990
    iget-object v0, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    .line 991
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 992
    check-cast v0, Ljava/lang/String;

    .line 1000
    :goto_0
    return-object v0

    .line 994
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 996
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 997
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 998
    iput-object v1, p0, Lcom/google/maps/g/a/da;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1000
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/da;->newBuilder()Lcom/google/maps/g/a/dc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/da;->newBuilder()Lcom/google/maps/g/a/dc;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    .line 1033
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1034
    check-cast v0, Ljava/lang/String;

    .line 1042
    :goto_0
    return-object v0

    .line 1036
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1038
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1039
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1040
    iput-object v1, p0, Lcom/google/maps/g/a/da;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1042
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    .line 1075
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1076
    check-cast v0, Ljava/lang/String;

    .line 1084
    :goto_0
    return-object v0

    .line 1078
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1080
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1081
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082
    iput-object v1, p0, Lcom/google/maps/g/a/da;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1084
    goto :goto_0
.end method
