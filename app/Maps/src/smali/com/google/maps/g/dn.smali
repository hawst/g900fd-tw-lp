.class public final Lcom/google/maps/g/dn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/du;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dn;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/dn;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/maps/g/dw;

.field c:Lcom/google/maps/g/dq;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lcom/google/maps/g/do;

    invoke-direct {v0}, Lcom/google/maps/g/do;-><init>()V

    sput-object v0, Lcom/google/maps/g/dn;->PARSER:Lcom/google/n/ax;

    .line 817
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/dn;->g:Lcom/google/n/aw;

    .line 1078
    new-instance v0, Lcom/google/maps/g/dn;

    invoke-direct {v0}, Lcom/google/maps/g/dn;-><init>()V

    sput-object v0, Lcom/google/maps/g/dn;->d:Lcom/google/maps/g/dn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 122
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 768
    iput-byte v0, p0, Lcom/google/maps/g/dn;->e:B

    .line 796
    iput v0, p0, Lcom/google/maps/g/dn;->f:I

    .line 123
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 129
    invoke-direct {p0}, Lcom/google/maps/g/dn;-><init>()V

    .line 130
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 134
    const/4 v0, 0x0

    move v3, v0

    .line 135
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 136
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 137
    sparse-switch v0, :sswitch_data_0

    .line 142
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 144
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 140
    goto :goto_0

    .line 150
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 151
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    invoke-static {v0}, Lcom/google/maps/g/dw;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v0

    move-object v1, v0

    .line 153
    :goto_1
    sget-object v0, Lcom/google/maps/g/dw;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dw;

    iput-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    .line 154
    if-eqz v1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/dy;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    .line 156
    invoke-virtual {v1}, Lcom/google/maps/g/dy;->c()Lcom/google/maps/g/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    .line 158
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/dn;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/dn;->au:Lcom/google/n/bn;

    throw v0

    .line 163
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 164
    iget-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    invoke-static {v0}, Lcom/google/maps/g/dq;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v0

    move-object v1, v0

    .line 166
    :goto_2
    sget-object v0, Lcom/google/maps/g/dq;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dq;

    iput-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    .line 167
    if-eqz v1, :cond_2

    .line 168
    iget-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ds;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    .line 169
    invoke-virtual {v1}, Lcom/google/maps/g/ds;->c()Lcom/google/maps/g/dq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    .line 171
    :cond_2
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/dn;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 178
    :catch_1
    move-exception v0

    .line 179
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 180
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 182
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dn;->au:Lcom/google/n/bn;

    .line 183
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1

    .line 137
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 120
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 768
    iput-byte v0, p0, Lcom/google/maps/g/dn;->e:B

    .line 796
    iput v0, p0, Lcom/google/maps/g/dn;->f:I

    .line 121
    return-void
.end method

.method public static d()Lcom/google/maps/g/dn;
    .locals 1

    .prologue
    .line 1081
    sget-object v0, Lcom/google/maps/g/dn;->d:Lcom/google/maps/g/dn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/dp;
    .locals 1

    .prologue
    .line 879
    new-instance v0, Lcom/google/maps/g/dp;

    invoke-direct {v0}, Lcom/google/maps/g/dp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    sget-object v0, Lcom/google/maps/g/dn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 786
    invoke-virtual {p0}, Lcom/google/maps/g/dn;->c()I

    .line 787
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 788
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 790
    :cond_0
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 791
    iget-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/dq;->d()Lcom/google/maps/g/dq;

    move-result-object v0

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/dn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 794
    return-void

    .line 788
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    goto :goto_0

    .line 791
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 770
    iget-byte v0, p0, Lcom/google/maps/g/dn;->e:B

    .line 771
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 781
    :goto_0
    return v0

    .line 772
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 774
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 775
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/dw;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 776
    iput-byte v2, p0, Lcom/google/maps/g/dn;->e:B

    move v0, v2

    .line 777
    goto :goto_0

    :cond_2
    move v0, v2

    .line 774
    goto :goto_1

    .line 775
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    goto :goto_2

    .line 780
    :cond_4
    iput-byte v1, p0, Lcom/google/maps/g/dn;->e:B

    move v0, v1

    .line 781
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 798
    iget v0, p0, Lcom/google/maps/g/dn;->f:I

    .line 799
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 812
    :goto_0
    return v0

    .line 802
    :cond_0
    iget v0, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 804
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 806
    :goto_2
    iget v2, p0, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 808
    iget-object v2, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/maps/g/dq;->d()Lcom/google/maps/g/dq;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 810
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/dn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 811
    iput v0, p0, Lcom/google/maps/g/dn;->f:I

    goto :goto_0

    .line 804
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    goto :goto_1

    .line 808
    :cond_3
    iget-object v2, p0, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/maps/g/dn;->newBuilder()Lcom/google/maps/g/dp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/dp;->a(Lcom/google/maps/g/dn;)Lcom/google/maps/g/dp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/maps/g/dn;->newBuilder()Lcom/google/maps/g/dp;

    move-result-object v0

    return-object v0
.end method
