.class public final enum Lcom/google/maps/g/pz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/pz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/pz;

.field public static final enum b:Lcom/google/maps/g/pz;

.field public static final enum c:Lcom/google/maps/g/pz;

.field public static final enum d:Lcom/google/maps/g/pz;

.field public static final enum e:Lcom/google/maps/g/pz;

.field private static final synthetic g:[Lcom/google/maps/g/pz;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    new-instance v0, Lcom/google/maps/g/pz;

    const-string v1, "STARS_1"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/pz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    .line 242
    new-instance v0, Lcom/google/maps/g/pz;

    const-string v1, "STARS_2"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/pz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pz;->b:Lcom/google/maps/g/pz;

    .line 246
    new-instance v0, Lcom/google/maps/g/pz;

    const-string v1, "STARS_3"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/pz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pz;->c:Lcom/google/maps/g/pz;

    .line 250
    new-instance v0, Lcom/google/maps/g/pz;

    const-string v1, "STARS_4"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/pz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    .line 254
    new-instance v0, Lcom/google/maps/g/pz;

    const-string v1, "STARS_5"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/pz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    .line 233
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/pz;

    sget-object v1, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/pz;->b:Lcom/google/maps/g/pz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/pz;->c:Lcom/google/maps/g/pz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/pz;->g:[Lcom/google/maps/g/pz;

    .line 299
    new-instance v0, Lcom/google/maps/g/qa;

    invoke-direct {v0}, Lcom/google/maps/g/qa;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 308
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 309
    iput p3, p0, Lcom/google/maps/g/pz;->f:I

    .line 310
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/pz;
    .locals 1

    .prologue
    .line 284
    packed-switch p0, :pswitch_data_0

    .line 290
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 285
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    goto :goto_0

    .line 286
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/pz;->b:Lcom/google/maps/g/pz;

    goto :goto_0

    .line 287
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/pz;->c:Lcom/google/maps/g/pz;

    goto :goto_0

    .line 288
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    goto :goto_0

    .line 289
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/pz;
    .locals 1

    .prologue
    .line 233
    const-class v0, Lcom/google/maps/g/pz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pz;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/pz;
    .locals 1

    .prologue
    .line 233
    sget-object v0, Lcom/google/maps/g/pz;->g:[Lcom/google/maps/g/pz;

    invoke-virtual {v0}, [Lcom/google/maps/g/pz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/pz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lcom/google/maps/g/pz;->f:I

    return v0
.end method
