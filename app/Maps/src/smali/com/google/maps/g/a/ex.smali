.class public final enum Lcom/google/maps/g/a/ex;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/ex;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/ex;

.field public static final enum b:Lcom/google/maps/g/a/ex;

.field public static final enum c:Lcom/google/maps/g/a/ex;

.field public static final enum d:Lcom/google/maps/g/a/ex;

.field public static final enum e:Lcom/google/maps/g/a/ex;

.field public static final enum f:Lcom/google/maps/g/a/ex;

.field public static final enum g:Lcom/google/maps/g/a/ex;

.field public static final enum h:Lcom/google/maps/g/a/ex;

.field public static final enum i:Lcom/google/maps/g/a/ex;

.field private static final synthetic k:[Lcom/google/maps/g/a/ex;


# instance fields
.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 729
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "HEADING_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->a:Lcom/google/maps/g/a/ex;

    .line 733
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "NORTH"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->b:Lcom/google/maps/g/a/ex;

    .line 737
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "NORTH_EAST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->c:Lcom/google/maps/g/a/ex;

    .line 741
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "EAST"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->d:Lcom/google/maps/g/a/ex;

    .line 745
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "SOUTH_EAST"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->e:Lcom/google/maps/g/a/ex;

    .line 749
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "SOUTH"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->f:Lcom/google/maps/g/a/ex;

    .line 753
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "SOUTH_WEST"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->g:Lcom/google/maps/g/a/ex;

    .line 757
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "WEST"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->h:Lcom/google/maps/g/a/ex;

    .line 761
    new-instance v0, Lcom/google/maps/g/a/ex;

    const-string v1, "NORTH_WEST"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ex;->i:Lcom/google/maps/g/a/ex;

    .line 724
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/maps/g/a/ex;

    sget-object v1, Lcom/google/maps/g/a/ex;->a:Lcom/google/maps/g/a/ex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/ex;->b:Lcom/google/maps/g/a/ex;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/ex;->c:Lcom/google/maps/g/a/ex;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/ex;->d:Lcom/google/maps/g/a/ex;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/ex;->e:Lcom/google/maps/g/a/ex;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/ex;->f:Lcom/google/maps/g/a/ex;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/ex;->g:Lcom/google/maps/g/a/ex;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/ex;->h:Lcom/google/maps/g/a/ex;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/ex;->i:Lcom/google/maps/g/a/ex;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/ex;->k:[Lcom/google/maps/g/a/ex;

    .line 826
    new-instance v0, Lcom/google/maps/g/a/ey;

    invoke-direct {v0}, Lcom/google/maps/g/a/ey;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 835
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 836
    iput p3, p0, Lcom/google/maps/g/a/ex;->j:I

    .line 837
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/ex;
    .locals 1

    .prologue
    .line 807
    packed-switch p0, :pswitch_data_0

    .line 817
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 808
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/ex;->a:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 809
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/ex;->b:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 810
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/ex;->c:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 811
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/ex;->d:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 812
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/ex;->e:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 813
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/ex;->f:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 814
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/ex;->g:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 815
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/ex;->h:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 816
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/ex;->i:Lcom/google/maps/g/a/ex;

    goto :goto_0

    .line 807
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/ex;
    .locals 1

    .prologue
    .line 724
    const-class v0, Lcom/google/maps/g/a/ex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ex;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/ex;
    .locals 1

    .prologue
    .line 724
    sget-object v0, Lcom/google/maps/g/a/ex;->k:[Lcom/google/maps/g/a/ex;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/ex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/ex;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 803
    iget v0, p0, Lcom/google/maps/g/a/ex;->j:I

    return v0
.end method
