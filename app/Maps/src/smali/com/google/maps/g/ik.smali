.class public final Lcom/google/maps/g/ik;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/in;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ii;",
        "Lcom/google/maps/g/ik;",
        ">;",
        "Lcom/google/maps/g/in;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 844
    sget-object v0, Lcom/google/maps/g/ii;->e:Lcom/google/maps/g/ii;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 913
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ik;->b:I

    .line 950
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    .line 1086
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ik;->d:Lcom/google/n/ao;

    .line 845
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ii;)Lcom/google/maps/g/ik;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 885
    invoke-static {}, Lcom/google/maps/g/ii;->d()Lcom/google/maps/g/ii;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 904
    :goto_0
    return-object p0

    .line 886
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ii;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 887
    iget v2, p1, Lcom/google/maps/g/ii;->b:I

    invoke-static {v2}, Lcom/google/maps/g/il;->a(I)Lcom/google/maps/g/il;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/il;->a:Lcom/google/maps/g/il;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 886
    goto :goto_1

    .line 887
    :cond_3
    iget v3, p0, Lcom/google/maps/g/ik;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ik;->a:I

    iget v2, v2, Lcom/google/maps/g/il;->d:I

    iput v2, p0, Lcom/google/maps/g/ik;->b:I

    .line 889
    :cond_4
    iget-object v2, p1, Lcom/google/maps/g/ii;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 890
    iget-object v2, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 891
    iget-object v2, p1, Lcom/google/maps/g/ii;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    .line 892
    iget v2, p0, Lcom/google/maps/g/ik;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/ik;->a:I

    .line 899
    :cond_5
    :goto_2
    iget v2, p1, Lcom/google/maps/g/ii;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    :goto_3
    if-eqz v0, :cond_6

    .line 900
    iget-object v0, p0, Lcom/google/maps/g/ik;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ii;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 901
    iget v0, p0, Lcom/google/maps/g/ik;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/ik;->a:I

    .line 903
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/ii;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 894
    :cond_7
    iget v2, p0, Lcom/google/maps/g/ik;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ik;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ik;->a:I

    .line 895
    :cond_8
    iget-object v2, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/ii;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v0, v1

    .line 899
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 836
    new-instance v2, Lcom/google/maps/g/ii;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ii;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ik;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v4, p0, Lcom/google/maps/g/ik;->b:I

    iput v4, v2, Lcom/google/maps/g/ii;->b:I

    iget v4, p0, Lcom/google/maps/g/ik;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ik;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/ik;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ik;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/ii;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/ii;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/ik;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/ik;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/ii;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 836
    check-cast p1, Lcom/google/maps/g/ii;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ik;->a(Lcom/google/maps/g/ii;)Lcom/google/maps/g/ik;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 908
    const/4 v0, 0x1

    return v0
.end method
