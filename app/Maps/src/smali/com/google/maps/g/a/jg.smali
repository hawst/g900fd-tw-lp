.class public final Lcom/google/maps/g/a/jg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/jl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/je;",
        "Lcom/google/maps/g/a/jg;",
        ">;",
        "Lcom/google/maps/g/a/jl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:I

.field private k:Lcom/google/n/ao;

.field private l:Ljava/lang/Object;

.field private m:Z

.field private n:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 916
    sget-object v0, Lcom/google/maps/g/a/je;->o:Lcom/google/maps/g/a/je;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1084
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    .line 1160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    .line 1236
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->g:Lcom/google/n/ao;

    .line 1295
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->h:Ljava/lang/Object;

    .line 1371
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->d:Ljava/lang/Object;

    .line 1447
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/jg;->i:I

    .line 1483
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/jg;->j:I

    .line 1519
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->k:Lcom/google/n/ao;

    .line 1578
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->l:Ljava/lang/Object;

    .line 1686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->e:Ljava/lang/Object;

    .line 1794
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->f:Ljava/lang/Object;

    .line 917
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1018
    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1075
    :goto_0
    return-object p0

    .line 1019
    :cond_0
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_10

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1020
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1021
    iget-object v0, p1, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    .line 1024
    :cond_1
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_11

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 1025
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1026
    iget-object v0, p1, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    .line 1029
    :cond_2
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 1030
    iget-object v0, p0, Lcom/google/maps/g/a/jg;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1031
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1033
    :cond_3
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 1034
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1035
    iget-object v0, p1, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->h:Ljava/lang/Object;

    .line 1038
    :cond_4
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_5
    if-eqz v0, :cond_5

    .line 1039
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1040
    iget-object v0, p1, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->d:Ljava/lang/Object;

    .line 1043
    :cond_5
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_15

    move v0, v1

    :goto_6
    if-eqz v0, :cond_7

    .line 1044
    iget v0, p1, Lcom/google/maps/g/a/je;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/jh;->a(I)Lcom/google/maps/g/a/jh;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jh;)Lcom/google/maps/g/a/jg;

    .line 1046
    :cond_7
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_7
    if-eqz v0, :cond_9

    .line 1047
    iget v0, p1, Lcom/google/maps/g/a/je;->h:I

    invoke-static {v0}, Lcom/google/maps/g/a/jj;->a(I)Lcom/google/maps/g/a/jj;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/a/jj;->a:Lcom/google/maps/g/a/jj;

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;

    .line 1049
    :cond_9
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_8
    if-eqz v0, :cond_a

    .line 1050
    iget-object v0, p0, Lcom/google/maps/g/a/jg;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1051
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1053
    :cond_a
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_9
    if-eqz v0, :cond_b

    .line 1054
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1055
    iget-object v0, p1, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->l:Ljava/lang/Object;

    .line 1058
    :cond_b
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_a
    if-eqz v0, :cond_c

    .line 1059
    iget-boolean v0, p1, Lcom/google/maps/g/a/je;->k:Z

    iget v3, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/g/a/jg;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/jg;->m:Z

    .line 1061
    :cond_c
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_b
    if-eqz v0, :cond_d

    .line 1062
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1063
    iget-object v0, p1, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->e:Ljava/lang/Object;

    .line 1066
    :cond_d
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_c
    if-eqz v0, :cond_e

    .line 1067
    iget-boolean v0, p1, Lcom/google/maps/g/a/je;->m:Z

    iget v3, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/jg;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/jg;->n:Z

    .line 1069
    :cond_e
    iget v0, p1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_d
    if-eqz v0, :cond_f

    .line 1070
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1071
    iget-object v0, p1, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/jg;->f:Ljava/lang/Object;

    .line 1074
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/a/je;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v0, v2

    .line 1019
    goto/16 :goto_1

    :cond_11
    move v0, v2

    .line 1024
    goto/16 :goto_2

    :cond_12
    move v0, v2

    .line 1029
    goto/16 :goto_3

    :cond_13
    move v0, v2

    .line 1033
    goto/16 :goto_4

    :cond_14
    move v0, v2

    .line 1038
    goto/16 :goto_5

    :cond_15
    move v0, v2

    .line 1043
    goto/16 :goto_6

    :cond_16
    move v0, v2

    .line 1046
    goto/16 :goto_7

    :cond_17
    move v0, v2

    .line 1049
    goto/16 :goto_8

    :cond_18
    move v0, v2

    .line 1053
    goto/16 :goto_9

    :cond_19
    move v0, v2

    .line 1058
    goto :goto_a

    :cond_1a
    move v0, v2

    .line 1061
    goto :goto_b

    :cond_1b
    move v0, v2

    .line 1066
    goto :goto_c

    :cond_1c
    move v0, v2

    .line 1069
    goto :goto_d
.end method

.method public final a(Lcom/google/maps/g/a/jh;)Lcom/google/maps/g/a/jg;
    .locals 1

    .prologue
    .line 1465
    if-nez p1, :cond_0

    .line 1466
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1468
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1469
    iget v0, p1, Lcom/google/maps/g/a/jh;->f:I

    iput v0, p0, Lcom/google/maps/g/a/jg;->i:I

    .line 1471
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;
    .locals 1

    .prologue
    .line 1501
    if-nez p1, :cond_0

    .line 1502
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1504
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1505
    iget v0, p1, Lcom/google/maps/g/a/jj;->e:I

    iput v0, p0, Lcom/google/maps/g/a/jg;->j:I

    .line 1507
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/by;)Lcom/google/maps/g/a/jg;
    .locals 2

    .prologue
    .line 1534
    if-nez p1, :cond_0

    .line 1535
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1537
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/a/jg;->k:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1539
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1540
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;
    .locals 2

    .prologue
    .line 1251
    if-nez p1, :cond_0

    .line 1252
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1254
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/a/jg;->g:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1256
    iget v0, p0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/jg;->a:I

    .line 1257
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 908
    new-instance v2, Lcom/google/maps/g/a/je;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/je;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/jg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/je;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/je;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/jg;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/jg;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/jg;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/je;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/jg;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/je;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/maps/g/a/jg;->i:I

    iput v4, v2, Lcom/google/maps/g/a/je;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/maps/g/a/jg;->j:I

    iput v4, v2, Lcom/google/maps/g/a/je;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/jg;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/jg;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/a/jg;->l:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/je;->j:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v1, p0, Lcom/google/maps/g/a/jg;->m:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/je;->k:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/a/jg;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-boolean v1, p0, Lcom/google/maps/g/a/jg;->n:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/je;->m:Z

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-object v1, p0, Lcom/google/maps/g/a/jg;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/a/je;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 908
    check-cast p1, Lcom/google/maps/g/a/je;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1079
    const/4 v0, 0x1

    return v0
.end method
