.class public final Lcom/google/maps/g/rb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/rf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rb;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/maps/g/rb;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 660
    new-instance v0, Lcom/google/maps/g/rc;

    invoke-direct {v0}, Lcom/google/maps/g/rc;-><init>()V

    sput-object v0, Lcom/google/maps/g/rb;->PARSER:Lcom/google/n/ax;

    .line 678
    new-instance v0, Lcom/google/maps/g/rd;

    invoke-direct {v0}, Lcom/google/maps/g/rd;-><init>()V

    .line 747
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/rb;->e:Lcom/google/n/aw;

    .line 951
    new-instance v0, Lcom/google/maps/g/rb;

    invoke-direct {v0}, Lcom/google/maps/g/rb;-><init>()V

    sput-object v0, Lcom/google/maps/g/rb;->b:Lcom/google/maps/g/rb;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 586
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 706
    iput-byte v0, p0, Lcom/google/maps/g/rb;->c:B

    .line 725
    iput v0, p0, Lcom/google/maps/g/rb;->d:I

    .line 587
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    .line 588
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 594
    invoke-direct {p0}, Lcom/google/maps/g/rb;-><init>()V

    .line 597
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 600
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 601
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 602
    sparse-switch v1, :sswitch_data_0

    .line 607
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 609
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 605
    goto :goto_0

    .line 614
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 615
    invoke-static {v5}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v1

    .line 616
    if-nez v1, :cond_2

    .line 617
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 648
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 649
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 654
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 655
    iget-object v1, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    .line 657
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rb;->au:Lcom/google/n/bn;

    throw v0

    .line 619
    :cond_2
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_9

    .line 620
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 621
    or-int/lit8 v1, v0, 0x1

    .line 623
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 625
    goto :goto_0

    .line 628
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 629
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 630
    :goto_4
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_3

    const/4 v1, -0x1

    :goto_5
    if-lez v1, :cond_5

    .line 631
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 632
    invoke-static {v6}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v1

    .line 633
    if-nez v1, :cond_4

    .line 634
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    .line 650
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 651
    :goto_6
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 652
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 630
    :cond_3
    :try_start_6
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_5

    .line 636
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_8

    .line 637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 638
    or-int/lit8 v1, v0, 0x1

    .line 640
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 642
    goto :goto_4

    .line 643
    :cond_5
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 654
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto/16 :goto_2

    :cond_6
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 655
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    .line 657
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rb;->au:Lcom/google/n/bn;

    .line 658
    return-void

    .line 650
    :catch_2
    move-exception v0

    goto :goto_6

    .line 648
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v0

    goto/16 :goto_3

    .line 602
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 584
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 706
    iput-byte v0, p0, Lcom/google/maps/g/rb;->c:B

    .line 725
    iput v0, p0, Lcom/google/maps/g/rb;->d:I

    .line 585
    return-void
.end method

.method public static d()Lcom/google/maps/g/rb;
    .locals 1

    .prologue
    .line 954
    sget-object v0, Lcom/google/maps/g/rb;->b:Lcom/google/maps/g/rb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/re;
    .locals 1

    .prologue
    .line 809
    new-instance v0, Lcom/google/maps/g/re;

    invoke-direct {v0}, Lcom/google/maps/g/re;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 672
    sget-object v0, Lcom/google/maps/g/rb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 718
    invoke-virtual {p0}, Lcom/google/maps/g/rb;->c()I

    move v1, v2

    .line 719
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 720
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 719
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 720
    :cond_0
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 722
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/rb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 723
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 708
    iget-byte v1, p0, Lcom/google/maps/g/rb;->c:B

    .line 709
    if-ne v1, v0, :cond_0

    .line 713
    :goto_0
    return v0

    .line 710
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 712
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/rb;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 727
    iget v1, p0, Lcom/google/maps/g/rb;->d:I

    .line 728
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 742
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 733
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 734
    iget-object v0, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    .line 735
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v2, v0

    .line 733
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 735
    :cond_1
    const/16 v0, 0xa

    goto :goto_2

    .line 737
    :cond_2
    add-int/lit8 v0, v2, 0x0

    .line 738
    iget-object v1, p0, Lcom/google/maps/g/rb;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 740
    iget-object v1, p0, Lcom/google/maps/g/rb;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 741
    iput v0, p0, Lcom/google/maps/g/rb;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 578
    invoke-static {}, Lcom/google/maps/g/rb;->newBuilder()Lcom/google/maps/g/re;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/re;->a(Lcom/google/maps/g/rb;)Lcom/google/maps/g/re;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 578
    invoke-static {}, Lcom/google/maps/g/rb;->newBuilder()Lcom/google/maps/g/re;

    move-result-object v0

    return-object v0
.end method
