.class public final Lcom/google/maps/g/a/ge;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/gc;",
        "Lcom/google/maps/g/a/ge;",
        ">;",
        "Lcom/google/maps/g/a/gj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/maps/g/a/gc;->d:Lcom/google/maps/g/a/gc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ge;->b:Ljava/lang/Object;

    .line 390
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/ge;->c:Lcom/google/n/f;

    .line 264
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 295
    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 305
    :goto_0
    return-object p0

    .line 296
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/gc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 297
    iget v2, p0, Lcom/google/maps/g/a/ge;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/ge;->a:I

    .line 298
    iget-object v2, p1, Lcom/google/maps/g/a/gc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ge;->b:Ljava/lang/Object;

    .line 301
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/gc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :goto_2
    if-eqz v0, :cond_5

    .line 302
    iget-object v0, p1, Lcom/google/maps/g/a/gc;->c:Lcom/google/n/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 296
    goto :goto_1

    :cond_3
    move v0, v1

    .line 301
    goto :goto_2

    .line 302
    :cond_4
    iget v1, p0, Lcom/google/maps/g/a/ge;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/a/ge;->a:I

    iput-object v0, p0, Lcom/google/maps/g/a/ge;->c:Lcom/google/n/f;

    .line 304
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/gc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/maps/g/a/ge;->c()Lcom/google/maps/g/a/gc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 255
    check-cast p1, Lcom/google/maps/g/a/gc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ge;->a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/gc;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 279
    new-instance v2, Lcom/google/maps/g/a/gc;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/gc;-><init>(Lcom/google/n/v;)V

    .line 280
    iget v3, p0, Lcom/google/maps/g/a/ge;->a:I

    .line 281
    const/4 v1, 0x0

    .line 282
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 285
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/ge;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/gc;->b:Ljava/lang/Object;

    .line 286
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 287
    or-int/lit8 v0, v0, 0x2

    .line 289
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/ge;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/a/gc;->c:Lcom/google/n/f;

    .line 290
    iput v0, v2, Lcom/google/maps/g/a/gc;->a:I

    .line 291
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
