.class public final Lcom/google/maps/g/a/ec;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ed;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ea;",
        "Lcom/google/maps/g/a/ec;",
        ">;",
        "Lcom/google/maps/g/a/ed;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/f;

.field private e:Lcom/google/n/f;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 439
    sget-object v0, Lcom/google/maps/g/a/ea;->h:Lcom/google/maps/g/a/ea;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 564
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->b:Lcom/google/n/ao;

    .line 624
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    .line 760
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->d:Lcom/google/n/f;

    .line 795
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->e:Lcom/google/n/f;

    .line 830
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->f:Lcom/google/n/ao;

    .line 890
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    .line 440
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ea;)Lcom/google/maps/g/a/ec;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 501
    invoke-static {}, Lcom/google/maps/g/a/ea;->d()Lcom/google/maps/g/a/ea;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 537
    :goto_0
    return-object p0

    .line 502
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 503
    iget-object v2, p0, Lcom/google/maps/g/a/ec;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 504
    iget v2, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 506
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 507
    iget-object v2, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 508
    iget-object v2, p1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    .line 509
    iget v2, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 516
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 517
    iget-object v2, p1, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 502
    goto :goto_1

    .line 511
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 512
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_6
    move v2, v1

    .line 516
    goto :goto_3

    .line 517
    :cond_7
    iget v3, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/ec;->a:I

    iput-object v2, p0, Lcom/google/maps/g/a/ec;->d:Lcom/google/n/f;

    .line 519
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 520
    iget-object v2, p1, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v2, v1

    .line 519
    goto :goto_4

    .line 520
    :cond_a
    iget v3, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/ec;->a:I

    iput-object v2, p0, Lcom/google/maps/g/a/ec;->e:Lcom/google/n/f;

    .line 522
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    :goto_5
    if-eqz v0, :cond_c

    .line 523
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 524
    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 526
    :cond_c
    iget-object v0, p1, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 527
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 528
    iget-object v0, p1, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    .line 529
    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 536
    :cond_d
    :goto_6
    iget-object v0, p1, Lcom/google/maps/g/a/ea;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 522
    goto :goto_5

    .line 531
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_10

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/ec;->a:I

    .line 532
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 431
    new-instance v2, Lcom/google/maps/g/a/ea;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ea;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/ec;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/ec;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/a/ec;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/ec;->d:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/ec;->e:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/ec;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/ec;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/maps/g/a/ec;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/ec;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/a/ea;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 431
    check-cast p1, Lcom/google/maps/g/a/ea;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ec;->a(Lcom/google/maps/g/a/ea;)Lcom/google/maps/g/a/ec;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 541
    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 542
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 559
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 541
    goto :goto_0

    :cond_2
    move v1, v2

    .line 547
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 548
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ff;->d()Lcom/google/maps/g/a/ff;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ff;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ff;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 553
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ec;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 554
    iget-object v0, p0, Lcom/google/maps/g/a/ec;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/s;->d()Lcom/google/maps/g/a/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/s;

    invoke-virtual {v0}, Lcom/google/maps/g/a/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 559
    goto :goto_1

    :cond_5
    move v0, v2

    .line 553
    goto :goto_3
.end method
