.class public final Lcom/google/maps/g/hm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/hk;",
        "Lcom/google/maps/g/hm;",
        ">;",
        "Lcom/google/maps/g/hx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1716
    sget-object v0, Lcom/google/maps/g/hk;->d:Lcom/google/maps/g/hk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1776
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/hm;->b:Lcom/google/n/ao;

    .line 1836
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    .line 1717
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/hk;)Lcom/google/maps/g/hm;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1751
    invoke-static {}, Lcom/google/maps/g/hk;->d()Lcom/google/maps/g/hk;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1767
    :goto_0
    return-object p0

    .line 1752
    :cond_0
    iget v1, p1, Lcom/google/maps/g/hk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 1753
    iget-object v0, p0, Lcom/google/maps/g/hm;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/hk;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1754
    iget v0, p0, Lcom/google/maps/g/hm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/hm;->a:I

    .line 1756
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/hk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1757
    iget-object v0, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1758
    iget-object v0, p1, Lcom/google/maps/g/hk;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    .line 1759
    iget v0, p0, Lcom/google/maps/g/hm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/hm;->a:I

    .line 1766
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/hk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1752
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1761
    :cond_4
    iget v0, p0, Lcom/google/maps/g/hm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/hm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/hm;->a:I

    .line 1762
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/hk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1708
    new-instance v2, Lcom/google/maps/g/hk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/hk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/hm;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/maps/g/hk;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/hm;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/hm;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/hm;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/hm;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/hm;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/hm;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/hk;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/hk;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1708
    check-cast p1, Lcom/google/maps/g/hk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/hm;->a(Lcom/google/maps/g/hk;)Lcom/google/maps/g/hm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1771
    const/4 v0, 0x1

    return v0
.end method
