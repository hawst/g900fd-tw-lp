.class public final Lcom/google/maps/g/mk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/mn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/mk;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/mk;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Z

.field f:I

.field g:Lcom/google/n/ao;

.field h:Z

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/maps/g/ml;

    invoke-direct {v0}, Lcom/google/maps/g/ml;-><init>()V

    sput-object v0, Lcom/google/maps/g/mk;->PARSER:Lcom/google/n/ax;

    .line 1426
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/mk;->m:Lcom/google/n/aw;

    .line 2134
    new-instance v0, Lcom/google/maps/g/mk;

    invoke-direct {v0}, Lcom/google/maps/g/mk;-><init>()V

    sput-object v0, Lcom/google/maps/g/mk;->j:Lcom/google/maps/g/mk;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1138
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    .line 1268
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    .line 1341
    iput-byte v3, p0, Lcom/google/maps/g/mk;->k:B

    .line 1381
    iput v3, p0, Lcom/google/maps/g/mk;->l:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    .line 21
    iput-boolean v2, p0, Lcom/google/maps/g/mk;->e:Z

    .line 22
    iput v2, p0, Lcom/google/maps/g/mk;->f:I

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/g/mk;->h:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x80

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/mk;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 38
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 40
    sparse-switch v1, :sswitch_data_0

    .line 45
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 53
    iget v1, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/mk;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 100
    :catch_0
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 101
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v8, :cond_1

    .line 107
    iget-object v1, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    .line 109
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/mk;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 58
    iget v6, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/mk;->a:I

    .line 59
    iput-object v1, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 102
    :catch_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 103
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 64
    iget v6, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/mk;->a:I

    .line 65
    iput-object v1, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    goto :goto_0

    .line 106
    :catchall_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    goto :goto_1

    .line 69
    :sswitch_4
    iget v1, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/mk;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v10

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/maps/g/mk;->e:Z

    goto/16 :goto_0

    :cond_2
    move v1, v3

    goto :goto_2

    .line 74
    :sswitch_5
    iget v1, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/mk;->a:I

    .line 75
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/maps/g/mk;->f:I

    goto/16 :goto_0

    .line 79
    :sswitch_6
    iget-object v1, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 80
    iget v1, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/maps/g/mk;->a:I

    goto/16 :goto_0

    .line 84
    :sswitch_7
    iget v1, p0, Lcom/google/maps/g/mk;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/maps/g/mk;->a:I

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v10

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/google/maps/g/mk;->h:Z

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto :goto_3

    .line 89
    :sswitch_8
    and-int/lit16 v1, v0, 0x80

    if-eq v1, v8, :cond_4

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    .line 92
    or-int/lit16 v0, v0, 0x80

    .line 94
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 94
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 106
    :cond_5
    and-int/lit16 v0, v0, 0x80

    if-ne v0, v8, :cond_6

    .line 107
    iget-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    .line 109
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->au:Lcom/google/n/bn;

    .line 110
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1138
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    .line 1268
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    .line 1341
    iput-byte v1, p0, Lcom/google/maps/g/mk;->k:B

    .line 1381
    iput v1, p0, Lcom/google/maps/g/mk;->l:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/mk;
    .locals 1

    .prologue
    .line 2137
    sget-object v0, Lcom/google/maps/g/mk;->j:Lcom/google/maps/g/mk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/mm;
    .locals 1

    .prologue
    .line 1488
    new-instance v0, Lcom/google/maps/g/mm;

    invoke-direct {v0}, Lcom/google/maps/g/mm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/mk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/maps/g/mk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 1353
    invoke-virtual {p0}, Lcom/google/maps/g/mk;->c()I

    .line 1354
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1355
    iget-object v0, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1357
    :cond_0
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1358
    iget-object v0, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1360
    :cond_1
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1361
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1363
    :cond_2
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 1364
    iget-boolean v0, p0, Lcom/google/maps/g/mk;->e:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1366
    :cond_3
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1367
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/mk;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_a

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1369
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1370
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1372
    :cond_5
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 1373
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/mk;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_b

    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 1375
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 1376
    iget-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1375
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1358
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1361
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 1364
    goto/16 :goto_2

    .line 1367
    :cond_a
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    :cond_b
    move v1, v2

    .line 1373
    goto :goto_4

    .line 1378
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/mk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1379
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1343
    iget-byte v1, p0, Lcom/google/maps/g/mk;->k:B

    .line 1344
    if-ne v1, v0, :cond_0

    .line 1348
    :goto_0
    return v0

    .line 1345
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1347
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/mk;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1383
    iget v0, p0, Lcom/google/maps/g/mk;->l:I

    .line 1384
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1421
    :goto_0
    return v0

    .line 1387
    :cond_0
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 1388
    iget-object v0, p0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    .line 1389
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1391
    :goto_1
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1393
    iget-object v0, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1395
    :cond_1
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1396
    const/4 v3, 0x3

    .line 1397
    iget-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1399
    :cond_2
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 1400
    iget-boolean v0, p0, Lcom/google/maps/g/mk;->e:Z

    .line 1401
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1403
    :cond_3
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1404
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/mk;->f:I

    .line 1405
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1407
    :cond_4
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1408
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/mk;->g:Lcom/google/n/ao;

    .line 1409
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1411
    :cond_5
    iget v0, p0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 1412
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/mk;->h:Z

    .line 1413
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_6
    move v3, v1

    move v1, v2

    .line 1415
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 1416
    iget-object v0, p0, Lcom/google/maps/g/mk;->i:Ljava/util/List;

    .line 1417
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1415
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1393
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 1397
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1405
    :cond_9
    const/16 v0, 0xa

    goto :goto_4

    .line 1419
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/mk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1420
    iput v0, p0, Lcom/google/maps/g/mk;->l:I

    goto/16 :goto_0

    :cond_b
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    .line 1208
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1209
    check-cast v0, Ljava/lang/String;

    .line 1217
    :goto_0
    return-object v0

    .line 1211
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1213
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1214
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1215
    iput-object v1, p0, Lcom/google/maps/g/mk;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1217
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/mk;->newBuilder()Lcom/google/maps/g/mm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/mm;->a(Lcom/google/maps/g/mk;)Lcom/google/maps/g/mm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/mk;->newBuilder()Lcom/google/maps/g/mm;

    move-result-object v0

    return-object v0
.end method
