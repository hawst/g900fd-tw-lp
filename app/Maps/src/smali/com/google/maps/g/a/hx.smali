.class public final Lcom/google/maps/g/a/hx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ia;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hx;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/a/hx;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Lcom/google/maps/g/hg;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 961
    new-instance v0, Lcom/google/maps/g/a/hy;

    invoke-direct {v0}, Lcom/google/maps/g/a/hy;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hx;->PARSER:Lcom/google/n/ax;

    .line 1050
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/hx;->g:Lcom/google/n/aw;

    .line 1276
    new-instance v0, Lcom/google/maps/g/a/hx;

    invoke-direct {v0}, Lcom/google/maps/g/a/hx;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hx;->d:Lcom/google/maps/g/a/hx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 905
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1007
    iput-byte v0, p0, Lcom/google/maps/g/a/hx;->e:B

    .line 1029
    iput v0, p0, Lcom/google/maps/g/a/hx;->f:I

    .line 906
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/a/hx;->b:Z

    .line 907
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 913
    invoke-direct {p0}, Lcom/google/maps/g/a/hx;-><init>()V

    .line 914
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    .line 919
    :cond_0
    :goto_0
    if-nez v4, :cond_3

    .line 920
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 921
    sparse-switch v0, :sswitch_data_0

    .line 926
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 928
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 924
    goto :goto_0

    .line 933
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/a/hx;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/hx;->a:I

    .line 934
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/hx;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 952
    :catch_0
    move-exception v0

    .line 953
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 958
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hx;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v3

    .line 934
    goto :goto_1

    .line 938
    :sswitch_2
    const/4 v0, 0x0

    .line 939
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/a/hx;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v6, 0x2

    if-ne v1, v6, :cond_4

    .line 940
    iget-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    move-object v1, v0

    .line 942
    :goto_2
    sget-object v0, Lcom/google/maps/g/hg;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    iput-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    .line 943
    if-eqz v1, :cond_2

    .line 944
    iget-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    .line 945
    invoke-virtual {v1}, Lcom/google/maps/g/hi;->c()Lcom/google/maps/g/hg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    .line 947
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/hx;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/hx;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 954
    :catch_1
    move-exception v0

    .line 955
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 956
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 958
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hx;->au:Lcom/google/n/bn;

    .line 959
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_2

    .line 921
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 903
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1007
    iput-byte v0, p0, Lcom/google/maps/g/a/hx;->e:B

    .line 1029
    iput v0, p0, Lcom/google/maps/g/a/hx;->f:I

    .line 904
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;
    .locals 1

    .prologue
    .line 1115
    invoke-static {}, Lcom/google/maps/g/a/hx;->newBuilder()Lcom/google/maps/g/a/hz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/hz;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/hx;
    .locals 1

    .prologue
    .line 1279
    sget-object v0, Lcom/google/maps/g/a/hx;->d:Lcom/google/maps/g/a/hx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/hz;
    .locals 1

    .prologue
    .line 1112
    new-instance v0, Lcom/google/maps/g/a/hz;

    invoke-direct {v0}, Lcom/google/maps/g/a/hz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 973
    sget-object v0, Lcom/google/maps/g/a/hx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 1019
    invoke-virtual {p0}, Lcom/google/maps/g/a/hx;->c()I

    .line 1020
    iget v2, p0, Lcom/google/maps/g/a/hx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 1021
    iget-boolean v2, p0, Lcom/google/maps/g/a/hx;->b:Z

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_2

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1023
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/hx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1024
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v0

    :goto_1
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1026
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/hx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1027
    return-void

    :cond_2
    move v0, v1

    .line 1021
    goto :goto_0

    .line 1024
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1009
    iget-byte v1, p0, Lcom/google/maps/g/a/hx;->e:B

    .line 1010
    if-ne v1, v0, :cond_0

    .line 1014
    :goto_0
    return v0

    .line 1011
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1013
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/hx;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 1031
    iget v0, p0, Lcom/google/maps/g/a/hx;->f:I

    .line 1032
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1045
    :goto_0
    return v0

    .line 1035
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/hx;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 1036
    iget-boolean v0, p0, Lcom/google/maps/g/a/hx;->b:Z

    .line 1037
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1039
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/hx;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_1

    .line 1040
    const/4 v3, 0x3

    .line 1041
    iget-object v2, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    if-nez v2, :cond_2

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    :goto_2
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1043
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/hx;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    iput v0, p0, Lcom/google/maps/g/a/hx;->f:I

    goto :goto_0

    .line 1041
    :cond_2
    iget-object v2, p0, Lcom/google/maps/g/a/hx;->c:Lcom/google/maps/g/hg;

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 897
    invoke-static {}, Lcom/google/maps/g/a/hx;->newBuilder()Lcom/google/maps/g/a/hz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/hz;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 897
    invoke-static {}, Lcom/google/maps/g/a/hx;->newBuilder()Lcom/google/maps/g/a/hz;

    move-result-object v0

    return-object v0
.end method
