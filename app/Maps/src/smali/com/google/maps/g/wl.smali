.class public final Lcom/google/maps/g/wl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/wj;",
        "Lcom/google/maps/g/wl;",
        ">;",
        "Lcom/google/maps/g/wm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 359
    sget-object v0, Lcom/google/maps/g/wj;->g:Lcom/google/maps/g/wj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 455
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wl;->b:Lcom/google/n/ao;

    .line 514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wl;->c:Lcom/google/n/ao;

    .line 574
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    .line 710
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wl;->e:Lcom/google/n/ao;

    .line 769
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wl;->f:Lcom/google/n/ao;

    .line 360
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 418
    invoke-static {}, Lcom/google/maps/g/wj;->d()Lcom/google/maps/g/wj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 446
    :goto_0
    return-object p0

    .line 419
    :cond_0
    iget v2, p1, Lcom/google/maps/g/wj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 420
    iget-object v2, p0, Lcom/google/maps/g/wl;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 421
    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/wl;->a:I

    .line 423
    :cond_1
    iget v2, p1, Lcom/google/maps/g/wj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 424
    iget-object v2, p0, Lcom/google/maps/g/wl;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/wj;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 425
    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/wl;->a:I

    .line 427
    :cond_2
    iget-object v2, p1, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 428
    iget-object v2, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 429
    iget-object v2, p1, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    .line 430
    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/wl;->a:I

    .line 437
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/maps/g/wj;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 438
    iget-object v2, p0, Lcom/google/maps/g/wl;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/wj;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 439
    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/wl;->a:I

    .line 441
    :cond_4
    iget v2, p1, Lcom/google/maps/g/wj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 442
    iget-object v0, p0, Lcom/google/maps/g/wl;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/wj;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 443
    iget v0, p0, Lcom/google/maps/g/wl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/wl;->a:I

    .line 445
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/wj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 419
    goto :goto_1

    :cond_7
    move v2, v1

    .line 423
    goto :goto_2

    .line 432
    :cond_8
    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/wl;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/wl;->a:I

    .line 433
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 437
    goto :goto_4

    :cond_b
    move v0, v1

    .line 441
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/google/maps/g/wl;->c()Lcom/google/maps/g/wj;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 351
    check-cast p1, Lcom/google/maps/g/wj;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/wl;->a(Lcom/google/maps/g/wj;)Lcom/google/maps/g/wl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/wj;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 381
    new-instance v2, Lcom/google/maps/g/wj;

    invoke-direct {v2, p0}, Lcom/google/maps/g/wj;-><init>(Lcom/google/n/v;)V

    .line 382
    iget v3, p0, Lcom/google/maps/g/wl;->a:I

    .line 384
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 387
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/wl;->b:Lcom/google/n/ao;

    .line 388
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/wl;->b:Lcom/google/n/ao;

    .line 389
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 387
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 390
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 391
    or-int/lit8 v0, v0, 0x2

    .line 393
    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/wj;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/wl;->c:Lcom/google/n/ao;

    .line 394
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/wl;->c:Lcom/google/n/ao;

    .line 395
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 393
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 396
    iget v4, p0, Lcom/google/maps/g/wl;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 397
    iget-object v4, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    .line 398
    iget v4, p0, Lcom/google/maps/g/wl;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/wl;->a:I

    .line 400
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/wl;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    .line 401
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 402
    or-int/lit8 v0, v0, 0x4

    .line 404
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/wj;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/wl;->e:Lcom/google/n/ao;

    .line 405
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/wl;->e:Lcom/google/n/ao;

    .line 406
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 404
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 407
    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    .line 408
    or-int/lit8 v0, v0, 0x8

    .line 410
    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/wj;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/wl;->f:Lcom/google/n/ao;

    .line 411
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/wl;->f:Lcom/google/n/ao;

    .line 412
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 410
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 413
    iput v0, v2, Lcom/google/maps/g/wj;->a:I

    .line 414
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
