.class public final Lcom/google/maps/g/a/gm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/gk;",
        "Lcom/google/maps/g/a/gm;",
        ">;",
        "Lcom/google/maps/g/a/gn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 434
    sget-object v0, Lcom/google/maps/g/a/gk;->h:Lcom/google/maps/g/a/gk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 534
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->b:Ljava/lang/Object;

    .line 610
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->c:Ljava/lang/Object;

    .line 686
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->d:Lcom/google/n/ao;

    .line 745
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->e:Lcom/google/n/ao;

    .line 804
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->f:Ljava/lang/Object;

    .line 880
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gm;->g:Lcom/google/n/ao;

    .line 435
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/gk;)Lcom/google/maps/g/a/gm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 496
    invoke-static {}, Lcom/google/maps/g/a/gk;->g()Lcom/google/maps/g/a/gk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 525
    :goto_0
    return-object p0

    .line 497
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 498
    iget v2, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 499
    iget-object v2, p1, Lcom/google/maps/g/a/gk;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gm;->b:Ljava/lang/Object;

    .line 502
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 503
    iget v2, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 504
    iget-object v2, p1, Lcom/google/maps/g/a/gk;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gm;->c:Ljava/lang/Object;

    .line 507
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 508
    iget-object v2, p0, Lcom/google/maps/g/a/gm;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gk;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 509
    iget v2, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 511
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 512
    iget-object v2, p0, Lcom/google/maps/g/a/gm;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gk;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 513
    iget v2, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 515
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 516
    iget v2, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 517
    iget-object v2, p1, Lcom/google/maps/g/a/gk;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gm;->f:Ljava/lang/Object;

    .line 520
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/gk;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 521
    iget-object v0, p0, Lcom/google/maps/g/a/gm;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/gk;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 522
    iget v0, p0, Lcom/google/maps/g/a/gm;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/gm;->a:I

    .line 524
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/a/gk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 497
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 502
    goto :goto_2

    :cond_9
    move v2, v1

    .line 507
    goto :goto_3

    :cond_a
    move v2, v1

    .line 511
    goto :goto_4

    :cond_b
    move v2, v1

    .line 515
    goto :goto_5

    :cond_c
    move v0, v1

    .line 520
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 426
    new-instance v2, Lcom/google/maps/g/a/gk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/gk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/gm;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/gm;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gk;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/gm;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gk;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/gk;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gm;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gm;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/gk;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gm;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gm;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/a/gm;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gk;->f:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v3, v2, Lcom/google/maps/g/a/gk;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/gm;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/gm;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/gk;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 426
    check-cast p1, Lcom/google/maps/g/a/gk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/gm;->a(Lcom/google/maps/g/a/gk;)Lcom/google/maps/g/a/gm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x1

    return v0
.end method
