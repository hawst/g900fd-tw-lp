.class public final Lcom/google/maps/g/nx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ny;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/nv;",
        "Lcom/google/maps/g/nx;",
        ">;",
        "Lcom/google/maps/g/ny;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/maps/g/hg;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/google/maps/g/nv;->d:Lcom/google/maps/g/nv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 445
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nx;->b:Ljava/lang/Object;

    .line 521
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    .line 395
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/nv;)Lcom/google/maps/g/nx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 426
    invoke-static {}, Lcom/google/maps/g/nv;->d()Lcom/google/maps/g/nv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 436
    :goto_0
    return-object p0

    .line 427
    :cond_0
    iget v2, p1, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 428
    iget v2, p0, Lcom/google/maps/g/nx;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/nx;->a:I

    .line 429
    iget-object v2, p1, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/nx;->b:Ljava/lang/Object;

    .line 432
    :cond_1
    iget v2, p1, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 433
    iget-object v0, p1, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v0

    :goto_3
    iget v1, p0, Lcom/google/maps/g/nx;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    invoke-static {v1}, Lcom/google/maps/g/hg;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/hi;->c()Lcom/google/maps/g/hg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    :goto_4
    iget v0, p0, Lcom/google/maps/g/nx;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/nx;->a:I

    .line 435
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/nv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 427
    goto :goto_1

    :cond_4
    move v0, v1

    .line 432
    goto :goto_2

    .line 433
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    goto :goto_3

    :cond_6
    iput-object v0, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 386
    new-instance v2, Lcom/google/maps/g/nv;

    invoke-direct {v2, p0}, Lcom/google/maps/g/nv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/nx;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/nx;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/nx;->c:Lcom/google/maps/g/hg;

    iput-object v1, v2, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    iput v0, v2, Lcom/google/maps/g/nv;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 386
    check-cast p1, Lcom/google/maps/g/nv;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/nx;->a(Lcom/google/maps/g/nv;)Lcom/google/maps/g/nx;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x1

    return v0
.end method
