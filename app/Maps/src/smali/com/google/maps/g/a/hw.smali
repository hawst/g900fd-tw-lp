.class public final Lcom/google/maps/g/a/hw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/in;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/hu;",
        "Lcom/google/maps/g/a/hw;",
        ">;",
        "Lcom/google/maps/g/a/in;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/maps/g/a/fw;

.field public c:I

.field public d:Lcom/google/maps/g/a/gc;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/maps/g/a/fk;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ea;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/Object;

.field private j:Lcom/google/n/aq;

.field private k:Lcom/google/maps/g/a/be;

.field private l:Lcom/google/maps/g/a/be;

.field private m:Lcom/google/n/aq;

.field private n:Lcom/google/n/aq;

.field private o:I

.field private p:Lcom/google/maps/g/a/be;

.field private q:Lcom/google/maps/g/a/da;

.field private r:Lcom/google/maps/g/a/id;

.field private s:Lcom/google/maps/g/a/bi;

.field private t:Lcom/google/maps/g/a/hx;

.field private u:Lcom/google/maps/g/a/ij;

.field private v:Lcom/google/n/f;

.field private w:Z

.field private x:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2919
    sget-object v0, Lcom/google/maps/g/a/hu;->y:Lcom/google/maps/g/a/hu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3207
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->e:Ljava/lang/Object;

    .line 3283
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->f:Ljava/lang/Object;

    .line 3359
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    .line 3421
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    .line 3545
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    .line 3606
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->i:Ljava/lang/Object;

    .line 3682
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    .line 3807
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    .line 3868
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    .line 3929
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    .line 4022
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    .line 4115
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/hw;->o:I

    .line 4151
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    .line 4212
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    .line 4273
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    .line 4334
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    .line 4395
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    .line 4456
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    .line 4517
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->v:Lcom/google/n/f;

    .line 4584
    iput-object v1, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    .line 4645
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->x:Lcom/google/n/f;

    .line 2920
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/hu;)Lcom/google/maps/g/a/hw;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3081
    invoke-static {}, Lcom/google/maps/g/a/hu;->d()Lcom/google/maps/g/a/hu;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3186
    :goto_0
    return-object p0

    .line 3082
    :cond_0
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_e

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 3083
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3084
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->e:Ljava/lang/Object;

    .line 3087
    :cond_1
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 3088
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3089
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->f:Ljava/lang/Object;

    .line 3092
    :cond_2
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 3093
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v0, :cond_11

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_4
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v4

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    invoke-static {v3}, Lcom/google/maps/g/a/fk;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/fm;->c()Lcom/google/maps/g/a/fk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3095
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3096
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 3097
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    .line 3098
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3105
    :cond_4
    :goto_6
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_15

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 3106
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v4

    if-eq v3, v4, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    invoke-static {v3}, Lcom/google/maps/g/a/fw;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/fy;->c()Lcom/google/maps/g/a/fw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    :goto_9
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3108
    :cond_5
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_a
    if-eqz v0, :cond_6

    .line 3109
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3110
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->i:Ljava/lang/Object;

    .line 3113
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3114
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 3115
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    .line 3116
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3123
    :cond_7
    :goto_b
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_c
    if-eqz v0, :cond_8

    .line 3124
    iget v0, p1, Lcom/google/maps/g/a/hu;->i:I

    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/hw;->a:I

    iput v0, p0, Lcom/google/maps/g/a/hw;->c:I

    .line 3126
    :cond_8
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_d
    if-eqz v0, :cond_9

    .line 3127
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_1d

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_e
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_1e

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    if-eqz v3, :cond_1e

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    if-eq v3, v4, :cond_1e

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    invoke-static {v3}, Lcom/google/maps/g/a/be;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    :goto_f
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3129
    :cond_9
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_10
    if-eqz v0, :cond_a

    .line 3130
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_20

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_11
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_21

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    if-eqz v3, :cond_21

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    if-eq v3, v4, :cond_21

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    invoke-static {v3}, Lcom/google/maps/g/a/be;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    :goto_12
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3132
    :cond_a
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 3133
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 3134
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    .line 3135
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3142
    :cond_b
    :goto_13
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 3143
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 3144
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    .line 3145
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3152
    :cond_c
    :goto_14
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_26

    move v0, v1

    :goto_15
    if-eqz v0, :cond_28

    .line 3153
    iget v0, p1, Lcom/google/maps/g/a/hu;->n:I

    invoke-static {v0}, Lcom/google/maps/g/a/ib;->a(I)Lcom/google/maps/g/a/ib;

    move-result-object v0

    if-nez v0, :cond_d

    sget-object v0, Lcom/google/maps/g/a/ib;->a:Lcom/google/maps/g/a/ib;

    :cond_d
    if-nez v0, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v0, v2

    .line 3082
    goto/16 :goto_1

    :cond_f
    move v0, v2

    .line 3087
    goto/16 :goto_2

    :cond_10
    move v0, v2

    .line 3092
    goto/16 :goto_3

    .line 3093
    :cond_11
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto/16 :goto_4

    :cond_12
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    goto/16 :goto_5

    .line 3100
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-eq v0, v3, :cond_14

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3101
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v0, v2

    .line 3105
    goto/16 :goto_7

    .line 3106
    :cond_16
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    goto/16 :goto_8

    :cond_17
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    goto/16 :goto_9

    :cond_18
    move v0, v2

    .line 3108
    goto/16 :goto_a

    .line 3118
    :cond_19
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-eq v0, v3, :cond_1a

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3119
    :cond_1a
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_1b
    move v0, v2

    .line 3123
    goto/16 :goto_c

    :cond_1c
    move v0, v2

    .line 3126
    goto/16 :goto_d

    .line 3127
    :cond_1d
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    goto/16 :goto_e

    :cond_1e
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_f

    :cond_1f
    move v0, v2

    .line 3129
    goto/16 :goto_10

    .line 3130
    :cond_20
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_11

    :cond_21
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    goto/16 :goto_12

    .line 3137
    :cond_22
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-eq v0, v3, :cond_23

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3138
    :cond_23
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_13

    .line 3147
    :cond_24
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-eq v0, v3, :cond_25

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3148
    :cond_25
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_14

    :cond_26
    move v0, v2

    .line 3152
    goto/16 :goto_15

    .line 3153
    :cond_27
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/g/a/hw;->a:I

    iget v0, v0, Lcom/google/maps/g/a/ib;->d:I

    iput v0, p0, Lcom/google/maps/g/a/hw;->o:I

    .line 3155
    :cond_28
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_2f

    move v0, v1

    :goto_16
    if-eqz v0, :cond_29

    .line 3156
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_30

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_17
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v3, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_31

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    if-eqz v3, :cond_31

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    if-eq v3, v4, :cond_31

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    invoke-static {v3}, Lcom/google/maps/g/a/be;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    :goto_18
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3158
    :cond_29
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_32

    move v0, v1

    :goto_19
    if-eqz v0, :cond_2a

    .line 3159
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    if-nez v0, :cond_33

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v0

    :goto_1a
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v3, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_34

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    if-eqz v3, :cond_34

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v4

    if-eq v3, v4, :cond_34

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    invoke-static {v3}, Lcom/google/maps/g/a/da;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/dc;->c()Lcom/google/maps/g/a/da;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    :goto_1b
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3161
    :cond_2a
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_35

    move v0, v1

    :goto_1c
    if-eqz v0, :cond_2b

    .line 3162
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    if-nez v0, :cond_36

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v0

    :goto_1d
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/2addr v3, v5

    if-ne v3, v5, :cond_37

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    if-eqz v3, :cond_37

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v4

    if-eq v3, v4, :cond_37

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    invoke-static {v3}, Lcom/google/maps/g/a/id;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/if;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/if;->c()Lcom/google/maps/g/a/id;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    :goto_1e
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3164
    :cond_2b
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_38

    move v0, v1

    :goto_1f
    if-eqz v0, :cond_2c

    .line 3165
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v0, :cond_39

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    :goto_20
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/2addr v3, v6

    if-ne v3, v6, :cond_3a

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    if-eqz v3, :cond_3a

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v4

    if-eq v3, v4, :cond_3a

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    invoke-static {v3}, Lcom/google/maps/g/a/bi;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bk;->c()Lcom/google/maps/g/a/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    :goto_21
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3167
    :cond_2c
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_3b

    move v0, v1

    :goto_22
    if-eqz v0, :cond_2d

    .line 3168
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    if-nez v0, :cond_3c

    invoke-static {}, Lcom/google/maps/g/a/hx;->d()Lcom/google/maps/g/a/hx;

    move-result-object v0

    :goto_23
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/2addr v3, v7

    if-ne v3, v7, :cond_3d

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    if-eqz v3, :cond_3d

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    invoke-static {}, Lcom/google/maps/g/a/hx;->d()Lcom/google/maps/g/a/hx;

    move-result-object v4

    if-eq v3, v4, :cond_3d

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    invoke-static {v3}, Lcom/google/maps/g/a/hx;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/hz;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/hz;->c()Lcom/google/maps/g/a/hx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    :goto_24
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3170
    :cond_2d
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_3e

    move v0, v1

    :goto_25
    if-eqz v0, :cond_2e

    .line 3171
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    if-nez v0, :cond_3f

    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v0

    :goto_26
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v4, 0x40000

    and-int/2addr v3, v4

    const/high16 v4, 0x40000

    if-ne v3, v4, :cond_40

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    if-eqz v3, :cond_40

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v4

    if-eq v3, v4, :cond_40

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    invoke-static {v3}, Lcom/google/maps/g/a/ij;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/il;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/il;->c()Lcom/google/maps/g/a/ij;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    :goto_27
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3173
    :cond_2e
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_41

    move v0, v1

    :goto_28
    if-eqz v0, :cond_43

    .line 3174
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    if-nez v0, :cond_42

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2f
    move v0, v2

    .line 3155
    goto/16 :goto_16

    .line 3156
    :cond_30
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    goto/16 :goto_17

    :cond_31
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    goto/16 :goto_18

    :cond_32
    move v0, v2

    .line 3158
    goto/16 :goto_19

    .line 3159
    :cond_33
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    goto/16 :goto_1a

    :cond_34
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    goto/16 :goto_1b

    :cond_35
    move v0, v2

    .line 3161
    goto/16 :goto_1c

    .line 3162
    :cond_36
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    goto/16 :goto_1d

    :cond_37
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    goto/16 :goto_1e

    :cond_38
    move v0, v2

    .line 3164
    goto/16 :goto_1f

    .line 3165
    :cond_39
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto/16 :goto_20

    :cond_3a
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    goto/16 :goto_21

    :cond_3b
    move v0, v2

    .line 3167
    goto/16 :goto_22

    .line 3168
    :cond_3c
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    goto/16 :goto_23

    :cond_3d
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    goto/16 :goto_24

    :cond_3e
    move v0, v2

    .line 3170
    goto :goto_25

    .line 3171
    :cond_3f
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    goto :goto_26

    :cond_40
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    goto :goto_27

    :cond_41
    move v0, v2

    .line 3173
    goto :goto_28

    .line 3174
    :cond_42
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/a/hw;->a:I

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->v:Lcom/google/n/f;

    .line 3176
    :cond_43
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_46

    move v0, v1

    :goto_29
    if-eqz v0, :cond_44

    .line 3177
    iget-boolean v0, p1, Lcom/google/maps/g/a/hu;->v:Z

    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/a/hw;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/hw;->w:Z

    .line 3179
    :cond_44
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_47

    move v0, v1

    :goto_2a
    if-eqz v0, :cond_45

    .line 3180
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    if-nez v0, :cond_48

    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v0

    :goto_2b
    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v4, 0x200000

    and-int/2addr v3, v4

    const/high16 v4, 0x200000

    if-ne v3, v4, :cond_49

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    if-eqz v3, :cond_49

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v4

    if-eq v3, v4, :cond_49

    iget-object v3, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    invoke-static {v3}, Lcom/google/maps/g/a/gc;->a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/ge;->a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/ge;->c()Lcom/google/maps/g/a/gc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    :goto_2c
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v3, 0x200000

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/maps/g/a/hw;->a:I

    .line 3182
    :cond_45
    iget v0, p1, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_4a

    move v0, v1

    :goto_2d
    if-eqz v0, :cond_4c

    .line 3183
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;

    if-nez v0, :cond_4b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_46
    move v0, v2

    .line 3176
    goto :goto_29

    :cond_47
    move v0, v2

    .line 3179
    goto :goto_2a

    .line 3180
    :cond_48
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    goto :goto_2b

    :cond_49
    iput-object v0, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    goto :goto_2c

    :cond_4a
    move v0, v2

    .line 3182
    goto :goto_2d

    .line 3183
    :cond_4b
    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v2, 0x400000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/a/hw;->a:I

    iput-object v0, p0, Lcom/google/maps/g/a/hw;->x:Lcom/google/n/f;

    .line 3185
    :cond_4c
    iget-object v0, p1, Lcom/google/maps/g/a/hu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/high16 v8, 0x40000

    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    .line 2911
    new-instance v2, Lcom/google/maps/g/a/hu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/hu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/hw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_16

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/a/hw;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/maps/g/a/hw;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->j:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget v1, p0, Lcom/google/maps/g/a/hw;->c:I

    iput v1, v2, Lcom/google/maps/g/a/hu;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->k:Lcom/google/maps/g/a/be;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->l:Lcom/google/maps/g/a/be;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    iget-object v1, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v1, v1, -0x401

    iput v1, p0, Lcom/google/maps/g/a/hw;->a:I

    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->m:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    iget-object v1, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lcom/google/maps/g/a/hw;->a:I

    :cond_a
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->n:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x100

    :cond_b
    iget v1, p0, Lcom/google/maps/g/a/hw;->o:I

    iput v1, v2, Lcom/google/maps/g/a/hu;->n:I

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x200

    :cond_c
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->p:Lcom/google/maps/g/a/be;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x400

    :cond_d
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->q:Lcom/google/maps/g/a/da;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    and-int v1, v3, v5

    if-ne v1, v5, :cond_e

    or-int/lit16 v0, v0, 0x800

    :cond_e
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->r:Lcom/google/maps/g/a/id;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    and-int v1, v3, v6

    if-ne v1, v6, :cond_f

    or-int/lit16 v0, v0, 0x1000

    :cond_f
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->s:Lcom/google/maps/g/a/bi;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    and-int v1, v3, v7

    if-ne v1, v7, :cond_10

    or-int/lit16 v0, v0, 0x2000

    :cond_10
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->t:Lcom/google/maps/g/a/hx;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    and-int v1, v3, v8

    if-ne v1, v8, :cond_11

    or-int/lit16 v0, v0, 0x4000

    :cond_11
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->u:Lcom/google/maps/g/a/ij;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    const/high16 v1, 0x80000

    and-int/2addr v1, v3

    const/high16 v4, 0x80000

    if-ne v1, v4, :cond_12

    or-int/2addr v0, v5

    :cond_12
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->v:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    const/high16 v1, 0x100000

    and-int/2addr v1, v3

    const/high16 v4, 0x100000

    if-ne v1, v4, :cond_13

    or-int/2addr v0, v6

    :cond_13
    iget-boolean v1, p0, Lcom/google/maps/g/a/hw;->w:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/hu;->v:Z

    const/high16 v1, 0x200000

    and-int/2addr v1, v3

    const/high16 v4, 0x200000

    if-ne v1, v4, :cond_14

    or-int/2addr v0, v7

    :cond_14
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    const/high16 v1, 0x400000

    and-int/2addr v1, v3

    const/high16 v3, 0x400000

    if-ne v1, v3, :cond_15

    or-int/2addr v0, v8

    :cond_15
    iget-object v1, p0, Lcom/google/maps/g/a/hw;->x:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;

    iput v0, v2, Lcom/google/maps/g/a/hu;->a:I

    return-object v2

    :cond_16
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2911
    check-cast p1, Lcom/google/maps/g/a/hu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/hw;->a(Lcom/google/maps/g/a/hu;)Lcom/google/maps/g/a/hw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3190
    iget v0, p0, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 3191
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3202
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 3190
    goto :goto_0

    .line 3191
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->g:Lcom/google/maps/g/a/fk;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3196
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 3197
    iget-object v0, p0, Lcom/google/maps/g/a/hw;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ea;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3196
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 3202
    goto :goto_2
.end method
