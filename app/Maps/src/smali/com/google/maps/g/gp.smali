.class public final Lcom/google/maps/g/gp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/gn;",
        "Lcom/google/maps/g/gp;",
        ">;",
        "Lcom/google/maps/g/gs;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:I

.field public d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lcom/google/maps/g/gn;->e:Lcom/google/maps/g/gn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 416
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    .line 492
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/gp;->c:I

    .line 524
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/gp;->d:I

    .line 357
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/gn;)Lcom/google/maps/g/gp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 394
    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 407
    :goto_0
    return-object p0

    .line 395
    :cond_0
    iget v2, p1, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 396
    iget v2, p0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/gp;->a:I

    .line 397
    iget-object v2, p1, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    .line 400
    :cond_1
    iget v2, p1, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 401
    iget v2, p1, Lcom/google/maps/g/gn;->c:I

    iget v3, p0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/gp;->a:I

    iput v2, p0, Lcom/google/maps/g/gp;->c:I

    .line 403
    :cond_2
    iget v2, p1, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 404
    iget v0, p1, Lcom/google/maps/g/gn;->d:I

    invoke-static {v0}, Lcom/google/maps/g/gq;->a(I)Lcom/google/maps/g/gq;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/gq;->a:Lcom/google/maps/g/gq;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 395
    goto :goto_1

    :cond_5
    move v2, v1

    .line 400
    goto :goto_2

    :cond_6
    move v0, v1

    .line 403
    goto :goto_3

    .line 404
    :cond_7
    iget v1, p0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/gp;->a:I

    iget v0, v0, Lcom/google/maps/g/gq;->c:I

    iput v0, p0, Lcom/google/maps/g/gp;->d:I

    .line 406
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/gn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 348
    new-instance v2, Lcom/google/maps/g/gn;

    invoke-direct {v2, p0}, Lcom/google/maps/g/gn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/gp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/gn;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/gp;->c:I

    iput v1, v2, Lcom/google/maps/g/gn;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/gp;->d:I

    iput v1, v2, Lcom/google/maps/g/gn;->d:I

    iput v0, v2, Lcom/google/maps/g/gn;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 348
    check-cast p1, Lcom/google/maps/g/gn;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/gp;->a(Lcom/google/maps/g/gn;)Lcom/google/maps/g/gp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    return v0
.end method
