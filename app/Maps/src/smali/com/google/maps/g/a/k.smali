.class public final Lcom/google/maps/g/a/k;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/i;",
        "Lcom/google/maps/g/a/k;",
        ">;",
        "Lcom/google/maps/g/a/n;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1776
    sget-object v0, Lcom/google/maps/g/a/i;->g:Lcom/google/maps/g/a/i;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1867
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/k;->b:I

    .line 1903
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/k;->c:Ljava/lang/Object;

    .line 1979
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/k;->d:Lcom/google/n/ao;

    .line 2039
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    .line 2175
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/k;->f:Ljava/lang/Object;

    .line 1777
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1829
    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1858
    :goto_0
    return-object p0

    .line 1830
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1831
    iget v2, p1, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1830
    goto :goto_1

    .line 1831
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/k;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/k;->a:I

    iget v2, v2, Lcom/google/maps/g/a/l;->e:I

    iput v2, p0, Lcom/google/maps/g/a/k;->b:I

    .line 1833
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1834
    iget v2, p0, Lcom/google/maps/g/a/k;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1835
    iget-object v2, p1, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/k;->c:Ljava/lang/Object;

    .line 1838
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1839
    iget-object v2, p0, Lcom/google/maps/g/a/k;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1840
    iget v2, p0, Lcom/google/maps/g/a/k;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1842
    :cond_6
    iget-object v2, p1, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1843
    iget-object v2, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1844
    iget-object v2, p1, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    .line 1845
    iget v2, p0, Lcom/google/maps/g/a/k;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1852
    :cond_7
    :goto_4
    iget v2, p1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_d

    :goto_5
    if-eqz v0, :cond_8

    .line 1853
    iget v0, p0, Lcom/google/maps/g/a/k;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1854
    iget-object v0, p1, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/k;->f:Ljava/lang/Object;

    .line 1857
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/a/i;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 1833
    goto :goto_2

    :cond_a
    move v2, v1

    .line 1838
    goto :goto_3

    .line 1847
    :cond_b
    iget v2, p0, Lcom/google/maps/g/a/k;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/k;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1848
    :cond_c
    iget-object v2, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_d
    move v0, v1

    .line 1852
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/google/maps/g/a/k;->c()Lcom/google/maps/g/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1768
    check-cast p1, Lcom/google/maps/g/a/i;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/k;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1862
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/i;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1798
    new-instance v2, Lcom/google/maps/g/a/i;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/i;-><init>(Lcom/google/n/v;)V

    .line 1799
    iget v3, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1801
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 1804
    :goto_0
    iget v4, p0, Lcom/google/maps/g/a/k;->b:I

    iput v4, v2, Lcom/google/maps/g/a/i;->b:I

    .line 1805
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1806
    or-int/lit8 v0, v0, 0x2

    .line 1808
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/k;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/i;->c:Ljava/lang/Object;

    .line 1809
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1810
    or-int/lit8 v0, v0, 0x4

    .line 1812
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/i;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/k;->d:Lcom/google/n/ao;

    .line 1813
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/k;->d:Lcom/google/n/ao;

    .line 1814
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1812
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1815
    iget v1, p0, Lcom/google/maps/g/a/k;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1816
    iget-object v1, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    .line 1817
    iget v1, p0, Lcom/google/maps/g/a/k;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/a/k;->a:I

    .line 1819
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/k;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/i;->e:Ljava/util/List;

    .line 1820
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 1821
    or-int/lit8 v0, v0, 0x8

    .line 1823
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/k;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    .line 1824
    iput v0, v2, Lcom/google/maps/g/a/i;->a:I

    .line 1825
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
