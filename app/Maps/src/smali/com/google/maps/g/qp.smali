.class public final Lcom/google/maps/g/qp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/qs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qp;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/qp;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3336
    new-instance v0, Lcom/google/maps/g/qq;

    invoke-direct {v0}, Lcom/google/maps/g/qq;-><init>()V

    sput-object v0, Lcom/google/maps/g/qp;->PARSER:Lcom/google/n/ax;

    .line 3479
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/qp;->g:Lcom/google/n/aw;

    .line 3768
    new-instance v0, Lcom/google/maps/g/qp;

    invoke-direct {v0}, Lcom/google/maps/g/qp;-><init>()V

    sput-object v0, Lcom/google/maps/g/qp;->d:Lcom/google/maps/g/qp;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3285
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3436
    iput-byte v0, p0, Lcom/google/maps/g/qp;->e:B

    .line 3458
    iput v0, p0, Lcom/google/maps/g/qp;->f:I

    .line 3286
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    .line 3287
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    .line 3288
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3294
    invoke-direct {p0}, Lcom/google/maps/g/qp;-><init>()V

    .line 3295
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3299
    const/4 v0, 0x0

    .line 3300
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3301
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3302
    sparse-switch v3, :sswitch_data_0

    .line 3307
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3309
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3305
    goto :goto_0

    .line 3314
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3315
    iget v4, p0, Lcom/google/maps/g/qp;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/qp;->a:I

    .line 3316
    iput-object v3, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3327
    :catch_0
    move-exception v0

    .line 3328
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3333
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qp;->au:Lcom/google/n/bn;

    throw v0

    .line 3320
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3321
    iget v4, p0, Lcom/google/maps/g/qp;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/qp;->a:I

    .line 3322
    iput-object v3, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3329
    :catch_1
    move-exception v0

    .line 3330
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3331
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3333
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qp;->au:Lcom/google/n/bn;

    .line 3334
    return-void

    .line 3302
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3283
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3436
    iput-byte v0, p0, Lcom/google/maps/g/qp;->e:B

    .line 3458
    iput v0, p0, Lcom/google/maps/g/qp;->f:I

    .line 3284
    return-void
.end method

.method public static h()Lcom/google/maps/g/qp;
    .locals 1

    .prologue
    .line 3771
    sget-object v0, Lcom/google/maps/g/qp;->d:Lcom/google/maps/g/qp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/qr;
    .locals 1

    .prologue
    .line 3541
    new-instance v0, Lcom/google/maps/g/qr;

    invoke-direct {v0}, Lcom/google/maps/g/qr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3348
    sget-object v0, Lcom/google/maps/g/qp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 3448
    invoke-virtual {p0}, Lcom/google/maps/g/qp;->c()I

    .line 3449
    iget v0, p0, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3450
    iget-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3452
    :cond_0
    iget v0, p0, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3453
    iget-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3455
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/qp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3456
    return-void

    .line 3450
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 3453
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3438
    iget-byte v1, p0, Lcom/google/maps/g/qp;->e:B

    .line 3439
    if-ne v1, v0, :cond_0

    .line 3443
    :goto_0
    return v0

    .line 3440
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3442
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/qp;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3460
    iget v0, p0, Lcom/google/maps/g/qp;->f:I

    .line 3461
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3474
    :goto_0
    return v0

    .line 3464
    :cond_0
    iget v0, p0, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 3466
    iget-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 3468
    :goto_2
    iget v0, p0, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 3470
    iget-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 3472
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/qp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 3473
    iput v0, p0, Lcom/google/maps/g/qp;->f:I

    goto :goto_0

    .line 3466
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 3470
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3364
    iget-object v0, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    .line 3365
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3366
    check-cast v0, Ljava/lang/String;

    .line 3374
    :goto_0
    return-object v0

    .line 3368
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 3370
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 3371
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3372
    iput-object v1, p0, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3374
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3277
    invoke-static {}, Lcom/google/maps/g/qp;->newBuilder()Lcom/google/maps/g/qr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/qr;->a(Lcom/google/maps/g/qp;)Lcom/google/maps/g/qr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3277
    invoke-static {}, Lcom/google/maps/g/qp;->newBuilder()Lcom/google/maps/g/qr;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3406
    iget-object v0, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    .line 3407
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3408
    check-cast v0, Ljava/lang/String;

    .line 3416
    :goto_0
    return-object v0

    .line 3410
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 3412
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 3413
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3414
    iput-object v1, p0, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3416
    goto :goto_0
.end method
