.class public final Lcom/google/maps/g/a/fy;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/fw;",
        "Lcom/google/maps/g/a/fy;",
        ">;",
        "Lcom/google/maps/g/a/gb;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 398
    sget-object v0, Lcom/google/maps/g/a/fw;->e:Lcom/google/maps/g/a/fw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 473
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    .line 598
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    .line 722
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/fy;->c:I

    .line 399
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 600
    iget v0, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 601
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    .line 602
    iget v0, p0, Lcom/google/maps/g/a/fy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 604
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 438
    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 463
    :goto_0
    return-object p0

    .line 439
    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 440
    iget-object v1, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 441
    iget-object v1, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    .line 442
    iget v1, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 449
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 450
    iget-object v1, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 451
    iget-object v1, p1, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    .line 452
    iget v1, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 459
    :cond_2
    :goto_2
    iget v1, p1, Lcom/google/maps/g/a/fw;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 460
    iget v0, p1, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 444
    :cond_4
    invoke-virtual {p0}, Lcom/google/maps/g/a/fy;->d()V

    .line 445
    iget-object v1, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 454
    :cond_5
    invoke-direct {p0}, Lcom/google/maps/g/a/fy;->i()V

    .line 455
    iget-object v1, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 459
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 460
    :cond_7
    iget v1, p0, Lcom/google/maps/g/a/fy;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/fy;->a:I

    iget v0, v0, Lcom/google/maps/g/a/fz;->e:I

    iput v0, p0, Lcom/google/maps/g/a/fy;->c:I

    .line 462
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/a/fw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/google/maps/g/a/fy;->c()Lcom/google/maps/g/a/fw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 390
    check-cast p1, Lcom/google/maps/g/a/fw;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/fw;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 416
    new-instance v2, Lcom/google/maps/g/a/fw;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/fw;-><init>(Lcom/google/n/v;)V

    .line 417
    iget v3, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 418
    const/4 v1, 0x0

    .line 419
    iget v4, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 420
    iget-object v4, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    .line 421
    iget v4, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 423
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    .line 424
    iget v4, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 425
    iget-object v4, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    .line 426
    iget v4, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 428
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/fy;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 429
    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 432
    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/fy;->c:I

    iput v1, v2, Lcom/google/maps/g/a/fw;->d:I

    .line 433
    iput v0, v2, Lcom/google/maps/g/a/fw;->a:I

    .line 434
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 475
    iget v0, p0, Lcom/google/maps/g/a/fy;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 476
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/fy;->b:Ljava/util/List;

    .line 477
    iget v0, p0, Lcom/google/maps/g/a/fy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/fy;->a:I

    .line 479
    :cond_0
    return-void
.end method
