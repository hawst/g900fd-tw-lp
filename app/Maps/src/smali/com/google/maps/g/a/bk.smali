.class public final Lcom/google/maps/g/a/bk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/bi;",
        "Lcom/google/maps/g/a/bk;",
        ">;",
        "Lcom/google/maps/g/a/bl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 519
    sget-object v0, Lcom/google/maps/g/a/bi;->k:Lcom/google/maps/g/a/bi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 665
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->b:Lcom/google/n/ao;

    .line 724
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->c:Lcom/google/n/ao;

    .line 783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->d:Lcom/google/n/ao;

    .line 842
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->e:Lcom/google/n/ao;

    .line 901
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->f:Lcom/google/n/ao;

    .line 960
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->g:Lcom/google/n/ao;

    .line 1019
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    .line 1085
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    .line 520
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 607
    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 656
    :goto_0
    return-object p0

    .line 608
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 609
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 610
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 612
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 613
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 614
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 616
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 617
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 618
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 620
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 621
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 622
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 624
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 625
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 626
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 628
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 629
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 630
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 632
    :cond_6
    iget-object v2, p1, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 633
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 634
    iget-object v2, p1, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    .line 635
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 642
    :cond_7
    :goto_7
    iget-object v2, p1, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 643
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 644
    iget-object v2, p1, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    .line 645
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 652
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x40

    if-ne v2, v4, :cond_14

    :goto_9
    if-eqz v0, :cond_9

    .line 653
    iget-boolean v0, p1, Lcom/google/maps/g/a/bi;->j:Z

    iget v1, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/maps/g/a/bk;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/bk;->j:Z

    .line 655
    :cond_9
    iget-object v0, p1, Lcom/google/maps/g/a/bi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 608
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 612
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 616
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 620
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 624
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 628
    goto :goto_6

    .line 637
    :cond_10
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit8 v2, v2, 0x40

    if-eq v2, v4, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 638
    :cond_11
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    .line 647
    :cond_12
    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-eq v2, v3, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/bk;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 648
    :cond_13
    iget-object v2, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_14
    move v0, v1

    .line 652
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/google/maps/g/a/bk;->c()Lcom/google/maps/g/a/bi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 511
    check-cast p1, Lcom/google/maps/g/a/bi;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 660
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/bi;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 549
    new-instance v2, Lcom/google/maps/g/a/bi;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/bi;-><init>(Lcom/google/n/v;)V

    .line 550
    iget v3, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 552
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    .line 555
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->b:Lcom/google/n/ao;

    .line 556
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->b:Lcom/google/n/ao;

    .line 557
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 555
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 558
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 559
    or-int/lit8 v0, v0, 0x2

    .line 561
    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->c:Lcom/google/n/ao;

    .line 562
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->c:Lcom/google/n/ao;

    .line 563
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 561
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 564
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 565
    or-int/lit8 v0, v0, 0x4

    .line 567
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->d:Lcom/google/n/ao;

    .line 568
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->d:Lcom/google/n/ao;

    .line 569
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 567
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 570
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 571
    or-int/lit8 v0, v0, 0x8

    .line 573
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->e:Lcom/google/n/ao;

    .line 574
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->e:Lcom/google/n/ao;

    .line 575
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 573
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 576
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 577
    or-int/lit8 v0, v0, 0x10

    .line 579
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->f:Lcom/google/n/ao;

    .line 580
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->f:Lcom/google/n/ao;

    .line 581
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 579
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 582
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 583
    or-int/lit8 v0, v0, 0x20

    .line 585
    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bk;->g:Lcom/google/n/ao;

    .line 586
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bk;->g:Lcom/google/n/ao;

    .line 587
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 585
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 588
    iget v1, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 589
    iget-object v1, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    .line 590
    iget v1, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 592
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/a/bk;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 593
    iget v1, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 594
    iget-object v1, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    .line 595
    iget v1, p0, Lcom/google/maps/g/a/bk;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/g/a/bk;->a:I

    .line 597
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/a/bk;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 598
    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    .line 599
    or-int/lit8 v0, v0, 0x40

    .line 601
    :cond_7
    iget-boolean v1, p0, Lcom/google/maps/g/a/bk;->j:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/bi;->j:Z

    .line 602
    iput v0, v2, Lcom/google/maps/g/a/bi;->a:I

    .line 603
    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method
