.class public final enum Lcom/google/maps/g/en;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/en;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/en;

.field public static final enum b:Lcom/google/maps/g/en;

.field public static final enum c:Lcom/google/maps/g/en;

.field public static final enum d:Lcom/google/maps/g/en;

.field private static final synthetic f:[Lcom/google/maps/g/en;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 251
    new-instance v0, Lcom/google/maps/g/en;

    const-string v1, "GEOSTORE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/en;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/en;->a:Lcom/google/maps/g/en;

    .line 255
    new-instance v0, Lcom/google/maps/g/en;

    const-string v1, "VECTOR_DB"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/en;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/en;->b:Lcom/google/maps/g/en;

    .line 259
    new-instance v0, Lcom/google/maps/g/en;

    const-string v1, "LAT_LNG"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/en;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/en;->c:Lcom/google/maps/g/en;

    .line 263
    new-instance v0, Lcom/google/maps/g/en;

    const-string v1, "OBFUSCATED_GAIA_ID"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/en;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/en;->d:Lcom/google/maps/g/en;

    .line 246
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/en;

    sget-object v1, Lcom/google/maps/g/en;->a:Lcom/google/maps/g/en;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/en;->b:Lcom/google/maps/g/en;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/en;->c:Lcom/google/maps/g/en;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/en;->d:Lcom/google/maps/g/en;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/en;->f:[Lcom/google/maps/g/en;

    .line 303
    new-instance v0, Lcom/google/maps/g/eo;

    invoke-direct {v0}, Lcom/google/maps/g/eo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 313
    iput p3, p0, Lcom/google/maps/g/en;->e:I

    .line 314
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/en;
    .locals 1

    .prologue
    .line 289
    packed-switch p0, :pswitch_data_0

    .line 294
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 290
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/en;->a:Lcom/google/maps/g/en;

    goto :goto_0

    .line 291
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/en;->b:Lcom/google/maps/g/en;

    goto :goto_0

    .line 292
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/en;->c:Lcom/google/maps/g/en;

    goto :goto_0

    .line 293
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/en;->d:Lcom/google/maps/g/en;

    goto :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/en;
    .locals 1

    .prologue
    .line 246
    const-class v0, Lcom/google/maps/g/en;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/en;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/en;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/google/maps/g/en;->f:[Lcom/google/maps/g/en;

    invoke-virtual {v0}, [Lcom/google/maps/g/en;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/en;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/google/maps/g/en;->e:I

    return v0
.end method
