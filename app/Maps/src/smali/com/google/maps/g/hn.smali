.class public final Lcom/google/maps/g/hn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hn;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/hn;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/google/maps/a/a;

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/google/maps/g/ho;

    invoke-direct {v0}, Lcom/google/maps/g/ho;-><init>()V

    sput-object v0, Lcom/google/maps/g/hn;->PARSER:Lcom/google/n/ax;

    .line 697
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/hn;->j:Lcom/google/n/aw;

    .line 1062
    new-instance v0, Lcom/google/maps/g/hn;

    invoke-direct {v0}, Lcom/google/maps/g/hn;-><init>()V

    sput-object v0, Lcom/google/maps/g/hn;->g:Lcom/google/maps/g/hn;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 149
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 633
    iput-byte v1, p0, Lcom/google/maps/g/hn;->h:B

    .line 664
    iput v1, p0, Lcom/google/maps/g/hn;->i:I

    .line 150
    iput v0, p0, Lcom/google/maps/g/hn;->b:I

    .line 151
    iput v0, p0, Lcom/google/maps/g/hn;->c:I

    .line 152
    iput v0, p0, Lcom/google/maps/g/hn;->d:I

    .line 153
    iput v0, p0, Lcom/google/maps/g/hn;->f:I

    .line 154
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 160
    invoke-direct {p0}, Lcom/google/maps/g/hn;-><init>()V

    .line 161
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 165
    const/4 v0, 0x0

    move v2, v0

    .line 166
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 167
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 168
    sparse-switch v0, :sswitch_data_0

    .line 173
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 175
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 171
    goto :goto_0

    .line 180
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 181
    invoke-static {v0}, Lcom/google/maps/g/hs;->a(I)Lcom/google/maps/g/hs;

    move-result-object v1

    .line 182
    if-nez v1, :cond_1

    .line 183
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/hn;->au:Lcom/google/n/bn;

    throw v0

    .line 185
    :cond_1
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/hn;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/hn;->a:I

    .line 186
    iput v0, p0, Lcom/google/maps/g/hn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 240
    :catch_1
    move-exception v0

    .line 241
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 242
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 192
    invoke-static {v0}, Lcom/google/maps/g/hq;->a(I)Lcom/google/maps/g/hq;

    move-result-object v1

    .line 193
    if-nez v1, :cond_2

    .line 194
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 196
    :cond_2
    iget v1, p0, Lcom/google/maps/g/hn;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/hn;->a:I

    .line 197
    iput v0, p0, Lcom/google/maps/g/hn;->c:I

    goto :goto_0

    .line 202
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 203
    invoke-static {v0}, Lcom/google/maps/g/hu;->a(I)Lcom/google/maps/g/hu;

    move-result-object v1

    .line 204
    if-nez v1, :cond_3

    .line 205
    const/4 v1, 0x3

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 207
    :cond_3
    iget v1, p0, Lcom/google/maps/g/hn;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/hn;->a:I

    .line 208
    iput v0, p0, Lcom/google/maps/g/hn;->d:I

    goto :goto_0

    .line 213
    :sswitch_4
    const/4 v0, 0x0

    .line 214
    iget v1, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v5, 0x8

    if-ne v1, v5, :cond_7

    .line 215
    iget-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v0

    move-object v1, v0

    .line 217
    :goto_1
    sget-object v0, Lcom/google/maps/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    .line 218
    if-eqz v1, :cond_4

    .line 219
    iget-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    invoke-virtual {v1, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    .line 220
    invoke-virtual {v1}, Lcom/google/maps/a/c;->c()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    .line 222
    :cond_4
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/hn;->a:I

    goto/16 :goto_0

    .line 226
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 227
    invoke-static {v0}, Lcom/google/maps/g/op;->a(I)Lcom/google/maps/g/op;

    move-result-object v1

    .line 228
    if-nez v1, :cond_5

    .line 229
    const/4 v1, 0x5

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 231
    :cond_5
    iget v1, p0, Lcom/google/maps/g/hn;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/hn;->a:I

    .line 232
    iput v0, p0, Lcom/google/maps/g/hn;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 244
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/hn;->au:Lcom/google/n/bn;

    .line 245
    return-void

    :cond_7
    move-object v1, v0

    goto :goto_1

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 147
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 633
    iput-byte v0, p0, Lcom/google/maps/g/hn;->h:B

    .line 664
    iput v0, p0, Lcom/google/maps/g/hn;->i:I

    .line 148
    return-void
.end method

.method public static d()Lcom/google/maps/g/hn;
    .locals 1

    .prologue
    .line 1065
    sget-object v0, Lcom/google/maps/g/hn;->g:Lcom/google/maps/g/hn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/hp;
    .locals 1

    .prologue
    .line 759
    new-instance v0, Lcom/google/maps/g/hp;

    invoke-direct {v0}, Lcom/google/maps/g/hp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/hn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    sget-object v0, Lcom/google/maps/g/hn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 645
    invoke-virtual {p0}, Lcom/google/maps/g/hn;->c()I

    .line 646
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 647
    iget v0, p0, Lcom/google/maps/g/hn;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 649
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 650
    iget v0, p0, Lcom/google/maps/g/hn;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 652
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 653
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/hn;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 655
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 656
    iget-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_3
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 658
    :cond_3
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 659
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/hn;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 661
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/hn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 662
    return-void

    .line 647
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 650
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 653
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 656
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    goto :goto_3

    .line 659
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 635
    iget-byte v1, p0, Lcom/google/maps/g/hn;->h:B

    .line 636
    if-ne v1, v0, :cond_0

    .line 640
    :goto_0
    return v0

    .line 637
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 639
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/hn;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 666
    iget v0, p0, Lcom/google/maps/g/hn;->i:I

    .line 667
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 692
    :goto_0
    return v0

    .line 670
    :cond_0
    iget v0, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 671
    iget v0, p0, Lcom/google/maps/g/hn;->b:I

    .line 672
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 674
    :goto_2
    iget v3, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 675
    iget v3, p0, Lcom/google/maps/g/hn;->c:I

    .line 676
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 678
    :cond_1
    iget v3, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 679
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/g/hn;->d:I

    .line 680
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 682
    :cond_2
    iget v3, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 684
    iget-object v3, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    if-nez v3, :cond_9

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v3

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v3}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v3, v5

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 686
    :cond_3
    iget v3, p0, Lcom/google/maps/g/hn;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_5

    .line 687
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/maps/g/hn;->f:I

    .line 688
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_4
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 690
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/hn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    iput v0, p0, Lcom/google/maps/g/hn;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 672
    goto :goto_1

    :cond_7
    move v3, v1

    .line 676
    goto :goto_3

    :cond_8
    move v3, v1

    .line 680
    goto :goto_4

    .line 684
    :cond_9
    iget-object v3, p0, Lcom/google/maps/g/hn;->e:Lcom/google/maps/a/a;

    goto :goto_5

    :cond_a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 141
    invoke-static {}, Lcom/google/maps/g/hn;->newBuilder()Lcom/google/maps/g/hp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/hp;->a(Lcom/google/maps/g/hn;)Lcom/google/maps/g/hp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 141
    invoke-static {}, Lcom/google/maps/g/hn;->newBuilder()Lcom/google/maps/g/hp;

    move-result-object v0

    return-object v0
.end method
