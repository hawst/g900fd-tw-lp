.class public final Lcom/google/maps/g/a/aw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/au;",
        "Lcom/google/maps/g/a/aw;",
        ">;",
        "Lcom/google/maps/g/a/bd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/f;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1064
    sget-object v0, Lcom/google/maps/g/a/au;->k:Lcom/google/maps/g/a/au;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1194
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->b:Lcom/google/n/ao;

    .line 1253
    iput v1, p0, Lcom/google/maps/g/a/aw;->c:I

    .line 1289
    iput v1, p0, Lcom/google/maps/g/a/aw;->d:I

    .line 1325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->e:Lcom/google/n/ao;

    .line 1384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->f:Lcom/google/n/ao;

    .line 1443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->g:Lcom/google/n/ao;

    .line 1502
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->h:Lcom/google/n/f;

    .line 1537
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->i:Lcom/google/n/ao;

    .line 1596
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aw;->j:Lcom/google/n/ao;

    .line 1065
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1150
    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1185
    :goto_0
    return-object p0

    .line 1151
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1152
    iget-object v2, p0, Lcom/google/maps/g/a/aw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1153
    iget v2, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1155
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 1156
    iget v2, p1, Lcom/google/maps/g/a/au;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/bb;->a(I)Lcom/google/maps/g/a/bb;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/a/bb;->a:Lcom/google/maps/g/a/bb;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 1151
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1155
    goto :goto_2

    .line 1156
    :cond_5
    iget v3, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/aw;->a:I

    iget v2, v2, Lcom/google/maps/g/a/bb;->e:I

    iput v2, p0, Lcom/google/maps/g/a/aw;->c:I

    .line 1158
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 1159
    iget v2, p1, Lcom/google/maps/g/a/au;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_7
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 1158
    goto :goto_3

    .line 1159
    :cond_9
    iget v3, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/aw;->a:I

    iget v2, v2, Lcom/google/maps/g/a/fz;->e:I

    iput v2, p0, Lcom/google/maps/g/a/aw;->d:I

    .line 1161
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 1162
    iget-object v2, p0, Lcom/google/maps/g/a/aw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1163
    iget v2, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1165
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1166
    iget-object v2, p0, Lcom/google/maps/g/a/aw;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1167
    iget v2, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1169
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 1170
    iget-object v2, p0, Lcom/google/maps/g/a/aw;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1171
    iget v2, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1173
    :cond_d
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_13

    .line 1174
    iget-object v2, p1, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    if-nez v2, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 1161
    goto :goto_4

    :cond_f
    move v2, v1

    .line 1165
    goto :goto_5

    :cond_10
    move v2, v1

    .line 1169
    goto :goto_6

    :cond_11
    move v2, v1

    .line 1173
    goto :goto_7

    .line 1174
    :cond_12
    iget v3, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/aw;->a:I

    iput-object v2, p0, Lcom/google/maps/g/a/aw;->h:Lcom/google/n/f;

    .line 1176
    :cond_13
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_14

    .line 1177
    iget-object v2, p0, Lcom/google/maps/g/a/aw;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1178
    iget v2, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1180
    :cond_14
    iget v2, p1, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    :goto_9
    if-eqz v0, :cond_15

    .line 1181
    iget-object v0, p0, Lcom/google/maps/g/a/aw;->j:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1182
    iget v0, p0, Lcom/google/maps/g/a/aw;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1184
    :cond_15
    iget-object v0, p1, Lcom/google/maps/g/a/au;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_16
    move v2, v1

    .line 1176
    goto :goto_8

    :cond_17
    move v0, v1

    .line 1180
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1056
    invoke-virtual {p0}, Lcom/google/maps/g/a/aw;->c()Lcom/google/maps/g/a/au;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1056
    check-cast p1, Lcom/google/maps/g/a/au;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1189
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/au;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1094
    new-instance v2, Lcom/google/maps/g/a/au;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/au;-><init>(Lcom/google/n/v;)V

    .line 1095
    iget v3, p0, Lcom/google/maps/g/a/aw;->a:I

    .line 1097
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    .line 1100
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->b:Lcom/google/n/ao;

    .line 1101
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aw;->b:Lcom/google/n/ao;

    .line 1102
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1100
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1103
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1104
    or-int/lit8 v0, v0, 0x2

    .line 1106
    :cond_0
    iget v4, p0, Lcom/google/maps/g/a/aw;->c:I

    iput v4, v2, Lcom/google/maps/g/a/au;->c:I

    .line 1107
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1108
    or-int/lit8 v0, v0, 0x4

    .line 1110
    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/aw;->d:I

    iput v4, v2, Lcom/google/maps/g/a/au;->d:I

    .line 1111
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1112
    or-int/lit8 v0, v0, 0x8

    .line 1114
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->e:Lcom/google/n/ao;

    .line 1115
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aw;->e:Lcom/google/n/ao;

    .line 1116
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1114
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1117
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 1118
    or-int/lit8 v0, v0, 0x10

    .line 1120
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->f:Lcom/google/n/ao;

    .line 1121
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aw;->f:Lcom/google/n/ao;

    .line 1122
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1120
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1123
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 1124
    or-int/lit8 v0, v0, 0x20

    .line 1126
    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->g:Lcom/google/n/ao;

    .line 1127
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aw;->g:Lcom/google/n/ao;

    .line 1128
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1126
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1129
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 1130
    or-int/lit8 v0, v0, 0x40

    .line 1132
    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/a/aw;->h:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    .line 1133
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 1134
    or-int/lit16 v0, v0, 0x80

    .line 1136
    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->i:Lcom/google/n/ao;

    .line 1137
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aw;->i:Lcom/google/n/ao;

    .line 1138
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1136
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1139
    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    .line 1140
    or-int/lit16 v0, v0, 0x100

    .line 1142
    :cond_7
    iget-object v3, v2, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/aw;->j:Lcom/google/n/ao;

    .line 1143
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/aw;->j:Lcom/google/n/ao;

    .line 1144
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1142
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 1145
    iput v0, v2, Lcom/google/maps/g/a/au;->a:I

    .line 1146
    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method
