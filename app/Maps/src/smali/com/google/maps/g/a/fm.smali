.class public final Lcom/google/maps/g/a/fm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/fk;",
        "Lcom/google/maps/g/a/fm;",
        ">;",
        "Lcom/google/maps/g/a/fn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/maps/g/a/ai;

.field private e:Lcom/google/maps/g/a/be;

.field private f:Lcom/google/maps/g/a/ek;

.field private g:Lcom/google/maps/g/a/gy;

.field private h:Lcom/google/maps/g/a/cc;

.field private i:Z

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/maps/g/a/au;

.field private l:Lcom/google/maps/g/a/bq;

.field private m:Lcom/google/maps/g/a/cs;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 692
    sget-object v0, Lcom/google/maps/g/a/fk;->o:Lcom/google/maps/g/a/fk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 864
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/fm;->b:I

    .line 900
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->c:Ljava/lang/Object;

    .line 976
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    .line 1037
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    .line 1098
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    .line 1159
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    .line 1220
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    .line 1314
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    .line 1438
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    .line 1499
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    .line 1560
    iput-object v1, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    .line 1622
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    .line 693
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 792
    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 849
    :goto_0
    return-object p0

    .line 793
    :cond_0
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 794
    iget v0, p1, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 793
    goto :goto_1

    .line 794
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/fm;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hm;->h:I

    iput v0, p0, Lcom/google/maps/g/a/fm;->b:I

    .line 796
    :cond_4
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_11

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 797
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 798
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->c:Ljava/lang/Object;

    .line 801
    :cond_5
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_12

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 802
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_4
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v4, :cond_14

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    if-eq v3, v4, :cond_14

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    invoke-static {v3}, Lcom/google/maps/g/a/ai;->a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/ak;->a(Lcom/google/maps/g/a/ai;)Lcom/google/maps/g/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/ak;->c()Lcom/google/maps/g/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 804
    :cond_6
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_15

    move v0, v1

    :goto_6
    if-eqz v0, :cond_7

    .line 805
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_7
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v5, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    if-eq v3, v4, :cond_17

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    invoke-static {v3}, Lcom/google/maps/g/a/be;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    :goto_8
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 807
    :cond_7
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_18

    move v0, v1

    :goto_9
    if-eqz v0, :cond_8

    .line 808
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v0, :cond_19

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v0

    :goto_a
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v3, v3, 0x10

    if-ne v3, v6, :cond_1a

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    if-eqz v3, :cond_1a

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v4

    if-eq v3, v4, :cond_1a

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    invoke-static {v3}, Lcom/google/maps/g/a/ek;->a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/em;->a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/em;->c()Lcom/google/maps/g/a/ek;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    :goto_b
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 810
    :cond_8
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_c
    if-eqz v0, :cond_9

    .line 811
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v0, :cond_1c

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    :goto_d
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1d

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    if-eqz v3, :cond_1d

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v4

    if-eq v3, v4, :cond_1d

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    invoke-static {v3}, Lcom/google/maps/g/a/gy;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/ha;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/ha;->c()Lcom/google/maps/g/a/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    :goto_e
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 813
    :cond_9
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1e

    move v0, v1

    :goto_f
    if-eqz v0, :cond_a

    .line 814
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    if-nez v0, :cond_1f

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v0

    :goto_10
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_20

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v4

    if-eq v3, v4, :cond_20

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    invoke-static {v3}, Lcom/google/maps/g/a/cc;->a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/ce;->a(Lcom/google/maps/g/a/cc;)Lcom/google/maps/g/a/ce;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/ce;->c()Lcom/google/maps/g/a/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    :goto_11
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 816
    :cond_a
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_21

    move v0, v1

    :goto_12
    if-eqz v0, :cond_b

    .line 817
    iget-boolean v0, p1, Lcom/google/maps/g/a/fk;->i:Z

    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/fm;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/fm;->i:Z

    .line 819
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 820
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 821
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    .line 822
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 829
    :cond_c
    :goto_13
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_24

    move v0, v1

    :goto_14
    if-eqz v0, :cond_d

    .line 830
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v0, :cond_25

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_15
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_26

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    if-eqz v3, :cond_26

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v4

    if-eq v3, v4, :cond_26

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    invoke-static {v3}, Lcom/google/maps/g/a/au;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/aw;->c()Lcom/google/maps/g/a/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    :goto_16
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 832
    :cond_d
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_27

    move v0, v1

    :goto_17
    if-eqz v0, :cond_e

    .line 833
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    if-nez v0, :cond_28

    invoke-static {}, Lcom/google/maps/g/a/bq;->d()Lcom/google/maps/g/a/bq;

    move-result-object v0

    :goto_18
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_29

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    if-eqz v3, :cond_29

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    invoke-static {}, Lcom/google/maps/g/a/bq;->d()Lcom/google/maps/g/a/bq;

    move-result-object v4

    if-eq v3, v4, :cond_29

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    invoke-static {v3}, Lcom/google/maps/g/a/bq;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/bs;->c()Lcom/google/maps/g/a/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    :goto_19
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 835
    :cond_e
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_2a

    move v0, v1

    :goto_1a
    if-eqz v0, :cond_f

    .line 836
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    if-nez v0, :cond_2b

    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v0

    :goto_1b
    iget v1, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_2c

    iget-object v1, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    if-eqz v1, :cond_2c

    iget-object v1, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v2

    if-eq v1, v2, :cond_2c

    iget-object v1, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    invoke-static {v1}, Lcom/google/maps/g/a/cs;->a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/cu;->a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/cu;->c()Lcom/google/maps/g/a/cs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    :goto_1c
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 838
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 839
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 840
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    .line 841
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 848
    :cond_10
    :goto_1d
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v0, v2

    .line 796
    goto/16 :goto_2

    :cond_12
    move v0, v2

    .line 801
    goto/16 :goto_3

    .line 802
    :cond_13
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_4

    :cond_14
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_5

    :cond_15
    move v0, v2

    .line 804
    goto/16 :goto_6

    .line 805
    :cond_16
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_7

    :cond_17
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_8

    :cond_18
    move v0, v2

    .line 807
    goto/16 :goto_9

    .line 808
    :cond_19
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_a

    :cond_1a
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_b

    :cond_1b
    move v0, v2

    .line 810
    goto/16 :goto_c

    .line 811
    :cond_1c
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto/16 :goto_d

    :cond_1d
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    goto/16 :goto_e

    :cond_1e
    move v0, v2

    .line 813
    goto/16 :goto_f

    .line 814
    :cond_1f
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    goto/16 :goto_10

    :cond_20
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    goto/16 :goto_11

    :cond_21
    move v0, v2

    .line 816
    goto/16 :goto_12

    .line 824
    :cond_22
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-eq v0, v3, :cond_23

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 825
    :cond_23
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_13

    :cond_24
    move v0, v2

    .line 829
    goto/16 :goto_14

    .line 830
    :cond_25
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto/16 :goto_15

    :cond_26
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    goto/16 :goto_16

    :cond_27
    move v0, v2

    .line 832
    goto/16 :goto_17

    .line 833
    :cond_28
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    goto/16 :goto_18

    :cond_29
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    goto/16 :goto_19

    :cond_2a
    move v0, v2

    .line 835
    goto/16 :goto_1a

    .line 836
    :cond_2b
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    goto/16 :goto_1b

    :cond_2c
    iput-object v0, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    goto/16 :goto_1c

    .line 843
    :cond_2d
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-eq v0, v1, :cond_2e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 844
    :cond_2e
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/google/maps/g/a/fm;->c()Lcom/google/maps/g/a/fk;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 684
    check-cast p1, Lcom/google/maps/g/a/fk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 853
    iget v0, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 854
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/a/cc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 859
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 853
    goto :goto_0

    .line 854
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 859
    goto :goto_2
.end method

.method public final c()Lcom/google/maps/g/a/fk;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 730
    new-instance v2, Lcom/google/maps/g/a/fk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/fk;-><init>(Lcom/google/n/v;)V

    .line 731
    iget v3, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 732
    const/4 v1, 0x0

    .line 733
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    .line 736
    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/fm;->b:I

    iput v1, v2, Lcom/google/maps/g/a/fk;->b:I

    .line 737
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 738
    or-int/lit8 v0, v0, 0x2

    .line 740
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->c:Ljava/lang/Object;

    .line 741
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 742
    or-int/lit8 v0, v0, 0x4

    .line 744
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->d:Lcom/google/maps/g/a/ai;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    .line 745
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 746
    or-int/lit8 v0, v0, 0x8

    .line 748
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->e:Lcom/google/maps/g/a/be;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    .line 749
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 750
    or-int/lit8 v0, v0, 0x10

    .line 752
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->f:Lcom/google/maps/g/a/ek;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    .line 753
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 754
    or-int/lit8 v0, v0, 0x20

    .line 756
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->g:Lcom/google/maps/g/a/gy;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    .line 757
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 758
    or-int/lit8 v0, v0, 0x40

    .line 760
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->h:Lcom/google/maps/g/a/cc;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->h:Lcom/google/maps/g/a/cc;

    .line 761
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 762
    or-int/lit16 v0, v0, 0x80

    .line 764
    :cond_6
    iget-boolean v1, p0, Lcom/google/maps/g/a/fm;->i:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/fk;->i:Z

    .line 765
    iget v1, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 766
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    .line 767
    iget v1, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 769
    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    .line 770
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 771
    or-int/lit16 v0, v0, 0x100

    .line 773
    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->k:Lcom/google/maps/g/a/au;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    .line 774
    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 775
    or-int/lit16 v0, v0, 0x200

    .line 777
    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->l:Lcom/google/maps/g/a/bq;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->l:Lcom/google/maps/g/a/bq;

    .line 778
    and-int/lit16 v1, v3, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    .line 779
    or-int/lit16 v0, v0, 0x400

    .line 781
    :cond_a
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->m:Lcom/google/maps/g/a/cs;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    .line 782
    iget v1, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    .line 783
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    .line 784
    iget v1, p0, Lcom/google/maps/g/a/fm;->a:I

    and-int/lit16 v1, v1, -0x1001

    iput v1, p0, Lcom/google/maps/g/a/fm;->a:I

    .line 786
    :cond_b
    iget-object v1, p0, Lcom/google/maps/g/a/fm;->n:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    .line 787
    iput v0, v2, Lcom/google/maps/g/a/fk;->a:I

    .line 788
    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method
