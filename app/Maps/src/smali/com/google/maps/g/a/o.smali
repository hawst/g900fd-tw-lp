.class public final Lcom/google/maps/g/a/o;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/r;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/o;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/a/o;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Z

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field g:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/google/maps/g/a/p;

    invoke-direct {v0}, Lcom/google/maps/g/a/p;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/o;->PARSER:Lcom/google/n/ax;

    .line 380
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/o;->k:Lcom/google/n/aw;

    .line 955
    new-instance v0, Lcom/google/maps/g/a/o;

    invoke-direct {v0}, Lcom/google/maps/g/a/o;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/o;->h:Lcom/google/maps/g/a/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 252
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    .line 309
    iput-byte v2, p0, Lcom/google/maps/g/a/o;->i:B

    .line 343
    iput v2, p0, Lcom/google/maps/g/a/o;->j:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/a/o;->c:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/a/o;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 36
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 38
    sparse-switch v0, :sswitch_data_0

    .line 43
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 51
    iget v5, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/o;->a:I

    .line 52
    iput-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/o;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/o;->a:I

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/o;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 89
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 57
    goto :goto_1

    .line 61
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget v5, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/a/o;->a:I

    .line 63
    iput-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 68
    iget v5, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/a/o;->a:I

    .line 69
    iput-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    goto :goto_0

    .line 73
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 74
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/o;->a:I

    goto :goto_0

    .line 78
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 79
    iget v5, p0, Lcom/google/maps/g/a/o;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/a/o;->a:I

    .line 80
    iput-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 91
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->au:Lcom/google/n/bn;

    .line 92
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 252
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    .line 309
    iput-byte v1, p0, Lcom/google/maps/g/a/o;->i:B

    .line 343
    iput v1, p0, Lcom/google/maps/g/a/o;->j:I

    .line 16
    return-void
.end method

.method public static i()Lcom/google/maps/g/a/o;
    .locals 1

    .prologue
    .line 958
    sget-object v0, Lcom/google/maps/g/a/o;->h:Lcom/google/maps/g/a/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/q;
    .locals 1

    .prologue
    .line 442
    new-instance v0, Lcom/google/maps/g/a/q;

    invoke-direct {v0}, Lcom/google/maps/g/a/q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lcom/google/maps/g/a/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x2

    .line 321
    invoke-virtual {p0}, Lcom/google/maps/g/a/o;->c()I

    .line 322
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 325
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 326
    iget-boolean v0, p0, Lcom/google/maps/g/a/o;->c:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 328
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 329
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 331
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 332
    iget-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 334
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 335
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 337
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 338
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 340
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 341
    return-void

    .line 323
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 326
    goto/16 :goto_1

    .line 329
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 332
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 338
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 311
    iget-byte v1, p0, Lcom/google/maps/g/a/o;->i:B

    .line 312
    if-ne v1, v0, :cond_0

    .line 316
    :goto_0
    return v0

    .line 313
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 315
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/o;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 345
    iget v0, p0, Lcom/google/maps/g/a/o;->j:I

    .line 346
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 375
    :goto_0
    return v0

    .line 349
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 351
    iget-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 353
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    .line 354
    iget-boolean v2, p0, Lcom/google/maps/g/a/o;->c:Z

    .line 355
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 357
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 358
    const/4 v3, 0x3

    .line 359
    iget-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 361
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 363
    iget-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 365
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 366
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    .line 367
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 369
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 370
    const/4 v3, 0x6

    .line 371
    iget-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 373
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/a/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 374
    iput v0, p0, Lcom/google/maps/g/a/o;->j:I

    goto/16 :goto_0

    .line 351
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 359
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 363
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 371
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_9
    move v2, v0

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    .line 123
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 124
    check-cast v0, Ljava/lang/String;

    .line 132
    :goto_0
    return-object v0

    .line 126
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 128
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 129
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iput-object v1, p0, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 132
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/o;->newBuilder()Lcom/google/maps/g/a/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/q;->a(Lcom/google/maps/g/a/o;)Lcom/google/maps/g/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/o;->newBuilder()Lcom/google/maps/g/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    .line 180
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 181
    check-cast v0, Ljava/lang/String;

    .line 189
    :goto_0
    return-object v0

    .line 183
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 185
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iput-object v1, p0, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 189
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    .line 222
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 223
    check-cast v0, Ljava/lang/String;

    .line 231
    :goto_0
    return-object v0

    .line 225
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 227
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    iput-object v1, p0, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 231
    goto :goto_0
.end method
