.class public final Lcom/google/maps/g/qr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/qs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/qp;",
        "Lcom/google/maps/g/qr;",
        ">;",
        "Lcom/google/maps/g/qs;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3559
    sget-object v0, Lcom/google/maps/g/qp;->d:Lcom/google/maps/g/qp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3612
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qr;->c:Ljava/lang/Object;

    .line 3688
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qr;->b:Ljava/lang/Object;

    .line 3560
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qp;)Lcom/google/maps/g/qr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3591
    invoke-static {}, Lcom/google/maps/g/qp;->h()Lcom/google/maps/g/qp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3603
    :goto_0
    return-object p0

    .line 3592
    :cond_0
    iget v2, p1, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3593
    iget v2, p0, Lcom/google/maps/g/qr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/qr;->a:I

    .line 3594
    iget-object v2, p1, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/qr;->c:Ljava/lang/Object;

    .line 3597
    :cond_1
    iget v2, p1, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 3598
    iget v0, p0, Lcom/google/maps/g/qr;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/qr;->a:I

    .line 3599
    iget-object v0, p1, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/qr;->b:Ljava/lang/Object;

    .line 3602
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/qp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 3592
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3597
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3551
    new-instance v2, Lcom/google/maps/g/qp;

    invoke-direct {v2, p0}, Lcom/google/maps/g/qp;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/qr;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/qr;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/qp;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/qr;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/qp;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/qp;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3551
    check-cast p1, Lcom/google/maps/g/qp;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/qr;->a(Lcom/google/maps/g/qp;)Lcom/google/maps/g/qr;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3607
    const/4 v0, 0x1

    return v0
.end method
