.class public final Lcom/google/maps/g/fe;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ff;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/fc;",
        "Lcom/google/maps/g/fe;",
        ">;",
        "Lcom/google/maps/g/ff;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 375
    sget-object v0, Lcom/google/maps/g/fc;->f:Lcom/google/maps/g/fc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 448
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fe;->c:Ljava/lang/Object;

    .line 524
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fe;->d:Ljava/lang/Object;

    .line 600
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fe;->b:Ljava/lang/Object;

    .line 376
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 419
    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 439
    :goto_0
    return-object p0

    .line 420
    :cond_0
    iget v2, p1, Lcom/google/maps/g/fc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 421
    iget v2, p0, Lcom/google/maps/g/fe;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/fe;->a:I

    .line 422
    iget-object v2, p1, Lcom/google/maps/g/fc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fe;->c:Ljava/lang/Object;

    .line 425
    :cond_1
    iget v2, p1, Lcom/google/maps/g/fc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 426
    iget v2, p0, Lcom/google/maps/g/fe;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/fe;->a:I

    .line 427
    iget-object v2, p1, Lcom/google/maps/g/fc;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fe;->d:Ljava/lang/Object;

    .line 430
    :cond_2
    iget v2, p1, Lcom/google/maps/g/fc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 431
    iget v2, p0, Lcom/google/maps/g/fe;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/fe;->a:I

    .line 432
    iget-object v2, p1, Lcom/google/maps/g/fc;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fe;->b:Ljava/lang/Object;

    .line 435
    :cond_3
    iget v2, p1, Lcom/google/maps/g/fc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 436
    iget v0, p1, Lcom/google/maps/g/fc;->e:I

    iget v1, p0, Lcom/google/maps/g/fe;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/fe;->a:I

    iput v0, p0, Lcom/google/maps/g/fe;->e:I

    .line 438
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/fc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 420
    goto :goto_1

    :cond_6
    move v2, v1

    .line 425
    goto :goto_2

    :cond_7
    move v2, v1

    .line 430
    goto :goto_3

    :cond_8
    move v0, v1

    .line 435
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/maps/g/fe;->c()Lcom/google/maps/g/fc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 367
    check-cast p1, Lcom/google/maps/g/fc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/fe;->a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/fc;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 395
    new-instance v2, Lcom/google/maps/g/fc;

    invoke-direct {v2, p0}, Lcom/google/maps/g/fc;-><init>(Lcom/google/n/v;)V

    .line 396
    iget v3, p0, Lcom/google/maps/g/fe;->a:I

    .line 397
    const/4 v1, 0x0

    .line 398
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 401
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/fe;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fc;->b:Ljava/lang/Object;

    .line 402
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 403
    or-int/lit8 v0, v0, 0x2

    .line 405
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/fe;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fc;->c:Ljava/lang/Object;

    .line 406
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 407
    or-int/lit8 v0, v0, 0x4

    .line 409
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/fe;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fc;->d:Ljava/lang/Object;

    .line 410
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 411
    or-int/lit8 v0, v0, 0x8

    .line 413
    :cond_2
    iget v1, p0, Lcom/google/maps/g/fe;->e:I

    iput v1, v2, Lcom/google/maps/g/fc;->e:I

    .line 414
    iput v0, v2, Lcom/google/maps/g/fc;->a:I

    .line 415
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
