.class public final Lcom/google/maps/g/tx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ua;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tx;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/tx;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 370
    new-instance v0, Lcom/google/maps/g/ty;

    invoke-direct {v0}, Lcom/google/maps/g/ty;-><init>()V

    sput-object v0, Lcom/google/maps/g/tx;->PARSER:Lcom/google/n/ax;

    .line 562
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/tx;->h:Lcom/google/n/aw;

    .line 938
    new-instance v0, Lcom/google/maps/g/tx;

    invoke-direct {v0}, Lcom/google/maps/g/tx;-><init>()V

    sput-object v0, Lcom/google/maps/g/tx;->e:Lcom/google/maps/g/tx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 312
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 512
    iput-byte v0, p0, Lcom/google/maps/g/tx;->f:B

    .line 537
    iput v0, p0, Lcom/google/maps/g/tx;->g:I

    .line 313
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    .line 314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    .line 315
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    .line 316
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 322
    invoke-direct {p0}, Lcom/google/maps/g/tx;-><init>()V

    .line 323
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 327
    const/4 v0, 0x0

    .line 328
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 329
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 330
    sparse-switch v3, :sswitch_data_0

    .line 335
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 337
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 333
    goto :goto_0

    .line 342
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 343
    iget v4, p0, Lcom/google/maps/g/tx;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/tx;->a:I

    .line 344
    iput-object v3, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 361
    :catch_0
    move-exception v0

    .line 362
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/tx;->au:Lcom/google/n/bn;

    throw v0

    .line 348
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 349
    iget v4, p0, Lcom/google/maps/g/tx;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/tx;->a:I

    .line 350
    iput-object v3, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 363
    :catch_1
    move-exception v0

    .line 364
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 365
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 354
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 355
    iget v4, p0, Lcom/google/maps/g/tx;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/tx;->a:I

    .line 356
    iput-object v3, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 367
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->au:Lcom/google/n/bn;

    .line 368
    return-void

    .line 330
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 310
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 512
    iput-byte v0, p0, Lcom/google/maps/g/tx;->f:B

    .line 537
    iput v0, p0, Lcom/google/maps/g/tx;->g:I

    .line 311
    return-void
.end method

.method public static a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;
    .locals 1

    .prologue
    .line 627
    invoke-static {}, Lcom/google/maps/g/tx;->newBuilder()Lcom/google/maps/g/tz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/tz;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/tx;
    .locals 1

    .prologue
    .line 941
    sget-object v0, Lcom/google/maps/g/tx;->e:Lcom/google/maps/g/tx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/tz;
    .locals 1

    .prologue
    .line 624
    new-instance v0, Lcom/google/maps/g/tz;

    invoke-direct {v0}, Lcom/google/maps/g/tz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382
    sget-object v0, Lcom/google/maps/g/tx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 524
    invoke-virtual {p0}, Lcom/google/maps/g/tx;->c()I

    .line 525
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 528
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 529
    iget-object v0, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 531
    :cond_1
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    .line 532
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 534
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/tx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 535
    return-void

    .line 526
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 529
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 532
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 514
    iget-byte v1, p0, Lcom/google/maps/g/tx;->f:B

    .line 515
    if-ne v1, v0, :cond_0

    .line 519
    :goto_0
    return v0

    .line 516
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/tx;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 539
    iget v0, p0, Lcom/google/maps/g/tx;->g:I

    .line 540
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 557
    :goto_0
    return v0

    .line 543
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 545
    iget-object v0, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 547
    :goto_2
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 549
    iget-object v0, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 551
    :cond_1
    iget v0, p0, Lcom/google/maps/g/tx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 552
    const/4 v3, 0x3

    .line 553
    iget-object v0, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tx;->b:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 555
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/tx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 556
    iput v0, p0, Lcom/google/maps/g/tx;->g:I

    goto/16 :goto_0

    .line 545
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 549
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 553
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcom/google/maps/g/tx;->newBuilder()Lcom/google/maps/g/tz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/tz;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcom/google/maps/g/tx;->newBuilder()Lcom/google/maps/g/tz;

    move-result-object v0

    return-object v0
.end method
