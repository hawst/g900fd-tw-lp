.class public final Lcom/google/maps/g/a/y;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/w;",
        "Lcom/google/maps/g/a/y;",
        ">;",
        "Lcom/google/maps/g/a/ab;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:I

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/n/f;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Ljava/lang/Object;

.field private q:Ljava/lang/Object;

.field private r:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 975
    sget-object v0, Lcom/google/maps/g/a/w;->s:Lcom/google/maps/g/a/w;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    .line 1351
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    .line 1583
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/y;->g:Lcom/google/n/ao;

    .line 1642
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/y;->h:I

    .line 1678
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/y;->i:Lcom/google/n/ao;

    .line 1737
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/y;->j:Lcom/google/n/ao;

    .line 1829
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    .line 1965
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/y;->m:Lcom/google/n/f;

    .line 2001
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    .line 2169
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/y;->p:Ljava/lang/Object;

    .line 2245
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/y;->q:Ljava/lang/Object;

    .line 2321
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/y;->r:Lcom/google/n/ao;

    .line 976
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/w;)Lcom/google/maps/g/a/y;
    .locals 7

    .prologue
    const/16 v6, 0x1000

    const/16 v5, 0x400

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1109
    invoke-static {}, Lcom/google/maps/g/a/w;->d()Lcom/google/maps/g/a/w;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1198
    :goto_0
    return-object p0

    .line 1110
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1111
    iget-object v2, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1112
    iget-object v2, p1, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    .line 1113
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1120
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1121
    iget-object v2, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1122
    iget-object v2, p1, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    .line 1123
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1130
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1131
    iget v2, p1, Lcom/google/maps/g/a/w;->d:I

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput v2, p0, Lcom/google/maps/g/a/y;->d:I

    .line 1133
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1134
    iget v2, p1, Lcom/google/maps/g/a/w;->e:I

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput v2, p0, Lcom/google/maps/g/a/y;->e:I

    .line 1136
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1137
    iget v2, p1, Lcom/google/maps/g/a/w;->f:I

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput v2, p0, Lcom/google/maps/g/a/y;->f:I

    .line 1139
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1140
    iget-object v2, p0, Lcom/google/maps/g/a/y;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1141
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1143
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 1144
    iget v2, p1, Lcom/google/maps/g/a/w;->h:I

    invoke-static {v2}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_7
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1115
    :cond_8
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1116
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 1125
    :cond_a
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1126
    :cond_b
    iget-object v2, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 1130
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 1133
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 1136
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 1139
    goto :goto_6

    :cond_10
    move v2, v1

    .line 1143
    goto :goto_7

    .line 1144
    :cond_11
    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iget v2, v2, Lcom/google/maps/g/a/z;->h:I

    iput v2, p0, Lcom/google/maps/g/a/y;->h:I

    .line 1146
    :cond_12
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_8
    if-eqz v2, :cond_13

    .line 1147
    iget-object v2, p0, Lcom/google/maps/g/a/y;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1148
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1150
    :cond_13
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_9
    if-eqz v2, :cond_14

    .line 1151
    iget-object v2, p0, Lcom/google/maps/g/a/y;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1152
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1154
    :cond_14
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_a
    if-eqz v2, :cond_15

    .line 1155
    iget-boolean v2, p1, Lcom/google/maps/g/a/w;->k:Z

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/y;->k:Z

    .line 1157
    :cond_15
    iget-object v2, p1, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1158
    iget-object v2, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1159
    iget-object v2, p1, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    .line 1160
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1167
    :cond_16
    :goto_b
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_c
    if-eqz v2, :cond_1e

    .line 1168
    iget-object v2, p1, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    if-nez v2, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    move v2, v1

    .line 1146
    goto :goto_8

    :cond_18
    move v2, v1

    .line 1150
    goto :goto_9

    :cond_19
    move v2, v1

    .line 1154
    goto :goto_a

    .line 1162
    :cond_1a
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v2, v2, 0x400

    if-eq v2, v5, :cond_1b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1163
    :cond_1b
    iget-object v2, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_b

    :cond_1c
    move v2, v1

    .line 1167
    goto :goto_c

    .line 1168
    :cond_1d
    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput-object v2, p0, Lcom/google/maps/g/a/y;->m:Lcom/google/n/f;

    .line 1170
    :cond_1e
    iget-object v2, p1, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1f

    .line 1171
    iget-object v2, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1172
    iget-object v2, p1, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    .line 1173
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1180
    :cond_1f
    :goto_d
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_e
    if-eqz v2, :cond_20

    .line 1181
    iget-boolean v2, p1, Lcom/google/maps/g/a/w;->o:Z

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/a/y;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/y;->o:Z

    .line 1183
    :cond_20
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x400

    if-ne v2, v5, :cond_27

    move v2, v0

    :goto_f
    if-eqz v2, :cond_21

    .line 1184
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1185
    iget-object v2, p1, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->p:Ljava/lang/Object;

    .line 1188
    :cond_21
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_10
    if-eqz v2, :cond_22

    .line 1189
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    const v3, 0x8000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1190
    iget-object v2, p1, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/y;->q:Ljava/lang/Object;

    .line 1193
    :cond_22
    iget v2, p1, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-ne v2, v6, :cond_29

    :goto_11
    if-eqz v0, :cond_23

    .line 1194
    iget-object v0, p0, Lcom/google/maps/g/a/y;->r:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1195
    iget v0, p0, Lcom/google/maps/g/a/y;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1197
    :cond_23
    iget-object v0, p1, Lcom/google/maps/g/a/w;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1175
    :cond_24
    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-eq v2, v6, :cond_25

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/y;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/y;->a:I

    .line 1176
    :cond_25
    iget-object v2, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_d

    :cond_26
    move v2, v1

    .line 1180
    goto :goto_e

    :cond_27
    move v2, v1

    .line 1183
    goto :goto_f

    :cond_28
    move v2, v1

    .line 1188
    goto :goto_10

    :cond_29
    move v0, v1

    .line 1193
    goto :goto_11
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 9

    .prologue
    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 967
    new-instance v2, Lcom/google/maps/g/a/w;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/w;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/y;->a:I

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/a/y;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/y;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/a/y;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_10

    :goto_0
    iget v4, p0, Lcom/google/maps/g/a/y;->d:I

    iput v4, v2, Lcom/google/maps/g/a/w;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v4, p0, Lcom/google/maps/g/a/y;->e:I

    iput v4, v2, Lcom/google/maps/g/a/w;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget v4, p0, Lcom/google/maps/g/a/y;->f:I

    iput v4, v2, Lcom/google/maps/g/a/w;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/y;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/y;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v4, p0, Lcom/google/maps/g/a/y;->h:I

    iput v4, v2, Lcom/google/maps/g/a/w;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/y;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/y;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/y;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/y;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-boolean v4, p0, Lcom/google/maps/g/a/y;->k:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/w;->k:Z

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/maps/g/a/y;->a:I

    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/a/y;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x100

    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/a/y;->m:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/y;->a:I

    and-int/lit16 v4, v4, -0x1001

    iput v4, p0, Lcom/google/maps/g/a/y;->a:I

    :cond_b
    iget-object v4, p0, Lcom/google/maps/g/a/y;->n:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x200

    :cond_c
    iget-boolean v4, p0, Lcom/google/maps/g/a/y;->o:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/w;->o:Z

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x400

    :cond_d
    iget-object v4, p0, Lcom/google/maps/g/a/y;->p:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/lit16 v0, v0, 0x800

    :cond_e
    iget-object v4, p0, Lcom/google/maps/g/a/y;->q:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    and-int/2addr v3, v8

    if-ne v3, v8, :cond_f

    or-int/lit16 v0, v0, 0x1000

    :cond_f
    iget-object v3, v2, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/y;->r:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/y;->r:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/w;->a:I

    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 967
    check-cast p1, Lcom/google/maps/g/a/w;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/y;->a(Lcom/google/maps/g/a/w;)Lcom/google/maps/g/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1202
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1203
    iget-object v0, p0, Lcom/google/maps/g/a/y;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hu;->d()Lcom/google/maps/g/a/hu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/hu;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1208
    :goto_1
    return v2

    .line 1202
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1208
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
