.class public final Lcom/google/maps/g/a/q;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/r;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/o;",
        "Lcom/google/maps/g/a/q;",
        ">;",
        "Lcom/google/maps/g/a/r;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private e:Z

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 460
    sget-object v0, Lcom/google/maps/g/a/o;->h:Lcom/google/maps/g/a/o;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 556
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/q;->b:Ljava/lang/Object;

    .line 664
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/q;->c:Ljava/lang/Object;

    .line 740
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/q;->d:Ljava/lang/Object;

    .line 816
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/q;->f:Lcom/google/n/ao;

    .line 875
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/q;->g:Ljava/lang/Object;

    .line 461
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/o;)Lcom/google/maps/g/a/q;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 518
    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 547
    :goto_0
    return-object p0

    .line 519
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 520
    iget v2, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/q;->a:I

    .line 521
    iget-object v2, p1, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/q;->b:Ljava/lang/Object;

    .line 524
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 525
    iget-boolean v2, p1, Lcom/google/maps/g/a/o;->c:Z

    iget v3, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/q;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/q;->e:Z

    .line 527
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 528
    iget v2, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/q;->a:I

    .line 529
    iget-object v2, p1, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/q;->c:Ljava/lang/Object;

    .line 532
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 533
    iget v2, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/q;->a:I

    .line 534
    iget-object v2, p1, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/q;->d:Ljava/lang/Object;

    .line 537
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 538
    iget-object v2, p0, Lcom/google/maps/g/a/q;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 539
    iget v2, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/q;->a:I

    .line 541
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 542
    iget v0, p0, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/q;->a:I

    .line 543
    iget-object v0, p1, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/q;->g:Ljava/lang/Object;

    .line 546
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/a/o;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 519
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 524
    goto :goto_2

    :cond_9
    move v2, v1

    .line 527
    goto :goto_3

    :cond_a
    move v2, v1

    .line 532
    goto :goto_4

    :cond_b
    move v2, v1

    .line 537
    goto :goto_5

    :cond_c
    move v0, v1

    .line 541
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 452
    new-instance v2, Lcom/google/maps/g/a/o;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/o;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/q;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/q;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/o;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/maps/g/a/q;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/o;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/q;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/o;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/q;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/o;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/o;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/q;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/q;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/q;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/o;->g:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/a/o;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 452
    check-cast p1, Lcom/google/maps/g/a/o;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/q;->a(Lcom/google/maps/g/a/o;)Lcom/google/maps/g/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 551
    const/4 v0, 0x1

    return v0
.end method
