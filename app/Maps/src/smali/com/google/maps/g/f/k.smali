.class public final enum Lcom/google/maps/g/f/k;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/f/k;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/f/k;

.field public static final enum b:Lcom/google/maps/g/f/k;

.field public static final enum c:Lcom/google/maps/g/f/k;

.field private static final synthetic e:[Lcom/google/maps/g/f/k;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 14
    new-instance v0, Lcom/google/maps/g/f/k;

    const-string v1, "EARTH"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/f/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/k;->a:Lcom/google/maps/g/f/k;

    .line 18
    new-instance v0, Lcom/google/maps/g/f/k;

    const-string v1, "MOON"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/k;->b:Lcom/google/maps/g/f/k;

    .line 22
    new-instance v0, Lcom/google/maps/g/f/k;

    const-string v1, "MARS"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/g/f/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/k;->c:Lcom/google/maps/g/f/k;

    .line 9
    new-array v0, v5, [Lcom/google/maps/g/f/k;

    sget-object v1, Lcom/google/maps/g/f/k;->a:Lcom/google/maps/g/f/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/f/k;->b:Lcom/google/maps/g/f/k;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/f/k;->c:Lcom/google/maps/g/f/k;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/f/k;->e:[Lcom/google/maps/g/f/k;

    .line 57
    new-instance v0, Lcom/google/maps/g/f/l;

    invoke-direct {v0}, Lcom/google/maps/g/f/l;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lcom/google/maps/g/f/k;->d:I

    .line 68
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/f/k;
    .locals 1

    .prologue
    .line 44
    packed-switch p0, :pswitch_data_0

    .line 48
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 45
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/f/k;->a:Lcom/google/maps/g/f/k;

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/f/k;->b:Lcom/google/maps/g/f/k;

    goto :goto_0

    .line 47
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/f/k;->c:Lcom/google/maps/g/f/k;

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/f/k;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/f/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/f/k;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/f/k;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/f/k;->e:[Lcom/google/maps/g/f/k;

    invoke-virtual {v0}, [Lcom/google/maps/g/f/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/f/k;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/maps/g/f/k;->d:I

    return v0
.end method
