.class public final Lcom/google/maps/g/a/ax;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ba;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ax;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/a/ax;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314
    new-instance v0, Lcom/google/maps/g/a/ay;

    invoke-direct {v0}, Lcom/google/maps/g/a/ay;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ax;->PARSER:Lcom/google/n/ax;

    .line 452
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ax;->h:Lcom/google/n/aw;

    .line 736
    new-instance v0, Lcom/google/maps/g/a/ax;

    invoke-direct {v0}, Lcom/google/maps/g/a/ax;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ax;->e:Lcom/google/maps/g/a/ax;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 258
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 402
    iput-byte v0, p0, Lcom/google/maps/g/a/ax;->f:B

    .line 427
    iput v0, p0, Lcom/google/maps/g/a/ax;->g:I

    .line 259
    iput v1, p0, Lcom/google/maps/g/a/ax;->b:I

    .line 260
    iput v1, p0, Lcom/google/maps/g/a/ax;->c:I

    .line 261
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;

    .line 262
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 268
    invoke-direct {p0}, Lcom/google/maps/g/a/ax;-><init>()V

    .line 269
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 273
    const/4 v0, 0x0

    .line 274
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 275
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 276
    sparse-switch v3, :sswitch_data_0

    .line 281
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 283
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 279
    goto :goto_0

    .line 288
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/a/ax;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/ax;->a:I

    .line 289
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/a/ax;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 305
    :catch_0
    move-exception v0

    .line 306
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ax;->au:Lcom/google/n/bn;

    throw v0

    .line 293
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/a/ax;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/ax;->a:I

    .line 294
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/a/ax;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 307
    :catch_1
    move-exception v0

    .line 308
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 309
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 299
    iget v4, p0, Lcom/google/maps/g/a/ax;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/a/ax;->a:I

    .line 300
    iput-object v3, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 311
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ax;->au:Lcom/google/n/bn;

    .line 312
    return-void

    .line 276
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 256
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 402
    iput-byte v0, p0, Lcom/google/maps/g/a/ax;->f:B

    .line 427
    iput v0, p0, Lcom/google/maps/g/a/ax;->g:I

    .line 257
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/ax;
    .locals 1

    .prologue
    .line 739
    sget-object v0, Lcom/google/maps/g/a/ax;->e:Lcom/google/maps/g/a/ax;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/az;
    .locals 1

    .prologue
    .line 514
    new-instance v0, Lcom/google/maps/g/a/az;

    invoke-direct {v0}, Lcom/google/maps/g/a/az;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ax;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    sget-object v0, Lcom/google/maps/g/a/ax;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 414
    invoke-virtual {p0}, Lcom/google/maps/g/a/ax;->c()I

    .line 415
    iget v0, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 416
    iget v0, p0, Lcom/google/maps/g/a/ax;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 418
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 419
    iget v0, p0, Lcom/google/maps/g/a/ax;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 421
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 422
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/ax;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 425
    return-void

    .line 416
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 419
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 422
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 404
    iget-byte v1, p0, Lcom/google/maps/g/a/ax;->f:B

    .line 405
    if-ne v1, v0, :cond_0

    .line 409
    :goto_0
    return v0

    .line 406
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 408
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/ax;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 429
    iget v0, p0, Lcom/google/maps/g/a/ax;->g:I

    .line 430
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 447
    :goto_0
    return v0

    .line 433
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 434
    iget v0, p0, Lcom/google/maps/g/a/ax;->b:I

    .line 435
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 437
    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_5

    .line 438
    iget v3, p0, Lcom/google/maps/g/a/ax;->c:I

    .line 439
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    move v1, v0

    .line 441
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 442
    const/4 v3, 0x3

    .line 443
    iget-object v0, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ax;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 445
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/ax;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 446
    iput v0, p0, Lcom/google/maps/g/a/ax;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 435
    goto :goto_1

    .line 443
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_5
    move v1, v0

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 250
    invoke-static {}, Lcom/google/maps/g/a/ax;->newBuilder()Lcom/google/maps/g/a/az;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/az;->a(Lcom/google/maps/g/a/ax;)Lcom/google/maps/g/a/az;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 250
    invoke-static {}, Lcom/google/maps/g/a/ax;->newBuilder()Lcom/google/maps/g/a/az;

    move-result-object v0

    return-object v0
.end method
