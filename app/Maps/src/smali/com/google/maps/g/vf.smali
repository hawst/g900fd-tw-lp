.class public final Lcom/google/maps/g/vf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vo;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vf;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/maps/g/vf;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Z

.field e:Z

.field public f:I

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/google/n/ao;

.field public i:Ljava/lang/Object;

.field public j:Ljava/lang/Object;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/google/maps/g/vg;

    invoke-direct {v0}, Lcom/google/maps/g/vg;-><init>()V

    sput-object v0, Lcom/google/maps/g/vf;->PARSER:Lcom/google/n/ax;

    .line 611
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/vf;->o:Lcom/google/n/aw;

    .line 1564
    new-instance v0, Lcom/google/maps/g/vf;

    invoke-direct {v0}, Lcom/google/maps/g/vf;-><init>()V

    sput-object v0, Lcom/google/maps/g/vf;->l:Lcom/google/maps/g/vf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 223
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    .line 370
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    .line 512
    iput-byte v3, p0, Lcom/google/maps/g/vf;->m:B

    .line 558
    iput v3, p0, Lcom/google/maps/g/vf;->n:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    .line 20
    iput-boolean v2, p0, Lcom/google/maps/g/vf;->d:Z

    .line 21
    iput-boolean v2, p0, Lcom/google/maps/g/vf;->e:Z

    .line 22
    iput v2, p0, Lcom/google/maps/g/vf;->f:I

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x200

    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/maps/g/vf;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 40
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 42
    sparse-switch v1, :sswitch_data_0

    .line 47
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 49
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 55
    iget v1, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/vf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 124
    :catch_0
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 125
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v8, :cond_1

    .line 131
    iget-object v2, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    .line 133
    :cond_1
    and-int/lit16 v1, v1, 0x200

    if-ne v1, v9, :cond_2

    .line 134
    iget-object v1, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    .line 136
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vf;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 60
    iget v6, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/vf;->a:I

    .line 61
    iput-object v1, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 126
    :catch_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 127
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 128
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/vf;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v10

    if-eqz v1, :cond_3

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/google/maps/g/vf;->d:Z

    goto :goto_0

    .line 130
    :catchall_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    goto :goto_2

    :cond_3
    move v1, v3

    .line 66
    goto :goto_4

    .line 70
    :sswitch_4
    iget v1, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/vf;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v10

    if-eqz v1, :cond_4

    move v1, v2

    :goto_5
    iput-boolean v1, p0, Lcom/google/maps/g/vf;->e:Z

    goto/16 :goto_0

    :cond_4
    move v1, v3

    goto :goto_5

    .line 75
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 76
    invoke-static {v1}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v6

    .line 77
    if-nez v6, :cond_5

    .line 78
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 80
    :cond_5
    iget v6, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/vf;->a:I

    .line 81
    iput v1, p0, Lcom/google/maps/g/vf;->f:I

    goto/16 :goto_0

    .line 86
    :sswitch_6
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v8, :cond_a

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 89
    or-int/lit8 v1, v0, 0x20

    .line 91
    :goto_6
    :try_start_5
    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 91
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 93
    goto/16 :goto_0

    .line 96
    :sswitch_7
    :try_start_6
    iget-object v1, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 97
    iget v1, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/maps/g/vf;->a:I

    goto/16 :goto_0

    .line 101
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 102
    iget v6, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/maps/g/vf;->a:I

    .line 103
    iput-object v1, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 107
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 108
    iget v6, p0, Lcom/google/maps/g/vf;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/vf;->a:I

    .line 109
    iput-object v1, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 113
    :sswitch_a
    and-int/lit16 v1, v0, 0x200

    if-eq v1, v9, :cond_6

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    .line 116
    or-int/lit16 v0, v0, 0x200

    .line 118
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 118
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 130
    :cond_7
    and-int/lit8 v1, v0, 0x20

    if-ne v1, v8, :cond_8

    .line 131
    iget-object v1, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    .line 133
    :cond_8
    and-int/lit16 v0, v0, 0x200

    if-ne v0, v9, :cond_9

    .line 134
    iget-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    .line 136
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->au:Lcom/google/n/bn;

    .line 137
    return-void

    .line 126
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 124
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_a
    move v1, v0

    goto/16 :goto_6

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 223
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    .line 370
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    .line 512
    iput-byte v1, p0, Lcom/google/maps/g/vf;->m:B

    .line 558
    iput v1, p0, Lcom/google/maps/g/vf;->n:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/vf;
    .locals 1

    .prologue
    .line 1567
    sget-object v0, Lcom/google/maps/g/vf;->l:Lcom/google/maps/g/vf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/vh;
    .locals 1

    .prologue
    .line 673
    new-instance v0, Lcom/google/maps/g/vh;

    invoke-direct {v0}, Lcom/google/maps/g/vh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lcom/google/maps/g/vf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 524
    invoke-virtual {p0}, Lcom/google/maps/g/vf;->c()I

    .line 525
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 528
    :cond_0
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 529
    iget-object v0, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 531
    :cond_1
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 532
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/vf;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 534
    :cond_2
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 535
    iget-boolean v0, p0, Lcom/google/maps/g/vf;->e:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 537
    :cond_3
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 538
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/vf;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_4
    :goto_3
    move v1, v2

    .line 540
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 541
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 529
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_6
    move v0, v2

    .line 532
    goto :goto_1

    :cond_7
    move v1, v2

    .line 535
    goto :goto_2

    .line 538
    :cond_8
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 543
    :cond_9
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 544
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 546
    :cond_a
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 547
    iget-object v0, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 549
    :cond_b
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 550
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 552
    :cond_c
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_f

    .line 553
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 552
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 547
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 550
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 555
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/vf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 556
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 514
    iget-byte v1, p0, Lcom/google/maps/g/vf;->m:B

    .line 515
    if-ne v1, v0, :cond_0

    .line 519
    :goto_0
    return v0

    .line 516
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/vf;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560
    iget v0, p0, Lcom/google/maps/g/vf;->n:I

    .line 561
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 606
    :goto_0
    return v0

    .line 564
    :cond_0
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_e

    .line 565
    iget-object v0, p0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    .line 566
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 568
    :goto_1
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 570
    iget-object v0, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 572
    :cond_1
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 573
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/vf;->d:Z

    .line 574
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 576
    :cond_2
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 577
    iget-boolean v0, p0, Lcom/google/maps/g/vf;->e:Z

    .line 578
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 580
    :cond_3
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 581
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/vf;->f:I

    .line 582
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    :cond_4
    move v3, v1

    move v1, v2

    .line 584
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 585
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    .line 586
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 584
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 570
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 582
    :cond_6
    const/16 v0, 0xa

    goto :goto_3

    .line 588
    :cond_7
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 589
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    .line 590
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 592
    :cond_8
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 594
    iget-object v0, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 596
    :cond_9
    iget v0, p0, Lcom/google/maps/g/vf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 597
    const/16 v1, 0x9

    .line 598
    iget-object v0, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_a
    move v1, v2

    .line 600
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 601
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/vf;->k:Ljava/util/List;

    .line 602
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 600
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 594
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 598
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 604
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/vf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 605
    iput v0, p0, Lcom/google/maps/g/vf;->n:I

    goto/16 :goto_0

    :cond_e
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    .line 334
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 335
    iget-object v0, p0, Lcom/google/maps/g/vf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 336
    invoke-static {}, Lcom/google/maps/g/a/ee;->d()Lcom/google/maps/g/a/ee;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 338
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vf;->newBuilder()Lcom/google/maps/g/vh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/vh;->a(Lcom/google/maps/g/vf;)Lcom/google/maps/g/vh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vf;->newBuilder()Lcom/google/maps/g/vh;

    move-result-object v0

    return-object v0
.end method
