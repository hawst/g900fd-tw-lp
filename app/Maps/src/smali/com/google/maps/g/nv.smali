.class public final Lcom/google/maps/g/nv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ny;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nv;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/nv;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/maps/g/hg;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lcom/google/maps/g/nw;

    invoke-direct {v0}, Lcom/google/maps/g/nw;-><init>()V

    sput-object v0, Lcom/google/maps/g/nv;->PARSER:Lcom/google/n/ax;

    .line 314
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/nv;->g:Lcom/google/n/aw;

    .line 586
    new-instance v0, Lcom/google/maps/g/nv;

    invoke-direct {v0}, Lcom/google/maps/g/nv;-><init>()V

    sput-object v0, Lcom/google/maps/g/nv;->d:Lcom/google/maps/g/nv;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 141
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 271
    iput-byte v0, p0, Lcom/google/maps/g/nv;->e:B

    .line 293
    iput v0, p0, Lcom/google/maps/g/nv;->f:I

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    .line 143
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 149
    invoke-direct {p0}, Lcom/google/maps/g/nv;-><init>()V

    .line 150
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 154
    const/4 v0, 0x0

    move v2, v0

    .line 155
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 156
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 157
    sparse-switch v0, :sswitch_data_0

    .line 162
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 164
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 160
    goto :goto_0

    .line 169
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 170
    iget v1, p0, Lcom/google/maps/g/nv;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/nv;->a:I

    .line 171
    iput-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nv;->au:Lcom/google/n/bn;

    throw v0

    .line 175
    :sswitch_2
    const/4 v0, 0x0

    .line 176
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    .line 177
    iget-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    move-object v1, v0

    .line 179
    :goto_1
    sget-object v0, Lcom/google/maps/g/hg;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    iput-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    .line 180
    if-eqz v1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    .line 182
    invoke-virtual {v1}, Lcom/google/maps/g/hi;->c()Lcom/google/maps/g/hg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    .line 184
    :cond_1
    iget v0, p0, Lcom/google/maps/g/nv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/nv;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 191
    :catch_1
    move-exception v0

    .line 192
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 193
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nv;->au:Lcom/google/n/bn;

    .line 196
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 157
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 139
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 271
    iput-byte v0, p0, Lcom/google/maps/g/nv;->e:B

    .line 293
    iput v0, p0, Lcom/google/maps/g/nv;->f:I

    .line 140
    return-void
.end method

.method public static d()Lcom/google/maps/g/nv;
    .locals 1

    .prologue
    .line 589
    sget-object v0, Lcom/google/maps/g/nv;->d:Lcom/google/maps/g/nv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/nx;
    .locals 1

    .prologue
    .line 376
    new-instance v0, Lcom/google/maps/g/nx;

    invoke-direct {v0}, Lcom/google/maps/g/nx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    sget-object v0, Lcom/google/maps/g/nv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 283
    invoke-virtual {p0}, Lcom/google/maps/g/nv;->c()I

    .line 284
    iget v0, p0, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 287
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 288
    iget-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v0

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/nv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 291
    return-void

    .line 285
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 273
    iget-byte v1, p0, Lcom/google/maps/g/nv;->e:B

    .line 274
    if-ne v1, v0, :cond_0

    .line 278
    :goto_0
    return v0

    .line 275
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 277
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/nv;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 295
    iget v0, p0, Lcom/google/maps/g/nv;->f:I

    .line 296
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 309
    :goto_0
    return v0

    .line 299
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 301
    iget-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nv;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 303
    :goto_2
    iget v2, p0, Lcom/google/maps/g/nv;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 305
    iget-object v2, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 307
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/nv;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    iput v0, p0, Lcom/google/maps/g/nv;->f:I

    goto :goto_0

    .line 301
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 305
    :cond_3
    iget-object v2, p0, Lcom/google/maps/g/nv;->c:Lcom/google/maps/g/hg;

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 133
    invoke-static {}, Lcom/google/maps/g/nv;->newBuilder()Lcom/google/maps/g/nx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/nx;->a(Lcom/google/maps/g/nv;)Lcom/google/maps/g/nx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 133
    invoke-static {}, Lcom/google/maps/g/nv;->newBuilder()Lcom/google/maps/g/nx;

    move-result-object v0

    return-object v0
.end method
