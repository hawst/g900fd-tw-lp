.class public final Lcom/google/maps/g/fk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/fn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fk;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/fk;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Lcom/google/n/aq;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/google/maps/g/fl;

    invoke-direct {v0}, Lcom/google/maps/g/fl;-><init>()V

    sput-object v0, Lcom/google/maps/g/fk;->PARSER:Lcom/google/n/ax;

    .line 301
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/fk;->i:Lcom/google/n/aw;

    .line 808
    new-instance v0, Lcom/google/maps/g/fk;

    invoke-direct {v0}, Lcom/google/maps/g/fk;-><init>()V

    sput-object v0, Lcom/google/maps/g/fk;->f:Lcom/google/maps/g/fk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 239
    iput-byte v0, p0, Lcom/google/maps/g/fk;->g:B

    .line 267
    iput v0, p0, Lcom/google/maps/g/fk;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/fk;->d:Z

    .line 21
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/fk;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 34
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 43
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v10, :cond_1

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    .line 51
    or-int/lit8 v1, v1, 0x2

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 53
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v10, :cond_2

    .line 86
    iget-object v2, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    .line 88
    :cond_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v11, :cond_3

    .line 89
    iget-object v1, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    .line 91
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fk;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/fk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/fk;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/fk;->d:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 81
    :catch_1
    move-exception v0

    .line 82
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    move v0, v3

    .line 59
    goto :goto_1

    .line 63
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    and-int/lit8 v6, v1, 0x8

    if-eq v6, v11, :cond_5

    .line 65
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    .line 66
    or-int/lit8 v1, v1, 0x8

    .line 68
    :cond_5
    iget-object v6, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget v6, p0, Lcom/google/maps/g/fk;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/fk;->a:I

    .line 74
    iput-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 85
    :cond_6
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v10, :cond_7

    .line 86
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    .line 88
    :cond_7
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v11, :cond_8

    .line 89
    iget-object v0, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    .line 91
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->au:Lcom/google/n/bn;

    .line 92
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 239
    iput-byte v0, p0, Lcom/google/maps/g/fk;->g:B

    .line 267
    iput v0, p0, Lcom/google/maps/g/fk;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/fk;
    .locals 1

    .prologue
    .line 811
    sget-object v0, Lcom/google/maps/g/fk;->f:Lcom/google/maps/g/fk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/fm;
    .locals 1

    .prologue
    .line 363
    new-instance v0, Lcom/google/maps/g/fm;

    invoke-direct {v0}, Lcom/google/maps/g/fm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lcom/google/maps/g/fk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 251
    invoke-virtual {p0}, Lcom/google/maps/g/fk;->c()I

    move v1, v2

    .line 252
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 252
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255
    :cond_0
    iget v0, p0, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 256
    iget-boolean v0, p0, Lcom/google/maps/g/fk;->d:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 258
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 259
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 258
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 256
    goto :goto_1

    .line 261
    :cond_3
    iget v0, p0, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 262
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 264
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/fk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 265
    return-void

    .line 262
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 241
    iget-byte v1, p0, Lcom/google/maps/g/fk;->g:B

    .line 242
    if-ne v1, v0, :cond_0

    .line 246
    :goto_0
    return v0

    .line 243
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 245
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/fk;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 269
    iget v0, p0, Lcom/google/maps/g/fk;->h:I

    .line 270
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 296
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 273
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    .line 275
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 277
    :cond_1
    iget v0, p0, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 278
    iget-boolean v0, p0, Lcom/google/maps/g/fk;->d:Z

    .line 279
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_2
    move v0, v2

    move v1, v2

    .line 283
    :goto_2
    iget-object v4, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 284
    iget-object v4, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    .line 285
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 287
    :cond_3
    add-int v0, v3, v1

    .line 288
    iget-object v1, p0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 290
    iget v0, p0, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_5

    .line 291
    const/4 v3, 0x4

    .line 292
    iget-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 294
    :goto_4
    iget-object v1, p0, Lcom/google/maps/g/fk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    iput v0, p0, Lcom/google/maps/g/fk;->h:I

    goto/16 :goto_0

    .line 292
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/fk;->newBuilder()Lcom/google/maps/g/fm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/fm;->a(Lcom/google/maps/g/fk;)Lcom/google/maps/g/fm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/fk;->newBuilder()Lcom/google/maps/g/fm;

    move-result-object v0

    return-object v0
.end method
