.class public final Lcom/google/maps/g/a/bq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bq;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/maps/g/a/bq;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/aq;

.field m:I

.field n:I

.field o:I

.field p:I

.field q:Lcom/google/n/ao;

.field r:Ljava/lang/Object;

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/google/maps/g/a/br;

    invoke-direct {v0}, Lcom/google/maps/g/a/br;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bq;->PARSER:Lcom/google/n/ax;

    .line 694
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/bq;->v:Lcom/google/n/aw;

    .line 1983
    new-instance v0, Lcom/google/maps/g/a/bq;

    invoke-direct {v0}, Lcom/google/maps/g/a/bq;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bq;->s:Lcom/google/maps/g/a/bq;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 267
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    .line 299
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    .line 315
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    .line 331
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    .line 363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    .line 379
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    .line 484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    .line 541
    iput-byte v4, p0, Lcom/google/maps/g/a/bq;->t:B

    .line 608
    iput v4, p0, Lcom/google/maps/g/a/bq;->u:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 29
    iput v3, p0, Lcom/google/maps/g/a/bq;->m:I

    .line 30
    iput v3, p0, Lcom/google/maps/g/a/bq;->n:I

    .line 31
    iput v3, p0, Lcom/google/maps/g/a/bq;->o:I

    .line 32
    iput v3, p0, Lcom/google/maps/g/a/bq;->p:I

    .line 33
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x400

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/maps/g/a/bq;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 47
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 48
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 49
    sparse-switch v4, :sswitch_data_0

    .line 54
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 56
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 52
    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 62
    iget v5, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 63
    iput-object v4, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x400

    if-ne v1, v7, :cond_1

    .line 161
    iget-object v1, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 163
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bq;->au:Lcom/google/n/bn;

    throw v0

    .line 67
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 68
    iget v5, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 69
    iput-object v4, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 156
    :catch_1
    move-exception v0

    .line 157
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 158
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 74
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto :goto_0

    .line 78
    :sswitch_4
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 79
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto :goto_0

    .line 83
    :sswitch_5
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 84
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 88
    :sswitch_6
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 89
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_7
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 94
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 98
    :sswitch_8
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 99
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 103
    :sswitch_9
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 104
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_a
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 109
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 114
    and-int/lit16 v5, v1, 0x400

    if-eq v5, v7, :cond_2

    .line 115
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 116
    or-int/lit16 v1, v1, 0x400

    .line 118
    :cond_2
    iget-object v5, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 122
    :sswitch_c
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 123
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/a/bq;->m:I

    goto/16 :goto_0

    .line 127
    :sswitch_d
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 128
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/a/bq;->n:I

    goto/16 :goto_0

    .line 132
    :sswitch_e
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 133
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/a/bq;->o:I

    goto/16 :goto_0

    .line 137
    :sswitch_f
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 138
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/a/bq;->p:I

    goto/16 :goto_0

    .line 142
    :sswitch_10
    iget-object v4, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 143
    iget v4, p0, Lcom/google/maps/g/a/bq;->a:I

    or-int/lit16 v4, v4, 0x4000

    iput v4, p0, Lcom/google/maps/g/a/bq;->a:I

    goto/16 :goto_0

    .line 147
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 148
    iget v5, p0, Lcom/google/maps/g/a/bq;->a:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/maps/g/a/bq;->a:I

    .line 149
    iput-object v4, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 160
    :cond_3
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v7, :cond_4

    .line 161
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 163
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->au:Lcom/google/n/bn;

    .line 164
    return-void

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 267
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    .line 299
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    .line 315
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    .line 331
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    .line 363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    .line 379
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    .line 484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    .line 541
    iput-byte v1, p0, Lcom/google/maps/g/a/bq;->t:B

    .line 608
    iput v1, p0, Lcom/google/maps/g/a/bq;->u:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;
    .locals 1

    .prologue
    .line 759
    invoke-static {}, Lcom/google/maps/g/a/bq;->newBuilder()Lcom/google/maps/g/a/bs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/bq;
    .locals 1

    .prologue
    .line 1986
    sget-object v0, Lcom/google/maps/g/a/bq;->s:Lcom/google/maps/g/a/bq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/bs;
    .locals 1

    .prologue
    .line 756
    new-instance v0, Lcom/google/maps/g/a/bs;

    invoke-direct {v0}, Lcom/google/maps/g/a/bs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lcom/google/maps/g/a/bq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 553
    invoke-virtual {p0}, Lcom/google/maps/g/a/bq;->c()I

    .line 554
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 555
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 557
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 558
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 560
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 561
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 564
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 566
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 567
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 569
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 570
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 572
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 573
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 575
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_7

    .line 576
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 578
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_8

    .line 579
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 581
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_9

    .line 582
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_9
    move v0, v1

    .line 584
    :goto_2
    iget-object v2, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_c

    .line 585
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 584
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 555
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 558
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 587
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 588
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/maps/g/a/bq;->m:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_13

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 590
    :cond_d
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_e

    .line 591
    const/16 v0, 0xd

    iget v2, p0, Lcom/google/maps/g/a/bq;->n:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_14

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 593
    :cond_e
    :goto_4
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_f

    .line 594
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/maps/g/a/bq;->o:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_15

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 596
    :cond_f
    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_10

    .line 597
    const/16 v0, 0xf

    iget v2, p0, Lcom/google/maps/g/a/bq;->p:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_16

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 599
    :cond_10
    :goto_6
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_11

    .line 600
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 602
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_12

    .line 603
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 605
    :cond_12
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 606
    return-void

    .line 588
    :cond_13
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 591
    :cond_14
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 594
    :cond_15
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 597
    :cond_16
    int-to-long v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 603
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto :goto_7
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 543
    iget-byte v1, p0, Lcom/google/maps/g/a/bq;->t:B

    .line 544
    if-ne v1, v0, :cond_0

    .line 548
    :goto_0
    return v0

    .line 545
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 547
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/bq;->t:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 610
    iget v0, p0, Lcom/google/maps/g/a/bq;->u:I

    .line 611
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 689
    :goto_0
    return v0

    .line 614
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_18

    .line 616
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 618
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 620
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 622
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 623
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    .line 624
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 626
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 627
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    .line 628
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 630
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 631
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    .line 632
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 634
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 635
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    .line 636
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 638
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 639
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    .line 640
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 642
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 643
    const/16 v0, 0x8

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    .line 644
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 646
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 647
    const/16 v0, 0x9

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    .line 648
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 650
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_9

    .line 651
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    .line 652
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_9
    move v0, v2

    move v3, v2

    .line 656
    :goto_4
    iget-object v5, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_c

    .line 657
    iget-object v5, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 658
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 656
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 616
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 620
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 660
    :cond_c
    add-int v0, v1, v3

    .line 661
    iget-object v1, p0, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 663
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_17

    .line 664
    const/16 v0, 0xc

    iget v3, p0, Lcom/google/maps/g/a/bq;->m:I

    .line 665
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_12

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v0, v1

    .line 667
    :goto_6
    iget v1, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_d

    .line 668
    const/16 v1, 0xd

    iget v3, p0, Lcom/google/maps/g/a/bq;->n:I

    .line 669
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_13

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_7
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 671
    :cond_d
    iget v1, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_e

    .line 672
    const/16 v1, 0xe

    iget v3, p0, Lcom/google/maps/g/a/bq;->o:I

    .line 673
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_14

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_8
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 675
    :cond_e
    iget v1, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_10

    .line 676
    const/16 v1, 0xf

    iget v3, p0, Lcom/google/maps/g/a/bq;->p:I

    .line 677
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_f
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 679
    :cond_10
    iget v1, p0, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_16

    .line 680
    const/16 v1, 0x10

    iget-object v3, p0, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    .line 681
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 683
    :goto_9
    iget v0, p0, Lcom/google/maps/g/a/bq;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_11

    .line 684
    const/16 v3, 0x11

    .line 685
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 687
    :cond_11
    iget-object v0, p0, Lcom/google/maps/g/a/bq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 688
    iput v0, p0, Lcom/google/maps/g/a/bq;->u:I

    goto/16 :goto_0

    :cond_12
    move v0, v4

    .line 665
    goto/16 :goto_5

    :cond_13
    move v1, v4

    .line 669
    goto/16 :goto_7

    :cond_14
    move v1, v4

    .line 673
    goto :goto_8

    .line 685
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    :cond_16
    move v1, v0

    goto :goto_9

    :cond_17
    move v0, v1

    goto/16 :goto_6

    :cond_18
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bq;->newBuilder()Lcom/google/maps/g/a/bs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bq;->newBuilder()Lcom/google/maps/g/a/bs;

    move-result-object v0

    return-object v0
.end method
