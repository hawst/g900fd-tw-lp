.class public final Lcom/google/maps/g/dq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dq;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/dq;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 303
    new-instance v0, Lcom/google/maps/g/dr;

    invoke-direct {v0}, Lcom/google/maps/g/dr;-><init>()V

    sput-object v0, Lcom/google/maps/g/dq;->PARSER:Lcom/google/n/ax;

    .line 441
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/dq;->h:Lcom/google/n/aw;

    .line 725
    new-instance v0, Lcom/google/maps/g/dq;

    invoke-direct {v0}, Lcom/google/maps/g/dq;-><init>()V

    sput-object v0, Lcom/google/maps/g/dq;->e:Lcom/google/maps/g/dq;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 247
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 391
    iput-byte v0, p0, Lcom/google/maps/g/dq;->f:B

    .line 416
    iput v0, p0, Lcom/google/maps/g/dq;->g:I

    .line 248
    iput v1, p0, Lcom/google/maps/g/dq;->b:I

    .line 249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    .line 250
    iput v1, p0, Lcom/google/maps/g/dq;->d:I

    .line 251
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 257
    invoke-direct {p0}, Lcom/google/maps/g/dq;-><init>()V

    .line 258
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 262
    const/4 v0, 0x0

    .line 263
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 264
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 265
    sparse-switch v3, :sswitch_data_0

    .line 270
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 272
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 268
    goto :goto_0

    .line 277
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/dq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/dq;->a:I

    .line 278
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/dq;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/dq;->au:Lcom/google/n/bn;

    throw v0

    .line 282
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 283
    iget v4, p0, Lcom/google/maps/g/dq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/dq;->a:I

    .line 284
    iput-object v3, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 296
    :catch_1
    move-exception v0

    .line 297
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 298
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 288
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/g/dq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/dq;->a:I

    .line 289
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/dq;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 300
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dq;->au:Lcom/google/n/bn;

    .line 301
    return-void

    .line 265
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 245
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 391
    iput-byte v0, p0, Lcom/google/maps/g/dq;->f:B

    .line 416
    iput v0, p0, Lcom/google/maps/g/dq;->g:I

    .line 246
    return-void
.end method

.method public static a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;
    .locals 1

    .prologue
    .line 506
    invoke-static {}, Lcom/google/maps/g/dq;->newBuilder()Lcom/google/maps/g/ds;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ds;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/dq;
    .locals 1

    .prologue
    .line 728
    sget-object v0, Lcom/google/maps/g/dq;->e:Lcom/google/maps/g/dq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ds;
    .locals 1

    .prologue
    .line 503
    new-instance v0, Lcom/google/maps/g/ds;

    invoke-direct {v0}, Lcom/google/maps/g/ds;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    sget-object v0, Lcom/google/maps/g/dq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 403
    invoke-virtual {p0}, Lcom/google/maps/g/dq;->c()I

    .line 404
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 405
    iget v0, p0, Lcom/google/maps/g/dq;->b:I

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 407
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 408
    iget-object v0, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 410
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 411
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/dq;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 413
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/dq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 414
    return-void

    .line 405
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 408
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 411
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 393
    iget-byte v1, p0, Lcom/google/maps/g/dq;->f:B

    .line 394
    if-ne v1, v0, :cond_0

    .line 398
    :goto_0
    return v0

    .line 395
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 397
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/dq;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 418
    iget v0, p0, Lcom/google/maps/g/dq;->g:I

    .line 419
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 436
    :goto_0
    return v0

    .line 422
    :cond_0
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 423
    iget v0, p0, Lcom/google/maps/g/dq;->b:I

    .line 424
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 426
    :goto_2
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 428
    iget-object v0, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dq;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 430
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    .line 431
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/maps/g/dq;->d:I

    .line 432
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 434
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/dq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 435
    iput v0, p0, Lcom/google/maps/g/dq;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 424
    goto :goto_1

    .line 428
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lcom/google/maps/g/dq;->newBuilder()Lcom/google/maps/g/ds;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ds;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lcom/google/maps/g/dq;->newBuilder()Lcom/google/maps/g/ds;

    move-result-object v0

    return-object v0
.end method
