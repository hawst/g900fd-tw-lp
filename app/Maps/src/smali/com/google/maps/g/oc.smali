.class public final Lcom/google/maps/g/oc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/of;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/oa;",
        "Lcom/google/maps/g/oc;",
        ">;",
        "Lcom/google/maps/g/of;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/google/maps/g/oa;->e:Lcom/google/maps/g/oa;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 446
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/oc;->b:I

    .line 482
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/oc;->c:Ljava/lang/Object;

    .line 558
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/oc;->d:Ljava/lang/Object;

    .line 385
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/oa;)Lcom/google/maps/g/oc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 422
    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 437
    :goto_0
    return-object p0

    .line 423
    :cond_0
    iget v2, p1, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 424
    iget v2, p1, Lcom/google/maps/g/oa;->b:I

    invoke-static {v2}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 423
    goto :goto_1

    .line 424
    :cond_3
    iget v3, p0, Lcom/google/maps/g/oc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/oc;->a:I

    iget v2, v2, Lcom/google/maps/g/od;->c:I

    iput v2, p0, Lcom/google/maps/g/oc;->b:I

    .line 426
    :cond_4
    iget v2, p1, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 427
    iget v2, p0, Lcom/google/maps/g/oc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/oc;->a:I

    .line 428
    iget-object v2, p1, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/oc;->c:Ljava/lang/Object;

    .line 431
    :cond_5
    iget v2, p1, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_6

    .line 432
    iget v0, p0, Lcom/google/maps/g/oc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/oc;->a:I

    .line 433
    iget-object v0, p1, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/oc;->d:Ljava/lang/Object;

    .line 436
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/oa;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v2, v1

    .line 426
    goto :goto_2

    :cond_8
    move v0, v1

    .line 431
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 376
    new-instance v2, Lcom/google/maps/g/oa;

    invoke-direct {v2, p0}, Lcom/google/maps/g/oa;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/oc;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/maps/g/oc;->b:I

    iput v1, v2, Lcom/google/maps/g/oa;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/oc;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/oc;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/oa;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 376
    check-cast p1, Lcom/google/maps/g/oa;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/oc;->a(Lcom/google/maps/g/oa;)Lcom/google/maps/g/oc;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 441
    const/4 v0, 0x1

    return v0
.end method
