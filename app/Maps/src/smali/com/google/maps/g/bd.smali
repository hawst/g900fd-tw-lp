.class public final enum Lcom/google/maps/g/bd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/bd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/bd;

.field public static final enum b:Lcom/google/maps/g/bd;

.field public static final enum c:Lcom/google/maps/g/bd;

.field private static final synthetic e:[Lcom/google/maps/g/bd;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 116
    new-instance v0, Lcom/google/maps/g/bd;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/bd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bd;->a:Lcom/google/maps/g/bd;

    .line 120
    new-instance v0, Lcom/google/maps/g/bd;

    const-string v1, "ANSWERED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/bd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bd;->b:Lcom/google/maps/g/bd;

    .line 124
    new-instance v0, Lcom/google/maps/g/bd;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/bd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/bd;->c:Lcom/google/maps/g/bd;

    .line 111
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/bd;

    sget-object v1, Lcom/google/maps/g/bd;->a:Lcom/google/maps/g/bd;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/bd;->b:Lcom/google/maps/g/bd;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/bd;->c:Lcom/google/maps/g/bd;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/bd;->e:[Lcom/google/maps/g/bd;

    .line 159
    new-instance v0, Lcom/google/maps/g/be;

    invoke-direct {v0}, Lcom/google/maps/g/be;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 169
    iput p3, p0, Lcom/google/maps/g/bd;->d:I

    .line 170
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/bd;
    .locals 1

    .prologue
    .line 146
    packed-switch p0, :pswitch_data_0

    .line 150
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 147
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/bd;->a:Lcom/google/maps/g/bd;

    goto :goto_0

    .line 148
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/bd;->b:Lcom/google/maps/g/bd;

    goto :goto_0

    .line 149
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/bd;->c:Lcom/google/maps/g/bd;

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/bd;
    .locals 1

    .prologue
    .line 111
    const-class v0, Lcom/google/maps/g/bd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/bd;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/bd;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/maps/g/bd;->e:[Lcom/google/maps/g/bd;

    invoke-virtual {v0}, [Lcom/google/maps/g/bd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/bd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/google/maps/g/bd;->d:I

    return v0
.end method
