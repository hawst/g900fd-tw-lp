.class public final enum Lcom/google/maps/g/f/g;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/f/g;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/f/g;

.field public static final enum b:Lcom/google/maps/g/f/g;

.field public static final enum c:Lcom/google/maps/g/f/g;

.field public static final enum d:Lcom/google/maps/g/f/g;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum e:Lcom/google/maps/g/f/g;

.field public static final enum f:Lcom/google/maps/g/f/g;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum g:Lcom/google/maps/g/f/g;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum h:Lcom/google/maps/g/f/g;

.field public static final enum i:Lcom/google/maps/g/f/g;

.field private static final synthetic k:[Lcom/google/maps/g/f/g;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "UNKNOWN_SOURCE"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->a:Lcom/google/maps/g/f/g;

    .line 18
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "ALLEYCAT"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->b:Lcom/google/maps/g/f/g;

    .line 22
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "PANORAMIO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v5}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->c:Lcom/google/maps/g/f/g;

    .line 26
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "LEANBACK"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->d:Lcom/google/maps/g/f/g;

    .line 31
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "FIFE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->e:Lcom/google/maps/g/f/g;

    .line 35
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "DRAGONFLY"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->f:Lcom/google/maps/g/f/g;

    .line 40
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "MOOSEDOG"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->g:Lcom/google/maps/g/f/g;

    .line 45
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "INTERNET"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->h:Lcom/google/maps/g/f/g;

    .line 49
    new-instance v0, Lcom/google/maps/g/f/g;

    const-string v1, "METADATA_GEO_PHOTO_SERVICE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/g;->i:Lcom/google/maps/g/f/g;

    .line 9
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/maps/g/f/g;

    sget-object v1, Lcom/google/maps/g/f/g;->a:Lcom/google/maps/g/f/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/f/g;->b:Lcom/google/maps/g/f/g;

    aput-object v1, v0, v5

    const/4 v1, 0x2

    sget-object v2, Lcom/google/maps/g/f/g;->c:Lcom/google/maps/g/f/g;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/f/g;->d:Lcom/google/maps/g/f/g;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/f/g;->e:Lcom/google/maps/g/f/g;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/f/g;->f:Lcom/google/maps/g/f/g;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/f/g;->g:Lcom/google/maps/g/f/g;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/f/g;->h:Lcom/google/maps/g/f/g;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/f/g;->i:Lcom/google/maps/g/f/g;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/f/g;->k:[Lcom/google/maps/g/f/g;

    .line 114
    new-instance v0, Lcom/google/maps/g/f/h;

    invoke-direct {v0}, Lcom/google/maps/g/f/h;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 124
    iput p3, p0, Lcom/google/maps/g/f/g;->j:I

    .line 125
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/f/g;
    .locals 1

    .prologue
    .line 95
    sparse-switch p0, :sswitch_data_0

    .line 105
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 96
    :sswitch_0
    sget-object v0, Lcom/google/maps/g/f/g;->a:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 97
    :sswitch_1
    sget-object v0, Lcom/google/maps/g/f/g;->b:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 98
    :sswitch_2
    sget-object v0, Lcom/google/maps/g/f/g;->c:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 99
    :sswitch_3
    sget-object v0, Lcom/google/maps/g/f/g;->d:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 100
    :sswitch_4
    sget-object v0, Lcom/google/maps/g/f/g;->e:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 101
    :sswitch_5
    sget-object v0, Lcom/google/maps/g/f/g;->f:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 102
    :sswitch_6
    sget-object v0, Lcom/google/maps/g/f/g;->g:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 103
    :sswitch_7
    sget-object v0, Lcom/google/maps/g/f/g;->h:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 104
    :sswitch_8
    sget-object v0, Lcom/google/maps/g/f/g;->i:Lcom/google/maps/g/f/g;

    goto :goto_0

    .line 95
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x63 -> :sswitch_0
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/f/g;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/f/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/f/g;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/f/g;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/f/g;->k:[Lcom/google/maps/g/f/g;

    invoke-virtual {v0}, [Lcom/google/maps/g/f/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/f/g;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/maps/g/f/g;->j:I

    return v0
.end method
