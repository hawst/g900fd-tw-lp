.class public final Lcom/google/maps/g/ko;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/km;",
        "Lcom/google/maps/g/ko;",
        ">;",
        "Lcom/google/maps/g/kp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/lang/Object;

.field private f:I

.field private g:I

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/n/aq;

.field private j:Ljava/lang/Object;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:F

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;

.field private o:F

.field private p:Lcom/google/n/ao;

.field private q:Lcom/google/n/ao;

.field private r:Ljava/lang/Object;

.field private s:Ljava/lang/Object;

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1065
    sget-object v0, Lcom/google/maps/g/km;->v:Lcom/google/maps/g/km;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1344
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->b:Ljava/lang/Object;

    .line 1420
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->c:Ljava/lang/Object;

    .line 1496
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->d:Lcom/google/n/ao;

    .line 1555
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->e:Ljava/lang/Object;

    .line 1631
    const/16 v0, 0x63

    iput v0, p0, Lcom/google/maps/g/ko;->f:I

    .line 1667
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ko;->g:I

    .line 1703
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->h:Ljava/lang/Object;

    .line 1779
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    .line 1872
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->j:Ljava/lang/Object;

    .line 1949
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    .line 2117
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->m:Lcom/google/n/ao;

    .line 2176
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->n:Lcom/google/n/ao;

    .line 2267
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->p:Lcom/google/n/ao;

    .line 2326
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->q:Lcom/google/n/ao;

    .line 2385
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->r:Ljava/lang/Object;

    .line 2461
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ko;->s:Ljava/lang/Object;

    .line 2538
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    .line 2674
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ko;->u:Lcom/google/n/ao;

    .line 1066
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/km;)Lcom/google/maps/g/ko;
    .locals 7

    .prologue
    const/high16 v6, 0x40000

    const/high16 v5, 0x10000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1220
    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1323
    :goto_0
    return-object p0

    .line 1221
    :cond_0
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1222
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1223
    iget-object v2, p1, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->b:Ljava/lang/Object;

    .line 1226
    :cond_1
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1227
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1228
    iget-object v2, p1, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->c:Ljava/lang/Object;

    .line 1231
    :cond_2
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1232
    iget-object v2, p0, Lcom/google/maps/g/ko;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1233
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1235
    :cond_3
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1236
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1237
    iget-object v2, p1, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->e:Ljava/lang/Object;

    .line 1240
    :cond_4
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1241
    iget v2, p1, Lcom/google/maps/g/km;->f:I

    invoke-static {v2}, Lcom/google/maps/g/f/g;->a(I)Lcom/google/maps/g/f/g;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/f/g;->a:Lcom/google/maps/g/f/g;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1221
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1226
    goto :goto_2

    :cond_8
    move v2, v1

    .line 1231
    goto :goto_3

    :cond_9
    move v2, v1

    .line 1235
    goto :goto_4

    :cond_a
    move v2, v1

    .line 1240
    goto :goto_5

    .line 1241
    :cond_b
    iget v3, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/ko;->a:I

    iget v2, v2, Lcom/google/maps/g/f/g;->j:I

    iput v2, p0, Lcom/google/maps/g/ko;->f:I

    .line 1243
    :cond_c
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 1244
    iget v2, p1, Lcom/google/maps/g/km;->g:I

    invoke-static {v2}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/maps/g/f/i;->a:Lcom/google/maps/g/f/i;

    :cond_d
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 1243
    goto :goto_6

    .line 1244
    :cond_f
    iget v3, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/ko;->a:I

    iget v2, v2, Lcom/google/maps/g/f/i;->q:I

    iput v2, p0, Lcom/google/maps/g/ko;->g:I

    .line 1246
    :cond_10
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_11

    .line 1247
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1248
    iget-object v2, p1, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->h:Ljava/lang/Object;

    .line 1251
    :cond_11
    iget-object v2, p1, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_12

    .line 1252
    iget-object v2, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1253
    iget-object v2, p1, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    .line 1254
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1261
    :cond_12
    :goto_8
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_9
    if-eqz v2, :cond_13

    .line 1262
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1263
    iget-object v2, p1, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->j:Ljava/lang/Object;

    .line 1266
    :cond_13
    iget-object v2, p1, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 1267
    iget-object v2, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1268
    iget-object v2, p1, Lcom/google/maps/g/km;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    .line 1269
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1276
    :cond_14
    :goto_a
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_25

    move v2, v0

    :goto_b
    if-eqz v2, :cond_15

    .line 1277
    iget v2, p1, Lcom/google/maps/g/km;->l:F

    iget v3, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/g/ko;->a:I

    iput v2, p0, Lcom/google/maps/g/ko;->l:F

    .line 1279
    :cond_15
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_c
    if-eqz v2, :cond_16

    .line 1280
    iget-object v2, p0, Lcom/google/maps/g/ko;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1281
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1283
    :cond_16
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_27

    move v2, v0

    :goto_d
    if-eqz v2, :cond_17

    .line 1284
    iget-object v2, p0, Lcom/google/maps/g/ko;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1285
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1287
    :cond_17
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_e
    if-eqz v2, :cond_18

    .line 1288
    iget v2, p1, Lcom/google/maps/g/km;->o:F

    iget v3, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/ko;->a:I

    iput v2, p0, Lcom/google/maps/g/ko;->o:F

    .line 1290
    :cond_18
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_f
    if-eqz v2, :cond_19

    .line 1291
    iget-object v2, p0, Lcom/google/maps/g/ko;->p:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1292
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1294
    :cond_19
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_2a

    move v2, v0

    :goto_10
    if-eqz v2, :cond_1a

    .line 1295
    iget-object v2, p0, Lcom/google/maps/g/ko;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1296
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1298
    :cond_1a
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_2b

    move v2, v0

    :goto_11
    if-eqz v2, :cond_1b

    .line 1299
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1300
    iget-object v2, p1, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->r:Ljava/lang/Object;

    .line 1303
    :cond_1b
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_2c

    move v2, v0

    :goto_12
    if-eqz v2, :cond_1c

    .line 1304
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1305
    iget-object v2, p1, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ko;->s:Ljava/lang/Object;

    .line 1308
    :cond_1c
    iget-object v2, p1, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1309
    iget-object v2, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 1310
    iget-object v2, p1, Lcom/google/maps/g/km;->t:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    .line 1311
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    const v3, -0x40001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1318
    :cond_1d
    :goto_13
    iget v2, p1, Lcom/google/maps/g/km;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2f

    :goto_14
    if-eqz v0, :cond_1e

    .line 1319
    iget-object v0, p0, Lcom/google/maps/g/ko;->u:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1320
    iget v0, p0, Lcom/google/maps/g/ko;->a:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/g/ko;->a:I

    .line 1322
    :cond_1e
    iget-object v0, p1, Lcom/google/maps/g/km;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1f
    move v2, v1

    .line 1246
    goto/16 :goto_7

    .line 1256
    :cond_20
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-eq v2, v3, :cond_21

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1257
    :cond_21
    iget-object v2, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_22
    move v2, v1

    .line 1261
    goto/16 :goto_9

    .line 1271
    :cond_23
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-eq v2, v3, :cond_24

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1272
    :cond_24
    iget-object v2, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_25
    move v2, v1

    .line 1276
    goto/16 :goto_b

    :cond_26
    move v2, v1

    .line 1279
    goto/16 :goto_c

    :cond_27
    move v2, v1

    .line 1283
    goto/16 :goto_d

    :cond_28
    move v2, v1

    .line 1287
    goto/16 :goto_e

    :cond_29
    move v2, v1

    .line 1290
    goto/16 :goto_f

    :cond_2a
    move v2, v1

    .line 1294
    goto/16 :goto_10

    :cond_2b
    move v2, v1

    .line 1298
    goto/16 :goto_11

    :cond_2c
    move v2, v1

    .line 1303
    goto/16 :goto_12

    .line 1313
    :cond_2d
    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    and-int/2addr v2, v6

    if-eq v2, v6, :cond_2e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ko;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/maps/g/ko;->a:I

    .line 1314
    :cond_2e
    iget-object v2, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_13

    :cond_2f
    move v0, v1

    .line 1318
    goto/16 :goto_14
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/high16 v9, 0x20000

    const/4 v0, 0x1

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1057
    new-instance v2, Lcom/google/maps/g/km;

    invoke-direct {v2, p0}, Lcom/google/maps/g/km;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_13

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/ko;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ko;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ko;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ko;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/ko;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/g/ko;->f:I

    iput v4, v2, Lcom/google/maps/g/km;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/maps/g/ko;->g:I

    iput v4, v2, Lcom/google/maps/g/km;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/ko;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/maps/g/ko;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/ko;->i:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/ko;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/maps/g/ko;->a:I

    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/ko;->k:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/km;->k:Ljava/util/List;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget v4, p0, Lcom/google/maps/g/ko;->l:F

    iput v4, v2, Lcom/google/maps/g/km;->l:F

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-object v4, v2, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ko;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ko;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-object v4, v2, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ko;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ko;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x800

    :cond_c
    iget v4, p0, Lcom/google/maps/g/ko;->o:F

    iput v4, v2, Lcom/google/maps/g/km;->o:F

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x1000

    :cond_d
    iget-object v4, v2, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ko;->p:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ko;->p:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/lit16 v0, v0, 0x2000

    :cond_e
    iget-object v4, v2, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ko;->q:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ko;->q:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    or-int/lit16 v0, v0, 0x4000

    :cond_f
    iget-object v4, p0, Lcom/google/maps/g/ko;->r:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    or-int/2addr v0, v7

    :cond_10
    iget-object v4, p0, Lcom/google/maps/g/ko;->s:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    const/high16 v5, 0x40000

    if-ne v4, v5, :cond_11

    iget-object v4, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ko;->a:I

    const v5, -0x40001

    and-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/g/ko;->a:I

    :cond_11
    iget-object v4, p0, Lcom/google/maps/g/ko;->t:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/km;->t:Ljava/util/List;

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    const/high16 v4, 0x80000

    if-ne v3, v4, :cond_12

    or-int/2addr v0, v8

    :cond_12
    iget-object v3, v2, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/ko;->u:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/ko;->u:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/km;->a:I

    return-object v2

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1057
    check-cast p1, Lcom/google/maps/g/km;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ko;->a(Lcom/google/maps/g/km;)Lcom/google/maps/g/ko;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x80000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1327
    iget v0, p0, Lcom/google/maps/g/ko;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1328
    iget-object v0, p0, Lcom/google/maps/g/ko;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/m;->d()Lcom/google/geo/b/m;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/m;

    invoke-virtual {v0}, Lcom/google/geo/b/m;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1339
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1327
    goto :goto_0

    .line 1333
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ko;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 1334
    iget-object v0, p0, Lcom/google/maps/g/ko;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ku;->d()Lcom/google/maps/g/ku;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ku;

    invoke-virtual {v0}, Lcom/google/maps/g/ku;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1336
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1333
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1339
    goto :goto_1
.end method
