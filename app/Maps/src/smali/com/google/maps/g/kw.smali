.class public final Lcom/google/maps/g/kw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ku;",
        "Lcom/google/maps/g/kw;",
        ">;",
        "Lcom/google/maps/g/kx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 335
    sget-object v0, Lcom/google/maps/g/ku;->f:Lcom/google/maps/g/ku;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 454
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/kw;->c:Lcom/google/n/ao;

    .line 513
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/kw;->d:Lcom/google/n/ao;

    .line 573
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    .line 336
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ku;)Lcom/google/maps/g/kw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 384
    invoke-static {}, Lcom/google/maps/g/ku;->d()Lcom/google/maps/g/ku;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 407
    :goto_0
    return-object p0

    .line 385
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 386
    iget-boolean v2, p1, Lcom/google/maps/g/ku;->b:Z

    iget v3, p0, Lcom/google/maps/g/kw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/kw;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/kw;->b:Z

    .line 388
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 389
    iget-object v2, p0, Lcom/google/maps/g/kw;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 390
    iget v2, p0, Lcom/google/maps/g/kw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/kw;->a:I

    .line 392
    :cond_2
    iget v2, p1, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 393
    iget-object v0, p0, Lcom/google/maps/g/kw;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 394
    iget v0, p0, Lcom/google/maps/g/kw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/kw;->a:I

    .line 396
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 397
    iget-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 398
    iget-object v0, p1, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    .line 399
    iget v0, p0, Lcom/google/maps/g/kw;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/g/kw;->a:I

    .line 406
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/maps/g/ku;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 385
    goto :goto_1

    :cond_6
    move v2, v1

    .line 388
    goto :goto_2

    :cond_7
    move v0, v1

    .line 392
    goto :goto_3

    .line 401
    :cond_8
    iget v0, p0, Lcom/google/maps/g/kw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/kw;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/kw;->a:I

    .line 402
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 327
    new-instance v2, Lcom/google/maps/g/ku;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ku;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/kw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-boolean v4, p0, Lcom/google/maps/g/kw;->b:Z

    iput-boolean v4, v2, Lcom/google/maps/g/ku;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/kw;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/kw;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/kw;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/kw;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/kw;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kw;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/kw;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/ku;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 327
    check-cast p1, Lcom/google/maps/g/ku;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/kw;->a(Lcom/google/maps/g/ku;)Lcom/google/maps/g/kw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 411
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/google/maps/g/kw;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/e;->d()Lcom/google/geo/b/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/e;

    invoke-virtual {v0}, Lcom/google/geo/b/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    :goto_1
    return v2

    .line 411
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 417
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
