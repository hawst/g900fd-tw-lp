.class public final Lcom/google/maps/g/po;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/po;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/po;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field c:I

.field d:Z

.field e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/maps/g/pp;

    invoke-direct {v0}, Lcom/google/maps/g/pp;-><init>()V

    sput-object v0, Lcom/google/maps/g/po;->PARSER:Lcom/google/n/ax;

    .line 304
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/po;->i:Lcom/google/n/aw;

    .line 633
    new-instance v0, Lcom/google/maps/g/po;

    invoke-direct {v0}, Lcom/google/maps/g/po;-><init>()V

    sput-object v0, Lcom/google/maps/g/po;->f:Lcom/google/maps/g/po;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 247
    iput-byte v0, p0, Lcom/google/maps/g/po;->g:B

    .line 275
    iput v0, p0, Lcom/google/maps/g/po;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/po;->c:I

    .line 20
    iput-boolean v1, p0, Lcom/google/maps/g/po;->d:Z

    .line 21
    iput-boolean v1, p0, Lcom/google/maps/g/po;->e:Z

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/po;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 49
    iget v5, p0, Lcom/google/maps/g/po;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/po;->a:I

    .line 50
    iput-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/po;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 55
    invoke-static {v0}, Lcom/google/maps/g/pr;->a(I)Lcom/google/maps/g/pr;

    move-result-object v5

    .line 56
    if-nez v5, :cond_1

    .line 57
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 78
    :catch_1
    move-exception v0

    .line 79
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 80
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_1
    :try_start_4
    iget v5, p0, Lcom/google/maps/g/po;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/po;->a:I

    .line 60
    iput v0, p0, Lcom/google/maps/g/po;->c:I

    goto :goto_0

    .line 65
    :sswitch_3
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/po;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/po;->d:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 70
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/po;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/po;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 82
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/po;->au:Lcom/google/n/bn;

    .line 83
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 247
    iput-byte v0, p0, Lcom/google/maps/g/po;->g:B

    .line 275
    iput v0, p0, Lcom/google/maps/g/po;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/po;
    .locals 1

    .prologue
    .line 636
    sget-object v0, Lcom/google/maps/g/po;->f:Lcom/google/maps/g/po;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/pq;
    .locals 1

    .prologue
    .line 366
    new-instance v0, Lcom/google/maps/g/pq;

    invoke-direct {v0}, Lcom/google/maps/g/pq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/po;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/google/maps/g/po;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/google/maps/g/po;->c()I

    .line 260
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 263
    :cond_0
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 264
    iget v0, p0, Lcom/google/maps/g/po;->c:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 266
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 267
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/po;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 269
    :cond_2
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 270
    iget-boolean v0, p0, Lcom/google/maps/g/po;->e:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    :goto_3
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/po;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 273
    return-void

    .line 261
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 264
    :cond_5
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 267
    goto :goto_2

    :cond_7
    move v1, v2

    .line 270
    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 249
    iget-byte v1, p0, Lcom/google/maps/g/po;->g:B

    .line 250
    if-ne v1, v0, :cond_0

    .line 254
    :goto_0
    return v0

    .line 251
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 253
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/po;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 277
    iget v0, p0, Lcom/google/maps/g/po;->h:I

    .line 278
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 299
    :goto_0
    return v0

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 283
    iget-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 285
    :goto_2
    iget v2, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 286
    iget v2, p0, Lcom/google/maps/g/po;->c:I

    .line 287
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 289
    :cond_1
    iget v2, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 290
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/po;->d:Z

    .line 291
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 293
    :cond_2
    iget v2, p0, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 294
    iget-boolean v2, p0, Lcom/google/maps/g/po;->e:Z

    .line 295
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 297
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/po;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    iput v0, p0, Lcom/google/maps/g/po;->h:I

    goto :goto_0

    .line 283
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 287
    :cond_5
    const/16 v2, 0xa

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/po;->newBuilder()Lcom/google/maps/g/pq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/pq;->a(Lcom/google/maps/g/po;)Lcom/google/maps/g/pq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/po;->newBuilder()Lcom/google/maps/g/pq;

    move-result-object v0

    return-object v0
.end method
