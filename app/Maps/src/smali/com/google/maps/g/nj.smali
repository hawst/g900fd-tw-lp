.class public final Lcom/google/maps/g/nj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nj;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/nj;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/f;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/maps/g/nk;

    invoke-direct {v0}, Lcom/google/maps/g/nk;-><init>()V

    sput-object v0, Lcom/google/maps/g/nj;->PARSER:Lcom/google/n/ax;

    .line 239
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/nj;->h:Lcom/google/n/aw;

    .line 572
    new-instance v0, Lcom/google/maps/g/nj;

    invoke-direct {v0}, Lcom/google/maps/g/nj;-><init>()V

    sput-object v0, Lcom/google/maps/g/nj;->e:Lcom/google/maps/g/nj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 189
    iput-byte v0, p0, Lcom/google/maps/g/nj;->f:B

    .line 214
    iput v0, p0, Lcom/google/maps/g/nj;->g:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/nj;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/nj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/nj;->a:I

    .line 48
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nj;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 53
    iget v4, p0, Lcom/google/maps/g/nj;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/nj;->a:I

    .line 54
    iput-object v3, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 69
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 59
    iget v4, p0, Lcom/google/maps/g/nj;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/nj;->a:I

    .line 60
    iput-object v3, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nj;->au:Lcom/google/n/bn;

    .line 72
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 189
    iput-byte v0, p0, Lcom/google/maps/g/nj;->f:B

    .line 214
    iput v0, p0, Lcom/google/maps/g/nj;->g:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/nj;
    .locals 1

    .prologue
    .line 575
    sget-object v0, Lcom/google/maps/g/nj;->e:Lcom/google/maps/g/nj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/nl;
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/google/maps/g/nl;

    invoke-direct {v0}, Lcom/google/maps/g/nl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/google/maps/g/nj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 201
    invoke-virtual {p0}, Lcom/google/maps/g/nj;->c()I

    .line 202
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 205
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 208
    :cond_1
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/nj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 212
    return-void

    .line 206
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 209
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 191
    iget-byte v1, p0, Lcom/google/maps/g/nj;->f:B

    .line 192
    if-ne v1, v0, :cond_0

    .line 196
    :goto_0
    return v0

    .line 193
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/nj;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 216
    iget v0, p0, Lcom/google/maps/g/nj;->g:I

    .line 217
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 234
    :goto_0
    return v0

    .line 220
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_5

    .line 221
    iget-object v0, p0, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    .line 222
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 224
    :goto_1
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 228
    :cond_1
    iget v0, p0, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 229
    const/4 v3, 0x3

    .line 230
    iget-object v0, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/nj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 233
    iput v0, p0, Lcom/google/maps/g/nj;->g:I

    goto/16 :goto_0

    .line 226
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 230
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    .line 160
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    check-cast v0, Ljava/lang/String;

    .line 169
    :goto_0
    return-object v0

    .line 163
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 165
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iput-object v1, p0, Lcom/google/maps/g/nj;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 169
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/nj;->newBuilder()Lcom/google/maps/g/nl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/nl;->a(Lcom/google/maps/g/nj;)Lcom/google/maps/g/nl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/nj;->newBuilder()Lcom/google/maps/g/nl;

    move-result-object v0

    return-object v0
.end method
