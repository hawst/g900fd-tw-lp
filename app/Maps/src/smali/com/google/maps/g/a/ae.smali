.class public final Lcom/google/maps/g/a/ae;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ah;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ac;",
        "Lcom/google/maps/g/a/ae;",
        ">;",
        "Lcom/google/maps/g/a/ah;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 736
    sget-object v0, Lcom/google/maps/g/a/ac;->j:Lcom/google/maps/g/a/ac;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 853
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/ae;->b:I

    .line 889
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->c:Ljava/lang/Object;

    .line 965
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->e:Ljava/lang/Object;

    .line 1041
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->f:Ljava/lang/Object;

    .line 1117
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->g:Ljava/lang/Object;

    .line 1193
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->d:Ljava/lang/Object;

    .line 1269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->h:Ljava/lang/Object;

    .line 1345
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->i:Ljava/lang/Object;

    .line 737
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ac;)Lcom/google/maps/g/a/ae;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 804
    invoke-static {}, Lcom/google/maps/g/a/ac;->d()Lcom/google/maps/g/a/ac;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 844
    :goto_0
    return-object p0

    .line 805
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 806
    iget v2, p1, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 805
    goto :goto_1

    .line 806
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/ae;->a:I

    iget v2, v2, Lcom/google/maps/g/a/af;->k:I

    iput v2, p0, Lcom/google/maps/g/a/ae;->b:I

    .line 808
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 809
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 810
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->c:Ljava/lang/Object;

    .line 813
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 814
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 815
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->e:Ljava/lang/Object;

    .line 818
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 819
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 820
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->f:Ljava/lang/Object;

    .line 823
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 824
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 825
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->g:Ljava/lang/Object;

    .line 828
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 829
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 830
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->d:Ljava/lang/Object;

    .line 833
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 834
    iget v2, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 835
    iget-object v2, p1, Lcom/google/maps/g/a/ac;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/ae;->h:Ljava/lang/Object;

    .line 838
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 839
    iget v0, p0, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/ae;->a:I

    .line 840
    iget-object v0, p1, Lcom/google/maps/g/a/ac;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/ae;->i:Ljava/lang/Object;

    .line 843
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/a/ac;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 808
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 813
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 818
    goto :goto_4

    :cond_f
    move v2, v1

    .line 823
    goto :goto_5

    :cond_10
    move v2, v1

    .line 828
    goto :goto_6

    :cond_11
    move v2, v1

    .line 833
    goto :goto_7

    :cond_12
    move v0, v1

    .line 838
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 728
    new-instance v2, Lcom/google/maps/g/a/ac;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ac;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/ae;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/ae;->b:I

    iput v1, v2, Lcom/google/maps/g/a/ac;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/a/ae;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->i:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/a/ac;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 728
    check-cast p1, Lcom/google/maps/g/a/ac;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/ae;->a(Lcom/google/maps/g/a/ac;)Lcom/google/maps/g/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 848
    const/4 v0, 0x1

    return v0
.end method
