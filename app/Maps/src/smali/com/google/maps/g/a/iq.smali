.class public final Lcom/google/maps/g/a/iq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ir;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/io;",
        "Lcom/google/maps/g/a/iq;",
        ">;",
        "Lcom/google/maps/g/a/ir;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:I

.field private d:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/maps/g/a/io;->e:Lcom/google/maps/g/a/io;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 321
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/iq;->b:Lcom/google/n/f;

    .line 388
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/iq;->d:Lcom/google/n/f;

    .line 264
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/io;)Lcom/google/maps/g/a/iq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    invoke-static {}, Lcom/google/maps/g/a/io;->d()Lcom/google/maps/g/a/io;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 312
    :goto_0
    return-object p0

    .line 302
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/io;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 303
    iget-object v2, p1, Lcom/google/maps/g/a/io;->b:Lcom/google/n/f;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 302
    goto :goto_1

    .line 303
    :cond_2
    iget v3, p0, Lcom/google/maps/g/a/iq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/iq;->a:I

    iput-object v2, p0, Lcom/google/maps/g/a/iq;->b:Lcom/google/n/f;

    .line 305
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/io;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    .line 306
    iget v2, p1, Lcom/google/maps/g/a/io;->c:I

    iget v3, p0, Lcom/google/maps/g/a/iq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/iq;->a:I

    iput v2, p0, Lcom/google/maps/g/a/iq;->c:I

    .line 308
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/io;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 309
    iget-object v0, p1, Lcom/google/maps/g/a/io;->d:Lcom/google/n/f;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 305
    goto :goto_2

    :cond_6
    move v0, v1

    .line 308
    goto :goto_3

    .line 309
    :cond_7
    iget v1, p0, Lcom/google/maps/g/a/iq;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/iq;->a:I

    iput-object v0, p0, Lcom/google/maps/g/a/iq;->d:Lcom/google/n/f;

    .line 311
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/a/io;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 255
    new-instance v2, Lcom/google/maps/g/a/io;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/io;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/iq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/iq;->b:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/a/io;->b:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/a/iq;->c:I

    iput v1, v2, Lcom/google/maps/g/a/io;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/iq;->d:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/a/io;->d:Lcom/google/n/f;

    iput v0, v2, Lcom/google/maps/g/a/io;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 255
    check-cast p1, Lcom/google/maps/g/a/io;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/iq;->a(Lcom/google/maps/g/a/io;)Lcom/google/maps/g/a/iq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method
