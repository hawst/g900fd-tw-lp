.class public final enum Lcom/google/maps/g/tc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/tc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/tc;

.field public static final enum b:Lcom/google/maps/g/tc;

.field public static final enum c:Lcom/google/maps/g/tc;

.field private static final synthetic e:[Lcom/google/maps/g/tc;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 364
    new-instance v0, Lcom/google/maps/g/tc;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/tc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/tc;->a:Lcom/google/maps/g/tc;

    .line 365
    new-instance v0, Lcom/google/maps/g/tc;

    const-string v1, "TRANSIT_TRIP"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/tc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/tc;->b:Lcom/google/maps/g/tc;

    .line 366
    new-instance v0, Lcom/google/maps/g/tc;

    const-string v1, "RESULT_NOT_SET"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/maps/g/tc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/tc;->c:Lcom/google/maps/g/tc;

    .line 362
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/tc;

    sget-object v1, Lcom/google/maps/g/tc;->a:Lcom/google/maps/g/tc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/tc;->b:Lcom/google/maps/g/tc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/tc;->c:Lcom/google/maps/g/tc;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/tc;->e:[Lcom/google/maps/g/tc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 368
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 367
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/tc;->d:I

    .line 369
    iput p3, p0, Lcom/google/maps/g/tc;->d:I

    .line 370
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/tc;
    .locals 2

    .prologue
    .line 372
    packed-switch p0, :pswitch_data_0

    .line 376
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/tc;->a:Lcom/google/maps/g/tc;

    .line 375
    :goto_0
    return-object v0

    .line 374
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/tc;->b:Lcom/google/maps/g/tc;

    goto :goto_0

    .line 375
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/tc;->c:Lcom/google/maps/g/tc;

    goto :goto_0

    .line 372
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/tc;
    .locals 1

    .prologue
    .line 362
    const-class v0, Lcom/google/maps/g/tc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tc;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/tc;
    .locals 1

    .prologue
    .line 362
    sget-object v0, Lcom/google/maps/g/tc;->e:[Lcom/google/maps/g/tc;

    invoke-virtual {v0}, [Lcom/google/maps/g/tc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/tc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/google/maps/g/tc;->d:I

    return v0
.end method
