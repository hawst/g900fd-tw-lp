.class public final enum Lcom/google/maps/g/om;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/om;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/om;

.field public static final enum b:Lcom/google/maps/g/om;

.field public static final enum c:Lcom/google/maps/g/om;

.field public static final enum d:Lcom/google/maps/g/om;

.field public static final enum e:Lcom/google/maps/g/om;

.field public static final enum f:Lcom/google/maps/g/om;

.field public static final enum g:Lcom/google/maps/g/om;

.field private static final synthetic i:[Lcom/google/maps/g/om;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 145
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_ROAD"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    .line 149
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_POLITICAL"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->b:Lcom/google/maps/g/om;

    .line 153
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_NATURAL"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->c:Lcom/google/maps/g/om;

    .line 157
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_TRANSIT_STATION"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    .line 161
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_GEOCODED_ADDRESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->e:Lcom/google/maps/g/om;

    .line 165
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "TYPE_ESTABLISHMENT"

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    .line 169
    new-instance v0, Lcom/google/maps/g/om;

    const-string v1, "UNSUPPORTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/om;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    .line 140
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/g/om;

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/om;->b:Lcom/google/maps/g/om;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/om;->c:Lcom/google/maps/g/om;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/om;->e:Lcom/google/maps/g/om;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/om;->i:[Lcom/google/maps/g/om;

    .line 224
    new-instance v0, Lcom/google/maps/g/on;

    invoke-direct {v0}, Lcom/google/maps/g/on;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 234
    iput p3, p0, Lcom/google/maps/g/om;->h:I

    .line 235
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/om;
    .locals 1

    .prologue
    .line 207
    packed-switch p0, :pswitch_data_0

    .line 215
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 208
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    goto :goto_0

    .line 209
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/om;->b:Lcom/google/maps/g/om;

    goto :goto_0

    .line 210
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/om;->c:Lcom/google/maps/g/om;

    goto :goto_0

    .line 211
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    goto :goto_0

    .line 212
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/om;->e:Lcom/google/maps/g/om;

    goto :goto_0

    .line 213
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    goto :goto_0

    .line 214
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/om;
    .locals 1

    .prologue
    .line 140
    const-class v0, Lcom/google/maps/g/om;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/om;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/om;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/maps/g/om;->i:[Lcom/google/maps/g/om;

    invoke-virtual {v0}, [Lcom/google/maps/g/om;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/om;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/google/maps/g/om;->h:I

    return v0
.end method
