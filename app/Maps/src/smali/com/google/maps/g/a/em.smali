.class public final Lcom/google/maps/g/a/em;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/en;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ek;",
        "Lcom/google/maps/g/a/em;",
        ">;",
        "Lcom/google/maps/g/a/en;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/google/maps/g/a/ek;->f:Lcom/google/maps/g/a/ek;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 370
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/em;->b:Lcom/google/n/ao;

    .line 429
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/em;->c:Lcom/google/n/ao;

    .line 488
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/em;->d:Lcom/google/n/ao;

    .line 295
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 344
    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 361
    :goto_0
    return-object p0

    .line 345
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 346
    iget-object v2, p0, Lcom/google/maps/g/a/em;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ek;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 347
    iget v2, p0, Lcom/google/maps/g/a/em;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/em;->a:I

    .line 349
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 350
    iget-object v2, p0, Lcom/google/maps/g/a/em;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ek;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 351
    iget v2, p0, Lcom/google/maps/g/a/em;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/em;->a:I

    .line 353
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 354
    iget-object v2, p0, Lcom/google/maps/g/a/em;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ek;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 355
    iget v2, p0, Lcom/google/maps/g/a/em;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/em;->a:I

    .line 357
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 358
    iget-boolean v0, p1, Lcom/google/maps/g/a/ek;->e:Z

    iget v1, p0, Lcom/google/maps/g/a/em;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/a/em;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/em;->e:Z

    .line 360
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/a/ek;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 345
    goto :goto_1

    :cond_6
    move v2, v1

    .line 349
    goto :goto_2

    :cond_7
    move v2, v1

    .line 353
    goto :goto_3

    :cond_8
    move v0, v1

    .line 357
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/google/maps/g/a/em;->c()Lcom/google/maps/g/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 286
    check-cast p1, Lcom/google/maps/g/a/ek;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/em;->a(Lcom/google/maps/g/a/ek;)Lcom/google/maps/g/a/em;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/ek;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 314
    new-instance v2, Lcom/google/maps/g/a/ek;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ek;-><init>(Lcom/google/n/v;)V

    .line 315
    iget v3, p0, Lcom/google/maps/g/a/em;->a:I

    .line 317
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 320
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/ek;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/em;->b:Lcom/google/n/ao;

    .line 321
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/em;->b:Lcom/google/n/ao;

    .line 322
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 320
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 323
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 324
    or-int/lit8 v0, v0, 0x2

    .line 326
    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/ek;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/em;->c:Lcom/google/n/ao;

    .line 327
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/em;->c:Lcom/google/n/ao;

    .line 328
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 326
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 329
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 330
    or-int/lit8 v0, v0, 0x4

    .line 332
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/ek;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/em;->d:Lcom/google/n/ao;

    .line 333
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/em;->d:Lcom/google/n/ao;

    .line 334
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 332
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 335
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 336
    or-int/lit8 v0, v0, 0x8

    .line 338
    :cond_2
    iget-boolean v1, p0, Lcom/google/maps/g/a/em;->e:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/ek;->e:Z

    .line 339
    iput v0, v2, Lcom/google/maps/g/a/ek;->a:I

    .line 340
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
