.class public final Lcom/google/maps/g/ei;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ep;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ei;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/ei;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:I

.field d:Ljava/lang/Object;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:I

.field h:Z

.field i:Z

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/maps/g/ej;

    invoke-direct {v0}, Lcom/google/maps/g/ej;-><init>()V

    sput-object v0, Lcom/google/maps/g/ei;->PARSER:Lcom/google/n/ax;

    .line 583
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ei;->m:Lcom/google/n/aw;

    .line 1186
    new-instance v0, Lcom/google/maps/g/ei;

    invoke-direct {v0}, Lcom/google/maps/g/ei;-><init>()V

    sput-object v0, Lcom/google/maps/g/ei;->j:Lcom/google/maps/g/ei;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 421
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    .line 437
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    .line 498
    iput-byte v3, p0, Lcom/google/maps/g/ei;->k:B

    .line 538
    iput v3, p0, Lcom/google/maps/g/ei;->l:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    .line 19
    iput v2, p0, Lcom/google/maps/g/ei;->c:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput v2, p0, Lcom/google/maps/g/ei;->g:I

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/g/ei;->h:Z

    .line 25
    iput-boolean v2, p0, Lcom/google/maps/g/ei;->i:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/ei;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 53
    iget v5, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/ei;->a:I

    .line 54
    iput-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ei;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 59
    iget v5, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/ei;->a:I

    .line 60
    iput-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 111
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 65
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/ei;->a:I

    goto :goto_0

    .line 69
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 70
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/ei;->a:I

    goto :goto_0

    .line 74
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 75
    invoke-static {v0}, Lcom/google/maps/g/el;->a(I)Lcom/google/maps/g/el;

    move-result-object v5

    .line 76
    if-nez v5, :cond_1

    .line 77
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 79
    :cond_1
    iget v5, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/ei;->a:I

    .line 80
    iput v0, p0, Lcom/google/maps/g/ei;->g:I

    goto/16 :goto_0

    .line 85
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/ei;->a:I

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/ei;->h:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 90
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/ei;->a:I

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/ei;->i:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 95
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 96
    invoke-static {v0}, Lcom/google/maps/g/en;->a(I)Lcom/google/maps/g/en;

    move-result-object v5

    .line 97
    if-nez v5, :cond_4

    .line 98
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 100
    :cond_4
    iget v5, p0, Lcom/google/maps/g/ei;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/ei;->a:I

    .line 101
    iput v0, p0, Lcom/google/maps/g/ei;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 113
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ei;->au:Lcom/google/n/bn;

    .line 114
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 421
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    .line 437
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    .line 498
    iput-byte v1, p0, Lcom/google/maps/g/ei;->k:B

    .line 538
    iput v1, p0, Lcom/google/maps/g/ei;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/ei;
    .locals 1

    .prologue
    .line 1189
    sget-object v0, Lcom/google/maps/g/ei;->j:Lcom/google/maps/g/ei;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ek;
    .locals 1

    .prologue
    .line 645
    new-instance v0, Lcom/google/maps/g/ek;

    invoke-direct {v0}, Lcom/google/maps/g/ek;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ei;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/google/maps/g/ei;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 510
    invoke-virtual {p0}, Lcom/google/maps/g/ei;->c()I

    .line 511
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 512
    iget-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 514
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 515
    iget-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 517
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 518
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 520
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 521
    iget-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 523
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 524
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/g/ei;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_a

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 526
    :cond_4
    :goto_2
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 527
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/ei;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_b

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 529
    :cond_5
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 530
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/ei;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_c

    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 532
    :cond_6
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_7

    .line 533
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/g/ei;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 535
    :cond_7
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/ei;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 536
    return-void

    .line 512
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 515
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 524
    :cond_a
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_b
    move v0, v2

    .line 527
    goto :goto_3

    :cond_c
    move v1, v2

    .line 530
    goto :goto_4

    .line 533
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 500
    iget-byte v1, p0, Lcom/google/maps/g/ei;->k:B

    .line 501
    if-ne v1, v0, :cond_0

    .line 505
    :goto_0
    return v0

    .line 502
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 504
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/ei;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v3, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 540
    iget v0, p0, Lcom/google/maps/g/ei;->l:I

    .line 541
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 578
    :goto_0
    return v0

    .line 544
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 546
    iget-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 548
    :goto_2
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 550
    iget-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 552
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 553
    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    .line 554
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 556
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 557
    iget-object v0, p0, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    .line 558
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 560
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 561
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/maps/g/ei;->g:I

    .line 562
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 564
    :cond_4
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 565
    const/4 v0, 0x6

    iget-boolean v4, p0, Lcom/google/maps/g/ei;->h:Z

    .line 566
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 568
    :cond_5
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_6

    .line 569
    const/4 v0, 0x7

    iget-boolean v4, p0, Lcom/google/maps/g/ei;->i:Z

    .line 570
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 572
    :cond_6
    iget v0, p0, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 573
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/maps/g/ei;->c:I

    .line 574
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_7
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 576
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/ei;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 577
    iput v0, p0, Lcom/google/maps/g/ei;->l:I

    goto/16 :goto_0

    .line 546
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 550
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_b
    move v0, v3

    .line 562
    goto :goto_4

    :cond_c
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ei;->newBuilder()Lcom/google/maps/g/ek;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ek;->a(Lcom/google/maps/g/ei;)Lcom/google/maps/g/ek;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ei;->newBuilder()Lcom/google/maps/g/ek;

    move-result-object v0

    return-object v0
.end method
