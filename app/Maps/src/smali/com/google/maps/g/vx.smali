.class public final Lcom/google/maps/g/vx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vx;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/vx;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/google/maps/g/vy;

    invoke-direct {v0}, Lcom/google/maps/g/vy;-><init>()V

    sput-object v0, Lcom/google/maps/g/vx;->PARSER:Lcom/google/n/ax;

    .line 906
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/vx;->k:Lcom/google/n/aw;

    .line 1878
    new-instance v0, Lcom/google/maps/g/vx;

    invoke-direct {v0}, Lcom/google/maps/g/vx;-><init>()V

    sput-object v0, Lcom/google/maps/g/vx;->h:Lcom/google/maps/g/vx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 835
    iput-byte v0, p0, Lcom/google/maps/g/vx;->i:B

    .line 869
    iput v0, p0, Lcom/google/maps/g/vx;->j:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v10, 0x20

    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/vx;-><init>()V

    .line 31
    const/4 v1, 0x0

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 35
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 36
    :cond_0
    :goto_0
    if-nez v2, :cond_7

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 38
    sparse-switch v1, :sswitch_data_0

    .line 43
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 40
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_10

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 53
    or-int/lit8 v1, v0, 0x4

    .line 55
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 56
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 55
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 57
    goto :goto_0

    .line 60
    :sswitch_2
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_f

    .line 61
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63
    or-int/lit8 v1, v0, 0x8

    .line 65
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 65
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 67
    goto :goto_0

    .line 70
    :sswitch_3
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v9, :cond_e

    .line 71
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 73
    or-int/lit8 v1, v0, 0x10

    .line 75
    :goto_3
    :try_start_5
    iget-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 75
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 77
    goto :goto_0

    .line 80
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 81
    iget v4, p0, Lcom/google/maps/g/vx;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/vx;->a:I

    .line 82
    iput-object v1, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 108
    :goto_4
    :try_start_7
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 113
    :catchall_0
    move-exception v0

    :goto_5
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_1

    .line 114
    iget-object v2, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    .line 116
    :cond_1
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_2

    .line 117
    iget-object v2, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    .line 119
    :cond_2
    and-int/lit8 v2, v1, 0x10

    if-ne v2, v9, :cond_3

    .line 120
    iget-object v2, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    .line 122
    :cond_3
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v6, :cond_4

    .line 123
    iget-object v2, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    .line 125
    :cond_4
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v10, :cond_5

    .line 126
    iget-object v1, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    .line 128
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->au:Lcom/google/n/bn;

    throw v0

    .line 86
    :sswitch_5
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v6, :cond_d

    .line 87
    :try_start_8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 89
    or-int/lit8 v1, v0, 0x2

    .line 91
    :goto_6
    :try_start_9
    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 91
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 93
    goto/16 :goto_0

    .line 96
    :sswitch_6
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v10, :cond_6

    .line 97
    :try_start_a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    .line 99
    or-int/lit8 v0, v0, 0x20

    .line 101
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 101
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 109
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 110
    :goto_7
    :try_start_b
    new-instance v2, Lcom/google/n/ak;

    .line 111
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 113
    :cond_7
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v7, :cond_8

    .line 114
    iget-object v1, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    .line 116
    :cond_8
    and-int/lit8 v1, v0, 0x8

    if-ne v1, v8, :cond_9

    .line 117
    iget-object v1, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    .line 119
    :cond_9
    and-int/lit8 v1, v0, 0x10

    if-ne v1, v9, :cond_a

    .line 120
    iget-object v1, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    .line 122
    :cond_a
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v6, :cond_b

    .line 123
    iget-object v1, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    .line 125
    :cond_b
    and-int/lit8 v0, v0, 0x20

    if-ne v0, v10, :cond_c

    .line 126
    iget-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    .line 128
    :cond_c
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->au:Lcom/google/n/bn;

    .line 129
    return-void

    .line 113
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto/16 :goto_5

    .line 109
    :catch_2
    move-exception v0

    goto :goto_7

    .line 107
    :catch_3
    move-exception v0

    goto/16 :goto_4

    :cond_d
    move v1, v0

    goto/16 :goto_6

    :cond_e
    move v1, v0

    goto/16 :goto_3

    :cond_f
    move v1, v0

    goto/16 :goto_2

    :cond_10
    move v1, v0

    goto/16 :goto_1

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 835
    iput-byte v0, p0, Lcom/google/maps/g/vx;->i:B

    .line 869
    iput v0, p0, Lcom/google/maps/g/vx;->j:I

    .line 16
    return-void
.end method

.method public static i()Lcom/google/maps/g/vx;
    .locals 1

    .prologue
    .line 1881
    sget-object v0, Lcom/google/maps/g/vx;->h:Lcom/google/maps/g/vx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/vz;
    .locals 1

    .prologue
    .line 968
    new-instance v0, Lcom/google/maps/g/vz;

    invoke-direct {v0}, Lcom/google/maps/g/vz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/vx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    sget-object v0, Lcom/google/maps/g/vx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 847
    invoke-virtual {p0}, Lcom/google/maps/g/vx;->c()I

    move v1, v2

    .line 848
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 848
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 851
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 852
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 851
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 854
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 855
    const/4 v3, 0x4

    iget-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 854
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 857
    :cond_2
    iget v0, p0, Lcom/google/maps/g/vx;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 858
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 860
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 861
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 860
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 858
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 863
    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 864
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 863
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 866
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/vx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 867
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 837
    iget-byte v1, p0, Lcom/google/maps/g/vx;->i:B

    .line 838
    if-ne v1, v0, :cond_0

    .line 842
    :goto_0
    return v0

    .line 839
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 841
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/vx;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 871
    iget v0, p0, Lcom/google/maps/g/vx;->j:I

    .line 872
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 901
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 875
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 876
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    .line 877
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 875
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 879
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 880
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    .line 881
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 879
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 883
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 884
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    .line 885
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 883
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 887
    :cond_3
    iget v0, p0, Lcom/google/maps/g/vx;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 888
    const/4 v1, 0x5

    .line 889
    iget-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 891
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 892
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    .line 893
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 891
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 889
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    .line 895
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 896
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    .line 897
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 895
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 899
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/vx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 900
    iput v0, p0, Lcom/google/maps/g/vx;->j:I

    goto/16 :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    .line 591
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 592
    check-cast v0, Ljava/lang/String;

    .line 600
    :goto_0
    return-object v0

    .line 594
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 596
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 597
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    iput-object v1, p0, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 600
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vx;->newBuilder()Lcom/google/maps/g/vz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/vz;->a(Lcom/google/maps/g/vx;)Lcom/google/maps/g/vz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/vx;->newBuilder()Lcom/google/maps/g/vz;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    .line 628
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 629
    iget-object v0, p0, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 630
    invoke-static {}, Lcom/google/maps/g/a/ee;->d()Lcom/google/maps/g/a/ee;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 632
    :cond_0
    return-object v1
.end method

.method public final h()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/vk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 670
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    .line 671
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 672
    iget-object v0, p0, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 673
    invoke-static {}, Lcom/google/maps/g/vk;->h()Lcom/google/maps/g/vk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vk;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 675
    :cond_0
    return-object v1
.end method
