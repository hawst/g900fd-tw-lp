.class public final enum Lcom/google/maps/g/a/z;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/z;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/z;

.field public static final enum b:Lcom/google/maps/g/a/z;

.field public static final enum c:Lcom/google/maps/g/a/z;

.field public static final enum d:Lcom/google/maps/g/a/z;

.field public static final enum e:Lcom/google/maps/g/a/z;

.field public static final enum f:Lcom/google/maps/g/a/z;

.field public static final enum g:Lcom/google/maps/g/a/z;

.field private static final synthetic i:[Lcom/google/maps/g/a/z;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 219
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    .line 223
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "BAD_WAYPOINT_COUNT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->b:Lcom/google/maps/g/a/z;

    .line 227
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "WAYPOINT_REFINEMENT"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->c:Lcom/google/maps/g/a/z;

    .line 231
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "WAYPOINT_FAILURE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->d:Lcom/google/maps/g/a/z;

    .line 235
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "NO_ROUTES_FOUND"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    .line 239
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "NO_TRIPS_ON_GIVEN_DATE"

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->f:Lcom/google/maps/g/a/z;

    .line 243
    new-instance v0, Lcom/google/maps/g/a/z;

    const-string v1, "NAVIGATION_NOT_ALLOWED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/z;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/z;->g:Lcom/google/maps/g/a/z;

    .line 214
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/g/a/z;

    sget-object v1, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/z;->b:Lcom/google/maps/g/a/z;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/z;->c:Lcom/google/maps/g/a/z;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/z;->d:Lcom/google/maps/g/a/z;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/z;->f:Lcom/google/maps/g/a/z;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/z;->g:Lcom/google/maps/g/a/z;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/z;->i:[Lcom/google/maps/g/a/z;

    .line 298
    new-instance v0, Lcom/google/maps/g/a/aa;

    invoke-direct {v0}, Lcom/google/maps/g/a/aa;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 308
    iput p3, p0, Lcom/google/maps/g/a/z;->h:I

    .line 309
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/z;
    .locals 1

    .prologue
    .line 281
    packed-switch p0, :pswitch_data_0

    .line 289
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 282
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 283
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/z;->b:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 284
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/z;->c:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 285
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/z;->d:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 286
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 287
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/z;->f:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 288
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/z;->g:Lcom/google/maps/g/a/z;

    goto :goto_0

    .line 281
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/z;
    .locals 1

    .prologue
    .line 214
    const-class v0, Lcom/google/maps/g/a/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/z;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/z;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/google/maps/g/a/z;->i:[Lcom/google/maps/g/a/z;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/z;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/google/maps/g/a/z;->h:I

    return v0
.end method
