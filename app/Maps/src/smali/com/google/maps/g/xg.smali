.class public final Lcom/google/maps/g/xg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/xd;",
        "Lcom/google/maps/g/xg;",
        ">;",
        "Lcom/google/maps/g/xh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/ao;

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 380
    sget-object v0, Lcom/google/maps/g/xd;->g:Lcom/google/maps/g/xd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xg;->b:Lcom/google/n/ao;

    .line 538
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xg;->c:Lcom/google/n/ao;

    .line 598
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    .line 671
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/xg;->e:Lcom/google/n/ao;

    .line 381
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/a/a;)Lcom/google/maps/g/xg;
    .locals 2

    .prologue
    .line 494
    if-nez p1, :cond_0

    .line 495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/xg;->b:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 499
    iget v0, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/xg;->a:I

    .line 500
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/xd;)Lcom/google/maps/g/xg;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 437
    invoke-static {}, Lcom/google/maps/g/xd;->d()Lcom/google/maps/g/xd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 464
    :goto_0
    return-object p0

    .line 438
    :cond_0
    iget v2, p1, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 439
    iget-object v2, p0, Lcom/google/maps/g/xg;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 440
    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/xg;->a:I

    .line 442
    :cond_1
    iget v2, p1, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 443
    iget-object v2, p0, Lcom/google/maps/g/xg;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 444
    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/xg;->a:I

    .line 446
    :cond_2
    iget-object v2, p1, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 447
    iget-object v2, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 448
    iget-object v2, p1, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    .line 449
    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/xg;->a:I

    .line 456
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 457
    iget-object v2, p0, Lcom/google/maps/g/xg;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 458
    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/xg;->a:I

    .line 460
    :cond_4
    iget v2, p1, Lcom/google/maps/g/xd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 461
    iget-boolean v0, p1, Lcom/google/maps/g/xd;->f:Z

    iget v1, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/xg;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/xg;->f:Z

    .line 463
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/xd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 438
    goto :goto_1

    :cond_7
    move v2, v1

    .line 442
    goto :goto_2

    .line 451
    :cond_8
    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/xg;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/xg;->a:I

    .line 452
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 456
    goto :goto_4

    :cond_b
    move v0, v1

    .line 460
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 372
    new-instance v2, Lcom/google/maps/g/xd;

    invoke-direct {v2, p0}, Lcom/google/maps/g/xd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/xd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/xg;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/xg;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/xd;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/xg;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/xg;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/xg;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/xg;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/xd;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/xd;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/xg;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/xg;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v1, p0, Lcom/google/maps/g/xg;->f:Z

    iput-boolean v1, v2, Lcom/google/maps/g/xd;->f:Z

    iput v0, v2, Lcom/google/maps/g/xd;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 372
    check-cast p1, Lcom/google/maps/g/xd;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/xg;->a(Lcom/google/maps/g/xd;)Lcom/google/maps/g/xg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 468
    iget v0, p0, Lcom/google/maps/g/xg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/maps/g/xg;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 474
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 468
    goto :goto_0

    :cond_1
    move v0, v2

    .line 474
    goto :goto_1
.end method
