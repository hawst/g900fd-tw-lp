.class public final Lcom/google/maps/g/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a;",
            ">;"
        }
    .end annotation
.end field

.field static final q:Lcom/google/maps/g/a;

.field private static final serialVersionUID:J

.field private static volatile t:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/aq;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:Lcom/google/n/aq;

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/google/n/ao;

.field o:Z

.field p:Z

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/maps/g/b;

    invoke-direct {v0}, Lcom/google/maps/g/b;-><init>()V

    sput-object v0, Lcom/google/maps/g/a;->PARSER:Lcom/google/n/ax;

    .line 712
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a;->t:Lcom/google/n/aw;

    .line 2114
    new-instance v0, Lcom/google/maps/g/a;

    invoke-direct {v0}, Lcom/google/maps/g/a;-><init>()V

    sput-object v0, Lcom/google/maps/g/a;->q:Lcom/google/maps/g/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 193
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    .line 209
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    .line 354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    .line 370
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    .line 386
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    .line 517
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    .line 562
    iput-byte v3, p0, Lcom/google/maps/g/a;->r:B

    .line 629
    iput v3, p0, Lcom/google/maps/g/a;->s:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 28
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iput-boolean v4, p0, Lcom/google/maps/g/a;->o:Z

    .line 32
    iput-boolean v4, p0, Lcom/google/maps/g/a;->p:Z

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v12, 0x400

    const/16 v11, 0x200

    const/16 v10, 0x10

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    invoke-direct {p0}, Lcom/google/maps/g/a;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 45
    :cond_0
    :goto_0
    if-nez v4, :cond_b

    .line 46
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 47
    sparse-switch v0, :sswitch_data_0

    .line 52
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 54
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 50
    goto :goto_0

    .line 59
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 60
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v10, :cond_1

    .line 162
    iget-object v2, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 164
    :cond_1
    and-int/lit16 v2, v1, 0x200

    if-ne v2, v11, :cond_2

    .line 165
    iget-object v2, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 167
    :cond_2
    and-int/lit16 v2, v1, 0x400

    if-ne v2, v12, :cond_3

    .line 168
    iget-object v2, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 170
    :cond_3
    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_4

    .line 171
    iget-object v1, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 173
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a;->au:Lcom/google/n/bn;

    throw v0

    .line 64
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 65
    iget v6, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/a;->a:I

    .line 66
    iput-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 157
    :catch_1
    move-exception v0

    .line 158
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 159
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 71
    iget v6, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/maps/g/a;->a:I

    .line 72
    iput-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 76
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 77
    and-int/lit8 v6, v1, 0x10

    if-eq v6, v10, :cond_5

    .line 78
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 79
    or-int/lit8 v1, v1, 0x10

    .line 81
    :cond_5
    iget-object v6, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 85
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 86
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    goto/16 :goto_0

    .line 90
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 91
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    goto/16 :goto_0

    .line 95
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 96
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    goto/16 :goto_0

    .line 100
    :sswitch_8
    and-int/lit16 v0, v1, 0x200

    if-eq v0, v11, :cond_6

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 103
    or-int/lit16 v1, v1, 0x200

    .line 105
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 106
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 105
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 110
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 111
    and-int/lit16 v6, v1, 0x400

    if-eq v6, v12, :cond_7

    .line 112
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 113
    or-int/lit16 v1, v1, 0x400

    .line 115
    :cond_7
    iget-object v6, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 119
    :sswitch_a
    and-int/lit16 v0, v1, 0x800

    const/16 v6, 0x800

    if-eq v0, v6, :cond_8

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 122
    or-int/lit16 v1, v1, 0x800

    .line 124
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 124
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 129
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 130
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    goto/16 :goto_0

    .line 134
    :sswitch_c
    iget-object v0, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 135
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    goto/16 :goto_0

    .line 139
    :sswitch_d
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a;->o:Z

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_1

    .line 144
    :sswitch_e
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a;->a:I

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a;->p:Z

    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto :goto_2

    .line 149
    :sswitch_f
    iget-object v0, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 150
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 161
    :cond_b
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v10, :cond_c

    .line 162
    iget-object v0, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 164
    :cond_c
    and-int/lit16 v0, v1, 0x200

    if-ne v0, v11, :cond_d

    .line 165
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 167
    :cond_d
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v12, :cond_e

    .line 168
    iget-object v0, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 170
    :cond_e
    and-int/lit16 v0, v1, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 171
    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 173
    :cond_f
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->au:Lcom/google/n/bn;

    .line 174
    return-void

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x5a -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x80 -> :sswitch_d
        0x88 -> :sswitch_e
        0x92 -> :sswitch_f
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 193
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    .line 209
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    .line 354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    .line 370
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    .line 386
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    .line 517
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    .line 562
    iput-byte v1, p0, Lcom/google/maps/g/a;->r:B

    .line 629
    iput v1, p0, Lcom/google/maps/g/a;->s:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;
    .locals 1

    .prologue
    .line 777
    invoke-static {}, Lcom/google/maps/g/a;->newBuilder()Lcom/google/maps/g/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a;
    .locals 1

    .prologue
    .line 2117
    sget-object v0, Lcom/google/maps/g/a;->q:Lcom/google/maps/g/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/c;
    .locals 1

    .prologue
    .line 774
    new-instance v0, Lcom/google/maps/g/c;

    invoke-direct {v0}, Lcom/google/maps/g/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    sget-object v0, Lcom/google/maps/g/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x2

    .line 580
    invoke-virtual {p0}, Lcom/google/maps/g/a;->c()I

    .line 581
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 584
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 585
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 587
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    .line 588
    iget-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_2
    move v0, v1

    .line 590
    :goto_2
    iget-object v2, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 591
    const/4 v2, 0x5

    iget-object v4, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 585
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 588
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 593
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_6

    .line 594
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 596
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 597
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 599
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 600
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_8
    move v2, v1

    .line 602
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 603
    const/16 v4, 0xb

    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 602
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_9
    move v0, v1

    .line 605
    :goto_4
    iget-object v2, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_a

    .line 606
    const/16 v2, 0xc

    iget-object v4, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    move v2, v1

    .line 608
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 609
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 608
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 611
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_c

    .line 612
    const/16 v0, 0xe

    iget-object v2, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 614
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 615
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 617
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_e

    .line 618
    iget-boolean v0, p0, Lcom/google/maps/g/a;->o:Z

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_11

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 620
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_f

    .line 621
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/maps/g/a;->p:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_12

    :goto_7
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 623
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_10

    .line 624
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 626
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 627
    return-void

    :cond_11
    move v0, v1

    .line 618
    goto :goto_6

    :cond_12
    move v3, v1

    .line 621
    goto :goto_7
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 564
    iget-byte v0, p0, Lcom/google/maps/g/a;->r:B

    .line 565
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 575
    :cond_0
    :goto_0
    return v2

    .line 566
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 568
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 569
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 570
    iput-byte v2, p0, Lcom/google/maps/g/a;->r:B

    goto :goto_0

    .line 568
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 574
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/g/a;->r:B

    move v2, v3

    .line 575
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 631
    iget v0, p0, Lcom/google/maps/g/a;->s:I

    .line 632
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 707
    :goto_0
    return v0

    .line 635
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 636
    iget-object v0, p0, Lcom/google/maps/g/a;->c:Lcom/google/n/ao;

    .line 637
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 639
    :goto_1
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 640
    const/4 v3, 0x3

    .line 641
    iget-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 643
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 645
    iget-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_2
    move v0, v2

    move v3, v2

    .line 649
    :goto_4
    iget-object v4, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 650
    iget-object v4, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    .line 651
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 649
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 641
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 645
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 653
    :cond_5
    add-int v0, v1, v3

    .line 654
    iget-object v1, p0, Lcom/google/maps/g/a;->f:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 656
    iget v1, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_6

    .line 657
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a;->g:Lcom/google/n/ao;

    .line 658
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 660
    :cond_6
    iget v1, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_7

    .line 661
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a;->h:Lcom/google/n/ao;

    .line 662
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 664
    :cond_7
    iget v1, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_8

    .line 665
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/maps/g/a;->j:Lcom/google/n/ao;

    .line 666
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_8
    move v1, v2

    move v3, v0

    .line 668
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 669
    const/16 v4, 0xb

    iget-object v0, p0, Lcom/google/maps/g/a;->k:Ljava/util/List;

    .line 670
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 668
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_9
    move v0, v2

    move v1, v2

    .line 674
    :goto_6
    iget-object v4, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_a

    .line 675
    iget-object v4, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    .line 676
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 674
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 678
    :cond_a
    add-int v0, v3, v1

    .line 679
    iget-object v1, p0, Lcom/google/maps/g/a;->l:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v2

    move v3, v0

    .line 681
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 682
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a;->m:Ljava/util/List;

    .line 683
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 681
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 685
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_c

    .line 686
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a;->n:Lcom/google/n/ao;

    .line 687
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 689
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_d

    .line 690
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/a;->b:Lcom/google/n/ao;

    .line 691
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 693
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 694
    iget-boolean v0, p0, Lcom/google/maps/g/a;->o:Z

    .line 695
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 697
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 698
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/maps/g/a;->p:Z

    .line 699
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 701
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_10

    .line 702
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/maps/g/a;->i:Lcom/google/n/ao;

    .line 703
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 705
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 706
    iput v0, p0, Lcom/google/maps/g/a;->s:I

    goto/16 :goto_0

    :cond_11
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a;->newBuilder()Lcom/google/maps/g/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a;->newBuilder()Lcom/google/maps/g/c;

    move-result-object v0

    return-object v0
.end method
