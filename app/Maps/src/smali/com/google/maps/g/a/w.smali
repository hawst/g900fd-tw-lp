.class public final Lcom/google/maps/g/a/w;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ab;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/w;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/maps/g/a/w;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field e:I

.field f:I

.field g:Lcom/google/n/ao;

.field h:I

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Z

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/google/n/f;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field o:Z

.field p:Ljava/lang/Object;

.field q:Ljava/lang/Object;

.field r:Lcom/google/n/ao;

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lcom/google/maps/g/a/x;

    invoke-direct {v0}, Lcom/google/maps/g/a/x;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/w;->PARSER:Lcom/google/n/ax;

    .line 895
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/w;->v:Lcom/google/n/aw;

    .line 2384
    new-instance v0, Lcom/google/maps/g/a/w;

    invoke-direct {v0}, Lcom/google/maps/g/a/w;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/w;->s:Lcom/google/maps/g/a/w;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 447
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    .line 479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    .line 726
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    .line 741
    iput-byte v4, p0, Lcom/google/maps/g/a/w;->t:B

    .line 814
    iput v4, p0, Lcom/google/maps/g/a/w;->u:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    .line 20
    iput v2, p0, Lcom/google/maps/g/a/w;->d:I

    .line 21
    iput v2, p0, Lcom/google/maps/g/a/w;->e:I

    .line 22
    iput v2, p0, Lcom/google/maps/g/a/w;->f:I

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput v2, p0, Lcom/google/maps/g/a/w;->h:I

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iput-boolean v2, p0, Lcom/google/maps/g/a/w;->k:Z

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    .line 29
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    .line 31
    iput-boolean v2, p0, Lcom/google/maps/g/a/w;->o:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    .line 34
    iget-object v0, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v12, 0x1000

    const/16 v11, 0x400

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/maps/g/a/w;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 47
    :cond_0
    :goto_0
    if-nez v4, :cond_c

    .line 48
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 54
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 56
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 52
    goto :goto_0

    .line 61
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_1

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    .line 64
    or-int/lit8 v1, v1, 0x1

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 66
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :catchall_0
    move-exception v0

    and-int/lit8 v3, v1, 0x1

    if-ne v3, v2, :cond_2

    .line 182
    iget-object v2, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    .line 184
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v10, :cond_3

    .line 185
    iget-object v2, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    .line 187
    :cond_3
    and-int/lit16 v2, v1, 0x400

    if-ne v2, v11, :cond_4

    .line 188
    iget-object v2, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    .line 190
    :cond_4
    and-int/lit16 v1, v1, 0x1000

    if-ne v1, v12, :cond_5

    .line 191
    iget-object v1, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    .line 193
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/w;->au:Lcom/google/n/bn;

    throw v0

    .line 71
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v10, :cond_6

    .line 72
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    .line 74
    or-int/lit8 v1, v1, 0x2

    .line 76
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 76
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 177
    :catch_1
    move-exception v0

    .line 178
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 179
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/w;->d:I

    goto/16 :goto_0

    .line 86
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 87
    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v6

    .line 88
    if-nez v6, :cond_7

    .line 89
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 91
    :cond_7
    iget v6, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/a/w;->a:I

    .line 92
    iput v0, p0, Lcom/google/maps/g/a/w;->h:I

    goto/16 :goto_0

    .line 97
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 98
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    goto/16 :goto_0

    .line 102
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 103
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    goto/16 :goto_0

    .line 107
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/w;->k:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_1

    .line 112
    :sswitch_8
    and-int/lit16 v0, v1, 0x400

    if-eq v0, v11, :cond_9

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    .line 115
    or-int/lit16 v1, v1, 0x400

    .line 117
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 117
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 122
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 123
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    goto/16 :goto_0

    .line 127
    :sswitch_a
    and-int/lit16 v0, v1, 0x1000

    if-eq v0, v12, :cond_a

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    .line 130
    or-int/lit16 v1, v1, 0x1000

    .line 132
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 133
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 132
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 137
    :sswitch_b
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 138
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_b

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/w;->o:Z

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_2

    .line 142
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 143
    iget v6, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/g/a/w;->a:I

    .line 144
    iput-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 148
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 149
    iget v6, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/maps/g/a/w;->a:I

    .line 150
    iput-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 154
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 155
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    goto/16 :goto_0

    .line 159
    :sswitch_f
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 160
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/w;->e:I

    goto/16 :goto_0

    .line 164
    :sswitch_10
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I

    .line 165
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/w;->f:I

    goto/16 :goto_0

    .line 169
    :sswitch_11
    iget-object v0, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 170
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/w;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 181
    :cond_c
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_d

    .line 182
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    .line 184
    :cond_d
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v10, :cond_e

    .line 185
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    .line 187
    :cond_e
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v11, :cond_f

    .line 188
    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    .line 190
    :cond_f
    and-int/lit16 v0, v1, 0x1000

    if-ne v0, v12, :cond_10

    .line 191
    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    .line 193
    :cond_10
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->au:Lcom/google/n/bn;

    .line 194
    return-void

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 447
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    .line 479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    .line 726
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    .line 741
    iput-byte v1, p0, Lcom/google/maps/g/a/w;->t:B

    .line 814
    iput v1, p0, Lcom/google/maps/g/a/w;->u:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/w;
    .locals 1

    .prologue
    .line 2387
    sget-object v0, Lcom/google/maps/g/a/w;->s:Lcom/google/maps/g/a/w;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/y;
    .locals 1

    .prologue
    .line 957
    new-instance v0, Lcom/google/maps/g/a/y;

    invoke-direct {v0}, Lcom/google/maps/g/a/y;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    sget-object v0, Lcom/google/maps/g/a/w;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 759
    invoke-virtual {p0}, Lcom/google/maps/g/a/w;->c()I

    move v1, v2

    .line 760
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 760
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 763
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 764
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 763
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 766
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 767
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/w;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 769
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 770
    iget v0, p0, Lcom/google/maps/g/a/w;->h:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 772
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 773
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 775
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 776
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 778
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 779
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/maps/g/a/w;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_9

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_6
    move v1, v2

    .line 781
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 782
    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 767
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 770
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    :cond_9
    move v0, v2

    .line 779
    goto :goto_4

    .line 784
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 785
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_b
    move v1, v2

    .line 787
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 788
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 787
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 790
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_d

    .line 791
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/maps/g/a/w;->o:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_14

    :goto_7
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 793
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 794
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 796
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 797
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 799
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_10

    .line 800
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 802
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_11

    .line 803
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/maps/g/a/w;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_17

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 805
    :cond_11
    :goto_a
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_12

    .line 806
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/maps/g/a/w;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_18

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 808
    :cond_12
    :goto_b
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_13

    .line 809
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 811
    :cond_13
    iget-object v0, p0, Lcom/google/maps/g/a/w;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 812
    return-void

    :cond_14
    move v3, v2

    .line 791
    goto/16 :goto_7

    .line 794
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 797
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 803
    :cond_17
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_a

    .line 806
    :cond_18
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 743
    iget-byte v0, p0, Lcom/google/maps/g/a/w;->t:B

    .line 744
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 754
    :cond_0
    :goto_0
    return v2

    .line 745
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 747
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 748
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hu;->d()Lcom/google/maps/g/a/hu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/hu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 749
    iput-byte v2, p0, Lcom/google/maps/g/a/w;->t:B

    goto :goto_0

    .line 747
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 753
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/g/a/w;->t:B

    move v2, v3

    .line 754
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 816
    iget v0, p0, Lcom/google/maps/g/a/w;->u:I

    .line 817
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 890
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 820
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 821
    iget-object v0, p0, Lcom/google/maps/g/a/w;->b:Ljava/util/List;

    .line 822
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 820
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 824
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 825
    iget-object v0, p0, Lcom/google/maps/g/a/w;->c:Ljava/util/List;

    .line 826
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 824
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 828
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3

    .line 829
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/w;->d:I

    .line 830
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_8

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 832
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 833
    iget v0, p0, Lcom/google/maps/g/a/w;->h:I

    .line 834
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 836
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 837
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/w;->i:Lcom/google/n/ao;

    .line 838
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 840
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 841
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/w;->j:Lcom/google/n/ao;

    .line 842
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 844
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 845
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/maps/g/a/w;->k:Z

    .line 846
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_7
    move v1, v2

    .line 848
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 849
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/maps/g/a/w;->l:Ljava/util/List;

    .line 850
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 848
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_8
    move v0, v4

    .line 830
    goto/16 :goto_3

    :cond_9
    move v0, v4

    .line 834
    goto :goto_4

    .line 852
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 853
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/a/w;->m:Lcom/google/n/f;

    .line 854
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_b
    move v1, v2

    .line 856
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 857
    iget-object v0, p0, Lcom/google/maps/g/a/w;->n:Ljava/util/List;

    .line 858
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 856
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 860
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_d

    .line 861
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/maps/g/a/w;->o:Z

    .line 862
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 864
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 865
    const/16 v1, 0xc

    .line 866
    iget-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->p:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 868
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 869
    const/16 v1, 0xd

    .line 870
    iget-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/w;->q:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 872
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_10

    .line 873
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/w;->r:Lcom/google/n/ao;

    .line 874
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 876
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_11

    .line 877
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/maps/g/a/w;->e:I

    .line 878
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_17

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 880
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_13

    .line 881
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/maps/g/a/w;->f:I

    .line 882
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_12

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_12
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 884
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/w;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_14

    .line 885
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/w;->g:Lcom/google/n/ao;

    .line 886
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 888
    :cond_14
    iget-object v0, p0, Lcom/google/maps/g/a/w;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 889
    iput v0, p0, Lcom/google/maps/g/a/w;->u:I

    goto/16 :goto_0

    .line 866
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 870
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    :cond_17
    move v0, v4

    .line 878
    goto :goto_9
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/w;->newBuilder()Lcom/google/maps/g/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/y;->a(Lcom/google/maps/g/a/w;)Lcom/google/maps/g/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/w;->newBuilder()Lcom/google/maps/g/a/y;

    move-result-object v0

    return-object v0
.end method
