.class public final Lcom/google/maps/g/oa;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/of;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/oa;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/oa;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/maps/g/ob;

    invoke-direct {v0}, Lcom/google/maps/g/ob;-><init>()V

    sput-object v0, Lcom/google/maps/g/oa;->PARSER:Lcom/google/n/ax;

    .line 304
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/oa;->h:Lcom/google/n/aw;

    .line 638
    new-instance v0, Lcom/google/maps/g/oa;

    invoke-direct {v0}, Lcom/google/maps/g/oa;-><init>()V

    sput-object v0, Lcom/google/maps/g/oa;->e:Lcom/google/maps/g/oa;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 254
    iput-byte v0, p0, Lcom/google/maps/g/oa;->f:B

    .line 279
    iput v0, p0, Lcom/google/maps/g/oa;->g:I

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/oa;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/oa;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    const/4 v0, 0x0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 48
    invoke-static {v3}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v4

    .line 49
    if-nez v4, :cond_1

    .line 50
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/oa;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/maps/g/oa;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/oa;->a:I

    .line 53
    iput v3, p0, Lcom/google/maps/g/oa;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 75
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 59
    iget v4, p0, Lcom/google/maps/g/oa;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/oa;->a:I

    .line 60
    iput-object v3, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    goto :goto_0

    .line 64
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 65
    iget v4, p0, Lcom/google/maps/g/oa;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/oa;->a:I

    .line 66
    iput-object v3, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/oa;->au:Lcom/google/n/bn;

    .line 78
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 254
    iput-byte v0, p0, Lcom/google/maps/g/oa;->f:B

    .line 279
    iput v0, p0, Lcom/google/maps/g/oa;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/oa;
    .locals 1

    .prologue
    .line 641
    sget-object v0, Lcom/google/maps/g/oa;->e:Lcom/google/maps/g/oa;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/oc;
    .locals 1

    .prologue
    .line 366
    new-instance v0, Lcom/google/maps/g/oc;

    invoke-direct {v0}, Lcom/google/maps/g/oc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/oa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/google/maps/g/oa;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 266
    invoke-virtual {p0}, Lcom/google/maps/g/oa;->c()I

    .line 267
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 268
    iget v0, p0, Lcom/google/maps/g/oa;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 270
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 273
    :cond_1
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 274
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/oa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 277
    return-void

    .line 268
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 271
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 274
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 256
    iget-byte v1, p0, Lcom/google/maps/g/oa;->f:B

    .line 257
    if-ne v1, v0, :cond_0

    .line 261
    :goto_0
    return v0

    .line 258
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 260
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/oa;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 281
    iget v0, p0, Lcom/google/maps/g/oa;->g:I

    .line 282
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 299
    :goto_0
    return v0

    .line 285
    :cond_0
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 286
    iget v0, p0, Lcom/google/maps/g/oa;->b:I

    .line 287
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 289
    :goto_2
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 291
    iget-object v0, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 293
    :cond_1
    iget v0, p0, Lcom/google/maps/g/oa;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 294
    const/4 v3, 0x3

    .line 295
    iget-object v0, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/oa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 298
    iput v0, p0, Lcom/google/maps/g/oa;->g:I

    goto :goto_0

    .line 287
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 291
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 295
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/oa;->newBuilder()Lcom/google/maps/g/oc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/oc;->a(Lcom/google/maps/g/oa;)Lcom/google/maps/g/oc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/oa;->newBuilder()Lcom/google/maps/g/oc;

    move-result-object v0

    return-object v0
.end method
