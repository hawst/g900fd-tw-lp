.class public final Lcom/google/maps/g/la;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/lb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ky;",
        "Lcom/google/maps/g/la;",
        ">;",
        "Lcom/google/maps/g/lb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 349
    sget-object v0, Lcom/google/maps/g/ky;->f:Lcom/google/maps/g/ky;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 426
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/la;->b:Ljava/lang/Object;

    .line 502
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/la;->c:Ljava/lang/Object;

    .line 578
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/la;->d:Lcom/google/n/ao;

    .line 637
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/la;->e:Lcom/google/n/ao;

    .line 350
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 397
    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 417
    :goto_0
    return-object p0

    .line 398
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ky;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 399
    iget v2, p0, Lcom/google/maps/g/la;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/la;->a:I

    .line 400
    iget-object v2, p1, Lcom/google/maps/g/ky;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/la;->b:Ljava/lang/Object;

    .line 403
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ky;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 404
    iget v2, p0, Lcom/google/maps/g/la;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/la;->a:I

    .line 405
    iget-object v2, p1, Lcom/google/maps/g/ky;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/la;->c:Ljava/lang/Object;

    .line 408
    :cond_2
    iget v2, p1, Lcom/google/maps/g/ky;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 409
    iget-object v2, p0, Lcom/google/maps/g/la;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/ky;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 410
    iget v2, p0, Lcom/google/maps/g/la;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/la;->a:I

    .line 412
    :cond_3
    iget v2, p1, Lcom/google/maps/g/ky;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 413
    iget-object v0, p0, Lcom/google/maps/g/la;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/ky;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 414
    iget v0, p0, Lcom/google/maps/g/la;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/la;->a:I

    .line 416
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/ky;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 398
    goto :goto_1

    :cond_6
    move v2, v1

    .line 403
    goto :goto_2

    :cond_7
    move v2, v1

    .line 408
    goto :goto_3

    :cond_8
    move v0, v1

    .line 412
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/google/maps/g/la;->c()Lcom/google/maps/g/ky;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 341
    check-cast p1, Lcom/google/maps/g/ky;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/la;->a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/ky;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    new-instance v2, Lcom/google/maps/g/ky;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ky;-><init>(Lcom/google/n/v;)V

    .line 370
    iget v3, p0, Lcom/google/maps/g/la;->a:I

    .line 372
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 375
    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/la;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ky;->b:Ljava/lang/Object;

    .line 376
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 377
    or-int/lit8 v0, v0, 0x2

    .line 379
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/la;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ky;->c:Ljava/lang/Object;

    .line 380
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 381
    or-int/lit8 v0, v0, 0x4

    .line 383
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/ky;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/la;->d:Lcom/google/n/ao;

    .line 384
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/la;->d:Lcom/google/n/ao;

    .line 385
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 383
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 386
    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 387
    or-int/lit8 v0, v0, 0x8

    .line 389
    :cond_2
    iget-object v3, v2, Lcom/google/maps/g/ky;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/la;->e:Lcom/google/n/ao;

    .line 390
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/la;->e:Lcom/google/n/ao;

    .line 391
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 389
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 392
    iput v0, v2, Lcom/google/maps/g/ky;->a:I

    .line 393
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
