.class public final enum Lcom/google/maps/g/hs;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/hs;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/hs;

.field public static final enum b:Lcom/google/maps/g/hs;

.field public static final enum c:Lcom/google/maps/g/hs;

.field public static final enum d:Lcom/google/maps/g/hs;

.field public static final enum e:Lcom/google/maps/g/hs;

.field public static final enum f:Lcom/google/maps/g/hs;

.field public static final enum g:Lcom/google/maps/g/hs;

.field public static final enum h:Lcom/google/maps/g/hs;

.field public static final enum i:Lcom/google/maps/g/hs;

.field public static final enum j:Lcom/google/maps/g/hs;

.field public static final enum k:Lcom/google/maps/g/hs;

.field public static final enum l:Lcom/google/maps/g/hs;

.field public static final enum m:Lcom/google/maps/g/hs;

.field private static final synthetic o:[Lcom/google/maps/g/hs;


# instance fields
.field public final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 270
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "UNKNOWN_ENTRY_POINT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->a:Lcom/google/maps/g/hs;

    .line 274
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "PLACE_CARD"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->b:Lcom/google/maps/g/hs;

    .line 278
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->c:Lcom/google/maps/g/hs;

    .line 282
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "FINEPRINT"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->d:Lcom/google/maps/g/hs;

    .line 286
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "DIRECTIONS_PAGE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->e:Lcom/google/maps/g/hs;

    .line 290
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "PHONE_SHAKE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->f:Lcom/google/maps/g/hs;

    .line 294
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "DRAWER_MENU"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->g:Lcom/google/maps/g/hs;

    .line 298
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "RATE_APP_DIALOG"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->h:Lcom/google/maps/g/hs;

    .line 302
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "HERE_PLACE_PICKER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->i:Lcom/google/maps/g/hs;

    .line 306
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "ZERO_SEARCH_RESULTS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->j:Lcom/google/maps/g/hs;

    .line 310
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "URL"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->k:Lcom/google/maps/g/hs;

    .line 314
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "NAVIGATION"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->l:Lcom/google/maps/g/hs;

    .line 318
    new-instance v0, Lcom/google/maps/g/hs;

    const-string v1, "SEARCH_SUGGESTIONS"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/hs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hs;->m:Lcom/google/maps/g/hs;

    .line 265
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/maps/g/hs;

    sget-object v1, Lcom/google/maps/g/hs;->a:Lcom/google/maps/g/hs;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/hs;->b:Lcom/google/maps/g/hs;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/hs;->c:Lcom/google/maps/g/hs;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/hs;->d:Lcom/google/maps/g/hs;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/hs;->e:Lcom/google/maps/g/hs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/hs;->f:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/hs;->g:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/hs;->h:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/hs;->i:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/hs;->j:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/hs;->k:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/hs;->l:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/hs;->m:Lcom/google/maps/g/hs;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/hs;->o:[Lcom/google/maps/g/hs;

    .line 403
    new-instance v0, Lcom/google/maps/g/ht;

    invoke-direct {v0}, Lcom/google/maps/g/ht;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 413
    iput p3, p0, Lcom/google/maps/g/hs;->n:I

    .line 414
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/hs;
    .locals 1

    .prologue
    .line 380
    packed-switch p0, :pswitch_data_0

    .line 394
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 381
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/hs;->a:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 382
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/hs;->b:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 383
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/hs;->c:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 384
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/hs;->d:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 385
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/hs;->e:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 386
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/hs;->f:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 387
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/hs;->g:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 388
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/hs;->h:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 389
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/hs;->i:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 390
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/hs;->j:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 391
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/hs;->k:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 392
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/hs;->l:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 393
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/hs;->m:Lcom/google/maps/g/hs;

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/hs;
    .locals 1

    .prologue
    .line 265
    const-class v0, Lcom/google/maps/g/hs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hs;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/hs;
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/google/maps/g/hs;->o:[Lcom/google/maps/g/hs;

    invoke-virtual {v0}, [Lcom/google/maps/g/hs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/hs;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/google/maps/g/hs;->n:I

    return v0
.end method
