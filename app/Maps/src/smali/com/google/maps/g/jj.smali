.class public final Lcom/google/maps/g/jj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/jk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/jh;",
        "Lcom/google/maps/g/jj;",
        ">;",
        "Lcom/google/maps/g/jk;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 415
    sget-object v0, Lcom/google/maps/g/jh;->d:Lcom/google/maps/g/jh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 474
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/jj;->b:Ljava/lang/Object;

    .line 550
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    .line 416
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 448
    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 465
    :goto_0
    return-object p0

    .line 449
    :cond_0
    iget v1, p1, Lcom/google/maps/g/jh;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 450
    iget v0, p0, Lcom/google/maps/g/jj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/jj;->a:I

    .line 451
    iget-object v0, p1, Lcom/google/maps/g/jh;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/jj;->b:Ljava/lang/Object;

    .line 454
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/jh;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 456
    iget-object v0, p1, Lcom/google/maps/g/jh;->c:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    .line 457
    iget v0, p0, Lcom/google/maps/g/jj;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/jj;->a:I

    .line 464
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/jh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 449
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 459
    :cond_4
    iget v0, p0, Lcom/google/maps/g/jj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/maps/g/jj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/jj;->a:I

    .line 460
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/maps/g/jh;->c:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/google/maps/g/jj;->c()Lcom/google/maps/g/jh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 407
    check-cast p1, Lcom/google/maps/g/jh;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/jj;->a(Lcom/google/maps/g/jh;)Lcom/google/maps/g/jj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/jh;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 431
    new-instance v2, Lcom/google/maps/g/jh;

    invoke-direct {v2, p0}, Lcom/google/maps/g/jh;-><init>(Lcom/google/n/v;)V

    .line 432
    iget v3, p0, Lcom/google/maps/g/jj;->a:I

    .line 433
    const/4 v1, 0x0

    .line 434
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    .line 437
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/jj;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/jh;->b:Ljava/lang/Object;

    .line 438
    iget v1, p0, Lcom/google/maps/g/jj;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 439
    iget-object v1, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    .line 440
    iget v1, p0, Lcom/google/maps/g/jj;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/jj;->a:I

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/jj;->c:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/jh;->c:Lcom/google/n/aq;

    .line 443
    iput v0, v2, Lcom/google/maps/g/jh;->a:I

    .line 444
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
