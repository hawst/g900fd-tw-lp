.class public final Lcom/google/maps/g/a/u;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/s;",
        "Lcom/google/maps/g/a/u;",
        ">;",
        "Lcom/google/maps/g/a/v;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/google/maps/g/a/s;->f:Lcom/google/maps/g/a/s;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 426
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/u;->b:Lcom/google/n/ao;

    .line 485
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/u;->c:Lcom/google/n/ao;

    .line 544
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/u;->d:Lcom/google/n/ao;

    .line 604
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    .line 337
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/s;)Lcom/google/maps/g/a/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 387
    invoke-static {}, Lcom/google/maps/g/a/s;->d()Lcom/google/maps/g/a/s;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 411
    :goto_0
    return-object p0

    .line 388
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/s;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 389
    iget-object v2, p0, Lcom/google/maps/g/a/u;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/s;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 390
    iget v2, p0, Lcom/google/maps/g/a/u;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/u;->a:I

    .line 392
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/s;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 393
    iget-object v2, p0, Lcom/google/maps/g/a/u;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/s;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 394
    iget v2, p0, Lcom/google/maps/g/a/u;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/u;->a:I

    .line 396
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/s;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 397
    iget-object v0, p0, Lcom/google/maps/g/a/u;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/s;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 398
    iget v0, p0, Lcom/google/maps/g/a/u;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/u;->a:I

    .line 400
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/a/s;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 401
    iget-object v0, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 402
    iget-object v0, p1, Lcom/google/maps/g/a/s;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    .line 403
    iget v0, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/g/a/u;->a:I

    .line 410
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/maps/g/a/s;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 388
    goto :goto_1

    :cond_6
    move v2, v1

    .line 392
    goto :goto_2

    :cond_7
    move v0, v1

    .line 396
    goto :goto_3

    .line 405
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/u;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/u;->a:I

    .line 406
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/a/s;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 328
    new-instance v2, Lcom/google/maps/g/a/s;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/s;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/s;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/u;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/u;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/s;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/u;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/u;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/a/s;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/u;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/u;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/a/u;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/u;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/s;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/a/s;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 328
    check-cast p1, Lcom/google/maps/g/a/s;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/u;->a(Lcom/google/maps/g/a/s;)Lcom/google/maps/g/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 415
    iget v0, p0, Lcom/google/maps/g/a/u;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/google/maps/g/a/u;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/cc;->d()Lcom/google/maps/g/a/cc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cc;

    invoke-virtual {v0}, Lcom/google/maps/g/a/cc;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 421
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 415
    goto :goto_0

    :cond_1
    move v0, v2

    .line 421
    goto :goto_1
.end method
