.class public final Lcom/google/maps/g/qv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/qw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/qt;",
        "Lcom/google/maps/g/qv;",
        ">;",
        "Lcom/google/maps/g/qw;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4064
    sget-object v0, Lcom/google/maps/g/qt;->d:Lcom/google/maps/g/qt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4147
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qv;->c:Ljava/lang/Object;

    .line 4065
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qt;)Lcom/google/maps/g/qv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4096
    invoke-static {}, Lcom/google/maps/g/qt;->g()Lcom/google/maps/g/qt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4106
    :goto_0
    return-object p0

    .line 4097
    :cond_0
    iget v2, p1, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4098
    iget v2, p1, Lcom/google/maps/g/qt;->b:I

    iget v3, p0, Lcom/google/maps/g/qv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/qv;->a:I

    iput v2, p0, Lcom/google/maps/g/qv;->b:I

    .line 4100
    :cond_1
    iget v2, p1, Lcom/google/maps/g/qt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 4101
    iget v0, p0, Lcom/google/maps/g/qv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/qv;->a:I

    .line 4102
    iget-object v0, p1, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/qv;->c:Ljava/lang/Object;

    .line 4105
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/qt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 4097
    goto :goto_1

    :cond_4
    move v0, v1

    .line 4100
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4056
    new-instance v2, Lcom/google/maps/g/qt;

    invoke-direct {v2, p0}, Lcom/google/maps/g/qt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/qv;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/qv;->b:I

    iput v1, v2, Lcom/google/maps/g/qt;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/qv;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/qt;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/qt;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4056
    check-cast p1, Lcom/google/maps/g/qt;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/qv;->a(Lcom/google/maps/g/qt;)Lcom/google/maps/g/qv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4110
    const/4 v0, 0x1

    return v0
.end method
