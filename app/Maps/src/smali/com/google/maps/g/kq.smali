.class public final Lcom/google/maps/g/kq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/kq;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/kq;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/maps/g/kr;

    invoke-direct {v0}, Lcom/google/maps/g/kr;-><init>()V

    sput-object v0, Lcom/google/maps/g/kq;->PARSER:Lcom/google/n/ax;

    .line 365
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/kq;->j:Lcom/google/n/aw;

    .line 942
    new-instance v0, Lcom/google/maps/g/kq;

    invoke-direct {v0}, Lcom/google/maps/g/kq;-><init>()V

    sput-object v0, Lcom/google/maps/g/kq;->g:Lcom/google/maps/g/kq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 295
    iput-byte v0, p0, Lcom/google/maps/g/kq;->h:B

    .line 332
    iput v0, p0, Lcom/google/maps/g/kq;->i:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/kq;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x2

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/kq;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 37
    sparse-switch v4, :sswitch_data_0

    .line 42
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_1

    .line 50
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 52
    or-int/lit8 v1, v1, 0x2

    .line 54
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 54
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_2

    .line 90
    iget-object v1, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 92
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/kq;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/maps/g/kq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/kq;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/kq;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 85
    :catch_1
    move-exception v0

    .line 86
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 87
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 65
    iget v5, p0, Lcom/google/maps/g/kq;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/kq;->a:I

    .line 66
    iput-object v4, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    goto :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 71
    iget v5, p0, Lcom/google/maps/g/kq;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/kq;->a:I

    .line 72
    iput-object v4, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    goto :goto_0

    .line 76
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 77
    iget v5, p0, Lcom/google/maps/g/kq;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/kq;->a:I

    .line 78
    iput-object v4, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 89
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 90
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 92
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->au:Lcom/google/n/bn;

    .line 93
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 295
    iput-byte v0, p0, Lcom/google/maps/g/kq;->h:B

    .line 332
    iput v0, p0, Lcom/google/maps/g/kq;->i:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;
    .locals 1

    .prologue
    .line 430
    invoke-static {}, Lcom/google/maps/g/kq;->newBuilder()Lcom/google/maps/g/ks;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/kq;
    .locals 1

    .prologue
    .line 945
    sget-object v0, Lcom/google/maps/g/kq;->g:Lcom/google/maps/g/kq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ks;
    .locals 1

    .prologue
    .line 427
    new-instance v0, Lcom/google/maps/g/ks;

    invoke-direct {v0}, Lcom/google/maps/g/ks;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/kq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lcom/google/maps/g/kq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 313
    invoke-virtual {p0}, Lcom/google/maps/g/kq;->c()I

    move v1, v2

    .line 314
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 314
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 317
    :cond_0
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 318
    iget v0, p0, Lcom/google/maps/g/kq;->d:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 320
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 321
    iget-object v0, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 323
    :cond_2
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 324
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 326
    :cond_3
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 327
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 329
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/kq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 330
    return-void

    .line 318
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 321
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 324
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 327
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 297
    iget-byte v0, p0, Lcom/google/maps/g/kq;->h:B

    .line 298
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 308
    :cond_0
    :goto_0
    return v2

    .line 299
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 301
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 302
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 303
    iput-byte v2, p0, Lcom/google/maps/g/kq;->h:B

    goto :goto_0

    .line 301
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 307
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/g/kq;->h:B

    move v2, v3

    .line 308
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 334
    iget v0, p0, Lcom/google/maps/g/kq;->i:I

    .line 335
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 360
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 338
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 340
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 338
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 342
    :cond_1
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 343
    iget v0, p0, Lcom/google/maps/g/kq;->d:I

    .line 344
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 346
    :cond_2
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 348
    iget-object v0, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 350
    :cond_3
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 351
    const/4 v1, 0x5

    .line 352
    iget-object v0, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 354
    :cond_4
    iget v0, p0, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 355
    const/4 v1, 0x6

    .line 356
    iget-object v0, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 358
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/kq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 359
    iput v0, p0, Lcom/google/maps/g/kq;->i:I

    goto/16 :goto_0

    .line 344
    :cond_6
    const/16 v0, 0xa

    goto/16 :goto_2

    .line 348
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 352
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 356
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/kq;->newBuilder()Lcom/google/maps/g/ks;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/kq;->newBuilder()Lcom/google/maps/g/ks;

    move-result-object v0

    return-object v0
.end method
