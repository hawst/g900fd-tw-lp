.class public final Lcom/google/maps/g/se;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/si;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/se;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/maps/g/se;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 255
    new-instance v0, Lcom/google/maps/g/sf;

    invoke-direct {v0}, Lcom/google/maps/g/sf;-><init>()V

    sput-object v0, Lcom/google/maps/g/se;->PARSER:Lcom/google/n/ax;

    .line 273
    new-instance v0, Lcom/google/maps/g/sg;

    invoke-direct {v0}, Lcom/google/maps/g/sg;-><init>()V

    .line 342
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/se;->e:Lcom/google/n/aw;

    .line 546
    new-instance v0, Lcom/google/maps/g/se;

    invoke-direct {v0}, Lcom/google/maps/g/se;-><init>()V

    sput-object v0, Lcom/google/maps/g/se;->b:Lcom/google/maps/g/se;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 181
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 301
    iput-byte v0, p0, Lcom/google/maps/g/se;->c:B

    .line 320
    iput v0, p0, Lcom/google/maps/g/se;->d:I

    .line 182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    .line 183
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 189
    invoke-direct {p0}, Lcom/google/maps/g/se;-><init>()V

    .line 192
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 195
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 196
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 197
    sparse-switch v1, :sswitch_data_0

    .line 202
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 204
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 200
    goto :goto_0

    .line 209
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 210
    invoke-static {v5}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v1

    .line 211
    if-nez v1, :cond_2

    .line 212
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 243
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 244
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 250
    iget-object v1, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    .line 252
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/se;->au:Lcom/google/n/bn;

    throw v0

    .line 214
    :cond_2
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_9

    .line 215
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    or-int/lit8 v1, v0, 0x1

    .line 218
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 220
    goto :goto_0

    .line 223
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 224
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 225
    :goto_4
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_3

    const/4 v1, -0x1

    :goto_5
    if-lez v1, :cond_5

    .line 226
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 227
    invoke-static {v6}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v1

    .line 228
    if-nez v1, :cond_4

    .line 229
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    .line 245
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 246
    :goto_6
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 247
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 225
    :cond_3
    :try_start_6
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_5

    .line 231
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_8

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 233
    or-int/lit8 v1, v0, 0x1

    .line 235
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 237
    goto :goto_4

    .line 238
    :cond_5
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 249
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto/16 :goto_2

    :cond_6
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 250
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    .line 252
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/se;->au:Lcom/google/n/bn;

    .line 253
    return-void

    .line 245
    :catch_2
    move-exception v0

    goto :goto_6

    .line 243
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v0

    goto/16 :goto_3

    .line 197
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 179
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 301
    iput-byte v0, p0, Lcom/google/maps/g/se;->c:B

    .line 320
    iput v0, p0, Lcom/google/maps/g/se;->d:I

    .line 180
    return-void
.end method

.method public static d()Lcom/google/maps/g/se;
    .locals 1

    .prologue
    .line 549
    sget-object v0, Lcom/google/maps/g/se;->b:Lcom/google/maps/g/se;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/sh;
    .locals 1

    .prologue
    .line 404
    new-instance v0, Lcom/google/maps/g/sh;

    invoke-direct {v0}, Lcom/google/maps/g/sh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/se;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    sget-object v0, Lcom/google/maps/g/se;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 313
    invoke-virtual {p0}, Lcom/google/maps/g/se;->c()I

    move v1, v2

    .line 314
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 315
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 314
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 315
    :cond_0
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/se;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 318
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 303
    iget-byte v1, p0, Lcom/google/maps/g/se;->c:B

    .line 304
    if-ne v1, v0, :cond_0

    .line 308
    :goto_0
    return v0

    .line 305
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 307
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/se;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 322
    iget v1, p0, Lcom/google/maps/g/se;->d:I

    .line 323
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 337
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 328
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    .line 330
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v2, v0

    .line 328
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 330
    :cond_1
    const/16 v0, 0xa

    goto :goto_2

    .line 332
    :cond_2
    add-int/lit8 v0, v2, 0x0

    .line 333
    iget-object v1, p0, Lcom/google/maps/g/se;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 335
    iget-object v1, p0, Lcom/google/maps/g/se;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 336
    iput v0, p0, Lcom/google/maps/g/se;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lcom/google/maps/g/se;->newBuilder()Lcom/google/maps/g/sh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/sh;->a(Lcom/google/maps/g/se;)Lcom/google/maps/g/sh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lcom/google/maps/g/se;->newBuilder()Lcom/google/maps/g/sh;

    move-result-object v0

    return-object v0
.end method
