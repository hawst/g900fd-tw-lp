.class public final Lcom/google/maps/g/vr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/vw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/vp;",
        "Lcom/google/maps/g/vr;",
        ">;",
        "Lcom/google/maps/g/vw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/aq;

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 732
    sget-object v0, Lcom/google/maps/g/vp;->k:Lcom/google/maps/g/vp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 879
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    .line 1015
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    .line 1108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vr;->d:Ljava/lang/Object;

    .line 1185
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    .line 1321
    iput v1, p0, Lcom/google/maps/g/vr;->f:I

    .line 1422
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    .line 1558
    iput v1, p0, Lcom/google/maps/g/vr;->j:I

    .line 733
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/vp;)Lcom/google/maps/g/vr;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 810
    invoke-static {}, Lcom/google/maps/g/vp;->g()Lcom/google/maps/g/vp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 869
    :goto_0
    return-object p0

    .line 811
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 812
    iget-object v2, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 813
    iget-object v2, p1, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    .line 814
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 821
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 822
    iget-object v2, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 823
    iget-object v2, p1, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    .line 824
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 831
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 832
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 833
    iget-object v2, p1, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/vr;->d:Ljava/lang/Object;

    .line 836
    :cond_3
    iget-object v2, p1, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 837
    iget-object v2, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 838
    iget-object v2, p1, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    .line 839
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 846
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_f

    .line 847
    iget v2, p1, Lcom/google/maps/g/vp;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vu;->a(I)Lcom/google/maps/g/vu;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/vu;->a:Lcom/google/maps/g/vu;

    :cond_5
    if-nez v2, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 816
    :cond_6
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 817
    :cond_7
    iget-object v2, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 826
    :cond_8
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_9

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 827
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 831
    goto :goto_3

    .line 841
    :cond_b
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 842
    :cond_c
    iget-object v2, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_d
    move v2, v1

    .line 846
    goto :goto_5

    .line 847
    :cond_e
    iget v3, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/vr;->a:I

    iget v2, v2, Lcom/google/maps/g/vu;->d:I

    iput v2, p0, Lcom/google/maps/g/vr;->f:I

    .line 849
    :cond_f
    iget v2, p1, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 850
    iget-boolean v2, p1, Lcom/google/maps/g/vp;->g:Z

    iget v3, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/vr;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/vr;->g:Z

    .line 852
    :cond_10
    iget v2, p1, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_11

    .line 853
    iget-boolean v2, p1, Lcom/google/maps/g/vp;->h:Z

    iget v3, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/vr;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/vr;->h:Z

    .line 855
    :cond_11
    iget-object v2, p1, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_12

    .line 856
    iget-object v2, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 857
    iget-object v2, p1, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    .line 858
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 865
    :cond_12
    :goto_8
    iget v2, p1, Lcom/google/maps/g/vp;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_18

    :goto_9
    if-eqz v0, :cond_1a

    .line 866
    iget v0, p1, Lcom/google/maps/g/vp;->j:I

    invoke-static {v0}, Lcom/google/maps/g/vs;->a(I)Lcom/google/maps/g/vs;

    move-result-object v0

    if-nez v0, :cond_13

    sget-object v0, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    :cond_13
    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    move v2, v1

    .line 849
    goto :goto_6

    :cond_15
    move v2, v1

    .line 852
    goto :goto_7

    .line 860
    :cond_16
    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-eq v2, v3, :cond_17

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/vr;->a:I

    .line 861
    :cond_17
    iget-object v2, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_18
    move v0, v1

    .line 865
    goto :goto_9

    .line 866
    :cond_19
    iget v1, p0, Lcom/google/maps/g/vr;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/maps/g/vr;->a:I

    iget v0, v0, Lcom/google/maps/g/vs;->c:I

    iput v0, p0, Lcom/google/maps/g/vr;->j:I

    .line 868
    :cond_1a
    iget-object v0, p1, Lcom/google/maps/g/vp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 724
    new-instance v2, Lcom/google/maps/g/vp;

    invoke-direct {v2, p0}, Lcom/google/maps/g/vp;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/vr;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/vr;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/vr;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/vr;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/vr;->c:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/vp;->c:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_8

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/vr;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/vp;->d:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/vr;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/vr;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vp;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x2

    :cond_3
    iget v1, p0, Lcom/google/maps/g/vr;->f:I

    iput v1, v2, Lcom/google/maps/g/vp;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x4

    :cond_4
    iget-boolean v1, p0, Lcom/google/maps/g/vr;->g:Z

    iput-boolean v1, v2, Lcom/google/maps/g/vp;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x8

    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/vr;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/vp;->h:Z

    iget v1, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vr;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/g/vr;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/vr;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit8 v0, v0, 0x10

    :cond_7
    iget v1, p0, Lcom/google/maps/g/vr;->j:I

    iput v1, v2, Lcom/google/maps/g/vp;->j:I

    iput v0, v2, Lcom/google/maps/g/vp;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 724
    check-cast p1, Lcom/google/maps/g/vp;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/vr;->a(Lcom/google/maps/g/vp;)Lcom/google/maps/g/vr;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 873
    const/4 v0, 0x1

    return v0
.end method
