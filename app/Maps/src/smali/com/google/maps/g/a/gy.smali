.class public final Lcom/google/maps/g/a/gy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/hb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/gy;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final t:Lcom/google/maps/g/a/gy;

.field private static volatile w:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field public d:I

.field e:Ljava/lang/Object;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Z

.field j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field l:Lcom/google/n/ao;

.field m:I

.field public n:Lcom/google/n/ao;

.field o:Lcom/google/n/ao;

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field q:Lcom/google/n/ao;

.field r:Lcom/google/n/ao;

.field s:Z

.field private u:B

.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lcom/google/maps/g/a/gz;

    invoke-direct {v0}, Lcom/google/maps/g/a/gz;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/gy;->PARSER:Lcom/google/n/ax;

    .line 876
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/gy;->w:Lcom/google/n/aw;

    .line 2427
    new-instance v0, Lcom/google/maps/g/a/gy;

    invoke-direct {v0}, Lcom/google/maps/g/a/gy;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/gy;->t:Lcom/google/maps/g/a/gy;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 569
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    .line 600
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    .line 616
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    .line 675
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    .line 691
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    .line 721
    iput-byte v4, p0, Lcom/google/maps/g/a/gy;->u:B

    .line 791
    iput v4, p0, Lcom/google/maps/g/a/gy;->v:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    .line 20
    iput v3, p0, Lcom/google/maps/g/a/gy;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    .line 25
    iput-boolean v3, p0, Lcom/google/maps/g/a/gy;->i:Z

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    .line 28
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iput v3, p0, Lcom/google/maps/g/a/gy;->m:I

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 33
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 35
    iput-boolean v3, p0, Lcom/google/maps/g/a/gy;->s:Z

    .line 36
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x4000

    const/16 v8, 0x10

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/maps/g/a/gy;-><init>()V

    .line 45
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 48
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 49
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 50
    sparse-switch v0, :sswitch_data_0

    .line 55
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 57
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 53
    goto :goto_0

    .line 62
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 64
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v8, :cond_1

    .line 177
    iget-object v2, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 179
    :cond_1
    and-int/lit16 v1, v1, 0x4000

    if-ne v1, v9, :cond_2

    .line 180
    iget-object v1, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 182
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/gy;->au:Lcom/google/n/bn;

    throw v0

    .line 68
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 69
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 70
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 172
    :catch_1
    move-exception v0

    .line 173
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 174
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 74
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 75
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/gy;->d:I

    goto :goto_0

    .line 79
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 81
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    goto :goto_0

    .line 85
    :sswitch_5
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v8, :cond_3

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 88
    or-int/lit8 v1, v1, 0x10

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 90
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 95
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 96
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 97
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 101
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 102
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 103
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 107
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/gy;->i:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 112
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 113
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 114
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 118
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 119
    iget v6, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 120
    iput-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 124
    :sswitch_b
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    goto/16 :goto_0

    .line 129
    :sswitch_c
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/gy;->m:I

    goto/16 :goto_0

    .line 134
    :sswitch_d
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 135
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    goto/16 :goto_0

    .line 139
    :sswitch_e
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 140
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    goto/16 :goto_0

    .line 144
    :sswitch_f
    and-int/lit16 v0, v1, 0x4000

    if-eq v0, v9, :cond_5

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 147
    or-int/lit16 v1, v1, 0x4000

    .line 149
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 154
    :sswitch_10
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 155
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    goto/16 :goto_0

    .line 159
    :sswitch_11
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 160
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    goto/16 :goto_0

    .line 164
    :sswitch_12
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/a/gy;->a:I

    .line 165
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/gy;->s:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_2

    .line 176
    :cond_7
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v8, :cond_8

    .line 177
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 179
    :cond_8
    and-int/lit16 v0, v1, 0x4000

    if-ne v0, v9, :cond_9

    .line 180
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 182
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->au:Lcom/google/n/bn;

    .line 183
    return-void

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 569
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    .line 600
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    .line 616
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    .line 675
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    .line 691
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    .line 721
    iput-byte v1, p0, Lcom/google/maps/g/a/gy;->u:B

    .line 791
    iput v1, p0, Lcom/google/maps/g/a/gy;->v:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;
    .locals 1

    .prologue
    .line 941
    invoke-static {}, Lcom/google/maps/g/a/gy;->newBuilder()Lcom/google/maps/g/a/ha;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/ha;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/gy;
    .locals 1

    .prologue
    .line 2430
    sget-object v0, Lcom/google/maps/g/a/gy;->t:Lcom/google/maps/g/a/gy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/ha;
    .locals 1

    .prologue
    .line 938
    new-instance v0, Lcom/google/maps/g/a/ha;

    invoke-direct {v0}, Lcom/google/maps/g/a/ha;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/gy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    sget-object v0, Lcom/google/maps/g/a/gy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 733
    invoke-virtual {p0}, Lcom/google/maps/g/a/gy;->c()I

    .line 734
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 735
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 737
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 738
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 740
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 741
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/gy;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 743
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 744
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 746
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 747
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 746
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 735
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 738
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 741
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 744
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 749
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 750
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 752
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 753
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 755
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 756
    iget-boolean v0, p0, Lcom/google/maps/g/a/gy;->i:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_14

    move v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 758
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 759
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 761
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_d

    .line 762
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 764
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 765
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 767
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 768
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/maps/g/a/gy;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_17

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 770
    :cond_f
    :goto_a
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_10

    .line 771
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 773
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_11

    .line 774
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_11
    move v1, v2

    .line 776
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_18

    .line 777
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 776
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 750
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 753
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    :cond_14
    move v0, v2

    .line 756
    goto/16 :goto_7

    .line 759
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 762
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 768
    :cond_17
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_a

    .line 779
    :cond_18
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_19

    .line 780
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 782
    :cond_19
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_1a

    .line 783
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 785
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_1b

    .line 786
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/maps/g/a/gy;->s:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1c

    :goto_c
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 788
    :cond_1b
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 789
    return-void

    :cond_1c
    move v3, v2

    .line 786
    goto :goto_c
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 723
    iget-byte v1, p0, Lcom/google/maps/g/a/gy;->u:B

    .line 724
    if-ne v1, v0, :cond_0

    .line 728
    :goto_0
    return v0

    .line 725
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 727
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/gy;->u:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 793
    iget v0, p0, Lcom/google/maps/g/a/gy;->v:I

    .line 794
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 871
    :goto_0
    return v0

    .line 797
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1b

    .line 799
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 801
    :goto_2
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 803
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 805
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 806
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/maps/g/a/gy;->d:I

    .line 807
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 809
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 811
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    :cond_3
    move v4, v1

    move v1, v2

    .line 813
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 814
    const/4 v5, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    .line 815
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 813
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 799
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 803
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_6
    move v0, v3

    .line 807
    goto :goto_4

    .line 811
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 817
    :cond_8
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 818
    const/4 v1, 0x6

    .line 819
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 821
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 822
    const/4 v1, 0x7

    .line 823
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->h:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 825
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 826
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/maps/g/a/gy;->i:Z

    .line 827
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 829
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 830
    const/16 v1, 0x9

    .line 831
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->j:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 833
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_d

    .line 835
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gy;->k:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 837
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 838
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->l:Lcom/google/n/ao;

    .line 839
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 841
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_10

    .line 842
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/maps/g/a/gy;->m:I

    .line 843
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_f

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_f
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 845
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_11

    .line 846
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    .line 847
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 849
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_12

    .line 850
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->o:Lcom/google/n/ao;

    .line 851
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    :cond_12
    move v1, v2

    .line 853
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_17

    .line 854
    const/16 v3, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/gy;->p:Ljava/util/List;

    .line 855
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 853
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 819
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 823
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 831
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 835
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 857
    :cond_17
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_18

    .line 858
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->q:Lcom/google/n/ao;

    .line 859
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 861
    :cond_18
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_19

    .line 862
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/a/gy;->r:Lcom/google/n/ao;

    .line 863
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 865
    :cond_19
    iget v0, p0, Lcom/google/maps/g/a/gy;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_1a

    .line 866
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/maps/g/a/gy;->s:Z

    .line 867
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 869
    :cond_1a
    iget-object v0, p0, Lcom/google/maps/g/a/gy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 870
    iput v0, p0, Lcom/google/maps/g/a/gy;->v:I

    goto/16 :goto_0

    :cond_1b
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/gy;->newBuilder()Lcom/google/maps/g/a/ha;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/ha;->a(Lcom/google/maps/g/a/gy;)Lcom/google/maps/g/a/ha;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/gy;->newBuilder()Lcom/google/maps/g/a/ha;

    move-result-object v0

    return-object v0
.end method
