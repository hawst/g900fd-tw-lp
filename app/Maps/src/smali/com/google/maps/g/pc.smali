.class public final Lcom/google/maps/g/pc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/maps/g/pc;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field public d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field i:Ljava/lang/Object;

.field public j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field public l:Lcom/google/n/ao;

.field m:Lcom/google/n/ao;

.field public n:Lcom/google/n/ao;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/maps/g/pd;

    invoke-direct {v0}, Lcom/google/maps/g/pd;-><init>()V

    sput-object v0, Lcom/google/maps/g/pc;->PARSER:Lcom/google/n/ax;

    .line 672
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/pc;->r:Lcom/google/n/aw;

    .line 1859
    new-instance v0, Lcom/google/maps/g/pc;

    invoke-direct {v0}, Lcom/google/maps/g/pc;-><init>()V

    sput-object v0, Lcom/google/maps/g/pc;->o:Lcom/google/maps/g/pc;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    .line 263
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    .line 505
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    .line 521
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    .line 537
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    .line 552
    iput-byte v3, p0, Lcom/google/maps/g/pc;->p:B

    .line 607
    iput v3, p0, Lcom/google/maps/g/pc;->q:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/pc;->h:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    .line 28
    iget-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iget-object v0, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x20

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/maps/g/pc;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 43
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 45
    sparse-switch v4, :sswitch_data_0

    .line 50
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 52
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 58
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/pc;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v7, :cond_1

    .line 141
    iget-object v1, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    .line 143
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/pc;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 63
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 64
    iput-object v4, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 138
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :sswitch_3
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v7, :cond_2

    .line 69
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    .line 71
    or-int/lit8 v1, v1, 0x20

    .line 73
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 73
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 79
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 80
    iput-object v4, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 84
    :sswitch_5
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/g/pc;->a:I

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/pc;->h:I

    goto/16 :goto_0

    .line 89
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 90
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 91
    iput-object v4, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 95
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 96
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 97
    iput-object v4, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 101
    :sswitch_8
    iget-object v4, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 102
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/maps/g/pc;->a:I

    goto/16 :goto_0

    .line 106
    :sswitch_9
    iget-object v4, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 107
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/maps/g/pc;->a:I

    goto/16 :goto_0

    .line 111
    :sswitch_a
    iget-object v4, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 112
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/maps/g/pc;->a:I

    goto/16 :goto_0

    .line 116
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 117
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 118
    iput-object v4, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 122
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 123
    iget v5, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/pc;->a:I

    .line 124
    iput-object v4, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 128
    :sswitch_d
    iget-object v4, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 129
    iget v4, p0, Lcom/google/maps/g/pc;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/pc;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 140
    :cond_3
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v7, :cond_4

    .line 141
    iget-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    .line 143
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->au:Lcom/google/n/bn;

    .line 144
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    .line 263
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    .line 505
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    .line 521
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    .line 537
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    .line 552
    iput-byte v1, p0, Lcom/google/maps/g/pc;->p:B

    .line 607
    iput v1, p0, Lcom/google/maps/g/pc;->q:I

    .line 16
    return-void
.end method

.method public static i()Lcom/google/maps/g/pc;
    .locals 1

    .prologue
    .line 1862
    sget-object v0, Lcom/google/maps/g/pc;->o:Lcom/google/maps/g/pc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/pe;
    .locals 1

    .prologue
    .line 734
    new-instance v0, Lcom/google/maps/g/pe;

    invoke-direct {v0}, Lcom/google/maps/g/pe;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    sget-object v0, Lcom/google/maps/g/pc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 564
    invoke-virtual {p0}, Lcom/google/maps/g/pc;->c()I

    .line 565
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_0

    .line 566
    iget-object v0, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 568
    :cond_0
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 569
    iget-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 571
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 572
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 571
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 569
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 574
    :cond_3
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 575
    iget-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 577
    :cond_4
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 578
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/pc;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 580
    :cond_5
    :goto_3
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 581
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 583
    :cond_6
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 584
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 586
    :cond_7
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_8

    .line 587
    iget-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 589
    :cond_8
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 590
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 592
    :cond_9
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    .line 593
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 595
    :cond_a
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_b

    .line 596
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 598
    :cond_b
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_c

    .line 599
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 601
    :cond_c
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_d

    .line 602
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 604
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/pc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 605
    return-void

    .line 575
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 578
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 581
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 584
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 596
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 599
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_7
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 554
    iget-byte v1, p0, Lcom/google/maps/g/pc;->p:B

    .line 555
    if-ne v1, v0, :cond_0

    .line 559
    :goto_0
    return v0

    .line 556
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 558
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/pc;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 609
    iget v0, p0, Lcom/google/maps/g/pc;->q:I

    .line 610
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 667
    :goto_0
    return v0

    .line 613
    :cond_0
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_14

    .line 614
    iget-object v0, p0, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    .line 615
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 617
    :goto_1
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 619
    iget-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v1

    move v1, v2

    .line 621
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 622
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/maps/g/pc;->g:Ljava/util/List;

    .line 623
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 621
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 619
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 625
    :cond_3
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 627
    iget-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 629
    :cond_4
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 630
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/pc;->h:I

    .line 631
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v1, :cond_f

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 633
    :cond_5
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 634
    const/4 v1, 0x6

    .line 635
    iget-object v0, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 637
    :cond_6
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 638
    const/4 v1, 0x7

    .line 639
    iget-object v0, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->k:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 641
    :cond_7
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_8

    .line 642
    iget-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    .line 643
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 645
    :cond_8
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 646
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/pc;->m:Lcom/google/n/ao;

    .line 647
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 649
    :cond_9
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    .line 650
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    .line 651
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 653
    :cond_a
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_b

    .line 654
    const/16 v1, 0xb

    .line 655
    iget-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 657
    :cond_b
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_c

    .line 658
    const/16 v1, 0xc

    .line 659
    iget-object v0, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pc;->b:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 661
    :cond_c
    iget v0, p0, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_d

    .line 662
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/g/pc;->e:Lcom/google/n/ao;

    .line 663
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 665
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/pc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 666
    iput v0, p0, Lcom/google/maps/g/pc;->q:I

    goto/16 :goto_0

    .line 627
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 631
    :cond_f
    const/16 v0, 0xa

    goto/16 :goto_5

    .line 635
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 639
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 655
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 659
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_14
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    .line 217
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 218
    check-cast v0, Ljava/lang/String;

    .line 226
    :goto_0
    return-object v0

    .line 220
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 222
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iput-object v1, p0, Lcom/google/maps/g/pc;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 226
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/pc;->newBuilder()Lcom/google/maps/g/pe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/pe;->a(Lcom/google/maps/g/pc;)Lcom/google/maps/g/pe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/pc;->newBuilder()Lcom/google/maps/g/pe;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    .line 291
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 292
    check-cast v0, Ljava/lang/String;

    .line 300
    :goto_0
    return-object v0

    .line 294
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 296
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    iput-object v1, p0, Lcom/google/maps/g/pc;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 300
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    .line 391
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 392
    check-cast v0, Ljava/lang/String;

    .line 400
    :goto_0
    return-object v0

    .line 394
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 396
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    iput-object v1, p0, Lcom/google/maps/g/pc;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 400
    goto :goto_0
.end method
