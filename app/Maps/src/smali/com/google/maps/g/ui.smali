.class public final Lcom/google/maps/g/ui;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ul;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ui;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/ui;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 283
    new-instance v0, Lcom/google/maps/g/uj;

    invoke-direct {v0}, Lcom/google/maps/g/uj;-><init>()V

    sput-object v0, Lcom/google/maps/g/ui;->PARSER:Lcom/google/n/ax;

    .line 372
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ui;->g:Lcom/google/n/aw;

    .line 569
    new-instance v0, Lcom/google/maps/g/ui;

    invoke-direct {v0}, Lcom/google/maps/g/ui;-><init>()V

    sput-object v0, Lcom/google/maps/g/ui;->d:Lcom/google/maps/g/ui;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 234
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 329
    iput-byte v0, p0, Lcom/google/maps/g/ui;->e:B

    .line 351
    iput v0, p0, Lcom/google/maps/g/ui;->f:I

    .line 235
    iput v1, p0, Lcom/google/maps/g/ui;->b:I

    .line 236
    iput v1, p0, Lcom/google/maps/g/ui;->c:I

    .line 237
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 243
    invoke-direct {p0}, Lcom/google/maps/g/ui;-><init>()V

    .line 244
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 248
    const/4 v0, 0x0

    .line 249
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 250
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 251
    sparse-switch v3, :sswitch_data_0

    .line 256
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 258
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 254
    goto :goto_0

    .line 263
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/ui;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ui;->a:I

    .line 264
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/ui;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ui;->au:Lcom/google/n/bn;

    throw v0

    .line 268
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/ui;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/ui;->a:I

    .line 269
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/ui;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 276
    :catch_1
    move-exception v0

    .line 277
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 278
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 280
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ui;->au:Lcom/google/n/bn;

    .line 281
    return-void

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 232
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 329
    iput-byte v0, p0, Lcom/google/maps/g/ui;->e:B

    .line 351
    iput v0, p0, Lcom/google/maps/g/ui;->f:I

    .line 233
    return-void
.end method

.method public static d()Lcom/google/maps/g/ui;
    .locals 1

    .prologue
    .line 572
    sget-object v0, Lcom/google/maps/g/ui;->d:Lcom/google/maps/g/ui;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/uk;
    .locals 1

    .prologue
    .line 434
    new-instance v0, Lcom/google/maps/g/uk;

    invoke-direct {v0}, Lcom/google/maps/g/uk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ui;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    sget-object v0, Lcom/google/maps/g/ui;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 341
    invoke-virtual {p0}, Lcom/google/maps/g/ui;->c()I

    .line 342
    iget v0, p0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 343
    iget v0, p0, Lcom/google/maps/g/ui;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 345
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 346
    iget v0, p0, Lcom/google/maps/g/ui;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 348
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/ui;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 349
    return-void

    .line 343
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 346
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 331
    iget-byte v1, p0, Lcom/google/maps/g/ui;->e:B

    .line 332
    if-ne v1, v0, :cond_0

    .line 336
    :goto_0
    return v0

    .line 333
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 335
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/ui;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 353
    iget v0, p0, Lcom/google/maps/g/ui;->f:I

    .line 354
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 367
    :goto_0
    return v0

    .line 357
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 358
    iget v0, p0, Lcom/google/maps/g/ui;->b:I

    .line 359
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 361
    :goto_2
    iget v3, p0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 362
    iget v3, p0, Lcom/google/maps/g/ui;->c:I

    .line 363
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 365
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/ui;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    iput v0, p0, Lcom/google/maps/g/ui;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 359
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/google/maps/g/ui;->newBuilder()Lcom/google/maps/g/uk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/uk;->a(Lcom/google/maps/g/ui;)Lcom/google/maps/g/uk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/google/maps/g/ui;->newBuilder()Lcom/google/maps/g/uk;

    move-result-object v0

    return-object v0
.end method
