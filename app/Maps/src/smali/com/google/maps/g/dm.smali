.class public final Lcom/google/maps/g/dm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/dk;",
        "Lcom/google/maps/g/dm;",
        ">;",
        "Lcom/google/maps/g/dv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1279
    sget-object v0, Lcom/google/maps/g/dk;->d:Lcom/google/maps/g/dk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1345
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dm;->b:Lcom/google/n/ao;

    .line 1405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    .line 1280
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/dk;)Lcom/google/maps/g/dm;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1314
    invoke-static {}, Lcom/google/maps/g/dk;->d()Lcom/google/maps/g/dk;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1330
    :goto_0
    return-object p0

    .line 1315
    :cond_0
    iget v1, p1, Lcom/google/maps/g/dk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 1316
    iget-object v0, p0, Lcom/google/maps/g/dm;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/dk;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1317
    iget v0, p0, Lcom/google/maps/g/dm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/dm;->a:I

    .line 1319
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/dk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1320
    iget-object v0, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1321
    iget-object v0, p1, Lcom/google/maps/g/dk;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    .line 1322
    iget v0, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/dm;->a:I

    .line 1329
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/dk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1315
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1324
    :cond_4
    iget v0, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/dm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/dm;->a:I

    .line 1325
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/dk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1271
    new-instance v2, Lcom/google/maps/g/dk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/dk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/maps/g/dk;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/dm;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/dm;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/dm;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/dm;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/dk;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/dk;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1271
    check-cast p1, Lcom/google/maps/g/dk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/dm;->a(Lcom/google/maps/g/dk;)Lcom/google/maps/g/dm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1334
    iget v0, p0, Lcom/google/maps/g/dm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1335
    iget-object v0, p0, Lcom/google/maps/g/dm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/dn;->d()Lcom/google/maps/g/dn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/dn;

    invoke-virtual {v0}, Lcom/google/maps/g/dn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1340
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1334
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1340
    goto :goto_1
.end method
