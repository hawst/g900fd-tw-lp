.class public final Lcom/google/maps/g/a/bs;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/bq;",
        "Lcom/google/maps/g/a/bs;",
        ">;",
        "Lcom/google/maps/g/a/bt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/aq;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Lcom/google/n/ao;

.field private r:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 774
    sget-object v0, Lcom/google/maps/g/a/bq;->s:Lcom/google/maps/g/a/bq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 999
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->b:Ljava/lang/Object;

    .line 1075
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->c:Ljava/lang/Object;

    .line 1151
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->d:Lcom/google/n/ao;

    .line 1210
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->e:Lcom/google/n/ao;

    .line 1269
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->f:Lcom/google/n/ao;

    .line 1328
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->g:Lcom/google/n/ao;

    .line 1387
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->h:Lcom/google/n/ao;

    .line 1446
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->i:Lcom/google/n/ao;

    .line 1505
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->j:Lcom/google/n/ao;

    .line 1564
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->k:Lcom/google/n/ao;

    .line 1623
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    .line 1844
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->q:Lcom/google/n/ao;

    .line 1903
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->r:Ljava/lang/Object;

    .line 775
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;
    .locals 6

    .prologue
    const/16 v5, 0x400

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 915
    invoke-static {}, Lcom/google/maps/g/a/bq;->d()Lcom/google/maps/g/a/bq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 990
    :goto_0
    return-object p0

    .line 916
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_12

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 917
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 918
    iget-object v2, p1, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/bs;->b:Ljava/lang/Object;

    .line 921
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 922
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 923
    iget-object v2, p1, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/bs;->c:Ljava/lang/Object;

    .line 926
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 927
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 928
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 930
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 931
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 932
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 934
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 935
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 936
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 938
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 939
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 940
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 942
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 943
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 944
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 946
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 947
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 948
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 950
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 951
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 952
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 954
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 955
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 956
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 958
    :cond_a
    iget-object v2, p1, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 959
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 960
    iget-object v2, p1, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    .line 961
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 968
    :cond_b
    :goto_b
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x400

    if-ne v2, v5, :cond_1e

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 969
    iget v2, p1, Lcom/google/maps/g/a/bq;->m:I

    iget v3, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/bs;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bs;->m:I

    .line 971
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 972
    iget v2, p1, Lcom/google/maps/g/a/bq;->n:I

    iget v3, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/g/a/bs;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bs;->n:I

    .line 974
    :cond_d
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 975
    iget v2, p1, Lcom/google/maps/g/a/bq;->o:I

    iget v3, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/a/bs;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bs;->o:I

    .line 977
    :cond_e
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_f
    if-eqz v2, :cond_f

    .line 978
    iget v2, p1, Lcom/google/maps/g/a/bq;->p:I

    iget v3, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/maps/g/a/bs;->a:I

    iput v2, p0, Lcom/google/maps/g/a/bs;->p:I

    .line 980
    :cond_f
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 981
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 982
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 984
    :cond_10
    iget v2, p1, Lcom/google/maps/g/a/bq;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_23

    :goto_11
    if-eqz v0, :cond_11

    .line 985
    iget v0, p0, Lcom/google/maps/g/a/bs;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 986
    iget-object v0, p1, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/bs;->r:Ljava/lang/Object;

    .line 989
    :cond_11
    iget-object v0, p1, Lcom/google/maps/g/a/bq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v2, v1

    .line 916
    goto/16 :goto_1

    :cond_13
    move v2, v1

    .line 921
    goto/16 :goto_2

    :cond_14
    move v2, v1

    .line 926
    goto/16 :goto_3

    :cond_15
    move v2, v1

    .line 930
    goto/16 :goto_4

    :cond_16
    move v2, v1

    .line 934
    goto/16 :goto_5

    :cond_17
    move v2, v1

    .line 938
    goto/16 :goto_6

    :cond_18
    move v2, v1

    .line 942
    goto/16 :goto_7

    :cond_19
    move v2, v1

    .line 946
    goto/16 :goto_8

    :cond_1a
    move v2, v1

    .line 950
    goto/16 :goto_9

    :cond_1b
    move v2, v1

    .line 954
    goto/16 :goto_a

    .line 963
    :cond_1c
    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    and-int/lit16 v2, v2, 0x400

    if-eq v2, v5, :cond_1d

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/a/bs;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 964
    :cond_1d
    iget-object v2, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_1e
    move v2, v1

    .line 968
    goto/16 :goto_c

    :cond_1f
    move v2, v1

    .line 971
    goto/16 :goto_d

    :cond_20
    move v2, v1

    .line 974
    goto/16 :goto_e

    :cond_21
    move v2, v1

    .line 977
    goto/16 :goto_f

    :cond_22
    move v2, v1

    .line 980
    goto :goto_10

    :cond_23
    move v0, v1

    .line 984
    goto :goto_11
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 766
    invoke-virtual {p0}, Lcom/google/maps/g/a/bs;->c()Lcom/google/maps/g/a/bq;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 766
    check-cast p1, Lcom/google/maps/g/a/bq;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/bs;->a(Lcom/google/maps/g/a/bq;)Lcom/google/maps/g/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 994
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/bq;
    .locals 9

    .prologue
    const/high16 v8, 0x10000

    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 820
    new-instance v2, Lcom/google/maps/g/a/bq;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/bq;-><init>(Lcom/google/n/v;)V

    .line 821
    iget v3, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 823
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_10

    .line 826
    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/bs;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/bq;->b:Ljava/lang/Object;

    .line 827
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 828
    or-int/lit8 v0, v0, 0x2

    .line 830
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/bs;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/bq;->c:Ljava/lang/Object;

    .line 831
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 832
    or-int/lit8 v0, v0, 0x4

    .line 834
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->d:Lcom/google/n/ao;

    .line 835
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->d:Lcom/google/n/ao;

    .line 836
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 834
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 837
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 838
    or-int/lit8 v0, v0, 0x8

    .line 840
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->e:Lcom/google/n/ao;

    .line 841
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->e:Lcom/google/n/ao;

    .line 842
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 840
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 843
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 844
    or-int/lit8 v0, v0, 0x10

    .line 846
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->f:Lcom/google/n/ao;

    .line 847
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->f:Lcom/google/n/ao;

    .line 848
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 846
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 849
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 850
    or-int/lit8 v0, v0, 0x20

    .line 852
    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->g:Lcom/google/n/ao;

    .line 853
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->g:Lcom/google/n/ao;

    .line 854
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 852
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 855
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 856
    or-int/lit8 v0, v0, 0x40

    .line 858
    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->h:Lcom/google/n/ao;

    .line 859
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->h:Lcom/google/n/ao;

    .line 860
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 858
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 861
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 862
    or-int/lit16 v0, v0, 0x80

    .line 864
    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->i:Lcom/google/n/ao;

    .line 865
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->i:Lcom/google/n/ao;

    .line 866
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 864
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 867
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 868
    or-int/lit16 v0, v0, 0x100

    .line 870
    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->j:Lcom/google/n/ao;

    .line 871
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->j:Lcom/google/n/ao;

    .line 872
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 870
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 873
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 874
    or-int/lit16 v0, v0, 0x200

    .line 876
    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->k:Lcom/google/n/ao;

    .line 877
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->k:Lcom/google/n/ao;

    .line 878
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 876
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 879
    iget v4, p0, Lcom/google/maps/g/a/bs;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 880
    iget-object v4, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    .line 881
    iget v4, p0, Lcom/google/maps/g/a/bs;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/maps/g/a/bs;->a:I

    .line 883
    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/a/bs;->l:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/a/bq;->l:Lcom/google/n/aq;

    .line 884
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 885
    or-int/lit16 v0, v0, 0x400

    .line 887
    :cond_a
    iget v4, p0, Lcom/google/maps/g/a/bs;->m:I

    iput v4, v2, Lcom/google/maps/g/a/bq;->m:I

    .line 888
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 889
    or-int/lit16 v0, v0, 0x800

    .line 891
    :cond_b
    iget v4, p0, Lcom/google/maps/g/a/bs;->n:I

    iput v4, v2, Lcom/google/maps/g/a/bq;->n:I

    .line 892
    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    .line 893
    or-int/lit16 v0, v0, 0x1000

    .line 895
    :cond_c
    iget v4, p0, Lcom/google/maps/g/a/bs;->o:I

    iput v4, v2, Lcom/google/maps/g/a/bq;->o:I

    .line 896
    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    .line 897
    or-int/lit16 v0, v0, 0x2000

    .line 899
    :cond_d
    iget v4, p0, Lcom/google/maps/g/a/bs;->p:I

    iput v4, v2, Lcom/google/maps/g/a/bq;->p:I

    .line 900
    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    .line 901
    or-int/lit16 v0, v0, 0x4000

    .line 903
    :cond_e
    iget-object v4, v2, Lcom/google/maps/g/a/bq;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/bs;->q:Lcom/google/n/ao;

    .line 904
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/bs;->q:Lcom/google/n/ao;

    .line 905
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 903
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 906
    and-int v1, v3, v8

    if-ne v1, v8, :cond_f

    .line 907
    or-int/2addr v0, v7

    .line 909
    :cond_f
    iget-object v1, p0, Lcom/google/maps/g/a/bs;->r:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/bq;->r:Ljava/lang/Object;

    .line 910
    iput v0, v2, Lcom/google/maps/g/a/bq;->a:I

    .line 911
    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method
