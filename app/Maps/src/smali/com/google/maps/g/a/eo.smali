.class public final Lcom/google/maps/g/a/eo;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/et;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/a/eo;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:J

.field d:F

.field public e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/maps/g/a/ep;

    invoke-direct {v0}, Lcom/google/maps/g/a/ep;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/eo;->PARSER:Lcom/google/n/ax;

    .line 303
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/eo;->i:Lcom/google/n/aw;

    .line 586
    new-instance v0, Lcom/google/maps/g/a/eo;

    invoke-direct {v0}, Lcom/google/maps/g/a/eo;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/eo;->f:Lcom/google/maps/g/a/eo;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 246
    iput-byte v0, p0, Lcom/google/maps/g/a/eo;->g:B

    .line 274
    iput v0, p0, Lcom/google/maps/g/a/eo;->h:I

    .line 18
    iput v2, p0, Lcom/google/maps/g/a/eo;->b:I

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/g/a/eo;->c:J

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/eo;->d:F

    .line 21
    iput v2, p0, Lcom/google/maps/g/a/eo;->e:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/a/eo;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    const/4 v0, 0x0

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/eo;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/a/eo;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/eo;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/eo;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/g/a/eo;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/eo;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/maps/g/a/eo;->d:F

    goto :goto_0

    .line 63
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 64
    invoke-static {v3}, Lcom/google/maps/g/a/er;->a(I)Lcom/google/maps/g/a/er;

    move-result-object v4

    .line 65
    if-nez v4, :cond_1

    .line 66
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 68
    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/eo;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/a/eo;->a:I

    .line 69
    iput v3, p0, Lcom/google/maps/g/a/eo;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/eo;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 246
    iput-byte v0, p0, Lcom/google/maps/g/a/eo;->g:B

    .line 274
    iput v0, p0, Lcom/google/maps/g/a/eo;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/eo;
    .locals 1

    .prologue
    .line 589
    sget-object v0, Lcom/google/maps/g/a/eo;->f:Lcom/google/maps/g/a/eo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/eq;
    .locals 1

    .prologue
    .line 365
    new-instance v0, Lcom/google/maps/g/a/eq;

    invoke-direct {v0}, Lcom/google/maps/g/a/eq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/g/a/eo;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 258
    invoke-virtual {p0}, Lcom/google/maps/g/a/eo;->c()I

    .line 259
    iget v0, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 260
    iget v0, p0, Lcom/google/maps/g/a/eo;->b:I

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 262
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 263
    iget-wide v0, p0, Lcom/google/maps/g/a/eo;->c:J

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 265
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 266
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/eo;->d:F

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 268
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 269
    iget v0, p0, Lcom/google/maps/g/a/eo;->e:I

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 271
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/eo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 272
    return-void

    .line 260
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 269
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 248
    iget-byte v1, p0, Lcom/google/maps/g/a/eo;->g:B

    .line 249
    if-ne v1, v0, :cond_0

    .line 253
    :goto_0
    return v0

    .line 250
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 252
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/eo;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v1, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 276
    iget v0, p0, Lcom/google/maps/g/a/eo;->h:I

    .line 277
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 298
    :goto_0
    return v0

    .line 280
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 281
    iget v0, p0, Lcom/google/maps/g/a/eo;->b:I

    .line 282
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 284
    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_1

    .line 285
    iget-wide v4, p0, Lcom/google/maps/g/a/eo;->c:J

    .line 286
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 288
    :cond_1
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_2

    .line 289
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/g/a/eo;->d:F

    .line 290
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    .line 292
    :cond_2
    iget v3, p0, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 293
    iget v3, p0, Lcom/google/maps/g/a/eo;->e:I

    .line 294
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 296
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/a/eo;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    iput v0, p0, Lcom/google/maps/g/a/eo;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 282
    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/eo;->newBuilder()Lcom/google/maps/g/a/eq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/eq;->a(Lcom/google/maps/g/a/eo;)Lcom/google/maps/g/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/eo;->newBuilder()Lcom/google/maps/g/a/eq;

    move-result-object v0

    return-object v0
.end method
