.class public final Lcom/google/maps/g/a/if;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ii;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/id;",
        "Lcom/google/maps/g/a/if;",
        ">;",
        "Lcom/google/maps/g/a/ii;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/maps/g/mg;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 710
    sget-object v0, Lcom/google/maps/g/a/id;->d:Lcom/google/maps/g/a/id;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 759
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/if;->b:I

    .line 795
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    .line 711
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 742
    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 750
    :goto_0
    return-object p0

    .line 743
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 744
    iget v2, p1, Lcom/google/maps/g/a/id;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/ig;->a(I)Lcom/google/maps/g/a/ig;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/ig;->a:Lcom/google/maps/g/a/ig;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 743
    goto :goto_1

    .line 744
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/if;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/if;->a:I

    iget v2, v2, Lcom/google/maps/g/a/ig;->c:I

    iput v2, p0, Lcom/google/maps/g/a/if;->b:I

    .line 746
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 747
    iget-object v0, p1, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v0

    :goto_3
    iget v1, p0, Lcom/google/maps/g/a/if;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v2

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    invoke-static {v1}, Lcom/google/maps/g/mg;->a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/mi;->a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/mi;->c()Lcom/google/maps/g/mg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    :goto_4
    iget v0, p0, Lcom/google/maps/g/a/if;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/if;->a:I

    .line 749
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/id;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 746
    goto :goto_2

    .line 747
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    goto :goto_3

    :cond_8
    iput-object v0, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/google/maps/g/a/if;->c()Lcom/google/maps/g/a/id;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 702
    check-cast p1, Lcom/google/maps/g/a/id;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/if;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/id;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 726
    new-instance v2, Lcom/google/maps/g/a/id;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/id;-><init>(Lcom/google/n/v;)V

    .line 727
    iget v3, p0, Lcom/google/maps/g/a/if;->a:I

    .line 728
    const/4 v1, 0x0

    .line 729
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 732
    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/if;->b:I

    iput v1, v2, Lcom/google/maps/g/a/id;->b:I

    .line 733
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 734
    or-int/lit8 v0, v0, 0x2

    .line 736
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/if;->c:Lcom/google/maps/g/mg;

    iput-object v1, v2, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    .line 737
    iput v0, v2, Lcom/google/maps/g/a/id;->a:I

    .line 738
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
