.class public final enum Lcom/google/maps/g/xa;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/xa;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/xa;

.field public static final enum b:Lcom/google/maps/g/xa;

.field public static final enum c:Lcom/google/maps/g/xa;

.field public static final enum d:Lcom/google/maps/g/xa;

.field public static final enum e:Lcom/google/maps/g/xa;

.field public static final enum f:Lcom/google/maps/g/xa;

.field public static final enum g:Lcom/google/maps/g/xa;

.field public static final enum h:Lcom/google/maps/g/xa;

.field public static final enum i:Lcom/google/maps/g/xa;

.field public static final enum j:Lcom/google/maps/g/xa;

.field private static final synthetic l:[Lcom/google/maps/g/xa;


# instance fields
.field final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 101
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_COPYRIGHTS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->a:Lcom/google/maps/g/xa;

    .line 105
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_LANGUAGE_SWITCHER"

    invoke-direct {v0, v1, v4, v7}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->b:Lcom/google/maps/g/xa;

    .line 109
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_BICYCLING_LAYER"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->c:Lcom/google/maps/g/xa;

    .line 113
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_TRANSIT_LAYER"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->d:Lcom/google/maps/g/xa;

    .line 117
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_TRAFFIC_LAYER"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->e:Lcom/google/maps/g/xa;

    .line 121
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_REPORT_MAPS_ISSUE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->f:Lcom/google/maps/g/xa;

    .line 125
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_MAPMAKER"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->g:Lcom/google/maps/g/xa;

    .line 129
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_TERRAIN_LAYER"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->h:Lcom/google/maps/g/xa;

    .line 133
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_RAP_ADD_A_PLACE"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->i:Lcom/google/maps/g/xa;

    .line 137
    new-instance v0, Lcom/google/maps/g/xa;

    const-string v1, "TYPE_REPORT_LOCAL_ISSUE"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/xa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/xa;->j:Lcom/google/maps/g/xa;

    .line 96
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/maps/g/xa;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/g/xa;->a:Lcom/google/maps/g/xa;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/xa;->b:Lcom/google/maps/g/xa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/xa;->c:Lcom/google/maps/g/xa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/xa;->d:Lcom/google/maps/g/xa;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/xa;->e:Lcom/google/maps/g/xa;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/xa;->f:Lcom/google/maps/g/xa;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/xa;->g:Lcom/google/maps/g/xa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/xa;->h:Lcom/google/maps/g/xa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/xa;->i:Lcom/google/maps/g/xa;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/xa;->j:Lcom/google/maps/g/xa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/xa;->l:[Lcom/google/maps/g/xa;

    .line 207
    new-instance v0, Lcom/google/maps/g/xb;

    invoke-direct {v0}, Lcom/google/maps/g/xb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 217
    iput p3, p0, Lcom/google/maps/g/xa;->k:I

    .line 218
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/xa;
    .locals 1

    .prologue
    .line 187
    packed-switch p0, :pswitch_data_0

    .line 198
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 188
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/xa;->a:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 189
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/xa;->b:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 190
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/xa;->c:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 191
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/xa;->d:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 192
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/xa;->e:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 193
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/xa;->f:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 194
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/xa;->g:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 195
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/xa;->h:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 196
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/xa;->i:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 197
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/xa;->j:Lcom/google/maps/g/xa;

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/xa;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/maps/g/xa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/xa;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/xa;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/g/xa;->l:[Lcom/google/maps/g/xa;

    invoke-virtual {v0}, [Lcom/google/maps/g/xa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/xa;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/google/maps/g/xa;->k:I

    return v0
.end method
