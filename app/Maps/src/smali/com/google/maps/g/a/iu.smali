.class public final Lcom/google/maps/g/a/iu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ix;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/is;",
        "Lcom/google/maps/g/a/iu;",
        ">;",
        "Lcom/google/maps/g/a/ix;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 999
    sget-object v0, Lcom/google/maps/g/a/is;->d:Lcom/google/maps/g/a/is;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1051
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/iu;->b:I

    .line 1087
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/iu;->c:Lcom/google/n/ao;

    .line 1000
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1033
    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1042
    :goto_0
    return-object p0

    .line 1034
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/is;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1035
    iget v2, p1, Lcom/google/maps/g/a/is;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/iv;->a(I)Lcom/google/maps/g/a/iv;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/iv;->b:Lcom/google/maps/g/a/iv;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1034
    goto :goto_1

    .line 1035
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/iu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/iu;->a:I

    iget v2, v2, Lcom/google/maps/g/a/iv;->d:I

    iput v2, p0, Lcom/google/maps/g/a/iu;->b:I

    .line 1037
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/is;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 1038
    iget-object v0, p0, Lcom/google/maps/g/a/iu;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/is;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1039
    iget v0, p0, Lcom/google/maps/g/a/iu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/iu;->a:I

    .line 1041
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/is;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1037
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/google/maps/g/a/iu;->c()Lcom/google/maps/g/a/is;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 991
    check-cast p1, Lcom/google/maps/g/a/is;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/iu;->a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1046
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/is;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1015
    new-instance v2, Lcom/google/maps/g/a/is;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/is;-><init>(Lcom/google/n/v;)V

    .line 1016
    iget v3, p0, Lcom/google/maps/g/a/iu;->a:I

    .line 1018
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 1021
    :goto_0
    iget v4, p0, Lcom/google/maps/g/a/iu;->b:I

    iput v4, v2, Lcom/google/maps/g/a/is;->b:I

    .line 1022
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1023
    or-int/lit8 v0, v0, 0x2

    .line 1025
    :cond_0
    iget-object v3, v2, Lcom/google/maps/g/a/is;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/iu;->c:Lcom/google/n/ao;

    .line 1026
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/iu;->c:Lcom/google/n/ao;

    .line 1027
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1025
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 1028
    iput v0, v2, Lcom/google/maps/g/a/is;->a:I

    .line 1029
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
