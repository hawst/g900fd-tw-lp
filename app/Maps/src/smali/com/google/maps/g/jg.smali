.class public final Lcom/google/maps/g/jg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/jt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/je;",
        "Lcom/google/maps/g/jg;",
        ">;",
        "Lcom/google/maps/g/jt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1759
    sget-object v0, Lcom/google/maps/g/je;->e:Lcom/google/maps/g/je;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1832
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    .line 1968
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/jg;->c:Lcom/google/n/ao;

    .line 2027
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/jg;->d:Lcom/google/n/ao;

    .line 1760
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/je;)Lcom/google/maps/g/jg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1802
    invoke-static {}, Lcom/google/maps/g/je;->d()Lcom/google/maps/g/je;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1822
    :goto_0
    return-object p0

    .line 1803
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/je;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1804
    iget-object v2, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1805
    iget-object v2, p1, Lcom/google/maps/g/je;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    .line 1806
    iget v2, p0, Lcom/google/maps/g/jg;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/jg;->a:I

    .line 1813
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/je;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1814
    iget-object v2, p0, Lcom/google/maps/g/jg;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/je;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1815
    iget v2, p0, Lcom/google/maps/g/jg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/jg;->a:I

    .line 1817
    :cond_2
    iget v2, p1, Lcom/google/maps/g/je;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 1818
    iget-object v0, p0, Lcom/google/maps/g/jg;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/je;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1819
    iget v0, p0, Lcom/google/maps/g/jg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/jg;->a:I

    .line 1821
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/je;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1808
    :cond_4
    iget v2, p0, Lcom/google/maps/g/jg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/jg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/jg;->a:I

    .line 1809
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/je;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 1813
    goto :goto_2

    :cond_7
    move v0, v1

    .line 1817
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1751
    new-instance v2, Lcom/google/maps/g/je;

    invoke-direct {v2, p0}, Lcom/google/maps/g/je;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/jg;->a:I

    iget v4, p0, Lcom/google/maps/g/jg;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/jg;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/jg;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/jg;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/je;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/je;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/jg;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/jg;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/je;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/jg;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/jg;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/je;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1751
    check-cast p1, Lcom/google/maps/g/je;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/jg;->a(Lcom/google/maps/g/je;)Lcom/google/maps/g/jg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1826
    const/4 v0, 0x1

    return v0
.end method
