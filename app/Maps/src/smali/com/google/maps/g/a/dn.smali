.class public final Lcom/google/maps/g/a/dn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ds;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/dn;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/a/dn;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/maps/g/a/do;

    invoke-direct {v0}, Lcom/google/maps/g/a/do;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/dn;->PARSER:Lcom/google/n/ax;

    .line 221
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/dn;->g:Lcom/google/n/aw;

    .line 452
    new-instance v0, Lcom/google/maps/g/a/dn;

    invoke-direct {v0}, Lcom/google/maps/g/a/dn;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/dn;->d:Lcom/google/maps/g/a/dn;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 163
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    .line 178
    iput-byte v2, p0, Lcom/google/maps/g/a/dn;->e:B

    .line 200
    iput v2, p0, Lcom/google/maps/g/a/dn;->f:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/dn;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/a/dn;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 47
    invoke-static {v3}, Lcom/google/maps/g/a/dq;->a(I)Lcom/google/maps/g/a/dq;

    move-result-object v4

    .line 48
    if-nez v4, :cond_1

    .line 49
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/dn;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/maps/g/a/dn;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/a/dn;->a:I

    .line 52
    iput v3, p0, Lcom/google/maps/g/a/dn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 58
    iget v3, p0, Lcom/google/maps/g/a/dn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/dn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/dn;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 163
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    .line 178
    iput-byte v1, p0, Lcom/google/maps/g/a/dn;->e:B

    .line 200
    iput v1, p0, Lcom/google/maps/g/a/dn;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/dn;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/google/maps/g/a/dn;->d:Lcom/google/maps/g/a/dn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/dp;
    .locals 1

    .prologue
    .line 283
    new-instance v0, Lcom/google/maps/g/a/dp;

    invoke-direct {v0}, Lcom/google/maps/g/a/dp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/dn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/g/a/dn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 190
    invoke-virtual {p0}, Lcom/google/maps/g/a/dn;->c()I

    .line 191
    iget v0, p0, Lcom/google/maps/g/a/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 192
    iget v0, p0, Lcom/google/maps/g/a/dn;->b:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 194
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/dn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 195
    iget-object v0, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/dn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 198
    return-void

    .line 192
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 180
    iget-byte v1, p0, Lcom/google/maps/g/a/dn;->e:B

    .line 181
    if-ne v1, v0, :cond_0

    .line 185
    :goto_0
    return v0

    .line 182
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 184
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/dn;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 202
    iget v0, p0, Lcom/google/maps/g/a/dn;->f:I

    .line 203
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 216
    :goto_0
    return v0

    .line 206
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 207
    iget v0, p0, Lcom/google/maps/g/a/dn;->b:I

    .line 208
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 210
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/dn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 211
    iget-object v2, p0, Lcom/google/maps/g/a/dn;->c:Lcom/google/n/ao;

    .line 212
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/dn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    iput v0, p0, Lcom/google/maps/g/a/dn;->f:I

    goto :goto_0

    .line 208
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/dn;->newBuilder()Lcom/google/maps/g/a/dp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/dp;->a(Lcom/google/maps/g/a/dn;)Lcom/google/maps/g/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/dn;->newBuilder()Lcom/google/maps/g/a/dp;

    move-result-object v0

    return-object v0
.end method
