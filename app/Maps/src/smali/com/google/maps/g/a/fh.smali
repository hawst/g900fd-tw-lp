.class public final Lcom/google/maps/g/a/fh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/fi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ff;",
        "Lcom/google/maps/g/a/fh;",
        ">;",
        "Lcom/google/maps/g/a/fi;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 397
    sget-object v0, Lcom/google/maps/g/a/ff;->g:Lcom/google/maps/g/a/ff;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 501
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/fh;->b:Ljava/lang/Object;

    .line 577
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/fh;->c:Lcom/google/n/ao;

    .line 637
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    .line 805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/fh;->f:Lcom/google/n/ao;

    .line 398
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ff;)Lcom/google/maps/g/a/fh;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 452
    invoke-static {}, Lcom/google/maps/g/a/ff;->d()Lcom/google/maps/g/a/ff;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 480
    :goto_0
    return-object p0

    .line 453
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 454
    iget v2, p0, Lcom/google/maps/g/a/fh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/fh;->a:I

    .line 455
    iget-object v2, p1, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/fh;->b:Ljava/lang/Object;

    .line 458
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 459
    iget-object v2, p0, Lcom/google/maps/g/a/fh;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 460
    iget v2, p0, Lcom/google/maps/g/a/fh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/fh;->a:I

    .line 462
    :cond_2
    iget-object v2, p1, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 463
    iget-object v2, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 464
    iget-object v2, p1, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    .line 465
    iget v2, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/a/fh;->a:I

    .line 472
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 473
    iget-boolean v2, p1, Lcom/google/maps/g/a/ff;->e:Z

    iget v3, p0, Lcom/google/maps/g/a/fh;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/fh;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/fh;->e:Z

    .line 475
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 476
    iget-object v0, p0, Lcom/google/maps/g/a/fh;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 477
    iget v0, p0, Lcom/google/maps/g/a/fh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/fh;->a:I

    .line 479
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/ff;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 453
    goto :goto_1

    :cond_7
    move v2, v1

    .line 458
    goto :goto_2

    .line 467
    :cond_8
    iget v2, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/fh;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/fh;->a:I

    .line 468
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 472
    goto :goto_4

    :cond_b
    move v0, v1

    .line 475
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    new-instance v2, Lcom/google/maps/g/a/ff;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ff;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/fh;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/ff;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/fh;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/fh;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/a/fh;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/a/fh;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ff;->e:Z

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/fh;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/fh;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/ff;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 389
    check-cast p1, Lcom/google/maps/g/a/ff;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/fh;->a(Lcom/google/maps/g/a/ff;)Lcom/google/maps/g/a/fh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 484
    iget v0, p0, Lcom/google/maps/g/a/fh;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 485
    iget-object v0, p0, Lcom/google/maps/g/a/fh;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 496
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 484
    goto :goto_0

    :cond_2
    move v1, v2

    .line 490
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/google/maps/g/a/fh;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/eu;->d()Lcom/google/maps/g/a/eu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/eu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 496
    goto :goto_1
.end method
