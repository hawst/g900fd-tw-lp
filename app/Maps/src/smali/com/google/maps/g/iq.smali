.class public final Lcom/google/maps/g/iq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ir;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/io;",
        "Lcom/google/maps/g/iq;",
        ">;",
        "Lcom/google/maps/g/ir;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Z

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/google/maps/g/io;->l:Lcom/google/maps/g/io;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 680
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/iq;->d:Ljava/lang/Object;

    .line 756
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->e:Lcom/google/n/ao;

    .line 815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/iq;->b:Ljava/lang/Object;

    .line 891
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->f:Lcom/google/n/ao;

    .line 950
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->g:Lcom/google/n/ao;

    .line 1009
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->h:Lcom/google/n/ao;

    .line 1068
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->c:Lcom/google/n/ao;

    .line 1159
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->j:Lcom/google/n/ao;

    .line 1218
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/iq;->k:Lcom/google/n/ao;

    .line 529
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/io;)Lcom/google/maps/g/iq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 622
    invoke-static {}, Lcom/google/maps/g/io;->d()Lcom/google/maps/g/io;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 665
    :goto_0
    return-object p0

    .line 623
    :cond_0
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 624
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 625
    iget-object v2, p1, Lcom/google/maps/g/io;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/iq;->d:Ljava/lang/Object;

    .line 628
    :cond_1
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 629
    iget-object v2, p0, Lcom/google/maps/g/iq;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 630
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 632
    :cond_2
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 633
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 634
    iget-object v2, p1, Lcom/google/maps/g/io;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/iq;->b:Ljava/lang/Object;

    .line 637
    :cond_3
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 638
    iget-object v2, p0, Lcom/google/maps/g/iq;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 639
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 641
    :cond_4
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 642
    iget-object v2, p0, Lcom/google/maps/g/iq;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 643
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 645
    :cond_5
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 646
    iget-object v2, p0, Lcom/google/maps/g/iq;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 647
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 649
    :cond_6
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 650
    iget-object v2, p0, Lcom/google/maps/g/iq;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 651
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 653
    :cond_7
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 654
    iget-boolean v2, p1, Lcom/google/maps/g/io;->i:Z

    iget v3, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/iq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/iq;->i:Z

    .line 656
    :cond_8
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 657
    iget-object v2, p0, Lcom/google/maps/g/iq;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/io;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 658
    iget v2, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/iq;->a:I

    .line 660
    :cond_9
    iget v2, p1, Lcom/google/maps/g/io;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_a

    .line 661
    iget-object v0, p0, Lcom/google/maps/g/iq;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/io;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 662
    iget v0, p0, Lcom/google/maps/g/iq;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/iq;->a:I

    .line 664
    :cond_a
    iget-object v0, p1, Lcom/google/maps/g/io;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 623
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 628
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 632
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 637
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 641
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 645
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 649
    goto :goto_7

    :cond_12
    move v2, v1

    .line 653
    goto :goto_8

    :cond_13
    move v2, v1

    .line 656
    goto :goto_9

    :cond_14
    move v0, v1

    .line 660
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 520
    new-instance v2, Lcom/google/maps/g/io;

    invoke-direct {v2, p0}, Lcom/google/maps/g/io;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/iq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/iq;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/io;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/io;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/iq;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/io;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/io;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/io;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/io;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/io;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v4, p0, Lcom/google/maps/g/iq;->i:Z

    iput-boolean v4, v2, Lcom/google/maps/g/io;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/io;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/iq;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/iq;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v3, v2, Lcom/google/maps/g/io;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/iq;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/iq;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/io;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 520
    check-cast p1, Lcom/google/maps/g/io;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/iq;->a(Lcom/google/maps/g/io;)Lcom/google/maps/g/iq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 669
    iget v0, p0, Lcom/google/maps/g/iq;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 670
    iget-object v0, p0, Lcom/google/maps/g/iq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 675
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 669
    goto :goto_0

    :cond_1
    move v0, v2

    .line 675
    goto :goto_1
.end method
