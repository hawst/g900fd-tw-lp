.class public final Lcom/google/maps/g/fq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ft;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fq;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/fq;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/maps/g/fr;

    invoke-direct {v0}, Lcom/google/maps/g/fr;-><init>()V

    sput-object v0, Lcom/google/maps/g/fq;->PARSER:Lcom/google/n/ax;

    .line 239
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/fq;->j:Lcom/google/n/aw;

    .line 559
    new-instance v0, Lcom/google/maps/g/fq;

    invoke-direct {v0}, Lcom/google/maps/g/fq;-><init>()V

    sput-object v0, Lcom/google/maps/g/fq;->g:Lcom/google/maps/g/fq;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 175
    iput-byte v1, p0, Lcom/google/maps/g/fq;->h:B

    .line 206
    iput v1, p0, Lcom/google/maps/g/fq;->i:I

    .line 18
    iput v0, p0, Lcom/google/maps/g/fq;->b:I

    .line 19
    iput v0, p0, Lcom/google/maps/g/fq;->c:I

    .line 20
    iput v0, p0, Lcom/google/maps/g/fq;->d:I

    .line 21
    iput v0, p0, Lcom/google/maps/g/fq;->e:I

    .line 22
    iput v0, p0, Lcom/google/maps/g/fq;->f:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/fq;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    const/4 v0, 0x0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/fq;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/fq;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fq;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/fq;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/fq;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/fq;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/fq;->d:I

    goto :goto_0

    .line 64
    :sswitch_4
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/fq;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/fq;->e:I

    goto :goto_0

    .line 69
    :sswitch_5
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/fq;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/fq;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fq;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 175
    iput-byte v0, p0, Lcom/google/maps/g/fq;->h:B

    .line 206
    iput v0, p0, Lcom/google/maps/g/fq;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/fq;
    .locals 1

    .prologue
    .line 562
    sget-object v0, Lcom/google/maps/g/fq;->g:Lcom/google/maps/g/fq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/fs;
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/google/maps/g/fs;

    invoke-direct {v0}, Lcom/google/maps/g/fs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/fq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/g/fq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/google/maps/g/fq;->c()I

    .line 188
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 189
    iget v0, p0, Lcom/google/maps/g/fq;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 191
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 192
    iget v0, p0, Lcom/google/maps/g/fq;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 194
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 195
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/fq;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 197
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 198
    iget v0, p0, Lcom/google/maps/g/fq;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 200
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 201
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/fq;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 203
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/fq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 204
    return-void

    .line 189
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 192
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 195
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 198
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 201
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 177
    iget-byte v1, p0, Lcom/google/maps/g/fq;->h:B

    .line 178
    if-ne v1, v0, :cond_0

    .line 182
    :goto_0
    return v0

    .line 179
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 181
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/fq;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 208
    iget v0, p0, Lcom/google/maps/g/fq;->i:I

    .line 209
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 234
    :goto_0
    return v0

    .line 212
    :cond_0
    iget v0, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 213
    iget v0, p0, Lcom/google/maps/g/fq;->b:I

    .line 214
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_2
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 217
    iget v3, p0, Lcom/google/maps/g/fq;->c:I

    .line 218
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 220
    :cond_1
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 221
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/g/fq;->d:I

    .line 222
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 224
    :cond_2
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 225
    iget v3, p0, Lcom/google/maps/g/fq;->e:I

    .line 226
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 228
    :cond_3
    iget v3, p0, Lcom/google/maps/g/fq;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_5

    .line 229
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/maps/g/fq;->f:I

    .line 230
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_4
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 232
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/fq;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    iput v0, p0, Lcom/google/maps/g/fq;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 214
    goto :goto_1

    :cond_7
    move v3, v1

    .line 218
    goto :goto_3

    :cond_8
    move v3, v1

    .line 222
    goto :goto_4

    :cond_9
    move v3, v1

    .line 226
    goto :goto_5

    :cond_a
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/fq;->newBuilder()Lcom/google/maps/g/fs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/fs;->a(Lcom/google/maps/g/fq;)Lcom/google/maps/g/fs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/fq;->newBuilder()Lcom/google/maps/g/fs;

    move-result-object v0

    return-object v0
.end method
