.class public final Lcom/google/maps/g/a/cy;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/cw;",
        "Lcom/google/maps/g/a/cy;",
        ">;",
        "Lcom/google/maps/g/a/cz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:D

.field private c:D

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/google/maps/g/a/cw;->f:Lcom/google/maps/g/a/cw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 482
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cy;->d:Ljava/lang/Object;

    .line 558
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cy;->e:Ljava/lang/Object;

    .line 348
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 391
    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 409
    :goto_0
    return-object p0

    .line 392
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 393
    iget-wide v2, p1, Lcom/google/maps/g/a/cw;->b:D

    iget v4, p0, Lcom/google/maps/g/a/cy;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/a/cy;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/a/cy;->b:D

    .line 395
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 396
    iget-wide v2, p1, Lcom/google/maps/g/a/cw;->c:D

    iget v4, p0, Lcom/google/maps/g/a/cy;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/a/cy;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/a/cy;->c:D

    .line 398
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 399
    iget v2, p0, Lcom/google/maps/g/a/cy;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/cy;->a:I

    .line 400
    iget-object v2, p1, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/cy;->d:Ljava/lang/Object;

    .line 403
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/cw;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 404
    iget v0, p0, Lcom/google/maps/g/a/cy;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/cy;->a:I

    .line 405
    iget-object v0, p1, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/cy;->e:Ljava/lang/Object;

    .line 408
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/a/cw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 392
    goto :goto_1

    :cond_6
    move v2, v1

    .line 395
    goto :goto_2

    :cond_7
    move v2, v1

    .line 398
    goto :goto_3

    :cond_8
    move v0, v1

    .line 403
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/maps/g/a/cy;->c()Lcom/google/maps/g/a/cw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 339
    check-cast p1, Lcom/google/maps/g/a/cw;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/cw;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 367
    new-instance v2, Lcom/google/maps/g/a/cw;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/cw;-><init>(Lcom/google/n/v;)V

    .line 368
    iget v3, p0, Lcom/google/maps/g/a/cy;->a:I

    .line 369
    const/4 v1, 0x0

    .line 370
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 373
    :goto_0
    iget-wide v4, p0, Lcom/google/maps/g/a/cy;->b:D

    iput-wide v4, v2, Lcom/google/maps/g/a/cw;->b:D

    .line 374
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 375
    or-int/lit8 v0, v0, 0x2

    .line 377
    :cond_0
    iget-wide v4, p0, Lcom/google/maps/g/a/cy;->c:D

    iput-wide v4, v2, Lcom/google/maps/g/a/cw;->c:D

    .line 378
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 379
    or-int/lit8 v0, v0, 0x4

    .line 381
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/cy;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    .line 382
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 383
    or-int/lit8 v0, v0, 0x8

    .line 385
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/cy;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/cw;->e:Ljava/lang/Object;

    .line 386
    iput v0, v2, Lcom/google/maps/g/a/cw;->a:I

    .line 387
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
