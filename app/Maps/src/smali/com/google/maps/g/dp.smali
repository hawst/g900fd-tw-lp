.class public final Lcom/google/maps/g/dp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/du;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/dn;",
        "Lcom/google/maps/g/dp;",
        ">;",
        "Lcom/google/maps/g/du;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/dw;

.field private c:Lcom/google/maps/g/dq;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 897
    sget-object v0, Lcom/google/maps/g/dn;->d:Lcom/google/maps/g/dn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 952
    iput-object v1, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    .line 1013
    iput-object v1, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    .line 898
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/dn;)Lcom/google/maps/g/dp;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 929
    invoke-static {}, Lcom/google/maps/g/dn;->d()Lcom/google/maps/g/dn;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 937
    :goto_0
    return-object p0

    .line 930
    :cond_0
    iget v0, p1, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 931
    iget-object v0, p1, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/dp;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    invoke-static {v3}, Lcom/google/maps/g/dw;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/dy;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/dy;->c()Lcom/google/maps/g/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/dp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/dp;->a:I

    .line 933
    :cond_1
    iget v0, p1, Lcom/google/maps/g/dn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 934
    iget-object v0, p1, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/dq;->d()Lcom/google/maps/g/dq;

    move-result-object v0

    :goto_5
    iget v1, p0, Lcom/google/maps/g/dp;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    invoke-static {}, Lcom/google/maps/g/dq;->d()Lcom/google/maps/g/dq;

    move-result-object v2

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    invoke-static {v1}, Lcom/google/maps/g/dq;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ds;->a(Lcom/google/maps/g/dq;)Lcom/google/maps/g/ds;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ds;->c()Lcom/google/maps/g/dq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    :goto_6
    iget v0, p0, Lcom/google/maps/g/dp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/dp;->a:I

    .line 936
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/dn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 930
    goto :goto_1

    .line 931
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 933
    goto :goto_4

    .line 934
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    goto :goto_5

    :cond_8
    iput-object v0, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 889
    new-instance v2, Lcom/google/maps/g/dn;

    invoke-direct {v2, p0}, Lcom/google/maps/g/dn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/dp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    iput-object v1, v2, Lcom/google/maps/g/dn;->b:Lcom/google/maps/g/dw;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/dp;->c:Lcom/google/maps/g/dq;

    iput-object v1, v2, Lcom/google/maps/g/dn;->c:Lcom/google/maps/g/dq;

    iput v0, v2, Lcom/google/maps/g/dn;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 889
    check-cast p1, Lcom/google/maps/g/dn;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/dp;->a(Lcom/google/maps/g/dn;)Lcom/google/maps/g/dp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 941
    iget v0, p0, Lcom/google/maps/g/dp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 942
    iget-object v0, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/dw;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 947
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 941
    goto :goto_0

    .line 942
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/dp;->b:Lcom/google/maps/g/dw;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 947
    goto :goto_2
.end method
