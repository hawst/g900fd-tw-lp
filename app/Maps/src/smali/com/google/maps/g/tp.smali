.class public final Lcom/google/maps/g/tp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/uc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tp;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/tp;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/maps/g/tq;

    invoke-direct {v0}, Lcom/google/maps/g/tq;-><init>()V

    sput-object v0, Lcom/google/maps/g/tp;->PARSER:Lcom/google/n/ax;

    .line 1411
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/tp;->g:Lcom/google/n/aw;

    .line 1642
    new-instance v0, Lcom/google/maps/g/tp;

    invoke-direct {v0}, Lcom/google/maps/g/tp;-><init>()V

    sput-object v0, Lcom/google/maps/g/tp;->d:Lcom/google/maps/g/tp;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1337
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    .line 1368
    iput-byte v2, p0, Lcom/google/maps/g/tp;->e:B

    .line 1390
    iput v2, p0, Lcom/google/maps/g/tp;->f:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/tp;->c:I

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/tp;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 47
    iget v3, p0, Lcom/google/maps/g/tp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/tp;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/tp;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 52
    invoke-static {v3}, Lcom/google/maps/g/ts;->a(I)Lcom/google/maps/g/ts;

    move-result-object v4

    .line 53
    if-nez v4, :cond_1

    .line 54
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/maps/g/tp;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/tp;->a:I

    .line 57
    iput v3, p0, Lcom/google/maps/g/tp;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tp;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1337
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    .line 1368
    iput-byte v1, p0, Lcom/google/maps/g/tp;->e:B

    .line 1390
    iput v1, p0, Lcom/google/maps/g/tp;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/tp;
    .locals 1

    .prologue
    .line 1645
    sget-object v0, Lcom/google/maps/g/tp;->d:Lcom/google/maps/g/tp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/tr;
    .locals 1

    .prologue
    .line 1473
    new-instance v0, Lcom/google/maps/g/tr;

    invoke-direct {v0}, Lcom/google/maps/g/tr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/g/tp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 1380
    invoke-virtual {p0}, Lcom/google/maps/g/tp;->c()I

    .line 1381
    iget v0, p0, Lcom/google/maps/g/tp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1382
    iget-object v0, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1384
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1385
    iget v0, p0, Lcom/google/maps/g/tp;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1387
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/tp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1388
    return-void

    .line 1385
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1370
    iget-byte v1, p0, Lcom/google/maps/g/tp;->e:B

    .line 1371
    if-ne v1, v0, :cond_0

    .line 1375
    :goto_0
    return v0

    .line 1372
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1374
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/tp;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1392
    iget v0, p0, Lcom/google/maps/g/tp;->f:I

    .line 1393
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1406
    :goto_0
    return v0

    .line 1396
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1397
    iget-object v0, p0, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    .line 1398
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1400
    :goto_1
    iget v2, p0, Lcom/google/maps/g/tp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1401
    iget v2, p0, Lcom/google/maps/g/tp;->c:I

    .line 1402
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1404
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/tp;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1405
    iput v0, p0, Lcom/google/maps/g/tp;->f:I

    goto :goto_0

    .line 1402
    :cond_2
    const/16 v1, 0xa

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/tp;->newBuilder()Lcom/google/maps/g/tr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/tr;->a(Lcom/google/maps/g/tp;)Lcom/google/maps/g/tr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/tp;->newBuilder()Lcom/google/maps/g/tr;

    move-result-object v0

    return-object v0
.end method
