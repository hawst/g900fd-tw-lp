.class public final enum Lcom/google/maps/g/nf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/nf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/nf;

.field public static final enum b:Lcom/google/maps/g/nf;

.field public static final enum c:Lcom/google/maps/g/nf;

.field private static final synthetic e:[Lcom/google/maps/g/nf;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    new-instance v0, Lcom/google/maps/g/nf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    .line 139
    new-instance v0, Lcom/google/maps/g/nf;

    const-string v1, "ANY_TIME"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/nf;->b:Lcom/google/maps/g/nf;

    .line 143
    new-instance v0, Lcom/google/maps/g/nf;

    const-string v1, "AFTER_RATING_OR_REVIEW"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/nf;->c:Lcom/google/maps/g/nf;

    .line 130
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/nf;

    sget-object v1, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/nf;->b:Lcom/google/maps/g/nf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/nf;->c:Lcom/google/maps/g/nf;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/nf;->e:[Lcom/google/maps/g/nf;

    .line 178
    new-instance v0, Lcom/google/maps/g/ng;

    invoke-direct {v0}, Lcom/google/maps/g/ng;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 188
    iput p3, p0, Lcom/google/maps/g/nf;->d:I

    .line 189
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/nf;
    .locals 1

    .prologue
    .line 165
    packed-switch p0, :pswitch_data_0

    .line 169
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 166
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    goto :goto_0

    .line 167
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/nf;->b:Lcom/google/maps/g/nf;

    goto :goto_0

    .line 168
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/nf;->c:Lcom/google/maps/g/nf;

    goto :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/nf;
    .locals 1

    .prologue
    .line 130
    const-class v0, Lcom/google/maps/g/nf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/nf;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/nf;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/maps/g/nf;->e:[Lcom/google/maps/g/nf;

    invoke-virtual {v0}, [Lcom/google/maps/g/nf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/nf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/google/maps/g/nf;->d:I

    return v0
.end method
