.class public final Lcom/google/maps/g/rr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ru;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rr;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/maps/g/rr;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206
    new-instance v0, Lcom/google/maps/g/rs;

    invoke-direct {v0}, Lcom/google/maps/g/rs;-><init>()V

    sput-object v0, Lcom/google/maps/g/rr;->PARSER:Lcom/google/n/ax;

    .line 2250
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/rr;->d:Lcom/google/n/aw;

    .line 2361
    new-instance v0, Lcom/google/maps/g/rr;

    invoke-direct {v0}, Lcom/google/maps/g/rr;-><init>()V

    sput-object v0, Lcom/google/maps/g/rr;->a:Lcom/google/maps/g/rr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2170
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2221
    iput-byte v0, p0, Lcom/google/maps/g/rr;->b:B

    .line 2237
    iput v0, p0, Lcom/google/maps/g/rr;->c:I

    .line 2171
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2177
    invoke-direct {p0}, Lcom/google/maps/g/rr;-><init>()V

    .line 2179
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2181
    const/4 v0, 0x0

    .line 2182
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2183
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2184
    packed-switch v3, :pswitch_data_0

    .line 2189
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2191
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2187
    goto :goto_0

    .line 2203
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/rr;->au:Lcom/google/n/bn;

    .line 2204
    return-void

    .line 2197
    :catch_0
    move-exception v0

    .line 2198
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2203
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/rr;->au:Lcom/google/n/bn;

    throw v0

    .line 2199
    :catch_1
    move-exception v0

    .line 2200
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2201
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2184
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2168
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2221
    iput-byte v0, p0, Lcom/google/maps/g/rr;->b:B

    .line 2237
    iput v0, p0, Lcom/google/maps/g/rr;->c:I

    .line 2169
    return-void
.end method

.method public static a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;
    .locals 1

    .prologue
    .line 2315
    invoke-static {}, Lcom/google/maps/g/rr;->newBuilder()Lcom/google/maps/g/rt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/rt;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/rr;
    .locals 1

    .prologue
    .line 2364
    sget-object v0, Lcom/google/maps/g/rr;->a:Lcom/google/maps/g/rr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/rt;
    .locals 1

    .prologue
    .line 2312
    new-instance v0, Lcom/google/maps/g/rt;

    invoke-direct {v0}, Lcom/google/maps/g/rt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/rr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2218
    sget-object v0, Lcom/google/maps/g/rr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 2233
    iget v0, p0, Lcom/google/maps/g/rr;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2234
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/rr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2235
    return-void

    .line 2233
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/maps/g/rr;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2223
    iget-byte v1, p0, Lcom/google/maps/g/rr;->b:B

    .line 2224
    if-ne v1, v0, :cond_0

    .line 2228
    :goto_0
    return v0

    .line 2225
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2227
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/rr;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2239
    iget v0, p0, Lcom/google/maps/g/rr;->c:I

    .line 2240
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2245
    :goto_0
    return v0

    .line 2242
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/rr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2244
    iput v0, p0, Lcom/google/maps/g/rr;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2162
    invoke-static {}, Lcom/google/maps/g/rr;->newBuilder()Lcom/google/maps/g/rt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/rt;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2162
    invoke-static {}, Lcom/google/maps/g/rr;->newBuilder()Lcom/google/maps/g/rt;

    move-result-object v0

    return-object v0
.end method
