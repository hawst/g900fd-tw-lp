.class public final Lcom/google/maps/g/vz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/vx;",
        "Lcom/google/maps/g/vz;",
        ">;",
        "Lcom/google/maps/g/wa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 986
    sget-object v0, Lcom/google/maps/g/vx;->h:Lcom/google/maps/g/vx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1113
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/vz;->b:Ljava/lang/Object;

    .line 1190
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    .line 1327
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    .line 1464
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    .line 1601
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    .line 1738
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    .line 987
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/vx;)Lcom/google/maps/g/vz;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1047
    invoke-static {}, Lcom/google/maps/g/vx;->i()Lcom/google/maps/g/vx;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1104
    :goto_0
    return-object p0

    .line 1048
    :cond_0
    iget v1, p1, Lcom/google/maps/g/vx;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_7

    :goto_1
    if-eqz v0, :cond_1

    .line 1049
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1050
    iget-object v0, p1, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/vz;->b:Ljava/lang/Object;

    .line 1053
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1054
    iget-object v0, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1055
    iget-object v0, p1, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    .line 1056
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1063
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1064
    iget-object v0, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1065
    iget-object v0, p1, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    .line 1066
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1073
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1074
    iget-object v0, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1075
    iget-object v0, p1, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    .line 1076
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1083
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1084
    iget-object v0, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1085
    iget-object v0, p1, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    .line 1086
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1093
    :cond_5
    :goto_5
    iget-object v0, p1, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1094
    iget-object v0, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1095
    iget-object v0, p1, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    .line 1096
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1103
    :cond_6
    :goto_6
    iget-object v0, p1, Lcom/google/maps/g/vx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1048
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1058
    :cond_8
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1059
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 1068
    :cond_a
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1069
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 1078
    :cond_c
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1079
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 1088
    :cond_e
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_f

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1089
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    .line 1098
    :cond_10
    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/vz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/vz;->a:I

    .line 1099
    :cond_11
    iget-object v0, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 978
    new-instance v2, Lcom/google/maps/g/vx;

    invoke-direct {v2, p0}, Lcom/google/maps/g/vx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/vz;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/vz;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/vx;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/vz;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/vz;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vx;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/maps/g/vz;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/vz;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/vz;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/vz;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/maps/g/vz;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/vz;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vx;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/vz;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/maps/g/vz;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/vz;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/vx;->g:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/vx;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 978
    check-cast p1, Lcom/google/maps/g/vx;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/vz;->a(Lcom/google/maps/g/vx;)Lcom/google/maps/g/vz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1108
    const/4 v0, 0x1

    return v0
.end method
