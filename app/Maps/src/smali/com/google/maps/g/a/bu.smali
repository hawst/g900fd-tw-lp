.class public final Lcom/google/maps/g/a/bu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/a/bu;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field public g:Ljava/lang/Object;

.field public h:I

.field public i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/google/maps/g/a/bv;

    invoke-direct {v0}, Lcom/google/maps/g/a/bv;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bu;->PARSER:Lcom/google/n/ax;

    .line 438
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/bu;->m:Lcom/google/n/aw;

    .line 935
    new-instance v0, Lcom/google/maps/g/a/bu;

    invoke-direct {v0}, Lcom/google/maps/g/a/bu;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bu;->j:Lcom/google/maps/g/a/bu;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 349
    iput-byte v0, p0, Lcom/google/maps/g/a/bu;->k:B

    .line 393
    iput v0, p0, Lcom/google/maps/g/a/bu;->l:I

    .line 18
    iput v1, p0, Lcom/google/maps/g/a/bu;->b:I

    .line 19
    iput v1, p0, Lcom/google/maps/g/a/bu;->c:I

    .line 20
    iput v1, p0, Lcom/google/maps/g/a/bu;->d:I

    .line 21
    iput v1, p0, Lcom/google/maps/g/a/bu;->e:I

    .line 22
    iput-boolean v1, p0, Lcom/google/maps/g/a/bu;->f:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    .line 24
    iput v1, p0, Lcom/google/maps/g/a/bu;->h:I

    .line 25
    iput v1, p0, Lcom/google/maps/g/a/bu;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/a/bu;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 53
    invoke-static {v0}, Lcom/google/maps/g/a/bx;->a(I)Lcom/google/maps/g/a/bx;

    move-result-object v5

    .line 54
    if-nez v5, :cond_1

    .line 55
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bu;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 58
    iput v0, p0, Lcom/google/maps/g/a/bu;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    ushr-int/lit8 v5, v0, 0x1

    and-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    xor-int/2addr v0, v5

    iput v0, p0, Lcom/google/maps/g/a/bu;->c:I

    goto :goto_0

    .line 68
    :sswitch_3
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/bu;->d:I

    goto :goto_0

    .line 73
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/bu;->e:I

    goto :goto_0

    .line 78
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/bu;->f:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 83
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 84
    iget v5, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 85
    iput-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 89
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 90
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/bu;->h:I

    goto/16 :goto_0

    .line 94
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/bu;->a:I

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/bu;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 106
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bu;->au:Lcom/google/n/bn;

    .line 107
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 349
    iput-byte v0, p0, Lcom/google/maps/g/a/bu;->k:B

    .line 393
    iput v0, p0, Lcom/google/maps/g/a/bu;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/bu;
    .locals 1

    .prologue
    .line 938
    sget-object v0, Lcom/google/maps/g/a/bu;->j:Lcom/google/maps/g/a/bu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/bw;
    .locals 1

    .prologue
    .line 500
    new-instance v0, Lcom/google/maps/g/a/bw;

    invoke-direct {v0}, Lcom/google/maps/g/a/bw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lcom/google/maps/g/a/bu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 365
    invoke-virtual {p0}, Lcom/google/maps/g/a/bu;->c()I

    .line 366
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 367
    iget v2, p0, Lcom/google/maps/g/a/bu;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_8

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 369
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 370
    iget v2, p0, Lcom/google/maps/g/a/bu;->c:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    shl-int/lit8 v3, v2, 0x1

    shr-int/lit8 v2, v2, 0x1f

    xor-int/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 372
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 373
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/maps/g/a/bu;->d:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_9

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 375
    :cond_2
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 376
    iget v2, p0, Lcom/google/maps/g/a/bu;->e:I

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_a

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 378
    :cond_3
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 379
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/a/bu;->f:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_b

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 381
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 382
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 384
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 385
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/maps/g/a/bu;->h:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_d

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 387
    :cond_6
    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_7

    .line 388
    iget v0, p0, Lcom/google/maps/g/a/bu;->i:I

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 390
    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/bu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 391
    return-void

    .line 367
    :cond_8
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 373
    :cond_9
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 376
    :cond_a
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    :cond_b
    move v0, v1

    .line 379
    goto :goto_3

    .line 382
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 385
    :cond_d
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 388
    :cond_e
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 351
    iget-byte v2, p0, Lcom/google/maps/g/a/bu;->k:B

    .line 352
    if-ne v2, v0, :cond_0

    .line 360
    :goto_0
    return v0

    .line 353
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 355
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 356
    iput-byte v1, p0, Lcom/google/maps/g/a/bu;->k:B

    move v0, v1

    .line 357
    goto :goto_0

    :cond_2
    move v2, v1

    .line 355
    goto :goto_1

    .line 359
    :cond_3
    iput-byte v0, p0, Lcom/google/maps/g/a/bu;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 395
    iget v0, p0, Lcom/google/maps/g/a/bu;->l:I

    .line 396
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 433
    :goto_0
    return v0

    .line 399
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 400
    iget v0, p0, Lcom/google/maps/g/a/bu;->b:I

    .line 401
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 403
    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 404
    iget v3, p0, Lcom/google/maps/g/a/bu;->c:I

    .line 405
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    shl-int/lit8 v5, v3, 0x1

    shr-int/lit8 v3, v3, 0x1f

    xor-int/2addr v3, v5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 407
    :cond_1
    iget v3, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 408
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/g/a/bu;->d:I

    .line 409
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 411
    :cond_2
    iget v3, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 412
    iget v3, p0, Lcom/google/maps/g/a/bu;->e:I

    .line 413
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 415
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_d

    .line 416
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/maps/g/a/bu;->f:Z

    .line 417
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    move v3, v0

    .line 419
    :goto_5
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 420
    const/4 v4, 0x6

    .line 421
    iget-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 423
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 424
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/maps/g/a/bu;->h:I

    .line 425
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 427
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_7

    .line 428
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/maps/g/a/bu;->i:I

    .line 429
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 431
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/a/bu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 432
    iput v0, p0, Lcom/google/maps/g/a/bu;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 401
    goto/16 :goto_1

    :cond_9
    move v3, v1

    .line 409
    goto/16 :goto_3

    :cond_a
    move v3, v1

    .line 413
    goto/16 :goto_4

    .line 421
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_c
    move v0, v1

    .line 425
    goto :goto_7

    :cond_d
    move v3, v0

    goto/16 :goto_5

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bu;->newBuilder()Lcom/google/maps/g/a/bw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/bw;->a(Lcom/google/maps/g/a/bu;)Lcom/google/maps/g/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bu;->newBuilder()Lcom/google/maps/g/a/bw;

    move-result-object v0

    return-object v0
.end method
