.class public final Lcom/google/maps/g/a/gf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/gf;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/maps/g/a/gf;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/f;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/maps/g/a/gg;

    invoke-direct {v0}, Lcom/google/maps/g/a/gg;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/gf;->PARSER:Lcom/google/n/ax;

    .line 127
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/gf;->f:Lcom/google/n/aw;

    .line 286
    new-instance v0, Lcom/google/maps/g/a/gf;

    invoke-direct {v0}, Lcom/google/maps/g/a/gf;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/gf;->c:Lcom/google/maps/g/a/gf;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 91
    iput-byte v0, p0, Lcom/google/maps/g/a/gf;->d:B

    .line 110
    iput v0, p0, Lcom/google/maps/g/a/gf;->e:I

    .line 18
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/gf;->b:Lcom/google/n/f;

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/maps/g/a/gf;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 30
    const/4 v0, 0x0

    .line 31
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 33
    sparse-switch v3, :sswitch_data_0

    .line 38
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 40
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/a/gf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/gf;->a:I

    .line 46
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/g/a/gf;->b:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/gf;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gf;->au:Lcom/google/n/bn;

    .line 58
    return-void

    .line 53
    :catch_1
    move-exception v0

    .line 54
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 55
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 91
    iput-byte v0, p0, Lcom/google/maps/g/a/gf;->d:B

    .line 110
    iput v0, p0, Lcom/google/maps/g/a/gf;->e:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/gf;
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/google/maps/g/a/gf;->c:Lcom/google/maps/g/a/gf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/gh;
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/google/maps/g/a/gh;

    invoke-direct {v0}, Lcom/google/maps/g/a/gh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/gf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/google/maps/g/a/gf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 103
    invoke-virtual {p0}, Lcom/google/maps/g/a/gf;->c()I

    .line 104
    iget v0, p0, Lcom/google/maps/g/a/gf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/maps/g/a/gf;->b:Lcom/google/n/f;

    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/a/gf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 108
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 93
    iget-byte v1, p0, Lcom/google/maps/g/a/gf;->d:B

    .line 94
    if-ne v1, v0, :cond_0

    .line 98
    :goto_0
    return v0

    .line 95
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/gf;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 112
    iget v1, p0, Lcom/google/maps/g/a/gf;->e:I

    .line 113
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 116
    :cond_0
    iget v1, p0, Lcom/google/maps/g/a/gf;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/maps/g/a/gf;->b:Lcom/google/n/f;

    .line 118
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/gf;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    iput v0, p0, Lcom/google/maps/g/a/gf;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/gf;->newBuilder()Lcom/google/maps/g/a/gh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/gh;->a(Lcom/google/maps/g/a/gf;)Lcom/google/maps/g/a/gh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/gf;->newBuilder()Lcom/google/maps/g/a/gh;

    move-result-object v0

    return-object v0
.end method
