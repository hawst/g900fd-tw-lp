.class public final Lcom/google/maps/g/a/ij;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/im;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ij;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/a/ij;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/maps/g/a/be;

.field public c:Lcom/google/maps/g/a/cw;

.field public d:Ljava/lang/Object;

.field public e:Z

.field f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1441
    new-instance v0, Lcom/google/maps/g/a/ik;

    invoke-direct {v0}, Lcom/google/maps/g/a/ik;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ij;->PARSER:Lcom/google/n/ax;

    .line 1650
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ij;->j:Lcom/google/n/aw;

    .line 2120
    new-instance v0, Lcom/google/maps/g/a/ij;

    invoke-direct {v0}, Lcom/google/maps/g/a/ij;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ij;->g:Lcom/google/maps/g/a/ij;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1358
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1586
    iput-byte v0, p0, Lcom/google/maps/g/a/ij;->h:B

    .line 1617
    iput v0, p0, Lcom/google/maps/g/a/ij;->i:I

    .line 1359
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    .line 1360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/a/ij;->e:Z

    .line 1361
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    .line 1362
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1368
    invoke-direct {p0}, Lcom/google/maps/g/a/ij;-><init>()V

    .line 1369
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v5, v2

    .line 1374
    :cond_0
    :goto_0
    if-nez v5, :cond_4

    .line 1375
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1376
    sparse-switch v0, :sswitch_data_0

    .line 1381
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v5, v1

    .line 1383
    goto :goto_0

    :sswitch_0
    move v5, v1

    .line 1379
    goto :goto_0

    .line 1389
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    .line 1390
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->newBuilder()Lcom/google/maps/g/a/bg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    move-object v3, v0

    .line 1392
    :goto_1
    sget-object v0, Lcom/google/maps/g/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    .line 1393
    if-eqz v3, :cond_1

    .line 1394
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    .line 1395
    invoke-virtual {v3}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    .line 1397
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/ij;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1432
    :catch_0
    move-exception v0

    .line 1433
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1438
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ij;->au:Lcom/google/n/bn;

    throw v0

    .line 1402
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    .line 1403
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    invoke-static {}, Lcom/google/maps/g/a/cw;->newBuilder()Lcom/google/maps/g/a/cy;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    move-result-object v0

    move-object v3, v0

    .line 1405
    :goto_2
    sget-object v0, Lcom/google/maps/g/a/cw;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cw;

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    .line 1406
    if-eqz v3, :cond_2

    .line 1407
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/cy;->a(Lcom/google/maps/g/a/cw;)Lcom/google/maps/g/a/cy;

    .line 1408
    invoke-virtual {v3}, Lcom/google/maps/g/a/cy;->c()Lcom/google/maps/g/a/cw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    .line 1410
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/ij;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1434
    :catch_1
    move-exception v0

    .line 1435
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1436
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1414
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1415
    iget v3, p0, Lcom/google/maps/g/a/ij;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/ij;->a:I

    .line 1416
    iput-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 1420
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/ij;->a:I

    .line 1421
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/a/ij;->e:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 1425
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1426
    iget v3, p0, Lcom/google/maps/g/a/ij;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/ij;->a:I

    .line 1427
    iput-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1438
    :cond_4
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->au:Lcom/google/n/bn;

    .line 1439
    return-void

    :cond_5
    move-object v3, v4

    goto :goto_2

    :cond_6
    move-object v3, v4

    goto/16 :goto_1

    .line 1376
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1356
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1586
    iput-byte v0, p0, Lcom/google/maps/g/a/ij;->h:B

    .line 1617
    iput v0, p0, Lcom/google/maps/g/a/ij;->i:I

    .line 1357
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;
    .locals 1

    .prologue
    .line 1715
    invoke-static {}, Lcom/google/maps/g/a/ij;->newBuilder()Lcom/google/maps/g/a/il;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/il;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/maps/g/a/ij;
    .locals 1

    .prologue
    .line 2123
    sget-object v0, Lcom/google/maps/g/a/ij;->g:Lcom/google/maps/g/a/ij;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/il;
    .locals 1

    .prologue
    .line 1712
    new-instance v0, Lcom/google/maps/g/a/il;

    invoke-direct {v0}, Lcom/google/maps/g/a/il;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ij;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1453
    sget-object v0, Lcom/google/maps/g/a/ij;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x2

    .line 1598
    invoke-virtual {p0}, Lcom/google/maps/g/a/ij;->c()I

    .line 1599
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1600
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_0
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1602
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1603
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v0

    :goto_1
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1605
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 1606
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1608
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1609
    iget-boolean v0, p0, Lcom/google/maps/g/a/ij;->e:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1611
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1612
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1614
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1615
    return-void

    .line 1600
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    goto/16 :goto_0

    .line 1603
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    goto/16 :goto_1

    .line 1606
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_8
    move v0, v2

    .line 1609
    goto :goto_3

    .line 1612
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1588
    iget-byte v1, p0, Lcom/google/maps/g/a/ij;->h:B

    .line 1589
    if-ne v1, v0, :cond_0

    .line 1593
    :goto_0
    return v0

    .line 1590
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1592
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/ij;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1619
    iget v0, p0, Lcom/google/maps/g/a/ij;->i:I

    .line 1620
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1645
    :goto_0
    return v0

    .line 1623
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 1625
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1627
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    .line 1629
    iget-object v2, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    if-nez v2, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1631
    :goto_4
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 1632
    const/4 v3, 0x3

    .line 1633
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1635
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 1636
    iget-boolean v0, p0, Lcom/google/maps/g/a/ij;->e:Z

    .line 1637
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 1639
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ij;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 1640
    const/4 v3, 0x5

    .line 1641
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1643
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1644
    iput v0, p0, Lcom/google/maps/g/a/ij;->i:I

    goto/16 :goto_0

    .line 1625
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    goto/16 :goto_1

    .line 1629
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    goto/16 :goto_3

    .line 1633
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 1641
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_8
    move v2, v0

    goto/16 :goto_4

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1556
    iget-object v0, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    .line 1557
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1558
    check-cast v0, Ljava/lang/String;

    .line 1566
    :goto_0
    return-object v0

    .line 1560
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1562
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1563
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1564
    iput-object v1, p0, Lcom/google/maps/g/a/ij;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1566
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1350
    invoke-static {}, Lcom/google/maps/g/a/ij;->newBuilder()Lcom/google/maps/g/a/il;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/il;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1350
    invoke-static {}, Lcom/google/maps/g/a/ij;->newBuilder()Lcom/google/maps/g/a/il;

    move-result-object v0

    return-object v0
.end method
