.class public final Lcom/google/maps/g/co;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/cr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/co;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/co;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field e:I

.field f:I

.field g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/maps/g/cp;

    invoke-direct {v0}, Lcom/google/maps/g/cp;-><init>()V

    sput-object v0, Lcom/google/maps/g/co;->PARSER:Lcom/google/n/ax;

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/co;->k:Lcom/google/n/aw;

    .line 628
    new-instance v0, Lcom/google/maps/g/co;

    invoke-direct {v0}, Lcom/google/maps/g/co;-><init>()V

    sput-object v0, Lcom/google/maps/g/co;->h:Lcom/google/maps/g/co;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 196
    iput-byte v1, p0, Lcom/google/maps/g/co;->i:B

    .line 230
    iput v1, p0, Lcom/google/maps/g/co;->j:I

    .line 18
    iput v0, p0, Lcom/google/maps/g/co;->b:I

    .line 19
    iput v0, p0, Lcom/google/maps/g/co;->c:I

    .line 20
    iput v0, p0, Lcom/google/maps/g/co;->d:I

    .line 21
    iput v0, p0, Lcom/google/maps/g/co;->e:I

    .line 22
    iput v0, p0, Lcom/google/maps/g/co;->f:I

    .line 23
    iput v0, p0, Lcom/google/maps/g/co;->g:I

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/co;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 38
    sparse-switch v3, :sswitch_data_0

    .line 43
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 51
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/co;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 56
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 61
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->d:I

    goto :goto_0

    .line 65
    :sswitch_4
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->e:I

    goto :goto_0

    .line 70
    :sswitch_5
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->f:I

    goto :goto_0

    .line 75
    :sswitch_6
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/co;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/co;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/co;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 196
    iput-byte v0, p0, Lcom/google/maps/g/co;->i:B

    .line 230
    iput v0, p0, Lcom/google/maps/g/co;->j:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;
    .locals 1

    .prologue
    .line 332
    invoke-static {}, Lcom/google/maps/g/co;->newBuilder()Lcom/google/maps/g/cq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/co;
    .locals 1

    .prologue
    .line 631
    sget-object v0, Lcom/google/maps/g/co;->h:Lcom/google/maps/g/co;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/cq;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/google/maps/g/cq;

    invoke-direct {v0}, Lcom/google/maps/g/cq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/co;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/co;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 208
    invoke-virtual {p0}, Lcom/google/maps/g/co;->c()I

    .line 209
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 210
    iget v0, p0, Lcom/google/maps/g/co;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 212
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 213
    iget v0, p0, Lcom/google/maps/g/co;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 215
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 216
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/co;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 218
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 219
    iget v0, p0, Lcom/google/maps/g/co;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 221
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 222
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/co;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 224
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 225
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/g/co;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 227
    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/co;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 228
    return-void

    .line 210
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 213
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 216
    :cond_8
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 219
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 222
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 225
    :cond_b
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 198
    iget-byte v1, p0, Lcom/google/maps/g/co;->i:B

    .line 199
    if-ne v1, v0, :cond_0

    .line 203
    :goto_0
    return v0

    .line 200
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/co;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 232
    iget v0, p0, Lcom/google/maps/g/co;->j:I

    .line 233
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 262
    :goto_0
    return v0

    .line 236
    :cond_0
    iget v0, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 237
    iget v0, p0, Lcom/google/maps/g/co;->b:I

    .line 238
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 240
    :goto_2
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 241
    iget v3, p0, Lcom/google/maps/g/co;->c:I

    .line 242
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 244
    :cond_1
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 245
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/g/co;->d:I

    .line 246
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 248
    :cond_2
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 249
    iget v3, p0, Lcom/google/maps/g/co;->e:I

    .line 250
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 252
    :cond_3
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 253
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/maps/g/co;->f:I

    .line 254
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 256
    :cond_4
    iget v3, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_6

    .line 257
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/maps/g/co;->g:I

    .line 258
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 260
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/co;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    iput v0, p0, Lcom/google/maps/g/co;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 238
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 242
    goto :goto_3

    :cond_9
    move v3, v1

    .line 246
    goto :goto_4

    :cond_a
    move v3, v1

    .line 250
    goto :goto_5

    :cond_b
    move v3, v1

    .line 254
    goto :goto_6

    :cond_c
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/co;->newBuilder()Lcom/google/maps/g/cq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/co;->newBuilder()Lcom/google/maps/g/cq;

    move-result-object v0

    return-object v0
.end method
