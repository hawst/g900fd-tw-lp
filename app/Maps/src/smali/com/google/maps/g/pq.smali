.class public final Lcom/google/maps/g/pq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/po;",
        "Lcom/google/maps/g/pq;",
        ">;",
        "Lcom/google/maps/g/pt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/google/maps/g/po;->f:Lcom/google/maps/g/po;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 453
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/pq;->b:Ljava/lang/Object;

    .line 529
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/pq;->c:I

    .line 385
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/po;)Lcom/google/maps/g/pq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 428
    invoke-static {}, Lcom/google/maps/g/po;->d()Lcom/google/maps/g/po;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 444
    :goto_0
    return-object p0

    .line 429
    :cond_0
    iget v2, p1, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 430
    iget v2, p0, Lcom/google/maps/g/pq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/pq;->a:I

    .line 431
    iget-object v2, p1, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/pq;->b:Ljava/lang/Object;

    .line 434
    :cond_1
    iget v2, p1, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 435
    iget v2, p1, Lcom/google/maps/g/po;->c:I

    invoke-static {v2}, Lcom/google/maps/g/pr;->a(I)Lcom/google/maps/g/pr;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/pr;->a:Lcom/google/maps/g/pr;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 429
    goto :goto_1

    :cond_4
    move v2, v1

    .line 434
    goto :goto_2

    .line 435
    :cond_5
    iget v3, p0, Lcom/google/maps/g/pq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/pq;->a:I

    iget v2, v2, Lcom/google/maps/g/pr;->c:I

    iput v2, p0, Lcom/google/maps/g/pq;->c:I

    .line 437
    :cond_6
    iget v2, p1, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 438
    iget-boolean v2, p1, Lcom/google/maps/g/po;->d:Z

    iget v3, p0, Lcom/google/maps/g/pq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/pq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/pq;->d:Z

    .line 440
    :cond_7
    iget v2, p1, Lcom/google/maps/g/po;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_8

    .line 441
    iget-boolean v0, p1, Lcom/google/maps/g/po;->e:Z

    iget v1, p0, Lcom/google/maps/g/pq;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/pq;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/pq;->e:Z

    .line 443
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/po;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_9
    move v2, v1

    .line 437
    goto :goto_3

    :cond_a
    move v0, v1

    .line 440
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 376
    new-instance v2, Lcom/google/maps/g/po;

    invoke-direct {v2, p0}, Lcom/google/maps/g/po;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/pq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/pq;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/pq;->c:I

    iput v1, v2, Lcom/google/maps/g/po;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/maps/g/pq;->d:Z

    iput-boolean v1, v2, Lcom/google/maps/g/po;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/maps/g/pq;->e:Z

    iput-boolean v1, v2, Lcom/google/maps/g/po;->e:Z

    iput v0, v2, Lcom/google/maps/g/po;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 376
    check-cast p1, Lcom/google/maps/g/po;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/pq;->a(Lcom/google/maps/g/po;)Lcom/google/maps/g/pq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x1

    return v0
.end method
