.class public final Lcom/google/maps/g/pf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/pf;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/pf;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Z

.field public g:Lcom/google/n/f;

.field public h:Z

.field public i:Z

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/google/maps/g/pg;

    invoke-direct {v0}, Lcom/google/maps/g/pg;-><init>()V

    sput-object v0, Lcom/google/maps/g/pf;->PARSER:Lcom/google/n/ax;

    .line 461
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/pf;->n:Lcom/google/n/aw;

    .line 1347
    new-instance v0, Lcom/google/maps/g/pf;

    invoke-direct {v0}, Lcom/google/maps/g/pf;-><init>()V

    sput-object v0, Lcom/google/maps/g/pf;->k:Lcom/google/maps/g/pf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 235
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    .line 251
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    .line 369
    iput-byte v3, p0, Lcom/google/maps/g/pf;->l:B

    .line 412
    iput v3, p0, Lcom/google/maps/g/pf;->m:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/maps/g/pf;->f:Z

    .line 23
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/g/pf;->h:Z

    .line 25
    iput-boolean v2, p0, Lcom/google/maps/g/pf;->i:Z

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x100

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 33
    invoke-direct {p0}, Lcom/google/maps/g/pf;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 39
    :cond_0
    :goto_0
    if-nez v4, :cond_a

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 46
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 48
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_1

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    .line 56
    or-int/lit8 v1, v1, 0x1

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 58
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    and-int/lit8 v3, v1, 0x1

    if-ne v3, v2, :cond_2

    .line 121
    iget-object v2, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    .line 123
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_3

    .line 124
    iget-object v2, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    .line 126
    :cond_3
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v9, :cond_4

    .line 127
    iget-object v1, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    .line 129
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/pf;->au:Lcom/google/n/bn;

    throw v0

    .line 63
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v8, :cond_5

    .line 64
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    .line 66
    or-int/lit8 v1, v1, 0x2

    .line 68
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 68
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 118
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 74
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    goto/16 :goto_0

    .line 78
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    goto/16 :goto_0

    .line 83
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_6

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/pf;->f:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_1

    .line 88
    :sswitch_6
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v9, :cond_7

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    .line 91
    or-int/lit16 v1, v1, 0x100

    .line 93
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 93
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 98
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    goto/16 :goto_0

    .line 103
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_8

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/pf;->h:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_2

    .line 108
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/pf;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_9

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/pf;->i:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_3

    .line 120
    :cond_a
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_b

    .line 121
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    .line 123
    :cond_b
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_c

    .line 124
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    .line 126
    :cond_c
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v9, :cond_d

    .line 127
    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    .line 129
    :cond_d
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/pf;->au:Lcom/google/n/bn;

    .line 130
    return-void

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 235
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    .line 251
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    .line 369
    iput-byte v1, p0, Lcom/google/maps/g/pf;->l:B

    .line 412
    iput v1, p0, Lcom/google/maps/g/pf;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/pf;
    .locals 1

    .prologue
    .line 1350
    sget-object v0, Lcom/google/maps/g/pf;->k:Lcom/google/maps/g/pf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ph;
    .locals 1

    .prologue
    .line 523
    new-instance v0, Lcom/google/maps/g/ph;

    invoke-direct {v0}, Lcom/google/maps/g/ph;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/pf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/google/maps/g/pf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 381
    invoke-virtual {p0}, Lcom/google/maps/g/pf;->c()I

    move v1, v2

    .line 382
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 385
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 388
    :cond_1
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 389
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 391
    :cond_2
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 392
    iget-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 394
    :cond_3
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 395
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/maps/g/pf;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_5

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_4
    move v1, v2

    .line 397
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 398
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 397
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v0, v2

    .line 395
    goto :goto_2

    .line 400
    :cond_6
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_7

    .line 401
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 403
    :cond_7
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 404
    iget-boolean v0, p0, Lcom/google/maps/g/pf;->h:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_a

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 406
    :cond_8
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 407
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/g/pf;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_b

    :goto_5
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 409
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/pf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 410
    return-void

    :cond_a
    move v0, v2

    .line 404
    goto :goto_4

    :cond_b
    move v3, v2

    .line 407
    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 371
    iget-byte v1, p0, Lcom/google/maps/g/pf;->l:B

    .line 372
    if-ne v1, v0, :cond_0

    .line 376
    :goto_0
    return v0

    .line 373
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 375
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/pf;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 414
    iget v0, p0, Lcom/google/maps/g/pf;->m:I

    .line 415
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 456
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 418
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    .line 420
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 418
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 422
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 423
    iget-object v0, p0, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    .line 424
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 422
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 426
    :cond_2
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 427
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    .line 428
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 430
    :cond_3
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 431
    iget-object v0, p0, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    .line 432
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 434
    :cond_4
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_5

    .line 435
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/maps/g/pf;->f:Z

    .line 436
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_5
    move v1, v2

    .line 438
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 439
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    .line 440
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 438
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 442
    :cond_6
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_7

    .line 443
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    .line 444
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 446
    :cond_7
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 447
    iget-boolean v0, p0, Lcom/google/maps/g/pf;->h:Z

    .line 448
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 450
    :cond_8
    iget v0, p0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 451
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/g/pf;->i:Z

    .line 452
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 454
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/pf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 455
    iput v0, p0, Lcom/google/maps/g/pf;->m:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/pf;->newBuilder()Lcom/google/maps/g/ph;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ph;->a(Lcom/google/maps/g/pf;)Lcom/google/maps/g/ph;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/pf;->newBuilder()Lcom/google/maps/g/ph;

    move-result-object v0

    return-object v0
.end method
