.class public final Lcom/google/maps/g/a/gq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/go;",
        "Lcom/google/maps/g/a/gq;",
        ">;",
        "Lcom/google/maps/g/a/gr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 419
    sget-object v0, Lcom/google/maps/g/a/go;->f:Lcom/google/maps/g/a/go;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 507
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    .line 644
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    .line 780
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gq;->d:Ljava/lang/Object;

    .line 856
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gq;->e:Ljava/lang/Object;

    .line 420
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/go;)Lcom/google/maps/g/a/gq;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 465
    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 497
    :goto_0
    return-object p0

    .line 466
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 467
    iget-object v2, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 468
    iget-object v2, p1, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    .line 469
    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 476
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/maps/g/a/go;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 477
    iget-object v2, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 478
    iget-object v2, p1, Lcom/google/maps/g/a/go;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    .line 479
    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 486
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/a/go;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 487
    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 488
    iget-object v2, p1, Lcom/google/maps/g/a/go;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gq;->d:Ljava/lang/Object;

    .line 491
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/go;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    :goto_4
    if-eqz v0, :cond_4

    .line 492
    iget v0, p0, Lcom/google/maps/g/a/gq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 493
    iget-object v0, p1, Lcom/google/maps/g/a/go;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/gq;->e:Ljava/lang/Object;

    .line 496
    :cond_4
    iget-object v0, p1, Lcom/google/maps/g/a/go;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 471
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 472
    :cond_6
    iget-object v2, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 481
    :cond_7
    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/gq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/gq;->a:I

    .line 482
    :cond_8
    iget-object v2, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/go;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 486
    goto :goto_3

    :cond_a
    move v0, v1

    .line 491
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 411
    new-instance v2, Lcom/google/maps/g/a/go;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/go;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/gq;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/a/gq;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/gq;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/gq;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/a/gq;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/a/gq;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/go;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/a/gq;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/go;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/a/gq;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/go;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/a/go;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 411
    check-cast p1, Lcom/google/maps/g/a/go;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/gq;->a(Lcom/google/maps/g/a/go;)Lcom/google/maps/g/a/gq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x1

    return v0
.end method
