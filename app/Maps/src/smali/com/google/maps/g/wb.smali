.class public final Lcom/google/maps/g/wb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/wb;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/maps/g/wb;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/ao;

.field g:Z

.field h:Z

.field i:Ljava/lang/Object;

.field j:Z

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:Z

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/maps/g/wc;

    invoke-direct {v0}, Lcom/google/maps/g/wc;-><init>()V

    sput-object v0, Lcom/google/maps/g/wb;->PARSER:Lcom/google/n/ax;

    .line 664
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/wb;->q:Lcom/google/n/aw;

    .line 1891
    new-instance v0, Lcom/google/maps/g/wb;

    invoke-direct {v0}, Lcom/google/maps/g/wb;-><init>()V

    sput-object v0, Lcom/google/maps/g/wb;->n:Lcom/google/maps/g/wb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 348
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    .line 551
    iput-byte v3, p0, Lcom/google/maps/g/wb;->o:B

    .line 603
    iput v3, p0, Lcom/google/maps/g/wb;->p:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput-boolean v2, p0, Lcom/google/maps/g/wb;->g:Z

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/g/wb;->h:Z

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    .line 26
    iput-boolean v2, p0, Lcom/google/maps/g/wb;->j:Z

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 28
    iput-boolean v2, p0, Lcom/google/maps/g/wb;->l:Z

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x4

    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/maps/g/wb;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 42
    :cond_0
    :goto_0
    if-nez v4, :cond_a

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 44
    sparse-switch v1, :sswitch_data_0

    .line 49
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 51
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 47
    goto :goto_0

    .line 56
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 57
    iget v6, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/wb;->a:I

    .line 58
    iput-object v1, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 140
    :catch_0
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 141
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v10, :cond_1

    .line 147
    iget-object v2, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 149
    :cond_1
    and-int/lit16 v2, v1, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2

    .line 150
    iget-object v2, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 152
    :cond_2
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v11, :cond_3

    .line 153
    iget-object v2, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    .line 155
    :cond_3
    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_4

    .line 156
    iget-object v1, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 158
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wb;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :sswitch_2
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v10, :cond_11

    .line 63
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 65
    or-int/lit8 v1, v0, 0x4

    .line 67
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 67
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 69
    goto :goto_0

    .line 72
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/wb;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_5

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/google/maps/g/wb;->g:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 142
    :catch_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    .line 143
    :goto_5
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 144
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_5
    move v1, v3

    .line 73
    goto :goto_4

    .line 77
    :sswitch_4
    :try_start_6
    iget v1, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/g/wb;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    move v1, v2

    :goto_6
    iput-boolean v1, p0, Lcom/google/maps/g/wb;->h:Z

    goto/16 :goto_0

    .line 146
    :catchall_1
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    goto/16 :goto_2

    :cond_6
    move v1, v3

    .line 78
    goto :goto_6

    .line 82
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 83
    iget v6, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/maps/g/wb;->a:I

    .line 84
    iput-object v1, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 88
    :sswitch_6
    iget v1, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/maps/g/wb;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_7

    move v1, v2

    :goto_7
    iput-boolean v1, p0, Lcom/google/maps/g/wb;->j:Z

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto :goto_7

    .line 93
    :sswitch_7
    and-int/lit16 v1, v0, 0x200

    const/16 v6, 0x200

    if-eq v1, v6, :cond_10

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 96
    or-int/lit16 v1, v0, 0x200

    .line 98
    :goto_8
    :try_start_7
    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 98
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 100
    goto/16 :goto_0

    .line 103
    :sswitch_8
    :try_start_8
    iget v1, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/wb;->a:I

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_8

    move v1, v2

    :goto_9
    iput-boolean v1, p0, Lcom/google/maps/g/wb;->l:Z

    goto/16 :goto_0

    :cond_8
    move v1, v3

    goto :goto_9

    .line 108
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 109
    iget v6, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/wb;->a:I

    .line 110
    iput-object v1, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 114
    :sswitch_a
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v11, :cond_f

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 117
    or-int/lit8 v1, v0, 0x8

    .line 119
    :goto_a
    :try_start_9
    iget-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 119
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 121
    goto/16 :goto_0

    .line 124
    :sswitch_b
    :try_start_a
    iget-object v1, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 125
    iget v1, p0, Lcom/google/maps/g/wb;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/wb;->a:I

    goto/16 :goto_0

    .line 129
    :sswitch_c
    and-int/lit16 v1, v0, 0x800

    const/16 v6, 0x800

    if-eq v1, v6, :cond_9

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 132
    or-int/lit16 v0, v0, 0x800

    .line 134
    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 134
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 146
    :cond_a
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v10, :cond_b

    .line 147
    iget-object v1, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 149
    :cond_b
    and-int/lit16 v1, v0, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_c

    .line 150
    iget-object v1, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 152
    :cond_c
    and-int/lit8 v1, v0, 0x8

    if-ne v1, v11, :cond_d

    .line 153
    iget-object v1, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    .line 155
    :cond_d
    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_e

    .line 156
    iget-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 158
    :cond_e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->au:Lcom/google/n/bn;

    .line 159
    return-void

    .line 142
    :catch_2
    move-exception v0

    goto/16 :goto_5

    .line 140
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_f
    move v1, v0

    goto/16 :goto_a

    :cond_10
    move v1, v0

    goto/16 :goto_8

    :cond_11
    move v1, v0

    goto/16 :goto_3

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 348
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    .line 551
    iput-byte v1, p0, Lcom/google/maps/g/wb;->o:B

    .line 603
    iput v1, p0, Lcom/google/maps/g/wb;->p:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;
    .locals 1

    .prologue
    .line 729
    invoke-static {}, Lcom/google/maps/g/wb;->newBuilder()Lcom/google/maps/g/wd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v0

    return-object v0
.end method

.method public static j()Lcom/google/maps/g/wb;
    .locals 1

    .prologue
    .line 1894
    sget-object v0, Lcom/google/maps/g/wb;->n:Lcom/google/maps/g/wb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/wd;
    .locals 1

    .prologue
    .line 726
    new-instance v0, Lcom/google/maps/g/wd;

    invoke-direct {v0}, Lcom/google/maps/g/wd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/wb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lcom/google/maps/g/wb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 563
    invoke-virtual {p0}, Lcom/google/maps/g/wb;->c()I

    .line 564
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 565
    iget-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 567
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 568
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 567
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 565
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 570
    :cond_2
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 571
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/maps/g/wb;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_7

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 573
    :cond_3
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 574
    iget-boolean v0, p0, Lcom/google/maps/g/wb;->h:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_8

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 576
    :cond_4
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 577
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 579
    :cond_5
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 580
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/maps/g/wb;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_a

    move v0, v3

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_6
    move v1, v2

    .line 582
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 583
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 582
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_7
    move v0, v2

    .line 571
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 574
    goto :goto_3

    .line 577
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_a
    move v0, v2

    .line 580
    goto :goto_5

    .line 585
    :cond_b
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 586
    iget-boolean v0, p0, Lcom/google/maps/g/wb;->l:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_e

    :goto_7
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 588
    :cond_c
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_d

    .line 589
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_d
    move v1, v2

    .line 591
    :goto_9
    iget-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 592
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 591
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_e
    move v3, v2

    .line 586
    goto :goto_7

    .line 589
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 594
    :cond_10
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_11

    .line 595
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 597
    :cond_11
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_12

    .line 598
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 597
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 600
    :cond_12
    iget-object v0, p0, Lcom/google/maps/g/wb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 601
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 553
    iget-byte v1, p0, Lcom/google/maps/g/wb;->o:B

    .line 554
    if-ne v1, v0, :cond_0

    .line 558
    :goto_0
    return v0

    .line 555
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 557
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/wb;->o:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 605
    iget v0, p0, Lcom/google/maps/g/wb;->p:I

    .line 606
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 659
    :goto_0
    return v0

    .line 609
    :cond_0
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 611
    iget-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 613
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 614
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 615
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 613
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 611
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 617
    :cond_2
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 618
    const/4 v0, 0x3

    iget-boolean v2, p0, Lcom/google/maps/g/wb;->g:Z

    .line 619
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 621
    :cond_3
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 622
    iget-boolean v0, p0, Lcom/google/maps/g/wb;->h:Z

    .line 623
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 625
    :cond_4
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 626
    const/4 v2, 0x5

    .line 627
    iget-object v0, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 629
    :cond_5
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 630
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/maps/g/wb;->j:Z

    .line 631
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_6
    move v2, v1

    .line 633
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 634
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 635
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 633
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 627
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 637
    :cond_8
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 638
    iget-boolean v0, p0, Lcom/google/maps/g/wb;->l:Z

    .line 639
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 641
    :cond_9
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_a

    .line 642
    const/16 v2, 0x9

    .line 643
    iget-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_a
    move v2, v1

    .line 645
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 646
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    .line 647
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 645
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 643
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 649
    :cond_c
    iget v0, p0, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_d

    .line 650
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    .line 651
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_d
    move v2, v1

    .line 653
    :goto_8
    iget-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 654
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 655
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 653
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 657
    :cond_e
    iget-object v0, p0, Lcom/google/maps/g/wb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 658
    iput v0, p0, Lcom/google/maps/g/wb;->p:I

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    .line 190
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 191
    check-cast v0, Ljava/lang/String;

    .line 199
    :goto_0
    return-object v0

    .line 193
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 195
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iput-object v1, p0, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 199
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/wb;->newBuilder()Lcom/google/maps/g/wd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/wb;->newBuilder()Lcom/google/maps/g/wd;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    .line 232
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 233
    check-cast v0, Ljava/lang/String;

    .line 241
    :goto_0
    return-object v0

    .line 235
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 237
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    iput-object v1, p0, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 241
    goto :goto_0
.end method

.method public final h()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/vp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 269
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 270
    iget-object v0, p0, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 271
    invoke-static {}, Lcom/google/maps/g/vp;->g()Lcom/google/maps/g/vp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vp;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 273
    :cond_0
    return-object v1
.end method

.method public final i()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 457
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 458
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 459
    iget-object v0, p0, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 460
    invoke-static {}, Lcom/google/maps/g/a/gk;->g()Lcom/google/maps/g/a/gk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gk;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 462
    :cond_0
    return-object v1
.end method
