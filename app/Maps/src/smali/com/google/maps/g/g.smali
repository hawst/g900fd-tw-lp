.class public final Lcom/google/maps/g/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/e;",
        "Lcom/google/maps/g/g;",
        ">;",
        "Lcom/google/maps/g/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/n/aq;

.field private j:Ljava/lang/Object;

.field private k:Lcom/google/n/ao;

.field private l:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 717
    sget-object v0, Lcom/google/maps/g/e;->m:Lcom/google/maps/g/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 881
    const/16 v0, 0x1c86

    iput v0, p0, Lcom/google/maps/g/g;->b:I

    .line 913
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->c:Ljava/lang/Object;

    .line 989
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g;->d:Lcom/google/n/ao;

    .line 1048
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->e:Ljava/lang/Object;

    .line 1124
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->f:Ljava/lang/Object;

    .line 1200
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->g:Ljava/lang/Object;

    .line 1276
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->h:Ljava/lang/Object;

    .line 1352
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    .line 1445
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->j:Ljava/lang/Object;

    .line 1521
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g;->k:Lcom/google/n/ao;

    .line 1580
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g;->l:Ljava/lang/Object;

    .line 718
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/e;)Lcom/google/maps/g/g;
    .locals 5

    .prologue
    const/16 v4, 0x80

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 808
    invoke-static {}, Lcom/google/maps/g/e;->d()Lcom/google/maps/g/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 866
    :goto_0
    return-object p0

    .line 809
    :cond_0
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 810
    iget v2, p1, Lcom/google/maps/g/e;->b:I

    iget v3, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/g;->a:I

    iput v2, p0, Lcom/google/maps/g/g;->b:I

    .line 812
    :cond_1
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 813
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 814
    iget-object v2, p1, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->c:Ljava/lang/Object;

    .line 817
    :cond_2
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 818
    iget-object v2, p0, Lcom/google/maps/g/g;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 819
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 821
    :cond_3
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 822
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 823
    iget-object v2, p1, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->e:Ljava/lang/Object;

    .line 826
    :cond_4
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 827
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 828
    iget-object v2, p1, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->f:Ljava/lang/Object;

    .line 831
    :cond_5
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 832
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 833
    iget-object v2, p1, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->g:Ljava/lang/Object;

    .line 836
    :cond_6
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 837
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 838
    iget-object v2, p1, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->h:Ljava/lang/Object;

    .line 841
    :cond_7
    iget-object v2, p1, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 842
    iget-object v2, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 843
    iget-object v2, p1, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    .line 844
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 851
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 852
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 853
    iget-object v2, p1, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/g;->j:Ljava/lang/Object;

    .line 856
    :cond_9
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 857
    iget-object v2, p0, Lcom/google/maps/g/g;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 858
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 860
    :cond_a
    iget v2, p1, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    :goto_b
    if-eqz v0, :cond_b

    .line 861
    iget v0, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/g;->a:I

    .line 862
    iget-object v0, p1, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/g;->l:Ljava/lang/Object;

    .line 865
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 809
    goto/16 :goto_1

    :cond_d
    move v2, v1

    .line 812
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 817
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 821
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 826
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 831
    goto/16 :goto_6

    :cond_12
    move v2, v1

    .line 836
    goto/16 :goto_7

    .line 846
    :cond_13
    iget v2, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v4, :cond_14

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/g;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/g;->a:I

    .line 847
    :cond_14
    iget-object v2, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_15
    move v2, v1

    .line 851
    goto :goto_9

    :cond_16
    move v2, v1

    .line 856
    goto :goto_a

    :cond_17
    move v0, v1

    .line 860
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 709
    new-instance v2, Lcom/google/maps/g/e;

    invoke-direct {v2, p0}, Lcom/google/maps/g/e;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget v4, p0, Lcom/google/maps/g/g;->b:I

    iput v4, v2, Lcom/google/maps/g/e;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/g;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/g;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/g;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/g;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/g;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/g;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/g;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/maps/g/g;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/g;->i:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, p0, Lcom/google/maps/g/g;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/g;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/g;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v1, p0, Lcom/google/maps/g/g;->l:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/e;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 709
    check-cast p1, Lcom/google/maps/g/e;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/g;->a(Lcom/google/maps/g/e;)Lcom/google/maps/g/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 870
    iget v0, p0, Lcom/google/maps/g/g;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lcom/google/maps/g/g;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/kq;

    invoke-virtual {v0}, Lcom/google/maps/g/kq;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 876
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 870
    goto :goto_0

    :cond_1
    move v0, v2

    .line 876
    goto :goto_1
.end method
