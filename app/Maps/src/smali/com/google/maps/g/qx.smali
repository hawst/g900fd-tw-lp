.class public final Lcom/google/maps/g/qx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ra;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qx;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/maps/g/qx;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1637
    new-instance v0, Lcom/google/maps/g/qy;

    invoke-direct {v0}, Lcom/google/maps/g/qy;-><init>()V

    sput-object v0, Lcom/google/maps/g/qx;->PARSER:Lcom/google/n/ax;

    .line 1681
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/qx;->d:Lcom/google/n/aw;

    .line 1792
    new-instance v0, Lcom/google/maps/g/qx;

    invoke-direct {v0}, Lcom/google/maps/g/qx;-><init>()V

    sput-object v0, Lcom/google/maps/g/qx;->a:Lcom/google/maps/g/qx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1601
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1652
    iput-byte v0, p0, Lcom/google/maps/g/qx;->b:B

    .line 1668
    iput v0, p0, Lcom/google/maps/g/qx;->c:I

    .line 1602
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1608
    invoke-direct {p0}, Lcom/google/maps/g/qx;-><init>()V

    .line 1610
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1612
    const/4 v0, 0x0

    .line 1613
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1614
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1615
    packed-switch v3, :pswitch_data_0

    .line 1620
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1622
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 1618
    goto :goto_0

    .line 1634
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qx;->au:Lcom/google/n/bn;

    .line 1635
    return-void

    .line 1628
    :catch_0
    move-exception v0

    .line 1629
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1634
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qx;->au:Lcom/google/n/bn;

    throw v0

    .line 1630
    :catch_1
    move-exception v0

    .line 1631
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 1632
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1615
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1599
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1652
    iput-byte v0, p0, Lcom/google/maps/g/qx;->b:B

    .line 1668
    iput v0, p0, Lcom/google/maps/g/qx;->c:I

    .line 1600
    return-void
.end method

.method public static d()Lcom/google/maps/g/qx;
    .locals 1

    .prologue
    .line 1795
    sget-object v0, Lcom/google/maps/g/qx;->a:Lcom/google/maps/g/qx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/qz;
    .locals 1

    .prologue
    .line 1743
    new-instance v0, Lcom/google/maps/g/qz;

    invoke-direct {v0}, Lcom/google/maps/g/qz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/qx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1649
    sget-object v0, Lcom/google/maps/g/qx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 1664
    iget v0, p0, Lcom/google/maps/g/qx;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1665
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/qx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1666
    return-void

    .line 1664
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/qx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/maps/g/qx;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1654
    iget-byte v1, p0, Lcom/google/maps/g/qx;->b:B

    .line 1655
    if-ne v1, v0, :cond_0

    .line 1659
    :goto_0
    return v0

    .line 1656
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1658
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/qx;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1670
    iget v0, p0, Lcom/google/maps/g/qx;->c:I

    .line 1671
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1676
    :goto_0
    return v0

    .line 1673
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/qx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1675
    iput v0, p0, Lcom/google/maps/g/qx;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1593
    invoke-static {}, Lcom/google/maps/g/qx;->newBuilder()Lcom/google/maps/g/qz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/qz;->a(Lcom/google/maps/g/qx;)Lcom/google/maps/g/qz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1593
    invoke-static {}, Lcom/google/maps/g/qx;->newBuilder()Lcom/google/maps/g/qz;

    move-result-object v0

    return-object v0
.end method
