.class public final Lcom/google/maps/g/a/jm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/jr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/jm;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/a/jm;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/maps/g/a/jn;

    invoke-direct {v0}, Lcom/google/maps/g/a/jn;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/jm;->PARSER:Lcom/google/n/ax;

    .line 453
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/jm;->k:Lcom/google/n/aw;

    .line 1083
    new-instance v0, Lcom/google/maps/g/a/jm;

    invoke-direct {v0}, Lcom/google/maps/g/a/jm;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/jm;->h:Lcom/google/maps/g/a/jm;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    .line 367
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    .line 382
    iput-byte v2, p0, Lcom/google/maps/g/a/jm;->i:B

    .line 416
    iput v2, p0, Lcom/google/maps/g/a/jm;->j:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/jm;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/a/jm;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 38
    sparse-switch v4, :sswitch_data_0

    .line 43
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 51
    iget v4, p0, Lcom/google/maps/g/a/jm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/a/jm;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 101
    iget-object v1, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    .line 103
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/jm;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 56
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    .line 58
    or-int/lit8 v1, v1, 0x2

    .line 60
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 61
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 60
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 96
    :catch_1
    move-exception v0

    .line 97
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 98
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 66
    invoke-static {v4}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v5

    .line 67
    if-nez v5, :cond_3

    .line 68
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 70
    :cond_3
    iget v5, p0, Lcom/google/maps/g/a/jm;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/a/jm;->a:I

    .line 71
    iput v4, p0, Lcom/google/maps/g/a/jm;->d:I

    goto :goto_0

    .line 76
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 77
    iget v5, p0, Lcom/google/maps/g/a/jm;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/a/jm;->a:I

    .line 78
    iput-object v4, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 82
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 83
    iget v5, p0, Lcom/google/maps/g/a/jm;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/a/jm;->a:I

    .line 84
    iput-object v4, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 88
    :sswitch_6
    iget-object v4, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 89
    iget v4, p0, Lcom/google/maps/g/a/jm;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/g/a/jm;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 100
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_5

    .line 101
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    .line 103
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->au:Lcom/google/n/bn;

    .line 104
    return-void

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    .line 367
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    .line 382
    iput-byte v1, p0, Lcom/google/maps/g/a/jm;->i:B

    .line 416
    iput v1, p0, Lcom/google/maps/g/a/jm;->j:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/a/jm;
    .locals 1

    .prologue
    .line 1086
    sget-object v0, Lcom/google/maps/g/a/jm;->h:Lcom/google/maps/g/a/jm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/jo;
    .locals 1

    .prologue
    .line 515
    new-instance v0, Lcom/google/maps/g/a/jo;

    invoke-direct {v0}, Lcom/google/maps/g/a/jo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/jm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/google/maps/g/a/jm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 394
    invoke-virtual {p0}, Lcom/google/maps/g/a/jm;->c()I

    .line 395
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 398
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 398
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 401
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 402
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 404
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 405
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 407
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 408
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 410
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 411
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 413
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 414
    return-void

    .line 402
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 405
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 408
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 384
    iget-byte v1, p0, Lcom/google/maps/g/a/jm;->i:B

    .line 385
    if-ne v1, v0, :cond_0

    .line 389
    :goto_0
    return v0

    .line 386
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 388
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/jm;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 418
    iget v0, p0, Lcom/google/maps/g/a/jm;->j:I

    .line 419
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 448
    :goto_0
    return v0

    .line 422
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 423
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    .line 424
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 426
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    .line 428
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 426
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 430
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 431
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/maps/g/a/jm;->d:I

    .line 432
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 434
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 436
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 438
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 439
    const/4 v2, 0x5

    .line 440
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 442
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 443
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/g/a/jm;->g:Lcom/google/n/ao;

    .line 444
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 446
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 447
    iput v0, p0, Lcom/google/maps/g/a/jm;->j:I

    goto/16 :goto_0

    .line 432
    :cond_6
    const/16 v0, 0xa

    goto/16 :goto_3

    .line 436
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 440
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    .line 337
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 338
    check-cast v0, Ljava/lang/String;

    .line 346
    :goto_0
    return-object v0

    .line 340
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 342
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 343
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    iput-object v1, p0, Lcom/google/maps/g/a/jm;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 346
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/jm;->newBuilder()Lcom/google/maps/g/a/jo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/jo;->a(Lcom/google/maps/g/a/jm;)Lcom/google/maps/g/a/jo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/jm;->newBuilder()Lcom/google/maps/g/a/jo;

    move-result-object v0

    return-object v0
.end method
