.class public final Lcom/google/maps/g/a/bi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bi;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/a/bi;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field private l:I

.field private m:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/maps/g/a/bj;

    invoke-direct {v0}, Lcom/google/maps/g/a/bj;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bi;->PARSER:Lcom/google/n/ax;

    .line 439
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/bi;->p:Lcom/google/n/aw;

    .line 1187
    new-instance v0, Lcom/google/maps/g/a/bi;

    invoke-direct {v0}, Lcom/google/maps/g/a/bi;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/bi;->k:Lcom/google/maps/g/a/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 163
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    .line 179
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    .line 211
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    .line 227
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    .line 243
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    .line 279
    iput v3, p0, Lcom/google/maps/g/a/bi;->l:I

    .line 302
    iput v3, p0, Lcom/google/maps/g/a/bi;->m:I

    .line 319
    iput-byte v3, p0, Lcom/google/maps/g/a/bi;->n:B

    .line 370
    iput v3, p0, Lcom/google/maps/g/a/bi;->o:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/a/bi;->j:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const v12, 0x7fffffff

    const/16 v11, 0x80

    const/16 v10, 0x40

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/maps/g/a/bi;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v2

    move v1, v2

    .line 39
    :cond_0
    :goto_0
    if-nez v4, :cond_e

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 46
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x1

    move v4, v0

    goto :goto_0

    .line 43
    :sswitch_0
    const/4 v0, 0x1

    move v4, v0

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 54
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x40

    if-ne v2, v10, :cond_1

    .line 138
    iget-object v2, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 140
    :cond_1
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v11, :cond_2

    .line 141
    iget-object v1, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 143
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/bi;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 59
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 133
    :catch_1
    move-exception v0

    .line 134
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 135
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I

    goto/16 :goto_0

    .line 68
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 69
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I

    goto/16 :goto_0

    .line 73
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 74
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I

    goto/16 :goto_0

    .line 78
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I

    goto/16 :goto_0

    .line 83
    :sswitch_7
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v10, :cond_3

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 85
    or-int/lit8 v1, v1, 0x40

    .line 87
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    ushr-int/lit8 v7, v6, 0x1

    and-int/lit8 v6, v6, 0x1

    neg-int v6, v6

    xor-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 91
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 92
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 93
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v10, :cond_4

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_5

    move v0, v3

    :goto_1
    if-lez v0, :cond_4

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 95
    or-int/lit8 v1, v1, 0x40

    .line 97
    :cond_4
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_6

    move v0, v3

    :goto_3
    if-lez v0, :cond_7

    .line 98
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    ushr-int/lit8 v8, v7, 0x1

    and-int/lit8 v7, v7, 0x1

    neg-int v7, v7

    xor-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 97
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 100
    :cond_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 104
    :sswitch_9
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v11, :cond_8

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 106
    or-int/lit16 v1, v1, 0x80

    .line 108
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    ushr-int/lit8 v7, v6, 0x1

    and-int/lit8 v6, v6, 0x1

    neg-int v6, v6

    xor-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 112
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 113
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 114
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v11, :cond_9

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_a

    move v0, v3

    :goto_4
    if-lez v0, :cond_9

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 116
    or-int/lit16 v1, v1, 0x80

    .line 118
    :cond_9
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_b

    move v0, v3

    :goto_6
    if-lez v0, :cond_c

    .line 119
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    ushr-int/lit8 v8, v7, 0x1

    and-int/lit8 v7, v7, 0x1

    neg-int v7, v7

    xor-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 114
    :cond_a
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 118
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_6

    .line 121
    :cond_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 125
    :sswitch_b
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/bi;->a:I

    .line 126
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/google/maps/g/a/bi;->j:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto :goto_7

    .line 137
    :cond_e
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v10, :cond_f

    .line 138
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 140
    :cond_f
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v11, :cond_10

    .line 141
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 143
    :cond_10
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->au:Lcom/google/n/bn;

    .line 144
    return-void

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x42 -> :sswitch_a
        0x48 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 163
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    .line 179
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    .line 195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    .line 211
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    .line 227
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    .line 243
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    .line 279
    iput v1, p0, Lcom/google/maps/g/a/bi;->l:I

    .line 302
    iput v1, p0, Lcom/google/maps/g/a/bi;->m:I

    .line 319
    iput-byte v1, p0, Lcom/google/maps/g/a/bi;->n:B

    .line 370
    iput v1, p0, Lcom/google/maps/g/a/bi;->o:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;
    .locals 1

    .prologue
    .line 504
    invoke-static {}, Lcom/google/maps/g/a/bi;->newBuilder()Lcom/google/maps/g/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/bi;
    .locals 1

    .prologue
    .line 1190
    sget-object v0, Lcom/google/maps/g/a/bi;->k:Lcom/google/maps/g/a/bi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/bk;
    .locals 1

    .prologue
    .line 501
    new-instance v0, Lcom/google/maps/g/a/bk;

    invoke-direct {v0}, Lcom/google/maps/g/a/bk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/bi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    sget-object v0, Lcom/google/maps/g/a/bi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 331
    invoke-virtual {p0}, Lcom/google/maps/g/a/bi;->c()I

    .line 332
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 333
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 335
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 336
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 338
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 339
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 341
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 342
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 345
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 347
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 348
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 350
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 351
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 352
    iget v0, p0, Lcom/google/maps/g/a/bi;->l:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_6
    move v1, v2

    .line 354
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 355
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v4, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 354
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 357
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 358
    const/16 v0, 0x42

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 359
    iget v0, p0, Lcom/google/maps/g/a/bi;->m:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_8
    move v1, v2

    .line 361
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 362
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v4, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 361
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 364
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 365
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/maps/g/a/bi;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_a

    move v2, v3

    :cond_a
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 367
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 368
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 321
    iget-byte v1, p0, Lcom/google/maps/g/a/bi;->n:B

    .line 322
    if-ne v1, v0, :cond_0

    .line 326
    :goto_0
    return v0

    .line 323
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 325
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/bi;->n:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v5, 0xa

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 372
    iget v0, p0, Lcom/google/maps/g/a/bi;->o:I

    .line 373
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 434
    :goto_0
    return v0

    .line 376
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 377
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    .line 378
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 380
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 381
    iget-object v2, p0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    .line 382
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 384
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 385
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    .line 386
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 388
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 389
    iget-object v2, p0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    .line 390
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 392
    :cond_3
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 393
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    .line 394
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 396
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    .line 397
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    .line 398
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    move v4, v1

    .line 402
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 403
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    .line 404
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v6, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 402
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 406
    :cond_5
    add-int v0, v2, v4

    .line 407
    iget-object v2, p0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 408
    add-int/lit8 v2, v0, 0x1

    .line 410
    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    move v2, v0

    .line 412
    :goto_5
    iput v4, p0, Lcom/google/maps/g/a/bi;->l:I

    move v3, v1

    move v4, v1

    .line 416
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 417
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    .line 418
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v6, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 416
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    :cond_6
    move v0, v5

    .line 410
    goto :goto_4

    .line 420
    :cond_7
    add-int v0, v2, v4

    .line 421
    iget-object v2, p0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 422
    add-int/lit8 v0, v0, 0x1

    .line 424
    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v5

    :cond_8
    add-int/2addr v0, v5

    .line 426
    :cond_9
    iput v4, p0, Lcom/google/maps/g/a/bi;->m:I

    .line 428
    iget v2, p0, Lcom/google/maps/g/a/bi;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_a

    .line 429
    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/maps/g/a/bi;->j:Z

    .line 430
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 432
    :cond_a
    iget-object v1, p0, Lcom/google/maps/g/a/bi;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 433
    iput v0, p0, Lcom/google/maps/g/a/bi;->o:I

    goto/16 :goto_0

    :cond_b
    move v2, v0

    goto :goto_5

    :cond_c
    move v2, v0

    goto/16 :goto_2

    :cond_d
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bi;->newBuilder()Lcom/google/maps/g/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/bi;->newBuilder()Lcom/google/maps/g/a/bk;

    move-result-object v0

    return-object v0
.end method
