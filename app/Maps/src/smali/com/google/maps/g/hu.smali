.class public final enum Lcom/google/maps/g/hu;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/hu;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/hu;

.field public static final enum b:Lcom/google/maps/g/hu;

.field public static final enum c:Lcom/google/maps/g/hu;

.field private static final synthetic e:[Lcom/google/maps/g/hu;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 494
    new-instance v0, Lcom/google/maps/g/hu;

    const-string v1, "UNKNOWN_MAP_TILES"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/hu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hu;->a:Lcom/google/maps/g/hu;

    .line 498
    new-instance v0, Lcom/google/maps/g/hu;

    const-string v1, "MAP_TILES"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/hu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hu;->b:Lcom/google/maps/g/hu;

    .line 502
    new-instance v0, Lcom/google/maps/g/hu;

    const-string v1, "SATELLITE_TILES"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/hu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/hu;->c:Lcom/google/maps/g/hu;

    .line 489
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/hu;

    sget-object v1, Lcom/google/maps/g/hu;->a:Lcom/google/maps/g/hu;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/hu;->b:Lcom/google/maps/g/hu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/hu;->c:Lcom/google/maps/g/hu;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/hu;->e:[Lcom/google/maps/g/hu;

    .line 537
    new-instance v0, Lcom/google/maps/g/hv;

    invoke-direct {v0}, Lcom/google/maps/g/hv;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 547
    iput p3, p0, Lcom/google/maps/g/hu;->d:I

    .line 548
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/hu;
    .locals 1

    .prologue
    .line 524
    packed-switch p0, :pswitch_data_0

    .line 528
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 525
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/hu;->a:Lcom/google/maps/g/hu;

    goto :goto_0

    .line 526
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/hu;->b:Lcom/google/maps/g/hu;

    goto :goto_0

    .line 527
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/hu;->c:Lcom/google/maps/g/hu;

    goto :goto_0

    .line 524
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/hu;
    .locals 1

    .prologue
    .line 489
    const-class v0, Lcom/google/maps/g/hu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hu;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/hu;
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lcom/google/maps/g/hu;->e:[Lcom/google/maps/g/hu;

    invoke-virtual {v0}, [Lcom/google/maps/g/hu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/hu;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 520
    iget v0, p0, Lcom/google/maps/g/hu;->d:I

    return v0
.end method
