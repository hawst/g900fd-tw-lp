.class public final Lcom/google/maps/g/g/p;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/s;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/p;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/g/p;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274
    new-instance v0, Lcom/google/maps/g/g/q;

    invoke-direct {v0}, Lcom/google/maps/g/g/q;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/p;->PARSER:Lcom/google/n/ax;

    .line 390
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/g/p;->g:Lcom/google/n/aw;

    .line 633
    new-instance v0, Lcom/google/maps/g/g/p;

    invoke-direct {v0}, Lcom/google/maps/g/g/p;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/p;->d:Lcom/google/maps/g/g/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 224
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 347
    iput-byte v0, p0, Lcom/google/maps/g/g/p;->e:B

    .line 369
    iput v0, p0, Lcom/google/maps/g/g/p;->f:I

    .line 225
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    .line 226
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/g/p;->c:I

    .line 227
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 233
    invoke-direct {p0}, Lcom/google/maps/g/g/p;-><init>()V

    .line 234
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 238
    const/4 v0, 0x0

    .line 239
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 240
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 241
    sparse-switch v3, :sswitch_data_0

    .line 246
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 248
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 244
    goto :goto_0

    .line 253
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 254
    iget v4, p0, Lcom/google/maps/g/g/p;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/g/p;->a:I

    .line 255
    iput-object v3, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 265
    :catch_0
    move-exception v0

    .line 266
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/p;->au:Lcom/google/n/bn;

    throw v0

    .line 259
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/g/g/p;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/g/p;->a:I

    .line 260
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/g/p;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 267
    :catch_1
    move-exception v0

    .line 268
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 269
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/p;->au:Lcom/google/n/bn;

    .line 272
    return-void

    .line 241
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 222
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 347
    iput-byte v0, p0, Lcom/google/maps/g/g/p;->e:B

    .line 369
    iput v0, p0, Lcom/google/maps/g/g/p;->f:I

    .line 223
    return-void
.end method

.method public static d()Lcom/google/maps/g/g/p;
    .locals 1

    .prologue
    .line 636
    sget-object v0, Lcom/google/maps/g/g/p;->d:Lcom/google/maps/g/g/p;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/g/r;
    .locals 1

    .prologue
    .line 452
    new-instance v0, Lcom/google/maps/g/g/r;

    invoke-direct {v0}, Lcom/google/maps/g/g/r;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    sget-object v0, Lcom/google/maps/g/g/p;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 359
    invoke-virtual {p0}, Lcom/google/maps/g/g/p;->c()I

    .line 360
    iget v0, p0, Lcom/google/maps/g/g/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 363
    :cond_0
    iget v0, p0, Lcom/google/maps/g/g/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 364
    iget v0, p0, Lcom/google/maps/g/g/p;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 366
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/g/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 367
    return-void

    .line 361
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 364
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 349
    iget-byte v1, p0, Lcom/google/maps/g/g/p;->e:B

    .line 350
    if-ne v1, v0, :cond_0

    .line 354
    :goto_0
    return v0

    .line 351
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 353
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/g/p;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 371
    iget v0, p0, Lcom/google/maps/g/g/p;->f:I

    .line 372
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 385
    :goto_0
    return v0

    .line 375
    :cond_0
    iget v0, p0, Lcom/google/maps/g/g/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 377
    iget-object v0, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 379
    :goto_2
    iget v2, p0, Lcom/google/maps/g/g/p;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 380
    iget v2, p0, Lcom/google/maps/g/g/p;->c:I

    .line 381
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 383
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/g/p;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 384
    iput v0, p0, Lcom/google/maps/g/g/p;->f:I

    goto :goto_0

    .line 377
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 381
    :cond_3
    const/16 v1, 0xa

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 216
    invoke-static {}, Lcom/google/maps/g/g/p;->newBuilder()Lcom/google/maps/g/g/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/g/r;->a(Lcom/google/maps/g/g/p;)Lcom/google/maps/g/g/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 216
    invoke-static {}, Lcom/google/maps/g/g/p;->newBuilder()Lcom/google/maps/g/g/r;

    move-result-object v0

    return-object v0
.end method
