.class public final enum Lcom/google/maps/g/a/iy;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/iy;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/iy;

.field public static final enum b:Lcom/google/maps/g/a/iy;

.field public static final enum c:Lcom/google/maps/g/a/iy;

.field public static final enum d:Lcom/google/maps/g/a/iy;

.field private static final synthetic f:[Lcom/google/maps/g/a/iy;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/a/iy;

    const-string v1, "PREFER_BUS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    .line 18
    new-instance v0, Lcom/google/maps/g/a/iy;

    const-string v1, "PREFER_SUBWAY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    .line 22
    new-instance v0, Lcom/google/maps/g/a/iy;

    const-string v1, "PREFER_TRAIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    .line 26
    new-instance v0, Lcom/google/maps/g/a/iy;

    const-string v1, "PREFER_TRAM"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/a/iy;

    sget-object v1, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/a/iy;->f:[Lcom/google/maps/g/a/iy;

    .line 66
    new-instance v0, Lcom/google/maps/g/a/iz;

    invoke-direct {v0}, Lcom/google/maps/g/a/iz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput p3, p0, Lcom/google/maps/g/a/iy;->e:I

    .line 77
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/iy;
    .locals 1

    .prologue
    .line 52
    packed-switch p0, :pswitch_data_0

    .line 57
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 53
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    goto :goto_0

    .line 54
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    goto :goto_0

    .line 55
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/iy;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/a/iy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/iy;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/iy;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/a/iy;->f:[Lcom/google/maps/g/a/iy;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/iy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/iy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/maps/g/a/iy;->e:I

    return v0
.end method
