.class public final Lcom/google/maps/g/lj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/lk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/lh;",
        "Lcom/google/maps/g/lj;",
        ">;",
        "Lcom/google/maps/g/lk;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lcom/google/n/ao;

.field private g:Z

.field private h:Z

.field private i:Lcom/google/n/ao;

.field private j:Z

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:I

.field private n:Z

.field private o:Lcom/google/n/ao;

.field private p:Z

.field private q:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1013
    sget-object v0, Lcom/google/maps/g/lh;->r:Lcom/google/maps/g/lh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1206
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->b:Lcom/google/n/ao;

    .line 1361
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->f:Lcom/google/n/ao;

    .line 1484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->i:Lcom/google/n/ao;

    .line 1543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/g/lj;->j:Z

    .line 1575
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->k:Lcom/google/n/ao;

    .line 1634
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->l:Lcom/google/n/ao;

    .line 1693
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/lj;->m:I

    .line 1757
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lj;->o:Lcom/google/n/ao;

    .line 1014
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/lh;)Lcom/google/maps/g/lj;
    .locals 5

    .prologue
    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1141
    invoke-static {}, Lcom/google/maps/g/lh;->d()Lcom/google/maps/g/lh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1197
    :goto_0
    return-object p0

    .line 1142
    :cond_0
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_11

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1143
    iget-object v2, p0, Lcom/google/maps/g/lj;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1144
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1146
    :cond_1
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1147
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->c:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->c:Z

    .line 1149
    :cond_2
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1150
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->d:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->d:Z

    .line 1152
    :cond_3
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1153
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->e:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->e:Z

    .line 1155
    :cond_4
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1156
    iget-object v2, p0, Lcom/google/maps/g/lj;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1157
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1159
    :cond_5
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1160
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->g:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->g:Z

    .line 1162
    :cond_6
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1163
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->h:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->h:Z

    .line 1165
    :cond_7
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1166
    iget-object v2, p0, Lcom/google/maps/g/lj;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1167
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1169
    :cond_8
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1170
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->j:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->j:Z

    .line 1172
    :cond_9
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1173
    iget-object v2, p0, Lcom/google/maps/g/lj;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1174
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1176
    :cond_a
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1177
    iget-object v2, p0, Lcom/google/maps/g/lj;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1178
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1180
    :cond_b
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1181
    iget v2, p1, Lcom/google/maps/g/lh;->m:I

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput v2, p0, Lcom/google/maps/g/lj;->m:I

    .line 1183
    :cond_c
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 1184
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->n:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->n:Z

    .line 1186
    :cond_d
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 1187
    iget-object v2, p0, Lcom/google/maps/g/lj;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1188
    iget v2, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/g/lj;->a:I

    .line 1190
    :cond_e
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_f
    if-eqz v2, :cond_f

    .line 1191
    iget-boolean v2, p1, Lcom/google/maps/g/lh;->p:Z

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/lj;->p:Z

    .line 1193
    :cond_f
    iget v2, p1, Lcom/google/maps/g/lh;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_20

    :goto_10
    if-eqz v0, :cond_10

    .line 1194
    iget-boolean v0, p1, Lcom/google/maps/g/lh;->q:Z

    iget v1, p0, Lcom/google/maps/g/lj;->a:I

    or-int/2addr v1, v4

    iput v1, p0, Lcom/google/maps/g/lj;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/lj;->q:Z

    .line 1196
    :cond_10
    iget-object v0, p1, Lcom/google/maps/g/lh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v2, v1

    .line 1142
    goto/16 :goto_1

    :cond_12
    move v2, v1

    .line 1146
    goto/16 :goto_2

    :cond_13
    move v2, v1

    .line 1149
    goto/16 :goto_3

    :cond_14
    move v2, v1

    .line 1152
    goto/16 :goto_4

    :cond_15
    move v2, v1

    .line 1155
    goto/16 :goto_5

    :cond_16
    move v2, v1

    .line 1159
    goto/16 :goto_6

    :cond_17
    move v2, v1

    .line 1162
    goto/16 :goto_7

    :cond_18
    move v2, v1

    .line 1165
    goto/16 :goto_8

    :cond_19
    move v2, v1

    .line 1169
    goto/16 :goto_9

    :cond_1a
    move v2, v1

    .line 1172
    goto/16 :goto_a

    :cond_1b
    move v2, v1

    .line 1176
    goto/16 :goto_b

    :cond_1c
    move v2, v1

    .line 1180
    goto/16 :goto_c

    :cond_1d
    move v2, v1

    .line 1183
    goto :goto_d

    :cond_1e
    move v2, v1

    .line 1186
    goto :goto_e

    :cond_1f
    move v2, v1

    .line 1190
    goto :goto_f

    :cond_20
    move v0, v1

    .line 1193
    goto :goto_10
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1005
    new-instance v2, Lcom/google/maps/g/lh;

    invoke-direct {v2, p0}, Lcom/google/maps/g/lh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/lj;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_f

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/lh;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->c:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->d:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->e:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/lh;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->g:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->h:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/lh;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->j:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/lh;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, v2, Lcom/google/maps/g/lh;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v4, p0, Lcom/google/maps/g/lj;->m:I

    iput v4, v2, Lcom/google/maps/g/lh;->m:I

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-boolean v4, p0, Lcom/google/maps/g/lj;->n:Z

    iput-boolean v4, v2, Lcom/google/maps/g/lh;->n:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-object v4, v2, Lcom/google/maps/g/lh;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/lj;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/lj;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget-boolean v1, p0, Lcom/google/maps/g/lj;->p:Z

    iput-boolean v1, v2, Lcom/google/maps/g/lh;->p:Z

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/2addr v0, v7

    :cond_e
    iget-boolean v1, p0, Lcom/google/maps/g/lj;->q:Z

    iput-boolean v1, v2, Lcom/google/maps/g/lh;->q:Z

    iput v0, v2, Lcom/google/maps/g/lh;->a:I

    return-object v2

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1005
    check-cast p1, Lcom/google/maps/g/lh;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/lj;->a(Lcom/google/maps/g/lh;)Lcom/google/maps/g/lj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1201
    const/4 v0, 0x1

    return v0
.end method
