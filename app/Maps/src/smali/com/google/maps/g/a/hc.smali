.class public final enum Lcom/google/maps/g/a/hc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/hc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/hc;

.field public static final enum b:Lcom/google/maps/g/a/hc;

.field public static final enum c:Lcom/google/maps/g/a/hc;

.field public static final enum d:Lcom/google/maps/g/a/hc;

.field public static final enum e:Lcom/google/maps/g/a/hc;

.field public static final enum f:Lcom/google/maps/g/a/hc;

.field public static final enum g:Lcom/google/maps/g/a/hc;

.field public static final enum h:Lcom/google/maps/g/a/hc;

.field private static final synthetic j:[Lcom/google/maps/g/a/hc;


# instance fields
.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_SERVER_DEFINED_TIME"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    .line 18
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_DEPARTURE_TIME"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    .line 22
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_ARRIVAL_TIME"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    .line 26
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_LAST_AVAILABLE"

    invoke-direct {v0, v1, v7, v6}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    .line 30
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_OVERVIEW_NOW"

    invoke-direct {v0, v1, v8, v7}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->e:Lcom/google/maps/g/a/hc;

    .line 34
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_CALENDAR_DEPARTURE_TIME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->f:Lcom/google/maps/g/a/hc;

    .line 38
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_CALENDAR_ARRIVAL_TIME"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->g:Lcom/google/maps/g/a/hc;

    .line 42
    new-instance v0, Lcom/google/maps/g/a/hc;

    const-string v1, "TRANSIT_OVERVIEW"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/hc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hc;->h:Lcom/google/maps/g/a/hc;

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/maps/g/a/hc;

    sget-object v1, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/hc;->e:Lcom/google/maps/g/a/hc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/hc;->f:Lcom/google/maps/g/a/hc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/hc;->g:Lcom/google/maps/g/a/hc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/hc;->h:Lcom/google/maps/g/a/hc;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/hc;->j:[Lcom/google/maps/g/a/hc;

    .line 102
    new-instance v0, Lcom/google/maps/g/a/hd;

    invoke-direct {v0}, Lcom/google/maps/g/a/hd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput p3, p0, Lcom/google/maps/g/a/hc;->i:I

    .line 113
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/hc;
    .locals 1

    .prologue
    .line 84
    sparse-switch p0, :sswitch_data_0

    .line 93
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 85
    :sswitch_0
    sget-object v0, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 86
    :sswitch_1
    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 87
    :sswitch_2
    sget-object v0, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 88
    :sswitch_3
    sget-object v0, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 89
    :sswitch_4
    sget-object v0, Lcom/google/maps/g/a/hc;->e:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 90
    :sswitch_5
    sget-object v0, Lcom/google/maps/g/a/hc;->f:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 91
    :sswitch_6
    sget-object v0, Lcom/google/maps/g/a/hc;->g:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 92
    :sswitch_7
    sget-object v0, Lcom/google/maps/g/a/hc;->h:Lcom/google/maps/g/a/hc;

    goto :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/hc;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/a/hc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hc;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/hc;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/a/hc;->j:[Lcom/google/maps/g/a/hc;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/hc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/hc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/maps/g/a/hc;->i:I

    return v0
.end method
