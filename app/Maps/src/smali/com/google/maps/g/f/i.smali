.class public final enum Lcom/google/maps/g/f/i;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/f/i;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/f/i;

.field public static final enum b:Lcom/google/maps/g/f/i;

.field public static final enum c:Lcom/google/maps/g/f/i;

.field public static final enum d:Lcom/google/maps/g/f/i;

.field public static final enum e:Lcom/google/maps/g/f/i;

.field public static final enum f:Lcom/google/maps/g/f/i;

.field public static final enum g:Lcom/google/maps/g/f/i;

.field public static final enum h:Lcom/google/maps/g/f/i;

.field public static final enum i:Lcom/google/maps/g/f/i;

.field public static final enum j:Lcom/google/maps/g/f/i;

.field public static final enum k:Lcom/google/maps/g/f/i;

.field public static final enum l:Lcom/google/maps/g/f/i;

.field public static final enum m:Lcom/google/maps/g/f/i;

.field public static final enum n:Lcom/google/maps/g/f/i;

.field public static final enum o:Lcom/google/maps/g/f/i;

.field public static final enum p:Lcom/google/maps/g/f/i;

.field private static final synthetic r:[Lcom/google/maps/g/f/i;


# instance fields
.field public final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 14
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "OUTDOOR_PANO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->a:Lcom/google/maps/g/f/i;

    .line 18
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "INDOOR_PANO"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->b:Lcom/google/maps/g/f/i;

    .line 22
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "INNERSPACE_PHOTO"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->c:Lcom/google/maps/g/f/i;

    .line 26
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "ART_PROJECT"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->d:Lcom/google/maps/g/f/i;

    .line 30
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "SPECIAL_COLLECT"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->e:Lcom/google/maps/g/f/i;

    .line 34
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "DRAGONFLY_PHOTO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->f:Lcom/google/maps/g/f/i;

    .line 38
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "LIGHTFIELD_TOUR"

    const/4 v2, 0x6

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->g:Lcom/google/maps/g/f/i;

    .line 42
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "PANORAMIO_PHOTO"

    const/4 v2, 0x7

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->h:Lcom/google/maps/g/f/i;

    .line 46
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "USER_GENERATED_PANO"

    const/16 v2, 0x8

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->i:Lcom/google/maps/g/f/i;

    .line 50
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "LOCAL_PLUS_PHOTO"

    const/16 v2, 0x9

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->j:Lcom/google/maps/g/f/i;

    .line 54
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "STOREFRONT_PANO"

    const/16 v2, 0xa

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->k:Lcom/google/maps/g/f/i;

    .line 58
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "DRAGONFLY_TOUR"

    const/16 v2, 0xb

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->l:Lcom/google/maps/g/f/i;

    .line 62
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "PHOTO_SERVICE"

    const/16 v2, 0xc

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->m:Lcom/google/maps/g/f/i;

    .line 66
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "HELICOPTER_TOUR"

    const/16 v2, 0xd

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->n:Lcom/google/maps/g/f/i;

    .line 70
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "INNERSPACE_TOUR"

    const/16 v2, 0xe

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->o:Lcom/google/maps/g/f/i;

    .line 74
    new-instance v0, Lcom/google/maps/g/f/i;

    const-string v1, "INTERNET_PHOTO"

    const/16 v2, 0xf

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/f/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/f/i;->p:Lcom/google/maps/g/f/i;

    .line 9
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/maps/g/f/i;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/g/f/i;->a:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/g/f/i;->b:Lcom/google/maps/g/f/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/f/i;->c:Lcom/google/maps/g/f/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/f/i;->d:Lcom/google/maps/g/f/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/f/i;->e:Lcom/google/maps/g/f/i;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/f/i;->f:Lcom/google/maps/g/f/i;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/f/i;->g:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/f/i;->h:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/f/i;->i:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/f/i;->j:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/f/i;->k:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/f/i;->l:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/f/i;->m:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/f/i;->n:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/f/i;->o:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/f/i;->p:Lcom/google/maps/g/f/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/f/i;->r:[Lcom/google/maps/g/f/i;

    .line 174
    new-instance v0, Lcom/google/maps/g/f/j;

    invoke-direct {v0}, Lcom/google/maps/g/f/j;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/google/maps/g/f/i;->q:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/f/i;
    .locals 1

    .prologue
    .line 148
    packed-switch p0, :pswitch_data_0

    .line 165
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 149
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/f/i;->a:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 150
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/f/i;->b:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 151
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/f/i;->c:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 152
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/f/i;->d:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 153
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/f/i;->e:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 154
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/f/i;->f:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 155
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/f/i;->g:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 156
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/f/i;->h:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 157
    :pswitch_9
    sget-object v0, Lcom/google/maps/g/f/i;->i:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 158
    :pswitch_a
    sget-object v0, Lcom/google/maps/g/f/i;->j:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 159
    :pswitch_b
    sget-object v0, Lcom/google/maps/g/f/i;->k:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 160
    :pswitch_c
    sget-object v0, Lcom/google/maps/g/f/i;->l:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 161
    :pswitch_d
    sget-object v0, Lcom/google/maps/g/f/i;->m:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 162
    :pswitch_e
    sget-object v0, Lcom/google/maps/g/f/i;->n:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 163
    :pswitch_f
    sget-object v0, Lcom/google/maps/g/f/i;->o:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 164
    :pswitch_10
    sget-object v0, Lcom/google/maps/g/f/i;->p:Lcom/google/maps/g/f/i;

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/f/i;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/f/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/f/i;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/f/i;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/f/i;->r:[Lcom/google/maps/g/f/i;

    invoke-virtual {v0}, [Lcom/google/maps/g/f/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/f/i;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/maps/g/f/i;->q:I

    return v0
.end method
