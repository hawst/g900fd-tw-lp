.class public final Lcom/google/maps/g/a/hi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/hl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/hg;",
        "Lcom/google/maps/g/a/hi;",
        ">;",
        "Lcom/google/maps/g/a/hl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Lcom/google/n/ao;

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Z

.field private m:Z

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/Object;

.field private p:Ljava/lang/Object;

.field private q:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 862
    sget-object v0, Lcom/google/maps/g/a/hg;->r:Lcom/google/maps/g/a/hg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1074
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->b:Lcom/google/n/ao;

    .line 1133
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->c:Lcom/google/n/ao;

    .line 1224
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->e:Ljava/lang/Object;

    .line 1332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->g:Lcom/google/n/ao;

    .line 1424
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    .line 1560
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->j:Lcom/google/n/ao;

    .line 1619
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->k:Lcom/google/n/ao;

    .line 1743
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    .line 1879
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->o:Ljava/lang/Object;

    .line 1955
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hi;->p:Ljava/lang/Object;

    .line 2031
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/hi;->q:I

    .line 863
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/hg;)Lcom/google/maps/g/a/hi;
    .locals 6

    .prologue
    const/16 v5, 0x1000

    const/16 v4, 0x80

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 990
    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1065
    :goto_0
    return-object p0

    .line 991
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_11

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 992
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 993
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 995
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 996
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 997
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 999
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1000
    iget v2, p1, Lcom/google/maps/g/a/hg;->d:I

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/hi;->a:I

    iput v2, p0, Lcom/google/maps/g/a/hi;->d:I

    .line 1002
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1003
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1004
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->e:Ljava/lang/Object;

    .line 1007
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1008
    iget-boolean v2, p1, Lcom/google/maps/g/a/hg;->f:Z

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/hi;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/hi;->f:Z

    .line 1010
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1011
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1012
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1014
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1015
    iget v2, p1, Lcom/google/maps/g/a/hg;->h:I

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/hi;->a:I

    iput v2, p0, Lcom/google/maps/g/a/hi;->h:I

    .line 1017
    :cond_7
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1018
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1019
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    .line 1020
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1027
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v4, :cond_1a

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1028
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1029
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1031
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1032
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1033
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1035
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1036
    iget-boolean v2, p1, Lcom/google/maps/g/a/hg;->l:Z

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/g/a/hi;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/hi;->l:Z

    .line 1038
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1039
    iget-boolean v2, p1, Lcom/google/maps/g/a/hg;->m:Z

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/a/hi;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/hi;->m:Z

    .line 1041
    :cond_c
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1042
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1043
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    .line 1044
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1051
    :cond_d
    :goto_d
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 1052
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1053
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->o:Ljava/lang/Object;

    .line 1056
    :cond_e
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-ne v2, v5, :cond_21

    move v2, v0

    :goto_f
    if-eqz v2, :cond_f

    .line 1057
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1058
    iget-object v2, p1, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->p:Ljava/lang/Object;

    .line 1061
    :cond_f
    iget v2, p1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_22

    :goto_10
    if-eqz v0, :cond_24

    .line 1062
    iget v0, p1, Lcom/google/maps/g/a/hg;->q:I

    invoke-static {v0}, Lcom/google/maps/g/a/hj;->a(I)Lcom/google/maps/g/a/hj;

    move-result-object v0

    if-nez v0, :cond_10

    sget-object v0, Lcom/google/maps/g/a/hj;->a:Lcom/google/maps/g/a/hj;

    :cond_10
    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move v2, v1

    .line 991
    goto/16 :goto_1

    :cond_12
    move v2, v1

    .line 995
    goto/16 :goto_2

    :cond_13
    move v2, v1

    .line 999
    goto/16 :goto_3

    :cond_14
    move v2, v1

    .line 1002
    goto/16 :goto_4

    :cond_15
    move v2, v1

    .line 1007
    goto/16 :goto_5

    :cond_16
    move v2, v1

    .line 1010
    goto/16 :goto_6

    :cond_17
    move v2, v1

    .line 1014
    goto/16 :goto_7

    .line 1022
    :cond_18
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v4, :cond_19

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1023
    :cond_19
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_1a
    move v2, v1

    .line 1027
    goto/16 :goto_9

    :cond_1b
    move v2, v1

    .line 1031
    goto/16 :goto_a

    :cond_1c
    move v2, v1

    .line 1035
    goto/16 :goto_b

    :cond_1d
    move v2, v1

    .line 1038
    goto/16 :goto_c

    .line 1046
    :cond_1e
    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-eq v2, v5, :cond_1f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/a/hi;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/a/hi;->a:I

    .line 1047
    :cond_1f
    iget-object v2, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_d

    :cond_20
    move v2, v1

    .line 1051
    goto/16 :goto_e

    :cond_21
    move v2, v1

    .line 1056
    goto/16 :goto_f

    :cond_22
    move v0, v1

    .line 1061
    goto :goto_10

    .line 1062
    :cond_23
    iget v1, p0, Lcom/google/maps/g/a/hi;->a:I

    const v2, 0x8000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/a/hi;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hj;->d:I

    iput v0, p0, Lcom/google/maps/g/a/hi;->q:I

    .line 1064
    :cond_24
    iget-object v0, p1, Lcom/google/maps/g/a/hg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 8

    .prologue
    const v7, 0x8000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 854
    new-instance v2, Lcom/google/maps/g/a/hg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/hg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_f

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/hi;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/hi;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/hi;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/hi;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/hi;->d:I

    iput v4, v2, Lcom/google/maps/g/a/hg;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/a/hi;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v4, p0, Lcom/google/maps/g/a/hi;->f:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/hg;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/hi;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/hi;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/maps/g/a/hi;->h:I

    iput v4, v2, Lcom/google/maps/g/a/hg;->h:I

    iget v4, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/maps/g/a/hi;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/a/hi;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/a/hg;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/hi;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/hi;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/a/hg;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/hi;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/hi;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-boolean v1, p0, Lcom/google/maps/g/a/hi;->l:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/hg;->l:Z

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-boolean v1, p0, Lcom/google/maps/g/a/hi;->m:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/hg;->m:Z

    iget v1, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    iget-object v1, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/hi;->a:I

    and-int/lit16 v1, v1, -0x1001

    iput v1, p0, Lcom/google/maps/g/a/hi;->a:I

    :cond_b
    iget-object v1, p0, Lcom/google/maps/g/a/hi;->n:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x800

    :cond_c
    iget-object v1, p0, Lcom/google/maps/g/a/hi;->o:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/hg;->o:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x1000

    :cond_d
    iget-object v1, p0, Lcom/google/maps/g/a/hi;->p:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/hg;->p:Ljava/lang/Object;

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/lit16 v0, v0, 0x2000

    :cond_e
    iget v1, p0, Lcom/google/maps/g/a/hi;->q:I

    iput v1, v2, Lcom/google/maps/g/a/hg;->q:I

    iput v0, v2, Lcom/google/maps/g/a/hg;->a:I

    return-object v2

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 854
    check-cast p1, Lcom/google/maps/g/a/hg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/hi;->a(Lcom/google/maps/g/a/hg;)Lcom/google/maps/g/a/hi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1069
    const/4 v0, 0x1

    return v0
.end method
