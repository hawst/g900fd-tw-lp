.class public final Lcom/google/maps/g/fw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/fu;",
        "Lcom/google/maps/g/fw;",
        ">;",
        "Lcom/google/maps/g/gd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1579
    sget-object v0, Lcom/google/maps/g/fu;->l:Lcom/google/maps/g/fu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1721
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fw;->b:Lcom/google/n/ao;

    .line 1780
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fw;->c:Lcom/google/n/ao;

    .line 1839
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/fw;->d:I

    .line 1875
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fw;->e:Ljava/lang/Object;

    .line 1951
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fw;->f:Ljava/lang/Object;

    .line 2027
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fw;->g:Ljava/lang/Object;

    .line 2135
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fw;->i:Lcom/google/n/ao;

    .line 2194
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fw;->j:Lcom/google/n/ao;

    .line 2253
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/fw;->k:Lcom/google/n/ao;

    .line 1580
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/fu;)Lcom/google/maps/g/fw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1669
    invoke-static {}, Lcom/google/maps/g/fu;->d()Lcom/google/maps/g/fu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1712
    :goto_0
    return-object p0

    .line 1670
    :cond_0
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1671
    iget-object v2, p0, Lcom/google/maps/g/fw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1672
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1674
    :cond_1
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1675
    iget-object v2, p0, Lcom/google/maps/g/fw;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1676
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1678
    :cond_2
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1679
    iget v2, p1, Lcom/google/maps/g/fu;->d:I

    invoke-static {v2}, Lcom/google/maps/g/gb;->a(I)Lcom/google/maps/g/gb;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1670
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1674
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1678
    goto :goto_3

    .line 1679
    :cond_7
    iget v3, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/fw;->a:I

    iget v2, v2, Lcom/google/maps/g/gb;->c:I

    iput v2, p0, Lcom/google/maps/g/fw;->d:I

    .line 1681
    :cond_8
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 1682
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1683
    iget-object v2, p1, Lcom/google/maps/g/fu;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fw;->e:Ljava/lang/Object;

    .line 1686
    :cond_9
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 1687
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1688
    iget-object v2, p1, Lcom/google/maps/g/fu;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fw;->f:Ljava/lang/Object;

    .line 1691
    :cond_a
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_b

    .line 1692
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1693
    iget-object v2, p1, Lcom/google/maps/g/fu;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fw;->g:Ljava/lang/Object;

    .line 1696
    :cond_b
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    .line 1697
    iget v2, p1, Lcom/google/maps/g/fu;->h:I

    iget v3, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/fw;->a:I

    iput v2, p0, Lcom/google/maps/g/fw;->h:I

    .line 1699
    :cond_c
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_d

    .line 1700
    iget-object v2, p0, Lcom/google/maps/g/fw;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/fu;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1701
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1703
    :cond_d
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_e

    .line 1704
    iget-object v2, p0, Lcom/google/maps/g/fw;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/fu;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1705
    iget v2, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/fw;->a:I

    .line 1707
    :cond_e
    iget v2, p1, Lcom/google/maps/g/fu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_16

    :goto_a
    if-eqz v0, :cond_f

    .line 1708
    iget-object v0, p0, Lcom/google/maps/g/fw;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/fu;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1709
    iget v0, p0, Lcom/google/maps/g/fw;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/fw;->a:I

    .line 1711
    :cond_f
    iget-object v0, p1, Lcom/google/maps/g/fu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v2, v1

    .line 1681
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 1686
    goto/16 :goto_5

    :cond_12
    move v2, v1

    .line 1691
    goto :goto_6

    :cond_13
    move v2, v1

    .line 1696
    goto :goto_7

    :cond_14
    move v2, v1

    .line 1699
    goto :goto_8

    :cond_15
    move v2, v1

    .line 1703
    goto :goto_9

    :cond_16
    move v0, v1

    .line 1707
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1571
    new-instance v2, Lcom/google/maps/g/fu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/fu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/fw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/fw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/fw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/fw;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/fw;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/g/fw;->d:I

    iput v4, v2, Lcom/google/maps/g/fu;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/fw;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/fu;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/fw;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/fu;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/fw;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/fu;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/maps/g/fw;->h:I

    iput v4, v2, Lcom/google/maps/g/fu;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/fu;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/fw;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/fw;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/fu;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/fw;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/fw;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v3, v2, Lcom/google/maps/g/fu;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/fw;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/fw;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/fu;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1571
    check-cast p1, Lcom/google/maps/g/fu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/fw;->a(Lcom/google/maps/g/fu;)Lcom/google/maps/g/fw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1716
    const/4 v0, 0x1

    return v0
.end method
