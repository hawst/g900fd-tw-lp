.class public final Lcom/google/maps/g/dw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dz;


# static fields
.field private static volatile C:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dw;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final z:Lcom/google/maps/g/dw;


# instance fields
.field private A:B

.field private B:I

.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:I

.field g:I

.field h:Lcom/google/n/ao;

.field i:I

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/lang/Object;

.field n:Ljava/lang/Object;

.field o:Z

.field p:Z

.field q:Lcom/google/n/ao;

.field r:Z

.field s:Z

.field t:Z

.field u:Lcom/google/n/ao;

.field v:Z

.field w:I

.field x:Lcom/google/n/ao;

.field y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/google/maps/g/dx;

    invoke-direct {v0}, Lcom/google/maps/g/dx;-><init>()V

    sput-object v0, Lcom/google/maps/g/dw;->PARSER:Lcom/google/n/ax;

    .line 934
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/dw;->C:Lcom/google/n/aw;

    .line 2609
    new-instance v0, Lcom/google/maps/g/dw;

    invoke-direct {v0}, Lcom/google/maps/g/dw;-><init>()V

    sput-object v0, Lcom/google/maps/g/dw;->z:Lcom/google/maps/g/dw;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 310
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    .line 326
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    .line 373
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    .line 405
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    .line 421
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    .line 594
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    .line 701
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    .line 731
    iput-byte v4, p0, Lcom/google/maps/g/dw;->A:B

    .line 825
    iput v4, p0, Lcom/google/maps/g/dw;->B:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 19
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/maps/g/dw;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput v3, p0, Lcom/google/maps/g/dw;->f:I

    .line 23
    iput v5, p0, Lcom/google/maps/g/dw;->g:I

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput v5, p0, Lcom/google/maps/g/dw;->i:I

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    .line 31
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->o:Z

    .line 32
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->p:Z

    .line 33
    iget-object v0, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->r:Z

    .line 35
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->s:Z

    .line 36
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->t:Z

    .line 37
    iget-object v0, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 38
    iput-boolean v3, p0, Lcom/google/maps/g/dw;->v:Z

    .line 39
    iput v3, p0, Lcom/google/maps/g/dw;->w:I

    .line 40
    iget-object v0, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 41
    iput-boolean v2, p0, Lcom/google/maps/g/dw;->y:Z

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x400

    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/maps/g/dw;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 54
    :cond_0
    :goto_0
    if-nez v4, :cond_f

    .line 55
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 61
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 63
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 59
    goto :goto_0

    .line 68
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_1

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 71
    or-int/lit8 v1, v1, 0x1

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 73
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :catchall_0
    move-exception v0

    and-int/lit8 v3, v1, 0x1

    if-ne v3, v2, :cond_2

    .line 226
    iget-object v2, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 228
    :cond_2
    and-int/lit16 v1, v1, 0x400

    if-ne v1, v10, :cond_3

    .line 229
    iget-object v1, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 231
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/dw;->au:Lcom/google/n/bn;

    throw v0

    .line 78
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 79
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v6

    .line 80
    if-nez v6, :cond_4

    .line 81
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 221
    :catch_1
    move-exception v0

    .line 222
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 223
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 83
    :cond_4
    :try_start_4
    iget v6, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/dw;->a:I

    .line 84
    iput v0, p0, Lcom/google/maps/g/dw;->c:I

    goto :goto_0

    .line 89
    :sswitch_3
    iget-object v0, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 90
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 94
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/dw;->f:I

    goto/16 :goto_0

    .line 99
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 100
    invoke-static {v0}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v6

    .line 101
    if-nez v6, :cond_5

    .line 102
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 104
    :cond_5
    iget v6, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/g/dw;->a:I

    .line 105
    iput v0, p0, Lcom/google/maps/g/dw;->g:I

    goto/16 :goto_0

    .line 110
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 111
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 115
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 116
    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v6

    .line 117
    if-nez v6, :cond_6

    .line 118
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 120
    :cond_6
    iget v6, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/maps/g/dw;->a:I

    .line 121
    iput v0, p0, Lcom/google/maps/g/dw;->i:I

    goto/16 :goto_0

    .line 126
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 127
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 131
    :sswitch_9
    and-int/lit16 v0, v1, 0x400

    if-eq v0, v10, :cond_7

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 134
    or-int/lit16 v1, v1, 0x400

    .line 136
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 137
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 136
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 141
    :sswitch_a
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 142
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->o:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_1

    .line 146
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 147
    iget v6, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/maps/g/dw;->a:I

    .line 148
    iput-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 152
    :sswitch_c
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 153
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->p:Z

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_2

    .line 157
    :sswitch_d
    iget-object v0, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 158
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 162
    :sswitch_e
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 163
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->r:Z

    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto :goto_3

    .line 167
    :sswitch_f
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 168
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_b

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->s:Z

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_4

    .line 172
    :sswitch_10
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 173
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_c

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->t:Z

    goto/16 :goto_0

    :cond_c
    move v0, v3

    goto :goto_5

    .line 177
    :sswitch_11
    iget-object v0, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 178
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 182
    :sswitch_12
    iget-object v0, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 183
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 187
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 188
    iget v6, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/maps/g/dw;->a:I

    .line 189
    iput-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 193
    :sswitch_14
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 194
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_d

    move v0, v2

    :goto_6
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->v:Z

    goto/16 :goto_0

    :cond_d
    move v0, v3

    goto :goto_6

    .line 198
    :sswitch_15
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 199
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/dw;->w:I

    goto/16 :goto_0

    .line 203
    :sswitch_16
    iget-object v0, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 204
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    goto/16 :goto_0

    .line 208
    :sswitch_17
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/maps/g/dw;->a:I

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    move v0, v2

    :goto_7
    iput-boolean v0, p0, Lcom/google/maps/g/dw;->y:Z

    goto/16 :goto_0

    :cond_e
    move v0, v3

    goto :goto_7

    .line 213
    :sswitch_18
    iget-object v0, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 214
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/dw;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 225
    :cond_f
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_10

    .line 226
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 228
    :cond_10
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v10, :cond_11

    .line 229
    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 231
    :cond_11
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->au:Lcom/google/n/bn;

    .line 232
    return-void

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x80 -> :sswitch_e
        0x88 -> :sswitch_f
        0x90 -> :sswitch_10
        0x9a -> :sswitch_11
        0xa2 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb0 -> :sswitch_14
        0xb8 -> :sswitch_15
        0xc2 -> :sswitch_16
        0xc8 -> :sswitch_17
        0xd2 -> :sswitch_18
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 310
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    .line 326
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    .line 373
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    .line 405
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    .line 421
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    .line 594
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    .line 701
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    .line 731
    iput-byte v1, p0, Lcom/google/maps/g/dw;->A:B

    .line 825
    iput v1, p0, Lcom/google/maps/g/dw;->B:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;
    .locals 1

    .prologue
    .line 999
    invoke-static {}, Lcom/google/maps/g/dw;->newBuilder()Lcom/google/maps/g/dy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/dy;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/dw;
    .locals 1

    .prologue
    .line 2612
    sget-object v0, Lcom/google/maps/g/dw;->z:Lcom/google/maps/g/dw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/dy;
    .locals 1

    .prologue
    .line 996
    new-instance v0, Lcom/google/maps/g/dy;

    invoke-direct {v0}, Lcom/google/maps/g/dy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/dw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    sget-object v0, Lcom/google/maps/g/dw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 749
    invoke-virtual {p0}, Lcom/google/maps/g/dw;->c()I

    move v1, v2

    .line 750
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 750
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 753
    :cond_0
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 754
    iget v0, p0, Lcom/google/maps/g/dw;->c:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 756
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 757
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 759
    :cond_2
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 760
    iget v0, p0, Lcom/google/maps/g/dw;->f:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 762
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 763
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/dw;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 765
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 766
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 768
    :cond_5
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 769
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/maps/g/dw;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 771
    :cond_6
    :goto_4
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 772
    iget-object v0, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_7
    move v1, v2

    .line 774
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 775
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 774
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 754
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 760
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 763
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 769
    :cond_b
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 777
    :cond_c
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 778
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->o:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1c

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 780
    :cond_d
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 781
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 783
    :cond_e
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_f

    .line 784
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->p:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1e

    move v0, v3

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 786
    :cond_f
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_10

    .line 787
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 789
    :cond_10
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_11

    .line 790
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->r:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1f

    move v0, v3

    :goto_9
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 792
    :cond_11
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_12

    .line 793
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->s:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_20

    move v0, v3

    :goto_a
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 795
    :cond_12
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_13

    .line 796
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->t:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_21

    move v0, v3

    :goto_b
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 798
    :cond_13
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_14

    .line 799
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 801
    :cond_14
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_15

    .line 802
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 804
    :cond_15
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_16

    .line 805
    const/16 v1, 0x15

    iget-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 807
    :cond_16
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_17

    .line 808
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->v:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_23

    move v0, v3

    :goto_d
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 810
    :cond_17
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_18

    .line 811
    const/16 v0, 0x17

    iget v1, p0, Lcom/google/maps/g/dw;->w:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_24

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 813
    :cond_18
    :goto_e
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_19

    .line 814
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 816
    :cond_19
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_1a

    .line 817
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->y:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_25

    :goto_f
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 819
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1b

    .line 820
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 822
    :cond_1b
    iget-object v0, p0, Lcom/google/maps/g/dw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 823
    return-void

    :cond_1c
    move v0, v2

    .line 778
    goto/16 :goto_6

    .line 781
    :cond_1d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    :cond_1e
    move v0, v2

    .line 784
    goto/16 :goto_8

    :cond_1f
    move v0, v2

    .line 790
    goto/16 :goto_9

    :cond_20
    move v0, v2

    .line 793
    goto/16 :goto_a

    :cond_21
    move v0, v2

    .line 796
    goto/16 :goto_b

    .line 805
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    :cond_23
    move v0, v2

    .line 808
    goto/16 :goto_d

    .line 811
    :cond_24
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_e

    :cond_25
    move v3, v2

    .line 817
    goto :goto_f
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 733
    iget-byte v0, p0, Lcom/google/maps/g/dw;->A:B

    .line 734
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 744
    :goto_0
    return v0

    .line 735
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 737
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 738
    iget-object v0, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 739
    iput-byte v2, p0, Lcom/google/maps/g/dw;->A:B

    move v0, v2

    .line 740
    goto :goto_0

    :cond_2
    move v0, v2

    .line 737
    goto :goto_1

    .line 743
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/g/dw;->A:B

    move v0, v1

    .line 744
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 827
    iget v0, p0, Lcom/google/maps/g/dw;->B:I

    .line 828
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 929
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 831
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 832
    iget-object v0, p0, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 833
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 831
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 835
    :cond_1
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 836
    iget v0, p0, Lcom/google/maps/g/dw;->c:I

    .line 837
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 839
    :cond_2
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_3

    .line 840
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    .line 841
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 843
    :cond_3
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 844
    iget v0, p0, Lcom/google/maps/g/dw;->f:I

    .line 845
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 847
    :cond_4
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 848
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/g/dw;->g:I

    .line 849
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_b

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 851
    :cond_5
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 852
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    .line 853
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 855
    :cond_6
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 856
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/maps/g/dw;->i:I

    .line 857
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_c

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 859
    :cond_7
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 860
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    .line 861
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_8
    move v1, v2

    .line 863
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 864
    const/16 v5, 0x9

    iget-object v0, p0, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 865
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 863
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_9
    move v0, v4

    .line 837
    goto/16 :goto_2

    :cond_a
    move v0, v4

    .line 845
    goto/16 :goto_3

    :cond_b
    move v0, v4

    .line 849
    goto :goto_4

    :cond_c
    move v0, v4

    .line 857
    goto :goto_5

    .line 867
    :cond_d
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_e

    .line 868
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->o:Z

    .line 869
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 871
    :cond_e
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 872
    const/16 v1, 0xd

    .line 873
    iget-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 875
    :cond_f
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_10

    .line 876
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->p:Z

    .line 877
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 879
    :cond_10
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_11

    .line 880
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    .line 881
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 883
    :cond_11
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_12

    .line 884
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->r:Z

    .line 885
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 887
    :cond_12
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_13

    .line 888
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->s:Z

    .line 889
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 891
    :cond_13
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_14

    .line 892
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->t:Z

    .line 893
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 895
    :cond_14
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_15

    .line 896
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    .line 897
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 899
    :cond_15
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_16

    .line 900
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    .line 901
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 903
    :cond_16
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_17

    .line 904
    const/16 v1, 0x15

    .line 905
    iget-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 907
    :cond_17
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_18

    .line 908
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->v:Z

    .line 909
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 911
    :cond_18
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_1a

    .line 912
    const/16 v0, 0x17

    iget v1, p0, Lcom/google/maps/g/dw;->w:I

    .line 913
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_19

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_19
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 915
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_1b

    .line 916
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    .line 917
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 919
    :cond_1b
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_1c

    .line 920
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/maps/g/dw;->y:Z

    .line 921
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 923
    :cond_1c
    iget v0, p0, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1d

    .line 924
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    .line 925
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 927
    :cond_1d
    iget-object v0, p0, Lcom/google/maps/g/dw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 928
    iput v0, p0, Lcom/google/maps/g/dw;->B:I

    goto/16 :goto_0

    .line 873
    :cond_1e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 905
    :cond_1f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/dw;->newBuilder()Lcom/google/maps/g/dy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/dy;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/dw;->newBuilder()Lcom/google/maps/g/dy;

    move-result-object v0

    return-object v0
.end method
