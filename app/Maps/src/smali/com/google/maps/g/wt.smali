.class public final Lcom/google/maps/g/wt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ww;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/wt;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/maps/g/wt;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/google/maps/g/wu;

    invoke-direct {v0}, Lcom/google/maps/g/wu;-><init>()V

    sput-object v0, Lcom/google/maps/g/wt;->PARSER:Lcom/google/n/ax;

    .line 226
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/wt;->f:Lcom/google/n/aw;

    .line 647
    new-instance v0, Lcom/google/maps/g/wt;

    invoke-direct {v0}, Lcom/google/maps/g/wt;-><init>()V

    sput-object v0, Lcom/google/maps/g/wt;->c:Lcom/google/maps/g/wt;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 183
    iput-byte v0, p0, Lcom/google/maps/g/wt;->d:B

    .line 205
    iput v0, p0, Lcom/google/maps/g/wt;->e:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/wt;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 34
    sparse-switch v1, :sswitch_data_0

    .line 39
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 41
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_7

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 49
    or-int/lit8 v1, v0, 0x1

    .line 51
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 52
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 51
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 53
    goto :goto_0

    .line 56
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_1

    .line 57
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    .line 59
    or-int/lit8 v0, v0, 0x2

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 61
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 68
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_2

    .line 74
    iget-object v2, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    .line 76
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 77
    iget-object v1, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    .line 79
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wt;->au:Lcom/google/n/bn;

    throw v0

    .line 73
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_5

    .line 74
    iget-object v1, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    .line 76
    :cond_5
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_6

    .line 77
    iget-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    .line 79
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wt;->au:Lcom/google/n/bn;

    .line 80
    return-void

    .line 69
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 70
    :goto_4
    :try_start_4
    new-instance v2, Lcom/google/n/ak;

    .line 71
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 73
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 69
    :catch_2
    move-exception v0

    goto :goto_4

    .line 67
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 183
    iput-byte v0, p0, Lcom/google/maps/g/wt;->d:B

    .line 205
    iput v0, p0, Lcom/google/maps/g/wt;->e:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/wt;
    .locals 1

    .prologue
    .line 650
    sget-object v0, Lcom/google/maps/g/wt;->c:Lcom/google/maps/g/wt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/wv;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/maps/g/wv;

    invoke-direct {v0}, Lcom/google/maps/g/wv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/wt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/google/maps/g/wt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 195
    invoke-virtual {p0}, Lcom/google/maps/g/wt;->c()I

    move v1, v2

    .line 196
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 197
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 196
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 199
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 199
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/wt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 203
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 185
    iget-byte v1, p0, Lcom/google/maps/g/wt;->d:B

    .line 186
    if-ne v1, v0, :cond_0

    .line 190
    :goto_0
    return v0

    .line 187
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/wt;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 207
    iget v0, p0, Lcom/google/maps/g/wt;->e:I

    .line 208
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 221
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 211
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 212
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/maps/g/wt;->a:Ljava/util/List;

    .line 213
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 211
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 215
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 216
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    .line 217
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 215
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/wt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 220
    iput v0, p0, Lcom/google/maps/g/wt;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/wt;->newBuilder()Lcom/google/maps/g/wv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/wv;->a(Lcom/google/maps/g/wt;)Lcom/google/maps/g/wv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/wt;->newBuilder()Lcom/google/maps/g/wv;

    move-result-object v0

    return-object v0
.end method
