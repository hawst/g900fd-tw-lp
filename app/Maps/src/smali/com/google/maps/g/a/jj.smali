.class public final enum Lcom/google/maps/g/a/jj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/jj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/jj;

.field public static final enum b:Lcom/google/maps/g/a/jj;

.field public static final enum c:Lcom/google/maps/g/a/jj;

.field public static final enum d:Lcom/google/maps/g/a/jj;

.field private static final synthetic f:[Lcom/google/maps/g/a/jj;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 259
    new-instance v0, Lcom/google/maps/g/a/jj;

    const-string v1, "QUERY_TYPE_FEATURE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/jj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jj;->a:Lcom/google/maps/g/a/jj;

    .line 263
    new-instance v0, Lcom/google/maps/g/a/jj;

    const-string v1, "QUERY_TYPE_REVERSE_GEOCODE"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/maps/g/a/jj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jj;->b:Lcom/google/maps/g/a/jj;

    .line 267
    new-instance v0, Lcom/google/maps/g/a/jj;

    const-string v1, "QUERY_TYPE_LAT_LNG"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/a/jj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jj;->c:Lcom/google/maps/g/a/jj;

    .line 271
    new-instance v0, Lcom/google/maps/g/a/jj;

    const-string v1, "QUERY_TYPE_USER_LOCATION"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/maps/g/a/jj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    .line 254
    new-array v0, v6, [Lcom/google/maps/g/a/jj;

    sget-object v1, Lcom/google/maps/g/a/jj;->a:Lcom/google/maps/g/a/jj;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/jj;->b:Lcom/google/maps/g/a/jj;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/jj;->c:Lcom/google/maps/g/a/jj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/a/jj;->f:[Lcom/google/maps/g/a/jj;

    .line 311
    new-instance v0, Lcom/google/maps/g/a/jk;

    invoke-direct {v0}, Lcom/google/maps/g/a/jk;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 321
    iput p3, p0, Lcom/google/maps/g/a/jj;->e:I

    .line 322
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/jj;
    .locals 1

    .prologue
    .line 297
    packed-switch p0, :pswitch_data_0

    .line 302
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 298
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/jj;->a:Lcom/google/maps/g/a/jj;

    goto :goto_0

    .line 299
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/jj;->b:Lcom/google/maps/g/a/jj;

    goto :goto_0

    .line 300
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/jj;->c:Lcom/google/maps/g/a/jj;

    goto :goto_0

    .line 301
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    goto :goto_0

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/jj;
    .locals 1

    .prologue
    .line 254
    const-class v0, Lcom/google/maps/g/a/jj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jj;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/jj;
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/google/maps/g/a/jj;->f:[Lcom/google/maps/g/a/jj;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/jj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/jj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/google/maps/g/a/jj;->e:I

    return v0
.end method
