.class public final Lcom/google/maps/g/f/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/f/d;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/f/a;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/f/a;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:Z

.field h:I

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/google/maps/g/f/b;

    invoke-direct {v0}, Lcom/google/maps/g/f/b;-><init>()V

    sput-object v0, Lcom/google/maps/g/f/a;->PARSER:Lcom/google/n/ax;

    .line 381
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/f/a;->m:Lcom/google/n/aw;

    .line 1010
    new-instance v0, Lcom/google/maps/g/f/a;

    invoke-direct {v0}, Lcom/google/maps/g/f/a;-><init>()V

    sput-object v0, Lcom/google/maps/g/f/a;->j:Lcom/google/maps/g/f/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 148
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    .line 180
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    .line 269
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    .line 284
    iput-byte v3, p0, Lcom/google/maps/g/f/a;->k:B

    .line 336
    iput v3, p0, Lcom/google/maps/g/f/a;->l:I

    .line 18
    iput v4, p0, Lcom/google/maps/g/f/a;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    .line 23
    iput-boolean v4, p0, Lcom/google/maps/g/f/a;->g:Z

    .line 24
    iput v2, p0, Lcom/google/maps/g/f/a;->h:I

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/f/a;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 53
    invoke-static {v0}, Lcom/google/maps/g/f/e;->a(I)Lcom/google/maps/g/f/e;

    move-result-object v5

    .line 54
    if-nez v5, :cond_1

    .line 55
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/f/a;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/f/a;->a:I

    .line 58
    iput v0, p0, Lcom/google/maps/g/f/a;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 108
    :catch_1
    move-exception v0

    .line 109
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 110
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/f/a;->a:I

    goto :goto_0

    .line 68
    :sswitch_3
    iget-object v0, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 69
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/f/a;->a:I

    goto :goto_0

    .line 73
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 74
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/f/a;->a:I

    goto :goto_0

    .line 78
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/f/a;->a:I

    goto/16 :goto_0

    .line 83
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 84
    iget v5, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/f/a;->a:I

    .line 85
    iput-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 89
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/f/a;->a:I

    .line 90
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/f/a;->g:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 94
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 95
    invoke-static {v0}, Lcom/google/maps/g/f/k;->a(I)Lcom/google/maps/g/f/k;

    move-result-object v5

    .line 96
    if-nez v5, :cond_3

    .line 97
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 99
    :cond_3
    iget v5, p0, Lcom/google/maps/g/f/a;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/f/a;->a:I

    .line 100
    iput v0, p0, Lcom/google/maps/g/f/a;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 112
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/f/a;->au:Lcom/google/n/bn;

    .line 113
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 148
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    .line 180
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    .line 269
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    .line 284
    iput-byte v1, p0, Lcom/google/maps/g/f/a;->k:B

    .line 336
    iput v1, p0, Lcom/google/maps/g/f/a;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/f/a;
    .locals 1

    .prologue
    .line 1013
    sget-object v0, Lcom/google/maps/g/f/a;->j:Lcom/google/maps/g/f/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/f/c;
    .locals 1

    .prologue
    .line 443
    new-instance v0, Lcom/google/maps/g/f/c;

    invoke-direct {v0}, Lcom/google/maps/g/f/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/f/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lcom/google/maps/g/f/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 308
    invoke-virtual {p0}, Lcom/google/maps/g/f/a;->c()I

    .line 309
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 310
    iget v0, p0, Lcom/google/maps/g/f/a;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 312
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1

    .line 313
    iget-object v0, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 315
    :cond_1
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 316
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 318
    :cond_2
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_3

    .line 319
    iget-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 321
    :cond_3
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 322
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 324
    :cond_4
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_5

    .line 325
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 327
    :cond_5
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_6

    .line 328
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/f/a;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 330
    :cond_6
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 331
    iget v0, p0, Lcom/google/maps/g/f/a;->h:I

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 333
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/f/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 334
    return-void

    .line 310
    :cond_8
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 325
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_a
    move v0, v2

    .line 328
    goto :goto_2

    .line 331
    :cond_b
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    iget-byte v0, p0, Lcom/google/maps/g/f/a;->k:B

    .line 287
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 303
    :goto_0
    return v0

    .line 288
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 290
    :cond_1
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 292
    iput-byte v2, p0, Lcom/google/maps/g/f/a;->k:B

    move v0, v2

    .line 293
    goto :goto_0

    :cond_2
    move v0, v2

    .line 290
    goto :goto_1

    .line 296
    :cond_3
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 298
    iput-byte v2, p0, Lcom/google/maps/g/f/a;->k:B

    move v0, v2

    .line 299
    goto :goto_0

    :cond_4
    move v0, v2

    .line 296
    goto :goto_2

    .line 302
    :cond_5
    iput-byte v1, p0, Lcom/google/maps/g/f/a;->k:B

    move v0, v1

    .line 303
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 338
    iget v0, p0, Lcom/google/maps/g/f/a;->l:I

    .line 339
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 376
    :goto_0
    return v0

    .line 342
    :cond_0
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 343
    iget v0, p0, Lcom/google/maps/g/f/a;->b:I

    .line 344
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 346
    :goto_2
    iget v2, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1

    .line 347
    iget-object v2, p0, Lcom/google/maps/g/f/a;->i:Lcom/google/n/ao;

    .line 348
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 350
    :cond_1
    iget v2, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 351
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/f/a;->d:Lcom/google/n/ao;

    .line 352
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 354
    :cond_2
    iget v2, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 355
    iget-object v2, p0, Lcom/google/maps/g/f/a;->e:Lcom/google/n/ao;

    .line 356
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 358
    :cond_3
    iget v2, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    .line 359
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/f/a;->c:Lcom/google/n/ao;

    .line 360
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 362
    :goto_3
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 363
    const/4 v3, 0x6

    .line 364
    iget-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/f/a;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 366
    :cond_4
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 367
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/g/f/a;->g:Z

    .line 368
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 370
    :cond_5
    iget v0, p0, Lcom/google/maps/g/f/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 371
    iget v0, p0, Lcom/google/maps/g/f/a;->h:I

    .line 372
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 374
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/f/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 375
    iput v0, p0, Lcom/google/maps/g/f/a;->l:I

    goto/16 :goto_0

    .line 344
    :cond_7
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 364
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 372
    :cond_9
    const/16 v0, 0xa

    goto :goto_5

    :cond_a
    move v2, v0

    goto :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/f/a;->newBuilder()Lcom/google/maps/g/f/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/f/c;->a(Lcom/google/maps/g/f/a;)Lcom/google/maps/g/f/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/f/a;->newBuilder()Lcom/google/maps/g/f/c;

    move-result-object v0

    return-object v0
.end method
