.class public final Lcom/google/maps/g/a/au;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/bd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/au;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/a/au;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:I

.field public d:I

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/f;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/maps/g/a/av;

    invoke-direct {v0}, Lcom/google/maps/g/a/av;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/au;->PARSER:Lcom/google/n/ax;

    .line 984
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/au;->n:Lcom/google/n/aw;

    .line 1659
    new-instance v0, Lcom/google/maps/g/a/au;

    invoke-direct {v0}, Lcom/google/maps/g/a/au;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/au;->k:Lcom/google/maps/g/a/au;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 750
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    .line 798
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    .line 830
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    .line 861
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    .line 877
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    .line 892
    iput-byte v3, p0, Lcom/google/maps/g/a/au;->l:B

    .line 935
    iput v3, p0, Lcom/google/maps/g/a/au;->m:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v4, p0, Lcom/google/maps/g/a/au;->c:I

    .line 20
    iput v4, p0, Lcom/google/maps/g/a/au;->d:I

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/maps/g/a/au;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 39
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 41
    sparse-switch v3, :sswitch_data_0

    .line 46
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 48
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 54
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/au;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 59
    invoke-static {v3}, Lcom/google/maps/g/a/bb;->a(I)Lcom/google/maps/g/a/bb;

    move-result-object v4

    .line 60
    if-nez v4, :cond_1

    .line 61
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 115
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/a/au;->a:I

    .line 64
    iput v3, p0, Lcom/google/maps/g/a/au;->c:I

    goto :goto_0

    .line 69
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 70
    invoke-static {v3}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v4

    .line 71
    if-nez v4, :cond_2

    .line 72
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 74
    :cond_2
    iget v4, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/a/au;->a:I

    .line 75
    iput v3, p0, Lcom/google/maps/g/a/au;->d:I

    goto :goto_0

    .line 80
    :sswitch_4
    iget-object v3, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 81
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I

    goto/16 :goto_0

    .line 85
    :sswitch_5
    iget-object v3, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 86
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I

    goto/16 :goto_0

    .line 90
    :sswitch_6
    iget-object v3, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 91
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I

    goto/16 :goto_0

    .line 95
    :sswitch_7
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    goto/16 :goto_0

    .line 100
    :sswitch_8
    iget-object v3, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 101
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I

    goto/16 :goto_0

    .line 105
    :sswitch_9
    iget-object v3, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 106
    iget v3, p0, Lcom/google/maps/g/a/au;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/a/au;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 117
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/au;->au:Lcom/google/n/bn;

    .line 118
    return-void

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 750
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    .line 798
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    .line 830
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    .line 861
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    .line 877
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    .line 892
    iput-byte v1, p0, Lcom/google/maps/g/a/au;->l:B

    .line 935
    iput v1, p0, Lcom/google/maps/g/a/au;->m:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;
    .locals 1

    .prologue
    .line 1049
    invoke-static {}, Lcom/google/maps/g/a/au;->newBuilder()Lcom/google/maps/g/a/aw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/au;
    .locals 1

    .prologue
    .line 1662
    sget-object v0, Lcom/google/maps/g/a/au;->k:Lcom/google/maps/g/a/au;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/aw;
    .locals 1

    .prologue
    .line 1046
    new-instance v0, Lcom/google/maps/g/a/aw;

    invoke-direct {v0}, Lcom/google/maps/g/a/aw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/maps/g/a/au;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 904
    invoke-virtual {p0}, Lcom/google/maps/g/a/au;->c()I

    .line 905
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 906
    iget-object v0, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 908
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 909
    iget v0, p0, Lcom/google/maps/g/a/au;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 911
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 912
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/a/au;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 914
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 915
    iget-object v0, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 917
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 918
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 920
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 921
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 923
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 924
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 926
    :cond_6
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 927
    iget-object v0, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 929
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 930
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 932
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/a/au;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 933
    return-void

    .line 909
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 912
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 894
    iget-byte v1, p0, Lcom/google/maps/g/a/au;->l:B

    .line 895
    if-ne v1, v0, :cond_0

    .line 899
    :goto_0
    return v0

    .line 896
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 898
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/au;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 937
    iget v0, p0, Lcom/google/maps/g/a/au;->m:I

    .line 938
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 979
    :goto_0
    return v0

    .line 941
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 942
    iget-object v0, p0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    .line 943
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 945
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 946
    iget v2, p0, Lcom/google/maps/g/a/au;->c:I

    .line 947
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 949
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 950
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/maps/g/a/au;->d:I

    .line 951
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 953
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 954
    iget-object v2, p0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    .line 955
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 957
    :cond_3
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 958
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    .line 959
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 961
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 962
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    .line 963
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 965
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 966
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    .line 967
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 969
    :cond_6
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 970
    iget-object v2, p0, Lcom/google/maps/g/a/au;->i:Lcom/google/n/ao;

    .line 971
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 973
    :cond_7
    iget v2, p0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_8

    .line 974
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/maps/g/a/au;->j:Lcom/google/n/ao;

    .line 975
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 977
    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/a/au;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 978
    iput v0, p0, Lcom/google/maps/g/a/au;->m:I

    goto/16 :goto_0

    .line 947
    :cond_9
    const/16 v2, 0xa

    goto/16 :goto_2

    .line 951
    :cond_a
    const/16 v2, 0xa

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/au;->newBuilder()Lcom/google/maps/g/a/aw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/aw;->a(Lcom/google/maps/g/a/au;)Lcom/google/maps/g/a/aw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/au;->newBuilder()Lcom/google/maps/g/a/aw;

    move-result-object v0

    return-object v0
.end method
