.class public final Lcom/google/maps/g/tw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ub;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/tu;",
        "Lcom/google/maps/g/tw;",
        ">;",
        "Lcom/google/maps/g/ub;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/maps/g/tx;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1131
    sget-object v0, Lcom/google/maps/g/tu;->d:Lcom/google/maps/g/tu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/tw;->b:Ljava/lang/Object;

    .line 1258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    .line 1132
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/tu;)Lcom/google/maps/g/tw;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1163
    invoke-static {}, Lcom/google/maps/g/tu;->d()Lcom/google/maps/g/tu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1173
    :goto_0
    return-object p0

    .line 1164
    :cond_0
    iget v2, p1, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1165
    iget v2, p0, Lcom/google/maps/g/tw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/tw;->a:I

    .line 1166
    iget-object v2, p1, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/tw;->b:Ljava/lang/Object;

    .line 1169
    :cond_1
    iget v2, p1, Lcom/google/maps/g/tu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 1170
    iget-object v0, p1, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v0

    :goto_3
    iget v1, p0, Lcom/google/maps/g/tw;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    invoke-static {v1}, Lcom/google/maps/g/tx;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/tz;->a(Lcom/google/maps/g/tx;)Lcom/google/maps/g/tz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/tz;->c()Lcom/google/maps/g/tx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    :goto_4
    iget v0, p0, Lcom/google/maps/g/tw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/tw;->a:I

    .line 1172
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/tu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1164
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1169
    goto :goto_2

    .line 1170
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    goto :goto_3

    :cond_6
    iput-object v0, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1123
    new-instance v2, Lcom/google/maps/g/tu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/tu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/tw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/tw;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/tw;->c:Lcom/google/maps/g/tx;

    iput-object v1, v2, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    iput v0, v2, Lcom/google/maps/g/tu;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1123
    check-cast p1, Lcom/google/maps/g/tu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/tw;->a(Lcom/google/maps/g/tu;)Lcom/google/maps/g/tw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1177
    const/4 v0, 0x1

    return v0
.end method
