.class public final Lcom/google/maps/g/uh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/uo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/uf;",
        "Lcom/google/maps/g/uh;",
        ">;",
        "Lcom/google/maps/g/uo;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 931
    sget-object v0, Lcom/google/maps/g/uf;->i:Lcom/google/maps/g/uf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1049
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uh;->b:Lcom/google/n/ao;

    .line 1108
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/uh;->c:Lcom/google/n/ao;

    .line 1167
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/uh;->d:Ljava/lang/Object;

    .line 1276
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    .line 1413
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    .line 1549
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/uh;->h:I

    .line 932
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/uf;)Lcom/google/maps/g/uh;
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 999
    invoke-static {}, Lcom/google/maps/g/uf;->h()Lcom/google/maps/g/uf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1040
    :goto_0
    return-object p0

    .line 1000
    :cond_0
    iget v2, p1, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1001
    iget-object v2, p0, Lcom/google/maps/g/uh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1002
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1004
    :cond_1
    iget v2, p1, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1005
    iget-object v2, p0, Lcom/google/maps/g/uh;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1006
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1008
    :cond_2
    iget v2, p1, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1009
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1010
    iget-object v2, p1, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/uh;->d:Ljava/lang/Object;

    .line 1013
    :cond_3
    iget v2, p1, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1014
    iget v2, p1, Lcom/google/maps/g/uf;->e:I

    iget v3, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/uh;->a:I

    iput v2, p0, Lcom/google/maps/g/uh;->e:I

    .line 1016
    :cond_4
    iget-object v2, p1, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1017
    iget-object v2, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1018
    iget-object v2, p1, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    .line 1019
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1026
    :cond_5
    :goto_5
    iget-object v2, p1, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1027
    iget-object v2, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1028
    iget-object v2, p1, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    .line 1029
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1036
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v4, :cond_10

    :goto_7
    if-eqz v0, :cond_12

    .line 1037
    iget v0, p1, Lcom/google/maps/g/uf;->h:I

    invoke-static {v0}, Lcom/google/maps/g/um;->a(I)Lcom/google/maps/g/um;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/maps/g/um;->a:Lcom/google/maps/g/um;

    :cond_7
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 1000
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 1004
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 1008
    goto :goto_3

    :cond_b
    move v2, v1

    .line 1013
    goto :goto_4

    .line 1021
    :cond_c
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v4, :cond_d

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1022
    :cond_d
    iget-object v2, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 1031
    :cond_e
    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-eq v2, v3, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/uh;->a:I

    .line 1032
    :cond_f
    iget-object v2, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_10
    move v0, v1

    .line 1036
    goto :goto_7

    .line 1037
    :cond_11
    iget v1, p0, Lcom/google/maps/g/uh;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/maps/g/uh;->a:I

    iget v0, v0, Lcom/google/maps/g/um;->d:I

    iput v0, p0, Lcom/google/maps/g/uh;->h:I

    .line 1039
    :cond_12
    iget-object v0, p1, Lcom/google/maps/g/uf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 923
    new-instance v2, Lcom/google/maps/g/uf;

    invoke-direct {v2, p0}, Lcom/google/maps/g/uf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/uh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/uh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/uf;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/uh;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/uh;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/uh;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/uf;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/uh;->e:I

    iput v1, v2, Lcom/google/maps/g/uf;->e:I

    iget v1, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/maps/g/uh;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/uh;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/uf;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/uh;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/maps/g/uh;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/uh;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/uf;->g:Ljava/util/List;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v1, p0, Lcom/google/maps/g/uh;->h:I

    iput v1, v2, Lcom/google/maps/g/uf;->h:I

    iput v0, v2, Lcom/google/maps/g/uf;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 923
    check-cast p1, Lcom/google/maps/g/uf;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/uh;->a(Lcom/google/maps/g/uf;)Lcom/google/maps/g/uh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1044
    const/4 v0, 0x1

    return v0
.end method
