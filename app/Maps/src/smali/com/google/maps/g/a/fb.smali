.class public final enum Lcom/google/maps/g/a/fb;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/fb;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/fb;

.field public static final enum b:Lcom/google/maps/g/a/fb;

.field public static final enum c:Lcom/google/maps/g/a/fb;

.field private static final synthetic e:[Lcom/google/maps/g/a/fb;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 662
    new-instance v0, Lcom/google/maps/g/a/fb;

    const-string v1, "SIDE_LEFT"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/a/fb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    .line 666
    new-instance v0, Lcom/google/maps/g/a/fb;

    const-string v1, "SIDE_RIGHT"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/fb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    .line 670
    new-instance v0, Lcom/google/maps/g/a/fb;

    const-string v1, "SIDE_UNSPECIFIED"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/g/a/fb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    .line 657
    new-array v0, v5, [Lcom/google/maps/g/a/fb;

    sget-object v1, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/a/fb;->e:[Lcom/google/maps/g/a/fb;

    .line 705
    new-instance v0, Lcom/google/maps/g/a/fc;

    invoke-direct {v0}, Lcom/google/maps/g/a/fc;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 714
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 715
    iput p3, p0, Lcom/google/maps/g/a/fb;->d:I

    .line 716
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 692
    packed-switch p0, :pswitch_data_0

    .line 696
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 693
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    goto :goto_0

    .line 694
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    goto :goto_0

    .line 695
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    goto :goto_0

    .line 692
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 657
    const-class v0, Lcom/google/maps/g/a/fb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fb;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 657
    sget-object v0, Lcom/google/maps/g/a/fb;->e:[Lcom/google/maps/g/a/fb;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/fb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/fb;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 688
    iget v0, p0, Lcom/google/maps/g/a/fb;->d:I

    return v0
.end method
