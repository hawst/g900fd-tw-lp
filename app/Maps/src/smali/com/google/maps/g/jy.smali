.class public final Lcom/google/maps/g/jy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/jy;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/jy;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/google/maps/g/jz;

    invoke-direct {v0}, Lcom/google/maps/g/jz;-><init>()V

    sput-object v0, Lcom/google/maps/g/jy;->PARSER:Lcom/google/n/ax;

    .line 332
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/jy;->h:Lcom/google/n/aw;

    .line 812
    new-instance v0, Lcom/google/maps/g/jy;

    invoke-direct {v0}, Lcom/google/maps/g/jy;-><init>()V

    sput-object v0, Lcom/google/maps/g/jy;->e:Lcom/google/maps/g/jy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 270
    iput-byte v0, p0, Lcom/google/maps/g/jy;->f:B

    .line 307
    iput v0, p0, Lcom/google/maps/g/jy;->g:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/jy;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/maps/g/jy;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 35
    sparse-switch v4, :sswitch_data_0

    .line 40
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 48
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 50
    or-int/lit8 v1, v1, 0x1

    .line 52
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 52
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 86
    iget-object v2, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 88
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 89
    iget-object v1, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 91
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/jy;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_4

    .line 58
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 60
    or-int/lit8 v1, v1, 0x2

    .line 62
    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 62
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 81
    :catch_1
    move-exception v0

    .line 82
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 68
    invoke-static {v4}, Lcom/google/maps/g/ka;->a(I)Lcom/google/maps/g/ka;

    move-result-object v5

    .line 69
    if-nez v5, :cond_5

    .line 70
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 72
    :cond_5
    iget v5, p0, Lcom/google/maps/g/jy;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/jy;->a:I

    .line 73
    iput v4, p0, Lcom/google/maps/g/jy;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 85
    :cond_6
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_7

    .line 86
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 88
    :cond_7
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_8

    .line 89
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 91
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/jy;->au:Lcom/google/n/bn;

    .line 92
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 270
    iput-byte v0, p0, Lcom/google/maps/g/jy;->f:B

    .line 307
    iput v0, p0, Lcom/google/maps/g/jy;->g:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;
    .locals 1

    .prologue
    .line 397
    invoke-static {}, Lcom/google/maps/g/jy;->newBuilder()Lcom/google/maps/g/kc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/maps/g/jy;
    .locals 1

    .prologue
    .line 815
    sget-object v0, Lcom/google/maps/g/jy;->e:Lcom/google/maps/g/jy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/kc;
    .locals 1

    .prologue
    .line 394
    new-instance v0, Lcom/google/maps/g/kc;

    invoke-direct {v0}, Lcom/google/maps/g/kc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/jy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lcom/google/maps/g/jy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 294
    invoke-virtual {p0}, Lcom/google/maps/g/jy;->c()I

    move v1, v2

    .line 295
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 295
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 298
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 298
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 301
    :cond_1
    iget v0, p0, Lcom/google/maps/g/jy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 302
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/jy;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 304
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/jy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 305
    return-void

    .line 302
    :cond_3
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272
    iget-byte v0, p0, Lcom/google/maps/g/jy;->f:B

    .line 273
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 289
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 276
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 277
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/w;

    invoke-virtual {v0}, Lcom/google/maps/b/w;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 278
    iput-byte v2, p0, Lcom/google/maps/g/jy;->f:B

    goto :goto_0

    .line 276
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 282
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 283
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/dc;->d()Lcom/google/maps/b/dc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/dc;

    invoke-virtual {v0}, Lcom/google/maps/b/dc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 284
    iput-byte v2, p0, Lcom/google/maps/g/jy;->f:B

    goto :goto_0

    .line 282
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 288
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/g/jy;->f:B

    move v2, v3

    .line 289
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 309
    iget v0, p0, Lcom/google/maps/g/jy;->g:I

    .line 310
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 327
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 313
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 315
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 313
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 317
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 318
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 319
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 317
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 321
    :cond_2
    iget v0, p0, Lcom/google/maps/g/jy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 322
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/jy;->d:I

    .line 323
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/jy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 326
    iput v0, p0, Lcom/google/maps/g/jy;->g:I

    goto :goto_0

    .line 323
    :cond_4
    const/16 v0, 0xa

    goto :goto_3
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/b/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 176
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 177
    iget-object v0, p0, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 178
    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/w;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/jy;->newBuilder()Lcom/google/maps/g/kc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/jy;->newBuilder()Lcom/google/maps/g/kc;

    move-result-object v0

    return-object v0
.end method
