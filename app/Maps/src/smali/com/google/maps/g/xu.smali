.class public final Lcom/google/maps/g/xu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/xs;",
        "Lcom/google/maps/g/xu;",
        ">;",
        "Lcom/google/maps/g/xv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 635
    sget-object v0, Lcom/google/maps/g/xs;->j:Lcom/google/maps/g/xs;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 760
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->b:Ljava/lang/Object;

    .line 836
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->c:Ljava/lang/Object;

    .line 912
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->d:Ljava/lang/Object;

    .line 989
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    .line 1125
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->f:Ljava/lang/Object;

    .line 1201
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->g:Ljava/lang/Object;

    .line 1277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->h:Ljava/lang/Object;

    .line 1353
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xu;->i:Ljava/lang/Object;

    .line 636
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 704
    invoke-static {}, Lcom/google/maps/g/xs;->d()Lcom/google/maps/g/xs;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 751
    :goto_0
    return-object p0

    .line 705
    :cond_0
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 706
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 707
    iget-object v2, p1, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->b:Ljava/lang/Object;

    .line 710
    :cond_1
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 711
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 712
    iget-object v2, p1, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->c:Ljava/lang/Object;

    .line 715
    :cond_2
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 716
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 717
    iget-object v2, p1, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->d:Ljava/lang/Object;

    .line 720
    :cond_3
    iget-object v2, p1, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 721
    iget-object v2, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 722
    iget-object v2, p1, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    .line 723
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 730
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 731
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 732
    iget-object v2, p1, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->f:Ljava/lang/Object;

    .line 735
    :cond_5
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 736
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 737
    iget-object v2, p1, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->g:Ljava/lang/Object;

    .line 740
    :cond_6
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 741
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 742
    iget-object v2, p1, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/xu;->h:Ljava/lang/Object;

    .line 745
    :cond_7
    iget v2, p1, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 746
    iget v0, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/xu;->a:I

    .line 747
    iget-object v0, p1, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/xu;->i:Ljava/lang/Object;

    .line 750
    :cond_8
    iget-object v0, p1, Lcom/google/maps/g/xs;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 705
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 710
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 715
    goto/16 :goto_3

    .line 725
    :cond_c
    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_d

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/xu;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/xu;->a:I

    .line 726
    :cond_d
    iget-object v2, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 730
    goto :goto_5

    :cond_f
    move v2, v1

    .line 735
    goto :goto_6

    :cond_10
    move v2, v1

    .line 740
    goto :goto_7

    :cond_11
    move v0, v1

    .line 745
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/google/maps/g/xu;->c()Lcom/google/maps/g/xs;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 627
    check-cast p1, Lcom/google/maps/g/xs;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/xu;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 755
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/xs;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 663
    new-instance v2, Lcom/google/maps/g/xs;

    invoke-direct {v2, p0}, Lcom/google/maps/g/xs;-><init>(Lcom/google/n/v;)V

    .line 664
    iget v3, p0, Lcom/google/maps/g/xu;->a:I

    .line 665
    const/4 v1, 0x0

    .line 666
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 669
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/xu;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    .line 670
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 671
    or-int/lit8 v0, v0, 0x2

    .line 673
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/xu;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    .line 674
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 675
    or-int/lit8 v0, v0, 0x4

    .line 677
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/xu;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    .line 678
    iget v1, p0, Lcom/google/maps/g/xu;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 679
    iget-object v1, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    .line 680
    iget v1, p0, Lcom/google/maps/g/xu;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/xu;->a:I

    .line 682
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/xu;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 683
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 684
    or-int/lit8 v0, v0, 0x8

    .line 686
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/xu;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    .line 687
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 688
    or-int/lit8 v0, v0, 0x10

    .line 690
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/xu;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    .line 691
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 692
    or-int/lit8 v0, v0, 0x20

    .line 694
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/xu;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    .line 695
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 696
    or-int/lit8 v0, v0, 0x40

    .line 698
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/xu;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    .line 699
    iput v0, v2, Lcom/google/maps/g/xs;->a:I

    .line 700
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method
