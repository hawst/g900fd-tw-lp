.class public final Lcom/google/maps/g/a/ja;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/jd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ja;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/a/ja;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field d:Z

.field e:Z

.field f:Z

.field g:Lcom/google/n/ao;

.field h:I

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/maps/g/a/jb;

    invoke-direct {v0}, Lcom/google/maps/g/a/jb;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ja;->PARSER:Lcom/google/n/ax;

    .line 363
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ja;->n:Lcom/google/n/aw;

    .line 1001
    new-instance v0, Lcom/google/maps/g/a/ja;

    invoke-direct {v0}, Lcom/google/maps/g/a/ja;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ja;->k:Lcom/google/maps/g/a/ja;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    .line 147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    .line 240
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    .line 256
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    .line 271
    iput-byte v4, p0, Lcom/google/maps/g/a/ja;->l:B

    .line 314
    iput v4, p0, Lcom/google/maps/g/a/ja;->m:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput-boolean v3, p0, Lcom/google/maps/g/a/ja;->d:Z

    .line 21
    iput-boolean v3, p0, Lcom/google/maps/g/a/ja;->e:Z

    .line 22
    iput-boolean v3, p0, Lcom/google/maps/g/a/ja;->f:Z

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput v3, p0, Lcom/google/maps/g/a/ja;->h:I

    .line 25
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/maps/g/a/ja;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 39
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 46
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 48
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 54
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ja;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 59
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 108
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 109
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/ja;->d:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 68
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/g/a/ja;->e:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 73
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/maps/g/a/ja;->f:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 78
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I

    goto/16 :goto_0

    .line 83
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 84
    invoke-static {v0}, Lcom/google/maps/g/ud;->a(I)Lcom/google/maps/g/ud;

    move-result-object v5

    .line 85
    if-nez v5, :cond_4

    .line 86
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 88
    :cond_4
    iget v5, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/a/ja;->a:I

    .line 89
    iput v0, p0, Lcom/google/maps/g/a/ja;->h:I

    goto/16 :goto_0

    .line 94
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 95
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I

    goto/16 :goto_0

    .line 99
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 100
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/ja;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 111
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->au:Lcom/google/n/bn;

    .line 112
    return-void

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    .line 147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    .line 240
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    .line 256
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    .line 271
    iput-byte v1, p0, Lcom/google/maps/g/a/ja;->l:B

    .line 314
    iput v1, p0, Lcom/google/maps/g/a/ja;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/ja;
    .locals 1

    .prologue
    .line 1004
    sget-object v0, Lcom/google/maps/g/a/ja;->k:Lcom/google/maps/g/a/ja;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/jc;
    .locals 1

    .prologue
    .line 425
    new-instance v0, Lcom/google/maps/g/a/jc;

    invoke-direct {v0}, Lcom/google/maps/g/a/jc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ja;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/google/maps/g/a/ja;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 283
    invoke-virtual {p0}, Lcom/google/maps/g/a/ja;->c()I

    .line 284
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 287
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 288
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 290
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 291
    iget-boolean v0, p0, Lcom/google/maps/g/a/ja;->d:Z

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 293
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 294
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/a/ja;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 296
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 297
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/a/ja;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_b

    :goto_2
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 299
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 300
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 302
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 303
    iget v0, p0, Lcom/google/maps/g/a/ja;->h:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 305
    :cond_6
    :goto_3
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 306
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 308
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 309
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 311
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 312
    return-void

    :cond_9
    move v0, v2

    .line 291
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 294
    goto/16 :goto_1

    :cond_b
    move v1, v2

    .line 297
    goto/16 :goto_2

    .line 303
    :cond_c
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 273
    iget-byte v1, p0, Lcom/google/maps/g/a/ja;->l:B

    .line 274
    if-ne v1, v0, :cond_0

    .line 278
    :goto_0
    return v0

    .line 275
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 277
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/ja;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 316
    iget v0, p0, Lcom/google/maps/g/a/ja;->m:I

    .line 317
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 358
    :goto_0
    return v0

    .line 320
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 321
    iget-object v0, p0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    .line 322
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 324
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 325
    iget-object v2, p0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    .line 326
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 328
    :cond_1
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 329
    iget-boolean v2, p0, Lcom/google/maps/g/a/ja;->d:Z

    .line 330
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 332
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 333
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/a/ja;->e:Z

    .line 334
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 336
    :cond_3
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 337
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/a/ja;->f:Z

    .line 338
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 340
    :cond_4
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 341
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/maps/g/a/ja;->g:Lcom/google/n/ao;

    .line 342
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 344
    :cond_5
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 345
    iget v2, p0, Lcom/google/maps/g/a/ja;->h:I

    .line 346
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 348
    :cond_6
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 349
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/maps/g/a/ja;->i:Lcom/google/n/ao;

    .line 350
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 352
    :cond_7
    iget v2, p0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_8

    .line 353
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/maps/g/a/ja;->j:Lcom/google/n/ao;

    .line 354
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 356
    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/a/ja;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    iput v0, p0, Lcom/google/maps/g/a/ja;->m:I

    goto/16 :goto_0

    .line 346
    :cond_9
    const/16 v2, 0xa

    goto :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ja;->newBuilder()Lcom/google/maps/g/a/jc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/jc;->a(Lcom/google/maps/g/a/ja;)Lcom/google/maps/g/a/jc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ja;->newBuilder()Lcom/google/maps/g/a/jc;

    move-result-object v0

    return-object v0
.end method
