.class public final Lcom/google/maps/g/a/eg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ej;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ee;",
        "Lcom/google/maps/g/a/eg;",
        ">;",
        "Lcom/google/maps/g/a/ej;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 474
    sget-object v0, Lcom/google/maps/g/a/ee;->e:Lcom/google/maps/g/a/ee;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 538
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/eg;->b:I

    .line 574
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eg;->c:Lcom/google/n/ao;

    .line 633
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/eg;->d:Lcom/google/n/ao;

    .line 475
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ee;)Lcom/google/maps/g/a/eg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 516
    invoke-static {}, Lcom/google/maps/g/a/ee;->d()Lcom/google/maps/g/a/ee;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 529
    :goto_0
    return-object p0

    .line 517
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 518
    iget v2, p1, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 517
    goto :goto_1

    .line 518
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/eg;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/eg;->a:I

    iget v2, v2, Lcom/google/maps/g/a/eh;->s:I

    iput v2, p0, Lcom/google/maps/g/a/eg;->b:I

    .line 520
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 521
    iget-object v2, p0, Lcom/google/maps/g/a/eg;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 522
    iget v2, p0, Lcom/google/maps/g/a/eg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/eg;->a:I

    .line 524
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_6

    .line 525
    iget-object v0, p0, Lcom/google/maps/g/a/eg;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 526
    iget v0, p0, Lcom/google/maps/g/a/eg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/eg;->a:I

    .line 528
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/a/ee;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v2, v1

    .line 520
    goto :goto_2

    :cond_8
    move v0, v1

    .line 524
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 466
    new-instance v2, Lcom/google/maps/g/a/ee;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ee;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/eg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v4, p0, Lcom/google/maps/g/a/eg;->b:I

    iput v4, v2, Lcom/google/maps/g/a/ee;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/eg;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/eg;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/a/eg;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/a/eg;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/ee;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 466
    check-cast p1, Lcom/google/maps/g/a/ee;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/eg;->a(Lcom/google/maps/g/a/ee;)Lcom/google/maps/g/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x1

    return v0
.end method
