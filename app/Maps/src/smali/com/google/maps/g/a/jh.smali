.class public final enum Lcom/google/maps/g/a/jh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/jh;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/jh;

.field public static final enum b:Lcom/google/maps/g/a/jh;

.field public static final enum c:Lcom/google/maps/g/a/jh;

.field public static final enum d:Lcom/google/maps/g/a/jh;

.field public static final enum e:Lcom/google/maps/g/a/jh;

.field private static final synthetic g:[Lcom/google/maps/g/a/jh;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 174
    new-instance v0, Lcom/google/maps/g/a/jh;

    const-string v1, "ENTITY_TYPE_MY_LOCATION"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/jh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    .line 178
    new-instance v0, Lcom/google/maps/g/a/jh;

    const-string v1, "ENTITY_TYPE_HOME"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/jh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jh;->b:Lcom/google/maps/g/a/jh;

    .line 182
    new-instance v0, Lcom/google/maps/g/a/jh;

    const-string v1, "ENTITY_TYPE_WORK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/jh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jh;->c:Lcom/google/maps/g/a/jh;

    .line 186
    new-instance v0, Lcom/google/maps/g/a/jh;

    const-string v1, "ENTITY_TYPE_AD"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/jh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jh;->d:Lcom/google/maps/g/a/jh;

    .line 190
    new-instance v0, Lcom/google/maps/g/a/jh;

    const-string v1, "ENTITY_TYPE_DEFAULT"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/jh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    .line 169
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/a/jh;

    sget-object v1, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/jh;->b:Lcom/google/maps/g/a/jh;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/jh;->c:Lcom/google/maps/g/a/jh;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/jh;->d:Lcom/google/maps/g/a/jh;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/g/a/jh;->g:[Lcom/google/maps/g/a/jh;

    .line 235
    new-instance v0, Lcom/google/maps/g/a/ji;

    invoke-direct {v0}, Lcom/google/maps/g/a/ji;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 245
    iput p3, p0, Lcom/google/maps/g/a/jh;->f:I

    .line 246
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/jh;
    .locals 1

    .prologue
    .line 220
    packed-switch p0, :pswitch_data_0

    .line 226
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 221
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    goto :goto_0

    .line 222
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/jh;->b:Lcom/google/maps/g/a/jh;

    goto :goto_0

    .line 223
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/jh;->c:Lcom/google/maps/g/a/jh;

    goto :goto_0

    .line 224
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/jh;->d:Lcom/google/maps/g/a/jh;

    goto :goto_0

    .line 225
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/jh;
    .locals 1

    .prologue
    .line 169
    const-class v0, Lcom/google/maps/g/a/jh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jh;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/jh;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/google/maps/g/a/jh;->g:[Lcom/google/maps/g/a/jh;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/jh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/jh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/google/maps/g/a/jh;->f:I

    return v0
.end method
