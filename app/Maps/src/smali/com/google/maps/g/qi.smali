.class public final Lcom/google/maps/g/qi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ql;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/qd;",
        "Lcom/google/maps/g/qi;",
        ">;",
        "Lcom/google/maps/g/ql;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/aq;

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1295
    sget-object v0, Lcom/google/maps/g/qd;->o:Lcom/google/maps/g/qd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    .line 1560
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qi;->c:Lcom/google/n/ao;

    .line 1619
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/maps/g/qi;->d:I

    .line 1656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    .line 1729
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/qi;->f:Ljava/lang/Object;

    .line 1805
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/qi;->g:I

    .line 1841
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qi;->h:Lcom/google/n/ao;

    .line 1901
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    .line 1974
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/qi;->j:Lcom/google/n/ao;

    .line 2033
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    .line 1296
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qd;)Lcom/google/maps/g/qi;
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1403
    invoke-static {}, Lcom/google/maps/g/qd;->d()Lcom/google/maps/g/qd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1477
    :goto_0
    return-object p0

    .line 1404
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1405
    iget-object v2, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1406
    iget-object v2, p1, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    .line 1407
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1414
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1415
    iget-object v2, p0, Lcom/google/maps/g/qi;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1416
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1418
    :cond_2
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 1419
    iget v2, p1, Lcom/google/maps/g/qd;->d:I

    invoke-static {v2}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    :cond_3
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1409
    :cond_4
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1410
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 1414
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1418
    goto :goto_3

    .line 1419
    :cond_8
    iget v3, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/qi;->a:I

    iget v2, v2, Lcom/google/maps/g/pv;->e:I

    iput v2, p0, Lcom/google/maps/g/qi;->d:I

    .line 1421
    :cond_9
    iget-object v2, p1, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1422
    iget-object v2, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1423
    iget-object v2, p1, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    .line 1424
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1431
    :cond_a
    :goto_4
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 1432
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1433
    iget-object v2, p1, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/qi;->f:Ljava/lang/Object;

    .line 1436
    :cond_b
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_12

    .line 1437
    iget v2, p1, Lcom/google/maps/g/qd;->g:I

    invoke-static {v2}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v2

    if-nez v2, :cond_c

    sget-object v2, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_c
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1426
    :cond_d
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1427
    :cond_e
    iget-object v2, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_f
    move v2, v1

    .line 1431
    goto :goto_5

    :cond_10
    move v2, v1

    .line 1436
    goto :goto_6

    .line 1437
    :cond_11
    iget v3, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/qi;->a:I

    iget v2, v2, Lcom/google/maps/g/pz;->f:I

    iput v2, p0, Lcom/google/maps/g/qi;->g:I

    .line 1439
    :cond_12
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_7
    if-eqz v2, :cond_13

    .line 1440
    iget-object v2, p0, Lcom/google/maps/g/qi;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1441
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1443
    :cond_13
    iget-object v2, p1, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 1444
    iget-object v2, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1445
    iget-object v2, p1, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    .line 1446
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1453
    :cond_14
    :goto_8
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_9
    if-eqz v2, :cond_15

    .line 1454
    iget-object v2, p0, Lcom/google/maps/g/qi;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1455
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1457
    :cond_15
    iget-object v2, p1, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1458
    iget-object v2, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1459
    iget-object v2, p1, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    .line 1460
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1467
    :cond_16
    :goto_a
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_b
    if-eqz v2, :cond_17

    .line 1468
    iget v2, p1, Lcom/google/maps/g/qd;->l:I

    iget v3, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/g/qi;->a:I

    iput v2, p0, Lcom/google/maps/g/qi;->l:I

    .line 1470
    :cond_17
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_21

    move v2, v0

    :goto_c
    if-eqz v2, :cond_18

    .line 1471
    iget v2, p1, Lcom/google/maps/g/qd;->m:I

    iget v3, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/g/qi;->a:I

    iput v2, p0, Lcom/google/maps/g/qi;->m:I

    .line 1473
    :cond_18
    iget v2, p1, Lcom/google/maps/g/qd;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_22

    :goto_d
    if-eqz v0, :cond_19

    .line 1474
    iget v0, p1, Lcom/google/maps/g/qd;->n:I

    iget v1, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/maps/g/qi;->a:I

    iput v0, p0, Lcom/google/maps/g/qi;->n:I

    .line 1476
    :cond_19
    iget-object v0, p1, Lcom/google/maps/g/qd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1a
    move v2, v1

    .line 1439
    goto/16 :goto_7

    .line 1448
    :cond_1b
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v5, :cond_1c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1449
    :cond_1c
    iget-object v2, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_1d
    move v2, v1

    .line 1453
    goto/16 :goto_9

    .line 1462
    :cond_1e
    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-eq v2, v3, :cond_1f

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/qi;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/qi;->a:I

    .line 1463
    :cond_1f
    iget-object v2, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_20
    move v2, v1

    .line 1467
    goto/16 :goto_b

    :cond_21
    move v2, v1

    .line 1470
    goto :goto_c

    :cond_22
    move v0, v1

    .line 1473
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1287
    new-instance v2, Lcom/google/maps/g/qd;

    invoke-direct {v2, p0}, Lcom/google/maps/g/qd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/qi;->a:I

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/qi;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/qi;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/qd;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/qd;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qi;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qi;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/maps/g/qi;->d:I

    iput v4, v2, Lcom/google/maps/g/qd;->d:I

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/maps/g/qi;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/qi;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/qd;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/qi;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/qd;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget v4, p0, Lcom/google/maps/g/qi;->g:I

    iput v4, v2, Lcom/google/maps/g/qd;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/qd;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qi;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qi;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/maps/g/qi;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/qi;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/qd;->i:Ljava/util/List;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/qd;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/qi;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/qi;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/qi;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/g/qi;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/qi;->k:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/qd;->k:Lcom/google/n/aq;

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit8 v0, v0, 0x40

    :cond_9
    iget v1, p0, Lcom/google/maps/g/qi;->l:I

    iput v1, v2, Lcom/google/maps/g/qd;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x80

    :cond_a
    iget v1, p0, Lcom/google/maps/g/qi;->m:I

    iput v1, v2, Lcom/google/maps/g/qd;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x100

    :cond_b
    iget v1, p0, Lcom/google/maps/g/qi;->n:I

    iput v1, v2, Lcom/google/maps/g/qd;->n:I

    iput v0, v2, Lcom/google/maps/g/qd;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1287
    check-cast p1, Lcom/google/maps/g/qd;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/qi;->a(Lcom/google/maps/g/qd;)Lcom/google/maps/g/qi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1481
    const/4 v0, 0x1

    return v0
.end method
