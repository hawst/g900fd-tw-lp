.class public final Lcom/google/maps/g/ci;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/cg;",
        "Lcom/google/maps/g/ci;",
        ">;",
        "Lcom/google/maps/g/cj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/google/maps/g/cg;->e:Lcom/google/maps/g/cg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 359
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ci;->d:Ljava/lang/Object;

    .line 435
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ci;->b:Lcom/google/n/ao;

    .line 494
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ci;->c:Lcom/google/n/ao;

    .line 294
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/cg;)Lcom/google/maps/g/ci;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 335
    invoke-static {}, Lcom/google/maps/g/cg;->d()Lcom/google/maps/g/cg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 350
    :goto_0
    return-object p0

    .line 336
    :cond_0
    iget v2, p1, Lcom/google/maps/g/cg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 337
    iget v2, p0, Lcom/google/maps/g/ci;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ci;->a:I

    .line 338
    iget-object v2, p1, Lcom/google/maps/g/cg;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ci;->d:Ljava/lang/Object;

    .line 341
    :cond_1
    iget v2, p1, Lcom/google/maps/g/cg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 342
    iget-object v2, p0, Lcom/google/maps/g/ci;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/cg;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 343
    iget v2, p0, Lcom/google/maps/g/ci;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ci;->a:I

    .line 345
    :cond_2
    iget v2, p1, Lcom/google/maps/g/cg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 346
    iget-object v0, p0, Lcom/google/maps/g/ci;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/cg;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 347
    iget v0, p0, Lcom/google/maps/g/ci;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/ci;->a:I

    .line 349
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/cg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 336
    goto :goto_1

    :cond_5
    move v2, v1

    .line 341
    goto :goto_2

    :cond_6
    move v0, v1

    .line 345
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 285
    new-instance v2, Lcom/google/maps/g/cg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/cg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ci;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/ci;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/cg;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/cg;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ci;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ci;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/maps/g/cg;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/ci;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/ci;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/cg;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 285
    check-cast p1, Lcom/google/maps/g/cg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ci;->a(Lcom/google/maps/g/cg;)Lcom/google/maps/g/ci;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x1

    return v0
.end method
