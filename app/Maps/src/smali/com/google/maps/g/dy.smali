.class public final Lcom/google/maps/g/dy;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/dz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/dw;",
        "Lcom/google/maps/g/dy;",
        ">;",
        "Lcom/google/maps/g/dz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I

.field private g:I

.field private h:Lcom/google/n/ao;

.field private i:I

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/Object;

.field private n:Ljava/lang/Object;

.field private o:Z

.field private p:Z

.field private q:Lcom/google/n/ao;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/google/n/ao;

.field private v:Z

.field private w:I

.field private x:Lcom/google/n/ao;

.field private y:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1014
    sget-object v0, Lcom/google/maps/g/dw;->z:Lcom/google/maps/g/dw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1312
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    .line 1448
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/maps/g/dy;->c:I

    .line 1484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->d:Lcom/google/n/ao;

    .line 1543
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->e:Lcom/google/n/ao;

    .line 1634
    iput v1, p0, Lcom/google/maps/g/dy;->g:I

    .line 1670
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->h:Lcom/google/n/ao;

    .line 1729
    iput v1, p0, Lcom/google/maps/g/dy;->i:I

    .line 1765
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->j:Lcom/google/n/ao;

    .line 1824
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->k:Lcom/google/n/ao;

    .line 1884
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    .line 2020
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dy;->m:Ljava/lang/Object;

    .line 2096
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dy;->n:Ljava/lang/Object;

    .line 2236
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->q:Lcom/google/n/ao;

    .line 2391
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->u:Lcom/google/n/ao;

    .line 2514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/dy;->x:Lcom/google/n/ao;

    .line 2573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/g/dy;->y:Z

    .line 1015
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;
    .locals 7

    .prologue
    const/high16 v6, 0x20000

    const/high16 v5, 0x10000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1196
    invoke-static {}, Lcom/google/maps/g/dw;->d()Lcom/google/maps/g/dw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1296
    :goto_0
    return-object p0

    .line 1197
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1198
    iget-object v2, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1199
    iget-object v2, p1, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    .line 1200
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1207
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 1208
    iget v2, p1, Lcom/google/maps/g/dw;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1202
    :cond_3
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1203
    :cond_4
    iget-object v2, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1207
    goto :goto_2

    .line 1208
    :cond_6
    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, p0, Lcom/google/maps/g/dy;->c:I

    .line 1210
    :cond_7
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1211
    iget-object v2, p0, Lcom/google/maps/g/dy;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1212
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1214
    :cond_8
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 1215
    iget-object v2, p0, Lcom/google/maps/g/dy;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1216
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1218
    :cond_9
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 1219
    iget v2, p1, Lcom/google/maps/g/dw;->f:I

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput v2, p0, Lcom/google/maps/g/dy;->f:I

    .line 1221
    :cond_a
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_11

    .line 1222
    iget v2, p1, Lcom/google/maps/g/dw;->g:I

    invoke-static {v2}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v2

    if-nez v2, :cond_b

    sget-object v2, Lcom/google/maps/g/a/dx;->b:Lcom/google/maps/g/a/dx;

    :cond_b
    if-nez v2, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    move v2, v1

    .line 1210
    goto :goto_3

    :cond_d
    move v2, v1

    .line 1214
    goto :goto_4

    :cond_e
    move v2, v1

    .line 1218
    goto :goto_5

    :cond_f
    move v2, v1

    .line 1221
    goto :goto_6

    .line 1222
    :cond_10
    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iget v2, v2, Lcom/google/maps/g/a/dx;->e:I

    iput v2, p0, Lcom/google/maps/g/dy;->g:I

    .line 1224
    :cond_11
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 1225
    iget-object v2, p0, Lcom/google/maps/g/dy;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1226
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1228
    :cond_12
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_17

    .line 1229
    iget v2, p1, Lcom/google/maps/g/dw;->i:I

    invoke-static {v2}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v2

    if-nez v2, :cond_13

    sget-object v2, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_13
    if-nez v2, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    move v2, v1

    .line 1224
    goto :goto_7

    :cond_15
    move v2, v1

    .line 1228
    goto :goto_8

    .line 1229
    :cond_16
    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iget v2, v2, Lcom/google/maps/g/a/al;->e:I

    iput v2, p0, Lcom/google/maps/g/dy;->i:I

    .line 1231
    :cond_17
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_9
    if-eqz v2, :cond_18

    .line 1232
    iget-object v2, p0, Lcom/google/maps/g/dy;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1233
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1235
    :cond_18
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_a
    if-eqz v2, :cond_19

    .line 1236
    iget-object v2, p0, Lcom/google/maps/g/dy;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1237
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1239
    :cond_19
    iget-object v2, p1, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1240
    iget-object v2, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1241
    iget-object v2, p1, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    .line 1242
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1249
    :cond_1a
    :goto_b
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2c

    move v2, v0

    :goto_c
    if-eqz v2, :cond_1b

    .line 1250
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1251
    iget-object v2, p1, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/dy;->m:Ljava/lang/Object;

    .line 1254
    :cond_1b
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_2d

    move v2, v0

    :goto_d
    if-eqz v2, :cond_1c

    .line 1255
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1256
    iget-object v2, p1, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/dy;->n:Ljava/lang/Object;

    .line 1259
    :cond_1c
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_2e

    move v2, v0

    :goto_e
    if-eqz v2, :cond_1d

    .line 1260
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->o:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->o:Z

    .line 1262
    :cond_1d
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_2f

    move v2, v0

    :goto_f
    if-eqz v2, :cond_1e

    .line 1263
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->p:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->p:Z

    .line 1265
    :cond_1e
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_30

    move v2, v0

    :goto_10
    if-eqz v2, :cond_1f

    .line 1266
    iget-object v2, p0, Lcom/google/maps/g/dy;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1267
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1269
    :cond_1f
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_31

    move v2, v0

    :goto_11
    if-eqz v2, :cond_20

    .line 1270
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->r:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->r:Z

    .line 1272
    :cond_20
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_32

    move v2, v0

    :goto_12
    if-eqz v2, :cond_21

    .line 1273
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->s:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->s:Z

    .line 1275
    :cond_21
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_33

    move v2, v0

    :goto_13
    if-eqz v2, :cond_22

    .line 1276
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->t:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v4, 0x40000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->t:Z

    .line 1278
    :cond_22
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_34

    move v2, v0

    :goto_14
    if-eqz v2, :cond_23

    .line 1279
    iget-object v2, p0, Lcom/google/maps/g/dy;->u:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1280
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1282
    :cond_23
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_35

    move v2, v0

    :goto_15
    if-eqz v2, :cond_24

    .line 1283
    iget-boolean v2, p1, Lcom/google/maps/g/dw;->v:Z

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/dy;->v:Z

    .line 1285
    :cond_24
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_36

    move v2, v0

    :goto_16
    if-eqz v2, :cond_25

    .line 1286
    iget v2, p1, Lcom/google/maps/g/dw;->w:I

    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/g/dy;->a:I

    iput v2, p0, Lcom/google/maps/g/dy;->w:I

    .line 1288
    :cond_25
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_37

    move v2, v0

    :goto_17
    if-eqz v2, :cond_26

    .line 1289
    iget-object v2, p0, Lcom/google/maps/g/dy;->x:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1290
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v3, 0x400000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1292
    :cond_26
    iget v2, p1, Lcom/google/maps/g/dw;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_38

    :goto_18
    if-eqz v0, :cond_27

    .line 1293
    iget-boolean v0, p1, Lcom/google/maps/g/dw;->y:Z

    iget v1, p0, Lcom/google/maps/g/dy;->a:I

    const/high16 v2, 0x800000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/g/dy;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/dy;->y:Z

    .line 1295
    :cond_27
    iget-object v0, p1, Lcom/google/maps/g/dw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_28
    move v2, v1

    .line 1231
    goto/16 :goto_9

    :cond_29
    move v2, v1

    .line 1235
    goto/16 :goto_a

    .line 1244
    :cond_2a
    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-eq v2, v3, :cond_2b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/dy;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/dy;->a:I

    .line 1245
    :cond_2b
    iget-object v2, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_2c
    move v2, v1

    .line 1249
    goto/16 :goto_c

    :cond_2d
    move v2, v1

    .line 1254
    goto/16 :goto_d

    :cond_2e
    move v2, v1

    .line 1259
    goto/16 :goto_e

    :cond_2f
    move v2, v1

    .line 1262
    goto/16 :goto_f

    :cond_30
    move v2, v1

    .line 1265
    goto/16 :goto_10

    :cond_31
    move v2, v1

    .line 1269
    goto/16 :goto_11

    :cond_32
    move v2, v1

    .line 1272
    goto/16 :goto_12

    :cond_33
    move v2, v1

    .line 1275
    goto/16 :goto_13

    :cond_34
    move v2, v1

    .line 1278
    goto/16 :goto_14

    :cond_35
    move v2, v1

    .line 1282
    goto/16 :goto_15

    :cond_36
    move v2, v1

    .line 1285
    goto/16 :goto_16

    :cond_37
    move v2, v1

    .line 1288
    goto :goto_17

    :cond_38
    move v0, v1

    .line 1292
    goto :goto_18
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1006
    invoke-virtual {p0}, Lcom/google/maps/g/dy;->c()Lcom/google/maps/g/dw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1006
    check-cast p1, Lcom/google/maps/g/dw;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/dy;->a(Lcom/google/maps/g/dw;)Lcom/google/maps/g/dy;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const v3, 0x8000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1300
    iget v0, p0, Lcom/google/maps/g/dy;->a:I

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1301
    iget-object v0, p0, Lcom/google/maps/g/dy;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1306
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1300
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1306
    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/dw;
    .locals 11

    .prologue
    const/high16 v10, 0x40000

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1074
    new-instance v2, Lcom/google/maps/g/dw;

    invoke-direct {v2, p0}, Lcom/google/maps/g/dw;-><init>(Lcom/google/n/v;)V

    .line 1075
    iget v3, p0, Lcom/google/maps/g/dy;->a:I

    .line 1077
    iget v0, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    .line 1079
    iget v0, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/maps/g/dy;->a:I

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/dy;->b:Ljava/util/List;

    iput-object v0, v2, Lcom/google/maps/g/dw;->b:Ljava/util/List;

    .line 1082
    and-int/lit8 v0, v3, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_17

    .line 1083
    const/4 v0, 0x1

    .line 1085
    :goto_0
    iget v4, p0, Lcom/google/maps/g/dy;->c:I

    iput v4, v2, Lcom/google/maps/g/dw;->c:I

    .line 1086
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1087
    or-int/lit8 v0, v0, 0x2

    .line 1089
    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/dw;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->d:Lcom/google/n/ao;

    .line 1090
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->d:Lcom/google/n/ao;

    .line 1091
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1089
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1092
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1093
    or-int/lit8 v0, v0, 0x4

    .line 1095
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/dw;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->e:Lcom/google/n/ao;

    .line 1096
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->e:Lcom/google/n/ao;

    .line 1097
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1095
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1098
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 1099
    or-int/lit8 v0, v0, 0x8

    .line 1101
    :cond_3
    iget v4, p0, Lcom/google/maps/g/dy;->f:I

    iput v4, v2, Lcom/google/maps/g/dw;->f:I

    .line 1102
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 1103
    or-int/lit8 v0, v0, 0x10

    .line 1105
    :cond_4
    iget v4, p0, Lcom/google/maps/g/dy;->g:I

    iput v4, v2, Lcom/google/maps/g/dw;->g:I

    .line 1106
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 1107
    or-int/lit8 v0, v0, 0x20

    .line 1109
    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/dw;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->h:Lcom/google/n/ao;

    .line 1110
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->h:Lcom/google/n/ao;

    .line 1111
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1109
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1112
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 1113
    or-int/lit8 v0, v0, 0x40

    .line 1115
    :cond_6
    iget v4, p0, Lcom/google/maps/g/dy;->i:I

    iput v4, v2, Lcom/google/maps/g/dw;->i:I

    .line 1116
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 1117
    or-int/lit16 v0, v0, 0x80

    .line 1119
    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/dw;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->j:Lcom/google/n/ao;

    .line 1120
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->j:Lcom/google/n/ao;

    .line 1121
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1119
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1122
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 1123
    or-int/lit16 v0, v0, 0x100

    .line 1125
    :cond_8
    iget-object v4, v2, Lcom/google/maps/g/dw;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->k:Lcom/google/n/ao;

    .line 1126
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->k:Lcom/google/n/ao;

    .line 1127
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1125
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1128
    iget v4, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 1129
    iget-object v4, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    .line 1130
    iget v4, p0, Lcom/google/maps/g/dy;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/maps/g/dy;->a:I

    .line 1132
    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/dy;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/dw;->l:Ljava/util/List;

    .line 1133
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 1134
    or-int/lit16 v0, v0, 0x200

    .line 1136
    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/dy;->m:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/dw;->m:Ljava/lang/Object;

    .line 1137
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 1138
    or-int/lit16 v0, v0, 0x400

    .line 1140
    :cond_b
    iget-object v4, p0, Lcom/google/maps/g/dy;->n:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/dw;->n:Ljava/lang/Object;

    .line 1141
    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    .line 1142
    or-int/lit16 v0, v0, 0x800

    .line 1144
    :cond_c
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->o:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->o:Z

    .line 1145
    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    .line 1146
    or-int/lit16 v0, v0, 0x1000

    .line 1148
    :cond_d
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->p:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->p:Z

    .line 1149
    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    .line 1150
    or-int/lit16 v0, v0, 0x2000

    .line 1152
    :cond_e
    iget-object v4, v2, Lcom/google/maps/g/dw;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->q:Lcom/google/n/ao;

    .line 1153
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->q:Lcom/google/n/ao;

    .line 1154
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1152
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1155
    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    .line 1156
    or-int/lit16 v0, v0, 0x4000

    .line 1158
    :cond_f
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->r:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->r:Z

    .line 1159
    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    .line 1160
    or-int/2addr v0, v7

    .line 1162
    :cond_10
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->s:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->s:Z

    .line 1163
    and-int v4, v3, v10

    if-ne v4, v10, :cond_11

    .line 1164
    or-int/2addr v0, v8

    .line 1166
    :cond_11
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->t:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->t:Z

    .line 1167
    const/high16 v4, 0x80000

    and-int/2addr v4, v3

    const/high16 v5, 0x80000

    if-ne v4, v5, :cond_12

    .line 1168
    or-int/2addr v0, v9

    .line 1170
    :cond_12
    iget-object v4, v2, Lcom/google/maps/g/dw;->u:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->u:Lcom/google/n/ao;

    .line 1171
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->u:Lcom/google/n/ao;

    .line 1172
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1170
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1173
    const/high16 v4, 0x100000

    and-int/2addr v4, v3

    const/high16 v5, 0x100000

    if-ne v4, v5, :cond_13

    .line 1174
    or-int/2addr v0, v10

    .line 1176
    :cond_13
    iget-boolean v4, p0, Lcom/google/maps/g/dy;->v:Z

    iput-boolean v4, v2, Lcom/google/maps/g/dw;->v:Z

    .line 1177
    const/high16 v4, 0x200000

    and-int/2addr v4, v3

    const/high16 v5, 0x200000

    if-ne v4, v5, :cond_14

    .line 1178
    const/high16 v4, 0x80000

    or-int/2addr v0, v4

    .line 1180
    :cond_14
    iget v4, p0, Lcom/google/maps/g/dy;->w:I

    iput v4, v2, Lcom/google/maps/g/dw;->w:I

    .line 1181
    const/high16 v4, 0x400000

    and-int/2addr v4, v3

    const/high16 v5, 0x400000

    if-ne v4, v5, :cond_15

    .line 1182
    const/high16 v4, 0x100000

    or-int/2addr v0, v4

    .line 1184
    :cond_15
    iget-object v4, v2, Lcom/google/maps/g/dw;->x:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/dy;->x:Lcom/google/n/ao;

    .line 1185
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/dy;->x:Lcom/google/n/ao;

    .line 1186
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1184
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1187
    const/high16 v1, 0x800000

    and-int/2addr v1, v3

    const/high16 v3, 0x800000

    if-ne v1, v3, :cond_16

    .line 1188
    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    .line 1190
    :cond_16
    iget-boolean v1, p0, Lcom/google/maps/g/dy;->y:Z

    iput-boolean v1, v2, Lcom/google/maps/g/dw;->y:Z

    .line 1191
    iput v0, v2, Lcom/google/maps/g/dw;->a:I

    .line 1192
    return-object v2

    :cond_17
    move v0, v1

    goto/16 :goto_0
.end method
