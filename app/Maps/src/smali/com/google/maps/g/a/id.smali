.class public final Lcom/google/maps/g/a/id;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ii;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/id;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/a/id;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Lcom/google/maps/g/mg;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 482
    new-instance v0, Lcom/google/maps/g/a/ie;

    invoke-direct {v0}, Lcom/google/maps/g/a/ie;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/id;->PARSER:Lcom/google/n/ax;

    .line 630
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/id;->g:Lcom/google/n/aw;

    .line 860
    new-instance v0, Lcom/google/maps/g/a/id;

    invoke-direct {v0}, Lcom/google/maps/g/a/id;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/id;->d:Lcom/google/maps/g/a/id;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 420
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 587
    iput-byte v0, p0, Lcom/google/maps/g/a/id;->e:B

    .line 609
    iput v0, p0, Lcom/google/maps/g/a/id;->f:I

    .line 421
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/id;->b:I

    .line 422
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 428
    invoke-direct {p0}, Lcom/google/maps/g/a/id;-><init>()V

    .line 429
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 433
    const/4 v0, 0x0

    move v2, v0

    .line 434
    :cond_0
    :goto_0
    if-nez v2, :cond_3

    .line 435
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 436
    sparse-switch v0, :sswitch_data_0

    .line 441
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 443
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 439
    goto :goto_0

    .line 448
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 449
    invoke-static {v0}, Lcom/google/maps/g/a/ig;->a(I)Lcom/google/maps/g/a/ig;

    move-result-object v1

    .line 450
    if-nez v1, :cond_1

    .line 451
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 473
    :catch_0
    move-exception v0

    .line 474
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 479
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/id;->au:Lcom/google/n/bn;

    throw v0

    .line 453
    :cond_1
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/a/id;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/a/id;->a:I

    .line 454
    iput v0, p0, Lcom/google/maps/g/a/id;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 475
    :catch_1
    move-exception v0

    .line 476
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 477
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 459
    :sswitch_2
    const/4 v0, 0x0

    .line 460
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_4

    .line 461
    iget-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    invoke-static {}, Lcom/google/maps/g/mg;->newBuilder()Lcom/google/maps/g/mi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/mi;->a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;

    move-result-object v0

    move-object v1, v0

    .line 463
    :goto_1
    sget-object v0, Lcom/google/maps/g/mg;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mg;

    iput-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    .line 464
    if-eqz v1, :cond_2

    .line 465
    iget-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/mi;->a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;

    .line 466
    invoke-virtual {v1}, Lcom/google/maps/g/mi;->c()Lcom/google/maps/g/mg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    .line 468
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/id;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/id;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 479
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/id;->au:Lcom/google/n/bn;

    .line 480
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_1

    .line 436
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 418
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 587
    iput-byte v0, p0, Lcom/google/maps/g/a/id;->e:B

    .line 609
    iput v0, p0, Lcom/google/maps/g/a/id;->f:I

    .line 419
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;
    .locals 1

    .prologue
    .line 695
    invoke-static {}, Lcom/google/maps/g/a/id;->newBuilder()Lcom/google/maps/g/a/if;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/if;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/a/id;
    .locals 1

    .prologue
    .line 863
    sget-object v0, Lcom/google/maps/g/a/id;->d:Lcom/google/maps/g/a/id;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/if;
    .locals 1

    .prologue
    .line 692
    new-instance v0, Lcom/google/maps/g/a/if;

    invoke-direct {v0}, Lcom/google/maps/g/a/if;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/id;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494
    sget-object v0, Lcom/google/maps/g/a/id;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 599
    invoke-virtual {p0}, Lcom/google/maps/g/a/id;->c()I

    .line 600
    iget v0, p0, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 601
    iget v0, p0, Lcom/google/maps/g/a/id;->b:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 603
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 604
    iget-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v0

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/id;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 607
    return-void

    .line 601
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 604
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 589
    iget-byte v1, p0, Lcom/google/maps/g/a/id;->e:B

    .line 590
    if-ne v1, v0, :cond_0

    .line 594
    :goto_0
    return v0

    .line 591
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 593
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/id;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 611
    iget v0, p0, Lcom/google/maps/g/a/id;->f:I

    .line 612
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 625
    :goto_0
    return v0

    .line 615
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 616
    iget v0, p0, Lcom/google/maps/g/a/id;->b:I

    .line 617
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 619
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/id;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 621
    iget-object v2, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 623
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/id;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 624
    iput v0, p0, Lcom/google/maps/g/a/id;->f:I

    goto :goto_0

    .line 617
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 621
    :cond_3
    iget-object v2, p0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 412
    invoke-static {}, Lcom/google/maps/g/a/id;->newBuilder()Lcom/google/maps/g/a/if;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/if;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 412
    invoke-static {}, Lcom/google/maps/g/a/id;->newBuilder()Lcom/google/maps/g/a/if;

    move-result-object v0

    return-object v0
.end method
