.class public final Lcom/google/maps/g/a/eq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/et;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/eo;",
        "Lcom/google/maps/g/a/eq;",
        ">;",
        "Lcom/google/maps/g/a/et;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private c:J

.field private d:F

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/google/maps/g/a/eo;->f:Lcom/google/maps/g/a/eo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 546
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/eq;->e:I

    .line 384
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/eo;)Lcom/google/maps/g/a/eq;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 427
    invoke-static {}, Lcom/google/maps/g/a/eo;->d()Lcom/google/maps/g/a/eo;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 441
    :goto_0
    return-object p0

    .line 428
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 429
    iget v2, p1, Lcom/google/maps/g/a/eo;->b:I

    iget v3, p0, Lcom/google/maps/g/a/eq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/eq;->a:I

    iput v2, p0, Lcom/google/maps/g/a/eq;->b:I

    .line 431
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 432
    iget-wide v2, p1, Lcom/google/maps/g/a/eo;->c:J

    iget v4, p0, Lcom/google/maps/g/a/eq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/a/eq;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/a/eq;->c:J

    .line 434
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 435
    iget v2, p1, Lcom/google/maps/g/a/eo;->d:F

    iget v3, p0, Lcom/google/maps/g/a/eq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/eq;->a:I

    iput v2, p0, Lcom/google/maps/g/a/eq;->d:F

    .line 437
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/eo;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_a

    .line 438
    iget v0, p1, Lcom/google/maps/g/a/eo;->e:I

    invoke-static {v0}, Lcom/google/maps/g/a/er;->a(I)Lcom/google/maps/g/a/er;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    :cond_4
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 428
    goto :goto_1

    :cond_6
    move v2, v1

    .line 431
    goto :goto_2

    :cond_7
    move v2, v1

    .line 434
    goto :goto_3

    :cond_8
    move v0, v1

    .line 437
    goto :goto_4

    .line 438
    :cond_9
    iget v1, p0, Lcom/google/maps/g/a/eq;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/a/eq;->a:I

    iget v0, v0, Lcom/google/maps/g/a/er;->f:I

    iput v0, p0, Lcom/google/maps/g/a/eq;->e:I

    .line 440
    :cond_a
    iget-object v0, p1, Lcom/google/maps/g/a/eo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 375
    new-instance v2, Lcom/google/maps/g/a/eo;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/eo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/eq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/eq;->b:I

    iput v1, v2, Lcom/google/maps/g/a/eo;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/maps/g/a/eq;->c:J

    iput-wide v4, v2, Lcom/google/maps/g/a/eo;->c:J

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/g/a/eq;->d:F

    iput v1, v2, Lcom/google/maps/g/a/eo;->d:F

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/g/a/eq;->e:I

    iput v1, v2, Lcom/google/maps/g/a/eo;->e:I

    iput v0, v2, Lcom/google/maps/g/a/eo;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 375
    check-cast p1, Lcom/google/maps/g/a/eo;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/eq;->a(Lcom/google/maps/g/a/eo;)Lcom/google/maps/g/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x1

    return v0
.end method
