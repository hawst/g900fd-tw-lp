.class public final Lcom/google/maps/g/fz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ga;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/fx;",
        "Lcom/google/maps/g/fz;",
        ">;",
        "Lcom/google/maps/g/ga;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/fc;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 697
    sget-object v0, Lcom/google/maps/g/fx;->g:Lcom/google/maps/g/fx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 781
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    .line 842
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fz;->c:Ljava/lang/Object;

    .line 918
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fz;->d:Ljava/lang/Object;

    .line 994
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fz;->e:Ljava/lang/Object;

    .line 1070
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fz;->f:Ljava/lang/Object;

    .line 698
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/fx;)Lcom/google/maps/g/fz;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 747
    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 772
    :goto_0
    return-object p0

    .line 748
    :cond_0
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p1, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/fz;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    invoke-static {v3}, Lcom/google/maps/g/fc;->a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/fe;->a(Lcom/google/maps/g/fc;)Lcom/google/maps/g/fe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/fe;->c()Lcom/google/maps/g/fc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/fz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/fz;->a:I

    .line 751
    :cond_1
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 752
    iget v0, p0, Lcom/google/maps/g/fz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/fz;->a:I

    .line 753
    iget-object v0, p1, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/fz;->c:Ljava/lang/Object;

    .line 756
    :cond_2
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 757
    iget v0, p0, Lcom/google/maps/g/fz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/fz;->a:I

    .line 758
    iget-object v0, p1, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/fz;->d:Ljava/lang/Object;

    .line 761
    :cond_3
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_6
    if-eqz v0, :cond_4

    .line 762
    iget v0, p0, Lcom/google/maps/g/fz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/fz;->a:I

    .line 763
    iget-object v0, p1, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/fz;->e:Ljava/lang/Object;

    .line 766
    :cond_4
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 767
    iget v0, p0, Lcom/google/maps/g/fz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/fz;->a:I

    .line 768
    iget-object v0, p1, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/fz;->f:Ljava/lang/Object;

    .line 771
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/fx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 748
    goto/16 :goto_1

    .line 749
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto/16 :goto_2

    :cond_8
    iput-object v0, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    goto :goto_3

    :cond_9
    move v0, v2

    .line 751
    goto :goto_4

    :cond_a
    move v0, v2

    .line 756
    goto :goto_5

    :cond_b
    move v0, v2

    .line 761
    goto :goto_6

    :cond_c
    move v0, v2

    .line 766
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 689
    new-instance v2, Lcom/google/maps/g/fx;

    invoke-direct {v2, p0}, Lcom/google/maps/g/fx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/fz;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/fz;->b:Lcom/google/maps/g/fc;

    iput-object v1, v2, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/fz;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fx;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/fz;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fx;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/fz;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/fz;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/fx;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 689
    check-cast p1, Lcom/google/maps/g/fx;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/fz;->a(Lcom/google/maps/g/fx;)Lcom/google/maps/g/fz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 776
    const/4 v0, 0x1

    return v0
.end method
