.class public final Lcom/google/maps/g/u;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ab;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/u;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/maps/g/u;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/maps/g/x;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614
    new-instance v0, Lcom/google/maps/g/v;

    invoke-direct {v0}, Lcom/google/maps/g/v;-><init>()V

    sput-object v0, Lcom/google/maps/g/u;->PARSER:Lcom/google/n/ax;

    .line 1140
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/u;->f:Lcom/google/n/aw;

    .line 1325
    new-instance v0, Lcom/google/maps/g/u;

    invoke-direct {v0}, Lcom/google/maps/g/u;-><init>()V

    sput-object v0, Lcom/google/maps/g/u;->c:Lcom/google/maps/g/u;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 564
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1104
    iput-byte v0, p0, Lcom/google/maps/g/u;->d:B

    .line 1123
    iput v0, p0, Lcom/google/maps/g/u;->e:I

    .line 565
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 571
    invoke-direct {p0}, Lcom/google/maps/g/u;-><init>()V

    .line 572
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 576
    const/4 v0, 0x0

    move v2, v0

    .line 577
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 578
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 579
    sparse-switch v0, :sswitch_data_0

    .line 584
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 586
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 582
    goto :goto_0

    .line 591
    :sswitch_1
    const/4 v0, 0x0

    .line 592
    iget v1, p0, Lcom/google/maps/g/u;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 593
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    invoke-static {v0}, Lcom/google/maps/g/x;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    move-result-object v0

    move-object v1, v0

    .line 595
    :goto_1
    sget-object v0, Lcom/google/maps/g/x;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/x;

    iput-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    .line 596
    if-eqz v1, :cond_1

    .line 597
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/z;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    .line 598
    invoke-virtual {v1}, Lcom/google/maps/g/z;->c()Lcom/google/maps/g/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    .line 600
    :cond_1
    iget v0, p0, Lcom/google/maps/g/u;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/u;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 605
    :catch_0
    move-exception v0

    .line 606
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/u;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/u;->au:Lcom/google/n/bn;

    .line 612
    return-void

    .line 607
    :catch_1
    move-exception v0

    .line 608
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 609
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 579
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 562
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1104
    iput-byte v0, p0, Lcom/google/maps/g/u;->d:B

    .line 1123
    iput v0, p0, Lcom/google/maps/g/u;->e:I

    .line 563
    return-void
.end method

.method public static a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;
    .locals 1

    .prologue
    .line 1205
    invoke-static {}, Lcom/google/maps/g/u;->newBuilder()Lcom/google/maps/g/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/w;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/u;
    .locals 1

    .prologue
    .line 1328
    sget-object v0, Lcom/google/maps/g/u;->c:Lcom/google/maps/g/u;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/w;
    .locals 1

    .prologue
    .line 1202
    new-instance v0, Lcom/google/maps/g/w;

    invoke-direct {v0}, Lcom/google/maps/g/w;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/u;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626
    sget-object v0, Lcom/google/maps/g/u;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1116
    invoke-virtual {p0}, Lcom/google/maps/g/u;->c()I

    .line 1117
    iget v0, p0, Lcom/google/maps/g/u;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1118
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/x;->d()Lcom/google/maps/g/x;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1120
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/u;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1121
    return-void

    .line 1118
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1106
    iget-byte v1, p0, Lcom/google/maps/g/u;->d:B

    .line 1107
    if-ne v1, v0, :cond_0

    .line 1111
    :goto_0
    return v0

    .line 1108
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1110
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/u;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1125
    iget v0, p0, Lcom/google/maps/g/u;->e:I

    .line 1126
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1135
    :goto_0
    return v0

    .line 1129
    :cond_0
    iget v0, p0, Lcom/google/maps/g/u;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 1131
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/x;->d()Lcom/google/maps/g/x;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1133
    :goto_2
    iget-object v1, p0, Lcom/google/maps/g/u;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1134
    iput v0, p0, Lcom/google/maps/g/u;->e:I

    goto :goto_0

    .line 1131
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/google/maps/g/u;->newBuilder()Lcom/google/maps/g/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/w;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/google/maps/g/u;->newBuilder()Lcom/google/maps/g/w;

    move-result-object v0

    return-object v0
.end method
