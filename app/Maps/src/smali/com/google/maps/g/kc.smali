.class public final Lcom/google/maps/g/kc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/jy;",
        "Lcom/google/maps/g/kc;",
        ">;",
        "Lcom/google/maps/g/kd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 412
    sget-object v0, Lcom/google/maps/g/jy;->e:Lcom/google/maps/g/jy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 499
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    .line 636
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    .line 772
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/kc;->d:I

    .line 413
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 452
    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 477
    :goto_0
    return-object p0

    .line 453
    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 454
    iget-object v1, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 455
    iget-object v1, p1, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    .line 456
    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/kc;->a:I

    .line 463
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 464
    iget-object v1, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 465
    iget-object v1, p1, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    .line 466
    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/kc;->a:I

    .line 473
    :cond_2
    :goto_2
    iget v1, p1, Lcom/google/maps/g/jy;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_3
    if-eqz v0, :cond_a

    .line 474
    iget v0, p1, Lcom/google/maps/g/jy;->d:I

    invoke-static {v0}, Lcom/google/maps/g/ka;->a(I)Lcom/google/maps/g/ka;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/ka;->a:Lcom/google/maps/g/ka;

    :cond_3
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 458
    :cond_4
    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/kc;->a:I

    .line 459
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 468
    :cond_6
    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/kc;->a:I

    .line 469
    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    iget-object v2, p1, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 473
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 474
    :cond_9
    iget v1, p0, Lcom/google/maps/g/kc;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/kc;->a:I

    iget v0, v0, Lcom/google/maps/g/ka;->c:I

    iput v0, p0, Lcom/google/maps/g/kc;->d:I

    .line 476
    :cond_a
    iget-object v0, p1, Lcom/google/maps/g/jy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/google/maps/g/kc;->c()Lcom/google/maps/g/jy;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 404
    check-cast p1, Lcom/google/maps/g/jy;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 481
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 482
    iget-object v0, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/w;

    invoke-virtual {v0}, Lcom/google/maps/b/w;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 493
    :cond_0
    :goto_1
    return v2

    .line 481
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 487
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 488
    iget-object v0, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/dc;->d()Lcom/google/maps/b/dc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/dc;

    invoke-virtual {v0}, Lcom/google/maps/b/dc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 493
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/jy;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 430
    new-instance v2, Lcom/google/maps/g/jy;

    invoke-direct {v2, p0}, Lcom/google/maps/g/jy;-><init>(Lcom/google/n/v;)V

    .line 431
    iget v3, p0, Lcom/google/maps/g/kc;->a:I

    .line 432
    const/4 v1, 0x0

    .line 433
    iget v4, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 434
    iget-object v4, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    .line 435
    iget v4, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/kc;->a:I

    .line 437
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/kc;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/jy;->b:Ljava/util/List;

    .line 438
    iget v4, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 439
    iget-object v4, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    .line 440
    iget v4, p0, Lcom/google/maps/g/kc;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/kc;->a:I

    .line 442
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/kc;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/jy;->c:Ljava/util/List;

    .line 443
    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 446
    :goto_0
    iget v1, p0, Lcom/google/maps/g/kc;->d:I

    iput v1, v2, Lcom/google/maps/g/jy;->d:I

    .line 447
    iput v0, v2, Lcom/google/maps/g/jy;->a:I

    .line 448
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
