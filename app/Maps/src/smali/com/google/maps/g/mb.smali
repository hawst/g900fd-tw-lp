.class public final Lcom/google/maps/g/mb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/me;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/lz;",
        "Lcom/google/maps/g/mb;",
        ">;",
        "Lcom/google/maps/g/me;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/maps/g/a;

.field private d:Lcom/google/maps/g/ea;

.field private e:Lcom/google/maps/g/ck;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/maps/g/kq;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Lcom/google/maps/g/gy;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 900
    sget-object v0, Lcom/google/maps/g/lz;->k:Lcom/google/maps/g/lz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1040
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/maps/g/mb;->b:I

    .line 1072
    iput-object v1, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    .line 1133
    iput-object v1, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    .line 1194
    iput-object v1, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    .line 1255
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/mb;->f:Ljava/lang/Object;

    .line 1331
    iput-object v1, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    .line 1393
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    .line 1517
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/mb;->i:I

    .line 1553
    iput-object v1, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    .line 901
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/lz;)Lcom/google/maps/g/mb;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 975
    invoke-static {}, Lcom/google/maps/g/lz;->d()Lcom/google/maps/g/lz;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1013
    :goto_0
    return-object p0

    .line 976
    :cond_0
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 977
    iget v0, p1, Lcom/google/maps/g/lz;->b:I

    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/mb;->a:I

    iput v0, p0, Lcom/google/maps/g/mb;->b:I

    .line 979
    :cond_1
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 980
    iget-object v0, p1, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v0

    :goto_3
    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v4, :cond_c

    iget-object v3, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v4

    if-eq v3, v4, :cond_c

    iget-object v3, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    invoke-static {v3}, Lcom/google/maps/g/a;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/c;->c()Lcom/google/maps/g/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    :goto_4
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 982
    :cond_2
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_d

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 983
    iget-object v0, p1, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v0

    :goto_6
    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v5, :cond_f

    iget-object v3, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v4

    if-eq v3, v4, :cond_f

    iget-object v3, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    invoke-static {v3}, Lcom/google/maps/g/ea;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ec;->c()Lcom/google/maps/g/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    :goto_7
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 985
    :cond_3
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_10

    move v0, v1

    :goto_8
    if-eqz v0, :cond_4

    .line 986
    iget-object v0, p1, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    if-nez v0, :cond_11

    invoke-static {}, Lcom/google/maps/g/ck;->d()Lcom/google/maps/g/ck;

    move-result-object v0

    :goto_9
    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v6, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    invoke-static {}, Lcom/google/maps/g/ck;->d()Lcom/google/maps/g/ck;

    move-result-object v4

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    invoke-static {v3}, Lcom/google/maps/g/ck;->a(Lcom/google/maps/g/ck;)Lcom/google/maps/g/cm;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/cm;->a(Lcom/google/maps/g/ck;)Lcom/google/maps/g/cm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/cm;->c()Lcom/google/maps/g/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    :goto_a
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 988
    :cond_4
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_b
    if-eqz v0, :cond_5

    .line 989
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 990
    iget-object v0, p1, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/mb;->f:Ljava/lang/Object;

    .line 993
    :cond_5
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_c
    if-eqz v0, :cond_6

    .line 994
    iget-object v0, p1, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v0

    :goto_d
    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_16

    iget-object v3, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v4

    if-eq v3, v4, :cond_16

    iget-object v3, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    invoke-static {v3}, Lcom/google/maps/g/kq;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ks;->c()Lcom/google/maps/g/kq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    :goto_e
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 996
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 997
    iget-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 998
    iget-object v0, p1, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    .line 999
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 1006
    :cond_7
    :goto_f
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_10
    if-eqz v0, :cond_1b

    .line 1007
    iget v0, p1, Lcom/google/maps/g/lz;->i:I

    invoke-static {v0}, Lcom/google/maps/g/mc;->a(I)Lcom/google/maps/g/mc;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/mc;->a:Lcom/google/maps/g/mc;

    :cond_8
    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v0, v2

    .line 976
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 979
    goto/16 :goto_2

    .line 980
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    goto/16 :goto_3

    :cond_c
    iput-object v0, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    goto/16 :goto_4

    :cond_d
    move v0, v2

    .line 982
    goto/16 :goto_5

    .line 983
    :cond_e
    iget-object v0, p1, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    goto/16 :goto_6

    :cond_f
    iput-object v0, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    goto/16 :goto_7

    :cond_10
    move v0, v2

    .line 985
    goto/16 :goto_8

    .line 986
    :cond_11
    iget-object v0, p1, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    goto/16 :goto_9

    :cond_12
    iput-object v0, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    goto/16 :goto_a

    :cond_13
    move v0, v2

    .line 988
    goto/16 :goto_b

    :cond_14
    move v0, v2

    .line 993
    goto/16 :goto_c

    .line 994
    :cond_15
    iget-object v0, p1, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    goto/16 :goto_d

    :cond_16
    iput-object v0, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    goto :goto_e

    .line 1001
    :cond_17
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-eq v0, v3, :cond_18

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 1002
    :cond_18
    iget-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_f

    :cond_19
    move v0, v2

    .line 1006
    goto :goto_10

    .line 1007
    :cond_1a
    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/g/mb;->a:I

    iget v0, v0, Lcom/google/maps/g/mc;->e:I

    iput v0, p0, Lcom/google/maps/g/mb;->i:I

    .line 1009
    :cond_1b
    iget v0, p1, Lcom/google/maps/g/lz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1d

    move v0, v1

    :goto_11
    if-eqz v0, :cond_1c

    .line 1010
    iget-object v0, p1, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    if-nez v0, :cond_1e

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_12
    iget v1, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_1f

    iget-object v1, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v2

    if-eq v1, v2, :cond_1f

    iget-object v1, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    invoke-static {v1}, Lcom/google/maps/g/gy;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ha;->c()Lcom/google/maps/g/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    :goto_13
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/mb;->a:I

    .line 1012
    :cond_1c
    iget-object v0, p1, Lcom/google/maps/g/lz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1d
    move v0, v2

    .line 1009
    goto :goto_11

    .line 1010
    :cond_1e
    iget-object v0, p1, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    goto :goto_12

    :cond_1f
    iput-object v0, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    goto :goto_13
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 892
    new-instance v2, Lcom/google/maps/g/lz;

    invoke-direct {v2, p0}, Lcom/google/maps/g/lz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/mb;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v1, p0, Lcom/google/maps/g/mb;->b:I

    iput v1, v2, Lcom/google/maps/g/lz;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    iput-object v1, v2, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/mb;->d:Lcom/google/maps/g/ea;

    iput-object v1, v2, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/mb;->e:Lcom/google/maps/g/ck;

    iput-object v1, v2, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/mb;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    iput-object v1, v2, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    iget v1, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/maps/g/mb;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v1, p0, Lcom/google/maps/g/mb;->i:I

    iput v1, v2, Lcom/google/maps/g/lz;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/mb;->j:Lcom/google/maps/g/gy;

    iput-object v1, v2, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    iput v0, v2, Lcom/google/maps/g/lz;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 892
    check-cast p1, Lcom/google/maps/g/lz;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/mb;->a(Lcom/google/maps/g/lz;)Lcom/google/maps/g/mb;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1017
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 1018
    iget-object v0, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1035
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 1017
    goto :goto_0

    .line 1018
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/mb;->c:Lcom/google/maps/g/a;

    goto :goto_1

    .line 1023
    :cond_3
    iget v0, p0, Lcom/google/maps/g/mb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1024
    iget-object v0, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/maps/g/kq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v1, v2

    .line 1029
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1030
    iget-object v0, p0, Lcom/google/maps/g/mb;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a;

    invoke-virtual {v0}, Lcom/google/maps/g/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1029
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_5
    move v0, v2

    .line 1023
    goto :goto_3

    .line 1024
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/mb;->g:Lcom/google/maps/g/kq;

    goto :goto_4

    :cond_7
    move v2, v3

    .line 1035
    goto :goto_2
.end method
