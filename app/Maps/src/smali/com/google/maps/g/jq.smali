.class public final enum Lcom/google/maps/g/jq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/jq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/jq;

.field public static final enum b:Lcom/google/maps/g/jq;

.field public static final enum c:Lcom/google/maps/g/jq;

.field public static final enum d:Lcom/google/maps/g/jq;

.field public static final enum e:Lcom/google/maps/g/jq;

.field private static final synthetic g:[Lcom/google/maps/g/jq;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 892
    new-instance v0, Lcom/google/maps/g/jq;

    const-string v1, "OPEN_NOW"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/maps/g/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    .line 896
    new-instance v0, Lcom/google/maps/g/jq;

    const-string v1, "CLOSING_SOON"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/g/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jq;->b:Lcom/google/maps/g/jq;

    .line 900
    new-instance v0, Lcom/google/maps/g/jq;

    const-string v1, "OPEN_LATER"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jq;->c:Lcom/google/maps/g/jq;

    .line 904
    new-instance v0, Lcom/google/maps/g/jq;

    const-string v1, "CLOSED_FOR_DAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/g/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jq;->d:Lcom/google/maps/g/jq;

    .line 908
    new-instance v0, Lcom/google/maps/g/jq;

    const-string v1, "CLOSED_ALL_DAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/g/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jq;->e:Lcom/google/maps/g/jq;

    .line 887
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/g/jq;

    sget-object v1, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/jq;->b:Lcom/google/maps/g/jq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/jq;->c:Lcom/google/maps/g/jq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/jq;->d:Lcom/google/maps/g/jq;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/jq;->e:Lcom/google/maps/g/jq;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/jq;->g:[Lcom/google/maps/g/jq;

    .line 953
    new-instance v0, Lcom/google/maps/g/jr;

    invoke-direct {v0}, Lcom/google/maps/g/jr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 962
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 963
    iput p3, p0, Lcom/google/maps/g/jq;->f:I

    .line 964
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/jq;
    .locals 1

    .prologue
    .line 938
    packed-switch p0, :pswitch_data_0

    .line 944
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 939
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    goto :goto_0

    .line 940
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/jq;->b:Lcom/google/maps/g/jq;

    goto :goto_0

    .line 941
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/jq;->c:Lcom/google/maps/g/jq;

    goto :goto_0

    .line 942
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/jq;->d:Lcom/google/maps/g/jq;

    goto :goto_0

    .line 943
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/jq;->e:Lcom/google/maps/g/jq;

    goto :goto_0

    .line 938
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/jq;
    .locals 1

    .prologue
    .line 887
    const-class v0, Lcom/google/maps/g/jq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jq;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/jq;
    .locals 1

    .prologue
    .line 887
    sget-object v0, Lcom/google/maps/g/jq;->g:[Lcom/google/maps/g/jq;

    invoke-virtual {v0}, [Lcom/google/maps/g/jq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/jq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 934
    iget v0, p0, Lcom/google/maps/g/jq;->f:I

    return v0
.end method
