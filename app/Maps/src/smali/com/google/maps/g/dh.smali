.class public final Lcom/google/maps/g/dh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/df;",
        "Lcom/google/maps/g/dh;",
        ">;",
        "Lcom/google/maps/g/di;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/ux;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/maps/g/a/i;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 564
    sget-object v0, Lcom/google/maps/g/df;->g:Lcom/google/maps/g/df;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 652
    iput-object v1, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    .line 713
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dh;->c:Ljava/lang/Object;

    .line 789
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dh;->d:Ljava/lang/Object;

    .line 865
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/dh;->e:Ljava/lang/Object;

    .line 941
    iput-object v1, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    .line 565
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/df;)Lcom/google/maps/g/dh;
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 614
    invoke-static {}, Lcom/google/maps/g/df;->d()Lcom/google/maps/g/df;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 637
    :goto_0
    return-object p0

    .line 615
    :cond_0
    iget v0, p1, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 616
    iget-object v0, p1, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/maps/g/dh;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    invoke-static {v3}, Lcom/google/maps/g/ux;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/uz;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/uz;->c()Lcom/google/maps/g/ux;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    :goto_3
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/dh;->a:I

    .line 618
    :cond_1
    iget v0, p1, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 619
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/dh;->a:I

    .line 620
    iget-object v0, p1, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/dh;->c:Ljava/lang/Object;

    .line 623
    :cond_2
    iget v0, p1, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 624
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/dh;->a:I

    .line 625
    iget-object v0, p1, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/dh;->d:Ljava/lang/Object;

    .line 628
    :cond_3
    iget v0, p1, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_6
    if-eqz v0, :cond_4

    .line 629
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/dh;->a:I

    .line 630
    iget-object v0, p1, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/dh;->e:Ljava/lang/Object;

    .line 633
    :cond_4
    iget v0, p1, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_c

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 634
    iget-object v0, p1, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v0

    :goto_8
    iget v1, p0, Lcom/google/maps/g/dh;->a:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v5, :cond_e

    iget-object v1, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v2

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    invoke-static {v1}, Lcom/google/maps/g/a/i;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/k;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/k;->c()Lcom/google/maps/g/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    :goto_9
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/dh;->a:I

    .line 636
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/df;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 615
    goto/16 :goto_1

    .line 616
    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    goto/16 :goto_2

    :cond_8
    iput-object v0, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    goto/16 :goto_3

    :cond_9
    move v0, v2

    .line 618
    goto/16 :goto_4

    :cond_a
    move v0, v2

    .line 623
    goto :goto_5

    :cond_b
    move v0, v2

    .line 628
    goto :goto_6

    :cond_c
    move v0, v2

    .line 633
    goto :goto_7

    .line 634
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    goto :goto_8

    :cond_e
    iput-object v0, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 556
    new-instance v2, Lcom/google/maps/g/df;

    invoke-direct {v2, p0}, Lcom/google/maps/g/df;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/dh;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    iput-object v1, v2, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/dh;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/dh;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/dh;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/dh;->f:Lcom/google/maps/g/a/i;

    iput-object v1, v2, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    iput v0, v2, Lcom/google/maps/g/df;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 556
    check-cast p1, Lcom/google/maps/g/df;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/dh;->a(Lcom/google/maps/g/df;)Lcom/google/maps/g/dh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 641
    iget v0, p0, Lcom/google/maps/g/dh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 642
    iget-object v0, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/g/ux;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 647
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 641
    goto :goto_0

    .line 642
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/dh;->b:Lcom/google/maps/g/ux;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 647
    goto :goto_2
.end method
