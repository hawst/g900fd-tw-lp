.class public final Lcom/google/maps/g/ol;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/oo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/oj;",
        "Lcom/google/maps/g/ol;",
        ">;",
        "Lcom/google/maps/g/oo;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 632
    sget-object v0, Lcom/google/maps/g/oj;->l:Lcom/google/maps/g/oj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 778
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/ol;->b:I

    .line 814
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ol;->c:Ljava/lang/Object;

    .line 890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->d:Lcom/google/n/ao;

    .line 949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ol;->e:Ljava/lang/Object;

    .line 1025
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->f:Lcom/google/n/ao;

    .line 1084
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->g:Lcom/google/n/ao;

    .line 1143
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->h:Lcom/google/n/ao;

    .line 1202
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->i:Lcom/google/n/ao;

    .line 1261
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->j:Lcom/google/n/ao;

    .line 1320
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ol;->k:Lcom/google/n/ao;

    .line 633
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/oj;)Lcom/google/maps/g/ol;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 726
    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 769
    :goto_0
    return-object p0

    .line 727
    :cond_0
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 728
    iget v2, p1, Lcom/google/maps/g/oj;->b:I

    invoke-static {v2}, Lcom/google/maps/g/om;->a(I)Lcom/google/maps/g/om;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 727
    goto :goto_1

    .line 728
    :cond_3
    iget v3, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ol;->a:I

    iget v2, v2, Lcom/google/maps/g/om;->h:I

    iput v2, p0, Lcom/google/maps/g/ol;->b:I

    .line 730
    :cond_4
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 731
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 732
    iget-object v2, p1, Lcom/google/maps/g/oj;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ol;->c:Ljava/lang/Object;

    .line 735
    :cond_5
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 736
    iget-object v2, p0, Lcom/google/maps/g/ol;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 737
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 739
    :cond_6
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 740
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 741
    iget-object v2, p1, Lcom/google/maps/g/oj;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ol;->e:Ljava/lang/Object;

    .line 744
    :cond_7
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 745
    iget-object v2, p0, Lcom/google/maps/g/ol;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 746
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 748
    :cond_8
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 749
    iget-object v2, p0, Lcom/google/maps/g/ol;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 750
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 752
    :cond_9
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 753
    iget-object v2, p0, Lcom/google/maps/g/ol;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 754
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 756
    :cond_a
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 757
    iget-object v2, p0, Lcom/google/maps/g/ol;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 758
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 760
    :cond_b
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_c

    .line 761
    iget-object v2, p0, Lcom/google/maps/g/ol;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/oj;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 762
    iget v2, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/ol;->a:I

    .line 764
    :cond_c
    iget v2, p1, Lcom/google/maps/g/oj;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_16

    :goto_a
    if-eqz v0, :cond_d

    .line 765
    iget-object v0, p0, Lcom/google/maps/g/ol;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 766
    iget v0, p0, Lcom/google/maps/g/ol;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/ol;->a:I

    .line 768
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/oj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 730
    goto/16 :goto_2

    :cond_f
    move v2, v1

    .line 735
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 739
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 744
    goto/16 :goto_5

    :cond_12
    move v2, v1

    .line 748
    goto/16 :goto_6

    :cond_13
    move v2, v1

    .line 752
    goto :goto_7

    :cond_14
    move v2, v1

    .line 756
    goto :goto_8

    :cond_15
    move v2, v1

    .line 760
    goto :goto_9

    :cond_16
    move v0, v1

    .line 764
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 624
    new-instance v2, Lcom/google/maps/g/oj;

    invoke-direct {v2, p0}, Lcom/google/maps/g/oj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ol;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget v4, p0, Lcom/google/maps/g/ol;->b:I

    iput v4, v2, Lcom/google/maps/g/oj;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ol;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/oj;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/oj;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/ol;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/oj;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/oj;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/oj;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/oj;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/g/oj;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ol;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ol;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v3, v2, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/ol;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/ol;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/g/oj;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 624
    check-cast p1, Lcom/google/maps/g/oj;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ol;->a(Lcom/google/maps/g/oj;)Lcom/google/maps/g/ol;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 773
    const/4 v0, 0x1

    return v0
.end method
