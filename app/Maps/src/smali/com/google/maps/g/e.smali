.class public final Lcom/google/maps/g/e;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/h;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/e;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/maps/g/e;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Lcom/google/n/aq;

.field j:Ljava/lang/Object;

.field k:Lcom/google/n/ao;

.field l:Ljava/lang/Object;

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/google/maps/g/f;

    invoke-direct {v0}, Lcom/google/maps/g/f;-><init>()V

    sput-object v0, Lcom/google/maps/g/e;->PARSER:Lcom/google/n/ax;

    .line 637
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/e;->p:Lcom/google/n/aw;

    .line 1660
    new-instance v0, Lcom/google/maps/g/e;

    invoke-direct {v0}, Lcom/google/maps/g/e;-><init>()V

    sput-object v0, Lcom/google/maps/g/e;->m:Lcom/google/maps/g/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    .line 463
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    .line 520
    iput-byte v2, p0, Lcom/google/maps/g/e;->n:B

    .line 575
    iput v2, p0, Lcom/google/maps/g/e;->o:I

    .line 18
    const/16 v0, 0x1c86

    iput v0, p0, Lcom/google/maps/g/e;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0x80

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/maps/g/e;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 41
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 43
    sparse-switch v4, :sswitch_data_0

    .line 48
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 50
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 56
    iget v4, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/e;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x80

    if-ne v1, v6, :cond_1

    .line 129
    iget-object v1, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    .line 131
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/e;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 61
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 62
    iput-object v4, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 124
    :catch_1
    move-exception v0

    .line 125
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 126
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 66
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 67
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 68
    iput-object v4, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    goto :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 73
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 74
    iput-object v4, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    goto :goto_0

    .line 78
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 79
    and-int/lit16 v5, v1, 0x80

    if-eq v5, v6, :cond_2

    .line 80
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    .line 81
    or-int/lit16 v1, v1, 0x80

    .line 83
    :cond_2
    iget-object v5, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 87
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 88
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 89
    iput-object v4, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 93
    :sswitch_7
    iget-object v4, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 94
    iget v4, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/maps/g/e;->a:I

    goto/16 :goto_0

    .line 98
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 99
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 100
    iput-object v4, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 104
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 105
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 106
    iput-object v4, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 110
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 111
    iget v5, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/e;->a:I

    .line 112
    iput-object v4, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 116
    :sswitch_b
    iget v4, p0, Lcom/google/maps/g/e;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/e;->a:I

    .line 117
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/g/e;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 128
    :cond_3
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v6, :cond_4

    .line 129
    iget-object v0, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    .line 131
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->au:Lcom/google/n/bn;

    .line 132
    return-void

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    .line 463
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    .line 520
    iput-byte v1, p0, Lcom/google/maps/g/e;->n:B

    .line 575
    iput v1, p0, Lcom/google/maps/g/e;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/e;
    .locals 1

    .prologue
    .line 1663
    sget-object v0, Lcom/google/maps/g/e;->m:Lcom/google/maps/g/e;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/g;
    .locals 1

    .prologue
    .line 699
    new-instance v0, Lcom/google/maps/g/g;

    invoke-direct {v0}, Lcom/google/maps/g/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    sget-object v0, Lcom/google/maps/g/e;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 538
    invoke-virtual {p0}, Lcom/google/maps/g/e;->c()I

    .line 539
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 542
    :cond_0
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_1

    .line 543
    iget-object v0, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 545
    :cond_1
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    .line 546
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 548
    :cond_2
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_3

    .line 549
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    :goto_2
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v0, v1

    .line 551
    :goto_3
    iget-object v2, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 552
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 551
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 543
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 546
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 549
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 554
    :cond_7
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 555
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 557
    :cond_8
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_9

    .line 558
    iget-object v0, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 560
    :cond_9
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_a

    .line 561
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    :cond_a
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_b

    .line 564
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 566
    :cond_b
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_c

    .line 567
    const/16 v2, 0xb

    iget-object v0, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 569
    :cond_c
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_d

    .line 570
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/maps/g/e;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_12

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 572
    :cond_d
    :goto_8
    iget-object v0, p0, Lcom/google/maps/g/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 573
    return-void

    .line 555
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 561
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 564
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 567
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 570
    :cond_12
    int-to-long v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 522
    iget-byte v0, p0, Lcom/google/maps/g/e;->n:B

    .line 523
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 533
    :goto_0
    return v0

    .line 524
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 526
    :cond_1
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 527
    iget-object v0, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/kq;

    invoke-virtual {v0}, Lcom/google/maps/g/kq;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 528
    iput-byte v2, p0, Lcom/google/maps/g/e;->n:B

    move v0, v2

    .line 529
    goto :goto_0

    :cond_2
    move v0, v2

    .line 526
    goto :goto_1

    .line 532
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/g/e;->n:B

    move v0, v1

    .line 533
    goto :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/16 v4, 0xa

    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 577
    iget v0, p0, Lcom/google/maps/g/e;->o:I

    .line 578
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 632
    :goto_0
    return v0

    .line 581
    :cond_0
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_13

    .line 582
    iget-object v0, p0, Lcom/google/maps/g/e;->d:Lcom/google/n/ao;

    .line 583
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 585
    :goto_1
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v9, :cond_1

    .line 587
    iget-object v0, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 589
    :cond_1
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 590
    const/4 v3, 0x3

    .line 591
    iget-object v0, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 593
    :cond_2
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    .line 594
    const/4 v3, 0x5

    .line 595
    iget-object v0, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_3
    move v0, v2

    move v3, v2

    .line 599
    :goto_5
    iget-object v5, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_7

    .line 600
    iget-object v5, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    .line 601
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 599
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 587
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 591
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 595
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 603
    :cond_7
    add-int v0, v1, v3

    .line 604
    iget-object v1, p0, Lcom/google/maps/g/e;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 606
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_12

    .line 607
    const/4 v3, 0x7

    .line 608
    iget-object v0, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->j:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 610
    :goto_7
    iget v1, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_11

    .line 611
    iget-object v1, p0, Lcom/google/maps/g/e;->k:Lcom/google/n/ao;

    .line 612
    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 614
    :goto_8
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    .line 615
    const/16 v3, 0x9

    .line 616
    iget-object v0, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->l:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 618
    :cond_8
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_9

    .line 620
    iget-object v0, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->h:Ljava/lang/Object;

    :goto_a
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 622
    :cond_9
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_a

    .line 623
    const/16 v3, 0xb

    .line 624
    iget-object v0, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/e;->c:Ljava/lang/Object;

    :goto_b
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 626
    :cond_a
    iget v0, p0, Lcom/google/maps/g/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_b

    .line 627
    const/16 v0, 0xc

    iget v3, p0, Lcom/google/maps/g/e;->b:I

    .line 628
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_10

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 630
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/e;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 631
    iput v0, p0, Lcom/google/maps/g/e;->o:I

    goto/16 :goto_0

    .line 608
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 616
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 620
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    .line 624
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    :cond_10
    move v0, v4

    .line 628
    goto :goto_c

    :cond_11
    move v1, v0

    goto/16 :goto_8

    :cond_12
    move v0, v1

    goto/16 :goto_7

    :cond_13
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/e;->newBuilder()Lcom/google/maps/g/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/g;->a(Lcom/google/maps/g/e;)Lcom/google/maps/g/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/e;->newBuilder()Lcom/google/maps/g/g;

    move-result-object v0

    return-object v0
.end method
