.class public final Lcom/google/maps/g/ha;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/gy;",
        "Lcom/google/maps/g/ha;",
        ">;",
        "Lcom/google/maps/g/hb;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:D

.field public c:D


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/google/maps/g/gy;->d:Lcom/google/maps/g/gy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 236
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 267
    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 275
    :goto_0
    return-object p0

    .line 268
    :cond_0
    iget v2, p1, Lcom/google/maps/g/gy;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 269
    iget-wide v2, p1, Lcom/google/maps/g/gy;->b:D

    iget v4, p0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/ha;->b:D

    .line 271
    :cond_1
    iget v2, p1, Lcom/google/maps/g/gy;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 272
    iget-wide v0, p1, Lcom/google/maps/g/gy;->c:D

    iget v2, p0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ha;->a:I

    iput-wide v0, p0, Lcom/google/maps/g/ha;->c:D

    .line 274
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/gy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 268
    goto :goto_1

    :cond_4
    move v0, v1

    .line 271
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/maps/g/ha;->c()Lcom/google/maps/g/gy;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 227
    check-cast p1, Lcom/google/maps/g/gy;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/gy;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 251
    new-instance v2, Lcom/google/maps/g/gy;

    invoke-direct {v2, p0}, Lcom/google/maps/g/gy;-><init>(Lcom/google/n/v;)V

    .line 252
    iget v3, p0, Lcom/google/maps/g/ha;->a:I

    .line 253
    const/4 v1, 0x0

    .line 254
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 257
    :goto_0
    iget-wide v4, p0, Lcom/google/maps/g/ha;->b:D

    iput-wide v4, v2, Lcom/google/maps/g/gy;->b:D

    .line 258
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 259
    or-int/lit8 v0, v0, 0x2

    .line 261
    :cond_0
    iget-wide v4, p0, Lcom/google/maps/g/ha;->c:D

    iput-wide v4, v2, Lcom/google/maps/g/gy;->c:D

    .line 262
    iput v0, v2, Lcom/google/maps/g/gy;->a:I

    .line 263
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
