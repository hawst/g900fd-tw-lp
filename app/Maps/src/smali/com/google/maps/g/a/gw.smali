.class public final Lcom/google/maps/g/a/gw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/gx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/gu;",
        "Lcom/google/maps/g/a/gw;",
        ">;",
        "Lcom/google/maps/g/a/gx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 612
    sget-object v0, Lcom/google/maps/g/a/gu;->l:Lcom/google/maps/g/a/gu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 764
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->b:Ljava/lang/Object;

    .line 840
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->c:Ljava/lang/Object;

    .line 916
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->d:Lcom/google/n/ao;

    .line 975
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->e:Lcom/google/n/ao;

    .line 1034
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->f:Lcom/google/n/ao;

    .line 1093
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->g:Lcom/google/n/ao;

    .line 1152
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->h:Lcom/google/n/ao;

    .line 1211
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->i:Ljava/lang/Object;

    .line 1287
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->j:Ljava/lang/Object;

    .line 1364
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    .line 613
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/gu;)Lcom/google/maps/g/a/gw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 703
    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 755
    :goto_0
    return-object p0

    .line 704
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 705
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 706
    iget-object v2, p1, Lcom/google/maps/g/a/gu;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gw;->b:Ljava/lang/Object;

    .line 709
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 710
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 711
    iget-object v2, p1, Lcom/google/maps/g/a/gu;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gw;->c:Ljava/lang/Object;

    .line 714
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 715
    iget-object v2, p0, Lcom/google/maps/g/a/gw;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 716
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 718
    :cond_3
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 719
    iget-object v2, p0, Lcom/google/maps/g/a/gw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 720
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 722
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 723
    iget-object v2, p0, Lcom/google/maps/g/a/gw;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gu;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 724
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 726
    :cond_5
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 727
    iget-object v2, p0, Lcom/google/maps/g/a/gw;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gu;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 728
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 730
    :cond_6
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 731
    iget-object v2, p0, Lcom/google/maps/g/a/gw;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/gu;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 732
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 734
    :cond_7
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 735
    iget v2, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 736
    iget-object v2, p1, Lcom/google/maps/g/a/gu;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/gw;->i:Ljava/lang/Object;

    .line 739
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    :goto_9
    if-eqz v0, :cond_9

    .line 740
    iget v0, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 741
    iget-object v0, p1, Lcom/google/maps/g/a/gu;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->j:Ljava/lang/Object;

    .line 744
    :cond_9
    iget-object v0, p1, Lcom/google/maps/g/a/gu;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 745
    iget-object v0, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 746
    iget-object v0, p1, Lcom/google/maps/g/a/gu;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    .line 747
    iget v0, p0, Lcom/google/maps/g/a/gw;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 754
    :cond_a
    :goto_a
    iget-object v0, p1, Lcom/google/maps/g/a/gu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 704
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 709
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 714
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 718
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 722
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 726
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 730
    goto :goto_7

    :cond_12
    move v2, v1

    .line 734
    goto :goto_8

    :cond_13
    move v0, v1

    .line 739
    goto :goto_9

    .line 749
    :cond_14
    iget v0, p0, Lcom/google/maps/g/a/gw;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/a/gw;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/gw;->a:I

    .line 750
    :cond_15
    iget-object v0, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/a/gu;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 604
    new-instance v2, Lcom/google/maps/g/a/gu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/gu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/gw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/a/gw;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gu;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/a/gw;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/a/gu;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gw;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gw;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gw;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gw;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/a/gu;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gw;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gw;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/maps/g/a/gu;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gw;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gw;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/a/gu;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/gw;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/gw;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/a/gw;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/gu;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/a/gw;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/gu;->j:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/a/gw;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    iget-object v1, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/a/gw;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/g/a/gw;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/a/gw;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/a/gu;->k:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/a/gu;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 604
    check-cast p1, Lcom/google/maps/g/a/gu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/gw;->a(Lcom/google/maps/g/a/gu;)Lcom/google/maps/g/a/gw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 759
    const/4 v0, 0x1

    return v0
.end method
