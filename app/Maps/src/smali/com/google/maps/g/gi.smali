.class public final Lcom/google/maps/g/gi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/gg;",
        "Lcom/google/maps/g/gi;",
        ">;",
        "Lcom/google/maps/g/gj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/aq;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/google/maps/g/gg;->d:Lcom/google/maps/g/gg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 348
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    .line 441
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gi;->c:Ljava/lang/Object;

    .line 290
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/gg;)Lcom/google/maps/g/gi;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 322
    invoke-static {}, Lcom/google/maps/g/gg;->d()Lcom/google/maps/g/gg;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 339
    :goto_0
    return-object p0

    .line 323
    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 324
    iget-object v1, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 325
    iget-object v1, p1, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    iput-object v1, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    .line 326
    iget v1, p0, Lcom/google/maps/g/gi;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/g/gi;->a:I

    .line 333
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/maps/g/gg;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 334
    iget v0, p0, Lcom/google/maps/g/gi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/gi;->a:I

    .line 335
    iget-object v0, p1, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/gi;->c:Ljava/lang/Object;

    .line 338
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/gg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 328
    :cond_3
    iget v1, p0, Lcom/google/maps/g/gi;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Lcom/google/n/ap;

    iget-object v2, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    invoke-direct {v1, v2}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v1, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/gi;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/gi;->a:I

    .line 329
    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    iget-object v2, p1, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 333
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 281
    new-instance v2, Lcom/google/maps/g/gg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/gg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/gi;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/maps/g/gi;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/maps/g/gi;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/gi;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/gi;->b:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/gi;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/gg;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 281
    check-cast p1, Lcom/google/maps/g/gg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/gi;->a(Lcom/google/maps/g/gg;)Lcom/google/maps/g/gi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x1

    return v0
.end method
