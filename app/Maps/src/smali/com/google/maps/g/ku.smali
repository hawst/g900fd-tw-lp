.class public final Lcom/google/maps/g/ku;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ku;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/ku;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/maps/g/kv;

    invoke-direct {v0}, Lcom/google/maps/g/kv;-><init>()V

    sput-object v0, Lcom/google/maps/g/ku;->PARSER:Lcom/google/n/ax;

    .line 255
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ku;->i:Lcom/google/n/aw;

    .line 713
    new-instance v0, Lcom/google/maps/g/ku;

    invoke-direct {v0}, Lcom/google/maps/g/ku;-><init>()V

    sput-object v0, Lcom/google/maps/g/ku;->f:Lcom/google/maps/g/ku;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 118
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    .line 134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    .line 192
    iput-byte v2, p0, Lcom/google/maps/g/ku;->g:B

    .line 226
    iput v2, p0, Lcom/google/maps/g/ku;->h:I

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/ku;->b:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/ku;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 34
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 36
    sparse-switch v1, :sswitch_data_0

    .line 41
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 43
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v1, p0, Lcom/google/maps/g/ku;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/ku;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/maps/g/ku;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 75
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v10, :cond_1

    .line 81
    iget-object v1, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    .line 83
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ku;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    move v1, v3

    .line 49
    goto :goto_1

    .line 53
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 54
    iget v1, p0, Lcom/google/maps/g/ku;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/ku;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 76
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 77
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 78
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    iget-object v1, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 59
    iget v1, p0, Lcom/google/maps/g/ku;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/ku;->a:I

    goto :goto_0

    .line 80
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_2

    .line 63
    :sswitch_4
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v10, :cond_3

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    .line 66
    or-int/lit8 v0, v0, 0x8

    .line 68
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 68
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 80
    :cond_4
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v10, :cond_5

    .line 81
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    .line 83
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ku;->au:Lcom/google/n/bn;

    .line 84
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 118
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    .line 134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    .line 192
    iput-byte v1, p0, Lcom/google/maps/g/ku;->g:B

    .line 226
    iput v1, p0, Lcom/google/maps/g/ku;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/ku;
    .locals 1

    .prologue
    .line 716
    sget-object v0, Lcom/google/maps/g/ku;->f:Lcom/google/maps/g/ku;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/kw;
    .locals 1

    .prologue
    .line 317
    new-instance v0, Lcom/google/maps/g/kw;

    invoke-direct {v0}, Lcom/google/maps/g/kw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ku;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lcom/google/maps/g/ku;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 210
    invoke-virtual {p0}, Lcom/google/maps/g/ku;->c()I

    .line 211
    iget v2, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 212
    iget-boolean v2, p0, Lcom/google/maps/g/ku;->b:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_3

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 214
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 215
    iget-object v0, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 217
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 218
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 220
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 212
    goto :goto_0

    .line 223
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/ku;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 224
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 194
    iget-byte v0, p0, Lcom/google/maps/g/ku;->g:B

    .line 195
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 205
    :cond_0
    :goto_0
    return v2

    .line 196
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 198
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 199
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/e;->d()Lcom/google/geo/b/e;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/e;

    invoke-virtual {v0}, Lcom/google/geo/b/e;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 200
    iput-byte v2, p0, Lcom/google/maps/g/ku;->g:B

    goto :goto_0

    .line 198
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 204
    :cond_3
    iput-byte v3, p0, Lcom/google/maps/g/ku;->g:B

    move v2, v3

    .line 205
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 228
    iget v0, p0, Lcom/google/maps/g/ku;->h:I

    .line 229
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 250
    :goto_0
    return v0

    .line 232
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 233
    iget-boolean v0, p0, Lcom/google/maps/g/ku;->b:Z

    .line 234
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 236
    :goto_1
    iget v2, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 237
    iget-object v2, p0, Lcom/google/maps/g/ku;->c:Lcom/google/n/ao;

    .line 238
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 240
    :cond_1
    iget v2, p0, Lcom/google/maps/g/ku;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 241
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/g/ku;->d:Lcom/google/n/ao;

    .line 242
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_2
    move v2, v1

    move v3, v0

    .line 244
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 245
    iget-object v0, p0, Lcom/google/maps/g/ku;->e:Ljava/util/List;

    .line 246
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 244
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ku;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 249
    iput v0, p0, Lcom/google/maps/g/ku;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ku;->newBuilder()Lcom/google/maps/g/kw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/kw;->a(Lcom/google/maps/g/ku;)Lcom/google/maps/g/kw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ku;->newBuilder()Lcom/google/maps/g/kw;

    move-result-object v0

    return-object v0
.end method
