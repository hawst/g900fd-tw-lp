.class public final Lcom/google/maps/g/g/d;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/i;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/d;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/maps/g/g/d;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/google/maps/g/g/e;

    invoke-direct {v0}, Lcom/google/maps/g/g/e;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/d;->PARSER:Lcom/google/n/ax;

    .line 286
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/g/d;->f:Lcom/google/n/aw;

    .line 446
    new-instance v0, Lcom/google/maps/g/g/d;

    invoke-direct {v0}, Lcom/google/maps/g/g/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/d;->c:Lcom/google/maps/g/g/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 111
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 250
    iput-byte v0, p0, Lcom/google/maps/g/g/d;->d:B

    .line 269
    iput v0, p0, Lcom/google/maps/g/g/d;->e:I

    .line 112
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/g/g/d;->b:I

    .line 113
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 119
    invoke-direct {p0}, Lcom/google/maps/g/g/d;-><init>()V

    .line 120
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 124
    const/4 v0, 0x0

    .line 125
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 126
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 127
    sparse-switch v3, :sswitch_data_0

    .line 132
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 134
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 130
    goto :goto_0

    .line 139
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 140
    invoke-static {v3}, Lcom/google/maps/g/g/g;->a(I)Lcom/google/maps/g/g/g;

    move-result-object v4

    .line 141
    if-nez v4, :cond_1

    .line 142
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/d;->au:Lcom/google/n/bn;

    throw v0

    .line 144
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/maps/g/g/d;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/g/d;->a:I

    .line 145
    iput v3, p0, Lcom/google/maps/g/g/d;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 153
    :catch_1
    move-exception v0

    .line 154
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 155
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/d;->au:Lcom/google/n/bn;

    .line 158
    return-void

    .line 127
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 109
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 250
    iput-byte v0, p0, Lcom/google/maps/g/g/d;->d:B

    .line 269
    iput v0, p0, Lcom/google/maps/g/g/d;->e:I

    .line 110
    return-void
.end method

.method public static d()Lcom/google/maps/g/g/d;
    .locals 1

    .prologue
    .line 449
    sget-object v0, Lcom/google/maps/g/g/d;->c:Lcom/google/maps/g/g/d;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/g/f;
    .locals 1

    .prologue
    .line 348
    new-instance v0, Lcom/google/maps/g/g/f;

    invoke-direct {v0}, Lcom/google/maps/g/g/f;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    sget-object v0, Lcom/google/maps/g/g/d;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 262
    invoke-virtual {p0}, Lcom/google/maps/g/g/d;->c()I

    .line 263
    iget v0, p0, Lcom/google/maps/g/g/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 264
    iget v0, p0, Lcom/google/maps/g/g/d;->b:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 266
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/g/d;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 267
    return-void

    .line 264
    :cond_1
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 252
    iget-byte v1, p0, Lcom/google/maps/g/g/d;->d:B

    .line 253
    if-ne v1, v0, :cond_0

    .line 257
    :goto_0
    return v0

    .line 254
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 256
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/g/d;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 271
    iget v1, p0, Lcom/google/maps/g/g/d;->e:I

    .line 272
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 281
    :goto_0
    return v0

    .line 275
    :cond_0
    iget v1, p0, Lcom/google/maps/g/g/d;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 276
    iget v1, p0, Lcom/google/maps/g/g/d;->b:I

    .line 277
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 279
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/g/d;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    iput v0, p0, Lcom/google/maps/g/g/d;->e:I

    goto :goto_0

    .line 277
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/google/maps/g/g/d;->newBuilder()Lcom/google/maps/g/g/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/g/f;->a(Lcom/google/maps/g/g/d;)Lcom/google/maps/g/g/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/google/maps/g/g/d;->newBuilder()Lcom/google/maps/g/g/f;

    move-result-object v0

    return-object v0
.end method
