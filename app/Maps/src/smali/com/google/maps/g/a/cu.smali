.class public final Lcom/google/maps/g/a/cu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/cs;",
        "Lcom/google/maps/g/a/cu;",
        ">;",
        "Lcom/google/maps/g/a/cv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:D

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/google/maps/g/a/cs;->e:Lcom/google/maps/g/a/cs;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cu;->c:Ljava/lang/Object;

    .line 489
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/cu;->d:Ljava/lang/Object;

    .line 320
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 357
    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 372
    :goto_0
    return-object p0

    .line 358
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/cs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 359
    iget-wide v2, p1, Lcom/google/maps/g/a/cs;->b:D

    iget v4, p0, Lcom/google/maps/g/a/cu;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/a/cu;->a:I

    iput-wide v2, p0, Lcom/google/maps/g/a/cu;->b:D

    .line 361
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/cs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 362
    iget v2, p0, Lcom/google/maps/g/a/cu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/cu;->a:I

    .line 363
    iget-object v2, p1, Lcom/google/maps/g/a/cs;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/a/cu;->c:Ljava/lang/Object;

    .line 366
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/cs;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 367
    iget v0, p0, Lcom/google/maps/g/a/cu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/cu;->a:I

    .line 368
    iget-object v0, p1, Lcom/google/maps/g/a/cs;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/a/cu;->d:Ljava/lang/Object;

    .line 371
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/a/cs;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 358
    goto :goto_1

    :cond_5
    move v2, v1

    .line 361
    goto :goto_2

    :cond_6
    move v0, v1

    .line 366
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/maps/g/a/cu;->c()Lcom/google/maps/g/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 311
    check-cast p1, Lcom/google/maps/g/a/cs;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/cu;->a(Lcom/google/maps/g/a/cs;)Lcom/google/maps/g/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/a/cs;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 337
    new-instance v2, Lcom/google/maps/g/a/cs;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/cs;-><init>(Lcom/google/n/v;)V

    .line 338
    iget v3, p0, Lcom/google/maps/g/a/cu;->a:I

    .line 339
    const/4 v1, 0x0

    .line 340
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 343
    :goto_0
    iget-wide v4, p0, Lcom/google/maps/g/a/cu;->b:D

    iput-wide v4, v2, Lcom/google/maps/g/a/cs;->b:D

    .line 344
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 345
    or-int/lit8 v0, v0, 0x2

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/a/cu;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/cs;->c:Ljava/lang/Object;

    .line 348
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 349
    or-int/lit8 v0, v0, 0x4

    .line 351
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/cu;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/a/cs;->d:Ljava/lang/Object;

    .line 352
    iput v0, v2, Lcom/google/maps/g/a/cs;->a:I

    .line 353
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
