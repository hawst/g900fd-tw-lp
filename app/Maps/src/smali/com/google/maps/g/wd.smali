.class public final Lcom/google/maps/g/wd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/wb;",
        "Lcom/google/maps/g/wd;",
        ">;",
        "Lcom/google/maps/g/wi;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 744
    sget-object v0, Lcom/google/maps/g/wb;->n:Lcom/google/maps/g/wb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 924
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wd;->b:Ljava/lang/Object;

    .line 1000
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wd;->c:Ljava/lang/Object;

    .line 1077
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    .line 1214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    .line 1350
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/wd;->f:Lcom/google/n/ao;

    .line 1473
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/wd;->i:Ljava/lang/Object;

    .line 1582
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    .line 1751
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    .line 745
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 842
    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 915
    :goto_0
    return-object p0

    .line 843
    :cond_0
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_d

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 844
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 845
    iget-object v2, p1, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/wd;->b:Ljava/lang/Object;

    .line 848
    :cond_1
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 849
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 850
    iget-object v2, p1, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/wd;->c:Ljava/lang/Object;

    .line 853
    :cond_2
    iget-object v2, p1, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 854
    iget-object v2, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 855
    iget-object v2, p1, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    .line 856
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 863
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 864
    iget-object v2, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 865
    iget-object v2, p1, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    .line 866
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 873
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 874
    iget-object v2, p0, Lcom/google/maps/g/wd;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 875
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 877
    :cond_5
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 878
    iget-boolean v2, p1, Lcom/google/maps/g/wb;->g:Z

    iget v3, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/wd;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/wd;->g:Z

    .line 880
    :cond_6
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 881
    iget-boolean v2, p1, Lcom/google/maps/g/wb;->h:Z

    iget v3, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/wd;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/wd;->h:Z

    .line 883
    :cond_7
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 884
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 885
    iget-object v2, p1, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/wd;->i:Ljava/lang/Object;

    .line 888
    :cond_8
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 889
    iget-boolean v2, p1, Lcom/google/maps/g/wb;->j:Z

    iget v3, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/wd;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/wd;->j:Z

    .line 891
    :cond_9
    iget-object v2, p1, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 892
    iget-object v2, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 893
    iget-object v2, p1, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    .line 894
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 901
    :cond_a
    :goto_a
    iget v2, p1, Lcom/google/maps/g/wb;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    :goto_b
    if-eqz v0, :cond_b

    .line 902
    iget-boolean v0, p1, Lcom/google/maps/g/wb;->l:Z

    iget v1, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/maps/g/wd;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/wd;->l:Z

    .line 904
    :cond_b
    iget-object v0, p1, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 905
    iget-object v0, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 906
    iget-object v0, p1, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    .line 907
    iget v0, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/maps/g/wd;->a:I

    .line 914
    :cond_c
    :goto_c
    iget-object v0, p1, Lcom/google/maps/g/wb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 843
    goto/16 :goto_1

    :cond_e
    move v2, v1

    .line 848
    goto/16 :goto_2

    .line 858
    :cond_f
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_10

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 859
    :cond_10
    iget-object v2, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 868
    :cond_11
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_12

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 869
    :cond_12
    iget-object v2, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 873
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 877
    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 880
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 883
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 888
    goto/16 :goto_9

    .line 896
    :cond_18
    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-eq v2, v3, :cond_19

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/wd;->a:I

    .line 897
    :cond_19
    iget-object v2, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_1a
    move v0, v1

    .line 901
    goto/16 :goto_b

    .line 909
    :cond_1b
    iget v0, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_1c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/wd;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/wd;->a:I

    .line 910
    :cond_1c
    iget-object v0, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/google/maps/g/wd;->c()Lcom/google/maps/g/wb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 736
    check-cast p1, Lcom/google/maps/g/wb;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 919
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/wb;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 780
    new-instance v2, Lcom/google/maps/g/wb;

    invoke-direct {v2, p0}, Lcom/google/maps/g/wb;-><init>(Lcom/google/n/v;)V

    .line 781
    iget v3, p0, Lcom/google/maps/g/wd;->a:I

    .line 783
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    .line 786
    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/wd;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/wb;->b:Ljava/lang/Object;

    .line 787
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 788
    or-int/lit8 v0, v0, 0x2

    .line 790
    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/wd;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/wb;->c:Ljava/lang/Object;

    .line 791
    iget v4, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 792
    iget-object v4, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    .line 793
    iget v4, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/maps/g/wd;->a:I

    .line 795
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/wd;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/wb;->d:Ljava/util/List;

    .line 796
    iget v4, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 797
    iget-object v4, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    .line 798
    iget v4, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/maps/g/wd;->a:I

    .line 800
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/wd;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/wb;->e:Ljava/util/List;

    .line 801
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 802
    or-int/lit8 v0, v0, 0x4

    .line 804
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/wb;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/wd;->f:Lcom/google/n/ao;

    .line 805
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/wd;->f:Lcom/google/n/ao;

    .line 806
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 804
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 807
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 808
    or-int/lit8 v0, v0, 0x8

    .line 810
    :cond_4
    iget-boolean v1, p0, Lcom/google/maps/g/wd;->g:Z

    iput-boolean v1, v2, Lcom/google/maps/g/wb;->g:Z

    .line 811
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 812
    or-int/lit8 v0, v0, 0x10

    .line 814
    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/wd;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/wb;->h:Z

    .line 815
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 816
    or-int/lit8 v0, v0, 0x20

    .line 818
    :cond_6
    iget-object v1, p0, Lcom/google/maps/g/wd;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/wb;->i:Ljava/lang/Object;

    .line 819
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 820
    or-int/lit8 v0, v0, 0x40

    .line 822
    :cond_7
    iget-boolean v1, p0, Lcom/google/maps/g/wd;->j:Z

    iput-boolean v1, v2, Lcom/google/maps/g/wb;->j:Z

    .line 823
    iget v1, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 824
    iget-object v1, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    .line 825
    iget v1, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/g/wd;->a:I

    .line 827
    :cond_8
    iget-object v1, p0, Lcom/google/maps/g/wd;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/wb;->k:Ljava/util/List;

    .line 828
    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    .line 829
    or-int/lit16 v0, v0, 0x80

    .line 831
    :cond_9
    iget-boolean v1, p0, Lcom/google/maps/g/wd;->l:Z

    iput-boolean v1, v2, Lcom/google/maps/g/wb;->l:Z

    .line 832
    iget v1, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    .line 833
    iget-object v1, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    .line 834
    iget v1, p0, Lcom/google/maps/g/wd;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lcom/google/maps/g/wd;->a:I

    .line 836
    :cond_a
    iget-object v1, p0, Lcom/google/maps/g/wd;->m:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/wb;->m:Ljava/util/List;

    .line 837
    iput v0, v2, Lcom/google/maps/g/wb;->a:I

    .line 838
    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method
