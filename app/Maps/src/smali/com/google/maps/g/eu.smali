.class public final Lcom/google/maps/g/eu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ex;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/eu;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/eu;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/aq;

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/aq;

.field public f:Ljava/lang/Object;

.field public g:Z

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/google/maps/g/ev;

    invoke-direct {v0}, Lcom/google/maps/g/ev;-><init>()V

    sput-object v0, Lcom/google/maps/g/eu;->PARSER:Lcom/google/n/ax;

    .line 403
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/eu;->k:Lcom/google/n/aw;

    .line 1040
    new-instance v0, Lcom/google/maps/g/eu;

    invoke-direct {v0}, Lcom/google/maps/g/eu;-><init>()V

    sput-object v0, Lcom/google/maps/g/eu;->h:Lcom/google/maps/g/eu;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 322
    iput-byte v0, p0, Lcom/google/maps/g/eu;->i:B

    .line 356
    iput v0, p0, Lcom/google/maps/g/eu;->j:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/eu;->g:Z

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/eu;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 36
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 38
    sparse-switch v0, :sswitch_data_0

    .line 43
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 45
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 51
    iget v6, p0, Lcom/google/maps/g/eu;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/eu;->a:I

    .line 52
    iput-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v10, :cond_1

    .line 99
    iget-object v2, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 101
    :cond_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v11, :cond_2

    .line 102
    iget-object v1, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    .line 104
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/eu;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 57
    and-int/lit8 v6, v1, 0x2

    if-eq v6, v10, :cond_3

    .line 58
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 59
    or-int/lit8 v1, v1, 0x2

    .line 61
    :cond_3
    iget-object v6, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 96
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 66
    and-int/lit8 v6, v1, 0x8

    if-eq v6, v11, :cond_4

    .line 67
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    .line 68
    or-int/lit8 v1, v1, 0x8

    .line 70
    :cond_4
    iget-object v6, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 75
    iget v6, p0, Lcom/google/maps/g/eu;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/maps/g/eu;->a:I

    .line 76
    iput-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 80
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/eu;->a:I

    .line 81
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/eu;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 85
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v6, p0, Lcom/google/maps/g/eu;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/eu;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 98
    :cond_6
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v10, :cond_7

    .line 99
    iget-object v0, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 101
    :cond_7
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v11, :cond_8

    .line 102
    iget-object v0, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    .line 104
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->au:Lcom/google/n/bn;

    .line 105
    return-void

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 322
    iput-byte v0, p0, Lcom/google/maps/g/eu;->i:B

    .line 356
    iput v0, p0, Lcom/google/maps/g/eu;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/eu;
    .locals 1

    .prologue
    .line 1043
    sget-object v0, Lcom/google/maps/g/eu;->h:Lcom/google/maps/g/eu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ew;
    .locals 1

    .prologue
    .line 465
    new-instance v0, Lcom/google/maps/g/ew;

    invoke-direct {v0}, Lcom/google/maps/g/ew;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/eu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lcom/google/maps/g/eu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x2

    .line 334
    invoke-virtual {p0}, Lcom/google/maps/g/eu;->c()I

    .line 335
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v0, v1

    .line 338
    :goto_1
    iget-object v3, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 339
    iget-object v3, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 336
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 341
    :goto_2
    iget-object v3, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 342
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 345
    iget-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 347
    :cond_4
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    .line 348
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/eu;->g:Z

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_5

    move v1, v2

    :cond_5
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 350
    :cond_6
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_7

    .line 351
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 353
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/eu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 354
    return-void

    .line 345
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 351
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 324
    iget-byte v1, p0, Lcom/google/maps/g/eu;->i:B

    .line 325
    if-ne v1, v0, :cond_0

    .line 329
    :goto_0
    return v0

    .line 326
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 328
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/eu;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 358
    iget v0, p0, Lcom/google/maps/g/eu;->j:I

    .line 359
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 398
    :goto_0
    return v0

    .line 362
    :cond_0
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 364
    iget-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v1

    .line 368
    :goto_3
    iget-object v4, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 369
    iget-object v4, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 370
    invoke-interface {v4, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 368
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 364
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 372
    :cond_2
    add-int/2addr v0, v3

    .line 373
    iget-object v2, p0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 377
    :goto_4
    iget-object v4, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 378
    iget-object v4, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    .line 379
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 381
    :cond_3
    add-int v0, v3, v2

    .line 382
    iget-object v2, p0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 384
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_8

    .line 386
    iget-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 388
    :goto_6
    iget v2, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_7

    .line 389
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/maps/g/eu;->g:Z

    .line 390
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 392
    :goto_7
    iget v0, p0, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    .line 393
    const/4 v3, 0x6

    .line 394
    iget-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 396
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/eu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 397
    iput v0, p0, Lcom/google/maps/g/eu;->j:I

    goto/16 :goto_0

    .line 386
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 394
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_7
    move v2, v0

    goto :goto_7

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/eu;->newBuilder()Lcom/google/maps/g/ew;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ew;->a(Lcom/google/maps/g/eu;)Lcom/google/maps/g/ew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/eu;->newBuilder()Lcom/google/maps/g/ew;

    move-result-object v0

    return-object v0
.end method
