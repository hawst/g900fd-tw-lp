.class public final Lcom/google/maps/g/fm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/fn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/fk;",
        "Lcom/google/maps/g/fm;",
        ">;",
        "Lcom/google/maps/g/fn;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Lcom/google/n/aq;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 381
    sget-object v0, Lcom/google/maps/g/fk;->f:Lcom/google/maps/g/fk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 466
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/fm;->e:Ljava/lang/Object;

    .line 543
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    .line 711
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    .line 382
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/fk;)Lcom/google/maps/g/fm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 427
    invoke-static {}, Lcom/google/maps/g/fk;->d()Lcom/google/maps/g/fk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 457
    :goto_0
    return-object p0

    .line 428
    :cond_0
    iget v2, p1, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 429
    iget v2, p0, Lcom/google/maps/g/fm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/fm;->a:I

    .line 430
    iget-object v2, p1, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/fm;->e:Ljava/lang/Object;

    .line 433
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 434
    iget-object v2, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 435
    iget-object v2, p1, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    .line 436
    iget v2, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/fm;->a:I

    .line 443
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/fk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 444
    iget-boolean v0, p1, Lcom/google/maps/g/fk;->d:Z

    iget v1, p0, Lcom/google/maps/g/fm;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/fm;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/fm;->c:Z

    .line 446
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 447
    iget-object v0, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 448
    iget-object v0, p1, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    .line 449
    iget v0, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/g/fm;->a:I

    .line 456
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/maps/g/fk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 428
    goto :goto_1

    .line 438
    :cond_6
    invoke-virtual {p0}, Lcom/google/maps/g/fm;->c()V

    .line 439
    iget-object v2, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v0, v1

    .line 443
    goto :goto_3

    .line 451
    :cond_8
    invoke-virtual {p0}, Lcom/google/maps/g/fm;->d()V

    .line 452
    iget-object v0, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 373
    new-instance v2, Lcom/google/maps/g/fk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/fk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/fm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/fm;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/fk;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/fm;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v1, p0, Lcom/google/maps/g/fm;->c:Z

    iput-boolean v1, v2, Lcom/google/maps/g/fk;->d:Z

    iget v1, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/fm;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/maps/g/fk;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 373
    check-cast p1, Lcom/google/maps/g/fk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/fm;->a(Lcom/google/maps/g/fk;)Lcom/google/maps/g/fm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 545
    iget v0, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 546
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    .line 549
    iget v0, p0, Lcom/google/maps/g/fm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/fm;->a:I

    .line 551
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 713
    iget v0, p0, Lcom/google/maps/g/fm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 714
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    .line 715
    iget v0, p0, Lcom/google/maps/g/fm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/fm;->a:I

    .line 717
    :cond_0
    return-void
.end method
