.class public final Lcom/google/maps/g/g/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/g/a;",
        "Lcom/google/maps/g/g/c;",
        ">;",
        "Lcom/google/maps/g/g/j;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1172
    sget-object v0, Lcom/google/maps/g/g/a;->e:Lcom/google/maps/g/g/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1236
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g/c;->b:Lcom/google/n/ao;

    .line 1295
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g/c;->c:Lcom/google/n/ao;

    .line 1173
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/g/a;)Lcom/google/maps/g/g/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1214
    invoke-static {}, Lcom/google/maps/g/g/a;->d()Lcom/google/maps/g/g/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1227
    :goto_0
    return-object p0

    .line 1215
    :cond_0
    iget v2, p1, Lcom/google/maps/g/g/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1216
    iget-object v2, p0, Lcom/google/maps/g/g/c;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/g/a;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1217
    iget v2, p0, Lcom/google/maps/g/g/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/g/c;->a:I

    .line 1219
    :cond_1
    iget v2, p1, Lcom/google/maps/g/g/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1220
    iget-object v2, p0, Lcom/google/maps/g/g/c;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/g/a;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1221
    iget v2, p0, Lcom/google/maps/g/g/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/g/c;->a:I

    .line 1223
    :cond_2
    iget v2, p1, Lcom/google/maps/g/g/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1224
    iget-boolean v0, p1, Lcom/google/maps/g/g/a;->d:Z

    iget v1, p0, Lcom/google/maps/g/g/c;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/g/c;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/g/c;->d:Z

    .line 1226
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/g/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1215
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1219
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1223
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1164
    new-instance v2, Lcom/google/maps/g/g/a;

    invoke-direct {v2, p0}, Lcom/google/maps/g/g/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/g/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/g/a;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/g/c;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/g/c;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/g/a;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/g/c;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/g/c;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/maps/g/g/c;->d:Z

    iput-boolean v1, v2, Lcom/google/maps/g/g/a;->d:Z

    iput v0, v2, Lcom/google/maps/g/g/a;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1164
    check-cast p1, Lcom/google/maps/g/g/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/g/c;->a(Lcom/google/maps/g/g/a;)Lcom/google/maps/g/g/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1231
    const/4 v0, 0x1

    return v0
.end method
