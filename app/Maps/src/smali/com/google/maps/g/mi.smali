.class public final Lcom/google/maps/g/mi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/mj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/mg;",
        "Lcom/google/maps/g/mi;",
        ">;",
        "Lcom/google/maps/g/mj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/google/maps/g/mg;->d:Lcom/google/maps/g/mg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 292
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mi;->b:Lcom/google/n/ao;

    .line 351
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/mi;->c:Lcom/google/n/ao;

    .line 238
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 273
    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 283
    :goto_0
    return-object p0

    .line 274
    :cond_0
    iget v2, p1, Lcom/google/maps/g/mg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 275
    iget-object v2, p0, Lcom/google/maps/g/mi;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/mg;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 276
    iget v2, p0, Lcom/google/maps/g/mi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/mi;->a:I

    .line 278
    :cond_1
    iget v2, p1, Lcom/google/maps/g/mg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/google/maps/g/mi;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/mg;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 280
    iget v0, p0, Lcom/google/maps/g/mi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/mi;->a:I

    .line 282
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/mg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 274
    goto :goto_1

    :cond_4
    move v0, v1

    .line 278
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/maps/g/mi;->c()Lcom/google/maps/g/mg;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 229
    check-cast p1, Lcom/google/maps/g/mg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/mi;->a(Lcom/google/maps/g/mg;)Lcom/google/maps/g/mi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/mg;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 253
    new-instance v2, Lcom/google/maps/g/mg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/mg;-><init>(Lcom/google/n/v;)V

    .line 254
    iget v3, p0, Lcom/google/maps/g/mi;->a:I

    .line 256
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 259
    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/mg;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/mi;->b:Lcom/google/n/ao;

    .line 260
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/mi;->b:Lcom/google/n/ao;

    .line 261
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 259
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 262
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 263
    or-int/lit8 v0, v0, 0x2

    .line 265
    :cond_0
    iget-object v3, v2, Lcom/google/maps/g/mg;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/mi;->c:Lcom/google/n/ao;

    .line 266
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/mi;->c:Lcom/google/n/ao;

    .line 267
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 265
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 268
    iput v0, v2, Lcom/google/maps/g/mg;->a:I

    .line 269
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
