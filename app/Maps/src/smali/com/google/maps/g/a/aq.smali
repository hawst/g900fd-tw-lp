.class public final Lcom/google/maps/g/a/aq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/at;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ao;",
        "Lcom/google/maps/g/a/aq;",
        ">;",
        "Lcom/google/maps/g/a/at;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:Lcom/google/n/ao;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 478
    sget-object v0, Lcom/google/maps/g/a/ao;->j:Lcom/google/maps/g/a/ao;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 648
    iput v1, p0, Lcom/google/maps/g/a/aq;->e:I

    .line 684
    iput-boolean v1, p0, Lcom/google/maps/g/a/aq;->f:Z

    .line 716
    iput-boolean v1, p0, Lcom/google/maps/g/a/aq;->g:Z

    .line 748
    iput-boolean v1, p0, Lcom/google/maps/g/a/aq;->h:Z

    .line 780
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/aq;->d:Lcom/google/n/ao;

    .line 479
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 548
    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 575
    :goto_0
    return-object p0

    .line 549
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 550
    iget-boolean v2, p1, Lcom/google/maps/g/a/ao;->b:Z

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/aq;->b:Z

    .line 552
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 553
    iget-boolean v2, p1, Lcom/google/maps/g/a/ao;->c:Z

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/aq;->c:Z

    .line 555
    :cond_2
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 556
    iget v2, p1, Lcom/google/maps/g/a/ao;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/ar;->a(I)Lcom/google/maps/g/a/ar;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/ar;->a:Lcom/google/maps/g/a/ar;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 549
    goto :goto_1

    :cond_5
    move v2, v1

    .line 552
    goto :goto_2

    :cond_6
    move v2, v1

    .line 555
    goto :goto_3

    .line 556
    :cond_7
    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iget v2, v2, Lcom/google/maps/g/a/ar;->d:I

    iput v2, p0, Lcom/google/maps/g/a/aq;->e:I

    .line 558
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 559
    iget-boolean v2, p1, Lcom/google/maps/g/a/ao;->e:Z

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/aq;->f:Z

    .line 561
    :cond_9
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 562
    iget-boolean v2, p1, Lcom/google/maps/g/a/ao;->f:Z

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/aq;->g:Z

    .line 564
    :cond_a
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_b

    .line 565
    iget-boolean v2, p1, Lcom/google/maps/g/a/ao;->g:Z

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/a/aq;->h:Z

    .line 567
    :cond_b
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    .line 568
    iget-object v2, p0, Lcom/google/maps/g/a/aq;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 569
    iget v2, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/a/aq;->a:I

    .line 571
    :cond_c
    iget v2, p1, Lcom/google/maps/g/a/ao;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_d

    .line 572
    iget-boolean v0, p1, Lcom/google/maps/g/a/ao;->i:Z

    iget v1, p0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/aq;->i:Z

    .line 574
    :cond_d
    iget-object v0, p1, Lcom/google/maps/g/a/ao;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 558
    goto :goto_4

    :cond_f
    move v2, v1

    .line 561
    goto :goto_5

    :cond_10
    move v2, v1

    .line 564
    goto :goto_6

    :cond_11
    move v2, v1

    .line 567
    goto :goto_7

    :cond_12
    move v0, v1

    .line 571
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 470
    new-instance v2, Lcom/google/maps/g/a/ao;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ao;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/aq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-boolean v4, p0, Lcom/google/maps/g/a/aq;->b:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ao;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/maps/g/a/aq;->c:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ao;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/g/a/aq;->e:I

    iput v4, v2, Lcom/google/maps/g/a/ao;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/maps/g/a/aq;->f:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ao;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v4, p0, Lcom/google/maps/g/a/aq;->g:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ao;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/maps/g/a/aq;->h:Z

    iput-boolean v4, v2, Lcom/google/maps/g/a/ao;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/g/a/ao;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/a/aq;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/a/aq;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v1, p0, Lcom/google/maps/g/a/aq;->i:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/ao;->i:Z

    iput v0, v2, Lcom/google/maps/g/a/ao;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 470
    check-cast p1, Lcom/google/maps/g/a/ao;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/aq;->a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 579
    const/4 v0, 0x1

    return v0
.end method
