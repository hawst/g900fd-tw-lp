.class public final Lcom/google/maps/g/nc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/nn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nc;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/nc;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/f;

.field public f:Lcom/google/n/f;

.field public g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/maps/g/nd;

    invoke-direct {v0}, Lcom/google/maps/g/nd;-><init>()V

    sput-object v0, Lcom/google/maps/g/nc;->PARSER:Lcom/google/n/ax;

    .line 451
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/nc;->k:Lcom/google/n/aw;

    .line 1069
    new-instance v0, Lcom/google/maps/g/nc;

    invoke-direct {v0}, Lcom/google/maps/g/nc;-><init>()V

    sput-object v0, Lcom/google/maps/g/nc;->h:Lcom/google/maps/g/nc;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 195
    iput v1, p0, Lcom/google/maps/g/nc;->b:I

    .line 370
    iput-byte v0, p0, Lcom/google/maps/g/nc;->i:B

    .line 410
    iput v0, p0, Lcom/google/maps/g/nc;->j:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    .line 20
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    .line 21
    iput v1, p0, Lcom/google/maps/g/nc;->g:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/nc;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 34
    :cond_0
    :goto_0
    if-nez v1, :cond_5

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 43
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/nc;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/nc;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 54
    iget v4, p0, Lcom/google/maps/g/nc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/g/nc;->a:I

    .line 55
    iput-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 106
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 107
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-eq v0, v5, :cond_1

    .line 60
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 62
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 64
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/g/nc;->b:I

    goto :goto_0

    .line 68
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-eq v0, v6, :cond_2

    .line 69
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 71
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 73
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/maps/g/nc;->b:I

    goto/16 :goto_0

    .line 77
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-eq v0, v7, :cond_3

    .line 78
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 81
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 80
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 82
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/maps/g/nc;->b:I

    goto/16 :goto_0

    .line 86
    :sswitch_6
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/nc;->a:I

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    goto/16 :goto_0

    .line 91
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 92
    invoke-static {v0}, Lcom/google/maps/g/nf;->a(I)Lcom/google/maps/g/nf;

    move-result-object v4

    .line 93
    if-nez v4, :cond_4

    .line 94
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 96
    :cond_4
    iget v4, p0, Lcom/google/maps/g/nc;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/nc;->a:I

    .line 97
    iput v0, p0, Lcom/google/maps/g/nc;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 109
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nc;->au:Lcom/google/n/bn;

    .line 110
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 195
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/nc;->b:I

    .line 370
    iput-byte v1, p0, Lcom/google/maps/g/nc;->i:B

    .line 410
    iput v1, p0, Lcom/google/maps/g/nc;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/nc;
    .locals 1

    .prologue
    .line 1072
    sget-object v0, Lcom/google/maps/g/nc;->h:Lcom/google/maps/g/nc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/nh;
    .locals 1

    .prologue
    .line 513
    new-instance v0, Lcom/google/maps/g/nh;

    invoke-direct {v0}, Lcom/google/maps/g/nh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/nc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/maps/g/nc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 382
    invoke-virtual {p0}, Lcom/google/maps/g/nc;->c()I

    .line 383
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 386
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 387
    iget-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 389
    :cond_1
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-ne v0, v5, :cond_2

    .line 390
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 391
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 390
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 393
    :cond_2
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-ne v0, v3, :cond_3

    .line 394
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 395
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 394
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 397
    :cond_3
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-ne v0, v6, :cond_4

    .line 398
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 399
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 398
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 401
    :cond_4
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_5

    .line 402
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 404
    :cond_5
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 405
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/maps/g/nc;->g:I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 407
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/nc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 408
    return-void

    .line 387
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 405
    :cond_8
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 372
    iget-byte v1, p0, Lcom/google/maps/g/nc;->i:B

    .line 373
    if-ne v1, v0, :cond_0

    .line 377
    :goto_0
    return v0

    .line 374
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 376
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/nc;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x4

    const/4 v2, 0x0

    .line 412
    iget v0, p0, Lcom/google/maps/g/nc;->j:I

    .line 413
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 446
    :goto_0
    return v0

    .line 416
    :cond_0
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_9

    .line 417
    iget-object v0, p0, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    .line 418
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 420
    :goto_1
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 424
    :cond_1
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-ne v0, v7, :cond_2

    .line 425
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 426
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 428
    :cond_2
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    if-ne v0, v5, :cond_3

    .line 429
    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 430
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 432
    :cond_3
    iget v0, p0, Lcom/google/maps/g/nc;->b:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_4

    .line 433
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 434
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 436
    :cond_4
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 437
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    .line 438
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 440
    :cond_5
    iget v0, p0, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    .line 441
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/maps/g/nc;->g:I

    .line 442
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 444
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/nc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 445
    iput v0, p0, Lcom/google/maps/g/nc;->j:I

    goto/16 :goto_0

    .line 422
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 442
    :cond_8
    const/16 v0, 0xa

    goto :goto_3

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/nc;->newBuilder()Lcom/google/maps/g/nh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/nh;->a(Lcom/google/maps/g/nc;)Lcom/google/maps/g/nh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/nc;->newBuilder()Lcom/google/maps/g/nh;

    move-result-object v0

    return-object v0
.end method
