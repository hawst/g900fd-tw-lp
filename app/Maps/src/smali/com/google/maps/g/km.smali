.class public final Lcom/google/maps/g/km;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/km;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final v:Lcom/google/maps/g/km;

.field private static volatile y:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field f:I

.field g:I

.field h:Ljava/lang/Object;

.field i:Lcom/google/n/aq;

.field j:Ljava/lang/Object;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:F

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field o:F

.field p:Lcom/google/n/ao;

.field q:Lcom/google/n/ao;

.field r:Ljava/lang/Object;

.field s:Ljava/lang/Object;

.field t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field u:Lcom/google/n/ao;

.field private w:B

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/google/maps/g/kn;

    invoke-direct {v0}, Lcom/google/maps/g/kn;-><init>()V

    sput-object v0, Lcom/google/maps/g/km;->PARSER:Lcom/google/n/ax;

    .line 985
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/km;->y:Lcom/google/n/aw;

    .line 2737
    new-instance v0, Lcom/google/maps/g/km;

    invoke-direct {v0}, Lcom/google/maps/g/km;-><init>()V

    sput-object v0, Lcom/google/maps/g/km;->v:Lcom/google/maps/g/km;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    .line 578
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    .line 594
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    .line 625
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    .line 641
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    .line 784
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    .line 799
    iput-byte v4, p0, Lcom/google/maps/g/km;->w:B

    .line 887
    iput v4, p0, Lcom/google/maps/g/km;->x:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    .line 22
    const/16 v0, 0x63

    iput v0, p0, Lcom/google/maps/g/km;->f:I

    .line 23
    iput v2, p0, Lcom/google/maps/g/km;->g:I

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    .line 28
    iput v3, p0, Lcom/google/maps/g/km;->l:F

    .line 29
    iget-object v0, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iget-object v0, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iput v3, p0, Lcom/google/maps/g/km;->o:F

    .line 32
    iget-object v0, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 33
    iget-object v0, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    .line 37
    iget-object v0, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 38
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/16 v9, 0x200

    const/16 v8, 0x80

    const/high16 v7, 0x40000

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/google/maps/g/km;-><init>()V

    .line 47
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 50
    :cond_0
    :goto_0
    if-nez v0, :cond_9

    .line 51
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 52
    sparse-switch v4, :sswitch_data_0

    .line 57
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 59
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 55
    goto :goto_0

    .line 64
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 65
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 66
    iput-object v4, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :catchall_0
    move-exception v0

    and-int v2, v1, v7

    if-ne v2, v7, :cond_1

    .line 205
    iget-object v2, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    .line 207
    :cond_1
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v8, :cond_2

    .line 208
    iget-object v2, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    .line 210
    :cond_2
    and-int/lit16 v1, v1, 0x200

    if-ne v1, v9, :cond_3

    .line 211
    iget-object v1, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    .line 213
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/km;->au:Lcom/google/n/bn;

    throw v0

    .line 70
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 71
    invoke-static {v4}, Lcom/google/maps/g/f/g;->a(I)Lcom/google/maps/g/f/g;

    move-result-object v5

    .line 72
    if-nez v5, :cond_4

    .line 73
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 200
    :catch_1
    move-exception v0

    .line 201
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 202
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 75
    :cond_4
    :try_start_4
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 76
    iput v4, p0, Lcom/google/maps/g/km;->f:I

    goto :goto_0

    .line 81
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 82
    invoke-static {v4}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v5

    .line 83
    if-nez v5, :cond_5

    .line 84
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 86
    :cond_5
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 87
    iput v4, p0, Lcom/google/maps/g/km;->g:I

    goto/16 :goto_0

    .line 92
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 93
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 94
    iput-object v4, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 98
    :sswitch_5
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/maps/g/km;->l:F

    goto/16 :goto_0

    .line 103
    :sswitch_6
    iget-object v4, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 104
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_7
    iget-object v4, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 109
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 114
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 115
    iput-object v4, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 119
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 120
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 121
    iput-object v4, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 125
    :sswitch_a
    iget-object v4, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 126
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    goto/16 :goto_0

    .line 130
    :sswitch_b
    iget-object v4, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 131
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    goto/16 :goto_0

    .line 135
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 136
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 137
    iput-object v4, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    goto/16 :goto_0

    .line 141
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 142
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 143
    iput-object v4, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    goto/16 :goto_0

    .line 147
    :sswitch_e
    and-int v4, v1, v7

    if-eq v4, v7, :cond_6

    .line 148
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    .line 150
    or-int/2addr v1, v7

    .line 152
    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 153
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 152
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 157
    :sswitch_f
    iget-object v4, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 158
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    const/high16 v5, 0x10000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    goto/16 :goto_0

    .line 162
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 163
    and-int/lit16 v5, v1, 0x80

    if-eq v5, v8, :cond_7

    .line 164
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    .line 165
    or-int/lit16 v1, v1, 0x80

    .line 167
    :cond_7
    iget-object v5, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 171
    :sswitch_11
    and-int/lit16 v4, v1, 0x200

    if-eq v4, v9, :cond_8

    .line 172
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    .line 174
    or-int/lit16 v1, v1, 0x200

    .line 176
    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 177
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 176
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 181
    :sswitch_12
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/maps/g/km;->a:I

    .line 182
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/maps/g/km;->o:F

    goto/16 :goto_0

    .line 186
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 187
    iget v5, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/maps/g/km;->a:I

    .line 188
    iput-object v4, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 192
    :sswitch_14
    iget-object v4, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 193
    iget v4, p0, Lcom/google/maps/g/km;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/g/km;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 204
    :cond_9
    and-int v0, v1, v7

    if-ne v0, v7, :cond_a

    .line 205
    iget-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    .line 207
    :cond_a
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v8, :cond_b

    .line 208
    iget-object v0, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    .line 210
    :cond_b
    and-int/lit16 v0, v1, 0x200

    if-ne v0, v9, :cond_c

    .line 211
    iget-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    .line 213
    :cond_c
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->au:Lcom/google/n/bn;

    .line 214
    return-void

    .line 52
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x35 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
        0x9a -> :sswitch_11
        0xa5 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb2 -> :sswitch_14
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    .line 578
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    .line 594
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    .line 625
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    .line 641
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    .line 784
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    .line 799
    iput-byte v1, p0, Lcom/google/maps/g/km;->w:B

    .line 887
    iput v1, p0, Lcom/google/maps/g/km;->x:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/km;
    .locals 1

    .prologue
    .line 2740
    sget-object v0, Lcom/google/maps/g/km;->v:Lcom/google/maps/g/km;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ko;
    .locals 1

    .prologue
    .line 1047
    new-instance v0, Lcom/google/maps/g/ko;

    invoke-direct {v0}, Lcom/google/maps/g/ko;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/km;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lcom/google/maps/g/km;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 823
    invoke-virtual {p0}, Lcom/google/maps/g/km;->c()I

    .line 824
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 825
    iget-object v0, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 827
    :cond_0
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 828
    iget v0, p0, Lcom/google/maps/g/km;->f:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 830
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    .line 831
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/km;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 833
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_3

    .line 834
    iget-object v0, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 836
    :cond_3
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_4

    .line 837
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/g/km;->l:F

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 839
    :cond_4
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_5

    .line 840
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 842
    :cond_5
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_6

    .line 843
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 845
    :cond_6
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 846
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 848
    :cond_7
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_8

    .line 849
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 851
    :cond_8
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_9

    .line 852
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 854
    :cond_9
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_a

    .line 855
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 857
    :cond_a
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_b

    .line 858
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 860
    :cond_b
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_c

    .line 861
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_c
    move v1, v2

    .line 863
    :goto_8
    iget-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 864
    const/16 v3, 0x10

    iget-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 863
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 825
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 828
    :cond_e
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 831
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 834
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 846
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 849
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 858
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 861
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 866
    :cond_15
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_16

    .line 867
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_16
    move v0, v2

    .line 869
    :goto_9
    iget-object v1, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_17

    .line 870
    const/16 v1, 0x12

    iget-object v3, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 869
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 872
    :cond_17
    :goto_a
    iget-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_18

    .line 873
    const/16 v1, 0x13

    iget-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 872
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 875
    :cond_18
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_19

    .line 876
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/maps/g/km;->o:F

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 878
    :cond_19
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_1a

    .line 879
    const/16 v1, 0x15

    iget-object v0, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    :goto_b
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 881
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1b

    .line 882
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 884
    :cond_1b
    iget-object v0, p0, Lcom/google/maps/g/km;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 885
    return-void

    .line 879
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto :goto_b
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x10000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 801
    iget-byte v0, p0, Lcom/google/maps/g/km;->w:B

    .line 802
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 818
    :goto_0
    return v0

    .line 803
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 805
    :cond_1
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 806
    iget-object v0, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/m;->d()Lcom/google/geo/b/m;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/m;

    invoke-virtual {v0}, Lcom/google/geo/b/m;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 807
    iput-byte v2, p0, Lcom/google/maps/g/km;->w:B

    move v0, v2

    .line 808
    goto :goto_0

    :cond_2
    move v0, v2

    .line 805
    goto :goto_1

    .line 811
    :cond_3
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 812
    iget-object v0, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ku;->d()Lcom/google/maps/g/ku;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ku;

    invoke-virtual {v0}, Lcom/google/maps/g/ku;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 813
    iput-byte v2, p0, Lcom/google/maps/g/km;->w:B

    move v0, v2

    .line 814
    goto :goto_0

    :cond_4
    move v0, v2

    .line 811
    goto :goto_2

    .line 817
    :cond_5
    iput-byte v1, p0, Lcom/google/maps/g/km;->w:B

    move v0, v1

    .line 818
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 889
    iget v0, p0, Lcom/google/maps/g/km;->x:I

    .line 890
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 980
    :goto_0
    return v0

    .line 893
    :cond_0
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1d

    .line 895
    iget-object v0, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 897
    :goto_2
    iget v2, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_1

    .line 898
    iget v2, p0, Lcom/google/maps/g/km;->f:I

    .line 899
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_d

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 901
    :cond_1
    iget v2, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_1c

    .line 902
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/g/km;->g:I

    .line 903
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    move v2, v0

    .line 905
    :goto_5
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_2

    .line 907
    iget-object v0, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 909
    :cond_2
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_3

    .line 910
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/maps/g/km;->l:F

    .line 911
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v2, v0

    .line 913
    :cond_3
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_4

    .line 914
    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/maps/g/km;->m:Lcom/google/n/ao;

    .line 915
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 917
    :cond_4
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_5

    .line 918
    const/16 v0, 0x9

    iget-object v4, p0, Lcom/google/maps/g/km;->n:Lcom/google/n/ao;

    .line 919
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 921
    :cond_5
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_6

    .line 923
    iget-object v0, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->b:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 925
    :cond_6
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_7

    .line 926
    const/16 v3, 0xb

    .line 927
    iget-object v0, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 929
    :cond_7
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_8

    .line 930
    const/16 v0, 0xc

    iget-object v3, p0, Lcom/google/maps/g/km;->p:Lcom/google/n/ao;

    .line 931
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 933
    :cond_8
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_9

    .line 934
    const/16 v0, 0xd

    iget-object v3, p0, Lcom/google/maps/g/km;->q:Lcom/google/n/ao;

    .line 935
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 937
    :cond_9
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_a

    .line 938
    const/16 v3, 0xe

    .line 939
    iget-object v0, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->r:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 941
    :cond_a
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_b

    .line 942
    const/16 v3, 0xf

    .line 943
    iget-object v0, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->s:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_b
    move v3, v2

    move v2, v1

    .line 945
    :goto_b
    iget-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_14

    .line 946
    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/maps/g/km;->t:Ljava/util/List;

    .line 947
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 945
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 895
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_d
    move v2, v3

    .line 899
    goto/16 :goto_3

    :cond_e
    move v2, v3

    .line 903
    goto/16 :goto_4

    .line 907
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 923
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 927
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 939
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 943
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    .line 949
    :cond_14
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_15

    .line 950
    const/16 v0, 0x11

    iget-object v2, p0, Lcom/google/maps/g/km;->u:Lcom/google/n/ao;

    .line 951
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_15
    move v0, v1

    move v2, v1

    .line 955
    :goto_c
    iget-object v4, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_16

    .line 956
    iget-object v4, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    .line 957
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 955
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 959
    :cond_16
    add-int v0, v3, v2

    .line 960
    iget-object v2, p0, Lcom/google/maps/g/km;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 962
    :goto_d
    iget-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 963
    const/16 v4, 0x13

    iget-object v0, p0, Lcom/google/maps/g/km;->k:Ljava/util/List;

    .line 964
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 962
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 966
    :cond_17
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_18

    .line 967
    const/16 v0, 0x14

    iget v2, p0, Lcom/google/maps/g/km;->o:F

    .line 968
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 970
    :cond_18
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_19

    .line 971
    const/16 v2, 0x15

    .line 972
    iget-object v0, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/km;->j:Ljava/lang/Object;

    :goto_e
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 974
    :cond_19
    iget v0, p0, Lcom/google/maps/g/km;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_1a

    .line 975
    const/16 v0, 0x16

    iget-object v2, p0, Lcom/google/maps/g/km;->d:Lcom/google/n/ao;

    .line 976
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 978
    :cond_1a
    iget-object v0, p0, Lcom/google/maps/g/km;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 979
    iput v0, p0, Lcom/google/maps/g/km;->x:I

    goto/16 :goto_0

    .line 972
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto :goto_e

    :cond_1c
    move v2, v0

    goto/16 :goto_5

    :cond_1d
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/km;->newBuilder()Lcom/google/maps/g/ko;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ko;->a(Lcom/google/maps/g/km;)Lcom/google/maps/g/ko;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/km;->newBuilder()Lcom/google/maps/g/ko;

    move-result-object v0

    return-object v0
.end method
