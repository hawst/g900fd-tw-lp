.class public final enum Lcom/google/maps/g/ge;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ge;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/ge;

.field public static final enum b:Lcom/google/maps/g/ge;

.field public static final enum c:Lcom/google/maps/g/ge;

.field public static final enum d:Lcom/google/maps/g/ge;

.field public static final enum e:Lcom/google/maps/g/ge;

.field public static final enum f:Lcom/google/maps/g/ge;

.field public static final enum g:Lcom/google/maps/g/ge;

.field public static final enum h:Lcom/google/maps/g/ge;

.field public static final enum i:Lcom/google/maps/g/ge;

.field public static final enum j:Lcom/google/maps/g/ge;

.field public static final enum k:Lcom/google/maps/g/ge;

.field public static final enum l:Lcom/google/maps/g/ge;

.field public static final enum m:Lcom/google/maps/g/ge;

.field public static final enum n:Lcom/google/maps/g/ge;

.field public static final enum o:Lcom/google/maps/g/ge;

.field public static final enum p:Lcom/google/maps/g/ge;

.field private static final synthetic r:[Lcom/google/maps/g/ge;


# instance fields
.field final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->a:Lcom/google/maps/g/ge;

    .line 18
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_GOOGLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->b:Lcom/google/maps/g/ge;

    .line 22
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_HOTELS"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->c:Lcom/google/maps/g/ge;

    .line 26
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_GMM_OLD"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->d:Lcom/google/maps/g/ge;

    .line 30
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_GMM"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->e:Lcom/google/maps/g/ge;

    .line 34
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_TACTILE"

    const/4 v2, 0x5

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->f:Lcom/google/maps/g/ge;

    .line 38
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_TACTILE_EMBED"

    const/4 v2, 0x6

    const/16 v3, 0x5a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->g:Lcom/google/maps/g/ge;

    .line 42
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_HANGOUTS"

    const/4 v2, 0x7

    const/16 v3, 0x5d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->h:Lcom/google/maps/g/ge;

    .line 46
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_MAPS_ACTIVITIES"

    const/16 v2, 0x8

    const/16 v3, 0x5e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->i:Lcom/google/maps/g/ge;

    .line 50
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_EARTH"

    const/16 v2, 0x9

    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->j:Lcom/google/maps/g/ge;

    .line 54
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_MAPS_API_V3"

    const/16 v2, 0xa

    const/16 v3, 0x62

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->k:Lcom/google/maps/g/ge;

    .line 58
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_GOOGLE_PLUS_LOCAL"

    const/16 v2, 0xb

    const/16 v3, 0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->l:Lcom/google/maps/g/ge;

    .line 62
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_PLACES_API"

    const/16 v2, 0xc

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->m:Lcom/google/maps/g/ge;

    .line 66
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_LOCAL_ACTIONS_BATCH"

    const/16 v2, 0xd

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->n:Lcom/google/maps/g/ge;

    .line 70
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_MAPS_LITE_DESKTOP"

    const/16 v2, 0xe

    const/16 v3, 0x6b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->o:Lcom/google/maps/g/ge;

    .line 74
    new-instance v0, Lcom/google/maps/g/ge;

    const-string v1, "PROPERTY_MAPS_LITE_MOBILE"

    const/16 v2, 0xf

    const/16 v3, 0x6c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/ge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ge;->p:Lcom/google/maps/g/ge;

    .line 9
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/maps/g/ge;

    sget-object v1, Lcom/google/maps/g/ge;->a:Lcom/google/maps/g/ge;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/ge;->b:Lcom/google/maps/g/ge;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/ge;->c:Lcom/google/maps/g/ge;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/ge;->d:Lcom/google/maps/g/ge;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/ge;->e:Lcom/google/maps/g/ge;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/ge;->f:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/ge;->g:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/ge;->h:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/ge;->i:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/ge;->j:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/ge;->k:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/ge;->l:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/g/ge;->m:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/g/ge;->n:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/ge;->o:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/g/ge;->p:Lcom/google/maps/g/ge;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/ge;->r:[Lcom/google/maps/g/ge;

    .line 174
    new-instance v0, Lcom/google/maps/g/gf;

    invoke-direct {v0}, Lcom/google/maps/g/gf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/google/maps/g/ge;->q:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ge;
    .locals 1

    .prologue
    .line 148
    sparse-switch p0, :sswitch_data_0

    .line 165
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 149
    :sswitch_0
    sget-object v0, Lcom/google/maps/g/ge;->a:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 150
    :sswitch_1
    sget-object v0, Lcom/google/maps/g/ge;->b:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 151
    :sswitch_2
    sget-object v0, Lcom/google/maps/g/ge;->c:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 152
    :sswitch_3
    sget-object v0, Lcom/google/maps/g/ge;->d:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 153
    :sswitch_4
    sget-object v0, Lcom/google/maps/g/ge;->e:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 154
    :sswitch_5
    sget-object v0, Lcom/google/maps/g/ge;->f:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 155
    :sswitch_6
    sget-object v0, Lcom/google/maps/g/ge;->g:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 156
    :sswitch_7
    sget-object v0, Lcom/google/maps/g/ge;->h:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 157
    :sswitch_8
    sget-object v0, Lcom/google/maps/g/ge;->i:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 158
    :sswitch_9
    sget-object v0, Lcom/google/maps/g/ge;->j:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 159
    :sswitch_a
    sget-object v0, Lcom/google/maps/g/ge;->k:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 160
    :sswitch_b
    sget-object v0, Lcom/google/maps/g/ge;->l:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 161
    :sswitch_c
    sget-object v0, Lcom/google/maps/g/ge;->m:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 162
    :sswitch_d
    sget-object v0, Lcom/google/maps/g/ge;->n:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 163
    :sswitch_e
    sget-object v0, Lcom/google/maps/g/ge;->o:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 164
    :sswitch_f
    sget-object v0, Lcom/google/maps/g/ge;->p:Lcom/google/maps/g/ge;

    goto :goto_0

    .line 148
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x4a -> :sswitch_2
        0x4e -> :sswitch_3
        0x51 -> :sswitch_5
        0x59 -> :sswitch_4
        0x5a -> :sswitch_6
        0x5d -> :sswitch_7
        0x5e -> :sswitch_8
        0x60 -> :sswitch_9
        0x62 -> :sswitch_a
        0x65 -> :sswitch_b
        0x67 -> :sswitch_c
        0x68 -> :sswitch_d
        0x6b -> :sswitch_e
        0x6c -> :sswitch_f
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ge;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/maps/g/ge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ge;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ge;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/maps/g/ge;->r:[Lcom/google/maps/g/ge;

    invoke-virtual {v0}, [Lcom/google/maps/g/ge;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ge;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/maps/g/ge;->q:I

    return v0
.end method
