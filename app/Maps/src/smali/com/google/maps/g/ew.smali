.class public final Lcom/google/maps/g/ew;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ex;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/eu;",
        "Lcom/google/maps/g/ew;",
        ">;",
        "Lcom/google/maps/g/ex;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/aq;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/aq;

.field private f:Ljava/lang/Object;

.field private g:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/google/maps/g/eu;->h:Lcom/google/maps/g/eu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 590
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ew;->b:Ljava/lang/Object;

    .line 666
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    .line 759
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ew;->d:Ljava/lang/Object;

    .line 835
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    .line 928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ew;->f:Ljava/lang/Object;

    .line 484
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/eu;)Lcom/google/maps/g/ew;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 541
    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 581
    :goto_0
    return-object p0

    .line 542
    :cond_0
    iget v2, p1, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 543
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 544
    iget-object v2, p1, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ew;->b:Ljava/lang/Object;

    .line 547
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 548
    iget-object v2, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 549
    iget-object v2, p1, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    .line 550
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 557
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 558
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 559
    iget-object v2, p1, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ew;->d:Ljava/lang/Object;

    .line 562
    :cond_3
    iget-object v2, p1, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 563
    iget-object v2, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 564
    iget-object v2, p1, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    .line 565
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 572
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 573
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 574
    iget-object v2, p1, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ew;->f:Ljava/lang/Object;

    .line 577
    :cond_5
    iget v2, p1, Lcom/google/maps/g/eu;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_e

    :goto_6
    if-eqz v0, :cond_6

    .line 578
    iget-boolean v0, p1, Lcom/google/maps/g/eu;->g:Z

    iget v1, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/maps/g/ew;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/ew;->g:Z

    .line 580
    :cond_6
    iget-object v0, p1, Lcom/google/maps/g/eu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 542
    goto/16 :goto_1

    .line 552
    :cond_8
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_9

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 553
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_a
    move v2, v1

    .line 557
    goto :goto_3

    .line 567
    :cond_b
    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_c

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/maps/g/ew;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ew;->a:I

    .line 568
    :cond_c
    iget-object v2, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_d
    move v2, v1

    .line 572
    goto :goto_5

    :cond_e
    move v0, v1

    .line 577
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 475
    new-instance v2, Lcom/google/maps/g/eu;

    invoke-direct {v2, p0}, Lcom/google/maps/g/eu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ew;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/ew;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/ew;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/ew;->c:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/ew;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    iget v1, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/maps/g/ew;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/g/ew;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/ew;->e:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/ew;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-boolean v1, p0, Lcom/google/maps/g/ew;->g:Z

    iput-boolean v1, v2, Lcom/google/maps/g/eu;->g:Z

    iput v0, v2, Lcom/google/maps/g/eu;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 475
    check-cast p1, Lcom/google/maps/g/eu;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ew;->a(Lcom/google/maps/g/eu;)Lcom/google/maps/g/ew;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 585
    const/4 v0, 0x1

    return v0
.end method
