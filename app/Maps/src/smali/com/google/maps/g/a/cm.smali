.class public final Lcom/google/maps/g/a/cm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/cm;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/a/cm;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/maps/g/a/cn;

    invoke-direct {v0}, Lcom/google/maps/g/a/cn;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/cm;->PARSER:Lcom/google/n/ax;

    .line 256
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/cm;->g:Lcom/google/n/aw;

    .line 457
    new-instance v0, Lcom/google/maps/g/a/cm;

    invoke-direct {v0}, Lcom/google/maps/g/a/cm;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/cm;->d:Lcom/google/maps/g/a/cm;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 213
    iput-byte v0, p0, Lcom/google/maps/g/a/cm;->e:B

    .line 235
    iput v0, p0, Lcom/google/maps/g/a/cm;->f:I

    .line 18
    iput v1, p0, Lcom/google/maps/g/a/cm;->b:I

    .line 19
    iput-boolean v1, p0, Lcom/google/maps/g/a/cm;->c:Z

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/maps/g/a/cm;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 32
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 34
    sparse-switch v0, :sswitch_data_0

    .line 39
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 47
    invoke-static {v0}, Lcom/google/maps/g/a/cp;->a(I)Lcom/google/maps/g/a/cp;

    move-result-object v5

    .line 48
    if-nez v5, :cond_1

    .line 49
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/cm;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/maps/g/a/cm;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/a/cm;->a:I

    .line 52
    iput v0, p0, Lcom/google/maps/g/a/cm;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/cm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/cm;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/cm;->c:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 69
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/cm;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 213
    iput-byte v0, p0, Lcom/google/maps/g/a/cm;->e:B

    .line 235
    iput v0, p0, Lcom/google/maps/g/a/cm;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/cm;
    .locals 1

    .prologue
    .line 460
    sget-object v0, Lcom/google/maps/g/a/cm;->d:Lcom/google/maps/g/a/cm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/co;
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lcom/google/maps/g/a/co;

    invoke-direct {v0}, Lcom/google/maps/g/a/co;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/cm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/g/a/cm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/google/maps/g/a/cm;->c()I

    .line 226
    iget v2, p0, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 227
    iget v2, p0, Lcom/google/maps/g/a/cm;->b:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 229
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 230
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/cm;->c:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/cm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 233
    return-void

    .line 227
    :cond_2
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 230
    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 215
    iget-byte v1, p0, Lcom/google/maps/g/a/cm;->e:B

    .line 216
    if-ne v1, v0, :cond_0

    .line 220
    :goto_0
    return v0

    .line 217
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 219
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/cm;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 237
    iget v0, p0, Lcom/google/maps/g/a/cm;->f:I

    .line 238
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 251
    :goto_0
    return v0

    .line 241
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 242
    iget v0, p0, Lcom/google/maps/g/a/cm;->b:I

    .line 243
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 245
    :goto_2
    iget v2, p0, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_1

    .line 246
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/cm;->c:Z

    .line 247
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/a/cm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    iput v0, p0, Lcom/google/maps/g/a/cm;->f:I

    goto :goto_0

    .line 243
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/cm;->newBuilder()Lcom/google/maps/g/a/co;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/co;->a(Lcom/google/maps/g/a/cm;)Lcom/google/maps/g/a/co;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/cm;->newBuilder()Lcom/google/maps/g/a/co;

    move-result-object v0

    return-object v0
.end method
