.class public final enum Lcom/google/maps/g/gb;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/gb;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/gb;

.field public static final enum b:Lcom/google/maps/g/gb;

.field private static final synthetic d:[Lcom/google/maps/g/gb;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 146
    new-instance v0, Lcom/google/maps/g/gb;

    const-string v1, "ARRIVAL_AIRPORT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    .line 150
    new-instance v0, Lcom/google/maps/g/gb;

    const-string v1, "DEPARTURE_AIRPORT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/gb;->b:Lcom/google/maps/g/gb;

    .line 141
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/maps/g/gb;

    sget-object v1, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/gb;->b:Lcom/google/maps/g/gb;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/gb;->d:[Lcom/google/maps/g/gb;

    .line 180
    new-instance v0, Lcom/google/maps/g/gc;

    invoke-direct {v0}, Lcom/google/maps/g/gc;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 190
    iput p3, p0, Lcom/google/maps/g/gb;->c:I

    .line 191
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/gb;
    .locals 1

    .prologue
    .line 168
    packed-switch p0, :pswitch_data_0

    .line 171
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 169
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    goto :goto_0

    .line 170
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/gb;->b:Lcom/google/maps/g/gb;

    goto :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/gb;
    .locals 1

    .prologue
    .line 141
    const-class v0, Lcom/google/maps/g/gb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gb;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/gb;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/google/maps/g/gb;->d:[Lcom/google/maps/g/gb;

    invoke-virtual {v0}, [Lcom/google/maps/g/gb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/gb;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/google/maps/g/gb;->c:I

    return v0
.end method
