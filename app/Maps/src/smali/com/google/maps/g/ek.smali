.class public final Lcom/google/maps/g/ek;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ep;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ei;",
        "Lcom/google/maps/g/ek;",
        ">;",
        "Lcom/google/maps/g/ep;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 663
    sget-object v0, Lcom/google/maps/g/ei;->j:Lcom/google/maps/g/ei;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 776
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ek;->b:Ljava/lang/Object;

    .line 852
    iput v1, p0, Lcom/google/maps/g/ek;->f:I

    .line 888
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ek;->c:Ljava/lang/Object;

    .line 964
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ek;->d:Lcom/google/n/ao;

    .line 1023
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ek;->e:Lcom/google/n/ao;

    .line 1082
    iput v1, p0, Lcom/google/maps/g/ek;->g:I

    .line 664
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ei;)Lcom/google/maps/g/ek;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 735
    invoke-static {}, Lcom/google/maps/g/ei;->d()Lcom/google/maps/g/ei;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 767
    :goto_0
    return-object p0

    .line 736
    :cond_0
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 737
    iget v2, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ek;->a:I

    .line 738
    iget-object v2, p1, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ek;->b:Ljava/lang/Object;

    .line 741
    :cond_1
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 742
    iget v2, p1, Lcom/google/maps/g/ei;->c:I

    invoke-static {v2}, Lcom/google/maps/g/en;->a(I)Lcom/google/maps/g/en;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/en;->a:Lcom/google/maps/g/en;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 736
    goto :goto_1

    :cond_4
    move v2, v1

    .line 741
    goto :goto_2

    .line 742
    :cond_5
    iget v3, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/ek;->a:I

    iget v2, v2, Lcom/google/maps/g/en;->e:I

    iput v2, p0, Lcom/google/maps/g/ek;->f:I

    .line 744
    :cond_6
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 745
    iget v2, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ek;->a:I

    .line 746
    iget-object v2, p1, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ek;->c:Ljava/lang/Object;

    .line 749
    :cond_7
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 750
    iget-object v2, p0, Lcom/google/maps/g/ek;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 751
    iget v2, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ek;->a:I

    .line 753
    :cond_8
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_9

    .line 754
    iget-object v2, p0, Lcom/google/maps/g/ek;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 755
    iget v2, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/ek;->a:I

    .line 757
    :cond_9
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 758
    iget v2, p1, Lcom/google/maps/g/ei;->g:I

    invoke-static {v2}, Lcom/google/maps/g/el;->a(I)Lcom/google/maps/g/el;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, Lcom/google/maps/g/el;->a:Lcom/google/maps/g/el;

    :cond_a
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v2, v1

    .line 744
    goto :goto_3

    :cond_c
    move v2, v1

    .line 749
    goto :goto_4

    :cond_d
    move v2, v1

    .line 753
    goto :goto_5

    :cond_e
    move v2, v1

    .line 757
    goto :goto_6

    .line 758
    :cond_f
    iget v3, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/ek;->a:I

    iget v2, v2, Lcom/google/maps/g/el;->i:I

    iput v2, p0, Lcom/google/maps/g/ek;->g:I

    .line 760
    :cond_10
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_11

    .line 761
    iget-boolean v2, p1, Lcom/google/maps/g/ei;->h:Z

    iget v3, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/ek;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/ek;->h:Z

    .line 763
    :cond_11
    iget v2, p1, Lcom/google/maps/g/ei;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    :goto_8
    if-eqz v0, :cond_12

    .line 764
    iget-boolean v0, p1, Lcom/google/maps/g/ei;->i:Z

    iget v1, p0, Lcom/google/maps/g/ek;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/ek;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/ek;->i:Z

    .line 766
    :cond_12
    iget-object v0, p1, Lcom/google/maps/g/ei;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 760
    goto :goto_7

    :cond_14
    move v0, v1

    .line 763
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/google/maps/g/ek;->c()Lcom/google/maps/g/ei;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 655
    check-cast p1, Lcom/google/maps/g/ei;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ek;->a(Lcom/google/maps/g/ei;)Lcom/google/maps/g/ek;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/ei;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 691
    new-instance v2, Lcom/google/maps/g/ei;

    invoke-direct {v2, p0}, Lcom/google/maps/g/ei;-><init>(Lcom/google/n/v;)V

    .line 692
    iget v3, p0, Lcom/google/maps/g/ek;->a:I

    .line 694
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 697
    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/ek;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ei;->b:Ljava/lang/Object;

    .line 698
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 699
    or-int/lit8 v0, v0, 0x2

    .line 701
    :cond_0
    iget v4, p0, Lcom/google/maps/g/ek;->f:I

    iput v4, v2, Lcom/google/maps/g/ei;->c:I

    .line 702
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 703
    or-int/lit8 v0, v0, 0x4

    .line 705
    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/ek;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/ei;->d:Ljava/lang/Object;

    .line 706
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 707
    or-int/lit8 v0, v0, 0x8

    .line 709
    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/ei;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ek;->d:Lcom/google/n/ao;

    .line 710
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ek;->d:Lcom/google/n/ao;

    .line 711
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 709
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 712
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 713
    or-int/lit8 v0, v0, 0x10

    .line 715
    :cond_3
    iget-object v4, v2, Lcom/google/maps/g/ei;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ek;->e:Lcom/google/n/ao;

    .line 716
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ek;->e:Lcom/google/n/ao;

    .line 717
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 715
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 718
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 719
    or-int/lit8 v0, v0, 0x20

    .line 721
    :cond_4
    iget v1, p0, Lcom/google/maps/g/ek;->g:I

    iput v1, v2, Lcom/google/maps/g/ei;->g:I

    .line 722
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 723
    or-int/lit8 v0, v0, 0x40

    .line 725
    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/ek;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/ei;->h:Z

    .line 726
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 727
    or-int/lit16 v0, v0, 0x80

    .line 729
    :cond_6
    iget-boolean v1, p0, Lcom/google/maps/g/ek;->i:Z

    iput-boolean v1, v2, Lcom/google/maps/g/ei;->i:Z

    .line 730
    iput v0, v2, Lcom/google/maps/g/ei;->a:I

    .line 731
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method
