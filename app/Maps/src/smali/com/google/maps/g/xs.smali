.class public final Lcom/google/maps/g/xs;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/xv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/xs;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/xs;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/google/maps/g/xt;

    invoke-direct {v0}, Lcom/google/maps/g/xt;-><init>()V

    sput-object v0, Lcom/google/maps/g/xs;->PARSER:Lcom/google/n/ax;

    .line 555
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/xs;->m:Lcom/google/n/aw;

    .line 1433
    new-instance v0, Lcom/google/maps/g/xs;

    invoke-direct {v0}, Lcom/google/maps/g/xs;-><init>()V

    sput-object v0, Lcom/google/maps/g/xs;->j:Lcom/google/maps/g/xs;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 470
    iput-byte v0, p0, Lcom/google/maps/g/xs;->k:B

    .line 510
    iput v0, p0, Lcom/google/maps/g/xs;->l:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/16 v7, 0x8

    .line 32
    invoke-direct {p0}, Lcom/google/maps/g/xs;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 40
    sparse-switch v4, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 53
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 54
    iput-object v4, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 112
    iget-object v1, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 114
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/xs;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 59
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 60
    iput-object v4, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 108
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 109
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 65
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 66
    iput-object v4, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    goto :goto_0

    .line 70
    :sswitch_4
    and-int/lit8 v4, v1, 0x8

    if-eq v4, v7, :cond_2

    .line 71
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 73
    or-int/lit8 v1, v1, 0x8

    .line 75
    :cond_2
    iget-object v4, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 75
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 81
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 82
    iput-object v4, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 86
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 87
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 88
    iput-object v4, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 92
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 93
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 94
    iput-object v4, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 98
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 99
    iget v5, p0, Lcom/google/maps/g/xs;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/g/xs;->a:I

    .line 100
    iput-object v4, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 111
    :cond_3
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_4

    .line 112
    iget-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 114
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->au:Lcom/google/n/bn;

    .line 115
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 470
    iput-byte v0, p0, Lcom/google/maps/g/xs;->k:B

    .line 510
    iput v0, p0, Lcom/google/maps/g/xs;->l:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;
    .locals 1

    .prologue
    .line 620
    invoke-static {}, Lcom/google/maps/g/xs;->newBuilder()Lcom/google/maps/g/xu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/xu;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/xs;
    .locals 1

    .prologue
    .line 1436
    sget-object v0, Lcom/google/maps/g/xs;->j:Lcom/google/maps/g/xs;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/xu;
    .locals 1

    .prologue
    .line 617
    new-instance v0, Lcom/google/maps/g/xu;

    invoke-direct {v0}, Lcom/google/maps/g/xu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/xs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcom/google/maps/g/xs;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 482
    invoke-virtual {p0}, Lcom/google/maps/g/xs;->c()I

    .line 483
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 486
    :cond_0
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 487
    iget-object v0, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 489
    :cond_1
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 490
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 492
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 493
    iget-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 484
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 487
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 490
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 495
    :cond_6
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_7

    .line 496
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 498
    :cond_7
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 499
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 501
    :cond_8
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 502
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 504
    :cond_9
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 505
    iget-object v0, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    :goto_7
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 507
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/xs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 508
    return-void

    .line 496
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 499
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 502
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 505
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_7
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 472
    iget-byte v1, p0, Lcom/google/maps/g/xs;->k:B

    .line 473
    if-ne v1, v0, :cond_0

    .line 477
    :goto_0
    return v0

    .line 474
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 476
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/xs;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 512
    iget v0, p0, Lcom/google/maps/g/xs;->l:I

    .line 513
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 550
    :goto_0
    return v0

    .line 516
    :cond_0
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 518
    iget-object v0, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 520
    :goto_2
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 524
    :cond_1
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 525
    const/4 v3, 0x3

    .line 526
    iget-object v0, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_2
    move v3, v1

    move v1, v2

    .line 528
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 529
    iget-object v0, p0, Lcom/google/maps/g/xs;->e:Ljava/util/List;

    .line 530
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 528
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 518
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 522
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 526
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 532
    :cond_6
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_7

    .line 533
    const/4 v1, 0x5

    .line 534
    iget-object v0, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 536
    :cond_7
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 537
    const/4 v1, 0x6

    .line 538
    iget-object v0, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 540
    :cond_8
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 541
    const/4 v1, 0x7

    .line 542
    iget-object v0, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->h:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 544
    :cond_9
    iget v0, p0, Lcom/google/maps/g/xs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 546
    iget-object v0, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/xs;->i:Ljava/lang/Object;

    :goto_9
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 548
    :cond_a
    iget-object v0, p0, Lcom/google/maps/g/xs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 549
    iput v0, p0, Lcom/google/maps/g/xs;->l:I

    goto/16 :goto_0

    .line 534
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 538
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 542
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 546
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_f
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/xs;->newBuilder()Lcom/google/maps/g/xu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/xu;->a(Lcom/google/maps/g/xs;)Lcom/google/maps/g/xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/xs;->newBuilder()Lcom/google/maps/g/xu;

    move-result-object v0

    return-object v0
.end method
