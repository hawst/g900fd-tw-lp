.class public final Lcom/google/maps/g/ey;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/fb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ey;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/maps/g/ey;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field public d:Ljava/lang/Object;

.field e:Z

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/maps/g/ez;

    invoke-direct {v0}, Lcom/google/maps/g/ez;-><init>()V

    sput-object v0, Lcom/google/maps/g/ey;->PARSER:Lcom/google/n/ax;

    .line 355
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ey;->l:Lcom/google/n/aw;

    .line 969
    new-instance v0, Lcom/google/maps/g/ey;

    invoke-direct {v0}, Lcom/google/maps/g/ey;-><init>()V

    sput-object v0, Lcom/google/maps/g/ey;->i:Lcom/google/maps/g/ey;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    .line 277
    iput-byte v3, p0, Lcom/google/maps/g/ey;->j:B

    .line 314
    iput v3, p0, Lcom/google/maps/g/ey;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/g/ey;->e:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/maps/g/ey;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 37
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 39
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 46
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 52
    iget v5, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/ey;->a:I

    .line 53
    iput-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ey;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/ey;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/ey;->e:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    .line 92
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 93
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 58
    goto :goto_1

    .line 62
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 63
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/ey;->a:I

    goto :goto_0

    .line 67
    :sswitch_4
    iget-object v0, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 68
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/ey;->a:I

    goto :goto_0

    .line 72
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget v5, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/g/ey;->a:I

    .line 74
    iput-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 78
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ey;->a:I

    goto/16 :goto_0

    .line 83
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 84
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ey;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 95
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ey;->au:Lcom/google/n/bn;

    .line 96
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x30 -> :sswitch_2
        0x3a -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
        0x52 -> :sswitch_6
        0x5a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    .line 277
    iput-byte v1, p0, Lcom/google/maps/g/ey;->j:B

    .line 314
    iput v1, p0, Lcom/google/maps/g/ey;->k:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/maps/g/ey;
    .locals 1

    .prologue
    .line 972
    sget-object v0, Lcom/google/maps/g/ey;->i:Lcom/google/maps/g/ey;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/fa;
    .locals 1

    .prologue
    .line 417
    new-instance v0, Lcom/google/maps/g/fa;

    invoke-direct {v0}, Lcom/google/maps/g/fa;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/google/maps/g/ey;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 289
    invoke-virtual {p0}, Lcom/google/maps/g/ey;->c()I

    .line 290
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_0

    .line 291
    iget-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 293
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_1

    .line 294
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/ey;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_8

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 296
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_2

    .line 297
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 299
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_3

    .line 300
    iget-object v0, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 302
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 303
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 305
    :cond_4
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_5

    .line 306
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 308
    :cond_5
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    .line 309
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 311
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/ey;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 312
    return-void

    .line 291
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 294
    goto/16 :goto_1

    .line 303
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 279
    iget-byte v1, p0, Lcom/google/maps/g/ey;->j:B

    .line 280
    if-ne v1, v0, :cond_0

    .line 284
    :goto_0
    return v0

    .line 281
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 283
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/ey;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 316
    iget v0, p0, Lcom/google/maps/g/ey;->k:I

    .line 317
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 350
    :goto_0
    return v0

    .line 320
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_9

    .line 322
    iget-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ey;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 324
    :goto_2
    iget v2, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_1

    .line 325
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/maps/g/ey;->e:Z

    .line 326
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 328
    :cond_1
    iget v2, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 329
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/maps/g/ey;->g:Lcom/google/n/ao;

    .line 330
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 332
    :cond_2
    iget v2, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_8

    .line 333
    iget-object v2, p0, Lcom/google/maps/g/ey;->h:Lcom/google/n/ao;

    .line 334
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 336
    :goto_3
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 337
    const/16 v3, 0x9

    .line 338
    iget-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 340
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    .line 341
    const/16 v0, 0xa

    iget-object v3, p0, Lcom/google/maps/g/ey;->c:Lcom/google/n/ao;

    .line 342
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 344
    :cond_4
    iget v0, p0, Lcom/google/maps/g/ey;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    .line 345
    const/16 v0, 0xb

    iget-object v3, p0, Lcom/google/maps/g/ey;->b:Lcom/google/n/ao;

    .line 346
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 348
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/ey;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 349
    iput v0, p0, Lcom/google/maps/g/ey;->k:I

    goto/16 :goto_0

    .line 322
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 338
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_8
    move v2, v0

    goto :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    .line 216
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 217
    check-cast v0, Ljava/lang/String;

    .line 225
    :goto_0
    return-object v0

    .line 219
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 221
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    iput-object v1, p0, Lcom/google/maps/g/ey;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 225
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ey;->newBuilder()Lcom/google/maps/g/fa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/fa;->a(Lcom/google/maps/g/ey;)Lcom/google/maps/g/fa;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/ey;->newBuilder()Lcom/google/maps/g/fa;

    move-result-object v0

    return-object v0
.end method
