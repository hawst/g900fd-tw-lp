.class public final enum Lcom/google/maps/g/a/fz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/fz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/fz;

.field public static final enum b:Lcom/google/maps/g/a/fz;

.field public static final enum c:Lcom/google/maps/g/a/fz;

.field public static final enum d:Lcom/google/maps/g/a/fz;

.field private static final synthetic f:[Lcom/google/maps/g/a/fz;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 113
    new-instance v0, Lcom/google/maps/g/a/fz;

    const-string v1, "DELAY_NODATA"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/fz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    .line 117
    new-instance v0, Lcom/google/maps/g/a/fz;

    const-string v1, "DELAY_HEAVY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/fz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fz;->b:Lcom/google/maps/g/a/fz;

    .line 121
    new-instance v0, Lcom/google/maps/g/a/fz;

    const-string v1, "DELAY_MEDIUM"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/fz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fz;->c:Lcom/google/maps/g/a/fz;

    .line 125
    new-instance v0, Lcom/google/maps/g/a/fz;

    const-string v1, "DELAY_LIGHT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/fz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fz;->d:Lcom/google/maps/g/a/fz;

    .line 108
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/a/fz;

    sget-object v1, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/fz;->b:Lcom/google/maps/g/a/fz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/fz;->c:Lcom/google/maps/g/a/fz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/fz;->d:Lcom/google/maps/g/a/fz;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/a/fz;->f:[Lcom/google/maps/g/a/fz;

    .line 165
    new-instance v0, Lcom/google/maps/g/a/ga;

    invoke-direct {v0}, Lcom/google/maps/g/a/ga;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 175
    iput p3, p0, Lcom/google/maps/g/a/fz;->e:I

    .line 176
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/fz;
    .locals 1

    .prologue
    .line 151
    packed-switch p0, :pswitch_data_0

    .line 156
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    goto :goto_0

    .line 153
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/fz;->b:Lcom/google/maps/g/a/fz;

    goto :goto_0

    .line 154
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/fz;->c:Lcom/google/maps/g/a/fz;

    goto :goto_0

    .line 155
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/fz;->d:Lcom/google/maps/g/a/fz;

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/fz;
    .locals 1

    .prologue
    .line 108
    const-class v0, Lcom/google/maps/g/a/fz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fz;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/fz;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/maps/g/a/fz;->f:[Lcom/google/maps/g/a/fz;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/fz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/fz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/maps/g/a/fz;->e:I

    return v0
.end method
