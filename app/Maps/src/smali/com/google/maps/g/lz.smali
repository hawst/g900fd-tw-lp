.class public final Lcom/google/maps/g/lz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/me;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lz;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/lz;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/maps/g/a;

.field d:Lcom/google/maps/g/ea;

.field e:Lcom/google/maps/g/ck;

.field f:Ljava/lang/Object;

.field g:Lcom/google/maps/g/kq;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:Lcom/google/maps/g/gy;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 435
    new-instance v0, Lcom/google/maps/g/ma;

    invoke-direct {v0}, Lcom/google/maps/g/ma;-><init>()V

    sput-object v0, Lcom/google/maps/g/lz;->PARSER:Lcom/google/n/ax;

    .line 820
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/lz;->n:Lcom/google/n/aw;

    .line 1618
    new-instance v0, Lcom/google/maps/g/lz;

    invoke-direct {v0}, Lcom/google/maps/g/lz;-><init>()V

    sput-object v0, Lcom/google/maps/g/lz;->k:Lcom/google/maps/g/lz;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 296
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 710
    iput-byte v0, p0, Lcom/google/maps/g/lz;->l:B

    .line 771
    iput v0, p0, Lcom/google/maps/g/lz;->m:I

    .line 297
    iput v0, p0, Lcom/google/maps/g/lz;->b:I

    .line 298
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    .line 299
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/lz;->i:I

    .line 301
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    const/16 v7, 0x40

    const/4 v3, 0x0

    .line 307
    invoke-direct {p0}, Lcom/google/maps/g/lz;-><init>()V

    .line 310
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 313
    :cond_0
    :goto_0
    if-nez v4, :cond_9

    .line 314
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 315
    sparse-switch v0, :sswitch_data_0

    .line 320
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 322
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 318
    goto :goto_0

    .line 328
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_f

    .line 329
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    invoke-static {}, Lcom/google/maps/g/a;->newBuilder()Lcom/google/maps/g/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    move-result-object v0

    move-object v2, v0

    .line 331
    :goto_1
    sget-object v0, Lcom/google/maps/g/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a;

    iput-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    .line 332
    if-eqz v2, :cond_1

    .line 333
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/c;->a(Lcom/google/maps/g/a;)Lcom/google/maps/g/c;

    .line 334
    invoke-virtual {v2}, Lcom/google/maps/g/c;->c()Lcom/google/maps/g/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    .line 336
    :cond_1
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/lz;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v0

    .line 424
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 429
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x40

    if-ne v1, v7, :cond_2

    .line 430
    iget-object v1, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    .line 432
    :cond_2
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/lz;->au:Lcom/google/n/bn;

    throw v0

    .line 341
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_e

    .line 342
    iget-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    invoke-static {}, Lcom/google/maps/g/ea;->newBuilder()Lcom/google/maps/g/ec;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v0

    move-object v2, v0

    .line 344
    :goto_2
    sget-object v0, Lcom/google/maps/g/ea;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ea;

    iput-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    .line 345
    if-eqz v2, :cond_3

    .line 346
    iget-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    .line 347
    invoke-virtual {v2}, Lcom/google/maps/g/ec;->c()Lcom/google/maps/g/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    .line 349
    :cond_3
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/lz;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 425
    :catch_1
    move-exception v0

    .line 426
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 427
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 353
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 354
    iget v2, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/lz;->a:I

    .line 355
    iput-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 360
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_d

    .line 361
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    invoke-static {}, Lcom/google/maps/g/kq;->newBuilder()Lcom/google/maps/g/ks;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v0

    move-object v2, v0

    .line 363
    :goto_3
    sget-object v0, Lcom/google/maps/g/kq;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/kq;

    iput-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    .line 364
    if-eqz v2, :cond_4

    .line 365
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    .line 366
    invoke-virtual {v2}, Lcom/google/maps/g/ks;->c()Lcom/google/maps/g/kq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    .line 368
    :cond_4
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/lz;->a:I

    goto/16 :goto_0

    .line 372
    :sswitch_5
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v7, :cond_5

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    .line 374
    or-int/lit8 v1, v1, 0x40

    .line 376
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 380
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 381
    invoke-static {v0}, Lcom/google/maps/g/mc;->a(I)Lcom/google/maps/g/mc;

    move-result-object v2

    .line 382
    if-nez v2, :cond_6

    .line 383
    const/4 v2, 0x6

    invoke-virtual {v6, v2, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 385
    :cond_6
    iget v2, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/lz;->a:I

    .line 386
    iput v0, p0, Lcom/google/maps/g/lz;->i:I

    goto/16 :goto_0

    .line 392
    :sswitch_7
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_c

    .line 393
    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v0

    move-object v2, v0

    .line 395
    :goto_4
    sget-object v0, Lcom/google/maps/g/gy;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iput-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    .line 396
    if-eqz v2, :cond_7

    .line 397
    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    .line 398
    invoke-virtual {v2}, Lcom/google/maps/g/ha;->c()Lcom/google/maps/g/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    .line 400
    :cond_7
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/lz;->a:I

    goto/16 :goto_0

    .line 404
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/lz;->a:I

    .line 405
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/lz;->b:I

    goto/16 :goto_0

    .line 410
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_b

    .line 411
    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    invoke-static {}, Lcom/google/maps/g/ck;->newBuilder()Lcom/google/maps/g/cm;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/cm;->a(Lcom/google/maps/g/ck;)Lcom/google/maps/g/cm;

    move-result-object v0

    move-object v2, v0

    .line 413
    :goto_5
    sget-object v0, Lcom/google/maps/g/ck;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ck;

    iput-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    .line 414
    if-eqz v2, :cond_8

    .line 415
    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/cm;->a(Lcom/google/maps/g/ck;)Lcom/google/maps/g/cm;

    .line 416
    invoke-virtual {v2}, Lcom/google/maps/g/cm;->c()Lcom/google/maps/g/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    .line 418
    :cond_8
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/lz;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 429
    :cond_9
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v7, :cond_a

    .line 430
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    .line 432
    :cond_a
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->au:Lcom/google/n/bn;

    .line 433
    return-void

    :cond_b
    move-object v2, v3

    goto :goto_5

    :cond_c
    move-object v2, v3

    goto :goto_4

    :cond_d
    move-object v2, v3

    goto/16 :goto_3

    :cond_e
    move-object v2, v3

    goto/16 :goto_2

    :cond_f
    move-object v2, v3

    goto/16 :goto_1

    .line 315
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 294
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 710
    iput-byte v0, p0, Lcom/google/maps/g/lz;->l:B

    .line 771
    iput v0, p0, Lcom/google/maps/g/lz;->m:I

    .line 295
    return-void
.end method

.method public static d()Lcom/google/maps/g/lz;
    .locals 1

    .prologue
    .line 1621
    sget-object v0, Lcom/google/maps/g/lz;->k:Lcom/google/maps/g/lz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/mb;
    .locals 1

    .prologue
    .line 882
    new-instance v0, Lcom/google/maps/g/mb;

    invoke-direct {v0}, Lcom/google/maps/g/mb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/lz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    sget-object v0, Lcom/google/maps/g/lz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 740
    invoke-virtual {p0}, Lcom/google/maps/g/lz;->c()I

    .line 741
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_0

    .line 742
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v0

    :goto_0
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 744
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 745
    iget-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v0

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 747
    :cond_1
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 748
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 750
    :cond_2
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 751
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v0

    :goto_3
    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    :cond_3
    move v1, v2

    .line 753
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 754
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 753
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 742
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    goto/16 :goto_0

    .line 745
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    goto/16 :goto_1

    .line 748
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 751
    :cond_7
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    goto :goto_3

    .line 756
    :cond_8
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 757
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/g/lz;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 759
    :cond_9
    :goto_5
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 760
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 762
    :cond_a
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_b

    .line 763
    iget v0, p0, Lcom/google/maps/g/lz;->b:I

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_f

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 765
    :cond_b
    :goto_7
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_c

    .line 766
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/g/ck;->d()Lcom/google/maps/g/ck;

    move-result-object v0

    :goto_8
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 768
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/lz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 769
    return-void

    .line 757
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 760
    :cond_e
    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    goto :goto_6

    .line 763
    :cond_f
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 766
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 712
    iget-byte v0, p0, Lcom/google/maps/g/lz;->l:B

    .line 713
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 735
    :cond_0
    :goto_0
    return v2

    .line 714
    :cond_1
    if-eqz v0, :cond_0

    .line 716
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 717
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 718
    iput-byte v2, p0, Lcom/google/maps/g/lz;->l:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 716
    goto :goto_1

    .line 717
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    goto :goto_2

    .line 722
    :cond_4
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 723
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/maps/g/kq;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 724
    iput-byte v2, p0, Lcom/google/maps/g/lz;->l:B

    goto :goto_0

    :cond_5
    move v0, v2

    .line 722
    goto :goto_3

    .line 723
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    goto :goto_4

    :cond_7
    move v1, v2

    .line 728
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 729
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a;

    invoke-virtual {v0}, Lcom/google/maps/g/a;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 730
    iput-byte v2, p0, Lcom/google/maps/g/lz;->l:B

    goto :goto_0

    .line 728
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 734
    :cond_9
    iput-byte v3, p0, Lcom/google/maps/g/lz;->l:B

    move v2, v3

    .line 735
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 773
    iget v0, p0, Lcom/google/maps/g/lz;->m:I

    .line 774
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 815
    :goto_0
    return v0

    .line 777
    :cond_0
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_11

    .line 779
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/a;->d()Lcom/google/maps/g/a;

    move-result-object v0

    :goto_1
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 781
    :goto_2
    iget v2, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_10

    .line 783
    iget-object v2, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 785
    :goto_4
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 786
    const/4 v3, 0x3

    .line 787
    iget-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/lz;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 789
    :cond_1
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    .line 791
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v0

    :goto_6
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_2
    move v3, v2

    move v2, v1

    .line 793
    :goto_7
    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 794
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/maps/g/lz;->h:Ljava/util/List;

    .line 795
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 793
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 779
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/lz;->c:Lcom/google/maps/g/a;

    goto/16 :goto_1

    .line 783
    :cond_4
    iget-object v2, p0, Lcom/google/maps/g/lz;->d:Lcom/google/maps/g/ea;

    goto/16 :goto_3

    .line 787
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 791
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/lz;->g:Lcom/google/maps/g/kq;

    goto :goto_6

    .line 797
    :cond_7
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 798
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/maps/g/lz;->i:I

    .line 799
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_c

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 801
    :cond_8
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 802
    const/4 v2, 0x7

    .line 803
    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_9
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 805
    :cond_9
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_a

    .line 806
    iget v0, p0, Lcom/google/maps/g/lz;->b:I

    .line 807
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 809
    :cond_a
    iget v0, p0, Lcom/google/maps/g/lz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_b

    .line 810
    const/16 v2, 0x9

    .line 811
    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/maps/g/ck;->d()Lcom/google/maps/g/ck;

    move-result-object v0

    :goto_b
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 813
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/lz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 814
    iput v0, p0, Lcom/google/maps/g/lz;->m:I

    goto/16 :goto_0

    .line 799
    :cond_c
    const/16 v0, 0xa

    goto :goto_8

    .line 803
    :cond_d
    iget-object v0, p0, Lcom/google/maps/g/lz;->j:Lcom/google/maps/g/gy;

    goto :goto_9

    .line 807
    :cond_e
    const/16 v0, 0xa

    goto :goto_a

    .line 811
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/lz;->e:Lcom/google/maps/g/ck;

    goto :goto_b

    :cond_10
    move v2, v0

    goto/16 :goto_4

    :cond_11
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Lcom/google/maps/g/lz;->newBuilder()Lcom/google/maps/g/mb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/mb;->a(Lcom/google/maps/g/lz;)Lcom/google/maps/g/mb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Lcom/google/maps/g/lz;->newBuilder()Lcom/google/maps/g/mb;

    move-result-object v0

    return-object v0
.end method
