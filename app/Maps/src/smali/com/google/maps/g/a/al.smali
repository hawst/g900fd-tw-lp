.class public final enum Lcom/google/maps/g/a/al;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/al;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/al;

.field public static final enum b:Lcom/google/maps/g/a/al;

.field public static final enum c:Lcom/google/maps/g/a/al;

.field public static final enum d:Lcom/google/maps/g/a/al;

.field private static final synthetic f:[Lcom/google/maps/g/a/al;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    new-instance v0, Lcom/google/maps/g/a/al;

    const-string v1, "KILOMETERS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    .line 106
    new-instance v0, Lcom/google/maps/g/a/al;

    const-string v1, "MILES"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    .line 110
    new-instance v0, Lcom/google/maps/g/a/al;

    const-string v1, "MILES_YARDS"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/g/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    .line 114
    new-instance v0, Lcom/google/maps/g/a/al;

    const-string v1, "REGIONAL"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/maps/g/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    .line 97
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/a/al;

    sget-object v1, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/a/al;->f:[Lcom/google/maps/g/a/al;

    .line 154
    new-instance v0, Lcom/google/maps/g/a/am;

    invoke-direct {v0}, Lcom/google/maps/g/a/am;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 164
    iput p3, p0, Lcom/google/maps/g/a/al;->e:I

    .line 165
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/al;
    .locals 1

    .prologue
    .line 140
    packed-switch p0, :pswitch_data_0

    .line 145
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 141
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 142
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 143
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 144
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/al;
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/google/maps/g/a/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/al;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/al;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/maps/g/a/al;->f:[Lcom/google/maps/g/a/al;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/al;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/google/maps/g/a/al;->e:I

    return v0
.end method
