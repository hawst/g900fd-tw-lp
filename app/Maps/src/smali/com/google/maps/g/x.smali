.class public final Lcom/google/maps/g/x;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/aa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/x;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/g/x;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 717
    new-instance v0, Lcom/google/maps/g/y;

    invoke-direct {v0}, Lcom/google/maps/g/y;-><init>()V

    sput-object v0, Lcom/google/maps/g/x;->PARSER:Lcom/google/n/ax;

    .line 833
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/x;->g:Lcom/google/n/aw;

    .line 1076
    new-instance v0, Lcom/google/maps/g/x;

    invoke-direct {v0}, Lcom/google/maps/g/x;-><init>()V

    sput-object v0, Lcom/google/maps/g/x;->d:Lcom/google/maps/g/x;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 667
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 790
    iput-byte v0, p0, Lcom/google/maps/g/x;->e:B

    .line 812
    iput v0, p0, Lcom/google/maps/g/x;->f:I

    .line 668
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/x;->b:I

    .line 669
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    .line 670
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 676
    invoke-direct {p0}, Lcom/google/maps/g/x;-><init>()V

    .line 677
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 681
    const/4 v0, 0x0

    .line 682
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 683
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 684
    sparse-switch v3, :sswitch_data_0

    .line 689
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 691
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 687
    goto :goto_0

    .line 696
    :sswitch_1
    iget v3, p0, Lcom/google/maps/g/x;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/x;->a:I

    .line 697
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/x;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 708
    :catch_0
    move-exception v0

    .line 709
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 714
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/x;->au:Lcom/google/n/bn;

    throw v0

    .line 701
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 702
    iget v4, p0, Lcom/google/maps/g/x;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/g/x;->a:I

    .line 703
    iput-object v3, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 710
    :catch_1
    move-exception v0

    .line 711
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 712
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 714
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/x;->au:Lcom/google/n/bn;

    .line 715
    return-void

    .line 684
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 665
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 790
    iput-byte v0, p0, Lcom/google/maps/g/x;->e:B

    .line 812
    iput v0, p0, Lcom/google/maps/g/x;->f:I

    .line 666
    return-void
.end method

.method public static a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;
    .locals 1

    .prologue
    .line 898
    invoke-static {}, Lcom/google/maps/g/x;->newBuilder()Lcom/google/maps/g/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/z;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/maps/g/x;
    .locals 1

    .prologue
    .line 1079
    sget-object v0, Lcom/google/maps/g/x;->d:Lcom/google/maps/g/x;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/z;
    .locals 1

    .prologue
    .line 895
    new-instance v0, Lcom/google/maps/g/z;

    invoke-direct {v0}, Lcom/google/maps/g/z;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 729
    sget-object v0, Lcom/google/maps/g/x;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 802
    invoke-virtual {p0}, Lcom/google/maps/g/x;->c()I

    .line 803
    iget v0, p0, Lcom/google/maps/g/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 804
    iget v0, p0, Lcom/google/maps/g/x;->b:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 806
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 807
    iget-object v0, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 809
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 810
    return-void

    .line 804
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 807
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 792
    iget-byte v1, p0, Lcom/google/maps/g/x;->e:B

    .line 793
    if-ne v1, v0, :cond_0

    .line 797
    :goto_0
    return v0

    .line 794
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 796
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/x;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 814
    iget v0, p0, Lcom/google/maps/g/x;->f:I

    .line 815
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 828
    :goto_0
    return v0

    .line 818
    :cond_0
    iget v0, p0, Lcom/google/maps/g/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 819
    iget v0, p0, Lcom/google/maps/g/x;->b:I

    .line 820
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 822
    :goto_2
    iget v0, p0, Lcom/google/maps/g/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 824
    iget-object v0, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 826
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 827
    iput v0, p0, Lcom/google/maps/g/x;->f:I

    goto :goto_0

    .line 820
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 824
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 659
    invoke-static {}, Lcom/google/maps/g/x;->newBuilder()Lcom/google/maps/g/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/z;->a(Lcom/google/maps/g/x;)Lcom/google/maps/g/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 659
    invoke-static {}, Lcom/google/maps/g/x;->newBuilder()Lcom/google/maps/g/z;

    move-result-object v0

    return-object v0
.end method
