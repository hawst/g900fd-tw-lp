.class public final Lcom/google/maps/g/a/dh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/dk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/dh;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/a/dh;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    new-instance v0, Lcom/google/maps/g/a/di;

    invoke-direct {v0}, Lcom/google/maps/g/a/di;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/dh;->PARSER:Lcom/google/n/ax;

    .line 695
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/dh;->h:Lcom/google/n/aw;

    .line 933
    new-instance v0, Lcom/google/maps/g/a/dh;

    invoke-direct {v0}, Lcom/google/maps/g/a/dh;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/dh;->e:Lcom/google/maps/g/a/dh;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 529
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 645
    iput-byte v1, p0, Lcom/google/maps/g/a/dh;->f:B

    .line 670
    iput v1, p0, Lcom/google/maps/g/a/dh;->g:I

    .line 530
    iput v0, p0, Lcom/google/maps/g/a/dh;->b:I

    .line 531
    iput v0, p0, Lcom/google/maps/g/a/dh;->c:I

    .line 532
    iput-boolean v0, p0, Lcom/google/maps/g/a/dh;->d:Z

    .line 533
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 539
    invoke-direct {p0}, Lcom/google/maps/g/a/dh;-><init>()V

    .line 540
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 545
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 546
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 547
    sparse-switch v0, :sswitch_data_0

    .line 552
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 554
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 550
    goto :goto_0

    .line 559
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/a/dh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/a/dh;->a:I

    .line 560
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/dh;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 575
    :catch_0
    move-exception v0

    .line 576
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/dh;->au:Lcom/google/n/bn;

    throw v0

    .line 564
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/a/dh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/a/dh;->a:I

    .line 565
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/dh;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 577
    :catch_1
    move-exception v0

    .line 578
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 579
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 569
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/a/dh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/dh;->a:I

    .line 570
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/g/a/dh;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 581
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/dh;->au:Lcom/google/n/bn;

    .line 582
    return-void

    .line 547
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 527
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 645
    iput-byte v0, p0, Lcom/google/maps/g/a/dh;->f:B

    .line 670
    iput v0, p0, Lcom/google/maps/g/a/dh;->g:I

    .line 528
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/dh;
    .locals 1

    .prologue
    .line 936
    sget-object v0, Lcom/google/maps/g/a/dh;->e:Lcom/google/maps/g/a/dh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/dj;
    .locals 1

    .prologue
    .line 757
    new-instance v0, Lcom/google/maps/g/a/dj;

    invoke-direct {v0}, Lcom/google/maps/g/a/dj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/dh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596
    sget-object v0, Lcom/google/maps/g/a/dh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 657
    invoke-virtual {p0}, Lcom/google/maps/g/a/dh;->c()I

    .line 658
    iget v2, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 659
    iget v2, p0, Lcom/google/maps/g/a/dh;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 661
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 662
    iget v2, p0, Lcom/google/maps/g/a/dh;->c:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 664
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 665
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/dh;->d:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 667
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/a/dh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 668
    return-void

    .line 659
    :cond_3
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 662
    :cond_4
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 665
    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 647
    iget-byte v1, p0, Lcom/google/maps/g/a/dh;->f:B

    .line 648
    if-ne v1, v0, :cond_0

    .line 652
    :goto_0
    return v0

    .line 649
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 651
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/a/dh;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 672
    iget v0, p0, Lcom/google/maps/g/a/dh;->g:I

    .line 673
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 690
    :goto_0
    return v0

    .line 676
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 677
    iget v0, p0, Lcom/google/maps/g/a/dh;->b:I

    .line 678
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 680
    :goto_2
    iget v3, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 681
    iget v3, p0, Lcom/google/maps/g/a/dh;->c:I

    .line 682
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 684
    :cond_2
    iget v1, p0, Lcom/google/maps/g/a/dh;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 685
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/maps/g/a/dh;->d:Z

    .line 686
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 688
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/dh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    iput v0, p0, Lcom/google/maps/g/a/dh;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 678
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 521
    invoke-static {}, Lcom/google/maps/g/a/dh;->newBuilder()Lcom/google/maps/g/a/dj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/dj;->a(Lcom/google/maps/g/a/dh;)Lcom/google/maps/g/a/dj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 521
    invoke-static {}, Lcom/google/maps/g/a/dh;->newBuilder()Lcom/google/maps/g/a/dj;

    move-result-object v0

    return-object v0
.end method
