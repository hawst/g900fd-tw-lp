.class public final Lcom/google/maps/g/a/hu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/in;


# static fields
.field private static volatile B:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hu;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final y:Lcom/google/maps/g/a/hu;


# instance fields
.field private A:I

.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Lcom/google/maps/g/a/fk;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ea;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/maps/g/a/fw;

.field g:Ljava/lang/Object;

.field public h:Lcom/google/n/aq;

.field public i:I

.field j:Lcom/google/maps/g/a/be;

.field public k:Lcom/google/maps/g/a/be;

.field public l:Lcom/google/n/aq;

.field public m:Lcom/google/n/aq;

.field public n:I

.field public o:Lcom/google/maps/g/a/be;

.field p:Lcom/google/maps/g/a/da;

.field public q:Lcom/google/maps/g/a/id;

.field public r:Lcom/google/maps/g/a/bi;

.field s:Lcom/google/maps/g/a/hx;

.field public t:Lcom/google/maps/g/a/ij;

.field public u:Lcom/google/n/f;

.field public v:Z

.field public w:Lcom/google/maps/g/a/gc;

.field x:Lcom/google/n/f;

.field private z:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 305
    new-instance v0, Lcom/google/maps/g/a/hv;

    invoke-direct {v0}, Lcom/google/maps/g/a/hv;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hu;->PARSER:Lcom/google/n/ax;

    .line 2839
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/hu;->B:Lcom/google/n/aw;

    .line 4684
    new-instance v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v0}, Lcom/google/maps/g/a/hu;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/hu;->y:Lcom/google/maps/g/a/hu;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2622
    iput-byte v0, p0, Lcom/google/maps/g/a/hu;->z:B

    .line 2719
    iput v0, p0, Lcom/google/maps/g/a/hu;->A:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    .line 23
    iput v1, p0, Lcom/google/maps/g/a/hu;->i:I

    .line 24
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    .line 25
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    .line 26
    iput v1, p0, Lcom/google/maps/g/a/hu;->n:I

    .line 27
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    .line 28
    iput-boolean v1, p0, Lcom/google/maps/g/a/hu;->v:Z

    .line 29
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/maps/g/a/hu;-><init>()V

    .line 37
    const/4 v1, 0x0

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 41
    const/4 v0, 0x0

    move v3, v0

    .line 42
    :cond_0
    :goto_0
    if-nez v3, :cond_16

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 44
    sparse-switch v0, :sswitch_data_0

    .line 49
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 46
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 47
    goto :goto_0

    .line 56
    :sswitch_1
    const/4 v0, 0x0

    .line 57
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v5, 0x4

    if-ne v2, v5, :cond_25

    .line 58
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    invoke-static {}, Lcom/google/maps/g/a/fk;->newBuilder()Lcom/google/maps/g/a/fm;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    move-result-object v0

    move-object v2, v0

    .line 60
    :goto_1
    sget-object v0, Lcom/google/maps/g/a/fk;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    .line 61
    if-eqz v2, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/fm;->a(Lcom/google/maps/g/a/fk;)Lcom/google/maps/g/a/fm;

    .line 63
    invoke-virtual {v2}, Lcom/google/maps/g/a/fm;->c()Lcom/google/maps/g/a/fk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    .line 65
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 284
    :catch_0
    move-exception v0

    .line 285
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 291
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    .line 293
    :cond_2
    and-int/lit8 v2, v1, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 294
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    .line 296
    :cond_3
    and-int/lit16 v2, v1, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_4

    .line 297
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    .line 299
    :cond_4
    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_5

    .line 300
    iget-object v1, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    .line 302
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/hu;->au:Lcom/google/n/bn;

    throw v0

    .line 69
    :sswitch_2
    and-int/lit8 v0, v1, 0x8

    const/16 v2, 0x8

    if-eq v0, v2, :cond_6

    .line 70
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    .line 71
    or-int/lit8 v1, v1, 0x8

    .line 73
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/ea;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 286
    :catch_1
    move-exception v0

    .line 287
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 288
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 77
    :sswitch_3
    const/4 v0, 0x0

    .line 78
    :try_start_4
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v5, 0x8

    if-ne v2, v5, :cond_24

    .line 79
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    invoke-static {}, Lcom/google/maps/g/a/fw;->newBuilder()Lcom/google/maps/g/a/fy;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    move-result-object v0

    move-object v2, v0

    .line 81
    :goto_2
    sget-object v0, Lcom/google/maps/g/a/fw;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fw;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    .line 82
    if-eqz v2, :cond_7

    .line 83
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/fy;->a(Lcom/google/maps/g/a/fw;)Lcom/google/maps/g/a/fy;

    .line 84
    invoke-virtual {v2}, Lcom/google/maps/g/a/fy;->c()Lcom/google/maps/g/a/fw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    .line 86
    :cond_7
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 90
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 91
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 92
    iput-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 96
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 97
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 98
    iput-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 102
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 103
    and-int/lit8 v2, v1, 0x40

    const/16 v5, 0x40

    if-eq v2, v5, :cond_8

    .line 104
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    .line 105
    or-int/lit8 v1, v1, 0x40

    .line 107
    :cond_8
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 111
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 113
    iput-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 117
    :sswitch_8
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/g/a/hu;->i:I

    goto/16 :goto_0

    .line 122
    :sswitch_9
    const/4 v0, 0x0

    .line 123
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v5, 0x40

    if-ne v2, v5, :cond_23

    .line 124
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->newBuilder()Lcom/google/maps/g/a/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    move-object v2, v0

    .line 126
    :goto_3
    sget-object v0, Lcom/google/maps/g/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    .line 127
    if-eqz v2, :cond_9

    .line 128
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    .line 129
    invoke-virtual {v2}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    .line 131
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 135
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 136
    and-int/lit16 v2, v1, 0x400

    const/16 v5, 0x400

    if-eq v2, v5, :cond_a

    .line 137
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    .line 138
    or-int/lit16 v1, v1, 0x400

    .line 140
    :cond_a
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 144
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 145
    and-int/lit16 v2, v1, 0x800

    const/16 v5, 0x800

    if-eq v2, v5, :cond_b

    .line 146
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    .line 147
    or-int/lit16 v1, v1, 0x800

    .line 149
    :cond_b
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 153
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 154
    invoke-static {v0}, Lcom/google/maps/g/a/ib;->a(I)Lcom/google/maps/g/a/ib;

    move-result-object v2

    .line 155
    if-nez v2, :cond_c

    .line 156
    const/16 v2, 0xd

    invoke-virtual {v4, v2, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 158
    :cond_c
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 159
    iput v0, p0, Lcom/google/maps/g/a/hu;->n:I

    goto/16 :goto_0

    .line 164
    :sswitch_d
    const/4 v0, 0x0

    .line 165
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v5, 0x80

    if-ne v2, v5, :cond_22

    .line 166
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->newBuilder()Lcom/google/maps/g/a/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    move-object v2, v0

    .line 168
    :goto_4
    sget-object v0, Lcom/google/maps/g/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    .line 169
    if-eqz v2, :cond_d

    .line 170
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    .line 171
    invoke-virtual {v2}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    .line 173
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 177
    :sswitch_e
    const/4 v0, 0x0

    .line 178
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v5, 0x200

    if-ne v2, v5, :cond_21

    .line 179
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    invoke-static {}, Lcom/google/maps/g/a/be;->newBuilder()Lcom/google/maps/g/a/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    move-result-object v0

    move-object v2, v0

    .line 181
    :goto_5
    sget-object v0, Lcom/google/maps/g/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    .line 182
    if-eqz v2, :cond_e

    .line 183
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bg;->a(Lcom/google/maps/g/a/be;)Lcom/google/maps/g/a/bg;

    .line 184
    invoke-virtual {v2}, Lcom/google/maps/g/a/bg;->c()Lcom/google/maps/g/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    .line 186
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 190
    :sswitch_f
    const/4 v0, 0x0

    .line 191
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v5, 0x400

    if-ne v2, v5, :cond_20

    .line 192
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    invoke-static {}, Lcom/google/maps/g/a/da;->newBuilder()Lcom/google/maps/g/a/dc;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    move-result-object v0

    move-object v2, v0

    .line 194
    :goto_6
    sget-object v0, Lcom/google/maps/g/a/da;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    .line 195
    if-eqz v2, :cond_f

    .line 196
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/dc;->a(Lcom/google/maps/g/a/da;)Lcom/google/maps/g/a/dc;

    .line 197
    invoke-virtual {v2}, Lcom/google/maps/g/a/dc;->c()Lcom/google/maps/g/a/da;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    .line 199
    :cond_f
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 203
    :sswitch_10
    const/4 v0, 0x0

    .line 204
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v5, 0x800

    if-ne v2, v5, :cond_1f

    .line 205
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    invoke-static {v0}, Lcom/google/maps/g/a/id;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    move-result-object v0

    move-object v2, v0

    .line 207
    :goto_7
    sget-object v0, Lcom/google/maps/g/a/id;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/id;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    .line 208
    if-eqz v2, :cond_10

    .line 209
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/if;->a(Lcom/google/maps/g/a/id;)Lcom/google/maps/g/a/if;

    .line 210
    invoke-virtual {v2}, Lcom/google/maps/g/a/if;->c()Lcom/google/maps/g/a/id;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    .line 212
    :cond_10
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 216
    :sswitch_11
    const/4 v0, 0x0

    .line 217
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v5, 0x1000

    if-ne v2, v5, :cond_1e

    .line 218
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    invoke-static {}, Lcom/google/maps/g/a/bi;->newBuilder()Lcom/google/maps/g/a/bk;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    move-result-object v0

    move-object v2, v0

    .line 220
    :goto_8
    sget-object v0, Lcom/google/maps/g/a/bi;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/bi;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    .line 221
    if-eqz v2, :cond_11

    .line 222
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/bk;->a(Lcom/google/maps/g/a/bi;)Lcom/google/maps/g/a/bk;

    .line 223
    invoke-virtual {v2}, Lcom/google/maps/g/a/bk;->c()Lcom/google/maps/g/a/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    .line 225
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 229
    :sswitch_12
    const/4 v0, 0x0

    .line 230
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v5, 0x2000

    if-ne v2, v5, :cond_1d

    .line 231
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    invoke-static {v0}, Lcom/google/maps/g/a/hx;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    move-result-object v0

    move-object v2, v0

    .line 233
    :goto_9
    sget-object v0, Lcom/google/maps/g/a/hx;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hx;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    .line 234
    if-eqz v2, :cond_12

    .line 235
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/hz;->a(Lcom/google/maps/g/a/hx;)Lcom/google/maps/g/a/hz;

    .line 236
    invoke-virtual {v2}, Lcom/google/maps/g/a/hz;->c()Lcom/google/maps/g/a/hx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    .line 238
    :cond_12
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 242
    :sswitch_13
    const/4 v0, 0x0

    .line 243
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v5, 0x4000

    if-ne v2, v5, :cond_1c

    .line 244
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    invoke-static {v0}, Lcom/google/maps/g/a/ij;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    move-result-object v0

    move-object v2, v0

    .line 246
    :goto_a
    sget-object v0, Lcom/google/maps/g/a/ij;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ij;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    .line 247
    if-eqz v2, :cond_13

    .line 248
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/il;->a(Lcom/google/maps/g/a/ij;)Lcom/google/maps/g/a/il;

    .line 249
    invoke-virtual {v2}, Lcom/google/maps/g/a/il;->c()Lcom/google/maps/g/a/ij;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    .line 251
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 255
    :sswitch_14
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const v2, 0x8000

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 256
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    goto/16 :goto_0

    .line 260
    :sswitch_15
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 261
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lcom/google/maps/g/a/hu;->v:Z

    goto/16 :goto_0

    :cond_14
    const/4 v0, 0x0

    goto :goto_b

    .line 265
    :sswitch_16
    const/4 v0, 0x0

    .line 266
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v5, 0x20000

    and-int/2addr v2, v5

    const/high16 v5, 0x20000

    if-ne v2, v5, :cond_1b

    .line 267
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    invoke-static {}, Lcom/google/maps/g/a/gc;->newBuilder()Lcom/google/maps/g/a/ge;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ge;->a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;

    move-result-object v0

    move-object v2, v0

    .line 269
    :goto_c
    sget-object v0, Lcom/google/maps/g/a/gc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gc;

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    .line 270
    if-eqz v2, :cond_15

    .line 271
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/ge;->a(Lcom/google/maps/g/a/gc;)Lcom/google/maps/g/a/ge;

    .line 272
    invoke-virtual {v2}, Lcom/google/maps/g/a/ge;->c()Lcom/google/maps/g/a/gc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    .line 274
    :cond_15
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    goto/16 :goto_0

    .line 278
    :sswitch_17
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v2, 0x40000

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/maps/g/a/hu;->a:I

    .line 279
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 290
    :cond_16
    and-int/lit8 v0, v1, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_17

    .line 291
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    .line 293
    :cond_17
    and-int/lit8 v0, v1, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_18

    .line 294
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    .line 296
    :cond_18
    and-int/lit16 v0, v1, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_19

    .line 297
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    .line 299
    :cond_19
    and-int/lit16 v0, v1, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_1a

    .line 300
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    .line 302
    :cond_1a
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->au:Lcom/google/n/bn;

    .line 303
    return-void

    :cond_1b
    move-object v2, v0

    goto :goto_c

    :cond_1c
    move-object v2, v0

    goto/16 :goto_a

    :cond_1d
    move-object v2, v0

    goto/16 :goto_9

    :cond_1e
    move-object v2, v0

    goto/16 :goto_8

    :cond_1f
    move-object v2, v0

    goto/16 :goto_7

    :cond_20
    move-object v2, v0

    goto/16 :goto_6

    :cond_21
    move-object v2, v0

    goto/16 :goto_5

    :cond_22
    move-object v2, v0

    goto/16 :goto_4

    :cond_23
    move-object v2, v0

    goto/16 :goto_3

    :cond_24
    move-object v2, v0

    goto/16 :goto_2

    :cond_25
    move-object v2, v0

    goto/16 :goto_1

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb0 -> :sswitch_15
        0xba -> :sswitch_16
        0xc2 -> :sswitch_17
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2622
    iput-byte v0, p0, Lcom/google/maps/g/a/hu;->z:B

    .line 2719
    iput v0, p0, Lcom/google/maps/g/a/hu;->A:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/hu;
    .locals 1

    .prologue
    .line 4687
    sget-object v0, Lcom/google/maps/g/a/hu;->y:Lcom/google/maps/g/a/hu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/hw;
    .locals 1

    .prologue
    .line 2901
    new-instance v0, Lcom/google/maps/g/a/hw;

    invoke-direct {v0}, Lcom/google/maps/g/a/hw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/hu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    sget-object v0, Lcom/google/maps/g/a/hu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 2646
    invoke-virtual {p0}, Lcom/google/maps/g/a/hu;->c()I

    .line 2647
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_0

    .line 2648
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    :cond_0
    move v1, v2

    .line 2650
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2651
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2650
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2648
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    .line 2653
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 2654
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v0

    :goto_2
    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2656
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 2657
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2659
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    .line 2660
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_5
    move v0, v2

    .line 2662
    :goto_5
    iget-object v1, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 2663
    const/4 v1, 0x7

    iget-object v4, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2662
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2654
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    goto/16 :goto_2

    .line 2657
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2660
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 2665
    :cond_9
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    .line 2666
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2668
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_b

    .line 2669
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/g/a/hu;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2671
    :cond_b
    :goto_7
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 2672
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_8
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    :cond_c
    move v0, v2

    .line 2674
    :goto_9
    iget-object v1, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_10

    .line 2675
    const/16 v1, 0xb

    iget-object v4, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2674
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 2666
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 2669
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 2672
    :cond_f
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    goto :goto_8

    :cond_10
    move v0, v2

    .line 2677
    :goto_a
    iget-object v1, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_11

    .line 2678
    const/16 v1, 0xc

    iget-object v4, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2677
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2680
    :cond_11
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_12

    .line 2681
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/g/a/hu;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_1f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2683
    :cond_12
    :goto_b
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_13

    .line 2684
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_20

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_c
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2686
    :cond_13
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_14

    .line 2687
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_21

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_d
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2689
    :cond_14
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_15

    .line 2690
    const/16 v1, 0x10

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    if-nez v0, :cond_22

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v0

    :goto_e
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2692
    :cond_15
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_16

    .line 2693
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    if-nez v0, :cond_23

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v0

    :goto_f
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2695
    :cond_16
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_17

    .line 2696
    const/16 v1, 0x12

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v0, :cond_24

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    :goto_10
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2698
    :cond_17
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_18

    .line 2699
    const/16 v1, 0x13

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    if-nez v0, :cond_25

    invoke-static {}, Lcom/google/maps/g/a/hx;->d()Lcom/google/maps/g/a/hx;

    move-result-object v0

    :goto_11
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2701
    :cond_18
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_19

    .line 2702
    const/16 v1, 0x14

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    if-nez v0, :cond_26

    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v0

    :goto_12
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2704
    :cond_19
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_1a

    .line 2705
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2707
    :cond_1a
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_1c

    .line 2708
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/maps/g/a/hu;->v:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1b

    move v2, v3

    :cond_1b
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 2710
    :cond_1c
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_1d

    .line 2711
    const/16 v1, 0x17

    iget-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    if-nez v0, :cond_27

    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v0

    :goto_13
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2713
    :cond_1d
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_1e

    .line 2714
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2716
    :cond_1e
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2717
    return-void

    .line 2681
    :cond_1f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_b

    .line 2684
    :cond_20
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_c

    .line 2687
    :cond_21
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    goto/16 :goto_d

    .line 2690
    :cond_22
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    goto/16 :goto_e

    .line 2693
    :cond_23
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    goto/16 :goto_f

    .line 2696
    :cond_24
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto/16 :goto_10

    .line 2699
    :cond_25
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    goto/16 :goto_11

    .line 2702
    :cond_26
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    goto/16 :goto_12

    .line 2711
    :cond_27
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    goto :goto_13
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2624
    iget-byte v0, p0, Lcom/google/maps/g/a/hu;->z:B

    .line 2625
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2641
    :cond_0
    :goto_0
    return v2

    .line 2626
    :cond_1
    if-eqz v0, :cond_0

    .line 2628
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 2629
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2630
    iput-byte v2, p0, Lcom/google/maps/g/a/hu;->z:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2628
    goto :goto_1

    .line 2629
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    :cond_4
    move v1, v2

    .line 2634
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2635
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ea;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2636
    iput-byte v2, p0, Lcom/google/maps/g/a/hu;->z:B

    goto :goto_0

    .line 2634
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2640
    :cond_6
    iput-byte v3, p0, Lcom/google/maps/g/a/hu;->z:B

    move v2, v3

    .line 2641
    goto :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 2721
    iget v0, p0, Lcom/google/maps/g/a/hu;->A:I

    .line 2722
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2834
    :goto_0
    return v0

    .line 2725
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_27

    .line 2727
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_1
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 2729
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2730
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    .line 2731
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 2729
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2727
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1

    .line 2733
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 2735
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v0

    :goto_4
    invoke-static {v9, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 2737
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_4

    .line 2738
    const/4 v2, 0x5

    .line 2739
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 2741
    :cond_4
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_5

    .line 2742
    const/4 v2, 0x6

    .line 2743
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_5
    move v0, v1

    move v2, v1

    .line 2747
    :goto_7
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_9

    .line 2748
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    .line 2749
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 2747
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2735
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    goto/16 :goto_4

    .line 2739
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 2743
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 2751
    :cond_9
    add-int v0, v3, v2

    .line 2752
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 2754
    iget v0, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_26

    .line 2755
    const/16 v3, 0x8

    .line 2756
    iget-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/hu;->g:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 2758
    :goto_9
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_a

    .line 2759
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/maps/g/a/hu;->i:I

    .line 2760
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_a
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 2762
    :cond_a
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_b

    .line 2764
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    if-nez v2, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_b
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_b
    move v2, v1

    move v3, v1

    .line 2768
    :goto_c
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v2, v5, :cond_f

    .line 2769
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    .line 2770
    invoke-interface {v5, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 2768
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 2756
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_d
    move v2, v4

    .line 2760
    goto :goto_a

    .line 2764
    :cond_e
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->j:Lcom/google/maps/g/a/be;

    goto :goto_b

    .line 2772
    :cond_f
    add-int/2addr v0, v3

    .line 2773
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 2777
    :goto_d
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_10

    .line 2778
    iget-object v5, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    .line 2779
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 2777
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2781
    :cond_10
    add-int v0, v3, v2

    .line 2782
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2784
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    .line 2785
    const/16 v2, 0xd

    iget v3, p0, Lcom/google/maps/g/a/hu;->n:I

    .line 2786
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_11
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 2788
    :cond_12
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    .line 2789
    const/16 v3, 0xe

    .line 2790
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    if-nez v2, :cond_1e

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_e
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2792
    :cond_13
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    .line 2793
    const/16 v3, 0xf

    .line 2794
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    if-nez v2, :cond_1f

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_f
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2796
    :cond_14
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_15

    .line 2797
    const/16 v3, 0x10

    .line 2798
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    if-nez v2, :cond_20

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v2

    :goto_10
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2800
    :cond_15
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_16

    .line 2801
    const/16 v3, 0x11

    .line 2802
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    if-nez v2, :cond_21

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v2

    :goto_11
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2804
    :cond_16
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_17

    .line 2805
    const/16 v3, 0x12

    .line 2806
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v2, :cond_22

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v2

    :goto_12
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2808
    :cond_17
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_18

    .line 2809
    const/16 v3, 0x13

    .line 2810
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    if-nez v2, :cond_23

    invoke-static {}, Lcom/google/maps/g/a/hx;->d()Lcom/google/maps/g/a/hx;

    move-result-object v2

    :goto_13
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2812
    :cond_18
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_19

    .line 2813
    const/16 v3, 0x14

    .line 2814
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    if-nez v2, :cond_24

    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v2

    :goto_14
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2816
    :cond_19
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    const v3, 0x8000

    and-int/2addr v2, v3

    const v3, 0x8000

    if-ne v2, v3, :cond_1a

    .line 2817
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    .line 2818
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2820
    :cond_1a
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_1b

    .line 2821
    const/16 v2, 0x16

    iget-boolean v3, p0, Lcom/google/maps/g/a/hu;->v:Z

    .line 2822
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2824
    :cond_1b
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_1c

    .line 2825
    const/16 v3, 0x17

    .line 2826
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    if-nez v2, :cond_25

    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v2

    :goto_15
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2828
    :cond_1c
    iget v2, p0, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_1d

    .line 2829
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/maps/g/a/hu;->x:Lcom/google/n/f;

    .line 2830
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2832
    :cond_1d
    iget-object v1, p0, Lcom/google/maps/g/a/hu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2833
    iput v0, p0, Lcom/google/maps/g/a/hu;->A:I

    goto/16 :goto_0

    .line 2790
    :cond_1e
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_e

    .line 2794
    :cond_1f
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    goto/16 :goto_f

    .line 2798
    :cond_20
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->p:Lcom/google/maps/g/a/da;

    goto/16 :goto_10

    .line 2802
    :cond_21
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    goto/16 :goto_11

    .line 2806
    :cond_22
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto/16 :goto_12

    .line 2810
    :cond_23
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->s:Lcom/google/maps/g/a/hx;

    goto/16 :goto_13

    .line 2814
    :cond_24
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    goto/16 :goto_14

    .line 2826
    :cond_25
    iget-object v2, p0, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    goto :goto_15

    :cond_26
    move v0, v2

    goto/16 :goto_9

    :cond_27
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/hu;->newBuilder()Lcom/google/maps/g/a/hw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/hw;->a(Lcom/google/maps/g/a/hu;)Lcom/google/maps/g/a/hw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/hu;->newBuilder()Lcom/google/maps/g/a/hw;

    move-result-object v0

    return-object v0
.end method
