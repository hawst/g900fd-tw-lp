.class public final enum Lcom/google/maps/g/a/dd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/dd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/dd;

.field public static final enum b:Lcom/google/maps/g/a/dd;

.field public static final enum c:Lcom/google/maps/g/a/dd;

.field private static final synthetic e:[Lcom/google/maps/g/a/dd;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 213
    new-instance v0, Lcom/google/maps/g/a/dd;

    const-string v1, "ALERT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/dd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dd;->a:Lcom/google/maps/g/a/dd;

    .line 217
    new-instance v0, Lcom/google/maps/g/a/dd;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/dd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dd;->b:Lcom/google/maps/g/a/dd;

    .line 221
    new-instance v0, Lcom/google/maps/g/a/dd;

    const-string v1, "INFORMATION"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/dd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    .line 208
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/a/dd;

    sget-object v1, Lcom/google/maps/g/a/dd;->a:Lcom/google/maps/g/a/dd;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/dd;->b:Lcom/google/maps/g/a/dd;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/a/dd;->e:[Lcom/google/maps/g/a/dd;

    .line 256
    new-instance v0, Lcom/google/maps/g/a/de;

    invoke-direct {v0}, Lcom/google/maps/g/a/de;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 265
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 266
    iput p3, p0, Lcom/google/maps/g/a/dd;->d:I

    .line 267
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/dd;
    .locals 1

    .prologue
    .line 243
    packed-switch p0, :pswitch_data_0

    .line 247
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 244
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/dd;->a:Lcom/google/maps/g/a/dd;

    goto :goto_0

    .line 245
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/dd;->b:Lcom/google/maps/g/a/dd;

    goto :goto_0

    .line 246
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    goto :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/dd;
    .locals 1

    .prologue
    .line 208
    const-class v0, Lcom/google/maps/g/a/dd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dd;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/dd;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/google/maps/g/a/dd;->e:[Lcom/google/maps/g/a/dd;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/dd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/dd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/google/maps/g/a/dd;->d:I

    return v0
.end method
