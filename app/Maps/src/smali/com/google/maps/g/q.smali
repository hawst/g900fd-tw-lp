.class public final Lcom/google/maps/g/q;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/ad;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/q;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/maps/g/q;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:I

.field e:Ljava/lang/Object;

.field public f:Lcom/google/maps/g/ky;

.field public g:Lcom/google/n/f;

.field public h:Lcom/google/maps/g/u;

.field public i:Ljava/lang/Object;

.field j:Lcom/google/maps/g/i;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 476
    new-instance v0, Lcom/google/maps/g/r;

    invoke-direct {v0}, Lcom/google/maps/g/r;-><init>()V

    sput-object v0, Lcom/google/maps/g/q;->PARSER:Lcom/google/n/ax;

    .line 1674
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/q;->n:Lcom/google/n/aw;

    .line 2436
    new-instance v0, Lcom/google/maps/g/q;

    invoke-direct {v0}, Lcom/google/maps/g/q;-><init>()V

    sput-object v0, Lcom/google/maps/g/q;->k:Lcom/google/maps/g/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 354
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1582
    iput-byte v0, p0, Lcom/google/maps/g/q;->l:B

    .line 1625
    iput v0, p0, Lcom/google/maps/g/q;->m:I

    .line 355
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    .line 356
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/q;->d:I

    .line 358
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    .line 360
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    .line 361
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 367
    invoke-direct {p0}, Lcom/google/maps/g/q;-><init>()V

    .line 368
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 372
    const/4 v0, 0x0

    move v3, v0

    .line 373
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 374
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 375
    sparse-switch v0, :sswitch_data_0

    .line 380
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 382
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 378
    goto :goto_0

    .line 387
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 388
    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v1

    .line 389
    if-nez v1, :cond_1

    .line 390
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 467
    :catch_0
    move-exception v0

    .line 468
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/q;->au:Lcom/google/n/bn;

    throw v0

    .line 392
    :cond_1
    :try_start_2
    iget v1, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/q;->a:I

    .line 393
    iput v0, p0, Lcom/google/maps/g/q;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 469
    :catch_1
    move-exception v0

    .line 470
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 471
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 398
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 399
    iget v1, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/q;->a:I

    .line 400
    iput-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    goto :goto_0

    .line 405
    :sswitch_3
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 406
    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    invoke-static {v0}, Lcom/google/maps/g/ky;->a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;

    move-result-object v0

    move-object v1, v0

    .line 408
    :goto_1
    sget-object v0, Lcom/google/maps/g/ky;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ky;

    iput-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    .line 409
    if-eqz v1, :cond_2

    .line 410
    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/la;->a(Lcom/google/maps/g/ky;)Lcom/google/maps/g/la;

    .line 411
    invoke-virtual {v1}, Lcom/google/maps/g/la;->c()Lcom/google/maps/g/ky;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    .line 413
    :cond_2
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/q;->a:I

    goto :goto_0

    .line 417
    :sswitch_4
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/g/q;->a:I

    .line 418
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    goto/16 :goto_0

    .line 423
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 424
    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    invoke-static {v0}, Lcom/google/maps/g/u;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    move-result-object v0

    move-object v1, v0

    .line 426
    :goto_2
    sget-object v0, Lcom/google/maps/g/u;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/u;

    iput-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    .line 427
    if-eqz v1, :cond_3

    .line 428
    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/w;->a(Lcom/google/maps/g/u;)Lcom/google/maps/g/w;

    .line 429
    invoke-virtual {v1}, Lcom/google/maps/g/w;->c()Lcom/google/maps/g/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    .line 431
    :cond_3
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/g/q;->a:I

    goto/16 :goto_0

    .line 435
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 436
    iget v1, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/q;->a:I

    .line 437
    iput-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 441
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 442
    iget v1, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/q;->a:I

    .line 443
    iput-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 447
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 448
    iget v1, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/q;->a:I

    .line 449
    iput-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 454
    :sswitch_9
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 455
    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    invoke-static {}, Lcom/google/maps/g/i;->newBuilder()Lcom/google/maps/g/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/k;->a(Lcom/google/maps/g/i;)Lcom/google/maps/g/k;

    move-result-object v0

    move-object v1, v0

    .line 457
    :goto_3
    sget-object v0, Lcom/google/maps/g/i;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/i;

    iput-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    .line 458
    if-eqz v1, :cond_4

    .line 459
    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/k;->a(Lcom/google/maps/g/i;)Lcom/google/maps/g/k;

    .line 460
    invoke-virtual {v1}, Lcom/google/maps/g/k;->c()Lcom/google/maps/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    .line 462
    :cond_4
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/q;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 473
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->au:Lcom/google/n/bn;

    .line 474
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_3

    :cond_7
    move-object v1, v2

    goto/16 :goto_2

    :cond_8
    move-object v1, v2

    goto/16 :goto_1

    .line 375
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 352
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1582
    iput-byte v0, p0, Lcom/google/maps/g/q;->l:B

    .line 1625
    iput v0, p0, Lcom/google/maps/g/q;->m:I

    .line 353
    return-void
.end method

.method public static h()Lcom/google/maps/g/q;
    .locals 1

    .prologue
    .line 2439
    sget-object v0, Lcom/google/maps/g/q;->k:Lcom/google/maps/g/q;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/ac;
    .locals 1

    .prologue
    .line 1736
    new-instance v0, Lcom/google/maps/g/ac;

    invoke-direct {v0}, Lcom/google/maps/g/ac;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488
    sget-object v0, Lcom/google/maps/g/q;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 1594
    invoke-virtual {p0}, Lcom/google/maps/g/q;->c()I

    .line 1595
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_0

    .line 1596
    iget v0, p0, Lcom/google/maps/g/q;->d:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1598
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_1

    .line 1599
    iget-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1601
    :cond_1
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 1602
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v0

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1604
    :cond_2
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 1605
    iget-object v0, p0, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1607
    :cond_3
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 1608
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v0

    :goto_3
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1610
    :cond_4
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 1611
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1613
    :cond_5
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 1614
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1616
    :cond_6
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_7

    .line 1617
    iget-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    :goto_6
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1619
    :cond_7
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 1620
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/g/i;->d()Lcom/google/maps/g/i;

    move-result-object v0

    :goto_7
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1622
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/q;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1623
    return-void

    .line 1596
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 1599
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1602
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    goto/16 :goto_2

    .line 1608
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    goto/16 :goto_3

    .line 1611
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 1614
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 1617
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 1620
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    goto :goto_7
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1584
    iget-byte v1, p0, Lcom/google/maps/g/q;->l:B

    .line 1585
    if-ne v1, v0, :cond_0

    .line 1589
    :goto_0
    return v0

    .line 1586
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1588
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/q;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1627
    iget v0, p0, Lcom/google/maps/g/q;->m:I

    .line 1628
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1669
    :goto_0
    return v0

    .line 1631
    :cond_0
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_11

    .line 1632
    iget v0, p0, Lcom/google/maps/g/q;->d:I

    .line 1633
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1635
    :goto_2
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_1

    .line 1637
    iget-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1639
    :cond_1
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 1640
    const/4 v3, 0x3

    .line 1641
    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v0

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1643
    :cond_2
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    .line 1644
    iget-object v0, p0, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    .line 1645
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1647
    :cond_3
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_4

    .line 1648
    const/4 v3, 0x5

    .line 1649
    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v0

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1651
    :cond_4
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_5

    .line 1652
    const/4 v3, 0x6

    .line 1653
    iget-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1655
    :cond_5
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_6

    .line 1656
    const/4 v3, 0x7

    .line 1657
    iget-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1659
    :cond_6
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_7

    .line 1661
    iget-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/q;->c:Ljava/lang/Object;

    :goto_8
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1663
    :cond_7
    iget v0, p0, Lcom/google/maps/g/q;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 1664
    const/16 v3, 0x9

    .line 1665
    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/g/i;->d()Lcom/google/maps/g/i;

    move-result-object v0

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1667
    :cond_8
    iget-object v0, p0, Lcom/google/maps/g/q;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1668
    iput v0, p0, Lcom/google/maps/g/q;->m:I

    goto/16 :goto_0

    .line 1633
    :cond_9
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 1637
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1641
    :cond_b
    iget-object v0, p0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    goto/16 :goto_4

    .line 1649
    :cond_c
    iget-object v0, p0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    goto/16 :goto_5

    .line 1653
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1657
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 1661
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 1665
    :cond_10
    iget-object v0, p0, Lcom/google/maps/g/q;->j:Lcom/google/maps/g/i;

    goto :goto_9

    :cond_11
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    .line 1351
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1352
    check-cast v0, Ljava/lang/String;

    .line 1360
    :goto_0
    return-object v0

    .line 1354
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1356
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1357
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1358
    iput-object v1, p0, Lcom/google/maps/g/q;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1360
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 346
    invoke-static {}, Lcom/google/maps/g/q;->newBuilder()Lcom/google/maps/g/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/ac;->a(Lcom/google/maps/g/q;)Lcom/google/maps/g/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 346
    invoke-static {}, Lcom/google/maps/g/q;->newBuilder()Lcom/google/maps/g/ac;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1450
    iget-object v0, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    .line 1451
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1452
    check-cast v0, Ljava/lang/String;

    .line 1460
    :goto_0
    return-object v0

    .line 1454
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1456
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1457
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1458
    iput-object v1, p0, Lcom/google/maps/g/q;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1460
    goto :goto_0
.end method
