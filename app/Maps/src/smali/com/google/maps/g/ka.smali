.class public final enum Lcom/google/maps/g/ka;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/ka;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/ka;

.field public static final enum b:Lcom/google/maps/g/ka;

.field private static final synthetic d:[Lcom/google/maps/g/ka;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    new-instance v0, Lcom/google/maps/g/ka;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ka;->a:Lcom/google/maps/g/ka;

    .line 121
    new-instance v0, Lcom/google/maps/g/ka;

    const-string v1, "TERRAIN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/ka;->b:Lcom/google/maps/g/ka;

    .line 112
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/maps/g/ka;

    sget-object v1, Lcom/google/maps/g/ka;->a:Lcom/google/maps/g/ka;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/ka;->b:Lcom/google/maps/g/ka;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/ka;->d:[Lcom/google/maps/g/ka;

    .line 151
    new-instance v0, Lcom/google/maps/g/kb;

    invoke-direct {v0}, Lcom/google/maps/g/kb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    iput p3, p0, Lcom/google/maps/g/ka;->c:I

    .line 162
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/ka;
    .locals 1

    .prologue
    .line 139
    packed-switch p0, :pswitch_data_0

    .line 142
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 140
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/ka;->a:Lcom/google/maps/g/ka;

    goto :goto_0

    .line 141
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/ka;->b:Lcom/google/maps/g/ka;

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/ka;
    .locals 1

    .prologue
    .line 112
    const-class v0, Lcom/google/maps/g/ka;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ka;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/ka;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/google/maps/g/ka;->d:[Lcom/google/maps/g/ka;

    invoke-virtual {v0}, [Lcom/google/maps/g/ka;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/ka;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/maps/g/ka;->c:I

    return v0
.end method
