.class public Lcom/google/maps/g/b/ac;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/e/a/a/a/d;

.field public static final b:Lcom/google/e/a/a/a/d;

.field public static final c:Lcom/google/e/a/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/16 v8, 0x21b

    const/4 v7, 0x0

    const/16 v6, 0x218

    .line 36
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/ac;->a:Lcom/google/e/a/a/a/d;

    .line 37
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/ac;->b:Lcom/google/e/a/a/a/d;

    .line 38
    new-instance v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v0}, Lcom/google/e/a/a/a/d;-><init>()V

    sput-object v0, Lcom/google/maps/g/b/ac;->c:Lcom/google/e/a/a/a/d;

    .line 40
    sget-object v0, Lcom/google/maps/g/b/ac;->a:Lcom/google/e/a/a/a/d;

    sget-object v1, Lcom/google/maps/g/b/o;->a:Lcom/google/e/a/a/a/d;

    .line 41
    invoke-virtual {v0, v8, v9, v1}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    .line 44
    invoke-virtual {v0, v6, v10, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x3

    .line 47
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x4

    .line 50
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/b/ah;->f:Lcom/google/e/a/a/a/d;

    .line 53
    invoke-virtual {v0, v8, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x6

    .line 56
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x7

    .line 59
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/b/ac;->c:Lcom/google/e/a/a/a/d;

    .line 62
    invoke-virtual {v0, v8, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x9

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    .line 65
    invoke-virtual {v0, v6, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/g/b/ao;->a:Lcom/google/e/a/a/a/d;

    .line 68
    invoke-virtual {v0, v8, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/g/a/a/a;->i:Lcom/google/e/a/a/a/d;

    .line 71
    invoke-virtual {v0, v8, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x215

    const/16 v2, 0xc

    const-wide/16 v4, 0x4

    .line 76
    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    .line 74
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0xd

    .line 77
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/g/b/ac;->b:Lcom/google/e/a/a/a/d;

    .line 80
    invoke-virtual {v0, v8, v1, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0xf

    .line 83
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x10

    .line 86
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 90
    sget-object v0, Lcom/google/maps/g/b/ac;->b:Lcom/google/e/a/a/a/d;

    const/16 v1, 0x215

    const-wide/16 v2, 0xa

    .line 93
    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    .line 91
    invoke-virtual {v0, v1, v9, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/16 v1, 0x215

    const-wide/16 v2, 0x0

    .line 96
    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    .line 94
    invoke-virtual {v0, v1, v10, v2}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 98
    sget-object v0, Lcom/google/maps/g/b/ac;->c:Lcom/google/e/a/a/a/d;

    .line 99
    invoke-virtual {v0, v6, v9, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v6, v10, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x3

    .line 105
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    move-result-object v0

    const/4 v1, 0x4

    .line 108
    invoke-virtual {v0, v6, v1, v7}, Lcom/google/e/a/a/a/d;->a(IILjava/lang/Object;)Lcom/google/e/a/a/a/d;

    .line 112
    return-void
.end method
