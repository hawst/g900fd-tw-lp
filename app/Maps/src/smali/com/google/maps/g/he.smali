.class public final Lcom/google/maps/g/he;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/hc;",
        "Lcom/google/maps/g/he;",
        ">;",
        "Lcom/google/maps/g/hf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcom/google/maps/g/hc;->e:Lcom/google/maps/g/hc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 385
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/he;->b:Ljava/lang/Object;

    .line 461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/he;->c:Lcom/google/n/ao;

    .line 520
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/he;->d:Ljava/lang/Object;

    .line 321
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/hc;)Lcom/google/maps/g/he;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 360
    invoke-static {}, Lcom/google/maps/g/hc;->h()Lcom/google/maps/g/hc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 376
    :goto_0
    return-object p0

    .line 361
    :cond_0
    iget v2, p1, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 362
    iget v2, p0, Lcom/google/maps/g/he;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/he;->a:I

    .line 363
    iget-object v2, p1, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/he;->b:Ljava/lang/Object;

    .line 366
    :cond_1
    iget v2, p1, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 367
    iget-object v2, p0, Lcom/google/maps/g/he;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 368
    iget v2, p0, Lcom/google/maps/g/he;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/he;->a:I

    .line 370
    :cond_2
    iget v2, p1, Lcom/google/maps/g/hc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 371
    iget v0, p0, Lcom/google/maps/g/he;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/he;->a:I

    .line 372
    iget-object v0, p1, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/he;->d:Ljava/lang/Object;

    .line 375
    :cond_3
    iget-object v0, p1, Lcom/google/maps/g/hc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 361
    goto :goto_1

    :cond_5
    move v2, v1

    .line 366
    goto :goto_2

    :cond_6
    move v0, v1

    .line 370
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 312
    new-instance v2, Lcom/google/maps/g/hc;

    invoke-direct {v2, p0}, Lcom/google/maps/g/hc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/he;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/he;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/hc;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/he;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/he;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/he;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hc;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/hc;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 312
    check-cast p1, Lcom/google/maps/g/hc;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/he;->a(Lcom/google/maps/g/hc;)Lcom/google/maps/g/he;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x1

    return v0
.end method
