.class public final Lcom/google/maps/g/ph;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/pi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/pf;",
        "Lcom/google/maps/g/ph;",
        ">;",
        "Lcom/google/maps/g/pi;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/ao;

.field private f:Z

.field private g:Lcom/google/n/f;

.field private h:Z

.field private i:Z

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 541
    sget-object v0, Lcom/google/maps/g/pf;->k:Lcom/google/maps/g/pf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 684
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    .line 821
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    .line 957
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    .line 1016
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/ph;->e:Lcom/google/n/ao;

    .line 1107
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/ph;->g:Lcom/google/n/f;

    .line 1207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    .line 542
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/pf;)Lcom/google/maps/g/ph;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 622
    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 674
    :goto_0
    return-object p0

    .line 623
    :cond_0
    iget-object v2, p1, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 624
    iget-object v2, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 625
    iget-object v2, p1, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    .line 626
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 633
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 634
    iget-object v2, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 635
    iget-object v2, p1, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    .line 636
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 643
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 644
    iget-object v2, p0, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 645
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 647
    :cond_3
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 648
    iget-object v2, p0, Lcom/google/maps/g/ph;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 649
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 651
    :cond_4
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 652
    iget-boolean v2, p1, Lcom/google/maps/g/pf;->f:Z

    iget v3, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/ph;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/ph;->f:Z

    .line 654
    :cond_5
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 655
    iget-object v2, p1, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    if-nez v2, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 628
    :cond_6
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 629
    :cond_7
    iget-object v2, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 638
    :cond_8
    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ph;->a:I

    .line 639
    :cond_9
    iget-object v2, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 643
    goto/16 :goto_3

    :cond_b
    move v2, v1

    .line 647
    goto :goto_4

    :cond_c
    move v2, v1

    .line 651
    goto :goto_5

    :cond_d
    move v2, v1

    .line 654
    goto :goto_6

    .line 655
    :cond_e
    iget v3, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/g/ph;->a:I

    iput-object v2, p0, Lcom/google/maps/g/ph;->g:Lcom/google/n/f;

    .line 657
    :cond_f
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 658
    iget-boolean v2, p1, Lcom/google/maps/g/pf;->h:Z

    iget v3, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/g/ph;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/ph;->h:Z

    .line 660
    :cond_10
    iget v2, p1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    :goto_8
    if-eqz v0, :cond_11

    .line 661
    iget-boolean v0, p1, Lcom/google/maps/g/pf;->i:Z

    iget v1, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/maps/g/ph;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/ph;->i:Z

    .line 663
    :cond_11
    iget-object v0, p1, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 664
    iget-object v0, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 665
    iget-object v0, p1, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    .line 666
    iget v0, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/maps/g/ph;->a:I

    .line 673
    :cond_12
    :goto_9
    iget-object v0, p1, Lcom/google/maps/g/pf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 657
    goto :goto_7

    :cond_14
    move v0, v1

    .line 660
    goto :goto_8

    .line 668
    :cond_15
    iget v0, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_16

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/g/ph;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/g/ph;->a:I

    .line 669
    :cond_16
    iget-object v0, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 533
    new-instance v2, Lcom/google/maps/g/pf;

    invoke-direct {v2, p0}, Lcom/google/maps/g/pf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ph;->a:I

    iget v4, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/g/ph;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/ph;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/g/ph;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/ph;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/pf;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_8

    :goto_0
    iget-object v4, v2, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/ph;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/ph;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-boolean v1, p0, Lcom/google/maps/g/ph;->f:Z

    iput-boolean v1, v2, Lcom/google/maps/g/pf;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v1, p0, Lcom/google/maps/g/ph;->g:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-boolean v1, p0, Lcom/google/maps/g/ph;->h:Z

    iput-boolean v1, v2, Lcom/google/maps/g/pf;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-boolean v1, p0, Lcom/google/maps/g/ph;->i:Z

    iput-boolean v1, v2, Lcom/google/maps/g/pf;->i:Z

    iget v1, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/ph;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/maps/g/ph;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/maps/g/ph;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/pf;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/pf;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 533
    check-cast p1, Lcom/google/maps/g/pf;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ph;->a(Lcom/google/maps/g/pf;)Lcom/google/maps/g/ph;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    return v0
.end method
