.class public final Lcom/google/maps/g/we;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/wh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/we;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/g/we;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/aq;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/google/maps/g/wf;

    invoke-direct {v0}, Lcom/google/maps/g/wf;-><init>()V

    sput-object v0, Lcom/google/maps/g/we;->PARSER:Lcom/google/n/ax;

    .line 321
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/we;->i:Lcom/google/n/aw;

    .line 807
    new-instance v0, Lcom/google/maps/g/we;

    invoke-direct {v0}, Lcom/google/maps/g/we;-><init>()V

    sput-object v0, Lcom/google/maps/g/we;->f:Lcom/google/maps/g/we;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 259
    iput-byte v0, p0, Lcom/google/maps/g/we;->g:B

    .line 287
    iput v0, p0, Lcom/google/maps/g/we;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 28
    invoke-direct {p0}, Lcom/google/maps/g/we;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 36
    sparse-switch v4, :sswitch_data_0

    .line 41
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 49
    iget v5, p0, Lcom/google/maps/g/we;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/maps/g/we;->a:I

    .line 50
    iput-object v4, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 83
    iget-object v1, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 85
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/we;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 55
    iget v5, p0, Lcom/google/maps/g/we;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/maps/g/we;->a:I

    .line 56
    iput-object v4, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 78
    :catch_1
    move-exception v0

    .line 79
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 80
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 61
    and-int/lit8 v5, v1, 0x2

    if-eq v5, v6, :cond_2

    .line 62
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 63
    or-int/lit8 v1, v1, 0x2

    .line 65
    :cond_2
    iget-object v5, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 70
    iget v5, p0, Lcom/google/maps/g/we;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/maps/g/we;->a:I

    .line 71
    iput-object v4, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 82
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_4

    .line 83
    iget-object v0, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 85
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->au:Lcom/google/n/bn;

    .line 86
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 259
    iput-byte v0, p0, Lcom/google/maps/g/we;->g:B

    .line 287
    iput v0, p0, Lcom/google/maps/g/we;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/we;
    .locals 1

    .prologue
    .line 810
    sget-object v0, Lcom/google/maps/g/we;->f:Lcom/google/maps/g/we;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/wg;
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/google/maps/g/wg;

    invoke-direct {v0}, Lcom/google/maps/g/wg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/we;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lcom/google/maps/g/we;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 271
    invoke-virtual {p0}, Lcom/google/maps/g/we;->c()I

    .line 272
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 278
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 279
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 273
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 276
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 281
    :cond_4
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_5

    .line 282
    iget-object v0, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 284
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/we;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 285
    return-void

    .line 282
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 261
    iget-byte v1, p0, Lcom/google/maps/g/we;->g:B

    .line 262
    if-ne v1, v0, :cond_0

    .line 266
    :goto_0
    return v0

    .line 263
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 265
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/we;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 289
    iget v0, p0, Lcom/google/maps/g/we;->h:I

    .line 290
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 316
    :goto_0
    return v0

    .line 293
    :cond_0
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 295
    iget-object v0, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 297
    :goto_2
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 299
    iget-object v0, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v0, v2

    move v3, v2

    .line 303
    :goto_4
    iget-object v4, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 304
    iget-object v4, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 305
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 295
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 299
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 307
    :cond_4
    add-int v0, v1, v3

    .line 308
    iget-object v1, p0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 310
    iget v0, p0, Lcom/google/maps/g/we;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 312
    iget-object v0, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 314
    :goto_6
    iget-object v1, p0, Lcom/google/maps/g/we;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    iput v0, p0, Lcom/google/maps/g/we;->h:I

    goto/16 :goto_0

    .line 312
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_6

    :cond_7
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/we;->newBuilder()Lcom/google/maps/g/wg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/wg;->a(Lcom/google/maps/g/we;)Lcom/google/maps/g/wg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/we;->newBuilder()Lcom/google/maps/g/wg;

    move-result-object v0

    return-object v0
.end method
