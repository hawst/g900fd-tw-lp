.class public final enum Lcom/google/maps/g/pr;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/pr;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/pr;

.field public static final enum b:Lcom/google/maps/g/pr;

.field private static final synthetic d:[Lcom/google/maps/g/pr;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 108
    new-instance v0, Lcom/google/maps/g/pr;

    const-string v1, "ONE_WAY"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/maps/g/pr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pr;->a:Lcom/google/maps/g/pr;

    .line 112
    new-instance v0, Lcom/google/maps/g/pr;

    const-string v1, "TWO_WAY"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/pr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/pr;->b:Lcom/google/maps/g/pr;

    .line 103
    new-array v0, v4, [Lcom/google/maps/g/pr;

    sget-object v1, Lcom/google/maps/g/pr;->a:Lcom/google/maps/g/pr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/pr;->b:Lcom/google/maps/g/pr;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/maps/g/pr;->d:[Lcom/google/maps/g/pr;

    .line 142
    new-instance v0, Lcom/google/maps/g/ps;

    invoke-direct {v0}, Lcom/google/maps/g/ps;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 152
    iput p3, p0, Lcom/google/maps/g/pr;->c:I

    .line 153
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/pr;
    .locals 1

    .prologue
    .line 130
    packed-switch p0, :pswitch_data_0

    .line 133
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 131
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/pr;->a:Lcom/google/maps/g/pr;

    goto :goto_0

    .line 132
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/pr;->b:Lcom/google/maps/g/pr;

    goto :goto_0

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/pr;
    .locals 1

    .prologue
    .line 103
    const-class v0, Lcom/google/maps/g/pr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pr;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/pr;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/maps/g/pr;->d:[Lcom/google/maps/g/pr;

    invoke-virtual {v0}, [Lcom/google/maps/g/pr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/pr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/google/maps/g/pr;->c:I

    return v0
.end method
