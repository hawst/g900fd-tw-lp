.class public final enum Lcom/google/maps/g/jo;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/jo;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/jo;

.field public static final enum b:Lcom/google/maps/g/jo;

.field private static final synthetic d:[Lcom/google/maps/g/jo;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 834
    new-instance v0, Lcom/google/maps/g/jo;

    const-string v1, "OPEN_TODAY"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/maps/g/jo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jo;->a:Lcom/google/maps/g/jo;

    .line 838
    new-instance v0, Lcom/google/maps/g/jo;

    const-string v1, "CLOSED_TODAY"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/g/jo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/jo;->b:Lcom/google/maps/g/jo;

    .line 829
    new-array v0, v4, [Lcom/google/maps/g/jo;

    sget-object v1, Lcom/google/maps/g/jo;->a:Lcom/google/maps/g/jo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/jo;->b:Lcom/google/maps/g/jo;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/maps/g/jo;->d:[Lcom/google/maps/g/jo;

    .line 868
    new-instance v0, Lcom/google/maps/g/jp;

    invoke-direct {v0}, Lcom/google/maps/g/jp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 878
    iput p3, p0, Lcom/google/maps/g/jo;->c:I

    .line 879
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/jo;
    .locals 1

    .prologue
    .line 856
    packed-switch p0, :pswitch_data_0

    .line 859
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 857
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/jo;->a:Lcom/google/maps/g/jo;

    goto :goto_0

    .line 858
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/jo;->b:Lcom/google/maps/g/jo;

    goto :goto_0

    .line 856
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/jo;
    .locals 1

    .prologue
    .line 829
    const-class v0, Lcom/google/maps/g/jo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jo;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/jo;
    .locals 1

    .prologue
    .line 829
    sget-object v0, Lcom/google/maps/g/jo;->d:[Lcom/google/maps/g/jo;

    invoke-virtual {v0}, [Lcom/google/maps/g/jo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/jo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 852
    iget v0, p0, Lcom/google/maps/g/jo;->c:I

    return v0
.end method
