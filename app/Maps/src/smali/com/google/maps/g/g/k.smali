.class public final Lcom/google/maps/g/g/k;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/g/t;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/k;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/px;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/pz;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/g/g/k;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/aq;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/google/maps/g/g/l;

    invoke-direct {v0}, Lcom/google/maps/g/g/l;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/k;->PARSER:Lcom/google/n/ax;

    .line 649
    new-instance v0, Lcom/google/maps/g/g/m;

    invoke-direct {v0}, Lcom/google/maps/g/g/m;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/k;->c:Lcom/google/n/aj;

    .line 696
    new-instance v0, Lcom/google/maps/g/g/n;

    invoke-direct {v0}, Lcom/google/maps/g/g/n;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/k;->f:Lcom/google/n/aj;

    .line 871
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/g/k;->m:Lcom/google/n/aw;

    .line 1439
    new-instance v0, Lcom/google/maps/g/g/k;

    invoke-direct {v0}, Lcom/google/maps/g/g/k;-><init>()V

    sput-object v0, Lcom/google/maps/g/g/k;->j:Lcom/google/maps/g/g/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 741
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    .line 785
    iput-byte v2, p0, Lcom/google/maps/g/g/k;->k:B

    .line 819
    iput v2, p0, Lcom/google/maps/g/g/k;->l:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    .line 19
    iput v3, p0, Lcom/google/maps/g/g/k;->d:I

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    .line 21
    iput v3, p0, Lcom/google/maps/g/g/k;->g:I

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v2, -0x1

    const/16 v9, 0x20

    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v4, 0x1

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/g/k;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v3, :cond_11

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 38
    sparse-switch v1, :sswitch_data_0

    .line 43
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 45
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 51
    invoke-static {v6}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v1

    .line 52
    if-nez v1, :cond_4

    .line 53
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 153
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 154
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 160
    iget-object v2, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    .line 162
    :cond_1
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v8, :cond_2

    .line 163
    iget-object v2, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    .line 165
    :cond_2
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v9, :cond_3

    .line 166
    iget-object v1, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    .line 168
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/k;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_16

    .line 56
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 57
    or-int/lit8 v1, v0, 0x1

    .line 59
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 61
    goto :goto_0

    .line 64
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 65
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v6

    move v1, v0

    .line 66
    :goto_4
    :try_start_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_5

    move v0, v2

    :goto_5
    if-lez v0, :cond_8

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 68
    invoke-static {v0}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v7

    .line 69
    if-nez v7, :cond_6

    .line 70
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_4

    .line 153
    :catch_1
    move-exception v0

    goto :goto_1

    .line 66
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_5

    .line 72
    :cond_6
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_7

    .line 73
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    .line 74
    or-int/lit8 v1, v1, 0x1

    .line 76
    :cond_7
    iget-object v7, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 155
    :catch_2
    move-exception v0

    .line 156
    :goto_6
    :try_start_6
    new-instance v2, Lcom/google/n/ak;

    .line 157
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 79
    :cond_8
    :try_start_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 80
    goto/16 :goto_0

    .line 83
    :sswitch_3
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 84
    invoke-static {v1}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v6

    .line 85
    if-nez v6, :cond_9

    .line 86
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 155
    :catch_3
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_6

    .line 88
    :cond_9
    iget v6, p0, Lcom/google/maps/g/g/k;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/g/g/k;->a:I

    .line 89
    iput v1, p0, Lcom/google/maps/g/g/k;->d:I

    goto/16 :goto_0

    .line 159
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 94
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 95
    invoke-static {v6}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v1

    .line 96
    if-nez v1, :cond_a

    .line 97
    const/4 v1, 0x3

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 99
    :cond_a
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_15

    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 101
    or-int/lit8 v1, v0, 0x4

    .line 103
    :goto_7
    :try_start_9
    iget-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 105
    goto/16 :goto_0

    .line 108
    :sswitch_5
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 109
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v6

    move v1, v0

    .line 110
    :goto_8
    :try_start_b
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_b

    move v0, v2

    :goto_9
    if-lez v0, :cond_e

    .line 111
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 112
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v7

    .line 113
    if-nez v7, :cond_c

    .line 114
    const/4 v7, 0x3

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_8

    .line 110
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_9

    .line 116
    :cond_c
    and-int/lit8 v7, v1, 0x4

    if-eq v7, v8, :cond_d

    .line 117
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    .line 118
    or-int/lit8 v1, v1, 0x4

    .line 120
    :cond_d
    iget-object v7, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 123
    :cond_e
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 124
    goto/16 :goto_0

    .line 127
    :sswitch_6
    :try_start_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 128
    invoke-static {v1}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v6

    .line 129
    if-nez v6, :cond_f

    .line 130
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 132
    :cond_f
    iget v6, p0, Lcom/google/maps/g/g/k;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/g/g/k;->a:I

    .line 133
    iput v1, p0, Lcom/google/maps/g/g/k;->g:I

    goto/16 :goto_0

    .line 138
    :sswitch_7
    iget-object v1, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 139
    iget v1, p0, Lcom/google/maps/g/g/k;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/g/k;->a:I

    goto/16 :goto_0

    .line 143
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 144
    and-int/lit8 v6, v0, 0x20

    if-eq v6, v9, :cond_10

    .line 145
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    .line 146
    or-int/lit8 v0, v0, 0x20

    .line 148
    :cond_10
    iget-object v6, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v6, v1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 159
    :cond_11
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v4, :cond_12

    .line 160
    iget-object v1, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    .line 162
    :cond_12
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v8, :cond_13

    .line 163
    iget-object v1, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    .line 165
    :cond_13
    and-int/lit8 v0, v0, 0x20

    if-ne v0, v9, :cond_14

    .line 166
    iget-object v0, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    .line 168
    :cond_14
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/g/k;->au:Lcom/google/n/bn;

    .line 169
    return-void

    :cond_15
    move v1, v0

    goto/16 :goto_7

    :cond_16
    move v1, v0

    goto/16 :goto_3

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1a -> :sswitch_5
        0x20 -> :sswitch_6
        0x2a -> :sswitch_7
        0x32 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 741
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    .line 785
    iput-byte v1, p0, Lcom/google/maps/g/g/k;->k:B

    .line 819
    iput v1, p0, Lcom/google/maps/g/g/k;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/g/k;
    .locals 1

    .prologue
    .line 1442
    sget-object v0, Lcom/google/maps/g/g/k;->j:Lcom/google/maps/g/g/k;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/g/o;
    .locals 1

    .prologue
    .line 933
    new-instance v0, Lcom/google/maps/g/g/o;

    invoke-direct {v0}, Lcom/google/maps/g/g/o;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/g/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/google/maps/g/g/k;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 797
    invoke-virtual {p0}, Lcom/google/maps/g/g/k;->c()I

    move v1, v2

    .line 798
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 799
    iget-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 798
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 799
    :cond_0
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 801
    :cond_1
    iget v0, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_2

    .line 802
    iget v0, p0, Lcom/google/maps/g/g/k;->d:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_2
    :goto_2
    move v1, v2

    .line 804
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 805
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 804
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 802
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 805
    :cond_4
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 807
    :cond_5
    iget v0, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 808
    iget v0, p0, Lcom/google/maps/g/g/k;->g:I

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 810
    :cond_6
    :goto_5
    iget v0, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_7

    .line 811
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 813
    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 814
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 813
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 808
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 816
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/g/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 817
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 787
    iget-byte v1, p0, Lcom/google/maps/g/g/k;->k:B

    .line 788
    if-ne v1, v0, :cond_0

    .line 792
    :goto_0
    return v0

    .line 789
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 791
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/g/k;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 821
    iget v0, p0, Lcom/google/maps/g/g/k;->l:I

    .line 822
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 866
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 827
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 828
    iget-object v0, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    .line 829
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 829
    goto :goto_2

    .line 831
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 832
    iget-object v1, p0, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 834
    iget v0, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 835
    iget v0, p0, Lcom/google/maps/g/g/k;->d:I

    .line 836
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    :cond_3
    move v3, v2

    move v5, v2

    .line 840
    :goto_4
    iget-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 841
    iget-object v0, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    .line 842
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v5, v0

    .line 840
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_4
    move v0, v4

    .line 836
    goto :goto_3

    :cond_5
    move v0, v4

    .line 842
    goto :goto_5

    .line 844
    :cond_6
    add-int v0, v1, v5

    .line 845
    iget-object v1, p0, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 847
    iget v1, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_8

    .line 848
    iget v1, p0, Lcom/google/maps/g/g/k;->g:I

    .line 849
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v1, :cond_7

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_7
    add-int v1, v3, v4

    add-int/2addr v0, v1

    .line 851
    :cond_8
    iget v1, p0, Lcom/google/maps/g/g/k;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_9

    .line 852
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    .line 853
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_9
    move v1, v2

    .line 857
    :goto_6
    iget-object v3, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 858
    iget-object v3, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    .line 859
    invoke-interface {v3, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 857
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 861
    :cond_a
    add-int/2addr v0, v1

    .line 862
    iget-object v1, p0, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 864
    iget-object v1, p0, Lcom/google/maps/g/g/k;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 865
    iput v0, p0, Lcom/google/maps/g/g/k;->l:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/g/k;->newBuilder()Lcom/google/maps/g/g/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/g/o;->a(Lcom/google/maps/g/g/k;)Lcom/google/maps/g/g/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/g/k;->newBuilder()Lcom/google/maps/g/g/o;

    move-result-object v0

    return-object v0
.end method
