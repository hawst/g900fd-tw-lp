.class public final Lcom/google/maps/g/ks;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/kt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/kq;",
        "Lcom/google/maps/g/ks;",
        ">;",
        "Lcom/google/maps/g/kt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/google/maps/g/kq;->g:Lcom/google/maps/g/kq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 541
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ks;->b:Ljava/lang/Object;

    .line 618
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    .line 786
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ks;->e:Ljava/lang/Object;

    .line 862
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ks;->f:Ljava/lang/Object;

    .line 446
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 496
    invoke-static {}, Lcom/google/maps/g/kq;->d()Lcom/google/maps/g/kq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 526
    :goto_0
    return-object p0

    .line 497
    :cond_0
    iget v2, p1, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 498
    iget v2, p0, Lcom/google/maps/g/ks;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/ks;->a:I

    .line 499
    iget-object v2, p1, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ks;->b:Ljava/lang/Object;

    .line 502
    :cond_1
    iget-object v2, p1, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 503
    iget-object v2, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 504
    iget-object v2, p1, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    .line 505
    iget v2, p0, Lcom/google/maps/g/ks;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/g/ks;->a:I

    .line 512
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 513
    iget v2, p1, Lcom/google/maps/g/kq;->d:I

    iget v3, p0, Lcom/google/maps/g/ks;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/ks;->a:I

    iput v2, p0, Lcom/google/maps/g/ks;->d:I

    .line 515
    :cond_3
    iget v2, p1, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 516
    iget v2, p0, Lcom/google/maps/g/ks;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/ks;->a:I

    .line 517
    iget-object v2, p1, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/ks;->e:Ljava/lang/Object;

    .line 520
    :cond_4
    iget v2, p1, Lcom/google/maps/g/kq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 521
    iget v0, p0, Lcom/google/maps/g/ks;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/ks;->a:I

    .line 522
    iget-object v0, p1, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ks;->f:Ljava/lang/Object;

    .line 525
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/kq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 497
    goto :goto_1

    .line 507
    :cond_7
    iget v2, p0, Lcom/google/maps/g/ks;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/ks;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/ks;->a:I

    .line 508
    :cond_8
    iget-object v2, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 512
    goto :goto_3

    :cond_a
    move v2, v1

    .line 515
    goto :goto_4

    :cond_b
    move v0, v1

    .line 520
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/google/maps/g/ks;->c()Lcom/google/maps/g/kq;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 437
    check-cast p1, Lcom/google/maps/g/kq;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ks;->a(Lcom/google/maps/g/kq;)Lcom/google/maps/g/ks;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 530
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 531
    iget-object v0, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/km;->d()Lcom/google/maps/g/km;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/km;

    invoke-virtual {v0}, Lcom/google/maps/g/km;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 536
    :goto_1
    return v2

    .line 530
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 536
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/maps/g/kq;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 467
    new-instance v2, Lcom/google/maps/g/kq;

    invoke-direct {v2, p0}, Lcom/google/maps/g/kq;-><init>(Lcom/google/n/v;)V

    .line 468
    iget v3, p0, Lcom/google/maps/g/ks;->a:I

    .line 469
    const/4 v1, 0x0

    .line 470
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 473
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/ks;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/kq;->b:Ljava/lang/Object;

    .line 474
    iget v1, p0, Lcom/google/maps/g/ks;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 475
    iget-object v1, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    .line 476
    iget v1, p0, Lcom/google/maps/g/ks;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/ks;->a:I

    .line 478
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/ks;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/kq;->c:Ljava/util/List;

    .line 479
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 480
    or-int/lit8 v0, v0, 0x2

    .line 482
    :cond_1
    iget v1, p0, Lcom/google/maps/g/ks;->d:I

    iput v1, v2, Lcom/google/maps/g/kq;->d:I

    .line 483
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 484
    or-int/lit8 v0, v0, 0x4

    .line 486
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/ks;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/kq;->e:Ljava/lang/Object;

    .line 487
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 488
    or-int/lit8 v0, v0, 0x8

    .line 490
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/ks;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/kq;->f:Ljava/lang/Object;

    .line 491
    iput v0, v2, Lcom/google/maps/g/kq;->a:I

    .line 492
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
