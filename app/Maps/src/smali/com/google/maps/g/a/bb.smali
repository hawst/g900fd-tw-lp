.class public final enum Lcom/google/maps/g/a/bb;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/bb;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/bb;

.field public static final enum b:Lcom/google/maps/g/a/bb;

.field public static final enum c:Lcom/google/maps/g/a/bb;

.field public static final enum d:Lcom/google/maps/g/a/bb;

.field private static final synthetic f:[Lcom/google/maps/g/a/bb;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-instance v0, Lcom/google/maps/g/a/bb;

    const-string v1, "TRAFFIC_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/bb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/bb;->a:Lcom/google/maps/g/a/bb;

    .line 147
    new-instance v0, Lcom/google/maps/g/a/bb;

    const-string v1, "TRAFFIC_LIGHT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/bb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/bb;->b:Lcom/google/maps/g/a/bb;

    .line 151
    new-instance v0, Lcom/google/maps/g/a/bb;

    const-string v1, "TRAFFIC_MEDIUM"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/bb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/bb;->c:Lcom/google/maps/g/a/bb;

    .line 155
    new-instance v0, Lcom/google/maps/g/a/bb;

    const-string v1, "TRAFFIC_HEAVY"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/bb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/bb;->d:Lcom/google/maps/g/a/bb;

    .line 138
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/g/a/bb;

    sget-object v1, Lcom/google/maps/g/a/bb;->a:Lcom/google/maps/g/a/bb;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/bb;->b:Lcom/google/maps/g/a/bb;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/bb;->c:Lcom/google/maps/g/a/bb;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/bb;->d:Lcom/google/maps/g/a/bb;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/g/a/bb;->f:[Lcom/google/maps/g/a/bb;

    .line 195
    new-instance v0, Lcom/google/maps/g/a/bc;

    invoke-direct {v0}, Lcom/google/maps/g/a/bc;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 205
    iput p3, p0, Lcom/google/maps/g/a/bb;->e:I

    .line 206
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/bb;
    .locals 1

    .prologue
    .line 181
    packed-switch p0, :pswitch_data_0

    .line 186
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 182
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/bb;->a:Lcom/google/maps/g/a/bb;

    goto :goto_0

    .line 183
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/bb;->b:Lcom/google/maps/g/a/bb;

    goto :goto_0

    .line 184
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/bb;->c:Lcom/google/maps/g/a/bb;

    goto :goto_0

    .line 185
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/bb;->d:Lcom/google/maps/g/a/bb;

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/bb;
    .locals 1

    .prologue
    .line 138
    const-class v0, Lcom/google/maps/g/a/bb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/bb;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/bb;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/google/maps/g/a/bb;->f:[Lcom/google/maps/g/a/bb;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/bb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/bb;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/maps/g/a/bb;->e:I

    return v0
.end method
