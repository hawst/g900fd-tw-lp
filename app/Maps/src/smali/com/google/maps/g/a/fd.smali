.class public final enum Lcom/google/maps/g/a/fd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/fd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/fd;

.field public static final enum b:Lcom/google/maps/g/a/fd;

.field public static final enum c:Lcom/google/maps/g/a/fd;

.field public static final enum d:Lcom/google/maps/g/a/fd;

.field public static final enum e:Lcom/google/maps/g/a/fd;

.field public static final enum f:Lcom/google/maps/g/a/fd;

.field public static final enum g:Lcom/google/maps/g/a/fd;

.field public static final enum h:Lcom/google/maps/g/a/fd;

.field public static final enum i:Lcom/google/maps/g/a/fd;

.field private static final synthetic k:[Lcom/google/maps/g/a/fd;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 541
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    .line 545
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_SLIGHT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->b:Lcom/google/maps/g/a/fd;

    .line 549
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_NORMAL"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->c:Lcom/google/maps/g/a/fd;

    .line 553
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_SHARP"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->d:Lcom/google/maps/g/a/fd;

    .line 557
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_KEEP"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->e:Lcom/google/maps/g/a/fd;

    .line 561
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_UTURN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->f:Lcom/google/maps/g/a/fd;

    .line 565
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_STRAIGHT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->g:Lcom/google/maps/g/a/fd;

    .line 569
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_MERGE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->h:Lcom/google/maps/g/a/fd;

    .line 573
    new-instance v0, Lcom/google/maps/g/a/fd;

    const-string v1, "TURN_FORK"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/fd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/fd;->i:Lcom/google/maps/g/a/fd;

    .line 536
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/maps/g/a/fd;

    sget-object v1, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/fd;->b:Lcom/google/maps/g/a/fd;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/g/a/fd;->c:Lcom/google/maps/g/a/fd;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/g/a/fd;->d:Lcom/google/maps/g/a/fd;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/g/a/fd;->e:Lcom/google/maps/g/a/fd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/g/a/fd;->f:Lcom/google/maps/g/a/fd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/g/a/fd;->g:Lcom/google/maps/g/a/fd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/g/a/fd;->h:Lcom/google/maps/g/a/fd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/g/a/fd;->i:Lcom/google/maps/g/a/fd;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/g/a/fd;->k:[Lcom/google/maps/g/a/fd;

    .line 638
    new-instance v0, Lcom/google/maps/g/a/fe;

    invoke-direct {v0}, Lcom/google/maps/g/a/fe;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 647
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 648
    iput p3, p0, Lcom/google/maps/g/a/fd;->j:I

    .line 649
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/fd;
    .locals 1

    .prologue
    .line 619
    packed-switch p0, :pswitch_data_0

    .line 629
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 620
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 621
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/fd;->b:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 622
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/fd;->c:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 623
    :pswitch_3
    sget-object v0, Lcom/google/maps/g/a/fd;->d:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 624
    :pswitch_4
    sget-object v0, Lcom/google/maps/g/a/fd;->e:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 625
    :pswitch_5
    sget-object v0, Lcom/google/maps/g/a/fd;->f:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 626
    :pswitch_6
    sget-object v0, Lcom/google/maps/g/a/fd;->g:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 627
    :pswitch_7
    sget-object v0, Lcom/google/maps/g/a/fd;->h:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 628
    :pswitch_8
    sget-object v0, Lcom/google/maps/g/a/fd;->i:Lcom/google/maps/g/a/fd;

    goto :goto_0

    .line 619
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/fd;
    .locals 1

    .prologue
    .line 536
    const-class v0, Lcom/google/maps/g/a/fd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fd;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/fd;
    .locals 1

    .prologue
    .line 536
    sget-object v0, Lcom/google/maps/g/a/fd;->k:[Lcom/google/maps/g/a/fd;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/fd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/fd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lcom/google/maps/g/a/fd;->j:I

    return v0
.end method
