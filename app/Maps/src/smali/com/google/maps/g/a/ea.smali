.class public final Lcom/google/maps/g/a/ea;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ed;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ea;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/maps/g/a/ea;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/n/f;

.field public e:Lcom/google/n/f;

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/maps/g/a/eb;

    invoke-direct {v0}, Lcom/google/maps/g/a/eb;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ea;->PARSER:Lcom/google/n/ax;

    .line 359
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/a/ea;->k:Lcom/google/n/aw;

    .line 1030
    new-instance v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v0}, Lcom/google/maps/g/a/ea;-><init>()V

    sput-object v0, Lcom/google/maps/g/a/ea;->h:Lcom/google/maps/g/a/ea;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    .line 212
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    .line 270
    iput-byte v2, p0, Lcom/google/maps/g/a/ea;->i:B

    .line 322
    iput v2, p0, Lcom/google/maps/g/a/ea;->j:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    .line 20
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    .line 21
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    .line 22
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x20

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/maps/g/a/ea;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 38
    sparse-switch v1, :sswitch_data_0

    .line 43
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 45
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 51
    iget v1, p0, Lcom/google/maps/g/a/ea;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/g/a/ea;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 91
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 92
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 98
    iget-object v2, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    .line 100
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v8, :cond_2

    .line 101
    iget-object v1, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    .line 103
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_7

    .line 56
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 58
    or-int/lit8 v1, v0, 0x2

    .line 60
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 61
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 60
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/maps/g/a/ea;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/a/ea;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 93
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 94
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 95
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 70
    :sswitch_4
    :try_start_6
    iget v1, p0, Lcom/google/maps/g/a/ea;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/ea;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    goto/16 :goto_0

    .line 97
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_2

    .line 75
    :sswitch_5
    iget-object v1, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 76
    iget v1, p0, Lcom/google/maps/g/a/ea;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/a/ea;->a:I

    goto/16 :goto_0

    .line 80
    :sswitch_6
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v8, :cond_3

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    .line 83
    or-int/lit8 v0, v0, 0x20

    .line 85
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 85
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 97
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_5

    .line 98
    iget-object v1, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    .line 100
    :cond_5
    and-int/lit8 v0, v0, 0x20

    if-ne v0, v8, :cond_6

    .line 101
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    .line 103
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->au:Lcom/google/n/bn;

    .line 104
    return-void

    .line 93
    :catch_2
    move-exception v0

    goto :goto_4

    .line 91
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto/16 :goto_3

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    .line 212
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    .line 270
    iput-byte v1, p0, Lcom/google/maps/g/a/ea;->i:B

    .line 322
    iput v1, p0, Lcom/google/maps/g/a/ea;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/a/ea;
    .locals 1

    .prologue
    .line 1033
    sget-object v0, Lcom/google/maps/g/a/ea;->h:Lcom/google/maps/g/a/ea;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/a/ec;
    .locals 1

    .prologue
    .line 421
    new-instance v0, Lcom/google/maps/g/a/ec;

    invoke-direct {v0}, Lcom/google/maps/g/a/ec;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/a/ea;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/google/maps/g/a/ea;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 300
    invoke-virtual {p0}, Lcom/google/maps/g/a/ea;->c()I

    .line 301
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 304
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 307
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 308
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 310
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 311
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 313
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 314
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 316
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 317
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 316
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 319
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 320
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272
    iget-byte v0, p0, Lcom/google/maps/g/a/ea;->i:B

    .line 273
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 295
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    if-eqz v0, :cond_0

    .line 276
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 277
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 278
    iput-byte v2, p0, Lcom/google/maps/g/a/ea;->i:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 276
    goto :goto_1

    :cond_3
    move v1, v2

    .line 282
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 283
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ff;->d()Lcom/google/maps/g/a/ff;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ff;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ff;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 284
    iput-byte v2, p0, Lcom/google/maps/g/a/ea;->i:B

    goto :goto_0

    .line 282
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 288
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 289
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/s;->d()Lcom/google/maps/g/a/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/s;

    invoke-virtual {v0}, Lcom/google/maps/g/a/s;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 290
    iput-byte v2, p0, Lcom/google/maps/g/a/ea;->i:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 288
    goto :goto_3

    .line 294
    :cond_7
    iput-byte v3, p0, Lcom/google/maps/g/a/ea;->i:B

    move v2, v3

    .line 295
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 324
    iget v0, p0, Lcom/google/maps/g/a/ea;->j:I

    .line 325
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 354
    :goto_0
    return v0

    .line 328
    :cond_0
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 329
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->b:Lcom/google/n/ao;

    .line 330
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 332
    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    .line 334
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 332
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 336
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 337
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    .line 338
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 340
    :cond_2
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 341
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    .line 342
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 345
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/maps/g/a/ea;->f:Lcom/google/n/ao;

    .line 346
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_4
    move v2, v1

    .line 348
    :goto_3
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 349
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/maps/g/a/ea;->g:Ljava/util/List;

    .line 350
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 348
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 352
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/a/ea;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 353
    iput v0, p0, Lcom/google/maps/g/a/ea;->j:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ea;->newBuilder()Lcom/google/maps/g/a/ec;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/a/ec;->a(Lcom/google/maps/g/a/ea;)Lcom/google/maps/g/a/ec;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/a/ea;->newBuilder()Lcom/google/maps/g/a/ec;

    move-result-object v0

    return-object v0
.end method
