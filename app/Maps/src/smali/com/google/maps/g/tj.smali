.class public final Lcom/google/maps/g/tj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/to;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tj;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/tj;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:F

.field d:I

.field e:I

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/maps/g/tk;

    invoke-direct {v0}, Lcom/google/maps/g/tk;-><init>()V

    sput-object v0, Lcom/google/maps/g/tj;->PARSER:Lcom/google/n/ax;

    .line 323
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/tj;->j:Lcom/google/n/aw;

    .line 677
    new-instance v0, Lcom/google/maps/g/tj;

    invoke-direct {v0}, Lcom/google/maps/g/tj;-><init>()V

    sput-object v0, Lcom/google/maps/g/tj;->g:Lcom/google/maps/g/tj;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    .line 259
    iput-byte v2, p0, Lcom/google/maps/g/tj;->h:B

    .line 290
    iput v2, p0, Lcom/google/maps/g/tj;->i:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/maps/g/tj;->c:F

    .line 20
    iput v3, p0, Lcom/google/maps/g/tj;->d:I

    .line 21
    iput v3, p0, Lcom/google/maps/g/tj;->e:I

    .line 22
    const/16 v0, 0x58

    iput v0, p0, Lcom/google/maps/g/tj;->f:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/g/tj;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 50
    iget v3, p0, Lcom/google/maps/g/tj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/tj;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/tj;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 55
    invoke-static {v3}, Lcom/google/maps/g/tm;->a(I)Lcom/google/maps/g/tm;

    move-result-object v4

    .line 56
    if-nez v4, :cond_1

    .line 57
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/maps/g/tj;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/g/tj;->a:I

    .line 60
    iput v3, p0, Lcom/google/maps/g/tj;->e:I

    goto :goto_0

    .line 65
    :sswitch_3
    iget v3, p0, Lcom/google/maps/g/tj;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/g/tj;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/tj;->f:I

    goto :goto_0

    .line 70
    :sswitch_4
    iget v3, p0, Lcom/google/maps/g/tj;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/g/tj;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/g/tj;->d:I

    goto :goto_0

    .line 75
    :sswitch_5
    iget v3, p0, Lcom/google/maps/g/tj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/tj;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/maps/g/tj;->c:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/tj;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2d -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    .line 259
    iput-byte v1, p0, Lcom/google/maps/g/tj;->h:B

    .line 290
    iput v1, p0, Lcom/google/maps/g/tj;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/g/tj;
    .locals 1

    .prologue
    .line 680
    sget-object v0, Lcom/google/maps/g/tj;->g:Lcom/google/maps/g/tj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/tl;
    .locals 1

    .prologue
    .line 385
    new-instance v0, Lcom/google/maps/g/tl;

    invoke-direct {v0}, Lcom/google/maps/g/tl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/tj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/maps/g/tj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 271
    invoke-virtual {p0}, Lcom/google/maps/g/tj;->c()I

    .line 272
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 276
    iget v0, p0, Lcom/google/maps/g/tj;->e:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 278
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 279
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/g/tj;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 281
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 282
    iget v0, p0, Lcom/google/maps/g/tj;->d:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 284
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    .line 285
    iget v0, p0, Lcom/google/maps/g/tj;->c:F

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 287
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/tj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 288
    return-void

    .line 276
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 279
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 282
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 261
    iget-byte v1, p0, Lcom/google/maps/g/tj;->h:B

    .line 262
    if-ne v1, v0, :cond_0

    .line 266
    :goto_0
    return v0

    .line 263
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 265
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/tj;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 292
    iget v0, p0, Lcom/google/maps/g/tj;->i:I

    .line 293
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 318
    :goto_0
    return v0

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 297
    iget-object v0, p0, Lcom/google/maps/g/tj;->b:Lcom/google/n/ao;

    .line 298
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 300
    :goto_1
    iget v2, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 301
    iget v2, p0, Lcom/google/maps/g/tj;->e:I

    .line 302
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 304
    :cond_1
    iget v2, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_2

    .line 305
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/g/tj;->f:I

    .line 306
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 308
    :cond_2
    iget v2, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_4

    .line 309
    iget v2, p0, Lcom/google/maps/g/tj;->d:I

    .line 310
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int v2, v4, v3

    add-int/2addr v0, v2

    .line 312
    :cond_4
    iget v2, p0, Lcom/google/maps/g/tj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_5

    .line 313
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/maps/g/tj;->c:F

    .line 314
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 316
    :cond_5
    iget-object v1, p0, Lcom/google/maps/g/tj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    iput v0, p0, Lcom/google/maps/g/tj;->i:I

    goto/16 :goto_0

    :cond_6
    move v2, v3

    .line 302
    goto :goto_2

    :cond_7
    move v2, v3

    .line 306
    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/tj;->newBuilder()Lcom/google/maps/g/tl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/tl;->a(Lcom/google/maps/g/tj;)Lcom/google/maps/g/tl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/g/tj;->newBuilder()Lcom/google/maps/g/tl;

    move-result-object v0

    return-object v0
.end method
