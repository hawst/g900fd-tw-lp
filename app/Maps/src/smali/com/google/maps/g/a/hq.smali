.class public final Lcom/google/maps/g/a/hq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/ht;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/ho;",
        "Lcom/google/maps/g/a/hq;",
        ">;",
        "Lcom/google/maps/g/a/ht;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 344
    sget-object v0, Lcom/google/maps/g/a/ho;->e:Lcom/google/maps/g/a/ho;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 402
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/maps/g/a/hq;->b:I

    .line 438
    iput v1, p0, Lcom/google/maps/g/a/hq;->c:I

    .line 474
    iput-boolean v1, p0, Lcom/google/maps/g/a/hq;->d:Z

    .line 345
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/ho;)Lcom/google/maps/g/a/hq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 382
    invoke-static {}, Lcom/google/maps/g/a/ho;->d()Lcom/google/maps/g/a/ho;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 393
    :goto_0
    return-object p0

    .line 383
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 384
    iget v2, p1, Lcom/google/maps/g/a/ho;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 383
    goto :goto_1

    .line 384
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/hq;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, p0, Lcom/google/maps/g/a/hq;->b:I

    .line 386
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 387
    iget v2, p1, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 386
    goto :goto_2

    .line 387
    :cond_7
    iget v3, p0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/g/a/hq;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hr;->d:I

    iput v2, p0, Lcom/google/maps/g/a/hq;->c:I

    .line 389
    :cond_8
    iget v2, p1, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    :goto_3
    if-eqz v0, :cond_9

    .line 390
    iget-boolean v0, p1, Lcom/google/maps/g/a/ho;->d:Z

    iget v1, p0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/a/hq;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/hq;->d:Z

    .line 392
    :cond_9
    iget-object v0, p1, Lcom/google/maps/g/a/ho;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_a
    move v0, v1

    .line 389
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 336
    new-instance v2, Lcom/google/maps/g/a/ho;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/ho;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/hq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/hq;->b:I

    iput v1, v2, Lcom/google/maps/g/a/ho;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/g/a/hq;->c:I

    iput v1, v2, Lcom/google/maps/g/a/ho;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/maps/g/a/hq;->d:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/ho;->d:Z

    iput v0, v2, Lcom/google/maps/g/a/ho;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 336
    check-cast p1, Lcom/google/maps/g/a/ho;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/hq;->a(Lcom/google/maps/g/a/ho;)Lcom/google/maps/g/a/hq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x1

    return v0
.end method
