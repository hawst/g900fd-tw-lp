.class public final Lcom/google/maps/g/a/co;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/a/cr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/a/cm;",
        "Lcom/google/maps/g/a/co;",
        ">;",
        "Lcom/google/maps/g/a/cr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/google/maps/g/a/cm;->d:Lcom/google/maps/g/a/cm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 385
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/g/a/co;->b:I

    .line 337
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/cm;)Lcom/google/maps/g/a/co;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 368
    invoke-static {}, Lcom/google/maps/g/a/cm;->d()Lcom/google/maps/g/a/cm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 376
    :goto_0
    return-object p0

    .line 369
    :cond_0
    iget v2, p1, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 370
    iget v2, p1, Lcom/google/maps/g/a/cm;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/cp;->a(I)Lcom/google/maps/g/a/cp;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/cp;->a:Lcom/google/maps/g/a/cp;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 369
    goto :goto_1

    .line 370
    :cond_3
    iget v3, p0, Lcom/google/maps/g/a/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/a/co;->a:I

    iget v2, v2, Lcom/google/maps/g/a/cp;->g:I

    iput v2, p0, Lcom/google/maps/g/a/co;->b:I

    .line 372
    :cond_4
    iget v2, p1, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 373
    iget-boolean v0, p1, Lcom/google/maps/g/a/cm;->c:Z

    iget v1, p0, Lcom/google/maps/g/a/co;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/a/co;->a:I

    iput-boolean v0, p0, Lcom/google/maps/g/a/co;->c:Z

    .line 375
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/cm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 372
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 328
    new-instance v2, Lcom/google/maps/g/a/cm;

    invoke-direct {v2, p0}, Lcom/google/maps/g/a/cm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/a/co;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/a/co;->b:I

    iput v1, v2, Lcom/google/maps/g/a/cm;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/maps/g/a/co;->c:Z

    iput-boolean v1, v2, Lcom/google/maps/g/a/cm;->c:Z

    iput v0, v2, Lcom/google/maps/g/a/cm;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 328
    check-cast p1, Lcom/google/maps/g/a/cm;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/a/co;->a(Lcom/google/maps/g/a/cm;)Lcom/google/maps/g/a/co;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x1

    return v0
.end method
