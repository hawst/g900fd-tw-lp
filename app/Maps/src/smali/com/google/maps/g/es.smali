.class public final Lcom/google/maps/g/es;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/et;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/eq;",
        "Lcom/google/maps/g/es;",
        ">;",
        "Lcom/google/maps/g/et;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/google/maps/g/eq;->d:Lcom/google/maps/g/eq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/es;->b:Lcom/google/n/ao;

    .line 392
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    .line 273
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 394
    iget v0, p0, Lcom/google/maps/g/es;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 395
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    .line 398
    iget v0, p0, Lcom/google/maps/g/es;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/es;->a:I

    .line 400
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/eq;)Lcom/google/maps/g/es;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 307
    invoke-static {}, Lcom/google/maps/g/eq;->d()Lcom/google/maps/g/eq;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 323
    :goto_0
    return-object p0

    .line 308
    :cond_0
    iget v1, p1, Lcom/google/maps/g/eq;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/google/maps/g/es;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 310
    iget v0, p0, Lcom/google/maps/g/es;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/es;->a:I

    .line 312
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 314
    iget-object v0, p1, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    .line 315
    iget v0, p0, Lcom/google/maps/g/es;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/g/es;->a:I

    .line 322
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/eq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 308
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 317
    :cond_4
    invoke-direct {p0}, Lcom/google/maps/g/es;->c()V

    .line 318
    iget-object v0, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/maps/g/es;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/maps/g/bq;",
            ">;)",
            "Lcom/google/maps/g/es;"
        }
    .end annotation

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/google/maps/g/es;->c()V

    .line 503
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 504
    iget-object v2, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 507
    :cond_0
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 264
    new-instance v2, Lcom/google/maps/g/eq;

    invoke-direct {v2, p0}, Lcom/google/maps/g/eq;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/es;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/maps/g/eq;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/g/es;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/g/es;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/g/es;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/g/es;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/g/es;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/es;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/g/eq;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/g/eq;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 264
    check-cast p1, Lcom/google/maps/g/eq;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/es;->a(Lcom/google/maps/g/eq;)Lcom/google/maps/g/es;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method
