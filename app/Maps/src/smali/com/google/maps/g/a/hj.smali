.class public final enum Lcom/google/maps/g/a/hj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/hj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/hj;

.field public static final enum b:Lcom/google/maps/g/a/hj;

.field public static final enum c:Lcom/google/maps/g/a/hj;

.field private static final synthetic e:[Lcom/google/maps/g/a/hj;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 198
    new-instance v0, Lcom/google/maps/g/a/hj;

    const-string v1, "ACCESSIBILITY_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/a/hj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hj;->a:Lcom/google/maps/g/a/hj;

    .line 202
    new-instance v0, Lcom/google/maps/g/a/hj;

    const-string v1, "NOT_ACCESSIBLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/a/hj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hj;->b:Lcom/google/maps/g/a/hj;

    .line 206
    new-instance v0, Lcom/google/maps/g/a/hj;

    const-string v1, "FULLY_ACCESSIBLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/a/hj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/hj;->c:Lcom/google/maps/g/a/hj;

    .line 193
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/a/hj;

    sget-object v1, Lcom/google/maps/g/a/hj;->a:Lcom/google/maps/g/a/hj;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/hj;->b:Lcom/google/maps/g/a/hj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/a/hj;->c:Lcom/google/maps/g/a/hj;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/a/hj;->e:[Lcom/google/maps/g/a/hj;

    .line 241
    new-instance v0, Lcom/google/maps/g/a/hk;

    invoke-direct {v0}, Lcom/google/maps/g/a/hk;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 250
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 251
    iput p3, p0, Lcom/google/maps/g/a/hj;->d:I

    .line 252
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/hj;
    .locals 1

    .prologue
    .line 228
    packed-switch p0, :pswitch_data_0

    .line 232
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 229
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/hj;->a:Lcom/google/maps/g/a/hj;

    goto :goto_0

    .line 230
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/hj;->b:Lcom/google/maps/g/a/hj;

    goto :goto_0

    .line 231
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/hj;->c:Lcom/google/maps/g/a/hj;

    goto :goto_0

    .line 228
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/hj;
    .locals 1

    .prologue
    .line 193
    const-class v0, Lcom/google/maps/g/a/hj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hj;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/hj;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/google/maps/g/a/hj;->e:[Lcom/google/maps/g/a/hj;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/hj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/hj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/google/maps/g/a/hj;->d:I

    return v0
.end method
