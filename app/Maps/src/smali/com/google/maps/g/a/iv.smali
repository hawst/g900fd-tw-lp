.class public final enum Lcom/google/maps/g/a/iv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/iv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/iv;

.field public static final enum b:Lcom/google/maps/g/a/iv;

.field public static final enum c:Lcom/google/maps/g/a/iv;

.field private static final synthetic e:[Lcom/google/maps/g/a/iv;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 95
    new-instance v0, Lcom/google/maps/g/a/iv;

    const-string v1, "REFRESH_WITH_RECONSTRUCTED_ROUTE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/a/iv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iv;->a:Lcom/google/maps/g/a/iv;

    .line 99
    new-instance v0, Lcom/google/maps/g/a/iv;

    const-string v1, "KEEP_CLIENT_ROUTE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/iv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iv;->b:Lcom/google/maps/g/a/iv;

    .line 103
    new-instance v0, Lcom/google/maps/g/a/iv;

    const-string v1, "BETTER_ROUTE_PROMPT"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/g/a/iv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/iv;->c:Lcom/google/maps/g/a/iv;

    .line 90
    new-array v0, v5, [Lcom/google/maps/g/a/iv;

    sget-object v1, Lcom/google/maps/g/a/iv;->a:Lcom/google/maps/g/a/iv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/iv;->b:Lcom/google/maps/g/a/iv;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/iv;->c:Lcom/google/maps/g/a/iv;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/a/iv;->e:[Lcom/google/maps/g/a/iv;

    .line 138
    new-instance v0, Lcom/google/maps/g/a/iw;

    invoke-direct {v0}, Lcom/google/maps/g/a/iw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 148
    iput p3, p0, Lcom/google/maps/g/a/iv;->d:I

    .line 149
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/iv;
    .locals 1

    .prologue
    .line 125
    packed-switch p0, :pswitch_data_0

    .line 129
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 126
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/iv;->a:Lcom/google/maps/g/a/iv;

    goto :goto_0

    .line 127
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/iv;->b:Lcom/google/maps/g/a/iv;

    goto :goto_0

    .line 128
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/iv;->c:Lcom/google/maps/g/a/iv;

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/iv;
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/google/maps/g/a/iv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/iv;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/iv;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/maps/g/a/iv;->e:[Lcom/google/maps/g/a/iv;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/iv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/iv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/maps/g/a/iv;->d:I

    return v0
.end method
