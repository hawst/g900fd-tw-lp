.class public final Lcom/google/maps/g/ri;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/rj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/rg;",
        "Lcom/google/maps/g/ri;",
        ">;",
        "Lcom/google/maps/g/rj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4523
    sget-object v0, Lcom/google/maps/g/rg;->d:Lcom/google/maps/g/rg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4606
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/ri;->c:Ljava/lang/Object;

    .line 4524
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/rg;)Lcom/google/maps/g/ri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4555
    invoke-static {}, Lcom/google/maps/g/rg;->g()Lcom/google/maps/g/rg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4565
    :goto_0
    return-object p0

    .line 4556
    :cond_0
    iget v2, p1, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4557
    iget v2, p1, Lcom/google/maps/g/rg;->b:I

    iget v3, p0, Lcom/google/maps/g/ri;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/g/ri;->a:I

    iput v2, p0, Lcom/google/maps/g/ri;->b:I

    .line 4559
    :cond_1
    iget v2, p1, Lcom/google/maps/g/rg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 4560
    iget v0, p0, Lcom/google/maps/g/ri;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ri;->a:I

    .line 4561
    iget-object v0, p1, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/ri;->c:Ljava/lang/Object;

    .line 4564
    :cond_2
    iget-object v0, p1, Lcom/google/maps/g/rg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 4556
    goto :goto_1

    :cond_4
    move v0, v1

    .line 4559
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4515
    new-instance v2, Lcom/google/maps/g/rg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/rg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/ri;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/g/ri;->b:I

    iput v1, v2, Lcom/google/maps/g/rg;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/ri;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/rg;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/rg;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4515
    check-cast p1, Lcom/google/maps/g/rg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/ri;->a(Lcom/google/maps/g/rg;)Lcom/google/maps/g/ri;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4569
    const/4 v0, 0x1

    return v0
.end method
