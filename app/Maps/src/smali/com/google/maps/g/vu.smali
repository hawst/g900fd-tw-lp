.class public final enum Lcom/google/maps/g/vu;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/vu;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/vu;

.field public static final enum b:Lcom/google/maps/g/vu;

.field public static final enum c:Lcom/google/maps/g/vu;

.field private static final synthetic e:[Lcom/google/maps/g/vu;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    new-instance v0, Lcom/google/maps/g/vu;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/g/vu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/vu;->a:Lcom/google/maps/g/vu;

    .line 179
    new-instance v0, Lcom/google/maps/g/vu;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/g/vu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/vu;->b:Lcom/google/maps/g/vu;

    .line 183
    new-instance v0, Lcom/google/maps/g/vu;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/g/vu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/vu;->c:Lcom/google/maps/g/vu;

    .line 170
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/maps/g/vu;

    sget-object v1, Lcom/google/maps/g/vu;->a:Lcom/google/maps/g/vu;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/vu;->b:Lcom/google/maps/g/vu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/g/vu;->c:Lcom/google/maps/g/vu;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/maps/g/vu;->e:[Lcom/google/maps/g/vu;

    .line 218
    new-instance v0, Lcom/google/maps/g/vv;

    invoke-direct {v0}, Lcom/google/maps/g/vv;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 227
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 228
    iput p3, p0, Lcom/google/maps/g/vu;->d:I

    .line 229
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/vu;
    .locals 1

    .prologue
    .line 205
    packed-switch p0, :pswitch_data_0

    .line 209
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 206
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/vu;->a:Lcom/google/maps/g/vu;

    goto :goto_0

    .line 207
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/vu;->b:Lcom/google/maps/g/vu;

    goto :goto_0

    .line 208
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/vu;->c:Lcom/google/maps/g/vu;

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/vu;
    .locals 1

    .prologue
    .line 170
    const-class v0, Lcom/google/maps/g/vu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vu;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/vu;
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/google/maps/g/vu;->e:[Lcom/google/maps/g/vu;

    invoke-virtual {v0}, [Lcom/google/maps/g/vu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/vu;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/google/maps/g/vu;->d:I

    return v0
.end method
