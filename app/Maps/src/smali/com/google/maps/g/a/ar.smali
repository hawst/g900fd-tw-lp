.class public final enum Lcom/google/maps/g/a/ar;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/g/a/ar;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/g/a/ar;

.field public static final enum b:Lcom/google/maps/g/a/ar;

.field public static final enum c:Lcom/google/maps/g/a/ar;

.field private static final synthetic e:[Lcom/google/maps/g/a/ar;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 131
    new-instance v0, Lcom/google/maps/g/a/ar;

    const-string v1, "IGNORE_TRAFFIC"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/g/a/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ar;->a:Lcom/google/maps/g/a/ar;

    .line 135
    new-instance v0, Lcom/google/maps/g/a/ar;

    const-string v1, "RANK_USING_TRAFFIC"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/g/a/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ar;->b:Lcom/google/maps/g/a/ar;

    .line 139
    new-instance v0, Lcom/google/maps/g/a/ar;

    const-string v1, "DYNAMIC_ROUTE_AROUND_TRAFFIC"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/g/a/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/g/a/ar;->c:Lcom/google/maps/g/a/ar;

    .line 126
    new-array v0, v5, [Lcom/google/maps/g/a/ar;

    sget-object v1, Lcom/google/maps/g/a/ar;->a:Lcom/google/maps/g/a/ar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/g/a/ar;->b:Lcom/google/maps/g/a/ar;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/g/a/ar;->c:Lcom/google/maps/g/a/ar;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/g/a/ar;->e:[Lcom/google/maps/g/a/ar;

    .line 174
    new-instance v0, Lcom/google/maps/g/a/as;

    invoke-direct {v0}, Lcom/google/maps/g/a/as;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/google/maps/g/a/ar;->d:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/google/maps/g/a/ar;
    .locals 1

    .prologue
    .line 161
    packed-switch p0, :pswitch_data_0

    .line 165
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/ar;->a:Lcom/google/maps/g/a/ar;

    goto :goto_0

    .line 163
    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/ar;->b:Lcom/google/maps/g/a/ar;

    goto :goto_0

    .line 164
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/ar;->c:Lcom/google/maps/g/a/ar;

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/g/a/ar;
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/maps/g/a/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ar;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/g/a/ar;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/maps/g/a/ar;->e:[Lcom/google/maps/g/a/ar;

    invoke-virtual {v0}, [Lcom/google/maps/g/a/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/ar;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/maps/g/a/ar;->d:I

    return v0
.end method
