.class public final Lcom/google/maps/g/df;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/di;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/df;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/g/df;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/maps/g/ux;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Lcom/google/maps/g/a/i;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    new-instance v0, Lcom/google/maps/g/dg;

    invoke-direct {v0}, Lcom/google/maps/g/dg;-><init>()V

    sput-object v0, Lcom/google/maps/g/df;->PARSER:Lcom/google/n/ax;

    .line 484
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/df;->j:Lcom/google/n/aw;

    .line 1006
    new-instance v0, Lcom/google/maps/g/df;

    invoke-direct {v0}, Lcom/google/maps/g/df;-><init>()V

    sput-object v0, Lcom/google/maps/g/df;->g:Lcom/google/maps/g/df;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 158
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 414
    iput-byte v0, p0, Lcom/google/maps/g/df;->h:B

    .line 451
    iput v0, p0, Lcom/google/maps/g/df;->i:I

    .line 159
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    .line 160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    .line 161
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    .line 162
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 168
    invoke-direct {p0}, Lcom/google/maps/g/df;-><init>()V

    .line 169
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 173
    const/4 v0, 0x0

    move v3, v0

    .line 174
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 175
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 176
    sparse-switch v0, :sswitch_data_0

    .line 181
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 183
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 179
    goto :goto_0

    .line 189
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 190
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    invoke-static {v0}, Lcom/google/maps/g/ux;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    move-result-object v0

    move-object v1, v0

    .line 192
    :goto_1
    sget-object v0, Lcom/google/maps/g/ux;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ux;

    iput-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    .line 193
    if-eqz v1, :cond_1

    .line 194
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/uz;->a(Lcom/google/maps/g/ux;)Lcom/google/maps/g/uz;

    .line 195
    invoke-virtual {v1}, Lcom/google/maps/g/uz;->c()Lcom/google/maps/g/ux;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    .line 197
    :cond_1
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/df;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/df;->au:Lcom/google/n/bn;

    throw v0

    .line 201
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 202
    iget v1, p0, Lcom/google/maps/g/df;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/g/df;->a:I

    .line 203
    iput-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 235
    :catch_1
    move-exception v0

    .line 236
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 237
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 207
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 208
    iget v1, p0, Lcom/google/maps/g/df;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/g/df;->a:I

    .line 209
    iput-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    goto :goto_0

    .line 213
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 214
    iget v1, p0, Lcom/google/maps/g/df;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/g/df;->a:I

    .line 215
    iput-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    goto :goto_0

    .line 220
    :sswitch_5
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 221
    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    invoke-static {v0}, Lcom/google/maps/g/a/i;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    move-result-object v0

    move-object v1, v0

    .line 223
    :goto_2
    sget-object v0, Lcom/google/maps/g/a/i;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    iput-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    .line 224
    if-eqz v1, :cond_2

    .line 225
    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/k;->a(Lcom/google/maps/g/a/i;)Lcom/google/maps/g/a/k;

    .line 226
    invoke-virtual {v1}, Lcom/google/maps/g/a/k;->c()Lcom/google/maps/g/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    .line 228
    :cond_2
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/df;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 239
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->au:Lcom/google/n/bn;

    .line 240
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto/16 :goto_1

    .line 176
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 156
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 414
    iput-byte v0, p0, Lcom/google/maps/g/df;->h:B

    .line 451
    iput v0, p0, Lcom/google/maps/g/df;->i:I

    .line 157
    return-void
.end method

.method public static d()Lcom/google/maps/g/df;
    .locals 1

    .prologue
    .line 1009
    sget-object v0, Lcom/google/maps/g/df;->g:Lcom/google/maps/g/df;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/dh;
    .locals 1

    .prologue
    .line 546
    new-instance v0, Lcom/google/maps/g/dh;

    invoke-direct {v0}, Lcom/google/maps/g/dh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/df;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    sget-object v0, Lcom/google/maps/g/df;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 432
    invoke-virtual {p0}, Lcom/google/maps/g/df;->c()I

    .line 433
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v0

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 436
    :cond_0
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 437
    iget-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 439
    :cond_1
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 440
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 442
    :cond_2
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 443
    iget-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 445
    :cond_3
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 446
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v0

    :goto_4
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 448
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/df;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 449
    return-void

    .line 434
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    goto/16 :goto_0

    .line 437
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 440
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 443
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 446
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    goto :goto_4
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 416
    iget-byte v0, p0, Lcom/google/maps/g/df;->h:B

    .line 417
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 427
    :goto_0
    return v0

    .line 418
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 420
    :cond_1
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 421
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/ux;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 422
    iput-byte v2, p0, Lcom/google/maps/g/df;->h:B

    move v0, v2

    .line 423
    goto :goto_0

    :cond_2
    move v0, v2

    .line 420
    goto :goto_1

    .line 421
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    goto :goto_2

    .line 426
    :cond_4
    iput-byte v1, p0, Lcom/google/maps/g/df;->h:B

    move v0, v1

    .line 427
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 453
    iget v0, p0, Lcom/google/maps/g/df;->i:I

    .line 454
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 479
    :goto_0
    return v0

    .line 457
    :cond_0
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 459
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/ux;->d()Lcom/google/maps/g/ux;

    move-result-object v0

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 461
    :goto_2
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 463
    iget-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 465
    :cond_1
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 466
    const/4 v3, 0x3

    .line 467
    iget-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 469
    :cond_2
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 471
    iget-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/df;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 473
    :cond_3
    iget v0, p0, Lcom/google/maps/g/df;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 474
    const/4 v3, 0x5

    .line 475
    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v0

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 477
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/df;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 478
    iput v0, p0, Lcom/google/maps/g/df;->i:I

    goto/16 :goto_0

    .line 459
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/df;->b:Lcom/google/maps/g/ux;

    goto/16 :goto_1

    .line 463
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 467
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 471
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 475
    :cond_9
    iget-object v0, p0, Lcom/google/maps/g/df;->f:Lcom/google/maps/g/a/i;

    goto :goto_6

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/google/maps/g/df;->newBuilder()Lcom/google/maps/g/dh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/dh;->a(Lcom/google/maps/g/df;)Lcom/google/maps/g/dh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/google/maps/g/df;->newBuilder()Lcom/google/maps/g/dh;

    move-result-object v0

    return-object v0
.end method
