.class public final Lcom/google/maps/g/ro;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/sd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ro;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/g/ro;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/maps/g/rr;

.field c:Lcom/google/maps/g/rz;

.field d:Lcom/google/maps/g/rv;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2140
    new-instance v0, Lcom/google/maps/g/rp;

    invoke-direct {v0}, Lcom/google/maps/g/rp;-><init>()V

    sput-object v0, Lcom/google/maps/g/ro;->PARSER:Lcom/google/n/ax;

    .line 2905
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/g/ro;->h:Lcom/google/n/aw;

    .line 3230
    new-instance v0, Lcom/google/maps/g/ro;

    invoke-direct {v0}, Lcom/google/maps/g/ro;-><init>()V

    sput-object v0, Lcom/google/maps/g/ro;->e:Lcom/google/maps/g/ro;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2064
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2855
    iput-byte v0, p0, Lcom/google/maps/g/ro;->f:B

    .line 2880
    iput v0, p0, Lcom/google/maps/g/ro;->g:I

    .line 2065
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 2071
    invoke-direct {p0}, Lcom/google/maps/g/ro;-><init>()V

    .line 2072
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 2076
    const/4 v0, 0x0

    move v3, v0

    .line 2077
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 2078
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 2079
    sparse-switch v0, :sswitch_data_0

    .line 2084
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 2086
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2082
    goto :goto_0

    .line 2092
    :sswitch_1
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 2093
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    invoke-static {v0}, Lcom/google/maps/g/rr;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    move-result-object v0

    move-object v1, v0

    .line 2095
    :goto_1
    sget-object v0, Lcom/google/maps/g/rr;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rr;

    iput-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    .line 2096
    if-eqz v1, :cond_1

    .line 2097
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/rt;->a(Lcom/google/maps/g/rr;)Lcom/google/maps/g/rt;

    .line 2098
    new-instance v0, Lcom/google/maps/g/rr;

    invoke-direct {v0, v1}, Lcom/google/maps/g/rr;-><init>(Lcom/google/n/v;)V

    iput-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    .line 2100
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/g/ro;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2131
    :catch_0
    move-exception v0

    .line 2132
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2137
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/g/ro;->au:Lcom/google/n/bn;

    throw v0

    .line 2105
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 2106
    iget-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    invoke-static {v0}, Lcom/google/maps/g/rz;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    move-result-object v0

    move-object v1, v0

    .line 2108
    :goto_2
    sget-object v0, Lcom/google/maps/g/rz;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rz;

    iput-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    .line 2109
    if-eqz v1, :cond_2

    .line 2110
    iget-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/sb;->a(Lcom/google/maps/g/rz;)Lcom/google/maps/g/sb;

    .line 2111
    new-instance v0, Lcom/google/maps/g/rz;

    invoke-direct {v0, v1}, Lcom/google/maps/g/rz;-><init>(Lcom/google/n/v;)V

    iput-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    .line 2113
    :cond_2
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/g/ro;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2133
    :catch_1
    move-exception v0

    .line 2134
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2135
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2118
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 2119
    iget-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    invoke-static {v0}, Lcom/google/maps/g/rv;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    move-result-object v0

    move-object v1, v0

    .line 2121
    :goto_3
    sget-object v0, Lcom/google/maps/g/rv;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rv;

    iput-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    .line 2122
    if-eqz v1, :cond_3

    .line 2123
    iget-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/rx;->a(Lcom/google/maps/g/rv;)Lcom/google/maps/g/rx;

    .line 2124
    new-instance v0, Lcom/google/maps/g/rv;

    invoke-direct {v0, v1}, Lcom/google/maps/g/rv;-><init>(Lcom/google/n/v;)V

    iput-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    .line 2126
    :cond_3
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/g/ro;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2137
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/ro;->au:Lcom/google/n/bn;

    .line 2138
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_1

    .line 2079
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2062
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2855
    iput-byte v0, p0, Lcom/google/maps/g/ro;->f:B

    .line 2880
    iput v0, p0, Lcom/google/maps/g/ro;->g:I

    .line 2063
    return-void
.end method

.method public static d()Lcom/google/maps/g/ro;
    .locals 1

    .prologue
    .line 3233
    sget-object v0, Lcom/google/maps/g/ro;->e:Lcom/google/maps/g/ro;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/g/rq;
    .locals 1

    .prologue
    .line 2967
    new-instance v0, Lcom/google/maps/g/rq;

    invoke-direct {v0}, Lcom/google/maps/g/rq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/g/ro;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2152
    sget-object v0, Lcom/google/maps/g/ro;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 2867
    invoke-virtual {p0}, Lcom/google/maps/g/ro;->c()I

    .line 2868
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2869
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/rr;->d()Lcom/google/maps/g/rr;

    move-result-object v0

    :goto_0
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2871
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2872
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/rz;->d()Lcom/google/maps/g/rz;

    move-result-object v0

    :goto_1
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2874
    :cond_1
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2875
    iget-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/rv;->d()Lcom/google/maps/g/rv;

    move-result-object v0

    :goto_2
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2877
    :cond_2
    iget-object v0, p0, Lcom/google/maps/g/ro;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2878
    return-void

    .line 2869
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    goto :goto_0

    .line 2872
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    goto :goto_1

    .line 2875
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2857
    iget-byte v1, p0, Lcom/google/maps/g/ro;->f:B

    .line 2858
    if-ne v1, v0, :cond_0

    .line 2862
    :goto_0
    return v0

    .line 2859
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2861
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/g/ro;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 2882
    iget v0, p0, Lcom/google/maps/g/ro;->g:I

    .line 2883
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2900
    :goto_0
    return v0

    .line 2886
    :cond_0
    iget v0, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 2888
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/rr;->d()Lcom/google/maps/g/rr;

    move-result-object v0

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2890
    :goto_2
    iget v2, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2891
    const/4 v3, 0x3

    .line 2892
    iget-object v2, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/maps/g/rz;->d()Lcom/google/maps/g/rz;

    move-result-object v2

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2894
    :cond_1
    iget v2, p0, Lcom/google/maps/g/ro;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2896
    iget-object v2, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    if-nez v2, :cond_5

    invoke-static {}, Lcom/google/maps/g/rv;->d()Lcom/google/maps/g/rv;

    move-result-object v2

    :goto_4
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2898
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/ro;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2899
    iput v0, p0, Lcom/google/maps/g/ro;->g:I

    goto :goto_0

    .line 2888
    :cond_3
    iget-object v0, p0, Lcom/google/maps/g/ro;->b:Lcom/google/maps/g/rr;

    goto :goto_1

    .line 2892
    :cond_4
    iget-object v2, p0, Lcom/google/maps/g/ro;->c:Lcom/google/maps/g/rz;

    goto :goto_3

    .line 2896
    :cond_5
    iget-object v2, p0, Lcom/google/maps/g/ro;->d:Lcom/google/maps/g/rv;

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2056
    invoke-static {}, Lcom/google/maps/g/ro;->newBuilder()Lcom/google/maps/g/rq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/g/rq;->a(Lcom/google/maps/g/ro;)Lcom/google/maps/g/rq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2056
    invoke-static {}, Lcom/google/maps/g/ro;->newBuilder()Lcom/google/maps/g/rq;

    move-result-object v0

    return-object v0
.end method
