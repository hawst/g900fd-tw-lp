.class public final Lcom/google/maps/g/hi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/hj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/hg;",
        "Lcom/google/maps/g/hi;",
        ">;",
        "Lcom/google/maps/g/hj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lcom/google/maps/g/hg;->g:Lcom/google/maps/g/hg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 545
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hi;->e:Ljava/lang/Object;

    .line 621
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hi;->b:Ljava/lang/Object;

    .line 697
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hi;->f:Ljava/lang/Object;

    .line 773
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hi;->c:Ljava/lang/Object;

    .line 849
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/hi;->d:Ljava/lang/Object;

    .line 460
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 509
    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 536
    :goto_0
    return-object p0

    .line 510
    :cond_0
    iget v2, p1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 511
    iget v2, p0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/hi;->a:I

    .line 512
    iget-object v2, p1, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/hi;->e:Ljava/lang/Object;

    .line 515
    :cond_1
    iget v2, p1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 516
    iget v2, p0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/hi;->a:I

    .line 517
    iget-object v2, p1, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/hi;->b:Ljava/lang/Object;

    .line 520
    :cond_2
    iget v2, p1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 521
    iget v2, p0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/hi;->a:I

    .line 522
    iget-object v2, p1, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/hi;->f:Ljava/lang/Object;

    .line 525
    :cond_3
    iget v2, p1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 526
    iget v2, p0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/hi;->a:I

    .line 527
    iget-object v2, p1, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/hi;->c:Ljava/lang/Object;

    .line 530
    :cond_4
    iget v2, p1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 531
    iget v0, p0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/g/hi;->a:I

    .line 532
    iget-object v0, p1, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/hi;->d:Ljava/lang/Object;

    .line 535
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/hg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 510
    goto :goto_1

    :cond_7
    move v2, v1

    .line 515
    goto :goto_2

    :cond_8
    move v2, v1

    .line 520
    goto :goto_3

    :cond_9
    move v2, v1

    .line 525
    goto :goto_4

    :cond_a
    move v0, v1

    .line 530
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/google/maps/g/hi;->c()Lcom/google/maps/g/hg;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 451
    check-cast p1, Lcom/google/maps/g/hg;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/hi;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 540
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/maps/g/hg;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 481
    new-instance v2, Lcom/google/maps/g/hg;

    invoke-direct {v2, p0}, Lcom/google/maps/g/hg;-><init>(Lcom/google/n/v;)V

    .line 482
    iget v3, p0, Lcom/google/maps/g/hi;->a:I

    .line 483
    const/4 v1, 0x0

    .line 484
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 487
    :goto_0
    iget-object v1, p0, Lcom/google/maps/g/hi;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hg;->b:Ljava/lang/Object;

    .line 488
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 489
    or-int/lit8 v0, v0, 0x2

    .line 491
    :cond_0
    iget-object v1, p0, Lcom/google/maps/g/hi;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hg;->c:Ljava/lang/Object;

    .line 492
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 493
    or-int/lit8 v0, v0, 0x4

    .line 495
    :cond_1
    iget-object v1, p0, Lcom/google/maps/g/hi;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hg;->d:Ljava/lang/Object;

    .line 496
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 497
    or-int/lit8 v0, v0, 0x8

    .line 499
    :cond_2
    iget-object v1, p0, Lcom/google/maps/g/hi;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hg;->e:Ljava/lang/Object;

    .line 500
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 501
    or-int/lit8 v0, v0, 0x10

    .line 503
    :cond_3
    iget-object v1, p0, Lcom/google/maps/g/hi;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/hg;->f:Ljava/lang/Object;

    .line 504
    iput v0, v2, Lcom/google/maps/g/hg;->a:I

    .line 505
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
