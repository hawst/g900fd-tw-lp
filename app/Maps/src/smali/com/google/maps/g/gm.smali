.class public final Lcom/google/maps/g/gm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/gt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/gk;",
        "Lcom/google/maps/g/gm;",
        ">;",
        "Lcom/google/maps/g/gt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:Ljava/lang/Object;

.field private l:Ljava/lang/Object;

.field private m:Ljava/lang/Object;

.field private n:Ljava/lang/Object;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/google/n/ao;

.field private q:Ljava/lang/Object;

.field private r:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1057
    sget-object v0, Lcom/google/maps/g/gk;->s:Lcom/google/maps/g/gk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1281
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->b:Ljava/lang/Object;

    .line 1357
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->c:Ljava/lang/Object;

    .line 1433
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->d:Ljava/lang/Object;

    .line 1509
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gm;->e:Lcom/google/n/ao;

    .line 1568
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->f:Ljava/lang/Object;

    .line 1644
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->g:Ljava/lang/Object;

    .line 1720
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->h:Ljava/lang/Object;

    .line 1796
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->i:Ljava/lang/Object;

    .line 1904
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->k:Ljava/lang/Object;

    .line 1980
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->l:Ljava/lang/Object;

    .line 2056
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->m:Ljava/lang/Object;

    .line 2132
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->n:Ljava/lang/Object;

    .line 2209
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    .line 2345
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/gm;->p:Lcom/google/n/ao;

    .line 2404
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->q:Ljava/lang/Object;

    .line 2480
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/g/gm;->r:Ljava/lang/Object;

    .line 1058
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/gk;)Lcom/google/maps/g/gm;
    .locals 6

    .prologue
    const/16 v5, 0x2000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1184
    invoke-static {}, Lcom/google/maps/g/gk;->d()Lcom/google/maps/g/gk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1272
    :goto_0
    return-object p0

    .line 1185
    :cond_0
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_12

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1186
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1187
    iget-object v2, p1, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->b:Ljava/lang/Object;

    .line 1190
    :cond_1
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1191
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1192
    iget-object v2, p1, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->c:Ljava/lang/Object;

    .line 1195
    :cond_2
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1196
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1197
    iget-object v2, p1, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->d:Ljava/lang/Object;

    .line 1200
    :cond_3
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1201
    iget-object v2, p0, Lcom/google/maps/g/gm;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1202
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1204
    :cond_4
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1205
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1206
    iget-object v2, p1, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->f:Ljava/lang/Object;

    .line 1209
    :cond_5
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1210
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1211
    iget-object v2, p1, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->g:Ljava/lang/Object;

    .line 1214
    :cond_6
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1215
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1216
    iget-object v2, p1, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->h:Ljava/lang/Object;

    .line 1219
    :cond_7
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1220
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1221
    iget-object v2, p1, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->i:Ljava/lang/Object;

    .line 1224
    :cond_8
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1225
    iget-boolean v2, p1, Lcom/google/maps/g/gk;->j:Z

    iget v3, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/g/gm;->a:I

    iput-boolean v2, p0, Lcom/google/maps/g/gm;->j:Z

    .line 1227
    :cond_9
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1228
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1229
    iget-object v2, p1, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->k:Ljava/lang/Object;

    .line 1232
    :cond_a
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1233
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1234
    iget-object v2, p1, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->l:Ljava/lang/Object;

    .line 1237
    :cond_b
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1238
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1239
    iget-object v2, p1, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->m:Ljava/lang/Object;

    .line 1242
    :cond_c
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 1243
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1244
    iget-object v2, p1, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->n:Ljava/lang/Object;

    .line 1247
    :cond_d
    iget-object v2, p1, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1248
    iget-object v2, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1249
    iget-object v2, p1, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    .line 1250
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    and-int/lit16 v2, v2, -0x2001

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1257
    :cond_e
    :goto_e
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x2000

    if-ne v2, v5, :cond_21

    move v2, v0

    :goto_f
    if-eqz v2, :cond_f

    .line 1258
    iget-object v2, p0, Lcom/google/maps/g/gm;->p:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1259
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1261
    :cond_f
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 1262
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1263
    iget-object v2, p1, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/g/gm;->q:Ljava/lang/Object;

    .line 1266
    :cond_10
    iget v2, p1, Lcom/google/maps/g/gk;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_23

    :goto_11
    if-eqz v0, :cond_11

    .line 1267
    iget v0, p0, Lcom/google/maps/g/gm;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/g/gm;->a:I

    .line 1268
    iget-object v0, p1, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/g/gm;->r:Ljava/lang/Object;

    .line 1271
    :cond_11
    iget-object v0, p1, Lcom/google/maps/g/gk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v2, v1

    .line 1185
    goto/16 :goto_1

    :cond_13
    move v2, v1

    .line 1190
    goto/16 :goto_2

    :cond_14
    move v2, v1

    .line 1195
    goto/16 :goto_3

    :cond_15
    move v2, v1

    .line 1200
    goto/16 :goto_4

    :cond_16
    move v2, v1

    .line 1204
    goto/16 :goto_5

    :cond_17
    move v2, v1

    .line 1209
    goto/16 :goto_6

    :cond_18
    move v2, v1

    .line 1214
    goto/16 :goto_7

    :cond_19
    move v2, v1

    .line 1219
    goto/16 :goto_8

    :cond_1a
    move v2, v1

    .line 1224
    goto/16 :goto_9

    :cond_1b
    move v2, v1

    .line 1227
    goto/16 :goto_a

    :cond_1c
    move v2, v1

    .line 1232
    goto/16 :goto_b

    :cond_1d
    move v2, v1

    .line 1237
    goto/16 :goto_c

    :cond_1e
    move v2, v1

    .line 1242
    goto/16 :goto_d

    .line 1252
    :cond_1f
    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    and-int/lit16 v2, v2, 0x2000

    if-eq v2, v5, :cond_20

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/g/gm;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/g/gm;->a:I

    .line 1253
    :cond_20
    iget-object v2, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_e

    :cond_21
    move v2, v1

    .line 1257
    goto/16 :goto_f

    :cond_22
    move v2, v1

    .line 1261
    goto :goto_10

    :cond_23
    move v0, v1

    .line 1266
    goto :goto_11
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 9

    .prologue
    const/high16 v8, 0x10000

    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1049
    new-instance v2, Lcom/google/maps/g/gk;

    invoke-direct {v2, p0}, Lcom/google/maps/g/gk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/g/gm;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_10

    :goto_0
    iget-object v4, p0, Lcom/google/maps/g/gm;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/g/gm;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/maps/g/gm;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/g/gk;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/gm;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/gm;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/maps/g/gm;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/maps/g/gm;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/maps/g/gm;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, p0, Lcom/google/maps/g/gm;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->i:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v4, p0, Lcom/google/maps/g/gm;->j:Z

    iput-boolean v4, v2, Lcom/google/maps/g/gk;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, p0, Lcom/google/maps/g/gm;->k:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->k:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, p0, Lcom/google/maps/g/gm;->l:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->l:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v4, p0, Lcom/google/maps/g/gm;->m:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->m:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-object v4, p0, Lcom/google/maps/g/gm;->n:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/g/gk;->n:Ljava/lang/Object;

    iget v4, p0, Lcom/google/maps/g/gm;->a:I

    and-int/lit16 v4, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/g/gm;->a:I

    and-int/lit16 v4, v4, -0x2001

    iput v4, p0, Lcom/google/maps/g/gm;->a:I

    :cond_c
    iget-object v4, p0, Lcom/google/maps/g/gm;->o:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/g/gk;->o:Ljava/util/List;

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x2000

    :cond_d
    iget-object v4, v2, Lcom/google/maps/g/gk;->p:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/g/gm;->p:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/g/gm;->p:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/lit16 v0, v0, 0x4000

    :cond_e
    iget-object v1, p0, Lcom/google/maps/g/gm;->q:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/gk;->q:Ljava/lang/Object;

    and-int v1, v3, v8

    if-ne v1, v8, :cond_f

    or-int/2addr v0, v7

    :cond_f
    iget-object v1, p0, Lcom/google/maps/g/gm;->r:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/g/gk;->r:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/g/gk;->a:I

    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1049
    check-cast p1, Lcom/google/maps/g/gk;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/gm;->a(Lcom/google/maps/g/gk;)Lcom/google/maps/g/gm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1276
    const/4 v0, 0x1

    return v0
.end method
