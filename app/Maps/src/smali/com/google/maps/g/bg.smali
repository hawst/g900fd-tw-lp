.class public final Lcom/google/maps/g/bg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/g/bh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/g/ba;",
        "Lcom/google/maps/g/bg;",
        ">;",
        "Lcom/google/maps/g/bh;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:I

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 419
    sget-object v0, Lcom/google/maps/g/ba;->e:Lcom/google/maps/g/ba;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 510
    iput v1, p0, Lcom/google/maps/g/bg;->a:I

    .line 742
    iput v1, p0, Lcom/google/maps/g/bg;->d:I

    .line 420
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/ba;)Lcom/google/maps/g/bg;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 466
    invoke-static {}, Lcom/google/maps/g/ba;->d()Lcom/google/maps/g/ba;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 503
    :goto_0
    return-object p0

    .line 467
    :cond_0
    iget v0, p1, Lcom/google/maps/g/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 468
    iget v0, p1, Lcom/google/maps/g/ba;->d:I

    invoke-static {v0}, Lcom/google/maps/g/bd;->a(I)Lcom/google/maps/g/bd;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/bd;->a:Lcom/google/maps/g/bd;

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/maps/g/bg;->a(Lcom/google/maps/g/bd;)Lcom/google/maps/g/bg;

    .line 470
    :cond_2
    sget-object v0, Lcom/google/maps/g/bc;->a:[I

    iget v1, p1, Lcom/google/maps/g/ba;->b:I

    invoke-static {v1}, Lcom/google/maps/g/bf;->a(I)Lcom/google/maps/g/bf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/bf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 496
    :goto_2
    iget-object v0, p1, Lcom/google/maps/g/ba;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 467
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 472
    :pswitch_0
    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    if-eq v0, v2, :cond_4

    .line 473
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    .line 475
    :cond_4
    iget-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 476
    iget-object v1, p1, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 475
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 477
    iput v2, p0, Lcom/google/maps/g/bg;->a:I

    goto :goto_2

    .line 481
    :pswitch_1
    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    if-eq v0, v3, :cond_5

    .line 482
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    .line 484
    :cond_5
    iget-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 485
    iget-object v1, p1, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 484
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 486
    iput v3, p0, Lcom/google/maps/g/bg;->a:I

    goto :goto_2

    .line 490
    :pswitch_2
    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    if-eq v0, v4, :cond_6

    .line 491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    .line 493
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 494
    iget-object v1, p1, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 493
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 495
    iput v4, p0, Lcom/google/maps/g/bg;->a:I

    goto :goto_2

    .line 470
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/maps/g/bd;)Lcom/google/maps/g/bg;
    .locals 1

    .prologue
    .line 760
    if-nez p1, :cond_0

    .line 761
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 763
    :cond_0
    iget v0, p0, Lcom/google/maps/g/bg;->c:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/g/bg;->c:I

    .line 764
    iget v0, p1, Lcom/google/maps/g/bd;->d:I

    iput v0, p0, Lcom/google/maps/g/bg;->d:I

    .line 766
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 411
    new-instance v4, Lcom/google/maps/g/ba;

    invoke-direct {v4, p0}, Lcom/google/maps/g/ba;-><init>(Lcom/google/n/v;)V

    iget v5, p0, Lcom/google/maps/g/bg;->c:I

    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_1
    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/maps/g/ba;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_2
    and-int/lit8 v0, v5, 0x8

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_0
    iget v1, p0, Lcom/google/maps/g/bg;->d:I

    iput v1, v4, Lcom/google/maps/g/ba;->d:I

    iput v0, v4, Lcom/google/maps/g/ba;->a:I

    iget v0, p0, Lcom/google/maps/g/bg;->a:I

    iput v0, v4, Lcom/google/maps/g/ba;->b:I

    return-object v4

    :cond_3
    move v0, v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 411
    check-cast p1, Lcom/google/maps/g/ba;

    invoke-virtual {p0, p1}, Lcom/google/maps/g/bg;->a(Lcom/google/maps/g/ba;)Lcom/google/maps/g/bg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 507
    const/4 v0, 0x1

    return v0
.end method
