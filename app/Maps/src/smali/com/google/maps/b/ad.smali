.class public final enum Lcom/google/maps/b/ad;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/ad;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/ad;

.field public static final enum b:Lcom/google/maps/b/ad;

.field public static final enum c:Lcom/google/maps/b/ad;

.field public static final enum d:Lcom/google/maps/b/ad;

.field public static final enum e:Lcom/google/maps/b/ad;

.field public static final enum f:Lcom/google/maps/b/ad;

.field public static final enum g:Lcom/google/maps/b/ad;

.field private static final synthetic i:[Lcom/google/maps/b/ad;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_MAP"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->a:Lcom/google/maps/b/ad;

    .line 163
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_SATELLITE_IMAGERY"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->b:Lcom/google/maps/b/ad;

    .line 167
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_TERRAIN"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->c:Lcom/google/maps/b/ad;

    .line 171
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_PLAIN_TERRAIN"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->d:Lcom/google/maps/b/ad;

    .line 175
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_TERRAIN_SHADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->e:Lcom/google/maps/b/ad;

    .line 179
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_TERRAIN_CONTOURS"

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->f:Lcom/google/maps/b/ad;

    .line 183
    new-instance v0, Lcom/google/maps/b/ad;

    const-string v1, "TYPE_AUX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v6}, Lcom/google/maps/b/ad;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ad;->g:Lcom/google/maps/b/ad;

    .line 154
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/b/ad;

    sget-object v1, Lcom/google/maps/b/ad;->a:Lcom/google/maps/b/ad;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/ad;->b:Lcom/google/maps/b/ad;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/ad;->c:Lcom/google/maps/b/ad;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/ad;->d:Lcom/google/maps/b/ad;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/ad;->e:Lcom/google/maps/b/ad;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/ad;->f:Lcom/google/maps/b/ad;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/ad;->g:Lcom/google/maps/b/ad;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/ad;->i:[Lcom/google/maps/b/ad;

    .line 238
    new-instance v0, Lcom/google/maps/b/ae;

    invoke-direct {v0}, Lcom/google/maps/b/ae;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 247
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 248
    iput p3, p0, Lcom/google/maps/b/ad;->h:I

    .line 249
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/ad;
    .locals 1

    .prologue
    .line 221
    packed-switch p0, :pswitch_data_0

    .line 229
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 222
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/ad;->a:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 223
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/ad;->b:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 224
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/ad;->c:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 225
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/ad;->d:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 226
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/ad;->e:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 227
    :pswitch_5
    sget-object v0, Lcom/google/maps/b/ad;->f:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 228
    :pswitch_6
    sget-object v0, Lcom/google/maps/b/ad;->g:Lcom/google/maps/b/ad;

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/ad;
    .locals 1

    .prologue
    .line 154
    const-class v0, Lcom/google/maps/b/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ad;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/ad;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/google/maps/b/ad;->i:[Lcom/google/maps/b/ad;

    invoke-virtual {v0}, [Lcom/google/maps/b/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/ad;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/google/maps/b/ad;->h:I

    return v0
.end method
