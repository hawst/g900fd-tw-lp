.class public Lcom/google/maps/b/a/cj;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cy;

.field public d:Lcom/google/maps/b/a/cz;

.field private e:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cj;->e:Lcom/google/maps/b/a/cz;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method protected final a(II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 76
    packed-switch p1, :pswitch_data_0

    .line 94
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 78
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 81
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 84
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    invoke-virtual {v1, p2}, Lcom/google/maps/b/a/cy;->a(I)V

    goto :goto_0

    .line 87
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/cj;->e:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 90
    :pswitch_5
    iget-object v1, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    ushr-int/lit8 v2, p2, 0x1

    and-int/lit8 v3, p2, 0x1

    neg-int v3, v3

    xor-int/2addr v2, v3

    iput v2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 66
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 67
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 68
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 69
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->e:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 70
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v2, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "color_argb: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "width: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget-object v2, v2, Lcom/google/maps/b/a/cy;->a:[I

    aget v2, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "dash: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->e:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->e:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "bevel_width: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_4

    .line 113
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "orthogonal_offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
