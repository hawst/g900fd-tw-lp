.class public final Lcom/google/maps/b/n;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/q;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/n;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/maps/b/n;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field f:I

.field public g:I

.field public h:Lcom/google/n/f;

.field i:I

.field public j:I

.field k:I

.field l:I

.field public m:I

.field public n:I

.field o:Ljava/lang/Object;

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667
    new-instance v0, Lcom/google/maps/b/o;

    invoke-direct {v0}, Lcom/google/maps/b/o;-><init>()V

    sput-object v0, Lcom/google/maps/b/n;->PARSER:Lcom/google/n/ax;

    .line 1050
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/n;->s:Lcom/google/n/aw;

    .line 1800
    new-instance v0, Lcom/google/maps/b/n;

    invoke-direct {v0}, Lcom/google/maps/b/n;-><init>()V

    sput-object v0, Lcom/google/maps/b/n;->p:Lcom/google/maps/b/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 527
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 923
    iput-byte v0, p0, Lcom/google/maps/b/n;->q:B

    .line 981
    iput v0, p0, Lcom/google/maps/b/n;->r:I

    .line 528
    iput v1, p0, Lcom/google/maps/b/n;->b:I

    .line 529
    iput v1, p0, Lcom/google/maps/b/n;->c:I

    .line 530
    iput v1, p0, Lcom/google/maps/b/n;->d:I

    .line 531
    iput v1, p0, Lcom/google/maps/b/n;->e:I

    .line 532
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/b/n;->f:I

    .line 533
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/b/n;->g:I

    .line 534
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    .line 535
    iput v1, p0, Lcom/google/maps/b/n;->i:I

    .line 536
    iput v1, p0, Lcom/google/maps/b/n;->j:I

    .line 537
    iput v1, p0, Lcom/google/maps/b/n;->k:I

    .line 538
    iput v1, p0, Lcom/google/maps/b/n;->l:I

    .line 539
    iput v1, p0, Lcom/google/maps/b/n;->m:I

    .line 540
    iput v1, p0, Lcom/google/maps/b/n;->n:I

    .line 541
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    .line 542
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 548
    invoke-direct {p0}, Lcom/google/maps/b/n;-><init>()V

    .line 549
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 553
    const/4 v0, 0x0

    .line 554
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 555
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 556
    sparse-switch v3, :sswitch_data_0

    .line 561
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 563
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 559
    goto :goto_0

    .line 568
    :sswitch_1
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 569
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 658
    :catch_0
    move-exception v0

    .line 659
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 664
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/n;->au:Lcom/google/n/bn;

    throw v0

    .line 573
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 574
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 660
    :catch_1
    move-exception v0

    .line 661
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 662
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 578
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 579
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->d:I

    goto :goto_0

    .line 583
    :sswitch_4
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 584
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->e:I

    goto :goto_0

    .line 588
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 589
    invoke-static {v3}, Lcom/google/maps/b/j;->a(I)Lcom/google/maps/b/j;

    move-result-object v4

    .line 590
    if-nez v4, :cond_1

    .line 591
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 593
    :cond_1
    iget v4, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/b/n;->a:I

    .line 594
    iput v3, p0, Lcom/google/maps/b/n;->f:I

    goto :goto_0

    .line 599
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 600
    invoke-static {v3}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v4

    .line 601
    if-nez v4, :cond_2

    .line 602
    const/4 v4, 0x6

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 604
    :cond_2
    iget v4, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/b/n;->a:I

    .line 605
    iput v3, p0, Lcom/google/maps/b/n;->g:I

    goto/16 :goto_0

    .line 610
    :sswitch_7
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 611
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    goto/16 :goto_0

    .line 615
    :sswitch_8
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 616
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->i:I

    goto/16 :goto_0

    .line 620
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 621
    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v4

    .line 622
    if-nez v4, :cond_3

    .line 623
    const/16 v4, 0x9

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 625
    :cond_3
    iget v4, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/maps/b/n;->a:I

    .line 626
    iput v3, p0, Lcom/google/maps/b/n;->j:I

    goto/16 :goto_0

    .line 631
    :sswitch_a
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 632
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->k:I

    goto/16 :goto_0

    .line 636
    :sswitch_b
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 637
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->l:I

    goto/16 :goto_0

    .line 641
    :sswitch_c
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 642
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->m:I

    goto/16 :goto_0

    .line 646
    :sswitch_d
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/b/n;->a:I

    .line 647
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/n;->n:I

    goto/16 :goto_0

    .line 651
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 652
    iget v4, p0, Lcom/google/maps/b/n;->a:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/maps/b/n;->a:I

    .line 653
    iput-object v3, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 664
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/n;->au:Lcom/google/n/bn;

    .line 665
    return-void

    .line 556
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 525
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 923
    iput-byte v0, p0, Lcom/google/maps/b/n;->q:B

    .line 981
    iput v0, p0, Lcom/google/maps/b/n;->r:I

    .line 526
    return-void
.end method

.method public static d()Lcom/google/maps/b/n;
    .locals 1

    .prologue
    .line 1803
    sget-object v0, Lcom/google/maps/b/n;->p:Lcom/google/maps/b/n;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/p;
    .locals 1

    .prologue
    .line 1112
    new-instance v0, Lcom/google/maps/b/p;

    invoke-direct {v0}, Lcom/google/maps/b/p;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 679
    sget-object v0, Lcom/google/maps/b/n;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 935
    invoke-virtual {p0}, Lcom/google/maps/b/n;->c()I

    .line 936
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 937
    iget v0, p0, Lcom/google/maps/b/n;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 939
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 940
    iget v0, p0, Lcom/google/maps/b/n;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_f

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 942
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 943
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/b/n;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 945
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 946
    iget v0, p0, Lcom/google/maps/b/n;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_11

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 948
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 949
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/maps/b/n;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_12

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 951
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 952
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/maps/b/n;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_13

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 954
    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 955
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 957
    :cond_6
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 958
    iget v0, p0, Lcom/google/maps/b/n;->i:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_14

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 960
    :cond_7
    :goto_6
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 961
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/b/n;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_15

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 963
    :cond_8
    :goto_7
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 964
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/maps/b/n;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_16

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 966
    :cond_9
    :goto_8
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 967
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/maps/b/n;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_17

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 969
    :cond_a
    :goto_9
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 970
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/maps/b/n;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_18

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 972
    :cond_b
    :goto_a
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 973
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/b/n;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_19

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 975
    :cond_c
    :goto_b
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 976
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 978
    :cond_d
    iget-object v0, p0, Lcom/google/maps/b/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 979
    return-void

    .line 937
    :cond_e
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 940
    :cond_f
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 943
    :cond_10
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 946
    :cond_11
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 949
    :cond_12
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 952
    :cond_13
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    .line 958
    :cond_14
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    .line 961
    :cond_15
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_7

    .line 964
    :cond_16
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_8

    .line 967
    :cond_17
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_9

    .line 970
    :cond_18
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_a

    .line 973
    :cond_19
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b

    .line 976
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto :goto_c
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 925
    iget-byte v1, p0, Lcom/google/maps/b/n;->q:B

    .line 926
    if-ne v1, v0, :cond_0

    .line 930
    :goto_0
    return v0

    .line 927
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 929
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/b/n;->q:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 983
    iget v0, p0, Lcom/google/maps/b/n;->r:I

    .line 984
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1045
    :goto_0
    return v0

    .line 987
    :cond_0
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1b

    .line 988
    iget v0, p0, Lcom/google/maps/b/n;->b:I

    .line 989
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 991
    :goto_2
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 992
    iget v3, p0, Lcom/google/maps/b/n;->c:I

    .line 993
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 995
    :cond_1
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 996
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/maps/b/n;->d:I

    .line 997
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 999
    :cond_2
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 1000
    iget v3, p0, Lcom/google/maps/b/n;->e:I

    .line 1001
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1003
    :cond_3
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 1004
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/maps/b/n;->f:I

    .line 1005
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1007
    :cond_4
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 1008
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/maps/b/n;->g:I

    .line 1009
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1011
    :cond_5
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 1012
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    .line 1013
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1015
    :cond_6
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 1016
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/maps/b/n;->i:I

    .line 1017
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_14

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1019
    :cond_7
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 1020
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/maps/b/n;->j:I

    .line 1021
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_15

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1023
    :cond_8
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 1024
    iget v3, p0, Lcom/google/maps/b/n;->k:I

    .line 1025
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_16

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_a
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1027
    :cond_9
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 1028
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/maps/b/n;->l:I

    .line 1029
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_b
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1031
    :cond_a
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 1032
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/maps/b/n;->m:I

    .line 1033
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_18

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_c
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1035
    :cond_b
    iget v3, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_1a

    .line 1036
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/maps/b/n;->n:I

    .line 1037
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_c
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 1039
    :goto_d
    iget v0, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_d

    .line 1040
    const/16 v3, 0xe

    .line 1041
    iget-object v0, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    :goto_e
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1043
    :cond_d
    iget-object v0, p0, Lcom/google/maps/b/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1044
    iput v0, p0, Lcom/google/maps/b/n;->r:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 989
    goto/16 :goto_1

    :cond_f
    move v3, v1

    .line 993
    goto/16 :goto_3

    :cond_10
    move v3, v1

    .line 997
    goto/16 :goto_4

    :cond_11
    move v3, v1

    .line 1001
    goto/16 :goto_5

    :cond_12
    move v3, v1

    .line 1005
    goto/16 :goto_6

    :cond_13
    move v3, v1

    .line 1009
    goto/16 :goto_7

    :cond_14
    move v3, v1

    .line 1017
    goto/16 :goto_8

    :cond_15
    move v3, v1

    .line 1021
    goto/16 :goto_9

    :cond_16
    move v3, v1

    .line 1025
    goto/16 :goto_a

    :cond_17
    move v3, v1

    .line 1029
    goto/16 :goto_b

    :cond_18
    move v3, v1

    .line 1033
    goto :goto_c

    .line 1041
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto :goto_e

    :cond_1a
    move v1, v0

    goto :goto_d

    :cond_1b
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 519
    invoke-static {}, Lcom/google/maps/b/n;->newBuilder()Lcom/google/maps/b/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/p;->a(Lcom/google/maps/b/n;)Lcom/google/maps/b/p;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 519
    invoke-static {}, Lcom/google/maps/b/n;->newBuilder()Lcom/google/maps/b/p;

    move-result-object v0

    return-object v0
.end method
