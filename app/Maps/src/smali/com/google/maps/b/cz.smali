.class public final enum Lcom/google/maps/b/cz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/cz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/cz;

.field public static final enum b:Lcom/google/maps/b/cz;

.field public static final enum c:Lcom/google/maps/b/cz;

.field public static final enum d:Lcom/google/maps/b/cz;

.field public static final enum e:Lcom/google/maps/b/cz;

.field public static final enum f:Lcom/google/maps/b/cz;

.field public static final enum g:Lcom/google/maps/b/cz;

.field public static final enum h:Lcom/google/maps/b/cz;

.field public static final enum i:Lcom/google/maps/b/cz;

.field public static final enum j:Lcom/google/maps/b/cz;

.field public static final enum k:Lcom/google/maps/b/cz;

.field public static final enum l:Lcom/google/maps/b/cz;

.field public static final enum m:Lcom/google/maps/b/cz;

.field private static final synthetic o:[Lcom/google/maps/b/cz;


# instance fields
.field public final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 96
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->a:Lcom/google/maps/b/cz;

    .line 100
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "EARTH_LOW_DENSITY"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->b:Lcom/google/maps/b/cz;

    .line 104
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "EARTH_LOWER_DENSITY"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->c:Lcom/google/maps/b/cz;

    .line 108
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "EARTH_LOWEST_DENSITY"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->d:Lcom/google/maps/b/cz;

    .line 112
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "NAVIGATION"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->e:Lcom/google/maps/b/cz;

    .line 116
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "NAVIGATION_LOW_LIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->f:Lcom/google/maps/b/cz;

    .line 120
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "NAVIGATION_SATELLITE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->g:Lcom/google/maps/b/cz;

    .line 124
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "NON_ROADMAP"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->h:Lcom/google/maps/b/cz;

    .line 128
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "ROADMAP"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->i:Lcom/google/maps/b/cz;

    .line 132
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "ROADMAP_GLASS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->j:Lcom/google/maps/b/cz;

    .line 136
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "ROADMAP_MUTED"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->k:Lcom/google/maps/b/cz;

    .line 140
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "ROADMAP_SATELLITE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->l:Lcom/google/maps/b/cz;

    .line 144
    new-instance v0, Lcom/google/maps/b/cz;

    const-string v1, "TERRAIN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/cz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/cz;->m:Lcom/google/maps/b/cz;

    .line 91
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/maps/b/cz;

    sget-object v1, Lcom/google/maps/b/cz;->a:Lcom/google/maps/b/cz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/cz;->b:Lcom/google/maps/b/cz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/cz;->c:Lcom/google/maps/b/cz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/cz;->d:Lcom/google/maps/b/cz;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/cz;->e:Lcom/google/maps/b/cz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/cz;->f:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/cz;->g:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/b/cz;->h:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/b/cz;->i:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/b/cz;->j:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/b/cz;->k:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/b/cz;->l:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/b/cz;->m:Lcom/google/maps/b/cz;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/cz;->o:[Lcom/google/maps/b/cz;

    .line 229
    new-instance v0, Lcom/google/maps/b/da;

    invoke-direct {v0}, Lcom/google/maps/b/da;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 238
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 239
    iput p3, p0, Lcom/google/maps/b/cz;->n:I

    .line 240
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/cz;
    .locals 1

    .prologue
    .line 206
    packed-switch p0, :pswitch_data_0

    .line 220
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 207
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/cz;->a:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 208
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/cz;->b:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 209
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/cz;->c:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 210
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/cz;->d:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 211
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/cz;->e:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 212
    :pswitch_5
    sget-object v0, Lcom/google/maps/b/cz;->f:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 213
    :pswitch_6
    sget-object v0, Lcom/google/maps/b/cz;->g:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 214
    :pswitch_7
    sget-object v0, Lcom/google/maps/b/cz;->h:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 215
    :pswitch_8
    sget-object v0, Lcom/google/maps/b/cz;->i:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 216
    :pswitch_9
    sget-object v0, Lcom/google/maps/b/cz;->j:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 217
    :pswitch_a
    sget-object v0, Lcom/google/maps/b/cz;->k:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 218
    :pswitch_b
    sget-object v0, Lcom/google/maps/b/cz;->l:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 219
    :pswitch_c
    sget-object v0, Lcom/google/maps/b/cz;->m:Lcom/google/maps/b/cz;

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/cz;
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/google/maps/b/cz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/cz;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/cz;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/maps/b/cz;->o:[Lcom/google/maps/b/cz;

    invoke-virtual {v0}, [Lcom/google/maps/b/cz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/cz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/google/maps/b/cz;->n:I

    return v0
.end method
