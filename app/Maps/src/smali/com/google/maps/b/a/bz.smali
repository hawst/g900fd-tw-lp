.class public Lcom/google/maps/b/a/bz;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Lcom/google/maps/b/a/bm;

.field private c:Lcom/google/maps/b/a/cr;

.field private d:Lcom/google/maps/b/a/bm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 10
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    .line 12
    new-instance v0, Lcom/google/maps/b/a/bm;

    invoke-direct {v0}, Lcom/google/maps/b/a/bm;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/bm;

    invoke-direct {v0}, Lcom/google/maps/b/a/bm;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/bm;
    .locals 4

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->c()V

    .line 21
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    .line 24
    :goto_0
    return-object v0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    iget-object v1, p0, Lcom/google/maps/b/a/bz;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/bm;->a([BII)V

    .line 24
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    goto :goto_0
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 55
    packed-switch p1, :pswitch_data_0

    .line 64
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 57
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 60
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Lcom/google/maps/b/a/bm;
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->c()V

    .line 34
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    .line 37
    :goto_0
    return-object v0

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    iget-object v1, p0, Lcom/google/maps/b/a/bz;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/bm;->a([BII)V

    .line 37
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    goto :goto_0
.end method

.method final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 47
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->b:Lcom/google/maps/b/a/bm;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->c()V

    .line 48
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 49
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->d:Lcom/google/maps/b/a/bm;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->c()V

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/google/maps/b/a/bz;->a()Lcom/google/maps/b/a/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 73
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 76
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "lo {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/bz;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 79
    invoke-virtual {p0}, Lcom/google/maps/b/a/bz;->b()Lcom/google/maps/b/a/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/bm;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 81
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 82
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "hi {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
