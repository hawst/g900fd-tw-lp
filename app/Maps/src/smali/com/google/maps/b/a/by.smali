.class public Lcom/google/maps/b/a/by;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cw;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 12
    new-instance v0, Lcom/google/maps/b/a/cw;

    invoke-direct {v0}, Lcom/google/maps/b/a/cw;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 52
    packed-switch p1, :pswitch_data_0

    .line 61
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 54
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 57
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 5

    .prologue
    .line 66
    packed-switch p1, :pswitch_data_0

    .line 72
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 68
    :pswitch_0
    iget-object v0, p0, Lcom/google/maps/b/a/by;->t:[B

    iget-object v1, p0, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    new-instance v2, Lcom/google/maps/b/a/dc;

    invoke-direct {v2, p2}, Lcom/google/maps/b/a/dc;-><init>(I)V

    add-int v3, p2, p3

    :goto_1
    iget v4, v2, Lcom/google/maps/b/a/dc;->a:I

    if-ge v4, v3, :cond_0

    invoke-static {v0, v2}, Lcom/google/maps/b/a/cq;->a([BLcom/google/maps/b/a/dc;)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/maps/b/a/cx;->a(I)V

    goto :goto_1

    .line 69
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 44
    iget-object v0, p0, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    iput v2, v0, Lcom/google/maps/b/a/cw;->b:I

    .line 45
    iget-object v0, p0, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 78
    :goto_0
    iget-object v3, p0, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    iget v3, v3, Lcom/google/maps/b/a/cw;->b:I

    if-ge v0, v3, :cond_0

    .line 79
    iget-object v3, p0, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    iget-object v3, v3, Lcom/google/maps/b/a/cw;->a:[F

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x28

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "color_transform_matrix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "z_plane: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0xb

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "off: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
