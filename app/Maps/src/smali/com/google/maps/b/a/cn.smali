.class public Lcom/google/maps/b/a/cn;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cz;

.field private b:Lcom/google/maps/b/a/cz;

.field private c:Lcom/google/maps/b/a/cz;

.field private d:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cn;->a:Lcom/google/maps/b/a/cz;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cn;->b:Lcom/google/maps/b/a/cz;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cn;->c:Lcom/google/maps/b/a/cz;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/cn;->d:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 61
    packed-switch p1, :pswitch_data_0

    .line 76
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 63
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 69
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 72
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->d:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 52
    iget-object v0, p0, Lcom/google/maps/b/a/cn;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 53
    iget-object v0, p0, Lcom/google/maps/b/a/cn;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 54
    iget-object v0, p0, Lcom/google/maps/b/a/cn;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 55
    iget-object v0, p0, Lcom/google/maps/b/a/cn;->d:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xf

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->a:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "zoom: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->b:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "x: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_2

    .line 89
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->c:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "y: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->d:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_3

    .line 92
    iget-object v1, p0, Lcom/google/maps/b/a/cn;->d:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "tile_size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
