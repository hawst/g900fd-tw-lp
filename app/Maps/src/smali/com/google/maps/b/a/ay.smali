.class public Lcom/google/maps/b/a/ay;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/db;

.field private b:Lcom/google/maps/b/a/db;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 10
    new-instance v0, Lcom/google/maps/b/a/db;

    invoke-direct {v0, v2, v3}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/ay;->a:Lcom/google/maps/b/a/db;

    .line 12
    new-instance v0, Lcom/google/maps/b/a/db;

    invoke-direct {v0, v2, v3}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/ay;->b:Lcom/google/maps/b/a/db;

    return-void
.end method


# virtual methods
.method protected final a(I)Z
    .locals 1

    .prologue
    .line 37
    packed-switch p1, :pswitch_data_0

    .line 43
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 41
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(IJ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 49
    packed-switch p1, :pswitch_data_0

    .line 58
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 51
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->a:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->b:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 30
    iget-object v0, p0, Lcom/google/maps/b/a/ay;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v1, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 31
    iget-object v0, p0, Lcom/google/maps/b/a/ay;->b:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v1, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 33
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x1e

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->a:Lcom/google/maps/b/a/db;

    iget-boolean v1, v1, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v1, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "lat: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->b:Lcom/google/maps/b/a/db;

    iget-boolean v1, v1, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/google/maps/b/a/ay;->b:Lcom/google/maps/b/a/db;

    iget-wide v2, v1, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "lng: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
