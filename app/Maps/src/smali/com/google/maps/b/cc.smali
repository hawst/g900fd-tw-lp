.class public final Lcom/google/maps/b/cc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/cj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/cc;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/maps/b/cc;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field public d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/maps/b/cd;

    invoke-direct {v0}, Lcom/google/maps/b/cd;-><init>()V

    sput-object v0, Lcom/google/maps/b/cc;->PARSER:Lcom/google/n/ax;

    .line 277
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/cc;->i:Lcom/google/n/aw;

    .line 680
    new-instance v0, Lcom/google/maps/b/cc;

    invoke-direct {v0}, Lcom/google/maps/b/cc;-><init>()V

    sput-object v0, Lcom/google/maps/b/cc;->f:Lcom/google/maps/b/cc;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 157
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    .line 209
    iput-byte v2, p0, Lcom/google/maps/b/cc;->g:B

    .line 243
    iput v2, p0, Lcom/google/maps/b/cc;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/cc;->d:I

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/16 v8, 0x8

    .line 28
    invoke-direct {p0}, Lcom/google/maps/b/cc;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 36
    sparse-switch v1, :sswitch_data_0

    .line 41
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 49
    iget v6, p0, Lcom/google/maps/b/cc;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/b/cc;->a:I

    .line 50
    iput-object v1, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 87
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_1

    .line 93
    iget-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    .line 95
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/cc;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 55
    iget v1, p0, Lcom/google/maps/b/cc;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/maps/b/cc;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 88
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 89
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 90
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/maps/b/cc;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/b/cc;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/maps/b/cc;->d:I

    goto :goto_0

    .line 92
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_2

    .line 64
    :sswitch_4
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_8

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 66
    or-int/lit8 v1, v0, 0x8

    .line 68
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 69
    goto/16 :goto_0

    .line 72
    :sswitch_5
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 73
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 74
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_2

    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_3

    move v1, v2

    :goto_5
    if-lez v1, :cond_2

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    .line 76
    or-int/lit8 v0, v0, 0x8

    .line 78
    :cond_2
    :goto_6
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_4

    move v1, v2

    :goto_7
    if-lez v1, :cond_5

    .line 79
    iget-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 74
    :cond_3
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_5

    .line 78
    :cond_4
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_7

    .line 81
    :cond_5
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 92
    :cond_6
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_7

    .line 93
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    .line 95
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/cc;->au:Lcom/google/n/bn;

    .line 96
    return-void

    .line 88
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 86
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_4

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 157
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    .line 209
    iput-byte v1, p0, Lcom/google/maps/b/cc;->g:B

    .line 243
    iput v1, p0, Lcom/google/maps/b/cc;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/cc;
    .locals 1

    .prologue
    .line 683
    sget-object v0, Lcom/google/maps/b/cc;->f:Lcom/google/maps/b/cc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/ce;
    .locals 1

    .prologue
    .line 339
    new-instance v0, Lcom/google/maps/b/ce;

    invoke-direct {v0}, Lcom/google/maps/b/ce;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/google/maps/b/cc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 227
    invoke-virtual {p0}, Lcom/google/maps/b/cc;->c()I

    .line 228
    iget v0, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 231
    :cond_0
    iget v0, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 232
    iget-object v0, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 234
    :cond_1
    iget v0, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 235
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/b/cc;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_2
    :goto_1
    move v1, v2

    .line 237
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 238
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 237
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 229
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 235
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 238
    :cond_5
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 240
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/cc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 241
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 211
    iget-byte v0, p0, Lcom/google/maps/b/cc;->g:B

    .line 212
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 222
    :goto_0
    return v0

    .line 213
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 215
    :cond_1
    iget v0, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 216
    iget-object v0, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/h/e;

    invoke-virtual {v0}, Lcom/google/maps/h/e;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 217
    iput-byte v2, p0, Lcom/google/maps/b/cc;->g:B

    move v0, v2

    .line 218
    goto :goto_0

    :cond_2
    move v0, v2

    .line 215
    goto :goto_1

    .line 221
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/b/cc;->g:B

    move v0, v1

    .line 222
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 245
    iget v0, p0, Lcom/google/maps/b/cc;->h:I

    .line 246
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 272
    :goto_0
    return v0

    .line 249
    :cond_0
    iget v0, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 251
    iget-object v0, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/cc;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 253
    :goto_2
    iget v2, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 254
    iget-object v2, p0, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    .line 255
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 257
    :cond_1
    iget v2, p0, Lcom/google/maps/b/cc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_6

    .line 258
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/b/cc;->d:I

    .line 259
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    move v2, v0

    :goto_4
    move v4, v1

    .line 263
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 264
    iget-object v0, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    .line 265
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v4

    .line 263
    add-int/lit8 v1, v1, 0x1

    move v4, v0

    goto :goto_5

    .line 251
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v2, v3

    .line 259
    goto :goto_3

    :cond_4
    move v0, v3

    .line 265
    goto :goto_6

    .line 267
    :cond_5
    add-int v0, v2, v4

    .line 268
    iget-object v1, p0, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 270
    iget-object v1, p0, Lcom/google/maps/b/cc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    iput v0, p0, Lcom/google/maps/b/cc;->h:I

    goto/16 :goto_0

    :cond_6
    move v2, v0

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/cc;->newBuilder()Lcom/google/maps/b/ce;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/ce;->a(Lcom/google/maps/b/cc;)Lcom/google/maps/b/ce;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/cc;->newBuilder()Lcom/google/maps/b/ce;

    move-result-object v0

    return-object v0
.end method
