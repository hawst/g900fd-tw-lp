.class public final Lcom/google/maps/b/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/a;",
        "Lcom/google/maps/b/c;",
        ">;",
        "Lcom/google/maps/b/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 327
    sget-object v0, Lcom/google/maps/b/a;->e:Lcom/google/maps/b/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 428
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    .line 564
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/c;->d:Ljava/lang/Object;

    .line 328
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/a;)Lcom/google/maps/b/c;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 366
    invoke-static {}, Lcom/google/maps/b/a;->d()Lcom/google/maps/b/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 386
    :goto_0
    return-object p0

    .line 367
    :cond_0
    iget v2, p1, Lcom/google/maps/b/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 368
    iget v2, p1, Lcom/google/maps/b/a;->b:I

    iget v3, p0, Lcom/google/maps/b/c;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/c;->a:I

    iput v2, p0, Lcom/google/maps/b/c;->b:I

    .line 370
    :cond_1
    iget-object v2, p1, Lcom/google/maps/b/a;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 371
    iget-object v2, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 372
    iget-object v2, p1, Lcom/google/maps/b/a;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    .line 373
    iget v2, p0, Lcom/google/maps/b/c;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/b/c;->a:I

    .line 380
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/b/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 381
    iget v0, p0, Lcom/google/maps/b/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/c;->a:I

    .line 382
    iget-object v0, p1, Lcom/google/maps/b/a;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/b/c;->d:Ljava/lang/Object;

    .line 385
    :cond_3
    iget-object v0, p1, Lcom/google/maps/b/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 367
    goto :goto_1

    .line 375
    :cond_5
    iget v2, p0, Lcom/google/maps/b/c;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/c;->a:I

    .line 376
    :cond_6
    iget-object v2, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/a;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v0, v1

    .line 380
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 319
    new-instance v2, Lcom/google/maps/b/a;

    invoke-direct {v2, p0}, Lcom/google/maps/b/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/c;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/maps/b/c;->b:I

    iput v1, v2, Lcom/google/maps/b/a;->b:I

    iget v1, p0, Lcom/google/maps/b/c;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/c;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/b/c;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/c;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/b/a;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/c;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/a;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/b/a;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 319
    check-cast p1, Lcom/google/maps/b/a;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/c;->a(Lcom/google/maps/b/a;)Lcom/google/maps/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    return v0
.end method
