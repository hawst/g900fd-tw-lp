.class public Lcom/google/maps/b/a/av;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cs;

.field public d:Lcom/google/maps/b/a/cr;

.field public e:Lcom/google/maps/b/a/cz;

.field public f:Lcom/google/maps/b/a/cz;

.field final g:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/maps/b/a/y;

.field private i:Lcom/google/maps/b/a/cr;

.field private j:Lcom/google/maps/b/a/w;

.field private k:Lcom/google/maps/b/a/ck;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 21
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    .line 23
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    .line 25
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    .line 27
    new-instance v0, Lcom/google/maps/b/a/y;

    invoke-direct {v0}, Lcom/google/maps/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->h:Lcom/google/maps/b/a/y;

    .line 29
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    .line 31
    new-instance v0, Lcom/google/maps/b/a/w;

    invoke-direct {v0}, Lcom/google/maps/b/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    .line 33
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    .line 35
    new-instance v0, Lcom/google/maps/b/a/ck;

    invoke-direct {v0}, Lcom/google/maps/b/a/ck;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    .line 37
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    .line 39
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    .line 57
    new-instance v0, Lcom/google/maps/b/a/aw;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/aw;-><init>(Lcom/google/maps/b/a/av;)V

    iput-object v0, p0, Lcom/google/maps/b/a/av;->g:Lcom/google/maps/b/a/dd;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/w;
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    invoke-virtual {v0}, Lcom/google/maps/b/a/w;->c()V

    .line 86
    iget-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    .line 89
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    iget-object v1, p0, Lcom/google/maps/b/a/av;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/w;->a([BII)V

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 140
    packed-switch p1, :pswitch_data_0

    .line 155
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 142
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 145
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 148
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 151
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 172
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 162
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 165
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 168
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Lcom/google/maps/b/a/ck;
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ck;->c()V

    .line 99
    iget-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    .line 102
    :goto_0
    return-object v0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    iget-object v1, p0, Lcom/google/maps/b/a/av;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/ck;->a([BII)V

    .line 102
    iget-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    goto :goto_0
.end method

.method public final b(I)Lcom/google/maps/b/a/y;
    .locals 4

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/maps/b/a/av;->h:Lcom/google/maps/b/a/y;

    iget-object v2, p0, Lcom/google/maps/b/a/av;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/y;->a([BII)V

    .line 79
    iget-object v0, p0, Lcom/google/maps/b/a/av;->h:Lcom/google/maps/b/a/y;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 125
    iget-object v0, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 126
    iget-object v0, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 127
    iget-object v0, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    iput v2, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 128
    iget-object v0, p0, Lcom/google/maps/b/a/av;->h:Lcom/google/maps/b/a/y;

    invoke-virtual {v0}, Lcom/google/maps/b/a/y;->c()V

    .line 129
    iget-object v0, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 130
    iget-object v0, p0, Lcom/google/maps/b/a/av;->j:Lcom/google/maps/b/a/w;

    invoke-virtual {v0}, Lcom/google/maps/b/a/w;->c()V

    .line 131
    iget-object v0, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 132
    iget-object v0, p0, Lcom/google/maps/b/a/av;->k:Lcom/google/maps/b/a/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ck;->c()V

    .line 133
    iget-object v0, p0, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 134
    iget-object v0, p0, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 136
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide v6, 0xffffffffL

    const/4 v1, 0x0

    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    iget-object v0, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v4, v0

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "text_color: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v4, v0

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "text_outline_color: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    move v0, v1

    .line 184
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_3

    .line 185
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/av;->b(I)Lcom/google/maps/b/a/y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/y;->toString()Ljava/lang/String;

    move-result-object v2

    .line 186
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 187
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 188
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 190
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "icon {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/av;->i:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_5

    .line 193
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->a()Lcom/google/maps/b/a/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/w;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    const-string v2, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 195
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 196
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "font {\n  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_7

    .line 201
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->b()Lcom/google/maps/b/a/ck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/ck;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    const-string v2, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 204
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "text_box_style {\n  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_8

    .line 209
    iget-object v0, p0, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x15

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "z_plane: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    :cond_8
    iget-object v0, p0, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_a

    .line 212
    iget-object v0, p0, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_9

    const/4 v1, 0x1

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "off: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
