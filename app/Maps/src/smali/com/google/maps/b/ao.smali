.class public final Lcom/google/maps/b/ao;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/at;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ao;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/maps/b/ao;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:Ljava/lang/Object;

.field i:I

.field j:Lcom/google/n/ao;

.field k:Z

.field l:Lcom/google/n/ao;

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/google/maps/b/ap;

    invoke-direct {v0}, Lcom/google/maps/b/ap;-><init>()V

    sput-object v0, Lcom/google/maps/b/ao;->PARSER:Lcom/google/n/ax;

    .line 890
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/ao;->p:Lcom/google/n/aw;

    .line 1620
    new-instance v0, Lcom/google/maps/b/ao;

    invoke-direct {v0}, Lcom/google/maps/b/ao;-><init>()V

    sput-object v0, Lcom/google/maps/b/ao;->m:Lcom/google/maps/b/ao;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 568
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    .line 718
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    .line 749
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    .line 764
    iput-byte v4, p0, Lcom/google/maps/b/ao;->n:B

    .line 833
    iput v4, p0, Lcom/google/maps/b/ao;->o:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v3, p0, Lcom/google/maps/b/ao;->c:I

    .line 20
    iput v3, p0, Lcom/google/maps/b/ao;->d:I

    .line 21
    iput v2, p0, Lcom/google/maps/b/ao;->e:I

    .line 22
    iput v2, p0, Lcom/google/maps/b/ao;->f:I

    .line 23
    iput v3, p0, Lcom/google/maps/b/ao;->g:I

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    .line 25
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/google/maps/b/ao;->i:I

    .line 26
    iget-object v0, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iput-boolean v3, p0, Lcom/google/maps/b/ao;->k:Z

    .line 28
    iget-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/maps/b/ao;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 41
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 43
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 50
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    iget-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 56
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/b/ao;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ao;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 61
    invoke-static {v0}, Lcom/google/maps/b/ar;->a(I)Lcom/google/maps/b/ar;

    move-result-object v5

    .line 62
    if-nez v5, :cond_1

    .line 63
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    .line 127
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 128
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :cond_1
    :try_start_4
    iget v5, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/maps/b/ao;->a:I

    .line 66
    iput v0, p0, Lcom/google/maps/b/ao;->e:I

    goto :goto_0

    .line 71
    :sswitch_3
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/ao;->g:I

    goto :goto_0

    .line 76
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 77
    iget v5, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/maps/b/ao;->a:I

    .line 78
    iput-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    goto :goto_0

    .line 82
    :sswitch_5
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/ao;->i:I

    goto :goto_0

    .line 87
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 88
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    goto/16 :goto_0

    .line 92
    :sswitch_7
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/b/ao;->k:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 97
    :sswitch_8
    iget-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 98
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    goto/16 :goto_0

    .line 102
    :sswitch_9
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/ao;->c:I

    goto/16 :goto_0

    .line 107
    :sswitch_a
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/ao;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/ao;->d:I

    goto/16 :goto_0

    .line 112
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 113
    invoke-static {v0}, Lcom/google/maps/b/ar;->a(I)Lcom/google/maps/b/ar;

    move-result-object v5

    .line 114
    if-nez v5, :cond_3

    .line 115
    const/16 v5, 0xb

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 117
    :cond_3
    iget v5, p0, Lcom/google/maps/b/ao;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/maps/b/ao;->a:I

    .line 118
    iput v0, p0, Lcom/google/maps/b/ao;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 130
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ao;->au:Lcom/google/n/bn;

    .line 131
    return-void

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 568
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    .line 718
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    .line 749
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    .line 764
    iput-byte v1, p0, Lcom/google/maps/b/ao;->n:B

    .line 833
    iput v1, p0, Lcom/google/maps/b/ao;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/ao;
    .locals 1

    .prologue
    .line 1623
    sget-object v0, Lcom/google/maps/b/ao;->m:Lcom/google/maps/b/ao;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/aq;
    .locals 1

    .prologue
    .line 952
    new-instance v0, Lcom/google/maps/b/aq;

    invoke-direct {v0}, Lcom/google/maps/b/aq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ao;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/google/maps/b/ao;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 796
    invoke-virtual {p0}, Lcom/google/maps/b/ao;->c()I

    .line 797
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 798
    iget-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 800
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_1

    .line 801
    iget v0, p0, Lcom/google/maps/b/ao;->e:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 803
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    .line 804
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/maps/b/ao;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 806
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_3

    .line 807
    iget-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    :goto_1
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 809
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    .line 810
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/b/ao;->i:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 812
    :cond_4
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_5

    .line 813
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 815
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_6

    .line 816
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/maps/b/ao;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_d

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 818
    :cond_6
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_7

    .line 819
    iget-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 821
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 822
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/b/ao;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 824
    :cond_8
    :goto_3
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_9

    .line 825
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/maps/b/ao;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 827
    :cond_9
    :goto_4
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    .line 828
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/maps/b/ao;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 830
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/ao;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 831
    return-void

    .line 801
    :cond_b
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 807
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 816
    goto :goto_2

    .line 822
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 825
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 828
    :cond_10
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 766
    iget-byte v0, p0, Lcom/google/maps/b/ao;->n:B

    .line 767
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 791
    :goto_0
    return v0

    .line 768
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 770
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 771
    iput-byte v2, p0, Lcom/google/maps/b/ao;->n:B

    move v0, v2

    .line 772
    goto :goto_0

    :cond_2
    move v0, v2

    .line 770
    goto :goto_1

    .line 774
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 775
    iput-byte v2, p0, Lcom/google/maps/b/ao;->n:B

    move v0, v2

    .line 776
    goto :goto_0

    .line 778
    :cond_4
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 779
    iget-object v0, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 780
    iput-byte v2, p0, Lcom/google/maps/b/ao;->n:B

    move v0, v2

    .line 781
    goto :goto_0

    :cond_5
    move v0, v2

    .line 778
    goto :goto_2

    .line 784
    :cond_6
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    .line 785
    iget-object v0, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 786
    iput-byte v2, p0, Lcom/google/maps/b/ao;->n:B

    move v0, v2

    .line 787
    goto :goto_0

    :cond_7
    move v0, v2

    .line 784
    goto :goto_3

    .line 790
    :cond_8
    iput-byte v1, p0, Lcom/google/maps/b/ao;->n:B

    move v0, v1

    .line 791
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 835
    iget v0, p0, Lcom/google/maps/b/ao;->o:I

    .line 836
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 885
    :goto_0
    return v0

    .line 839
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 840
    iget-object v0, p0, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    .line 841
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 843
    :goto_1
    iget v2, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 844
    iget v2, p0, Lcom/google/maps/b/ao;->e:I

    .line 845
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_b

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 847
    :cond_1
    iget v2, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_f

    .line 848
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/maps/b/ao;->g:I

    .line 849
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 851
    :goto_3
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_2

    .line 853
    iget-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 855
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_3

    .line 856
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/maps/b/ao;->i:I

    .line 857
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 859
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_4

    .line 860
    const/4 v0, 0x6

    iget-object v4, p0, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    .line 861
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 863
    :cond_4
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_5

    .line 864
    const/4 v0, 0x7

    iget-boolean v4, p0, Lcom/google/maps/b/ao;->k:Z

    .line 865
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 867
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_6

    .line 868
    const/16 v0, 0x8

    iget-object v4, p0, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    .line 869
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 871
    :cond_6
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_7

    .line 872
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/maps/b/ao;->c:I

    .line 873
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 875
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_8

    .line 876
    iget v0, p0, Lcom/google/maps/b/ao;->d:I

    .line 877
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 879
    :cond_8
    iget v0, p0, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_a

    .line 880
    const/16 v0, 0xb

    iget v4, p0, Lcom/google/maps/b/ao;->f:I

    .line 881
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_9
    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 883
    :cond_a
    iget-object v0, p0, Lcom/google/maps/b/ao;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 884
    iput v0, p0, Lcom/google/maps/b/ao;->o:I

    goto/16 :goto_0

    :cond_b
    move v2, v3

    .line 845
    goto/16 :goto_2

    .line 853
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_d
    move v0, v3

    .line 873
    goto :goto_5

    :cond_e
    move v0, v3

    .line 877
    goto :goto_6

    :cond_f
    move v2, v0

    goto/16 :goto_3

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ao;->newBuilder()Lcom/google/maps/b/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/aq;->a(Lcom/google/maps/b/ao;)Lcom/google/maps/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ao;->newBuilder()Lcom/google/maps/b/aq;

    move-result-object v0

    return-object v0
.end method
