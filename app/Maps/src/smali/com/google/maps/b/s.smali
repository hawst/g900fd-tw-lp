.class public final Lcom/google/maps/b/s;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/v;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/s;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/b/s;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/maps/b/t;

    invoke-direct {v0}, Lcom/google/maps/b/t;-><init>()V

    sput-object v0, Lcom/google/maps/b/s;->PARSER:Lcom/google/n/ax;

    .line 393
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/s;->m:Lcom/google/n/aw;

    .line 1099
    new-instance v0, Lcom/google/maps/b/s;

    invoke-direct {v0}, Lcom/google/maps/b/s;-><init>()V

    sput-object v0, Lcom/google/maps/b/s;->j:Lcom/google/maps/b/s;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    .line 188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    .line 263
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    .line 278
    iput-byte v3, p0, Lcom/google/maps/b/s;->k:B

    .line 348
    iput v3, p0, Lcom/google/maps/b/s;->l:I

    .line 18
    iput v4, p0, Lcom/google/maps/b/s;->b:I

    .line 19
    iput v4, p0, Lcom/google/maps/b/s;->c:I

    .line 20
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/maps/b/s;->d:I

    .line 21
    iget-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x20

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/maps/b/s;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 40
    sparse-switch v4, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v4, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 53
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/b/s;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v7, :cond_1

    .line 105
    iget-object v1, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    .line 107
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/s;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/b/s;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 102
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 63
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    goto :goto_0

    .line 67
    :sswitch_4
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v7, :cond_2

    .line 68
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    .line 70
    or-int/lit8 v1, v1, 0x20

    .line 72
    :cond_2
    iget-object v4, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 72
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 77
    :sswitch_5
    iget-object v4, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 78
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    goto/16 :goto_0

    .line 82
    :sswitch_6
    iget-object v4, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 83
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    goto/16 :goto_0

    .line 87
    :sswitch_7
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/b/s;->c:I

    goto/16 :goto_0

    .line 92
    :sswitch_8
    iget v4, p0, Lcom/google/maps/b/s;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/b/s;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/maps/b/s;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 104
    :cond_3
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v7, :cond_4

    .line 105
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    .line 107
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/s;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    .line 188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    .line 263
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    .line 278
    iput-byte v1, p0, Lcom/google/maps/b/s;->k:B

    .line 348
    iput v1, p0, Lcom/google/maps/b/s;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/s;
    .locals 1

    .prologue
    .line 1102
    sget-object v0, Lcom/google/maps/b/s;->j:Lcom/google/maps/b/s;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/u;
    .locals 1

    .prologue
    .line 455
    new-instance v0, Lcom/google/maps/b/u;

    invoke-direct {v0}, Lcom/google/maps/b/u;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/maps/b/s;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 320
    invoke-virtual {p0}, Lcom/google/maps/b/s;->c()I

    .line 321
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 324
    :cond_0
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 325
    iget v0, p0, Lcom/google/maps/b/s;->b:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 327
    :cond_1
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 328
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_2
    move v1, v2

    .line 330
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 331
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333
    :cond_3
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 334
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 336
    :cond_4
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 337
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 339
    :cond_5
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 340
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/maps/b/s;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 342
    :cond_6
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_7

    .line 343
    iget v0, p0, Lcom/google/maps/b/s;->d:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 345
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/s;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 346
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280
    iget-byte v0, p0, Lcom/google/maps/b/s;->k:B

    .line 281
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 315
    :cond_0
    :goto_0
    return v2

    .line 282
    :cond_1
    if-eqz v0, :cond_0

    .line 284
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 285
    iget-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ck;->d()Lcom/google/maps/b/ck;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/ck;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 286
    iput-byte v2, p0, Lcom/google/maps/b/s;->k:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 284
    goto :goto_1

    .line 290
    :cond_3
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 291
    iget-object v0, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 292
    iput-byte v2, p0, Lcom/google/maps/b/s;->k:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 290
    goto :goto_2

    :cond_5
    move v1, v2

    .line 296
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 297
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 298
    iput-byte v2, p0, Lcom/google/maps/b/s;->k:B

    goto :goto_0

    .line 296
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 302
    :cond_7
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 303
    iget-object v0, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 304
    iput-byte v2, p0, Lcom/google/maps/b/s;->k:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 302
    goto :goto_4

    .line 308
    :cond_9
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    .line 309
    iget-object v0, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 310
    iput-byte v2, p0, Lcom/google/maps/b/s;->k:B

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 308
    goto :goto_5

    .line 314
    :cond_b
    iput-byte v3, p0, Lcom/google/maps/b/s;->k:B

    move v2, v3

    .line 315
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 350
    iget v0, p0, Lcom/google/maps/b/s;->l:I

    .line 351
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 388
    :goto_0
    return v0

    .line 354
    :cond_0
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_8

    .line 355
    iget-object v0, p0, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    .line 356
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 358
    :goto_1
    iget v2, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    .line 359
    iget v2, p0, Lcom/google/maps/b/s;->b:I

    .line 360
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 362
    :cond_1
    iget v2, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 363
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    .line 364
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_2
    move v2, v1

    move v3, v0

    .line 366
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 367
    iget-object v0, p0, Lcom/google/maps/b/s;->g:Ljava/util/List;

    .line 368
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 366
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 370
    :cond_3
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_4

    .line 371
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    .line 372
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 374
    :cond_4
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_5

    .line 375
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    .line 376
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 378
    :cond_5
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    .line 379
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/maps/b/s;->c:I

    .line 380
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 382
    :cond_6
    iget v0, p0, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_7

    .line 383
    iget v0, p0, Lcom/google/maps/b/s;->d:I

    .line 384
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 386
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/s;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 387
    iput v0, p0, Lcom/google/maps/b/s;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/s;->newBuilder()Lcom/google/maps/b/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/u;->a(Lcom/google/maps/b/s;)Lcom/google/maps/b/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/s;->newBuilder()Lcom/google/maps/b/u;

    move-result-object v0

    return-object v0
.end method
