.class public final Lcom/google/maps/b/bu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/bx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/bs;",
        "Lcom/google/maps/b/bu;",
        ">;",
        "Lcom/google/maps/b/bx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Z

.field private e:Lcom/google/n/ao;

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1174
    sget-object v0, Lcom/google/maps/b/bs;->m:Lcom/google/maps/b/bs;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1352
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bu;->b:Ljava/lang/Object;

    .line 1428
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bu;->c:Ljava/lang/Object;

    .line 1536
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bu;->e:Lcom/google/n/ao;

    .line 1595
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/bu;->f:I

    .line 1632
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    .line 1768
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bu;->h:Lcom/google/n/ao;

    .line 1827
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    .line 1893
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    .line 1991
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bu;->l:Ljava/lang/Object;

    .line 1175
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/bs;)Lcom/google/maps/b/bu;
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1267
    invoke-static {}, Lcom/google/maps/b/bs;->d()Lcom/google/maps/b/bs;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1331
    :goto_0
    return-object p0

    .line 1268
    :cond_0
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1269
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1270
    iget-object v2, p1, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/bu;->b:Ljava/lang/Object;

    .line 1273
    :cond_1
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1274
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1275
    iget-object v2, p1, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/bu;->c:Ljava/lang/Object;

    .line 1278
    :cond_2
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1279
    iget-boolean v2, p1, Lcom/google/maps/b/bs;->d:Z

    iget v3, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/bu;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/bu;->d:Z

    .line 1281
    :cond_3
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1282
    iget-object v2, p0, Lcom/google/maps/b/bu;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1283
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1285
    :cond_4
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1286
    iget v2, p1, Lcom/google/maps/b/bs;->f:I

    invoke-static {v2}, Lcom/google/maps/b/bv;->a(I)Lcom/google/maps/b/bv;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/b/bv;->a:Lcom/google/maps/b/bv;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1268
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1273
    goto :goto_2

    :cond_8
    move v2, v1

    .line 1278
    goto :goto_3

    :cond_9
    move v2, v1

    .line 1281
    goto :goto_4

    :cond_a
    move v2, v1

    .line 1285
    goto :goto_5

    .line 1286
    :cond_b
    iget v3, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/b/bu;->a:I

    iget v2, v2, Lcom/google/maps/b/bv;->Y:I

    iput v2, p0, Lcom/google/maps/b/bu;->f:I

    .line 1288
    :cond_c
    iget-object v2, p1, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1289
    iget-object v2, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1290
    iget-object v2, p1, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    .line 1291
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1298
    :cond_d
    :goto_6
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 1299
    iget-object v2, p0, Lcom/google/maps/b/bu;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1300
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1302
    :cond_e
    iget-object v2, p1, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 1303
    iget-object v2, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1304
    iget-object v2, p1, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    .line 1305
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1312
    :cond_f
    :goto_8
    iget-object v2, p1, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 1313
    iget-object v2, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1314
    iget-object v2, p1, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    .line 1315
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1322
    :cond_10
    :goto_9
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_11

    .line 1323
    iget v2, p1, Lcom/google/maps/b/bs;->k:I

    iget v3, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/b/bu;->a:I

    iput v2, p0, Lcom/google/maps/b/bu;->k:I

    .line 1325
    :cond_11
    iget v2, p1, Lcom/google/maps/b/bs;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_1b

    :goto_b
    if-eqz v0, :cond_12

    .line 1326
    iget v0, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/b/bu;->a:I

    .line 1327
    iget-object v0, p1, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/b/bu;->l:Ljava/lang/Object;

    .line 1330
    :cond_12
    iget-object v0, p1, Lcom/google/maps/b/bs;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1293
    :cond_13
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1294
    :cond_14
    iget-object v2, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 1298
    goto/16 :goto_7

    .line 1307
    :cond_16
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v5, :cond_17

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1308
    :cond_17
    iget-object v2, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 1317
    :cond_18
    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_19

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bu;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/b/bu;->a:I

    .line 1318
    :cond_19
    iget-object v2, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    :cond_1a
    move v2, v1

    .line 1322
    goto/16 :goto_a

    :cond_1b
    move v0, v1

    .line 1325
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1166
    new-instance v2, Lcom/google/maps/b/bs;

    invoke-direct {v2, p0}, Lcom/google/maps/b/bs;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget-object v4, p0, Lcom/google/maps/b/bu;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/b/bu;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/maps/b/bu;->d:Z

    iput-boolean v4, v2, Lcom/google/maps/b/bs;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bu;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bu;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/b/bu;->f:I

    iput v4, v2, Lcom/google/maps/b/bs;->f:I

    iget v4, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/b/bu;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, v2, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bu;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bu;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/maps/b/bu;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/maps/b/bu;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    iget-object v1, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/maps/b/bu;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/maps/b/bu;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget v1, p0, Lcom/google/maps/b/bu;->k:I

    iput v1, v2, Lcom/google/maps/b/bs;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x80

    :cond_9
    iget-object v1, p0, Lcom/google/maps/b/bu;->l:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/b/bs;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1166
    check-cast p1, Lcom/google/maps/b/bs;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/bu;->a(Lcom/google/maps/b/bs;)Lcom/google/maps/b/bu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1335
    iget v0, p0, Lcom/google/maps/b/bu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1336
    iget-object v0, p0, Lcom/google/maps/b/bu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/c/a/a/b;->d()Lcom/google/c/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b;

    invoke-virtual {v0}, Lcom/google/c/a/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1347
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1335
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1341
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1342
    iget-object v0, p0, Lcom/google/maps/b/bu;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/dc;->d()Lcom/google/maps/b/dc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/dc;

    invoke-virtual {v0}, Lcom/google/maps/b/dc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1347
    goto :goto_1
.end method
