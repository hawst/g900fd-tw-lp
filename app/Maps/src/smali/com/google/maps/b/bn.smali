.class public final enum Lcom/google/maps/b/bn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/bn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/bn;

.field public static final enum b:Lcom/google/maps/b/bn;

.field public static final enum c:Lcom/google/maps/b/bn;

.field public static final enum d:Lcom/google/maps/b/bn;

.field public static final enum e:Lcom/google/maps/b/bn;

.field public static final enum f:Lcom/google/maps/b/bn;

.field public static final enum g:Lcom/google/maps/b/bn;

.field private static final synthetic i:[Lcom/google/maps/b/bn;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 260
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_IMAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->a:Lcom/google/maps/b/bn;

    .line 264
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_VECTOR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->b:Lcom/google/maps/b/bn;

    .line 268
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_KMZ"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->c:Lcom/google/maps/b/bn;

    .line 272
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_FEATUREMAP"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->d:Lcom/google/maps/b/bn;

    .line 276
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_PERTILE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->e:Lcom/google/maps/b/bn;

    .line 280
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_COPYRIGHTS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->f:Lcom/google/maps/b/bn;

    .line 284
    new-instance v0, Lcom/google/maps/b/bn;

    const-string v1, "OUTPUT_LOOM"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bn;->g:Lcom/google/maps/b/bn;

    .line 255
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/maps/b/bn;

    sget-object v1, Lcom/google/maps/b/bn;->a:Lcom/google/maps/b/bn;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/bn;->b:Lcom/google/maps/b/bn;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/bn;->c:Lcom/google/maps/b/bn;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/bn;->d:Lcom/google/maps/b/bn;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/bn;->e:Lcom/google/maps/b/bn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/bn;->f:Lcom/google/maps/b/bn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/bn;->g:Lcom/google/maps/b/bn;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/bn;->i:[Lcom/google/maps/b/bn;

    .line 339
    new-instance v0, Lcom/google/maps/b/bo;

    invoke-direct {v0}, Lcom/google/maps/b/bo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 348
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 349
    iput p3, p0, Lcom/google/maps/b/bn;->h:I

    .line 350
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/bn;
    .locals 1

    .prologue
    .line 322
    packed-switch p0, :pswitch_data_0

    .line 330
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 323
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/bn;->a:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 324
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/bn;->b:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 325
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/bn;->c:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 326
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/bn;->d:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 327
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/bn;->e:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 328
    :pswitch_5
    sget-object v0, Lcom/google/maps/b/bn;->f:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 329
    :pswitch_6
    sget-object v0, Lcom/google/maps/b/bn;->g:Lcom/google/maps/b/bn;

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/bn;
    .locals 1

    .prologue
    .line 255
    const-class v0, Lcom/google/maps/b/bn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bn;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/bn;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/google/maps/b/bn;->i:[Lcom/google/maps/b/bn;

    invoke-virtual {v0}, [Lcom/google/maps/b/bn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/bn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 318
    iget v0, p0, Lcom/google/maps/b/bn;->h:I

    return v0
.end method
