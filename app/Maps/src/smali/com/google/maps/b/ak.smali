.class public final Lcom/google/maps/b/ak;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/an;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ak;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/maps/b/ak;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:F

.field d:I

.field e:F

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/maps/b/al;

    invoke-direct {v0}, Lcom/google/maps/b/al;-><init>()V

    sput-object v0, Lcom/google/maps/b/ak;->PARSER:Lcom/google/n/ax;

    .line 246
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/ak;->j:Lcom/google/n/aw;

    .line 602
    new-instance v0, Lcom/google/maps/b/ak;

    invoke-direct {v0}, Lcom/google/maps/b/ak;-><init>()V

    sput-object v0, Lcom/google/maps/b/ak;->g:Lcom/google/maps/b/ak;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 101
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    .line 176
    iput-byte v2, p0, Lcom/google/maps/b/ak;->h:B

    .line 213
    iput v2, p0, Lcom/google/maps/b/ak;->i:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/ak;->c:F

    .line 20
    iput v3, p0, Lcom/google/maps/b/ak;->d:I

    .line 21
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/maps/b/ak;->e:F

    .line 22
    iput v3, p0, Lcom/google/maps/b/ak;->f:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/maps/b/ak;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 50
    iget v3, p0, Lcom/google/maps/b/ak;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/ak;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ak;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/maps/b/ak;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/ak;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/maps/b/ak;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/b/ak;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/ak;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/ak;->d:I

    goto :goto_0

    .line 64
    :sswitch_4
    iget v3, p0, Lcom/google/maps/b/ak;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/b/ak;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/maps/b/ak;->e:F

    goto :goto_0

    .line 69
    :sswitch_5
    iget v3, p0, Lcom/google/maps/b/ak;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/b/ak;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/ak;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ak;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 101
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    .line 176
    iput-byte v1, p0, Lcom/google/maps/b/ak;->h:B

    .line 213
    iput v1, p0, Lcom/google/maps/b/ak;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/ak;
    .locals 1

    .prologue
    .line 605
    sget-object v0, Lcom/google/maps/b/ak;->g:Lcom/google/maps/b/ak;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/am;
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/google/maps/b/am;

    invoke-direct {v0}, Lcom/google/maps/b/am;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/maps/b/ak;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x5

    const/4 v2, 0x2

    .line 194
    invoke-virtual {p0}, Lcom/google/maps/b/ak;->c()I

    .line 195
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 198
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 199
    iget v0, p0, Lcom/google/maps/b/ak;->c:F

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 201
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 202
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/b/ak;->d:I

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 204
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 205
    iget v0, p0, Lcom/google/maps/b/ak;->e:F

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 207
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 208
    iget v0, p0, Lcom/google/maps/b/ak;->f:I

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 210
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/ak;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 211
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 178
    iget-byte v0, p0, Lcom/google/maps/b/ak;->h:B

    .line 179
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 189
    :goto_0
    return v0

    .line 180
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 182
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 184
    iput-byte v2, p0, Lcom/google/maps/b/ak;->h:B

    move v0, v2

    .line 185
    goto :goto_0

    :cond_2
    move v0, v2

    .line 182
    goto :goto_1

    .line 188
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/b/ak;->h:B

    move v0, v1

    .line 189
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 215
    iget v0, p0, Lcom/google/maps/b/ak;->i:I

    .line 216
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 241
    :goto_0
    return v0

    .line 219
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 220
    iget-object v0, p0, Lcom/google/maps/b/ak;->b:Lcom/google/n/ao;

    .line 221
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 223
    :goto_1
    iget v2, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 224
    iget v2, p0, Lcom/google/maps/b/ak;->c:F

    .line 225
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 227
    :cond_1
    iget v2, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 228
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/maps/b/ak;->d:I

    .line 229
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 231
    :cond_2
    iget v2, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 232
    iget v2, p0, Lcom/google/maps/b/ak;->e:F

    .line 233
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 235
    :cond_3
    iget v2, p0, Lcom/google/maps/b/ak;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 236
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/maps/b/ak;->f:I

    .line 237
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/google/maps/b/ak;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    iput v0, p0, Lcom/google/maps/b/ak;->i:I

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ak;->newBuilder()Lcom/google/maps/b/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/am;->a(Lcom/google/maps/b/ak;)Lcom/google/maps/b/am;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ak;->newBuilder()Lcom/google/maps/b/am;

    move-result-object v0

    return-object v0
.end method
