.class public final Lcom/google/maps/b/w;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/af;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/w;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/maps/b/w;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Z

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/google/maps/b/x;

    invoke-direct {v0}, Lcom/google/maps/b/x;-><init>()V

    sput-object v0, Lcom/google/maps/b/w;->PARSER:Lcom/google/n/ax;

    .line 1152
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/w;->m:Lcom/google/n/aw;

    .line 1878
    new-instance v0, Lcom/google/maps/b/w;

    invoke-direct {v0}, Lcom/google/maps/b/w;-><init>()V

    sput-object v0, Lcom/google/maps/b/w;->j:Lcom/google/maps/b/w;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 976
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    .line 1029
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    .line 1044
    iput-byte v3, p0, Lcom/google/maps/b/w;->k:B

    .line 1102
    iput v3, p0, Lcom/google/maps/b/w;->l:I

    .line 18
    iput v2, p0, Lcom/google/maps/b/w;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    .line 20
    iput v2, p0, Lcom/google/maps/b/w;->d:I

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 24
    iput-boolean v2, p0, Lcom/google/maps/b/w;->h:Z

    .line 25
    iget-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/maps/b/w;-><init>()V

    .line 33
    const/4 v1, 0x0

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 37
    const/4 v0, 0x0

    move v2, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v2, :cond_b

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 42
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 53
    invoke-static {v0}, Lcom/google/maps/b/ad;->a(I)Lcom/google/maps/b/ad;

    move-result-object v4

    .line 54
    if-nez v4, :cond_3

    .line 55
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 128
    iget-object v2, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    .line 130
    :cond_1
    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 131
    iget-object v1, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 133
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/w;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_3
    :try_start_2
    iget v4, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/b/w;->a:I

    .line 58
    iput v0, p0, Lcom/google/maps/b/w;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 123
    :catch_1
    move-exception v0

    .line 124
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 125
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget v4, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/b/w;->a:I

    .line 65
    iput-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    goto :goto_0

    .line 69
    :sswitch_3
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/w;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/w;->d:I

    goto :goto_0

    .line 74
    :sswitch_4
    and-int/lit8 v0, v1, 0x8

    const/16 v4, 0x8

    if-eq v0, v4, :cond_4

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    .line 77
    or-int/lit8 v1, v1, 0x8

    .line 79
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 79
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 84
    :sswitch_5
    and-int/lit8 v0, v1, 0x20

    const/16 v4, 0x20

    if-eq v0, v4, :cond_5

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 86
    or-int/lit8 v1, v1, 0x20

    .line 88
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 92
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 93
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 94
    and-int/lit8 v0, v1, 0x20

    const/16 v5, 0x20

    if-eq v0, v5, :cond_6

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_7

    const/4 v0, -0x1

    :goto_1
    if-lez v0, :cond_6

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 96
    or-int/lit8 v1, v1, 0x20

    .line 98
    :cond_6
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_8

    const/4 v0, -0x1

    :goto_3
    if-lez v0, :cond_9

    .line 99
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 94
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_1

    .line 98
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_3

    .line 101
    :cond_9
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 105
    :sswitch_7
    iget-object v0, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 106
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/b/w;->a:I

    goto/16 :goto_0

    .line 110
    :sswitch_8
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/b/w;->a:I

    .line 111
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/b/w;->h:Z

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    .line 115
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 116
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/b/w;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 127
    :cond_b
    and-int/lit8 v0, v1, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_c

    .line 128
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    .line 130
    :cond_c
    and-int/lit8 v0, v1, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_d

    .line 131
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 133
    :cond_d
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->au:Lcom/google/n/bn;

    .line 134
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x38 -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 976
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    .line 1029
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    .line 1044
    iput-byte v1, p0, Lcom/google/maps/b/w;->k:B

    .line 1102
    iput v1, p0, Lcom/google/maps/b/w;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/w;
    .locals 1

    .prologue
    .line 1881
    sget-object v0, Lcom/google/maps/b/w;->j:Lcom/google/maps/b/w;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/ac;
    .locals 1

    .prologue
    .line 1214
    new-instance v0, Lcom/google/maps/b/ac;

    invoke-direct {v0}, Lcom/google/maps/b/ac;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Lcom/google/maps/b/w;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 1074
    invoke-virtual {p0}, Lcom/google/maps/b/w;->c()I

    .line 1075
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 1076
    iget v0, p0, Lcom/google/maps/b/w;->b:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1078
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1079
    iget-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1081
    :cond_1
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1082
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/b/w;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_2
    :goto_2
    move v1, v2

    .line 1084
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1085
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1076
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 1079
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1082
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_6
    move v1, v2

    .line 1087
    :goto_4
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1088
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1087
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1088
    :cond_7
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 1090
    :cond_8
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_9

    .line 1091
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1093
    :cond_9
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_b

    .line 1094
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/maps/b/w;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_a

    move v2, v3

    :cond_a
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 1096
    :cond_b
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_c

    .line 1097
    iget-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1099
    :cond_c
    iget-object v0, p0, Lcom/google/maps/b/w;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1100
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1046
    iget-byte v0, p0, Lcom/google/maps/b/w;->k:B

    .line 1047
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1069
    :cond_0
    :goto_0
    return v2

    .line 1048
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1050
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1051
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/y;->d()Lcom/google/maps/b/y;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/y;

    invoke-virtual {v0}, Lcom/google/maps/b/y;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1052
    iput-byte v2, p0, Lcom/google/maps/b/w;->k:B

    goto :goto_0

    .line 1050
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1056
    :cond_3
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1057
    iget-object v0, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/au;->d()Lcom/google/maps/b/au;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/au;

    invoke-virtual {v0}, Lcom/google/maps/b/au;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1058
    iput-byte v2, p0, Lcom/google/maps/b/w;->k:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1056
    goto :goto_2

    .line 1062
    :cond_5
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 1063
    iget-object v0, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1064
    iput-byte v2, p0, Lcom/google/maps/b/w;->k:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1062
    goto :goto_3

    .line 1068
    :cond_7
    iput-byte v3, p0, Lcom/google/maps/b/w;->k:B

    move v2, v3

    .line 1069
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 1104
    iget v0, p0, Lcom/google/maps/b/w;->l:I

    .line 1105
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1147
    :goto_0
    return v0

    .line 1108
    :cond_0
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 1109
    iget v0, p0, Lcom/google/maps/b/w;->b:I

    .line 1110
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 1112
    :goto_2
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1114
    iget-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1116
    :cond_1
    iget v0, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 1117
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/maps/b/w;->d:I

    .line 1118
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    :cond_2
    move v4, v2

    move v2, v3

    .line 1120
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1121
    iget-object v0, p0, Lcom/google/maps/b/w;->e:Ljava/util/List;

    .line 1122
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1120
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_3
    move v0, v1

    .line 1110
    goto :goto_1

    .line 1114
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v0, v1

    .line 1118
    goto :goto_4

    :cond_6
    move v2, v3

    move v5, v3

    .line 1126
    :goto_6
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 1127
    iget-object v0, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    .line 1128
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v5, v0

    .line 1126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_7
    move v0, v1

    .line 1128
    goto :goto_7

    .line 1130
    :cond_8
    add-int v0, v4, v5

    .line 1131
    iget-object v1, p0, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1133
    iget v1, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 1134
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    .line 1135
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1137
    :cond_9
    iget v1, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_a

    .line 1138
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/maps/b/w;->h:Z

    .line 1139
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1141
    :cond_a
    iget v1, p0, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_b

    .line 1142
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    .line 1143
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1145
    :cond_b
    iget-object v1, p0, Lcom/google/maps/b/w;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1146
    iput v0, p0, Lcom/google/maps/b/w;->l:I

    goto/16 :goto_0

    :cond_c
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/w;->newBuilder()Lcom/google/maps/b/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/ac;->a(Lcom/google/maps/b/w;)Lcom/google/maps/b/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/w;->newBuilder()Lcom/google/maps/b/ac;

    move-result-object v0

    return-object v0
.end method
