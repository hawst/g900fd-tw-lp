.class public final Lcom/google/maps/b/cq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/cr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/co;",
        "Lcom/google/maps/b/cq;",
        ">;",
        "Lcom/google/maps/b/cr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 377
    sget-object v0, Lcom/google/maps/b/co;->h:Lcom/google/maps/b/co;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 504
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->b:Lcom/google/n/ao;

    .line 563
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->c:Lcom/google/n/ao;

    .line 622
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->d:Lcom/google/n/ao;

    .line 681
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->e:Lcom/google/n/ao;

    .line 740
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->f:Lcom/google/n/ao;

    .line 799
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cq;->g:Lcom/google/n/ao;

    .line 378
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/co;)Lcom/google/maps/b/cq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 445
    invoke-static {}, Lcom/google/maps/b/co;->d()Lcom/google/maps/b/co;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 471
    :goto_0
    return-object p0

    .line 446
    :cond_0
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 447
    iget-object v2, p0, Lcom/google/maps/b/cq;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/co;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 448
    iget v2, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/cq;->a:I

    .line 450
    :cond_1
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 451
    iget-object v2, p0, Lcom/google/maps/b/cq;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/co;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 452
    iget v2, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/cq;->a:I

    .line 454
    :cond_2
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 455
    iget-object v2, p0, Lcom/google/maps/b/cq;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/co;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 456
    iget v2, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/b/cq;->a:I

    .line 458
    :cond_3
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 459
    iget-object v2, p0, Lcom/google/maps/b/cq;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/co;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 460
    iget v2, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/cq;->a:I

    .line 462
    :cond_4
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 463
    iget-object v2, p0, Lcom/google/maps/b/cq;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/co;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 464
    iget v2, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/cq;->a:I

    .line 466
    :cond_5
    iget v2, p1, Lcom/google/maps/b/co;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 467
    iget-object v0, p0, Lcom/google/maps/b/cq;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/b/co;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 468
    iget v0, p0, Lcom/google/maps/b/cq;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/b/cq;->a:I

    .line 470
    :cond_6
    iget-object v0, p1, Lcom/google/maps/b/co;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 446
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 450
    goto :goto_2

    :cond_9
    move v2, v1

    .line 454
    goto :goto_3

    :cond_a
    move v2, v1

    .line 458
    goto :goto_4

    :cond_b
    move v2, v1

    .line 462
    goto :goto_5

    :cond_c
    move v0, v1

    .line 466
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    new-instance v2, Lcom/google/maps/b/co;

    invoke-direct {v2, p0}, Lcom/google/maps/b/co;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/cq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/co;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cq;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cq;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/b/co;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cq;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cq;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/b/co;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cq;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cq;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/b/co;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cq;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cq;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/b/co;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cq;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cq;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v3, v2, Lcom/google/maps/b/co;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/b/cq;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/b/cq;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/b/co;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 369
    check-cast p1, Lcom/google/maps/b/co;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/cq;->a(Lcom/google/maps/b/co;)Lcom/google/maps/b/cq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 475
    iget v0, p0, Lcom/google/maps/b/cq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/google/maps/b/cq;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/h/e;

    invoke-virtual {v0}, Lcom/google/maps/h/e;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 499
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 475
    goto :goto_0

    .line 481
    :cond_1
    iget v0, p0, Lcom/google/maps/b/cq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 482
    iget-object v0, p0, Lcom/google/maps/b/cq;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/a;->d()Lcom/google/maps/h/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/h/a;

    invoke-virtual {v0}, Lcom/google/maps/h/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 484
    goto :goto_1

    :cond_2
    move v0, v1

    .line 481
    goto :goto_2

    .line 487
    :cond_3
    iget v0, p0, Lcom/google/maps/b/cq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 488
    iget-object v0, p0, Lcom/google/maps/b/cq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/cs;->d()Lcom/google/maps/b/cs;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/cs;

    invoke-virtual {v0}, Lcom/google/maps/b/cs;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 490
    goto :goto_1

    :cond_4
    move v0, v1

    .line 487
    goto :goto_3

    .line 493
    :cond_5
    iget v0, p0, Lcom/google/maps/b/cq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 494
    iget-object v0, p0, Lcom/google/maps/b/cq;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/s;->d()Lcom/google/maps/b/s;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/s;

    invoke-virtual {v0}, Lcom/google/maps/b/s;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 496
    goto :goto_1

    :cond_6
    move v0, v1

    .line 493
    goto :goto_4

    :cond_7
    move v0, v2

    .line 499
    goto :goto_1
.end method
