.class public final Lcom/google/maps/b/bm;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/br;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/maps/b/bj;",
        "Lcom/google/maps/b/bm;",
        ">;",
        "Lcom/google/maps/b/br;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:J

.field private h:Lcom/google/n/ao;

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;

.field private o:Lcom/google/n/ao;

.field private p:Lcom/google/n/ao;

.field private q:Ljava/lang/Object;

.field private r:J

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Lcom/google/n/f;

.field private x:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1133
    sget-object v0, Lcom/google/maps/b/bj;->w:Lcom/google/maps/b/bj;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 1427
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    .line 1564
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    .line 1700
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->f:Lcom/google/n/ao;

    .line 1791
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->h:Lcom/google/n/ao;

    .line 1850
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/bm;->i:I

    .line 1887
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    .line 1960
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->k:Lcom/google/n/ao;

    .line 2019
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->l:Lcom/google/n/ao;

    .line 2078
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->m:Lcom/google/n/ao;

    .line 2137
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->n:Lcom/google/n/ao;

    .line 2196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->o:Lcom/google/n/ao;

    .line 2255
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bm;->p:Lcom/google/n/ao;

    .line 2314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bm;->q:Ljava/lang/Object;

    .line 2550
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/b/bm;->w:Lcom/google/n/f;

    .line 1134
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/bj;)Lcom/google/maps/b/bm;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1298
    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1395
    :goto_0
    return-object p0

    .line 1299
    :cond_0
    iget-object v2, p1, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1300
    iget-object v2, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1301
    iget-object v2, p1, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    .line 1302
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1309
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1310
    iget-object v2, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1311
    iget-object v2, p1, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    .line 1312
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1319
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1320
    iget-object v2, p0, Lcom/google/maps/b/bm;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1321
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1323
    :cond_3
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1324
    iget-wide v2, p1, Lcom/google/maps/b/bj;->e:J

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/b/bm;->a:I

    iput-wide v2, p0, Lcom/google/maps/b/bm;->g:J

    .line 1326
    :cond_4
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1327
    iget-object v2, p0, Lcom/google/maps/b/bm;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1328
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1330
    :cond_5
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 1331
    iget v2, p1, Lcom/google/maps/b/bj;->g:I

    invoke-static {v2}, Lcom/google/maps/b/bn;->a(I)Lcom/google/maps/b/bn;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/maps/b/bn;->a:Lcom/google/maps/b/bn;

    :cond_6
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1304
    :cond_7
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1305
    :cond_8
    iget-object v2, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 1314
    :cond_9
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1315
    :cond_a
    iget-object v2, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 1319
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 1323
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 1326
    goto :goto_5

    :cond_e
    move v2, v1

    .line 1330
    goto :goto_6

    .line 1331
    :cond_f
    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iget v2, v2, Lcom/google/maps/b/bn;->h:I

    iput v2, p0, Lcom/google/maps/b/bm;->i:I

    .line 1333
    :cond_10
    iget-object v2, p1, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_11

    .line 1334
    iget-object v2, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1335
    iget-object v2, p1, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    .line 1336
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1343
    :cond_11
    :goto_7
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_8
    if-eqz v2, :cond_12

    .line 1344
    iget-object v2, p0, Lcom/google/maps/b/bm;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1345
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1347
    :cond_12
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_9
    if-eqz v2, :cond_13

    .line 1348
    iget-object v2, p0, Lcom/google/maps/b/bm;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1349
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1351
    :cond_13
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_a
    if-eqz v2, :cond_14

    .line 1352
    iget-object v2, p0, Lcom/google/maps/b/bm;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1353
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1355
    :cond_14
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_b
    if-eqz v2, :cond_15

    .line 1356
    iget-object v2, p0, Lcom/google/maps/b/bm;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1357
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1359
    :cond_15
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_c
    if-eqz v2, :cond_16

    .line 1360
    iget-object v2, p0, Lcom/google/maps/b/bm;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1361
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1363
    :cond_16
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_25

    move v2, v0

    :goto_d
    if-eqz v2, :cond_17

    .line 1364
    iget-object v2, p0, Lcom/google/maps/b/bm;->p:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1365
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1367
    :cond_17
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_e
    if-eqz v2, :cond_18

    .line 1368
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1369
    iget-object v2, p1, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/bm;->q:Ljava/lang/Object;

    .line 1372
    :cond_18
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_27

    move v2, v0

    :goto_f
    if-eqz v2, :cond_19

    .line 1373
    iget-wide v2, p1, Lcom/google/maps/b/bj;->p:J

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit16 v4, v4, 0x4000

    iput v4, p0, Lcom/google/maps/b/bm;->a:I

    iput-wide v2, p0, Lcom/google/maps/b/bm;->r:J

    .line 1375
    :cond_19
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_10
    if-eqz v2, :cond_1a

    .line 1376
    iget v2, p1, Lcom/google/maps/b/bj;->q:I

    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iput v2, p0, Lcom/google/maps/b/bm;->s:I

    .line 1378
    :cond_1a
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_11
    if-eqz v2, :cond_1b

    .line 1379
    iget-boolean v2, p1, Lcom/google/maps/b/bj;->r:Z

    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/bm;->t:Z

    .line 1381
    :cond_1b
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_2a

    move v2, v0

    :goto_12
    if-eqz v2, :cond_1c

    .line 1382
    iget-boolean v2, p1, Lcom/google/maps/b/bj;->s:Z

    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    or-int/2addr v3, v7

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/bm;->u:Z

    .line 1384
    :cond_1c
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2b

    move v2, v0

    :goto_13
    if-eqz v2, :cond_1d

    .line 1385
    iget-boolean v2, p1, Lcom/google/maps/b/bj;->t:Z

    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    const/high16 v4, 0x40000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/bm;->v:Z

    .line 1387
    :cond_1d
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2c

    move v2, v0

    :goto_14
    if-eqz v2, :cond_2e

    .line 1388
    iget-object v2, p1, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    if-nez v2, :cond_2d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1338
    :cond_1e
    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-eq v2, v3, :cond_1f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/bm;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/bm;->a:I

    .line 1339
    :cond_1f
    iget-object v2, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_20
    move v2, v1

    .line 1343
    goto/16 :goto_8

    :cond_21
    move v2, v1

    .line 1347
    goto/16 :goto_9

    :cond_22
    move v2, v1

    .line 1351
    goto/16 :goto_a

    :cond_23
    move v2, v1

    .line 1355
    goto/16 :goto_b

    :cond_24
    move v2, v1

    .line 1359
    goto/16 :goto_c

    :cond_25
    move v2, v1

    .line 1363
    goto/16 :goto_d

    :cond_26
    move v2, v1

    .line 1367
    goto/16 :goto_e

    :cond_27
    move v2, v1

    .line 1372
    goto/16 :goto_f

    :cond_28
    move v2, v1

    .line 1375
    goto/16 :goto_10

    :cond_29
    move v2, v1

    .line 1378
    goto :goto_11

    :cond_2a
    move v2, v1

    .line 1381
    goto :goto_12

    :cond_2b
    move v2, v1

    .line 1384
    goto :goto_13

    :cond_2c
    move v2, v1

    .line 1387
    goto :goto_14

    .line 1388
    :cond_2d
    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/maps/b/bm;->a:I

    iput-object v2, p0, Lcom/google/maps/b/bm;->w:Lcom/google/n/f;

    .line 1390
    :cond_2e
    iget v2, p1, Lcom/google/maps/b/bj;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_30

    :goto_15
    if-eqz v0, :cond_2f

    .line 1391
    iget-boolean v0, p1, Lcom/google/maps/b/bj;->v:Z

    iget v1, p0, Lcom/google/maps/b/bm;->a:I

    const/high16 v2, 0x100000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/maps/b/bm;->a:I

    iput-boolean v0, p0, Lcom/google/maps/b/bm;->x:Z

    .line 1393
    :cond_2f
    invoke-virtual {p0, p1}, Lcom/google/maps/b/bm;->a(Lcom/google/n/x;)V

    .line 1394
    iget-object v0, p1, Lcom/google/maps/b/bj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_30
    move v0, v1

    .line 1390
    goto :goto_15
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1126
    new-instance v2, Lcom/google/maps/b/bj;

    invoke-direct {v2, p0}, Lcom/google/maps/b/bj;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/maps/b/bm;->a:I

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/maps/b/bm;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/maps/b/bm;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_14

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-wide v4, p0, Lcom/google/maps/b/bm;->g:J

    iput-wide v4, v2, Lcom/google/maps/b/bj;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, v2, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget v4, p0, Lcom/google/maps/b/bm;->i:I

    iput v4, v2, Lcom/google/maps/b/bj;->g:I

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/maps/b/bm;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/maps/b/bm;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x10

    :cond_6
    iget-object v4, v2, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget-object v4, v2, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget-object v4, v2, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x80

    :cond_9
    iget-object v4, v2, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x100

    :cond_a
    iget-object v4, v2, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x200

    :cond_b
    iget-object v4, v2, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bm;->p:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bm;->p:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x400

    :cond_c
    iget-object v1, p0, Lcom/google/maps/b/bm;->q:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x800

    :cond_d
    iget-wide v4, p0, Lcom/google/maps/b/bm;->r:J

    iput-wide v4, v2, Lcom/google/maps/b/bj;->p:J

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/lit16 v0, v0, 0x1000

    :cond_e
    iget v1, p0, Lcom/google/maps/b/bm;->s:I

    iput v1, v2, Lcom/google/maps/b/bj;->q:I

    and-int v1, v3, v8

    if-ne v1, v8, :cond_f

    or-int/lit16 v0, v0, 0x2000

    :cond_f
    iget-boolean v1, p0, Lcom/google/maps/b/bm;->t:Z

    iput-boolean v1, v2, Lcom/google/maps/b/bj;->r:Z

    and-int v1, v3, v9

    if-ne v1, v9, :cond_10

    or-int/lit16 v0, v0, 0x4000

    :cond_10
    iget-boolean v1, p0, Lcom/google/maps/b/bm;->u:Z

    iput-boolean v1, v2, Lcom/google/maps/b/bj;->s:Z

    const/high16 v1, 0x40000

    and-int/2addr v1, v3

    const/high16 v4, 0x40000

    if-ne v1, v4, :cond_11

    or-int/2addr v0, v7

    :cond_11
    iget-boolean v1, p0, Lcom/google/maps/b/bm;->v:Z

    iput-boolean v1, v2, Lcom/google/maps/b/bj;->t:Z

    const/high16 v1, 0x80000

    and-int/2addr v1, v3

    const/high16 v4, 0x80000

    if-ne v1, v4, :cond_12

    or-int/2addr v0, v8

    :cond_12
    iget-object v1, p0, Lcom/google/maps/b/bm;->w:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    const/high16 v1, 0x100000

    and-int/2addr v1, v3

    const/high16 v3, 0x100000

    if-ne v1, v3, :cond_13

    or-int/2addr v0, v9

    :cond_13
    iget-boolean v1, p0, Lcom/google/maps/b/bm;->x:Z

    iput-boolean v1, v2, Lcom/google/maps/b/bj;->v:Z

    iput v0, v2, Lcom/google/maps/b/bj;->a:I

    return-object v2

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1126
    check-cast p1, Lcom/google/maps/b/bj;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/bm;->a(Lcom/google/maps/b/bj;)Lcom/google/maps/b/bm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1399
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1400
    iget-object v0, p0, Lcom/google/maps/b/bm;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/co;->d()Lcom/google/maps/b/co;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/co;

    invoke-virtual {v0}, Lcom/google/maps/b/co;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1421
    :cond_0
    :goto_1
    return v2

    .line 1399
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1405
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1406
    iget-object v0, p0, Lcom/google/maps/b/bm;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/w;

    invoke-virtual {v0}, Lcom/google/maps/b/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1411
    :cond_3
    iget v0, p0, Lcom/google/maps/b/bm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1412
    iget-object v0, p0, Lcom/google/maps/b/bm;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bs;->d()Lcom/google/maps/b/bs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bs;

    invoke-virtual {v0}, Lcom/google/maps/b/bs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417
    :cond_4
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    .line 1421
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1411
    goto :goto_3
.end method
