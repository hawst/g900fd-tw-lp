.class public Lcom/google/maps/b/a/bu;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cz;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Lcom/google/maps/b/a/cu;

.field private f:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bu;->f:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 28
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bu;->t:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v2, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 31
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    .line 34
    :goto_0
    return-object v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bu;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v3, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 34
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 82
    packed-switch p1, :pswitch_data_0

    .line 97
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 84
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 87
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 90
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 93
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->f:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 102
    packed-switch p1, :pswitch_data_0

    .line 108
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 104
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 71
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 72
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->e:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 73
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 74
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 75
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 76
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->f:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 78
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, 0xffffffffL

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/maps/b/a/bu;->a()Lcom/google/maps/b/a/cu;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "image_data: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "multi_zoom_style_index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "z_grade: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_3

    .line 124
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v2, v1

    and-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "z_within_grade: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->f:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_4

    .line 127
    iget-object v1, p0, Lcom/google/maps/b/a/bu;->f:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v2, v1

    and-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "legacy_base_tile_op_id: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
