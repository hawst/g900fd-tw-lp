.class public final enum Lcom/google/maps/b/ar;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/ar;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/maps/b/ar;

.field public static final enum B:Lcom/google/maps/b/ar;

.field public static final enum C:Lcom/google/maps/b/ar;

.field public static final enum D:Lcom/google/maps/b/ar;

.field public static final enum E:Lcom/google/maps/b/ar;

.field public static final enum F:Lcom/google/maps/b/ar;

.field public static final enum G:Lcom/google/maps/b/ar;

.field public static final enum H:Lcom/google/maps/b/ar;

.field public static final enum I:Lcom/google/maps/b/ar;

.field public static final enum J:Lcom/google/maps/b/ar;

.field public static final enum K:Lcom/google/maps/b/ar;

.field public static final enum L:Lcom/google/maps/b/ar;

.field public static final enum M:Lcom/google/maps/b/ar;

.field public static final enum N:Lcom/google/maps/b/ar;

.field public static final enum O:Lcom/google/maps/b/ar;

.field public static final enum P:Lcom/google/maps/b/ar;

.field private static final synthetic R:[Lcom/google/maps/b/ar;

.field public static final enum a:Lcom/google/maps/b/ar;

.field public static final enum b:Lcom/google/maps/b/ar;

.field public static final enum c:Lcom/google/maps/b/ar;

.field public static final enum d:Lcom/google/maps/b/ar;

.field public static final enum e:Lcom/google/maps/b/ar;

.field public static final enum f:Lcom/google/maps/b/ar;

.field public static final enum g:Lcom/google/maps/b/ar;

.field public static final enum h:Lcom/google/maps/b/ar;

.field public static final enum i:Lcom/google/maps/b/ar;

.field public static final enum j:Lcom/google/maps/b/ar;

.field public static final enum k:Lcom/google/maps/b/ar;

.field public static final enum l:Lcom/google/maps/b/ar;

.field public static final enum m:Lcom/google/maps/b/ar;

.field public static final enum n:Lcom/google/maps/b/ar;

.field public static final enum o:Lcom/google/maps/b/ar;

.field public static final enum p:Lcom/google/maps/b/ar;

.field public static final enum q:Lcom/google/maps/b/ar;

.field public static final enum r:Lcom/google/maps/b/ar;

.field public static final enum s:Lcom/google/maps/b/ar;

.field public static final enum t:Lcom/google/maps/b/ar;

.field public static final enum u:Lcom/google/maps/b/ar;

.field public static final enum v:Lcom/google/maps/b/ar;

.field public static final enum w:Lcom/google/maps/b/ar;

.field public static final enum x:Lcom/google/maps/b/ar;

.field public static final enum y:Lcom/google/maps/b/ar;

.field public static final enum z:Lcom/google/maps/b/ar;


# instance fields
.field final Q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 156
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_MARKER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->a:Lcom/google/maps/b/ar;

    .line 160
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_MARKER_MEDIUM"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->b:Lcom/google/maps/b/ar;

    .line 164
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_MARKER_SMALL"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->c:Lcom/google/maps/b/ar;

    .line 168
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_MARKER_TINY"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->d:Lcom/google/maps/b/ar;

    .line 172
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_CIRCLE"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->e:Lcom/google/maps/b/ar;

    .line 176
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_CIRCLE_MEDIUM"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->f:Lcom/google/maps/b/ar;

    .line 180
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_CIRCLE_MEDIUM_NO_SHADOW"

    const/4 v2, 0x6

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->g:Lcom/google/maps/b/ar;

    .line 184
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ARROW"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->h:Lcom/google/maps/b/ar;

    .line 188
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_GOOGLE_LOGO"

    const/16 v2, 0x8

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->i:Lcom/google/maps/b/ar;

    .line 192
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_GOOGLE_LOGO_SMALL"

    const/16 v2, 0x9

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->j:Lcom/google/maps/b/ar;

    .line 196
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_COUPON"

    const/16 v2, 0xa

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->k:Lcom/google/maps/b/ar;

    .line 200
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_DIRECTIONS"

    const/16 v2, 0xb

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->l:Lcom/google/maps/b/ar;

    .line 204
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_EVENT"

    const/16 v2, 0xc

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->m:Lcom/google/maps/b/ar;

    .line 208
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_MENU"

    const/16 v2, 0xd

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->n:Lcom/google/maps/b/ar;

    .line 212
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_PHOTO"

    const/16 v2, 0xe

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->o:Lcom/google/maps/b/ar;

    .line 216
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_RESERVATION"

    const/16 v2, 0xf

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->p:Lcom/google/maps/b/ar;

    .line 220
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_VIDEO"

    const/16 v2, 0x10

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->q:Lcom/google/maps/b/ar;

    .line 224
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_WEBSITE"

    const/16 v2, 0x11

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->r:Lcom/google/maps/b/ar;

    .line 228
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_ELA_BIZUPDATE"

    const/16 v2, 0x12

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->s:Lcom/google/maps/b/ar;

    .line 232
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_USER_LOCATION"

    const/16 v2, 0x13

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->t:Lcom/google/maps/b/ar;

    .line 236
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_USER_LOCATION_MEDIUM"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->u:Lcom/google/maps/b/ar;

    .line 240
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_USER_LOCATION_SMALL"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->v:Lcom/google/maps/b/ar;

    .line 244
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_CIRCLE"

    const/16 v2, 0x16

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->w:Lcom/google/maps/b/ar;

    .line 248
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_CIRCLE_NO_DOT"

    const/16 v2, 0x17

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->x:Lcom/google/maps/b/ar;

    .line 252
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MARKER_BLUE"

    const/16 v2, 0x18

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->y:Lcom/google/maps/b/ar;

    .line 256
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MARKER_GREEN"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->z:Lcom/google/maps/b/ar;

    .line 260
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MARKER_RED"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->A:Lcom/google/maps/b/ar;

    .line 264
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_ENCIRCLED_PIN_SMALL"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->B:Lcom/google/maps/b/ar;

    .line 268
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_ENCIRCLED_PIN_MEDIUM"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->C:Lcom/google/maps/b/ar;

    .line 272
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_ENCIRCLED_PIN_LARGE"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->D:Lcom/google/maps/b/ar;

    .line 276
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_POI"

    const/16 v2, 0x1e

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->E:Lcom/google/maps/b/ar;

    .line 280
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_POI_MEDIUM"

    const/16 v2, 0x1f

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->F:Lcom/google/maps/b/ar;

    .line 284
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_GENERIC_SEARCH_SMALL"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->G:Lcom/google/maps/b/ar;

    .line 288
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_GENERIC_SEARCH_MEDIUM"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->H:Lcom/google/maps/b/ar;

    .line 292
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_GENERIC_SEARCH_LARGE"

    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->I:Lcom/google/maps/b/ar;

    .line 296
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MEASLE_SMALL"

    const/16 v2, 0x23

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->J:Lcom/google/maps/b/ar;

    .line 300
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MEASLE_MEDIUM"

    const/16 v2, 0x24

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->K:Lcom/google/maps/b/ar;

    .line 304
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SPOTLIGHT_MEASLE_LARGE"

    const/16 v2, 0x25

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->L:Lcom/google/maps/b/ar;

    .line 308
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_SANTA_PIN"

    const/16 v2, 0x26

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->M:Lcom/google/maps/b/ar;

    .line 312
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_GAS_STATION_SEARCH_SMALL"

    const/16 v2, 0x27

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->N:Lcom/google/maps/b/ar;

    .line 316
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_GAS_STATION_SEARCH_MEDIUM"

    const/16 v2, 0x28

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->O:Lcom/google/maps/b/ar;

    .line 320
    new-instance v0, Lcom/google/maps/b/ar;

    const-string v1, "ICON_GAS_STATION_SEARCH_LARGE"

    const/16 v2, 0x29

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ar;->P:Lcom/google/maps/b/ar;

    .line 151
    const/16 v0, 0x2a

    new-array v0, v0, [Lcom/google/maps/b/ar;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/maps/b/ar;->a:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/maps/b/ar;->b:Lcom/google/maps/b/ar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/ar;->c:Lcom/google/maps/b/ar;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/ar;->d:Lcom/google/maps/b/ar;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/ar;->e:Lcom/google/maps/b/ar;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/ar;->f:Lcom/google/maps/b/ar;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/ar;->g:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/b/ar;->h:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/b/ar;->i:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/b/ar;->j:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/b/ar;->k:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/b/ar;->l:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/b/ar;->m:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/b/ar;->n:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/b/ar;->o:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/b/ar;->p:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/b/ar;->q:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/b/ar;->r:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/b/ar;->s:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/b/ar;->t:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/b/ar;->u:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/b/ar;->v:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/b/ar;->w:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/b/ar;->x:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/b/ar;->y:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/b/ar;->z:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/b/ar;->A:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/b/ar;->B:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/b/ar;->C:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/b/ar;->D:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/b/ar;->E:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/maps/b/ar;->F:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/maps/b/ar;->G:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/maps/b/ar;->H:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/maps/b/ar;->I:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/maps/b/ar;->J:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/maps/b/ar;->K:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/maps/b/ar;->L:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/maps/b/ar;->M:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/maps/b/ar;->N:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/maps/b/ar;->O:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/maps/b/ar;->P:Lcom/google/maps/b/ar;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/ar;->R:[Lcom/google/maps/b/ar;

    .line 550
    new-instance v0, Lcom/google/maps/b/as;

    invoke-direct {v0}, Lcom/google/maps/b/as;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 560
    iput p3, p0, Lcom/google/maps/b/ar;->Q:I

    .line 561
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/ar;
    .locals 1

    .prologue
    .line 498
    packed-switch p0, :pswitch_data_0

    .line 541
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 499
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/ar;->a:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 500
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/ar;->b:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 501
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/ar;->c:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 502
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/ar;->d:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 503
    :pswitch_5
    sget-object v0, Lcom/google/maps/b/ar;->e:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 504
    :pswitch_6
    sget-object v0, Lcom/google/maps/b/ar;->f:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 505
    :pswitch_7
    sget-object v0, Lcom/google/maps/b/ar;->g:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 506
    :pswitch_8
    sget-object v0, Lcom/google/maps/b/ar;->h:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 507
    :pswitch_9
    sget-object v0, Lcom/google/maps/b/ar;->i:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 508
    :pswitch_a
    sget-object v0, Lcom/google/maps/b/ar;->j:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 509
    :pswitch_b
    sget-object v0, Lcom/google/maps/b/ar;->k:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 510
    :pswitch_c
    sget-object v0, Lcom/google/maps/b/ar;->l:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 511
    :pswitch_d
    sget-object v0, Lcom/google/maps/b/ar;->m:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 512
    :pswitch_e
    sget-object v0, Lcom/google/maps/b/ar;->n:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 513
    :pswitch_f
    sget-object v0, Lcom/google/maps/b/ar;->o:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 514
    :pswitch_10
    sget-object v0, Lcom/google/maps/b/ar;->p:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 515
    :pswitch_11
    sget-object v0, Lcom/google/maps/b/ar;->q:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 516
    :pswitch_12
    sget-object v0, Lcom/google/maps/b/ar;->r:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 517
    :pswitch_13
    sget-object v0, Lcom/google/maps/b/ar;->s:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 518
    :pswitch_14
    sget-object v0, Lcom/google/maps/b/ar;->t:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 519
    :pswitch_15
    sget-object v0, Lcom/google/maps/b/ar;->u:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 520
    :pswitch_16
    sget-object v0, Lcom/google/maps/b/ar;->v:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 521
    :pswitch_17
    sget-object v0, Lcom/google/maps/b/ar;->w:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 522
    :pswitch_18
    sget-object v0, Lcom/google/maps/b/ar;->x:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 523
    :pswitch_19
    sget-object v0, Lcom/google/maps/b/ar;->y:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 524
    :pswitch_1a
    sget-object v0, Lcom/google/maps/b/ar;->z:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 525
    :pswitch_1b
    sget-object v0, Lcom/google/maps/b/ar;->A:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 526
    :pswitch_1c
    sget-object v0, Lcom/google/maps/b/ar;->B:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 527
    :pswitch_1d
    sget-object v0, Lcom/google/maps/b/ar;->C:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 528
    :pswitch_1e
    sget-object v0, Lcom/google/maps/b/ar;->D:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 529
    :pswitch_1f
    sget-object v0, Lcom/google/maps/b/ar;->E:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 530
    :pswitch_20
    sget-object v0, Lcom/google/maps/b/ar;->F:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 531
    :pswitch_21
    sget-object v0, Lcom/google/maps/b/ar;->G:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 532
    :pswitch_22
    sget-object v0, Lcom/google/maps/b/ar;->H:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 533
    :pswitch_23
    sget-object v0, Lcom/google/maps/b/ar;->I:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 534
    :pswitch_24
    sget-object v0, Lcom/google/maps/b/ar;->J:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 535
    :pswitch_25
    sget-object v0, Lcom/google/maps/b/ar;->K:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 536
    :pswitch_26
    sget-object v0, Lcom/google/maps/b/ar;->L:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 537
    :pswitch_27
    sget-object v0, Lcom/google/maps/b/ar;->M:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 538
    :pswitch_28
    sget-object v0, Lcom/google/maps/b/ar;->N:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 539
    :pswitch_29
    sget-object v0, Lcom/google/maps/b/ar;->O:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 540
    :pswitch_2a
    sget-object v0, Lcom/google/maps/b/ar;->P:Lcom/google/maps/b/ar;

    goto :goto_0

    .line 498
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_9
        :pswitch_a
        :pswitch_13
        :pswitch_14
        :pswitch_6
        :pswitch_15
        :pswitch_16
        :pswitch_1f
        :pswitch_0
        :pswitch_17
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_20
        :pswitch_19
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_18
        :pswitch_27
        :pswitch_7
        :pswitch_28
        :pswitch_29
        :pswitch_2a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/ar;
    .locals 1

    .prologue
    .line 151
    const-class v0, Lcom/google/maps/b/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ar;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/ar;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/maps/b/ar;->R:[Lcom/google/maps/b/ar;

    invoke-virtual {v0}, [Lcom/google/maps/b/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/ar;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lcom/google/maps/b/ar;->Q:I

    return v0
.end method
