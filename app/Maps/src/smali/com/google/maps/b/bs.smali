.class public final Lcom/google/maps/b/bs;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/bx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bs;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/maps/b/bs;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Z

.field e:Lcom/google/n/ao;

.field f:I

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field k:I

.field l:Ljava/lang/Object;

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/google/maps/b/bt;

    invoke-direct {v0}, Lcom/google/maps/b/bt;-><init>()V

    sput-object v0, Lcom/google/maps/b/bs;->PARSER:Lcom/google/n/ax;

    .line 1094
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/bs;->p:Lcom/google/n/aw;

    .line 2071
    new-instance v0, Lcom/google/maps/b/bs;

    invoke-direct {v0}, Lcom/google/maps/b/bs;-><init>()V

    sput-object v0, Lcom/google/maps/b/bs;->m:Lcom/google/maps/b/bs;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 781
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    .line 856
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    .line 972
    iput-byte v3, p0, Lcom/google/maps/b/bs;->n:B

    .line 1033
    iput v3, p0, Lcom/google/maps/b/bs;->o:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    .line 20
    iput-boolean v2, p0, Lcom/google/maps/b/bs;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput v2, p0, Lcom/google/maps/b/bs;->f:I

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    .line 27
    iput v2, p0, Lcom/google/maps/b/bs;->k:I

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const v12, 0x7fffffff

    const/16 v11, 0x100

    const/16 v10, 0x80

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/maps/b/bs;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 41
    :cond_0
    :goto_0
    if-nez v4, :cond_11

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 43
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x1

    move v4, v0

    goto :goto_0

    .line 45
    :sswitch_0
    const/4 v0, 0x1

    move v4, v0

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget v6, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/maps/b/bs;->a:I

    .line 57
    iput-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    .line 158
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x80

    if-ne v2, v10, :cond_1

    .line 164
    iget-object v2, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    .line 166
    :cond_1
    and-int/lit16 v2, v1, 0x100

    if-ne v2, v11, :cond_2

    .line 167
    iget-object v2, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    .line 169
    :cond_2
    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_3

    .line 170
    iget-object v1, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    .line 172
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bs;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget v6, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/maps/b/bs;->a:I

    .line 63
    iput-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 159
    :catch_1
    move-exception v0

    .line 160
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 161
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/bs;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/maps/b/bs;->d:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 73
    invoke-static {v0}, Lcom/google/maps/b/bv;->a(I)Lcom/google/maps/b/bv;

    move-result-object v6

    .line 74
    if-nez v6, :cond_5

    .line 75
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 77
    :cond_5
    iget v6, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/maps/b/bs;->a:I

    .line 78
    iput v0, p0, Lcom/google/maps/b/bs;->f:I

    goto/16 :goto_0

    .line 83
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 84
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/b/bs;->a:I

    goto/16 :goto_0

    .line 88
    :sswitch_6
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v10, :cond_6

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    .line 90
    or-int/lit16 v1, v1, 0x80

    .line 92
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 96
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 97
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 98
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v10, :cond_7

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_8

    move v0, v2

    :goto_2
    if-lez v0, :cond_7

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    .line 100
    or-int/lit16 v1, v1, 0x80

    .line 102
    :cond_7
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_9

    move v0, v2

    :goto_4
    if-lez v0, :cond_a

    .line 103
    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 98
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 102
    :cond_9
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 105
    :cond_a
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 109
    :sswitch_8
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v11, :cond_b

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    .line 111
    or-int/lit16 v1, v1, 0x100

    .line 113
    :cond_b
    iget-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 117
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 118
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 119
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v11, :cond_c

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_d

    move v0, v2

    :goto_5
    if-lez v0, :cond_c

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    .line 121
    or-int/lit16 v1, v1, 0x100

    .line 123
    :cond_c
    :goto_6
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v12, :cond_e

    move v0, v2

    :goto_7
    if-lez v0, :cond_f

    .line 124
    iget-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 119
    :cond_d
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_5

    .line 123
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_7

    .line 126
    :cond_f
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 130
    :sswitch_a
    and-int/lit8 v0, v1, 0x20

    const/16 v6, 0x20

    if-eq v0, v6, :cond_10

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    .line 133
    or-int/lit8 v1, v1, 0x20

    .line 135
    :cond_10
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 136
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 135
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 140
    :sswitch_b
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/b/bs;->a:I

    .line 141
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/bs;->k:I

    goto/16 :goto_0

    .line 145
    :sswitch_c
    iget-object v0, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 146
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/b/bs;->a:I

    goto/16 :goto_0

    .line 150
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 151
    iget v6, p0, Lcom/google/maps/b/bs;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/maps/b/bs;->a:I

    .line 152
    iput-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 163
    :cond_11
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v10, :cond_12

    .line 164
    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    .line 166
    :cond_12
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v11, :cond_13

    .line 167
    iget-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    .line 169
    :cond_13
    and-int/lit8 v0, v1, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_14

    .line 170
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    .line 172
    :cond_14
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->au:Lcom/google/n/bn;

    .line 173
    return-void

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x3a -> :sswitch_5
        0x4a -> :sswitch_7
        0x4d -> :sswitch_6
        0x52 -> :sswitch_9
        0x55 -> :sswitch_8
        0x62 -> :sswitch_a
        0x68 -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 781
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    .line 856
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    .line 972
    iput-byte v1, p0, Lcom/google/maps/b/bs;->n:B

    .line 1033
    iput v1, p0, Lcom/google/maps/b/bs;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/bs;
    .locals 1

    .prologue
    .line 2074
    sget-object v0, Lcom/google/maps/b/bs;->m:Lcom/google/maps/b/bs;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/bu;
    .locals 1

    .prologue
    .line 1156
    new-instance v0, Lcom/google/maps/b/bu;

    invoke-direct {v0}, Lcom/google/maps/b/bu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    sget-object v0, Lcom/google/maps/b/bs;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x5

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 996
    invoke-virtual {p0}, Lcom/google/maps/b/bs;->c()I

    .line 997
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 998
    iget-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1000
    :cond_0
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1001
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1003
    :cond_1
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 1004
    iget-boolean v0, p0, Lcom/google/maps/b/bs;->d:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1006
    :cond_2
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 1007
    iget v0, p0, Lcom/google/maps/b/bs;->f:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1009
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 1010
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_4
    move v1, v2

    .line 1012
    :goto_4
    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 1013
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1012
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 998
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1001
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 1004
    goto :goto_2

    .line 1007
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    :cond_9
    move v1, v2

    .line 1015
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 1016
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1015
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_a
    move v1, v2

    .line 1018
    :goto_6
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 1019
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1018
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1021
    :cond_b
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 1022
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/b/bs;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1024
    :cond_c
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    .line 1025
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1027
    :cond_d
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_e

    .line 1028
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1030
    :cond_e
    iget-object v0, p0, Lcom/google/maps/b/bs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1031
    return-void

    .line 1028
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_7
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 974
    iget-byte v0, p0, Lcom/google/maps/b/bs;->n:B

    .line 975
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 991
    :cond_0
    :goto_0
    return v2

    .line 976
    :cond_1
    if-eqz v0, :cond_0

    .line 978
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 979
    iget-object v0, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/c/a/a/b;->d()Lcom/google/c/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b;

    invoke-virtual {v0}, Lcom/google/c/a/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 980
    iput-byte v2, p0, Lcom/google/maps/b/bs;->n:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 978
    goto :goto_1

    :cond_3
    move v1, v2

    .line 984
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 985
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/dc;->d()Lcom/google/maps/b/dc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/dc;

    invoke-virtual {v0}, Lcom/google/maps/b/dc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 986
    iput-byte v2, p0, Lcom/google/maps/b/bs;->n:B

    goto :goto_0

    .line 984
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 990
    :cond_5
    iput-byte v3, p0, Lcom/google/maps/b/bs;->n:B

    move v2, v3

    .line 991
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 1035
    iget v0, p0, Lcom/google/maps/b/bs;->o:I

    .line 1036
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1089
    :goto_0
    return v0

    .line 1039
    :cond_0
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    .line 1041
    iget-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1043
    :goto_2
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1044
    const/4 v3, 0x3

    .line 1045
    iget-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1047
    :cond_1
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1048
    iget-boolean v0, p0, Lcom/google/maps/b/bs;->d:Z

    .line 1049
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1051
    :cond_2
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 1052
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/maps/b/bs;->f:I

    .line 1053
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1055
    :cond_3
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 1056
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/maps/b/bs;->h:Lcom/google/n/ao;

    .line 1057
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1060
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 1062
    add-int/2addr v0, v1

    .line 1063
    iget-object v1, p0, Lcom/google/maps/b/bs;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1066
    iget-object v1, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 1068
    add-int/2addr v0, v1

    .line 1069
    iget-object v1, p0, Lcom/google/maps/b/bs;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v2

    move v3, v0

    .line 1071
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1072
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/maps/b/bs;->g:Ljava/util/List;

    .line 1073
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1071
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1041
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1045
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 1053
    :cond_7
    const/16 v0, 0xa

    goto :goto_4

    .line 1075
    :cond_8
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 1076
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/b/bs;->k:I

    .line 1077
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1079
    :cond_9
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    .line 1080
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/maps/b/bs;->e:Lcom/google/n/ao;

    .line 1081
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1083
    :cond_a
    iget v0, p0, Lcom/google/maps/b/bs;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 1084
    const/16 v1, 0xf

    .line 1085
    iget-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bs;->l:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1087
    :cond_b
    iget-object v0, p0, Lcom/google/maps/b/bs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1088
    iput v0, p0, Lcom/google/maps/b/bs;->o:I

    goto/16 :goto_0

    .line 1085
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bs;->newBuilder()Lcom/google/maps/b/bu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/bu;->a(Lcom/google/maps/b/bs;)Lcom/google/maps/b/bu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bs;->newBuilder()Lcom/google/maps/b/bu;

    move-result-object v0

    return-object v0
.end method
