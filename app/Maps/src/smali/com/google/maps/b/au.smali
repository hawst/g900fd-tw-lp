.class public final Lcom/google/maps/b/au;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/ba;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/au;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/b/au;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/google/maps/b/av;

    invoke-direct {v0}, Lcom/google/maps/b/av;-><init>()V

    sput-object v0, Lcom/google/maps/b/au;->PARSER:Lcom/google/n/ax;

    .line 322
    new-instance v0, Lcom/google/maps/b/aw;

    invoke-direct {v0}, Lcom/google/maps/b/aw;-><init>()V

    .line 473
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/au;->h:Lcom/google/n/aw;

    .line 1157
    new-instance v0, Lcom/google/maps/b/au;

    invoke-direct {v0}, Lcom/google/maps/b/au;-><init>()V

    sput-object v0, Lcom/google/maps/b/au;->e:Lcom/google/maps/b/au;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 393
    iput-byte v0, p0, Lcom/google/maps/b/au;->f:B

    .line 439
    iput v0, p0, Lcom/google/maps/b/au;->g:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v3, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/maps/b/au;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 34
    :cond_0
    :goto_0
    if-nez v2, :cond_b

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 36
    sparse-switch v1, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 43
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_12

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 51
    or-int/lit8 v1, v0, 0x1

    .line 53
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 53
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 55
    goto :goto_0

    .line 58
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_11

    .line 59
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 61
    or-int/lit8 v1, v0, 0x2

    .line 63
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 63
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 69
    invoke-static {v5}, Lcom/google/maps/b/ay;->a(I)Lcom/google/maps/b/ay;

    move-result-object v1

    .line 70
    if-nez v1, :cond_5

    .line 71
    const/4 v1, 0x3

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 113
    :goto_3
    :try_start_5
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    :catchall_0
    move-exception v0

    :goto_4
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_1

    .line 119
    iget-object v2, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    .line 121
    :cond_1
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_2

    .line 122
    iget-object v2, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    .line 124
    :cond_2
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_3

    .line 125
    iget-object v2, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    .line 127
    :cond_3
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v9, :cond_4

    .line 128
    iget-object v1, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    .line 130
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/au;->au:Lcom/google/n/bn;

    throw v0

    .line 73
    :cond_5
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_10

    .line 74
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 75
    or-int/lit8 v1, v0, 0x4

    .line 77
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 79
    goto/16 :goto_0

    .line 82
    :sswitch_4
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 83
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v5

    move v1, v0

    .line 84
    :goto_6
    :try_start_9
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_6

    const/4 v0, -0x1

    :goto_7
    if-lez v0, :cond_9

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 86
    invoke-static {v0}, Lcom/google/maps/b/ay;->a(I)Lcom/google/maps/b/ay;

    move-result-object v6

    .line 87
    if-nez v6, :cond_7

    .line 88
    const/4 v6, 0x3

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_6

    .line 112
    :catch_1
    move-exception v0

    goto :goto_3

    .line 84
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_7

    .line 90
    :cond_7
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v7, :cond_8

    .line 91
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    .line 92
    or-int/lit8 v1, v1, 0x4

    .line 94
    :cond_8
    iget-object v6, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    .line 114
    :catch_2
    move-exception v0

    .line 115
    :goto_8
    :try_start_a
    new-instance v2, Lcom/google/n/ak;

    .line 116
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 97
    :cond_9
    :try_start_b
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 98
    goto/16 :goto_0

    .line 101
    :sswitch_5
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v9, :cond_a

    .line 102
    :try_start_c
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    .line 104
    or-int/lit8 v0, v0, 0x8

    .line 106
    :cond_a
    iget-object v1, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 106
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 114
    :catch_3
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_8

    .line 118
    :cond_b
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_c

    .line 119
    iget-object v1, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    .line 121
    :cond_c
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v8, :cond_d

    .line 122
    iget-object v1, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    .line 124
    :cond_d
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v7, :cond_e

    .line 125
    iget-object v1, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    .line 127
    :cond_e
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v9, :cond_f

    .line 128
    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    .line 130
    :cond_f
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/au;->au:Lcom/google/n/bn;

    .line 131
    return-void

    .line 118
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_4

    :cond_10
    move v1, v0

    goto/16 :goto_5

    :cond_11
    move v1, v0

    goto/16 :goto_2

    :cond_12
    move v1, v0

    goto/16 :goto_1

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 393
    iput-byte v0, p0, Lcom/google/maps/b/au;->f:B

    .line 439
    iput v0, p0, Lcom/google/maps/b/au;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/au;
    .locals 1

    .prologue
    .line 1160
    sget-object v0, Lcom/google/maps/b/au;->e:Lcom/google/maps/b/au;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/ax;
    .locals 1

    .prologue
    .line 535
    new-instance v0, Lcom/google/maps/b/ax;

    invoke-direct {v0}, Lcom/google/maps/b/ax;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/google/maps/b/au;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 423
    invoke-virtual {p0}, Lcom/google/maps/b/au;->c()I

    move v1, v2

    .line 424
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 425
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 424
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 427
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 427
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 430
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 431
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 430
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 431
    :cond_2
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 433
    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 434
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 433
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 436
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/au;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 437
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 395
    iget-byte v0, p0, Lcom/google/maps/b/au;->f:B

    .line 396
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 418
    :cond_0
    :goto_0
    return v2

    .line 397
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 399
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 400
    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ao;->d()Lcom/google/maps/b/ao;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ao;

    invoke-virtual {v0}, Lcom/google/maps/b/ao;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 401
    iput-byte v2, p0, Lcom/google/maps/b/au;->f:B

    goto :goto_0

    .line 399
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 405
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 406
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bb;->d()Lcom/google/maps/b/bb;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bb;

    invoke-virtual {v0}, Lcom/google/maps/b/bb;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 407
    iput-byte v2, p0, Lcom/google/maps/b/au;->f:B

    goto :goto_0

    .line 405
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 411
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 412
    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ak;->d()Lcom/google/maps/b/ak;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ak;

    invoke-virtual {v0}, Lcom/google/maps/b/ak;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 413
    iput-byte v2, p0, Lcom/google/maps/b/au;->f:B

    goto :goto_0

    .line 411
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 417
    :cond_7
    iput-byte v3, p0, Lcom/google/maps/b/au;->f:B

    move v2, v3

    .line 418
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 441
    iget v0, p0, Lcom/google/maps/b/au;->g:I

    .line 442
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 468
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 445
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 446
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    .line 447
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 445
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 449
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 450
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    .line 451
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 449
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v2

    move v4, v2

    .line 455
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 456
    iget-object v0, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    .line 457
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v4, v0

    .line 455
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 457
    :cond_3
    const/16 v0, 0xa

    goto :goto_4

    .line 459
    :cond_4
    add-int v0, v3, v4

    .line 460
    iget-object v1, p0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v2

    move v3, v0

    .line 462
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 463
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    .line 464
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 462
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 466
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/au;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 467
    iput v0, p0, Lcom/google/maps/b/au;->g:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/au;->newBuilder()Lcom/google/maps/b/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/ax;->a(Lcom/google/maps/b/au;)Lcom/google/maps/b/ax;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/au;->newBuilder()Lcom/google/maps/b/ax;

    move-result-object v0

    return-object v0
.end method
