.class public final Lcom/google/maps/b/ca;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/cb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/by;",
        "Lcom/google/maps/b/ca;",
        ">;",
        "Lcom/google/maps/b/cb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/google/maps/b/by;->l:Lcom/google/maps/b/by;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 706
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->b:Lcom/google/n/ao;

    .line 765
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->c:Lcom/google/n/ao;

    .line 824
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->d:Lcom/google/n/ao;

    .line 883
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->e:Lcom/google/n/ao;

    .line 942
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->f:Lcom/google/n/ao;

    .line 1001
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ca;->g:Ljava/lang/Object;

    .line 1077
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->h:Lcom/google/n/ao;

    .line 1136
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->i:Lcom/google/n/ao;

    .line 1195
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->j:Lcom/google/n/ao;

    .line 1254
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ca;->k:Lcom/google/n/ao;

    .line 527
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/by;)Lcom/google/maps/b/ca;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 624
    invoke-static {}, Lcom/google/maps/b/by;->d()Lcom/google/maps/b/by;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 667
    :goto_0
    return-object p0

    .line 625
    :cond_0
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 626
    iget-object v2, p0, Lcom/google/maps/b/ca;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 627
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 629
    :cond_1
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 630
    iget-object v2, p0, Lcom/google/maps/b/ca;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 631
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 633
    :cond_2
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 634
    iget-object v2, p0, Lcom/google/maps/b/ca;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 635
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 637
    :cond_3
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 638
    iget-object v2, p0, Lcom/google/maps/b/ca;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 639
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 641
    :cond_4
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 642
    iget-object v2, p0, Lcom/google/maps/b/ca;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 643
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 645
    :cond_5
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 646
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 647
    iget-object v2, p1, Lcom/google/maps/b/by;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ca;->g:Ljava/lang/Object;

    .line 650
    :cond_6
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 651
    iget-object v2, p0, Lcom/google/maps/b/ca;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 652
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 654
    :cond_7
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 655
    iget-object v2, p0, Lcom/google/maps/b/ca;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 656
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 658
    :cond_8
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 659
    iget-object v2, p0, Lcom/google/maps/b/ca;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/by;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 660
    iget v2, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/b/ca;->a:I

    .line 662
    :cond_9
    iget v2, p1, Lcom/google/maps/b/by;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_a

    .line 663
    iget-object v0, p0, Lcom/google/maps/b/ca;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/b/by;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 664
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/b/ca;->a:I

    .line 666
    :cond_a
    iget-object v0, p1, Lcom/google/maps/b/by;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 625
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 629
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 633
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 637
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 641
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 645
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 650
    goto :goto_7

    :cond_12
    move v2, v1

    .line 654
    goto :goto_8

    :cond_13
    move v2, v1

    .line 658
    goto :goto_9

    :cond_14
    move v0, v1

    .line 662
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 518
    new-instance v2, Lcom/google/maps/b/by;

    invoke-direct {v2, p0}, Lcom/google/maps/b/by;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/by;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/b/by;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/maps/b/by;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/b/by;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/b/by;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/maps/b/ca;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/b/by;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/maps/b/by;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/maps/b/by;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/b/by;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ca;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ca;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v3, v2, Lcom/google/maps/b/by;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/b/ca;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/b/ca;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/b/by;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 518
    check-cast p1, Lcom/google/maps/b/by;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/ca;->a(Lcom/google/maps/b/by;)Lcom/google/maps/b/ca;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 671
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 672
    iget-object v0, p0, Lcom/google/maps/b/ca;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bj;

    invoke-virtual {v0}, Lcom/google/maps/b/bj;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 701
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 671
    goto :goto_0

    .line 677
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 678
    iget-object v0, p0, Lcom/google/maps/b/ca;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bj;

    invoke-virtual {v0}, Lcom/google/maps/b/bj;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 680
    goto :goto_1

    :cond_2
    move v0, v1

    .line 677
    goto :goto_2

    .line 683
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 684
    iget-object v0, p0, Lcom/google/maps/b/ca;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bj;

    invoke-virtual {v0}, Lcom/google/maps/b/bj;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 686
    goto :goto_1

    :cond_4
    move v0, v1

    .line 683
    goto :goto_3

    .line 689
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 690
    iget-object v0, p0, Lcom/google/maps/b/ca;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bj;

    invoke-virtual {v0}, Lcom/google/maps/b/bj;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 692
    goto :goto_1

    :cond_6
    move v0, v1

    .line 689
    goto :goto_4

    .line 695
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ca;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 696
    iget-object v0, p0, Lcom/google/maps/b/ca;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bj;->d()Lcom/google/maps/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bj;

    invoke-virtual {v0}, Lcom/google/maps/b/bj;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 698
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 695
    goto :goto_5

    :cond_9
    move v0, v2

    .line 701
    goto/16 :goto_1
.end method
