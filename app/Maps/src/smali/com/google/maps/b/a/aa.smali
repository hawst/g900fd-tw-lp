.class public Lcom/google/maps/b/a/aa;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Lcom/google/maps/b/a/ad;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 8
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    .line 10
    new-instance v0, Lcom/google/maps/b/a/ad;

    invoke-direct {v0}, Lcom/google/maps/b/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/ad;
    .locals 4

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 14
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ad;->c()V

    .line 15
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    .line 18
    :goto_0
    return-object v0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    iget-object v1, p0, Lcom/google/maps/b/a/aa;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/ad;->a([BII)V

    .line 18
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    goto :goto_0
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 34
    packed-switch p1, :pswitch_data_0

    .line 40
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 36
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final c()V
    .locals 2

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 27
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 28
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->b:Lcom/google/maps/b/a/ad;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ad;->c()V

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/aa;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p0}, Lcom/google/maps/b/a/aa;->a()Lcom/google/maps/b/a/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/ad;->toString()Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 49
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "primary_occupant_poi {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
