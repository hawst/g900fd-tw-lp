.class public final enum Lcom/google/maps/b/j;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/j;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/j;

.field public static final enum b:Lcom/google/maps/b/j;

.field public static final enum c:Lcom/google/maps/b/j;

.field public static final enum d:Lcom/google/maps/b/j;

.field public static final enum e:Lcom/google/maps/b/j;

.field public static final enum f:Lcom/google/maps/b/j;

.field private static final synthetic h:[Lcom/google/maps/b/j;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 295
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->a:Lcom/google/maps/b/j;

    .line 299
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_TINY"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->b:Lcom/google/maps/b/j;

    .line 303
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_SMALL"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->c:Lcom/google/maps/b/j;

    .line 307
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_MEDIUM"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->d:Lcom/google/maps/b/j;

    .line 311
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_LARGE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->e:Lcom/google/maps/b/j;

    .line 315
    new-instance v0, Lcom/google/maps/b/j;

    const-string v1, "PROMINENCE_XLARGE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/j;->f:Lcom/google/maps/b/j;

    .line 290
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/maps/b/j;

    sget-object v1, Lcom/google/maps/b/j;->a:Lcom/google/maps/b/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/j;->b:Lcom/google/maps/b/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/j;->c:Lcom/google/maps/b/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/j;->d:Lcom/google/maps/b/j;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/j;->e:Lcom/google/maps/b/j;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/j;->f:Lcom/google/maps/b/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/j;->h:[Lcom/google/maps/b/j;

    .line 365
    new-instance v0, Lcom/google/maps/b/k;

    invoke-direct {v0}, Lcom/google/maps/b/k;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 374
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 375
    iput p3, p0, Lcom/google/maps/b/j;->g:I

    .line 376
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/j;
    .locals 1

    .prologue
    .line 349
    packed-switch p0, :pswitch_data_0

    .line 356
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 350
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/j;->a:Lcom/google/maps/b/j;

    goto :goto_0

    .line 351
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/j;->b:Lcom/google/maps/b/j;

    goto :goto_0

    .line 352
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/j;->c:Lcom/google/maps/b/j;

    goto :goto_0

    .line 353
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/j;->d:Lcom/google/maps/b/j;

    goto :goto_0

    .line 354
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/j;->e:Lcom/google/maps/b/j;

    goto :goto_0

    .line 355
    :pswitch_5
    sget-object v0, Lcom/google/maps/b/j;->f:Lcom/google/maps/b/j;

    goto :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/j;
    .locals 1

    .prologue
    .line 290
    const-class v0, Lcom/google/maps/b/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/j;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/j;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/google/maps/b/j;->h:[Lcom/google/maps/b/j;

    invoke-virtual {v0}, [Lcom/google/maps/b/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 345
    iget v0, p0, Lcom/google/maps/b/j;->g:I

    return v0
.end method
