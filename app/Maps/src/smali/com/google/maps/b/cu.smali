.class public final Lcom/google/maps/b/cu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/cv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/cs;",
        "Lcom/google/maps/b/cu;",
        ">;",
        "Lcom/google/maps/b/cv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/google/maps/b/cs;->e:Lcom/google/maps/b/cs;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 353
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cu;->b:Lcom/google/n/ao;

    .line 412
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/cu;->c:Lcom/google/n/ao;

    .line 278
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/cs;)Lcom/google/maps/b/cu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 319
    invoke-static {}, Lcom/google/maps/b/cs;->d()Lcom/google/maps/b/cs;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 332
    :goto_0
    return-object p0

    .line 320
    :cond_0
    iget v2, p1, Lcom/google/maps/b/cs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 321
    iget-object v2, p0, Lcom/google/maps/b/cu;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/cs;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 322
    iget v2, p0, Lcom/google/maps/b/cu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/cu;->a:I

    .line 324
    :cond_1
    iget v2, p1, Lcom/google/maps/b/cs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 325
    iget-object v2, p0, Lcom/google/maps/b/cu;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/cs;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 326
    iget v2, p0, Lcom/google/maps/b/cu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/cu;->a:I

    .line 328
    :cond_2
    iget v2, p1, Lcom/google/maps/b/cs;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 329
    iget v0, p1, Lcom/google/maps/b/cs;->d:I

    iget v1, p0, Lcom/google/maps/b/cu;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/maps/b/cu;->a:I

    iput v0, p0, Lcom/google/maps/b/cu;->d:I

    .line 331
    :cond_3
    iget-object v0, p1, Lcom/google/maps/b/cs;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 320
    goto :goto_1

    :cond_5
    move v2, v1

    .line 324
    goto :goto_2

    :cond_6
    move v0, v1

    .line 328
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 269
    new-instance v2, Lcom/google/maps/b/cs;

    invoke-direct {v2, p0}, Lcom/google/maps/b/cs;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/cu;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/cs;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cu;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cu;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/maps/b/cs;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/cu;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/cu;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/b/cu;->d:I

    iput v1, v2, Lcom/google/maps/b/cs;->d:I

    iput v0, v2, Lcom/google/maps/b/cs;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 269
    check-cast p1, Lcom/google/maps/b/cs;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/cu;->a(Lcom/google/maps/b/cs;)Lcom/google/maps/b/cu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 336
    iget v0, p0, Lcom/google/maps/b/cu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/google/maps/b/cu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ck;->d()Lcom/google/maps/b/ck;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/ck;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 348
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 336
    goto :goto_0

    .line 342
    :cond_1
    iget v0, p0, Lcom/google/maps/b/cu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 343
    iget-object v0, p0, Lcom/google/maps/b/cu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ck;->d()Lcom/google/maps/b/ck;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/ck;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 345
    goto :goto_1

    :cond_2
    move v0, v1

    .line 342
    goto :goto_2

    :cond_3
    move v0, v2

    .line 348
    goto :goto_1
.end method
