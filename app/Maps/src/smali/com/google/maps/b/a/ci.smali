.class public Lcom/google/maps/b/a/ci;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cy;

.field public c:Lcom/google/maps/b/a/cy;

.field public d:Lcom/google/maps/b/a/cy;

.field private e:Lcom/google/maps/b/a/cr;

.field private f:Lcom/google/maps/b/a/cu;

.field private g:Lcom/google/maps/b/a/cr;

.field private h:Lcom/google/maps/b/a/cu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    .line 30
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    .line 34
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    .line 36
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    .line 38
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/ci;->t:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v2, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 43
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    .line 46
    :goto_0
    return-object v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/ci;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v3, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 118
    packed-switch p1, :pswitch_data_0

    .line 124
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 120
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 129
    packed-switch p1, :pswitch_data_0

    .line 147
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 131
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 134
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 137
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ci;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 140
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ci;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 143
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/ci;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ci;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 105
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 106
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->f:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 107
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 108
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 109
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 110
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 111
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 112
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 114
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/google/maps/b/a/ci;->a()Lcom/google/maps/b/a/cu;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "vertex_data: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "vertex_count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    iget-object v3, p0, Lcom/google/maps/b/a/ci;->t:[B

    iput-object v3, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v1, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v1, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "vertex_normal_data: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v0, v1

    .line 162
    :goto_1
    iget-object v3, p0, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_4

    .line 163
    iget-object v3, p0, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x21

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "triangle_strip_data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    iget-object v3, p0, Lcom/google/maps/b/a/ci;->t:[B

    iget-object v4, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->a:I

    iget-object v5, p0, Lcom/google/maps/b/a/ci;->g:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->b:I

    iput-object v3, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v4, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v5, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/ci;->h:Lcom/google/maps/b/a/cu;

    goto :goto_0

    :cond_4
    move v0, v1

    .line 165
    :goto_2
    iget-object v3, p0, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_5

    .line 166
    iget-object v3, p0, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x22

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "triangle_strip_break: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 168
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    iget v0, v0, Lcom/google/maps/b/a/cy;->b:I

    if-ge v1, v0, :cond_6

    .line 169
    iget-object v0, p0, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    iget-object v0, v0, Lcom/google/maps/b/a/cy;->a:[I

    aget v0, v0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "edge_index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 172
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
