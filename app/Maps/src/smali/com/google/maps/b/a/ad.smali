.class public Lcom/google/maps/b/a/ad;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Lcom/google/maps/b/a/s;

.field private c:Lcom/google/maps/b/a/cr;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/maps/b/a/cr;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/maps/b/a/cr;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/s;

    invoke-direct {v0}, Lcom/google/maps/b/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    .line 20
    iput-object v1, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    .line 24
    iput-object v1, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    .line 28
    iput-object v1, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 59
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ad;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ad;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    .line 64
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    return-object v0

    .line 61
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 100
    packed-switch p1, :pswitch_data_0

    .line 115
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 102
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 108
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 111
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 73
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ad;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ad;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    .line 78
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    return-object v0

    .line 75
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 87
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 88
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    invoke-virtual {v0}, Lcom/google/maps/b/a/s;->c()V

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 90
    iput-object v2, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 92
    iput-object v2, p0, Lcom/google/maps/b/a/ad;->f:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 94
    iput-object v2, p0, Lcom/google/maps/b/a/ad;->h:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    invoke-virtual {v0}, Lcom/google/maps/b/a/s;->c()V

    iget-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/b/a/s;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 124
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "entity {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/maps/b/a/ad;->t:[B

    iget-object v3, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->a:I

    iget-object v4, p0, Lcom/google/maps/b/a/ad;->c:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->b:I

    sget-object v5, Lcom/google/maps/b/a/ad;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "level_id: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_4

    .line 133
    invoke-virtual {p0}, Lcom/google/maps/b/a/ad;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "feature_id: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_5

    .line 136
    invoke-virtual {p0}, Lcom/google/maps/b/a/ad;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "name: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 122
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    iget-object v2, p0, Lcom/google/maps/b/a/ad;->t:[B

    iget-object v3, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->a:I

    iget-object v4, p0, Lcom/google/maps/b/a/ad;->a:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/maps/b/a/s;->a([BII)V

    iget-object v0, p0, Lcom/google/maps/b/a/ad;->b:Lcom/google/maps/b/a/s;

    goto/16 :goto_0

    .line 130
    :cond_7
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ad;->d:Ljava/lang/String;

    goto/16 :goto_1
.end method
