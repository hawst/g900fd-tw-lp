.class public Lcom/google/maps/b/a/bg;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/db;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cs;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/maps/b/a/aj;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 17
    new-instance v0, Lcom/google/maps/b/a/db;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    .line 19
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    .line 21
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    .line 27
    new-instance v0, Lcom/google/maps/b/a/aj;

    invoke-direct {v0}, Lcom/google/maps/b/a/aj;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->f:Lcom/google/maps/b/a/aj;

    .line 59
    new-instance v0, Lcom/google/maps/b/a/bh;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/bh;-><init>(Lcom/google/maps/b/a/bg;)V

    return-void
.end method


# virtual methods
.method protected final a(I)Z
    .locals 1

    .prologue
    .line 98
    packed-switch p1, :pswitch_data_0

    .line 103
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 101
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 109
    packed-switch p1, :pswitch_data_0

    .line 115
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 111
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 131
    packed-switch p1, :pswitch_data_0

    .line 140
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 133
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 136
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(IJ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 120
    packed-switch p1, :pswitch_data_0

    .line 126
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 122
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/aj;
    .locals 4

    .prologue
    .line 80
    iget-object v1, p0, Lcom/google/maps/b/a/bg;->f:Lcom/google/maps/b/a/aj;

    iget-object v2, p0, Lcom/google/maps/b/a/bg;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/aj;->a([BII)V

    .line 81
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->f:Lcom/google/maps/b/a/aj;

    return-object v0
.end method

.method final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 86
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 87
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v4, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 88
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    iput v4, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 92
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->f:Lcom/google/maps/b/a/aj;

    invoke-virtual {v0}, Lcom/google/maps/b/a/aj;->c()V

    .line 94
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 145
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    iget-boolean v0, v0, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    iget-wide v4, v0, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "id: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "L\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0xe

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "global: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/maps/b/a/bg;->t:[B

    iget-object v4, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->a:I

    iget-object v5, p0, Lcom/google/maps/b/a/bg;->d:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->b:I

    sget-object v6, Lcom/google/maps/b/a/bg;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v4, v5, v6}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "config_set: \""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move v0, v1

    .line 155
    :goto_2
    iget-object v2, p0, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_7

    .line 156
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/bg;->b(I)Lcom/google/maps/b/a/aj;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/aj;->toString()Ljava/lang/String;

    move-result-object v2

    .line 157
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 158
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 159
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 161
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xf

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "key_zoom {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 150
    goto/16 :goto_0

    .line 153
    :cond_6
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/bg;->e:Ljava/lang/String;

    goto/16 :goto_1

    .line 164
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
