.class public final Lcom/google/maps/b/bj;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/br;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/maps/b/bj;",
        ">;",
        "Lcom/google/maps/b/br;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bj;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final w:Lcom/google/maps/b/bj;

.field private static volatile z:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field e:J

.field f:Lcom/google/n/ao;

.field g:I

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field o:Ljava/lang/Object;

.field p:J

.field q:I

.field r:Z

.field s:Z

.field t:Z

.field u:Lcom/google/n/f;

.field v:Z

.field private x:B

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 237
    new-instance v0, Lcom/google/maps/b/bk;

    invoke-direct {v0}, Lcom/google/maps/b/bk;-><init>()V

    sput-object v0, Lcom/google/maps/b/bj;->PARSER:Lcom/google/n/ax;

    .line 575
    new-instance v0, Lcom/google/maps/b/bl;

    invoke-direct {v0}, Lcom/google/maps/b/bl;-><init>()V

    .line 1054
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/bj;->z:Lcom/google/n/aw;

    .line 2621
    new-instance v0, Lcom/google/maps/b/bj;

    invoke-direct {v0}, Lcom/google/maps/b/bj;-><init>()V

    sput-object v0, Lcom/google/maps/b/bj;->w:Lcom/google/maps/b/bj;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    .line 541
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    .line 604
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    .line 620
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    .line 636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    .line 652
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    .line 668
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    .line 684
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    .line 846
    iput-byte v4, p0, Lcom/google/maps/b/bj;->x:B

    .line 951
    iput v4, p0, Lcom/google/maps/b/bj;->y:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    .line 21
    iget-object v0, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput-wide v6, p0, Lcom/google/maps/b/bj;->e:J

    .line 23
    iget-object v0, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput v3, p0, Lcom/google/maps/b/bj;->g:I

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    .line 26
    iget-object v0, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iget-object v0, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iget-object v0, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iget-object v0, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    .line 33
    iput-wide v6, p0, Lcom/google/maps/b/bj;->p:J

    .line 34
    iput v3, p0, Lcom/google/maps/b/bj;->q:I

    .line 35
    iput-boolean v3, p0, Lcom/google/maps/b/bj;->r:Z

    .line 36
    iput-boolean v3, p0, Lcom/google/maps/b/bj;->s:Z

    .line 37
    iput-boolean v3, p0, Lcom/google/maps/b/bj;->t:Z

    .line 38
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    .line 39
    iput-boolean v3, p0, Lcom/google/maps/b/bj;->v:Z

    .line 40
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/maps/b/bj;-><init>()V

    .line 47
    const/4 v6, 0x0

    .line 49
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 51
    const/4 v0, 0x0

    move v7, v0

    .line 52
    :cond_0
    :goto_0
    if-nez v7, :cond_f

    .line 53
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 54
    sparse-switch v5, :sswitch_data_0

    .line 59
    iget-object v0, p0, Lcom/google/maps/b/bj;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/maps/b/bj;->w:Lcom/google/maps/b/bj;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/maps/b/bj;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x1

    move v7, v0

    goto :goto_0

    .line 56
    :sswitch_0
    const/4 v0, 0x1

    move v7, v0

    .line 57
    goto :goto_0

    .line 67
    :sswitch_1
    and-int/lit8 v0, v6, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 70
    or-int/lit8 v1, v6, 0x1

    .line 72
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 72
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v6, v1

    .line 74
    goto :goto_0

    .line 77
    :sswitch_2
    and-int/lit8 v0, v6, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_15

    .line 78
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80
    or-int/lit8 v1, v6, 0x2

    .line 82
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 82
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v6, v1

    .line 84
    goto :goto_0

    .line 87
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 88
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/b/bj;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    move v1, v6

    .line 219
    :goto_3
    :try_start_5
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 224
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_4
    and-int/lit8 v1, v6, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 225
    iget-object v1, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    .line 227
    :cond_1
    and-int/lit8 v1, v6, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 228
    iget-object v1, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    .line 230
    :cond_2
    and-int/lit8 v1, v6, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_3

    .line 231
    iget-object v1, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    .line 233
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bj;->au:Lcom/google/n/bn;

    .line 234
    iget-object v1, p0, Lcom/google/maps/b/bj;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/q;->b:Z

    :cond_4
    throw v0

    .line 92
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 93
    invoke-static {v0}, Lcom/google/maps/b/bn;->a(I)Lcom/google/maps/b/bn;

    move-result-object v1

    .line 94
    if-nez v1, :cond_5

    .line 95
    const/4 v1, 0x4

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 220
    :catch_1
    move-exception v0

    .line 221
    :goto_5
    :try_start_7
    new-instance v1, Lcom/google/n/ak;

    .line 222
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 224
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 97
    :cond_5
    :try_start_8
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/maps/b/bj;->a:I

    .line 98
    iput v0, p0, Lcom/google/maps/b/bj;->g:I

    goto/16 :goto_0

    .line 103
    :sswitch_5
    iget-object v0, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 104
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_6
    iget-object v0, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 109
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/maps/b/bj;->a:I

    .line 115
    iput-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 119
    :sswitch_8
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/maps/b/bj;->p:J

    goto/16 :goto_0

    .line 124
    :sswitch_9
    iget-object v0, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 129
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 130
    invoke-static {v0}, Lcom/google/maps/b/bp;->a(I)Lcom/google/maps/b/bp;

    move-result-object v1

    .line 131
    if-nez v1, :cond_6

    .line 132
    const/16 v1, 0xa

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 134
    :cond_6
    and-int/lit8 v1, v6, 0x40

    const/16 v2, 0x40

    if-eq v1, v2, :cond_14

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 136
    or-int/lit8 v1, v6, 0x40

    .line 138
    :goto_6
    :try_start_9
    iget-object v2, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v6, v1

    .line 140
    goto/16 :goto_0

    .line 143
    :sswitch_b
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 144
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v2

    move v1, v6

    .line 145
    :goto_7
    :try_start_b
    iget v0, p1, Lcom/google/n/j;->f:I

    const v4, 0x7fffffff

    if-ne v0, v4, :cond_7

    const/4 v0, -0x1

    :goto_8
    if-lez v0, :cond_a

    .line 146
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 147
    invoke-static {v0}, Lcom/google/maps/b/bp;->a(I)Lcom/google/maps/b/bp;

    move-result-object v4

    .line 148
    if-nez v4, :cond_8

    .line 149
    const/16 v4, 0xa

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_7

    .line 218
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 145
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v4, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v4

    iget v4, p1, Lcom/google/n/j;->f:I

    sub-int v0, v4, v0

    goto :goto_8

    .line 151
    :cond_8
    and-int/lit8 v4, v1, 0x40

    const/16 v5, 0x40

    if-eq v4, v5, :cond_9

    .line 152
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    .line 153
    or-int/lit8 v1, v1, 0x40

    .line 155
    :cond_9
    iget-object v4, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 220
    :catch_3
    move-exception v0

    move v6, v1

    goto/16 :goto_5

    .line 158
    :cond_a
    iput v2, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v6, v1

    .line 159
    goto/16 :goto_0

    .line 162
    :sswitch_c
    :try_start_c
    iget-object v0, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 163
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 167
    :sswitch_d
    iget-object v0, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 168
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 172
    :sswitch_e
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 173
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/maps/b/bj;->q:I

    goto/16 :goto_0

    .line 177
    :sswitch_f
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 178
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lcom/google/maps/b/bj;->r:Z

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_9

    .line 182
    :sswitch_10
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 183
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Lcom/google/maps/b/bj;->s:Z

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_a

    .line 187
    :sswitch_11
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 188
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lcom/google/maps/b/bj;->t:Z

    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto :goto_b

    .line 192
    :sswitch_12
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 193
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    goto/16 :goto_0

    .line 197
    :sswitch_13
    iget-object v0, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 198
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 202
    :sswitch_14
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 203
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_c
    iput-boolean v0, p0, Lcom/google/maps/b/bj;->v:Z

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    .line 207
    :sswitch_15
    iget-object v0, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 208
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    goto/16 :goto_0

    .line 212
    :sswitch_16
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/b/bj;->a:I

    .line 213
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/maps/b/bj;->e:J
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 224
    :cond_f
    and-int/lit8 v0, v6, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    .line 225
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    .line 227
    :cond_10
    and-int/lit8 v0, v6, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 228
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    .line 230
    :cond_11
    and-int/lit8 v0, v6, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_12

    .line 231
    iget-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    .line 233
    :cond_12
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->au:Lcom/google/n/bn;

    .line 234
    iget-object v0, p0, Lcom/google/maps/b/bj;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_13

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/q;->b:Z

    .line 235
    :cond_13
    return-void

    :cond_14
    move v1, v6

    goto/16 :goto_6

    :cond_15
    move v1, v6

    goto/16 :goto_2

    :cond_16
    move v1, v6

    goto/16 :goto_1

    .line 54
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x68 -> :sswitch_e
        0x70 -> :sswitch_f
        0x78 -> :sswitch_10
        0x80 -> :sswitch_11
        0x8a -> :sswitch_12
        0x92 -> :sswitch_13
        0x98 -> :sswitch_14
        0xa2 -> :sswitch_15
        0xa8 -> :sswitch_16
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/maps/b/bj;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    .line 541
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    .line 604
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    .line 620
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    .line 636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    .line 652
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    .line 668
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    .line 684
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    .line 846
    iput-byte v1, p0, Lcom/google/maps/b/bj;->x:B

    .line 951
    iput v1, p0, Lcom/google/maps/b/bj;->y:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/maps/b/bj;
    .locals 1

    .prologue
    .line 2624
    sget-object v0, Lcom/google/maps/b/bj;->w:Lcom/google/maps/b/bj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/bm;
    .locals 1

    .prologue
    .line 1116
    new-instance v0, Lcom/google/maps/b/bm;

    invoke-direct {v0}, Lcom/google/maps/b/bm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    sget-object v0, Lcom/google/maps/b/bj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 10

    .prologue
    const/16 v6, 0x8

    const/4 v9, 0x4

    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v2, 0x0

    .line 880
    invoke-virtual {p0}, Lcom/google/maps/b/bj;->c()I

    .line 883
    new-instance v4, Lcom/google/n/y;

    invoke-direct {v4, p0, v2}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    move v1, v2

    .line 884
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v8}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 884
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 887
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v8}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 887
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 890
    :cond_1
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 891
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 893
    :cond_2
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 894
    iget v0, p0, Lcom/google/maps/b/bj;->g:I

    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 896
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 897
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 899
    :cond_4
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 900
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 902
    :cond_5
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_6

    .line 903
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v8}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 905
    :cond_6
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_7

    .line 906
    iget-wide v0, p0, Lcom/google/maps/b/bj;->p:J

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 908
    :cond_7
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 909
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_8
    move v1, v2

    .line 911
    :goto_4
    iget-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 912
    const/16 v5, 0xa

    iget-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 911
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 894
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 903
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 912
    :cond_b
    int-to-long v6, v0

    invoke-virtual {p1, v6, v7}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 914
    :cond_c
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 915
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 917
    :cond_d
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 918
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 920
    :cond_e
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_f

    .line 921
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/maps/b/bj;->q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_18

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 923
    :cond_f
    :goto_6
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_10

    .line 924
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/maps/b/bj;->r:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_19

    move v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 926
    :cond_10
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_11

    .line 927
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/maps/b/bj;->s:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1a

    move v0, v3

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 929
    :cond_11
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_12

    .line 930
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/maps/b/bj;->t:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1b

    move v0, v3

    :goto_9
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 932
    :cond_12
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_13

    .line 933
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 935
    :cond_13
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_14

    .line 936
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 938
    :cond_14
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_15

    .line 939
    const/16 v0, 0x13

    iget-boolean v1, p0, Lcom/google/maps/b/bj;->v:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_1c

    :goto_a
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 941
    :cond_15
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_16

    .line 942
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 944
    :cond_16
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_17

    .line 945
    const/16 v0, 0x15

    iget-wide v6, p0, Lcom/google/maps/b/bj;->e:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v6, v7}, Lcom/google/n/l;->a(J)V

    .line 947
    :cond_17
    const/high16 v0, 0x20000000

    invoke-virtual {v4, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 948
    iget-object v0, p0, Lcom/google/maps/b/bj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 949
    return-void

    .line 921
    :cond_18
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_6

    :cond_19
    move v0, v2

    .line 924
    goto/16 :goto_7

    :cond_1a
    move v0, v2

    .line 927
    goto/16 :goto_8

    :cond_1b
    move v0, v2

    .line 930
    goto/16 :goto_9

    :cond_1c
    move v3, v2

    .line 939
    goto :goto_a
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 848
    iget-byte v0, p0, Lcom/google/maps/b/bj;->x:B

    .line 849
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 875
    :cond_0
    :goto_0
    return v2

    .line 850
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 852
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 853
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/co;->d()Lcom/google/maps/b/co;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/co;

    invoke-virtual {v0}, Lcom/google/maps/b/co;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 854
    iput-byte v2, p0, Lcom/google/maps/b/bj;->x:B

    goto :goto_0

    .line 852
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 858
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 859
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/w;

    invoke-virtual {v0}, Lcom/google/maps/b/w;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 860
    iput-byte v2, p0, Lcom/google/maps/b/bj;->x:B

    goto :goto_0

    .line 858
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 864
    :cond_5
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 865
    iget-object v0, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bs;->d()Lcom/google/maps/b/bs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bs;

    invoke-virtual {v0}, Lcom/google/maps/b/bs;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 866
    iput-byte v2, p0, Lcom/google/maps/b/bj;->x:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 864
    goto :goto_3

    .line 870
    :cond_7
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 871
    iput-byte v2, p0, Lcom/google/maps/b/bj;->x:B

    goto :goto_0

    .line 874
    :cond_8
    iput-byte v3, p0, Lcom/google/maps/b/bj;->x:B

    move v2, v3

    .line 875
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 953
    iget v0, p0, Lcom/google/maps/b/bj;->y:I

    .line 954
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1049
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 957
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 958
    iget-object v0, p0, Lcom/google/maps/b/bj;->b:Ljava/util/List;

    .line 959
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 957
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 961
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 962
    iget-object v0, p0, Lcom/google/maps/b/bj;->c:Ljava/util/List;

    .line 963
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 961
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 965
    :cond_2
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3

    .line 966
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/maps/b/bj;->d:Lcom/google/n/ao;

    .line 967
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 969
    :cond_3
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 970
    iget v0, p0, Lcom/google/maps/b/bj;->g:I

    .line 971
    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 973
    :cond_4
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 974
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/maps/b/bj;->i:Lcom/google/n/ao;

    .line 975
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 977
    :cond_5
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 978
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/maps/b/bj;->j:Lcom/google/n/ao;

    .line 979
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 981
    :cond_6
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_7

    .line 982
    const/4 v1, 0x7

    .line 983
    iget-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bj;->o:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 985
    :cond_7
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_8

    .line 986
    const/16 v0, 0x8

    iget-wide v6, p0, Lcom/google/maps/b/bj;->p:J

    .line 987
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v6, v7}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 989
    :cond_8
    iget v0, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 990
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/maps/b/bj;->k:Lcom/google/n/ao;

    .line 991
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_9
    move v1, v2

    move v5, v2

    .line 995
    :goto_5
    iget-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 996
    iget-object v0, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    .line 997
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v5, v0

    .line 995
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_a
    move v0, v4

    .line 971
    goto/16 :goto_3

    .line 983
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_c
    move v0, v4

    .line 997
    goto :goto_6

    .line 999
    :cond_d
    add-int v0, v3, v5

    .line 1000
    iget-object v1, p0, Lcom/google/maps/b/bj;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1002
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_e

    .line 1003
    const/16 v1, 0xb

    iget-object v3, p0, Lcom/google/maps/b/bj;->l:Lcom/google/n/ao;

    .line 1004
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1006
    :cond_e
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_f

    .line 1007
    const/16 v1, 0xc

    iget-object v3, p0, Lcom/google/maps/b/bj;->m:Lcom/google/n/ao;

    .line 1008
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1010
    :cond_f
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_11

    .line 1011
    const/16 v1, 0xd

    iget v3, p0, Lcom/google/maps/b/bj;->q:I

    .line 1012
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_10

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_10
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 1014
    :cond_11
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_12

    .line 1015
    const/16 v1, 0xe

    iget-boolean v3, p0, Lcom/google/maps/b/bj;->r:Z

    .line 1016
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1018
    :cond_12
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_13

    .line 1019
    const/16 v1, 0xf

    iget-boolean v3, p0, Lcom/google/maps/b/bj;->s:Z

    .line 1020
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1022
    :cond_13
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    const v3, 0x8000

    and-int/2addr v1, v3

    const v3, 0x8000

    if-ne v1, v3, :cond_14

    .line 1023
    const/16 v1, 0x10

    iget-boolean v3, p0, Lcom/google/maps/b/bj;->t:Z

    .line 1024
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1026
    :cond_14
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v1, v3

    const/high16 v3, 0x10000

    if-ne v1, v3, :cond_15

    .line 1027
    const/16 v1, 0x11

    iget-object v3, p0, Lcom/google/maps/b/bj;->u:Lcom/google/n/f;

    .line 1028
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1030
    :cond_15
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_16

    .line 1031
    const/16 v1, 0x12

    iget-object v3, p0, Lcom/google/maps/b/bj;->n:Lcom/google/n/ao;

    .line 1032
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1034
    :cond_16
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v1, v3

    const/high16 v3, 0x20000

    if-ne v1, v3, :cond_17

    .line 1035
    const/16 v1, 0x13

    iget-boolean v3, p0, Lcom/google/maps/b/bj;->v:Z

    .line 1036
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1038
    :cond_17
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v9, :cond_18

    .line 1039
    const/16 v1, 0x14

    iget-object v3, p0, Lcom/google/maps/b/bj;->f:Lcom/google/n/ao;

    .line 1040
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1042
    :cond_18
    iget v1, p0, Lcom/google/maps/b/bj;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_19

    .line 1043
    const/16 v1, 0x15

    iget-wide v4, p0, Lcom/google/maps/b/bj;->e:J

    .line 1044
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1046
    :cond_19
    invoke-virtual {p0}, Lcom/google/maps/b/bj;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    iget-object v1, p0, Lcom/google/maps/b/bj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    iput v0, p0, Lcom/google/maps/b/bj;->y:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bj;->newBuilder()Lcom/google/maps/b/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/bm;->a(Lcom/google/maps/b/bj;)Lcom/google/maps/b/bm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bj;->newBuilder()Lcom/google/maps/b/bm;

    move-result-object v0

    return-object v0
.end method
