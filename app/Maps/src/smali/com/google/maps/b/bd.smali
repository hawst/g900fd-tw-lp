.class public final Lcom/google/maps/b/bd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/be;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/bb;",
        "Lcom/google/maps/b/bd;",
        ">;",
        "Lcom/google/maps/b/be;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/f;

.field private d:I

.field private e:F

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/google/maps/b/bb;->g:Lcom/google/maps/b/bb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 411
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bd;->b:Lcom/google/n/ao;

    .line 470
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/b/bd;->c:Lcom/google/n/f;

    .line 537
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/maps/b/bd;->e:F

    .line 327
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/bb;)Lcom/google/maps/b/bd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 378
    invoke-static {}, Lcom/google/maps/b/bb;->d()Lcom/google/maps/b/bb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 396
    :goto_0
    return-object p0

    .line 379
    :cond_0
    iget v2, p1, Lcom/google/maps/b/bb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 380
    iget-object v2, p0, Lcom/google/maps/b/bd;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/bb;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 381
    iget v2, p0, Lcom/google/maps/b/bd;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/bd;->a:I

    .line 383
    :cond_1
    iget v2, p1, Lcom/google/maps/b/bb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 384
    iget-object v2, p1, Lcom/google/maps/b/bb;->c:Lcom/google/n/f;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 379
    goto :goto_1

    :cond_3
    move v2, v1

    .line 383
    goto :goto_2

    .line 384
    :cond_4
    iget v3, p0, Lcom/google/maps/b/bd;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/bd;->a:I

    iput-object v2, p0, Lcom/google/maps/b/bd;->c:Lcom/google/n/f;

    .line 386
    :cond_5
    iget v2, p1, Lcom/google/maps/b/bb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 387
    iget v2, p1, Lcom/google/maps/b/bb;->d:I

    iget v3, p0, Lcom/google/maps/b/bd;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/bd;->a:I

    iput v2, p0, Lcom/google/maps/b/bd;->d:I

    .line 389
    :cond_6
    iget v2, p1, Lcom/google/maps/b/bb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 390
    iget v2, p1, Lcom/google/maps/b/bb;->e:F

    iget v3, p0, Lcom/google/maps/b/bd;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/b/bd;->a:I

    iput v2, p0, Lcom/google/maps/b/bd;->e:F

    .line 392
    :cond_7
    iget v2, p1, Lcom/google/maps/b/bb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_8

    .line 393
    iget-boolean v0, p1, Lcom/google/maps/b/bb;->f:Z

    iget v1, p0, Lcom/google/maps/b/bd;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/maps/b/bd;->a:I

    iput-boolean v0, p0, Lcom/google/maps/b/bd;->f:Z

    .line 395
    :cond_8
    iget-object v0, p1, Lcom/google/maps/b/bb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_9
    move v2, v1

    .line 386
    goto :goto_3

    :cond_a
    move v2, v1

    .line 389
    goto :goto_4

    :cond_b
    move v0, v1

    .line 392
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318
    new-instance v2, Lcom/google/maps/b/bb;

    invoke-direct {v2, p0}, Lcom/google/maps/b/bb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/bd;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/bb;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/bd;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/bd;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/bd;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/b/bb;->c:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/b/bd;->d:I

    iput v1, v2, Lcom/google/maps/b/bb;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/b/bd;->e:F

    iput v1, v2, Lcom/google/maps/b/bb;->e:F

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/maps/b/bd;->f:Z

    iput-boolean v1, v2, Lcom/google/maps/b/bb;->f:Z

    iput v0, v2, Lcom/google/maps/b/bb;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 318
    check-cast p1, Lcom/google/maps/b/bb;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/bd;->a(Lcom/google/maps/b/bb;)Lcom/google/maps/b/bd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 400
    iget v0, p0, Lcom/google/maps/b/bd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/maps/b/bd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ie;->d()Lcom/google/d/a/a/ie;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ie;

    invoke-virtual {v0}, Lcom/google/d/a/a/ie;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 406
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 400
    goto :goto_0

    :cond_1
    move v0, v2

    .line 406
    goto :goto_1
.end method
