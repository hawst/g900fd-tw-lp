.class public Lcom/google/maps/b/a/ag;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cs;

.field private b:Lcom/google/maps/b/a/af;

.field private c:Lcom/google/maps/b/a/cs;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/af;

    invoke-direct {v0}, Lcom/google/maps/b/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ag;->b:Lcom/google/maps/b/a/af;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ag;->c:Lcom/google/maps/b/a/cs;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ag;->d:Ljava/util/List;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/ah;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/ah;-><init>(Lcom/google/maps/b/a/ag;)V

    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 7

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/b/a/ag;->c:Lcom/google/maps/b/a/cs;

    invoke-virtual {v0}, Lcom/google/maps/b/a/cs;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget-object v2, p0, Lcom/google/maps/b/a/ag;->d:Ljava/util/List;

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/maps/b/a/ag;->t:[B

    iget v5, v0, Lcom/google/maps/b/a/cr;->a:I

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    sget-object v6, Lcom/google/maps/b/a/ag;->r:Ljava/nio/charset/Charset;

    invoke-direct {v3, v4, v5, v0, v6}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    packed-switch p1, :pswitch_data_0

    .line 85
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 78
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ag;->c:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/af;
    .locals 4

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/maps/b/a/ag;->b:Lcom/google/maps/b/a/af;

    iget-object v2, p0, Lcom/google/maps/b/a/ag;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/af;->a([BII)V

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->b:Lcom/google/maps/b/a/af;

    return-object v0
.end method

.method final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 67
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    iput v1, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 68
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->b:Lcom/google/maps/b/a/af;

    invoke-virtual {v0}, Lcom/google/maps/b/a/af;->c()V

    .line 69
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->c:Lcom/google/maps/b/a/cs;

    iput v1, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 70
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 91
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_1

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/ag;->b(I)Lcom/google/maps/b/a/af;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/af;->toString()Ljava/lang/String;

    move-result-object v2

    .line 93
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 94
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 97
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xf

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "on_level {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->c:Lcom/google/maps/b/a/cs;

    iget v0, v0, Lcom/google/maps/b/a/cs;->b:I

    if-ge v1, v0, :cond_2

    .line 100
    invoke-direct {p0, v1}, Lcom/google/maps/b/a/ag;->c(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "level_id: \""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 103
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
