.class public Lcom/google/maps/b/a/at;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field a:Lcom/google/maps/b/a/cs;

.field private b:Lcom/google/maps/b/a/cr;

.field private c:Lcom/google/maps/b/a/cu;

.field private d:Lcom/google/maps/b/a/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 11
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    .line 13
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    .line 15
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    .line 17
    new-instance v0, Lcom/google/maps/b/a/as;

    invoke-direct {v0}, Lcom/google/maps/b/a/as;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/at;->d:Lcom/google/maps/b/a/as;

    .line 34
    new-instance v0, Lcom/google/maps/b/a/au;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/au;-><init>(Lcom/google/maps/b/a/at;)V

    return-void
.end method


# virtual methods
.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 71
    packed-switch p1, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 73
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/as;
    .locals 4

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/maps/b/a/at;->d:Lcom/google/maps/b/a/as;

    iget-object v2, p0, Lcom/google/maps/b/a/at;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/as;->a([BII)V

    .line 56
    iget-object v0, p0, Lcom/google/maps/b/a/at;->d:Lcom/google/maps/b/a/as;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 62
    iget-object v0, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 63
    iget-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 64
    iget-object v0, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    iput v2, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 65
    iget-object v0, p0, Lcom/google/maps/b/a/at;->d:Lcom/google/maps/b/a/as;

    invoke-virtual {v0}, Lcom/google/maps/b/a/as;->c()V

    .line 67
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 85
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    iget-object v0, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    iget-object v2, p0, Lcom/google/maps/b/a/at;->t:[B

    iput-object v2, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v1, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v1, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "raster: \""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move v0, v1

    .line 89
    :goto_1
    iget-object v2, p0, Lcom/google/maps/b/a/at;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_3

    .line 90
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/at;->b(I)Lcom/google/maps/b/a/as;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/as;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 92
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 93
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 95
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sprite_info {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    iget-object v2, p0, Lcom/google/maps/b/a/at;->t:[B

    iget-object v4, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->a:I

    iget-object v5, p0, Lcom/google/maps/b/a/at;->b:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->b:I

    iput-object v2, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v4, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v5, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/at;->c:Lcom/google/maps/b/a/cu;

    goto/16 :goto_0

    .line 98
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
