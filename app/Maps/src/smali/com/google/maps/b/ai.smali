.class public final Lcom/google/maps/b/ai;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/aj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/ag;",
        "Lcom/google/maps/b/ai;",
        ">;",
        "Lcom/google/maps/b/aj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:J

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:F

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 714
    sget-object v0, Lcom/google/maps/b/ag;->m:Lcom/google/maps/b/ag;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 866
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->c:Ljava/lang/Object;

    .line 942
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->d:Ljava/lang/Object;

    .line 1050
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->f:Ljava/lang/Object;

    .line 1126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->g:Ljava/lang/Object;

    .line 1202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->b:Ljava/lang/Object;

    .line 1278
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->h:Ljava/lang/Object;

    .line 1354
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ai;->i:Ljava/lang/Object;

    .line 1462
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    .line 715
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/ag;)Lcom/google/maps/b/ai;
    .locals 6

    .prologue
    const/16 v5, 0x200

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 801
    invoke-static {}, Lcom/google/maps/b/ag;->d()Lcom/google/maps/b/ag;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 857
    :goto_0
    return-object p0

    .line 802
    :cond_0
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 803
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 804
    iget-object v2, p1, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->c:Ljava/lang/Object;

    .line 807
    :cond_1
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 808
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 809
    iget-object v2, p1, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->d:Ljava/lang/Object;

    .line 812
    :cond_2
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 813
    iget-wide v2, p1, Lcom/google/maps/b/ag;->d:J

    iget v4, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/maps/b/ai;->a:I

    iput-wide v2, p0, Lcom/google/maps/b/ai;->e:J

    .line 815
    :cond_3
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 816
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 817
    iget-object v2, p1, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->f:Ljava/lang/Object;

    .line 820
    :cond_4
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 821
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 822
    iget-object v2, p1, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->g:Ljava/lang/Object;

    .line 825
    :cond_5
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 826
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 827
    iget-object v2, p1, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->b:Ljava/lang/Object;

    .line 830
    :cond_6
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 831
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 832
    iget-object v2, p1, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->h:Ljava/lang/Object;

    .line 835
    :cond_7
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 836
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 837
    iget-object v2, p1, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ai;->i:Ljava/lang/Object;

    .line 840
    :cond_8
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 841
    iget v2, p1, Lcom/google/maps/b/ag;->j:F

    iget v3, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/b/ai;->a:I

    iput v2, p0, Lcom/google/maps/b/ai;->j:F

    .line 843
    :cond_9
    iget-object v2, p1, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 844
    iget-object v2, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 845
    iget-object v2, p1, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    .line 846
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 853
    :cond_a
    :goto_a
    iget v2, p1, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v2, v2, 0x200

    if-ne v2, v5, :cond_17

    :goto_b
    if-eqz v0, :cond_b

    .line 854
    iget-boolean v0, p1, Lcom/google/maps/b/ag;->l:Z

    iget v1, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/maps/b/ai;->a:I

    iput-boolean v0, p0, Lcom/google/maps/b/ai;->l:Z

    .line 856
    :cond_b
    iget-object v0, p1, Lcom/google/maps/b/ag;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 802
    goto/16 :goto_1

    :cond_d
    move v2, v1

    .line 807
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 812
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 815
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 820
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 825
    goto/16 :goto_6

    :cond_12
    move v2, v1

    .line 830
    goto/16 :goto_7

    :cond_13
    move v2, v1

    .line 835
    goto :goto_8

    :cond_14
    move v2, v1

    .line 840
    goto :goto_9

    .line 848
    :cond_15
    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    and-int/lit16 v2, v2, 0x200

    if-eq v2, v5, :cond_16

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/ai;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/maps/b/ai;->a:I

    .line 849
    :cond_16
    iget-object v2, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a

    :cond_17
    move v0, v1

    .line 853
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 706
    new-instance v2, Lcom/google/maps/b/ag;

    invoke-direct {v2, p0}, Lcom/google/maps/b/ag;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/ai;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget-object v1, p0, Lcom/google/maps/b/ai;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/ai;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/maps/b/ai;->e:J

    iput-wide v4, v2, Lcom/google/maps/b/ag;->d:J

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/ai;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/maps/b/ai;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/maps/b/ai;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/maps/b/ai;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/maps/b/ai;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/maps/b/ai;->j:F

    iput v1, v2, Lcom/google/maps/b/ag;->j:F

    iget v1, p0, Lcom/google/maps/b/ai;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ai;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/maps/b/ai;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/maps/b/ai;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-boolean v1, p0, Lcom/google/maps/b/ai;->l:Z

    iput-boolean v1, v2, Lcom/google/maps/b/ag;->l:Z

    iput v0, v2, Lcom/google/maps/b/ag;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 706
    check-cast p1, Lcom/google/maps/b/ag;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/ai;->a(Lcom/google/maps/b/ag;)Lcom/google/maps/b/ai;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 861
    const/4 v0, 0x1

    return v0
.end method
