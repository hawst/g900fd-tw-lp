.class public Lcom/google/maps/b/a/cw;
.super Lcom/google/maps/b/a/cx;
.source "PG"


# instance fields
.field public a:[F

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/maps/b/a/cx;-><init>()V

    .line 203
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/maps/b/a/cw;->a:[F

    .line 204
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/a/cw;->b:I

    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 209
    iget v0, p0, Lcom/google/maps/b/a/cw;->b:I

    iget-object v1, p0, Lcom/google/maps/b/a/cw;->a:[F

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/maps/b/a/cw;->a:[F

    .line 211
    array-length v1, v0

    shl-int/lit8 v1, v1, 0x1

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/maps/b/a/cw;->a:[F

    .line 212
    iget-object v1, p0, Lcom/google/maps/b/a/cw;->a:[F

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/cw;->a:[F

    iget v1, p0, Lcom/google/maps/b/a/cw;->b:I

    invoke-static {p1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v2

    aput v2, v0, v1

    .line 215
    iget v0, p0, Lcom/google/maps/b/a/cw;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/b/a/cw;->b:I

    .line 216
    return-void
.end method
