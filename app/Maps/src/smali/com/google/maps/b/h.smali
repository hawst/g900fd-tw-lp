.class public final enum Lcom/google/maps/b/h;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/h;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/h;

.field public static final enum b:Lcom/google/maps/b/h;

.field public static final enum c:Lcom/google/maps/b/h;

.field public static final enum d:Lcom/google/maps/b/h;

.field public static final enum e:Lcom/google/maps/b/h;

.field public static final enum f:Lcom/google/maps/b/h;

.field public static final enum g:Lcom/google/maps/b/h;

.field public static final enum h:Lcom/google/maps/b/h;

.field public static final enum i:Lcom/google/maps/b/h;

.field private static final synthetic k:[Lcom/google/maps/b/h;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 174
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    .line 178
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_LDPI"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->b:Lcom/google/maps/b/h;

    .line 182
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_MDPI"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->c:Lcom/google/maps/b/h;

    .line 186
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_HDPI"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->d:Lcom/google/maps/b/h;

    .line 190
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_XHDPI"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->e:Lcom/google/maps/b/h;

    .line 194
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_XXHDPI"

    const/4 v2, 0x5

    const/16 v3, 0x1e0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->f:Lcom/google/maps/b/h;

    .line 198
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_XXXHDPI"

    const/4 v2, 0x6

    const/16 v3, 0x280

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->g:Lcom/google/maps/b/h;

    .line 202
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_HDPI_6X"

    const/4 v2, 0x7

    const/16 v3, 0x3c0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->h:Lcom/google/maps/b/h;

    .line 206
    new-instance v0, Lcom/google/maps/b/h;

    const-string v1, "DENSITY_HDPI_8X"

    const/16 v2, 0x8

    const/16 v3, 0x500

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/h;->i:Lcom/google/maps/b/h;

    .line 169
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/maps/b/h;

    sget-object v1, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/h;->b:Lcom/google/maps/b/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/h;->c:Lcom/google/maps/b/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/h;->d:Lcom/google/maps/b/h;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/h;->e:Lcom/google/maps/b/h;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/h;->f:Lcom/google/maps/b/h;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/h;->g:Lcom/google/maps/b/h;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/b/h;->h:Lcom/google/maps/b/h;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/b/h;->i:Lcom/google/maps/b/h;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/h;->k:[Lcom/google/maps/b/h;

    .line 271
    new-instance v0, Lcom/google/maps/b/i;

    invoke-direct {v0}, Lcom/google/maps/b/i;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 281
    iput p3, p0, Lcom/google/maps/b/h;->j:I

    .line 282
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/h;
    .locals 1

    .prologue
    .line 252
    sparse-switch p0, :sswitch_data_0

    .line 262
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 253
    :sswitch_0
    sget-object v0, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    goto :goto_0

    .line 254
    :sswitch_1
    sget-object v0, Lcom/google/maps/b/h;->b:Lcom/google/maps/b/h;

    goto :goto_0

    .line 255
    :sswitch_2
    sget-object v0, Lcom/google/maps/b/h;->c:Lcom/google/maps/b/h;

    goto :goto_0

    .line 256
    :sswitch_3
    sget-object v0, Lcom/google/maps/b/h;->d:Lcom/google/maps/b/h;

    goto :goto_0

    .line 257
    :sswitch_4
    sget-object v0, Lcom/google/maps/b/h;->e:Lcom/google/maps/b/h;

    goto :goto_0

    .line 258
    :sswitch_5
    sget-object v0, Lcom/google/maps/b/h;->f:Lcom/google/maps/b/h;

    goto :goto_0

    .line 259
    :sswitch_6
    sget-object v0, Lcom/google/maps/b/h;->g:Lcom/google/maps/b/h;

    goto :goto_0

    .line 260
    :sswitch_7
    sget-object v0, Lcom/google/maps/b/h;->h:Lcom/google/maps/b/h;

    goto :goto_0

    .line 261
    :sswitch_8
    sget-object v0, Lcom/google/maps/b/h;->i:Lcom/google/maps/b/h;

    goto :goto_0

    .line 252
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x78 -> :sswitch_1
        0xa0 -> :sswitch_2
        0xf0 -> :sswitch_3
        0x140 -> :sswitch_4
        0x1e0 -> :sswitch_5
        0x280 -> :sswitch_6
        0x3c0 -> :sswitch_7
        0x500 -> :sswitch_8
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/h;
    .locals 1

    .prologue
    .line 169
    const-class v0, Lcom/google/maps/b/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/h;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/h;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/google/maps/b/h;->k:[Lcom/google/maps/b/h;

    invoke-virtual {v0}, [Lcom/google/maps/b/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/google/maps/b/h;->j:I

    return v0
.end method
