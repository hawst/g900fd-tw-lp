.class public Lcom/google/maps/b/a/dm;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cs;

.field private c:Lcom/google/maps/b/a/cu;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Lcom/google/maps/b/a/bo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 27
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    .line 29
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    .line 31
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dm;->c:Lcom/google/maps/b/a/cu;

    .line 33
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    .line 35
    new-instance v0, Lcom/google/maps/b/a/bo;

    invoke-direct {v0}, Lcom/google/maps/b/a/bo;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    .line 46
    new-instance v0, Lcom/google/maps/b/a/dn;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/dn;-><init>(Lcom/google/maps/b/a/dm;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/bo;
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bo;->c()V

    .line 75
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    .line 78
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    iget-object v1, p0, Lcom/google/maps/b/a/dm;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/bo;->a([BII)V

    .line 78
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    packed-switch p1, :pswitch_data_0

    .line 103
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 99
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 108
    packed-switch p1, :pswitch_data_0

    .line 117
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 110
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 113
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/maps/b/a/dm;->c:Lcom/google/maps/b/a/cu;

    iget-object v2, p0, Lcom/google/maps/b/a/dm;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    iput-object v2, v1, Lcom/google/maps/b/a/cu;->a:[B

    iput v3, v1, Lcom/google/maps/b/a/cu;->b:I

    iput v0, v1, Lcom/google/maps/b/a/cu;->c:I

    .line 68
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->c:Lcom/google/maps/b/a/cu;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 87
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 88
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    iput v2, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->c:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 90
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 91
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->e:Lcom/google/maps/b/a/bo;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bo;->c()V

    .line 93
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move v0, v1

    .line 126
    :goto_0
    iget-object v3, p0, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    iget v3, v3, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v3, :cond_1

    .line 127
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/dm;->b(I)Lcom/google/maps/b/a/cu;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "raster: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\"\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/dm;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 130
    invoke-virtual {p0}, Lcom/google/maps/b/a/dm;->a()Lcom/google/maps/b/a/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/bo;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v3, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 133
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "coverage {\n  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
