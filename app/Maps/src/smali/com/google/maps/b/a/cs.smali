.class public Lcom/google/maps/b/a/cs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/maps/b/a/cr;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/maps/b/a/cr;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field final c:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/cr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/a/cs;->b:I

    .line 318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    .line 319
    new-instance v0, Lcom/google/maps/b/a/ct;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/ct;-><init>(Lcom/google/maps/b/a/cs;)V

    iput-object v0, p0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    .line 331
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 339
    iget v0, p0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v1, p0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 341
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    .line 342
    iget-object v1, p0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    :goto_0
    iget v1, p0, Lcom/google/maps/b/a/cs;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/b/a/cs;->b:I

    .line 347
    iput p1, v0, Lcom/google/maps/b/a/cr;->a:I

    iput p2, v0, Lcom/google/maps/b/a/cr;->b:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 348
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/maps/b/a/cs;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/maps/b/a/cr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 335
    iget-object v0, p0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    return-object v0
.end method
