.class public final enum Lcom/google/maps/b/bp;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/bp;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/bp;

.field public static final enum b:Lcom/google/maps/b/bp;

.field public static final enum c:Lcom/google/maps/b/bp;

.field private static final synthetic e:[Lcom/google/maps/b/bp;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 363
    new-instance v0, Lcom/google/maps/b/bp;

    const-string v1, "METADATA_PERTILE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/b/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bp;->a:Lcom/google/maps/b/bp;

    .line 367
    new-instance v0, Lcom/google/maps/b/bp;

    const-string v1, "METADATA_COPYRIGHTS"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bp;->b:Lcom/google/maps/b/bp;

    .line 371
    new-instance v0, Lcom/google/maps/b/bp;

    const-string v1, "METADATA_COPYRIGHTS_V2"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/maps/b/bp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bp;->c:Lcom/google/maps/b/bp;

    .line 358
    new-array v0, v5, [Lcom/google/maps/b/bp;

    sget-object v1, Lcom/google/maps/b/bp;->a:Lcom/google/maps/b/bp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/bp;->b:Lcom/google/maps/b/bp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/b/bp;->c:Lcom/google/maps/b/bp;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/maps/b/bp;->e:[Lcom/google/maps/b/bp;

    .line 406
    new-instance v0, Lcom/google/maps/b/bq;

    invoke-direct {v0}, Lcom/google/maps/b/bq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 416
    iput p3, p0, Lcom/google/maps/b/bp;->d:I

    .line 417
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/bp;
    .locals 1

    .prologue
    .line 393
    packed-switch p0, :pswitch_data_0

    .line 397
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 394
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/bp;->a:Lcom/google/maps/b/bp;

    goto :goto_0

    .line 395
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/bp;->b:Lcom/google/maps/b/bp;

    goto :goto_0

    .line 396
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/bp;->c:Lcom/google/maps/b/bp;

    goto :goto_0

    .line 393
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/bp;
    .locals 1

    .prologue
    .line 358
    const-class v0, Lcom/google/maps/b/bp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bp;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/bp;
    .locals 1

    .prologue
    .line 358
    sget-object v0, Lcom/google/maps/b/bp;->e:[Lcom/google/maps/b/bp;

    invoke-virtual {v0}, [Lcom/google/maps/b/bp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/bp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 389
    iget v0, p0, Lcom/google/maps/b/bp;->d:I

    return v0
.end method
