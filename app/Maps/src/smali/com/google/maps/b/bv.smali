.class public final enum Lcom/google/maps/b/bv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/bv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/maps/b/bv;

.field public static final enum B:Lcom/google/maps/b/bv;

.field public static final enum C:Lcom/google/maps/b/bv;

.field public static final enum D:Lcom/google/maps/b/bv;

.field public static final enum E:Lcom/google/maps/b/bv;

.field public static final enum F:Lcom/google/maps/b/bv;

.field public static final enum G:Lcom/google/maps/b/bv;

.field public static final enum H:Lcom/google/maps/b/bv;

.field public static final enum I:Lcom/google/maps/b/bv;

.field public static final enum J:Lcom/google/maps/b/bv;

.field public static final enum K:Lcom/google/maps/b/bv;

.field public static final enum L:Lcom/google/maps/b/bv;

.field public static final enum M:Lcom/google/maps/b/bv;

.field public static final enum N:Lcom/google/maps/b/bv;

.field public static final enum O:Lcom/google/maps/b/bv;

.field public static final enum P:Lcom/google/maps/b/bv;

.field public static final enum Q:Lcom/google/maps/b/bv;

.field public static final enum R:Lcom/google/maps/b/bv;

.field public static final enum S:Lcom/google/maps/b/bv;

.field public static final enum T:Lcom/google/maps/b/bv;

.field public static final enum U:Lcom/google/maps/b/bv;

.field public static final enum V:Lcom/google/maps/b/bv;

.field public static final enum W:Lcom/google/maps/b/bv;

.field public static final enum X:Lcom/google/maps/b/bv;

.field private static final synthetic Z:[Lcom/google/maps/b/bv;

.field public static final enum a:Lcom/google/maps/b/bv;

.field public static final enum b:Lcom/google/maps/b/bv;

.field public static final enum c:Lcom/google/maps/b/bv;

.field public static final enum d:Lcom/google/maps/b/bv;

.field public static final enum e:Lcom/google/maps/b/bv;

.field public static final enum f:Lcom/google/maps/b/bv;

.field public static final enum g:Lcom/google/maps/b/bv;

.field public static final enum h:Lcom/google/maps/b/bv;

.field public static final enum i:Lcom/google/maps/b/bv;

.field public static final enum j:Lcom/google/maps/b/bv;

.field public static final enum k:Lcom/google/maps/b/bv;

.field public static final enum l:Lcom/google/maps/b/bv;

.field public static final enum m:Lcom/google/maps/b/bv;

.field public static final enum n:Lcom/google/maps/b/bv;

.field public static final enum o:Lcom/google/maps/b/bv;

.field public static final enum p:Lcom/google/maps/b/bv;

.field public static final enum q:Lcom/google/maps/b/bv;

.field public static final enum r:Lcom/google/maps/b/bv;

.field public static final enum s:Lcom/google/maps/b/bv;

.field public static final enum t:Lcom/google/maps/b/bv;

.field public static final enum u:Lcom/google/maps/b/bv;

.field public static final enum v:Lcom/google/maps/b/bv;

.field public static final enum w:Lcom/google/maps/b/bv;

.field public static final enum x:Lcom/google/maps/b/bv;

.field public static final enum y:Lcom/google/maps/b/bv;

.field public static final enum z:Lcom/google/maps/b/bv;


# instance fields
.field final Y:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 198
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->a:Lcom/google/maps/b/bv;

    .line 202
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_WEB"

    invoke-direct {v0, v1, v5, v8}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->b:Lcom/google/maps/b/bv;

    .line 206
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_APPLICATION"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->c:Lcom/google/maps/b/bv;

    .line 210
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_STREETVIEW"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->d:Lcom/google/maps/b/bv;

    .line 214
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_LOCAL_UNIVERSAL"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->e:Lcom/google/maps/b/bv;

    .line 218
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_MOBILE_WEB_MAPS"

    const/4 v2, 0x5

    const/16 v3, 0x44

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->f:Lcom/google/maps/b/bv;

    .line 222
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_VECTOR"

    const/4 v2, 0x6

    const/16 v3, 0x45

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->g:Lcom/google/maps/b/bv;

    .line 226
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_TACTILE"

    const/4 v2, 0x7

    const/16 v3, 0x451

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->h:Lcom/google/maps/b/bv;

    .line 230
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_TACTILE_GWS"

    const/16 v2, 0x8

    const/16 v3, 0x4511

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->i:Lcom/google/maps/b/bv;

    .line 234
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_TACTILE_SHARE"

    const/16 v2, 0x9

    const/16 v3, 0x4512

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->j:Lcom/google/maps/b/bv;

    .line 238
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_MOBILE_FE"

    const/16 v2, 0xa

    const/16 v3, 0x46

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->k:Lcom/google/maps/b/bv;

    .line 242
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_PRODUCT_SEARCH"

    const/16 v2, 0xb

    const/16 v3, 0x47

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->l:Lcom/google/maps/b/bv;

    .line 246
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_STATIC_MAP"

    const/16 v2, 0xc

    const/16 v3, 0x48

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->m:Lcom/google/maps/b/bv;

    .line 250
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_MAPMAKER"

    const/16 v2, 0xd

    const/16 v3, 0x49

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->n:Lcom/google/maps/b/bv;

    .line 254
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GT_ATLAS_DEPRECATED"

    const/16 v2, 0xe

    const/16 v3, 0x4a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->o:Lcom/google/maps/b/bv;

    .line 258
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_LOCATION_PLUS"

    const/16 v2, 0xf

    const/16 v3, 0x4b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->p:Lcom/google/maps/b/bv;

    .line 262
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_SMARTMAIL"

    const/16 v2, 0x10

    const/16 v3, 0x4c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->q:Lcom/google/maps/b/bv;

    .line 266
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ELECTIONS"

    const/16 v2, 0x11

    const/16 v3, 0x4d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->r:Lcom/google/maps/b/bv;

    .line 270
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_MAPS_LITE"

    const/16 v2, 0x12

    const/16 v3, 0x4e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->s:Lcom/google/maps/b/bv;

    .line 274
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_API"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v5}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->t:Lcom/google/maps/b/bv;

    .line 278
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_JS_V2_API"

    const/16 v2, 0x14

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->u:Lcom/google/maps/b/bv;

    .line 282
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_JS_V3_API"

    const/16 v2, 0x15

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->v:Lcom/google/maps/b/bv;

    .line 286
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_FLASH_API"

    const/16 v2, 0x16

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->w:Lcom/google/maps/b/bv;

    .line 290
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID_API"

    const/16 v2, 0x17

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->x:Lcom/google/maps/b/bv;

    .line 294
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID_API_V1"

    const/16 v2, 0x18

    const/16 v3, 0x141

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->y:Lcom/google/maps/b/bv;

    .line 298
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID_API_V2"

    const/16 v2, 0x19

    const/16 v3, 0x142

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->z:Lcom/google/maps/b/bv;

    .line 302
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID_API_V2_GLASS"

    const/16 v2, 0x1a

    const/16 v3, 0x1421

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->A:Lcom/google/maps/b/bv;

    .line 306
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_IPHONE_API"

    const/16 v2, 0x1b

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->B:Lcom/google/maps/b/bv;

    .line 310
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_IPHONE_API_V1"

    const/16 v2, 0x1c

    const/16 v3, 0x151

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->C:Lcom/google/maps/b/bv;

    .line 314
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_IPHONE_API_V2"

    const/16 v2, 0x1d

    const/16 v3, 0x152

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->D:Lcom/google/maps/b/bv;

    .line 318
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_STREETVIEW_API"

    const/16 v2, 0x1e

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->E:Lcom/google/maps/b/bv;

    .line 322
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_STATIC_MAP_API"

    const/16 v2, 0x1f

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->F:Lcom/google/maps/b/bv;

    .line 326
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_EARTHBUILDER_API"

    const/16 v2, 0x20

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->G:Lcom/google/maps/b/bv;

    .line 330
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_TILE_API"

    const/16 v2, 0x21

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->H:Lcom/google/maps/b/bv;

    .line 334
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_MOBILE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2, v6}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->I:Lcom/google/maps/b/bv;

    .line 338
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GMM"

    const/16 v2, 0x23

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->J:Lcom/google/maps/b/bv;

    .line 342
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_IPHONE"

    const/16 v2, 0x24

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->K:Lcom/google/maps/b/bv;

    .line 346
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_NAVIGATION"

    const/16 v2, 0x25

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->L:Lcom/google/maps/b/bv;

    .line 350
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID"

    const/16 v2, 0x26

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->M:Lcom/google/maps/b/bv;

    .line 354
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ANDROID_SEARCH"

    const/16 v2, 0x27

    const/16 v3, 0x241

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->N:Lcom/google/maps/b/bv;

    .line 358
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GOGGLES"

    const/16 v2, 0x28

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->O:Lcom/google/maps/b/bv;

    .line 362
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GLASS"

    const/16 v2, 0x29

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->P:Lcom/google/maps/b/bv;

    .line 366
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_IOS_GSA"

    const/16 v2, 0x2a

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->Q:Lcom/google/maps/b/bv;

    .line 370
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_EARTH"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2, v7}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->R:Lcom/google/maps/b/bv;

    .line 374
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_ADS"

    const/16 v2, 0x2c

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->S:Lcom/google/maps/b/bv;

    .line 378
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_CONTENTADS"

    const/16 v2, 0x2d

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->T:Lcom/google/maps/b/bv;

    .line 382
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_BOOKS"

    const/16 v2, 0x2e

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->U:Lcom/google/maps/b/bv;

    .line 386
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_INTERNAL_TOOL"

    const/16 v2, 0x2f

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->V:Lcom/google/maps/b/bv;

    .line 390
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GO_TILES"

    const/16 v2, 0x30

    const/16 v3, 0x71

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->W:Lcom/google/maps/b/bv;

    .line 394
    new-instance v0, Lcom/google/maps/b/bv;

    const-string v1, "MAP_SOURCE_GT_ATLAS"

    const/16 v2, 0x31

    const/16 v3, 0x72

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/b/bv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/bv;->X:Lcom/google/maps/b/bv;

    .line 193
    const/16 v0, 0x32

    new-array v0, v0, [Lcom/google/maps/b/bv;

    sget-object v1, Lcom/google/maps/b/bv;->a:Lcom/google/maps/b/bv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/bv;->b:Lcom/google/maps/b/bv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/bv;->c:Lcom/google/maps/b/bv;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/b/bv;->d:Lcom/google/maps/b/bv;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/bv;->e:Lcom/google/maps/b/bv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/b/bv;->f:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/b/bv;->g:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/b/bv;->h:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/b/bv;->i:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/b/bv;->j:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/b/bv;->k:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/b/bv;->l:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/b/bv;->m:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/b/bv;->n:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/b/bv;->o:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/b/bv;->p:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/b/bv;->q:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/b/bv;->r:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/b/bv;->s:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/b/bv;->t:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/b/bv;->u:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/b/bv;->v:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/b/bv;->w:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/b/bv;->x:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/b/bv;->y:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/b/bv;->z:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/b/bv;->A:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/b/bv;->B:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/b/bv;->C:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/b/bv;->D:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/b/bv;->E:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/maps/b/bv;->F:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/maps/b/bv;->G:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/maps/b/bv;->H:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/maps/b/bv;->I:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/maps/b/bv;->J:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/maps/b/bv;->K:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/maps/b/bv;->L:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/maps/b/bv;->M:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/maps/b/bv;->N:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/maps/b/bv;->O:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/maps/b/bv;->P:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/maps/b/bv;->Q:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/maps/b/bv;->R:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/maps/b/bv;->S:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/maps/b/bv;->T:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/maps/b/bv;->U:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/maps/b/bv;->V:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/maps/b/bv;->W:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/maps/b/bv;->X:Lcom/google/maps/b/bv;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/b/bv;->Z:[Lcom/google/maps/b/bv;

    .line 664
    new-instance v0, Lcom/google/maps/b/bw;

    invoke-direct {v0}, Lcom/google/maps/b/bw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674
    iput p3, p0, Lcom/google/maps/b/bv;->Y:I

    .line 675
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/bv;
    .locals 1

    .prologue
    .line 604
    sparse-switch p0, :sswitch_data_0

    .line 655
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 605
    :sswitch_0
    sget-object v0, Lcom/google/maps/b/bv;->a:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 606
    :sswitch_1
    sget-object v0, Lcom/google/maps/b/bv;->b:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 607
    :sswitch_2
    sget-object v0, Lcom/google/maps/b/bv;->c:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 608
    :sswitch_3
    sget-object v0, Lcom/google/maps/b/bv;->d:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 609
    :sswitch_4
    sget-object v0, Lcom/google/maps/b/bv;->e:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 610
    :sswitch_5
    sget-object v0, Lcom/google/maps/b/bv;->f:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 611
    :sswitch_6
    sget-object v0, Lcom/google/maps/b/bv;->g:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 612
    :sswitch_7
    sget-object v0, Lcom/google/maps/b/bv;->h:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 613
    :sswitch_8
    sget-object v0, Lcom/google/maps/b/bv;->i:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 614
    :sswitch_9
    sget-object v0, Lcom/google/maps/b/bv;->j:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 615
    :sswitch_a
    sget-object v0, Lcom/google/maps/b/bv;->k:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 616
    :sswitch_b
    sget-object v0, Lcom/google/maps/b/bv;->l:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 617
    :sswitch_c
    sget-object v0, Lcom/google/maps/b/bv;->m:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 618
    :sswitch_d
    sget-object v0, Lcom/google/maps/b/bv;->n:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 619
    :sswitch_e
    sget-object v0, Lcom/google/maps/b/bv;->o:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 620
    :sswitch_f
    sget-object v0, Lcom/google/maps/b/bv;->p:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 621
    :sswitch_10
    sget-object v0, Lcom/google/maps/b/bv;->q:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 622
    :sswitch_11
    sget-object v0, Lcom/google/maps/b/bv;->r:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 623
    :sswitch_12
    sget-object v0, Lcom/google/maps/b/bv;->s:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 624
    :sswitch_13
    sget-object v0, Lcom/google/maps/b/bv;->t:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 625
    :sswitch_14
    sget-object v0, Lcom/google/maps/b/bv;->u:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 626
    :sswitch_15
    sget-object v0, Lcom/google/maps/b/bv;->v:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 627
    :sswitch_16
    sget-object v0, Lcom/google/maps/b/bv;->w:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 628
    :sswitch_17
    sget-object v0, Lcom/google/maps/b/bv;->x:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 629
    :sswitch_18
    sget-object v0, Lcom/google/maps/b/bv;->y:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 630
    :sswitch_19
    sget-object v0, Lcom/google/maps/b/bv;->z:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 631
    :sswitch_1a
    sget-object v0, Lcom/google/maps/b/bv;->A:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 632
    :sswitch_1b
    sget-object v0, Lcom/google/maps/b/bv;->B:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 633
    :sswitch_1c
    sget-object v0, Lcom/google/maps/b/bv;->C:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 634
    :sswitch_1d
    sget-object v0, Lcom/google/maps/b/bv;->D:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 635
    :sswitch_1e
    sget-object v0, Lcom/google/maps/b/bv;->E:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 636
    :sswitch_1f
    sget-object v0, Lcom/google/maps/b/bv;->F:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 637
    :sswitch_20
    sget-object v0, Lcom/google/maps/b/bv;->G:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 638
    :sswitch_21
    sget-object v0, Lcom/google/maps/b/bv;->H:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 639
    :sswitch_22
    sget-object v0, Lcom/google/maps/b/bv;->I:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 640
    :sswitch_23
    sget-object v0, Lcom/google/maps/b/bv;->J:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 641
    :sswitch_24
    sget-object v0, Lcom/google/maps/b/bv;->K:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 642
    :sswitch_25
    sget-object v0, Lcom/google/maps/b/bv;->L:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 643
    :sswitch_26
    sget-object v0, Lcom/google/maps/b/bv;->M:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 644
    :sswitch_27
    sget-object v0, Lcom/google/maps/b/bv;->N:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 645
    :sswitch_28
    sget-object v0, Lcom/google/maps/b/bv;->O:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 646
    :sswitch_29
    sget-object v0, Lcom/google/maps/b/bv;->P:Lcom/google/maps/b/bv;

    goto :goto_0

    .line 647
    :sswitch_2a
    sget-object v0, Lcom/google/maps/b/bv;->Q:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 648
    :sswitch_2b
    sget-object v0, Lcom/google/maps/b/bv;->R:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 649
    :sswitch_2c
    sget-object v0, Lcom/google/maps/b/bv;->S:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 650
    :sswitch_2d
    sget-object v0, Lcom/google/maps/b/bv;->T:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 651
    :sswitch_2e
    sget-object v0, Lcom/google/maps/b/bv;->U:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 652
    :sswitch_2f
    sget-object v0, Lcom/google/maps/b/bv;->V:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 653
    :sswitch_30
    sget-object v0, Lcom/google/maps/b/bv;->W:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 654
    :sswitch_31
    sget-object v0, Lcom/google/maps/b/bv;->X:Lcom/google/maps/b/bv;

    goto/16 :goto_0

    .line 604
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_13
        0x2 -> :sswitch_22
        0x3 -> :sswitch_2b
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2c
        0x6 -> :sswitch_2e
        0x7 -> :sswitch_2f
        0x11 -> :sswitch_14
        0x12 -> :sswitch_15
        0x13 -> :sswitch_16
        0x14 -> :sswitch_17
        0x15 -> :sswitch_1b
        0x16 -> :sswitch_1e
        0x17 -> :sswitch_1f
        0x18 -> :sswitch_20
        0x19 -> :sswitch_21
        0x21 -> :sswitch_23
        0x22 -> :sswitch_24
        0x23 -> :sswitch_25
        0x24 -> :sswitch_26
        0x25 -> :sswitch_28
        0x26 -> :sswitch_29
        0x27 -> :sswitch_2a
        0x41 -> :sswitch_2
        0x42 -> :sswitch_3
        0x43 -> :sswitch_4
        0x44 -> :sswitch_5
        0x45 -> :sswitch_6
        0x46 -> :sswitch_a
        0x47 -> :sswitch_b
        0x48 -> :sswitch_c
        0x49 -> :sswitch_d
        0x4a -> :sswitch_e
        0x4b -> :sswitch_f
        0x4c -> :sswitch_10
        0x4d -> :sswitch_11
        0x4e -> :sswitch_12
        0x51 -> :sswitch_2d
        0x71 -> :sswitch_30
        0x72 -> :sswitch_31
        0x141 -> :sswitch_18
        0x142 -> :sswitch_19
        0x151 -> :sswitch_1c
        0x152 -> :sswitch_1d
        0x241 -> :sswitch_27
        0x451 -> :sswitch_7
        0x1421 -> :sswitch_1a
        0x4511 -> :sswitch_8
        0x4512 -> :sswitch_9
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/bv;
    .locals 1

    .prologue
    .line 193
    const-class v0, Lcom/google/maps/b/bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bv;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/bv;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/google/maps/b/bv;->Z:[Lcom/google/maps/b/bv;

    invoke-virtual {v0}, [Lcom/google/maps/b/bv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/bv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 600
    iget v0, p0, Lcom/google/maps/b/bv;->Y:I

    return v0
.end method
