.class public Lcom/google/maps/b/a/da;
.super Lcom/google/maps/b/a/cv;
.source "PG"


# instance fields
.field public a:[J

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/google/maps/b/a/cv;-><init>()V

    .line 243
    const/4 v0, 0x4

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/maps/b/a/da;->a:[J

    .line 244
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/a/da;->b:I

    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 249
    iget v0, p0, Lcom/google/maps/b/a/da;->b:I

    iget-object v1, p0, Lcom/google/maps/b/a/da;->a:[J

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/maps/b/a/da;->a:[J

    .line 251
    array-length v1, v0

    shl-int/lit8 v1, v1, 0x1

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/google/maps/b/a/da;->a:[J

    .line 252
    iget-object v1, p0, Lcom/google/maps/b/a/da;->a:[J

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/da;->a:[J

    iget v1, p0, Lcom/google/maps/b/a/da;->b:I

    aput-wide p1, v0, v1

    .line 255
    iget v0, p0, Lcom/google/maps/b/a/da;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/b/a/da;->b:I

    .line 256
    return-void
.end method
