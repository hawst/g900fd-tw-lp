.class public final Lcom/google/maps/b/de;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/dl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/dc;",
        "Lcom/google/maps/b/de;",
        ">;",
        "Lcom/google/maps/b/dl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1535
    sget-object v0, Lcom/google/maps/b/dc;->d:Lcom/google/maps/b/dc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1602
    const/16 v0, 0x25

    iput v0, p0, Lcom/google/maps/b/de;->b:I

    .line 1639
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    .line 1536
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/dc;)Lcom/google/maps/b/de;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1568
    invoke-static {}, Lcom/google/maps/b/dc;->d()Lcom/google/maps/b/dc;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1583
    :goto_0
    return-object p0

    .line 1569
    :cond_0
    iget v1, p1, Lcom/google/maps/b/dc;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 1570
    iget v0, p1, Lcom/google/maps/b/dc;->b:I

    invoke-static {v0}, Lcom/google/maps/b/dj;->a(I)Lcom/google/maps/b/dj;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/b/dj;->a:Lcom/google/maps/b/dj;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1569
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1570
    :cond_3
    iget v1, p0, Lcom/google/maps/b/de;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/maps/b/de;->a:I

    iget v0, v0, Lcom/google/maps/b/dj;->av:I

    iput v0, p0, Lcom/google/maps/b/de;->b:I

    .line 1572
    :cond_4
    iget-object v0, p1, Lcom/google/maps/b/dc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1573
    iget-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1574
    iget-object v0, p1, Lcom/google/maps/b/dc;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    .line 1575
    iget v0, p0, Lcom/google/maps/b/de;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/b/de;->a:I

    .line 1582
    :cond_5
    :goto_2
    iget-object v0, p1, Lcom/google/maps/b/dc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1577
    :cond_6
    iget v0, p0, Lcom/google/maps/b/de;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/b/de;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/b/de;->a:I

    .line 1578
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/b/dc;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1527
    new-instance v2, Lcom/google/maps/b/dc;

    invoke-direct {v2, p0}, Lcom/google/maps/b/dc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/de;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/maps/b/de;->b:I

    iput v1, v2, Lcom/google/maps/b/dc;->b:I

    iget v1, p0, Lcom/google/maps/b/de;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/de;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/b/de;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/maps/b/dc;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/maps/b/dc;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1527
    check-cast p1, Lcom/google/maps/b/dc;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/de;->a(Lcom/google/maps/b/dc;)Lcom/google/maps/b/de;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1587
    iget v0, p0, Lcom/google/maps/b/de;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 1597
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1587
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1591
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1592
    iget-object v0, p0, Lcom/google/maps/b/de;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/df;->d()Lcom/google/maps/b/df;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/df;

    invoke-virtual {v0}, Lcom/google/maps/b/df;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1591
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1597
    goto :goto_1
.end method
