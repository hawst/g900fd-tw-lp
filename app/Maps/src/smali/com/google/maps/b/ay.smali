.class public final enum Lcom/google/maps/b/ay;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/ay;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/ay;

.field public static final enum b:Lcom/google/maps/b/ay;

.field public static final enum c:Lcom/google/maps/b/ay;

.field public static final enum d:Lcom/google/maps/b/ay;

.field public static final enum e:Lcom/google/maps/b/ay;

.field private static final synthetic g:[Lcom/google/maps/b/ay;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 156
    new-instance v0, Lcom/google/maps/b/ay;

    const-string v1, "ELEMENT_COPYRIGHTS"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/maps/b/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ay;->a:Lcom/google/maps/b/ay;

    .line 160
    new-instance v0, Lcom/google/maps/b/ay;

    const-string v1, "ELEMENT_FRAME"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/maps/b/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ay;->b:Lcom/google/maps/b/ay;

    .line 164
    new-instance v0, Lcom/google/maps/b/ay;

    const-string v1, "ELEMENT_GOOGLE_LOGO"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/maps/b/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ay;->c:Lcom/google/maps/b/ay;

    .line 168
    new-instance v0, Lcom/google/maps/b/ay;

    const-string v1, "ELEMENT_GOOGLE_TEXT_ATTRIBUTION"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/maps/b/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ay;->d:Lcom/google/maps/b/ay;

    .line 172
    new-instance v0, Lcom/google/maps/b/ay;

    const-string v1, "ELEMENT_DEVELOPMENT_ONLY_WATERMARK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/b/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/ay;->e:Lcom/google/maps/b/ay;

    .line 151
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/maps/b/ay;

    sget-object v1, Lcom/google/maps/b/ay;->a:Lcom/google/maps/b/ay;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/b/ay;->b:Lcom/google/maps/b/ay;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/b/ay;->c:Lcom/google/maps/b/ay;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/ay;->d:Lcom/google/maps/b/ay;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/b/ay;->e:Lcom/google/maps/b/ay;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/maps/b/ay;->g:[Lcom/google/maps/b/ay;

    .line 217
    new-instance v0, Lcom/google/maps/b/az;

    invoke-direct {v0}, Lcom/google/maps/b/az;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 227
    iput p3, p0, Lcom/google/maps/b/ay;->f:I

    .line 228
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/ay;
    .locals 1

    .prologue
    .line 202
    packed-switch p0, :pswitch_data_0

    .line 208
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 203
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/ay;->a:Lcom/google/maps/b/ay;

    goto :goto_0

    .line 204
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/ay;->b:Lcom/google/maps/b/ay;

    goto :goto_0

    .line 205
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/ay;->c:Lcom/google/maps/b/ay;

    goto :goto_0

    .line 206
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/ay;->d:Lcom/google/maps/b/ay;

    goto :goto_0

    .line 207
    :pswitch_4
    sget-object v0, Lcom/google/maps/b/ay;->e:Lcom/google/maps/b/ay;

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/ay;
    .locals 1

    .prologue
    .line 151
    const-class v0, Lcom/google/maps/b/ay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ay;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/ay;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/maps/b/ay;->g:[Lcom/google/maps/b/ay;

    invoke-virtual {v0}, [Lcom/google/maps/b/ay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/ay;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/google/maps/b/ay;->f:I

    return v0
.end method
