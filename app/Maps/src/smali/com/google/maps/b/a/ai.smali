.class public Lcom/google/maps/b/a/ai;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cr;

.field public b:Lcom/google/maps/b/a/cr;

.field public c:Lcom/google/maps/b/a/db;

.field public d:Lcom/google/maps/b/a/db;

.field public e:Lcom/google/maps/b/a/cr;

.field public f:Lcom/google/maps/b/a/cz;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    .line 22
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    .line 26
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/db;

    invoke-direct {v0, v4, v5}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    .line 30
    new-instance v0, Lcom/google/maps/b/a/db;

    invoke-direct {v0, v4, v5}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    .line 34
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v2}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    .line 38
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v2}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->j:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 42
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ai;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ai;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    .line 47
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    return-object v0

    .line 44
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(I)Z
    .locals 1

    .prologue
    .line 127
    packed-switch p1, :pswitch_data_0

    .line 133
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 131
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 139
    packed-switch p1, :pswitch_data_0

    .line 148
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 141
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 144
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->j:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 167
    packed-switch p1, :pswitch_data_0

    .line 179
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 169
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 175
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(IJ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 153
    packed-switch p1, :pswitch_data_0

    .line 162
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 155
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 158
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 56
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ai;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ai;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    .line 61
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    return-object v0

    .line 58
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method final c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 111
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 112
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 113
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->g:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 115
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->h:Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v4, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 117
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v4, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 118
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 119
    iput-object v1, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 121
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->j:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 123
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 84
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ai;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ai;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    .line 89
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    return-object v0

    .line 86
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ai;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/maps/b/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "layer_backend_spec: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\"\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/google/maps/b/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "layer_backend_feature_id: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\"\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-boolean v0, v0, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v0, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xd

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "cell_id: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "L\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-boolean v0, v0, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v0, :cond_3

    .line 195
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-wide v4, v0, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "fprint: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "L\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_4

    .line 198
    invoke-virtual {p0}, Lcom/google/maps/b/a/ai;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x11

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "display_name: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\"\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_5

    .line 201
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x18

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "is_search_result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->j:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_6

    .line 204
    iget-object v0, p0, Lcom/google/maps/b/a/ai;->j:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_8

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "feature_detection_priority: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v2

    .line 201
    goto :goto_0

    :cond_8
    move v1, v2

    .line 204
    goto :goto_1
.end method
