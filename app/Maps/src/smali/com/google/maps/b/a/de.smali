.class public Lcom/google/maps/b/a/de;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cs;

.field public b:Lcom/google/maps/b/a/cr;

.field public c:Lcom/google/maps/b/a/db;

.field public d:Lcom/google/maps/b/a/cz;

.field public e:Lcom/google/maps/b/a/cz;

.field public f:Lcom/google/maps/b/a/cz;

.field final g:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/v;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/google/maps/b/a/cr;

.field private i:Lcom/google/maps/b/a/v;

.field private j:Lcom/google/maps/b/a/ci;

.field private k:Lcom/google/maps/b/a/cz;

.field private l:Lcom/google/maps/b/a/ai;

.field private m:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 21
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    .line 23
    new-instance v0, Lcom/google/maps/b/a/v;

    invoke-direct {v0}, Lcom/google/maps/b/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->i:Lcom/google/maps/b/a/v;

    .line 25
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    .line 27
    new-instance v0, Lcom/google/maps/b/a/ci;

    invoke-direct {v0}, Lcom/google/maps/b/a/ci;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    .line 29
    new-instance v0, Lcom/google/maps/b/a/db;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    .line 31
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    .line 33
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    .line 35
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    .line 37
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->k:Lcom/google/maps/b/a/cz;

    .line 41
    new-instance v0, Lcom/google/maps/b/a/df;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/df;-><init>(Lcom/google/maps/b/a/de;)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->g:Lcom/google/maps/b/a/dd;

    .line 248
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    .line 249
    new-instance v0, Lcom/google/maps/b/a/ai;

    invoke-direct {v0}, Lcom/google/maps/b/a/ai;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    .line 250
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/de;->m:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/ci;
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ci;->c()V

    .line 70
    iget-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    .line 73
    :goto_0
    return-object v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    iget-object v1, p0, Lcom/google/maps/b/a/de;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/ci;->a([BII)V

    .line 73
    iget-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    goto :goto_0
.end method

.method protected final a(I)Z
    .locals 1

    .prologue
    .line 134
    packed-switch p1, :pswitch_data_0

    .line 140
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 138
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 146
    sparse-switch p1, :sswitch_data_0

    .line 164
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 148
    :sswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 151
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 154
    :sswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 157
    :sswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/de;->k:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 161
    :sswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/de;->m:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 146
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x7 -> :sswitch_3
        0x22d23f6 -> :sswitch_4
    .end sparse-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 181
    sparse-switch p1, :sswitch_data_0

    .line 193
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 183
    :sswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 186
    :sswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 190
    :sswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x335e2b3 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(IJ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 169
    packed-switch p1, :pswitch_data_0

    .line 176
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 171
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lcom/google/maps/b/a/ai;
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ai;->c()V

    .line 255
    iget-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    .line 258
    :goto_0
    return-object v0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    iget-object v1, p0, Lcom/google/maps/b/a/de;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/ai;->a([BII)V

    .line 258
    iget-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    goto :goto_0
.end method

.method public final b(I)Lcom/google/maps/b/a/v;
    .locals 4

    .prologue
    .line 62
    iget-object v1, p0, Lcom/google/maps/b/a/de;->i:Lcom/google/maps/b/a/v;

    iget-object v2, p0, Lcom/google/maps/b/a/de;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/v;->a([BII)V

    .line 63
    iget-object v0, p0, Lcom/google/maps/b/a/de;->i:Lcom/google/maps/b/a/v;

    return-object v0
.end method

.method final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 116
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 117
    iget-object v0, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    iput v4, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 118
    iget-object v0, p0, Lcom/google/maps/b/a/de;->i:Lcom/google/maps/b/a/v;

    invoke-virtual {v0}, Lcom/google/maps/b/a/v;->c()V

    .line 119
    iget-object v0, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 120
    iget-object v0, p0, Lcom/google/maps/b/a/de;->j:Lcom/google/maps/b/a/ci;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ci;->c()V

    .line 121
    iget-object v0, p0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v4, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 122
    iget-object v0, p0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 123
    iget-object v0, p0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 124
    iget-object v0, p0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 125
    iget-object v0, p0, Lcom/google/maps/b/a/de;->k:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 127
    iget-object v0, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 128
    iget-object v0, p0, Lcom/google/maps/b/a/de;->l:Lcom/google/maps/b/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ai;->c()V

    .line 129
    iget-object v0, p0, Lcom/google/maps/b/a/de;->m:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 130
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide v6, 0xffffffffL

    const/4 v1, 0x0

    .line 198
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 199
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_1

    .line 200
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/de;->b(I)Lcom/google/maps/b/a/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/v;->toString()Ljava/lang/String;

    move-result-object v2

    .line 201
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 202
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 203
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 205
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "extruded_area {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 208
    invoke-virtual {p0}, Lcom/google/maps/b/a/de;->a()Lcom/google/maps/b/a/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/ci;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    const-string v2, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 210
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 211
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 213
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "mesh {\n  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iget-boolean v0, v0, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v0, :cond_4

    .line 216
    iget-object v0, p0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v0, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "base_tile_op_id: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "L\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_5

    .line 219
    iget-object v0, p0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x24

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "multi_zoom_style_index: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_6

    .line 222
    iget-object v0, p0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x15

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "z_grade: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_7

    .line 225
    iget-object v0, p0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v4, v0

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "z_within_grade: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/a/de;->k:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_8

    .line 228
    iget-object v0, p0, Lcom/google/maps/b/a/de;->k:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-long v4, v0

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "legacy_base_tile_op_id: 0x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_8
    iget-object v0, p0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_a

    .line 232
    invoke-virtual {p0}, Lcom/google/maps/b/a/de;->b()Lcom/google/maps/b/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/ai;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    const-string v2, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 234
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 235
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 237
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3c

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "[maps_paint_client.interactivity_metadata_for_volume] {\n  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    :cond_a
    iget-object v0, p0, Lcom/google/maps/b/a/de;->m:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_c

    .line 240
    iget-object v0, p0, Lcom/google/maps/b/a/de;->m:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_b

    const/4 v1, 0x1

    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[maps_paint_client.has_indoor_content]: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
