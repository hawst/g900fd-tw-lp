.class public Lcom/google/maps/b/a/bv;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field a:Lcom/google/maps/b/a/cs;

.field final b:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/maps/b/a/bu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 9
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    .line 11
    new-instance v0, Lcom/google/maps/b/a/bu;

    invoke-direct {v0}, Lcom/google/maps/b/a/bu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bv;->c:Lcom/google/maps/b/a/bu;

    .line 15
    new-instance v0, Lcom/google/maps/b/a/bw;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/bw;-><init>(Lcom/google/maps/b/a/bv;)V

    iput-object v0, p0, Lcom/google/maps/b/a/bv;->b:Lcom/google/maps/b/a/dd;

    return-void
.end method


# virtual methods
.method protected final a(III)Z
    .locals 1

    .prologue
    .line 50
    packed-switch p1, :pswitch_data_0

    .line 56
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 52
    :pswitch_0
    iget-object v0, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v0, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    .line 53
    const/4 v0, 0x1

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/bu;
    .locals 4

    .prologue
    .line 36
    iget-object v1, p0, Lcom/google/maps/b/a/bv;->c:Lcom/google/maps/b/a/bu;

    iget-object v2, p0, Lcom/google/maps/b/a/bv;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/bu;->a([BII)V

    .line 37
    iget-object v0, p0, Lcom/google/maps/b/a/bv;->c:Lcom/google/maps/b/a/bu;

    return-object v0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 43
    iget-object v0, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 44
    iget-object v0, p0, Lcom/google/maps/b/a/bv;->c:Lcom/google/maps/b/a/bu;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bu;->c()V

    .line 46
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 61
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 62
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/bv;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_1

    .line 63
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/bv;->b(I)Lcom/google/maps/b/a/bu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/bu;->toString()Ljava/lang/String;

    move-result-object v2

    .line 64
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 65
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 68
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "raster_op {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
