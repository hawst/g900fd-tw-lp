.class public Lcom/google/maps/b/a/ca;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/da;

.field private c:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/da;

    invoke-direct {v0}, Lcom/google/maps/b/a/da;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ca;->c:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 62
    packed-switch p1, :pswitch_data_0

    .line 71
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 64
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ca;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 6

    .prologue
    .line 76
    packed-switch p1, :pswitch_data_0

    .line 82
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->t:[B

    iget-object v1, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    new-instance v2, Lcom/google/maps/b/a/dc;

    invoke-direct {v2, p2}, Lcom/google/maps/b/a/dc;-><init>(I)V

    add-int v3, p2, p3

    :goto_1
    iget v4, v2, Lcom/google/maps/b/a/dc;->a:I

    if-ge v4, v3, :cond_0

    invoke-static {v0, v2}, Lcom/google/maps/b/a/cq;->b([BLcom/google/maps/b/a/dc;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/maps/b/a/cv;->a(J)V

    goto :goto_1

    .line 79
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 54
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 55
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    iput v2, v0, Lcom/google/maps/b/a/da;->b:I

    .line 56
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 58
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    iget v2, v2, Lcom/google/maps/b/a/da;->b:I

    if-ge v0, v2, :cond_1

    .line 92
    iget-object v2, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    iget-object v2, v2, Lcom/google/maps/b/a/da;->a:[J

    aget-wide v2, v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "base_tile_op_id: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "L\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "multi_zoom_style_index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
