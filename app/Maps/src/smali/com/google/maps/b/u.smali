.class public final Lcom/google/maps/b/u;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/s;",
        "Lcom/google/maps/b/u;",
        ">;",
        "Lcom/google/maps/b/v;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 473
    sget-object v0, Lcom/google/maps/b/s;->j:Lcom/google/maps/b/s;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 690
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/maps/b/u;->d:I

    .line 722
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/u;->e:Lcom/google/n/ao;

    .line 781
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/u;->f:Lcom/google/n/ao;

    .line 841
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    .line 977
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/u;->h:Lcom/google/n/ao;

    .line 1036
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/u;->i:Lcom/google/n/ao;

    .line 474
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/s;)Lcom/google/maps/b/u;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 550
    invoke-static {}, Lcom/google/maps/b/s;->d()Lcom/google/maps/b/s;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 587
    :goto_0
    return-object p0

    .line 551
    :cond_0
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 552
    iget v2, p1, Lcom/google/maps/b/s;->b:I

    iget v3, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/u;->a:I

    iput v2, p0, Lcom/google/maps/b/u;->b:I

    .line 554
    :cond_1
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 555
    iget v2, p1, Lcom/google/maps/b/s;->c:I

    iget v3, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/u;->a:I

    iput v2, p0, Lcom/google/maps/b/u;->c:I

    .line 557
    :cond_2
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 558
    iget v2, p1, Lcom/google/maps/b/s;->d:I

    iget v3, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/u;->a:I

    iput v2, p0, Lcom/google/maps/b/u;->d:I

    .line 560
    :cond_3
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 561
    iget-object v2, p0, Lcom/google/maps/b/u;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 562
    iget v2, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/u;->a:I

    .line 564
    :cond_4
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 565
    iget-object v2, p0, Lcom/google/maps/b/u;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 566
    iget v2, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/u;->a:I

    .line 568
    :cond_5
    iget-object v2, p1, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 569
    iget-object v2, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 570
    iget-object v2, p1, Lcom/google/maps/b/s;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    .line 571
    iget v2, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/b/u;->a:I

    .line 578
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 579
    iget-object v2, p0, Lcom/google/maps/b/u;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 580
    iget v2, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/u;->a:I

    .line 582
    :cond_7
    iget v2, p1, Lcom/google/maps/b/s;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 583
    iget-object v0, p0, Lcom/google/maps/b/u;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 584
    iget v0, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/b/u;->a:I

    .line 586
    :cond_8
    iget-object v0, p1, Lcom/google/maps/b/s;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 551
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 554
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 557
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 560
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 564
    goto :goto_5

    .line 573
    :cond_e
    iget v2, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/u;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/b/u;->a:I

    .line 574
    :cond_f
    iget-object v2, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/s;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_10
    move v2, v1

    .line 578
    goto :goto_7

    :cond_11
    move v0, v1

    .line 582
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465
    new-instance v2, Lcom/google/maps/b/s;

    invoke-direct {v2, p0}, Lcom/google/maps/b/s;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/maps/b/u;->b:I

    iput v4, v2, Lcom/google/maps/b/s;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/maps/b/u;->c:I

    iput v4, v2, Lcom/google/maps/b/s;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/b/u;->d:I

    iput v4, v2, Lcom/google/maps/b/s;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/maps/b/s;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/u;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/u;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/maps/b/s;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/u;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/u;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/b/u;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/s;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, v2, Lcom/google/maps/b/s;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/u;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/u;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v3, v2, Lcom/google/maps/b/s;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/b/u;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/b/u;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/b/s;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 465
    check-cast p1, Lcom/google/maps/b/s;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/u;->a(Lcom/google/maps/b/s;)Lcom/google/maps/b/u;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 591
    iget v0, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 592
    iget-object v0, p0, Lcom/google/maps/b/u;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ck;->d()Lcom/google/maps/b/ck;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ck;

    invoke-virtual {v0}, Lcom/google/maps/b/ck;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 621
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 591
    goto :goto_0

    .line 597
    :cond_2
    iget v0, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 598
    iget-object v0, p0, Lcom/google/maps/b/u;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 603
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 604
    iget-object v0, p0, Lcom/google/maps/b/u;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 597
    goto :goto_2

    .line 609
    :cond_5
    iget v0, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 610
    iget-object v0, p0, Lcom/google/maps/b/u;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    :cond_6
    iget v0, p0, Lcom/google/maps/b/u;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    .line 616
    iget-object v0, p0, Lcom/google/maps/b/u;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v3

    .line 621
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 609
    goto :goto_4

    :cond_9
    move v0, v2

    .line 615
    goto :goto_5
.end method
