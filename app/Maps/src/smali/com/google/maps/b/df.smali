.class public final Lcom/google/maps/b/df;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/di;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/df;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/maps/b/df;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 890
    new-instance v0, Lcom/google/maps/b/dg;

    invoke-direct {v0}, Lcom/google/maps/b/dg;-><init>()V

    sput-object v0, Lcom/google/maps/b/df;->PARSER:Lcom/google/n/ax;

    .line 1037
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/df;->g:Lcom/google/n/aw;

    .line 1330
    new-instance v0, Lcom/google/maps/b/df;

    invoke-direct {v0}, Lcom/google/maps/b/df;-><init>()V

    sput-object v0, Lcom/google/maps/b/df;->d:Lcom/google/maps/b/df;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 839
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 990
    iput-byte v0, p0, Lcom/google/maps/b/df;->e:B

    .line 1016
    iput v0, p0, Lcom/google/maps/b/df;->f:I

    .line 840
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;

    .line 841
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;

    .line 842
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 848
    invoke-direct {p0}, Lcom/google/maps/b/df;-><init>()V

    .line 849
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 853
    const/4 v0, 0x0

    .line 854
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 855
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 856
    sparse-switch v3, :sswitch_data_0

    .line 861
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 863
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 859
    goto :goto_0

    .line 868
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 869
    iget v4, p0, Lcom/google/maps/b/df;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/b/df;->a:I

    .line 870
    iput-object v3, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 881
    :catch_0
    move-exception v0

    .line 882
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 887
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/df;->au:Lcom/google/n/bn;

    throw v0

    .line 874
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 875
    iget v4, p0, Lcom/google/maps/b/df;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/b/df;->a:I

    .line 876
    iput-object v3, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 883
    :catch_1
    move-exception v0

    .line 884
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 885
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 887
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/df;->au:Lcom/google/n/bn;

    .line 888
    return-void

    .line 856
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 837
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 990
    iput-byte v0, p0, Lcom/google/maps/b/df;->e:B

    .line 1016
    iput v0, p0, Lcom/google/maps/b/df;->f:I

    .line 838
    return-void
.end method

.method public static d()Lcom/google/maps/b/df;
    .locals 1

    .prologue
    .line 1333
    sget-object v0, Lcom/google/maps/b/df;->d:Lcom/google/maps/b/df;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/dh;
    .locals 1

    .prologue
    .line 1099
    new-instance v0, Lcom/google/maps/b/dh;

    invoke-direct {v0}, Lcom/google/maps/b/dh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/df;",
            ">;"
        }
    .end annotation

    .prologue
    .line 902
    sget-object v0, Lcom/google/maps/b/df;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 1006
    invoke-virtual {p0}, Lcom/google/maps/b/df;->c()I

    .line 1007
    iget v0, p0, Lcom/google/maps/b/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1010
    :cond_0
    iget v0, p0, Lcom/google/maps/b/df;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1011
    iget-object v0, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1013
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/df;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1014
    return-void

    .line 1008
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1011
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 992
    iget-byte v2, p0, Lcom/google/maps/b/df;->e:B

    .line 993
    if-ne v2, v0, :cond_0

    .line 1001
    :goto_0
    return v0

    .line 994
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 996
    :cond_1
    iget v2, p0, Lcom/google/maps/b/df;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 997
    iput-byte v1, p0, Lcom/google/maps/b/df;->e:B

    move v0, v1

    .line 998
    goto :goto_0

    :cond_2
    move v2, v1

    .line 996
    goto :goto_1

    .line 1000
    :cond_3
    iput-byte v0, p0, Lcom/google/maps/b/df;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1018
    iget v0, p0, Lcom/google/maps/b/df;->f:I

    .line 1019
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1032
    :goto_0
    return v0

    .line 1022
    :cond_0
    iget v0, p0, Lcom/google/maps/b/df;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1024
    iget-object v0, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/df;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1026
    :goto_2
    iget v0, p0, Lcom/google/maps/b/df;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1028
    iget-object v0, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/df;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1030
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/df;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1031
    iput v0, p0, Lcom/google/maps/b/df;->f:I

    goto :goto_0

    .line 1024
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1028
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 831
    invoke-static {}, Lcom/google/maps/b/df;->newBuilder()Lcom/google/maps/b/dh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/dh;->a(Lcom/google/maps/b/df;)Lcom/google/maps/b/dh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 831
    invoke-static {}, Lcom/google/maps/b/df;->newBuilder()Lcom/google/maps/b/dh;

    move-result-object v0

    return-object v0
.end method
