.class public Lcom/google/maps/b/a/ak;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cr;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cz;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/maps/b/a/cy;

.field private f:Lcom/google/maps/b/a/cy;

.field private g:Lcom/google/maps/b/a/cy;

.field private h:Lcom/google/maps/b/a/cy;

.field private i:Lcom/google/maps/b/a/cy;

.field private j:Lcom/google/maps/b/a/cy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    .line 30
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->e:Lcom/google/maps/b/a/cy;

    .line 34
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->f:Lcom/google/maps/b/a/cy;

    .line 36
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->g:Lcom/google/maps/b/a/cy;

    .line 38
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->h:Lcom/google/maps/b/a/cy;

    .line 40
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->i:Lcom/google/maps/b/a/cy;

    .line 42
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->j:Lcom/google/maps/b/a/cy;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 46
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ak;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    .line 51
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    return-object v0

    .line 48
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 149
    packed-switch p1, :pswitch_data_0

    .line 158
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 151
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 154
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 163
    packed-switch p1, :pswitch_data_0

    .line 187
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 165
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 168
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->e:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 171
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->f:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 174
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->g:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cx;)V

    goto :goto_0

    .line 177
    :pswitch_5
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->h:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cx;)V

    goto :goto_0

    .line 180
    :pswitch_6
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->i:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cx;)V

    goto :goto_0

    .line 183
    :pswitch_7
    iget-object v1, p0, Lcom/google/maps/b/a/ak;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ak;->j:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/ak;->a([BIILcom/google/maps/b/a/cx;)V

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 134
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/ak;->d:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 137
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 138
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->e:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 139
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->f:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 140
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->g:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 141
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->h:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 142
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->i:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 143
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->j:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 145
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x22

    const/16 v6, 0x1a

    const/4 v1, 0x0

    .line 192
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 193
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/google/maps/b/a/ak;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "text: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x24

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "multi_zoom_style_index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "line_break: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v0, v1

    .line 202
    :goto_1
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->e:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_4

    .line 203
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->e:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sprite_index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 200
    goto :goto_0

    :cond_4
    move v0, v1

    .line 205
    :goto_2
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->f:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_5

    .line 206
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->f:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "outline_sprite_index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 208
    :goto_3
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->g:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_6

    .line 209
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->g:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sprite_x_pos: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v1

    .line 211
    :goto_4
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->h:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_7

    .line 212
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->h:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sprite_y_pos: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    move v0, v1

    .line 214
    :goto_5
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->i:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_8

    .line 215
    iget-object v3, p0, Lcom/google/maps/b/a/ak;->i:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "outline_sprite_x_pos: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 217
    :cond_8
    :goto_6
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->j:Lcom/google/maps/b/a/cy;

    iget v0, v0, Lcom/google/maps/b/a/cy;->b:I

    if-ge v1, v0, :cond_9

    .line 218
    iget-object v0, p0, Lcom/google/maps/b/a/ak;->j:Lcom/google/maps/b/a/cy;

    iget-object v0, v0, Lcom/google/maps/b/a/cy;->a:[I

    aget v0, v0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "outline_sprite_y_pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 221
    :cond_9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
