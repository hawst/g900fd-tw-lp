.class public Lcom/google/maps/b/a/dh;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field a:Lcom/google/maps/b/a/cs;

.field final b:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/de;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/maps/b/a/de;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Lcom/google/maps/b/a/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 11
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    .line 13
    new-instance v0, Lcom/google/maps/b/a/de;

    invoke-direct {v0}, Lcom/google/maps/b/a/de;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dh;->c:Lcom/google/maps/b/a/de;

    .line 15
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    .line 17
    new-instance v0, Lcom/google/maps/b/a/x;

    invoke-direct {v0}, Lcom/google/maps/b/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    .line 21
    new-instance v0, Lcom/google/maps/b/a/di;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/di;-><init>(Lcom/google/maps/b/a/dh;)V

    iput-object v0, p0, Lcom/google/maps/b/a/dh;->b:Lcom/google/maps/b/a/dd;

    return-void
.end method


# virtual methods
.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 71
    packed-switch p1, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 73
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/de;
    .locals 4

    .prologue
    .line 42
    iget-object v1, p0, Lcom/google/maps/b/a/dh;->c:Lcom/google/maps/b/a/de;

    iget-object v2, p0, Lcom/google/maps/b/a/dh;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/de;->a([BII)V

    .line 43
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->c:Lcom/google/maps/b/a/de;

    return-object v0
.end method

.method final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 62
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    iput v1, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 63
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->c:Lcom/google/maps/b/a/de;

    invoke-virtual {v0}, Lcom/google/maps/b/a/de;->c()V

    .line 64
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 65
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    invoke-virtual {v0}, Lcom/google/maps/b/a/x;->c()V

    .line 67
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 85
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 86
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/dh;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_1

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/dh;->b(I)Lcom/google/maps/b/a/de;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/de;->toString()Ljava/lang/String;

    move-result-object v2

    .line 88
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 89
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 90
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 92
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "volume_op {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    invoke-virtual {v0}, Lcom/google/maps/b/a/x;->c()V

    iget-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/b/a/x;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    const-string v2, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 97
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 100
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "geometry_stats {\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 95
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    iget-object v2, p0, Lcom/google/maps/b/a/dh;->t:[B

    iget-object v4, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->a:I

    iget-object v5, p0, Lcom/google/maps/b/a/dh;->d:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/maps/b/a/x;->a([BII)V

    iget-object v0, p0, Lcom/google/maps/b/a/dh;->e:Lcom/google/maps/b/a/x;

    goto :goto_1
.end method
