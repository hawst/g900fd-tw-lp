.class public final Lcom/google/maps/b/ag;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/aj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ag;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/maps/b/ag;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:J

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field j:F

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field l:Z

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/maps/b/ah;

    invoke-direct {v0}, Lcom/google/maps/b/ah;-><init>()V

    sput-object v0, Lcom/google/maps/b/ag;->PARSER:Lcom/google/n/ax;

    .line 634
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/ag;->p:Lcom/google/n/aw;

    .line 1564
    new-instance v0, Lcom/google/maps/b/ag;

    invoke-direct {v0}, Lcom/google/maps/b/ag;-><init>()V

    sput-object v0, Lcom/google/maps/b/ag;->m:Lcom/google/maps/b/ag;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 523
    iput-byte v0, p0, Lcom/google/maps/b/ag;->n:B

    .line 572
    iput v0, p0, Lcom/google/maps/b/ag;->o:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/b/ag;->d:J

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/ag;->j:F

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/b/ag;->l:Z

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/maps/b/ag;-><init>()V

    .line 36
    const/4 v1, 0x0

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 40
    const/4 v0, 0x0

    move v2, v0

    .line 41
    :cond_0
    :goto_0
    if-nez v2, :cond_8

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 43
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 45
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 57
    iput-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_1

    .line 141
    iget-object v1, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 143
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ag;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 63
    iput-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 138
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/ag;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/maps/b/ag;->d:J

    goto :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 74
    iput-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    goto :goto_0

    .line 78
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 79
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 80
    iput-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    goto :goto_0

    .line 84
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 86
    iput-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 91
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 92
    iput-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 96
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 97
    iget v4, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/maps/b/ag;->a:I

    .line 98
    iput-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 102
    :sswitch_9
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/maps/b/ag;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/maps/b/ag;->j:F

    goto/16 :goto_0

    .line 107
    :sswitch_a
    and-int/lit16 v0, v1, 0x200

    const/16 v4, 0x200

    if-eq v0, v4, :cond_2

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 109
    or-int/lit16 v1, v1, 0x200

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 115
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 116
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 117
    and-int/lit16 v0, v1, 0x200

    const/16 v5, 0x200

    if-eq v0, v5, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_4

    const/4 v0, -0x1

    :goto_1
    if-lez v0, :cond_3

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 119
    or-int/lit16 v1, v1, 0x200

    .line 121
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_5

    const/4 v0, -0x1

    :goto_3
    if-lez v0, :cond_6

    .line 122
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 117
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_1

    .line 121
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_3

    .line 124
    :cond_6
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 128
    :sswitch_c
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/maps/b/ag;->a:I

    .line 129
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/maps/b/ag;->l:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 140
    :cond_8
    and-int/lit16 v0, v1, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 141
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 143
    :cond_9
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->au:Lcom/google/n/bn;

    .line 144
    return-void

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x50 -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 523
    iput-byte v0, p0, Lcom/google/maps/b/ag;->n:B

    .line 572
    iput v0, p0, Lcom/google/maps/b/ag;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/ag;
    .locals 1

    .prologue
    .line 1567
    sget-object v0, Lcom/google/maps/b/ag;->m:Lcom/google/maps/b/ag;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/ai;
    .locals 1

    .prologue
    .line 696
    new-instance v0, Lcom/google/maps/b/ai;

    invoke-direct {v0}, Lcom/google/maps/b/ai;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    sget-object v0, Lcom/google/maps/b/ag;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 535
    invoke-virtual {p0}, Lcom/google/maps/b/ag;->c()I

    .line 536
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 539
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 540
    iget-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 542
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 543
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/maps/b/ag;->d:J

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->c(J)V

    .line 545
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 546
    iget-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 548
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 549
    iget-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 551
    :cond_4
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 552
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 554
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 555
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 557
    :cond_6
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 558
    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 560
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 561
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/maps/b/ag;->j:F

    invoke-static {v0, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    :cond_8
    move v1, v2

    .line 563
    :goto_7
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 564
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_10

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 563
    :goto_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 537
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 540
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 546
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 549
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 552
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 555
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 558
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 564
    :cond_10
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    .line 566
    :cond_11
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_13

    .line 567
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/maps/b/ag;->l:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_12

    move v2, v3

    :cond_12
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 569
    :cond_13
    iget-object v0, p0, Lcom/google/maps/b/ag;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 570
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 525
    iget-byte v1, p0, Lcom/google/maps/b/ag;->n:B

    .line 526
    if-ne v1, v0, :cond_0

    .line 530
    :goto_0
    return v0

    .line 527
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 529
    :cond_1
    iput-byte v0, p0, Lcom/google/maps/b/ag;->n:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 574
    iget v0, p0, Lcom/google/maps/b/ag;->o:I

    .line 575
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 629
    :goto_0
    return v0

    .line 578
    :cond_0
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_13

    .line 580
    iget-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 582
    :goto_2
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 584
    iget-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 586
    :cond_1
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 587
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/maps/b/ag;->d:J

    .line 588
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v1, v0

    .line 590
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 592
    iget-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 594
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 595
    const/4 v3, 0x5

    .line 596
    iget-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 598
    :cond_4
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 599
    const/4 v3, 0x6

    .line 600
    iget-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 602
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 603
    const/4 v3, 0x7

    .line 604
    iget-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->h:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 606
    :cond_6
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 608
    iget-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ag;->i:Ljava/lang/Object;

    :goto_8
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 610
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 611
    const/16 v0, 0x9

    iget v3, p0, Lcom/google/maps/b/ag;->j:F

    .line 612
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    :cond_8
    move v3, v2

    move v4, v2

    .line 616
    :goto_9
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_11

    .line 617
    iget-object v0, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    .line 618
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_10

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v4, v0

    .line 616
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_9

    .line 580
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 584
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 592
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 596
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 600
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 604
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 608
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 618
    :cond_10
    const/16 v0, 0xa

    goto :goto_a

    .line 620
    :cond_11
    add-int v0, v1, v4

    .line 621
    iget-object v1, p0, Lcom/google/maps/b/ag;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 623
    iget v1, p0, Lcom/google/maps/b/ag;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_12

    .line 624
    const/16 v1, 0xb

    iget-boolean v3, p0, Lcom/google/maps/b/ag;->l:Z

    .line 625
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 627
    :cond_12
    iget-object v1, p0, Lcom/google/maps/b/ag;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 628
    iput v0, p0, Lcom/google/maps/b/ag;->o:I

    goto/16 :goto_0

    :cond_13
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ag;->newBuilder()Lcom/google/maps/b/ai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/ai;->a(Lcom/google/maps/b/ag;)Lcom/google/maps/b/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/ag;->newBuilder()Lcom/google/maps/b/ai;

    move-result-object v0

    return-object v0
.end method
