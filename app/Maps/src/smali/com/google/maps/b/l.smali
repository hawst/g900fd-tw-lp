.class public final enum Lcom/google/maps/b/l;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/b/l;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/maps/b/l;

.field public static final enum b:Lcom/google/maps/b/l;

.field public static final enum c:Lcom/google/maps/b/l;

.field public static final enum d:Lcom/google/maps/b/l;

.field private static final synthetic f:[Lcom/google/maps/b/l;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    new-instance v0, Lcom/google/maps/b/l;

    const-string v1, "ICON_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/maps/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/l;->a:Lcom/google/maps/b/l;

    .line 102
    new-instance v0, Lcom/google/maps/b/l;

    const-string v1, "ICON_TYPE_PNG"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/maps/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    .line 106
    new-instance v0, Lcom/google/maps/b/l;

    const-string v1, "ICON_TYPE_NINEPATCH"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/maps/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/l;->c:Lcom/google/maps/b/l;

    .line 110
    new-instance v0, Lcom/google/maps/b/l;

    const-string v1, "ICON_TYPE_COMPILED_NINEPATCH"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/maps/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    .line 93
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/maps/b/l;

    sget-object v1, Lcom/google/maps/b/l;->a:Lcom/google/maps/b/l;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/maps/b/l;->c:Lcom/google/maps/b/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/maps/b/l;->f:[Lcom/google/maps/b/l;

    .line 150
    new-instance v0, Lcom/google/maps/b/m;

    invoke-direct {v0}, Lcom/google/maps/b/m;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 160
    iput p3, p0, Lcom/google/maps/b/l;->e:I

    .line 161
    return-void
.end method

.method public static a(I)Lcom/google/maps/b/l;
    .locals 1

    .prologue
    .line 136
    packed-switch p0, :pswitch_data_0

    .line 141
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 137
    :pswitch_0
    sget-object v0, Lcom/google/maps/b/l;->a:Lcom/google/maps/b/l;

    goto :goto_0

    .line 138
    :pswitch_1
    sget-object v0, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    goto :goto_0

    .line 139
    :pswitch_2
    sget-object v0, Lcom/google/maps/b/l;->c:Lcom/google/maps/b/l;

    goto :goto_0

    .line 140
    :pswitch_3
    sget-object v0, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/b/l;
    .locals 1

    .prologue
    .line 93
    const-class v0, Lcom/google/maps/b/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/l;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/b/l;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/google/maps/b/l;->f:[Lcom/google/maps/b/l;

    invoke-virtual {v0}, [Lcom/google/maps/b/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/b/l;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/google/maps/b/l;->e:I

    return v0
.end method
