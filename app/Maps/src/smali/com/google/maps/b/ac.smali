.class public final Lcom/google/maps/b/ac;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/w;",
        "Lcom/google/maps/b/ac;",
        ">;",
        "Lcom/google/maps/b/af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1232
    sget-object v0, Lcom/google/maps/b/w;->j:Lcom/google/maps/b/w;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1377
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/ac;->b:I

    .line 1413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/ac;->c:Ljava/lang/Object;

    .line 1522
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    .line 1658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ac;->f:Lcom/google/n/ao;

    .line 1717
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    .line 1815
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/ac;->i:Lcom/google/n/ao;

    .line 1233
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/w;)Lcom/google/maps/b/ac;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1306
    invoke-static {}, Lcom/google/maps/b/w;->d()Lcom/google/maps/b/w;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1350
    :goto_0
    return-object p0

    .line 1307
    :cond_0
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1308
    iget v2, p1, Lcom/google/maps/b/w;->b:I

    invoke-static {v2}, Lcom/google/maps/b/ad;->a(I)Lcom/google/maps/b/ad;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/b/ad;->a:Lcom/google/maps/b/ad;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1307
    goto :goto_1

    .line 1308
    :cond_3
    iget v3, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/ac;->a:I

    iget v2, v2, Lcom/google/maps/b/ad;->h:I

    iput v2, p0, Lcom/google/maps/b/ac;->b:I

    .line 1310
    :cond_4
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1311
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1312
    iget-object v2, p1, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/ac;->c:Ljava/lang/Object;

    .line 1315
    :cond_5
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1316
    iget v2, p1, Lcom/google/maps/b/w;->d:I

    iget v3, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/ac;->a:I

    iput v2, p0, Lcom/google/maps/b/ac;->d:I

    .line 1318
    :cond_6
    iget-object v2, p1, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1319
    iget-object v2, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1320
    iget-object v2, p1, Lcom/google/maps/b/w;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    .line 1321
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1328
    :cond_7
    :goto_4
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 1329
    iget-object v2, p0, Lcom/google/maps/b/ac;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1330
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1332
    :cond_8
    iget-object v2, p1, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1333
    iget-object v2, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1334
    iget-object v2, p1, Lcom/google/maps/b/w;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    .line 1335
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1342
    :cond_9
    :goto_6
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 1343
    iget-boolean v2, p1, Lcom/google/maps/b/w;->h:Z

    iget v3, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/b/ac;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/ac;->h:Z

    .line 1345
    :cond_a
    iget v2, p1, Lcom/google/maps/b/w;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v5, :cond_14

    :goto_8
    if-eqz v0, :cond_b

    .line 1346
    iget-object v0, p0, Lcom/google/maps/b/ac;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1347
    iget v0, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/maps/b/ac;->a:I

    .line 1349
    :cond_b
    iget-object v0, p1, Lcom/google/maps/b/w;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 1310
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 1315
    goto/16 :goto_3

    .line 1323
    :cond_e
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1324
    :cond_f
    iget-object v2, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/w;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 1328
    goto/16 :goto_5

    .line 1337
    :cond_11
    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v5, :cond_12

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/maps/b/ac;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/maps/b/ac;->a:I

    .line 1338
    :cond_12
    iget-object v2, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/maps/b/w;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_13
    move v2, v1

    .line 1342
    goto :goto_7

    :cond_14
    move v0, v1

    .line 1345
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1224
    new-instance v2, Lcom/google/maps/b/w;

    invoke-direct {v2, p0}, Lcom/google/maps/b/w;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/maps/b/ac;->b:I

    iput v4, v2, Lcom/google/maps/b/w;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/maps/b/ac;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/b/w;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/b/ac;->d:I

    iput v4, v2, Lcom/google/maps/b/w;->d:I

    iget v4, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/maps/b/ac;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/w;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/maps/b/w;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/ac;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/ac;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/maps/b/ac;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/maps/b/ac;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/maps/b/w;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-boolean v4, p0, Lcom/google/maps/b/ac;->h:Z

    iput-boolean v4, v2, Lcom/google/maps/b/w;->h:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v3, v2, Lcom/google/maps/b/w;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/b/ac;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/b/ac;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/b/w;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1224
    check-cast p1, Lcom/google/maps/b/w;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/ac;->a(Lcom/google/maps/b/w;)Lcom/google/maps/b/ac;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1354
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1355
    iget-object v0, p0, Lcom/google/maps/b/ac;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/y;->d()Lcom/google/maps/b/y;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/y;

    invoke-virtual {v0}, Lcom/google/maps/b/y;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1372
    :cond_0
    :goto_1
    return v2

    .line 1354
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1360
    :cond_2
    iget v0, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1361
    iget-object v0, p0, Lcom/google/maps/b/ac;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/au;->d()Lcom/google/maps/b/au;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/au;

    invoke-virtual {v0}, Lcom/google/maps/b/au;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1366
    :cond_3
    iget v0, p0, Lcom/google/maps/b/ac;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1367
    iget-object v0, p0, Lcom/google/maps/b/ac;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 1372
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1360
    goto :goto_2

    :cond_6
    move v0, v2

    .line 1366
    goto :goto_3
.end method
