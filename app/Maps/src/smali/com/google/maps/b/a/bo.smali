.class public Lcom/google/maps/b/a/bo;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cy;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cy;

.field public d:Lcom/google/maps/b/a/cz;

.field public e:Lcom/google/maps/b/a/cy;

.field private f:Lcom/google/maps/b/a/cr;

.field private g:Lcom/google/maps/b/a/cu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    .line 30
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 34
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bo;->t:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v2, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 37
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    .line 40
    :goto_0
    return-object v0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bo;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v3, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 40
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 105
    packed-switch p1, :pswitch_data_0

    .line 114
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 107
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 110
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 119
    packed-switch p1, :pswitch_data_0

    .line 134
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 121
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 124
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/bo;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 127
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/bo;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 130
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/bo;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    invoke-static {v1, p2, p3, v2}, Lcom/google/maps/b/a/bo;->a([BIILcom/google/maps/b/a/cy;)V

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 93
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 94
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->g:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 95
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 96
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 97
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 98
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 99
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 101
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x1c

    const/4 v1, 0x0

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/google/maps/b/a/bo;->a()Lcom/google/maps/b/a/cu;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "loop_data: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move v0, v1

    .line 143
    :goto_0
    iget-object v3, p0, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_1

    .line 144
    iget-object v3, p0, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x18

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "loop_break: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "vertex_count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v0, v1

    .line 149
    :goto_1
    iget-object v3, p0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_3

    .line 150
    iget-object v3, p0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iget-object v3, v3, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "triangle_index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_4

    .line 153
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "triangle_fan_start_index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget v0, v0, Lcom/google/maps/b/a/cy;->b:I

    if-ge v1, v0, :cond_5

    .line 156
    iget-object v0, p0, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget-object v0, v0, Lcom/google/maps/b/a/cy;->a:[I

    aget v0, v0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "unstroked_edge: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 159
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
