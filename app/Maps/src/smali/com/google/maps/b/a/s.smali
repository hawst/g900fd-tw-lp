.class public Lcom/google/maps/b/a/s;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/maps/b/a/cz;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/maps/b/a/cr;

.field private g:Lcom/google/maps/b/a/ay;

.field private h:Lcom/google/maps/b/a/cz;

.field private i:Lcom/google/maps/b/a/cz;

.field private j:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 54
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    .line 56
    iput-object v2, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    .line 58
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->c:Lcom/google/maps/b/a/cz;

    .line 60
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    .line 62
    iput-object v2, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    .line 66
    new-instance v0, Lcom/google/maps/b/a/ay;

    invoke-direct {v0}, Lcom/google/maps/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    .line 68
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->h:Lcom/google/maps/b/a/cz;

    .line 70
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->i:Lcom/google/maps/b/a/cz;

    .line 72
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->j:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 97
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/s;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/s;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    .line 102
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    return-object v0

    .line 99
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 175
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 162
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/s;->c:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 165
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/s;->h:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 168
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/s;->i:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 171
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/s;->j:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 180
    packed-switch p1, :pswitch_data_0

    .line 192
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 182
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 185
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 188
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 144
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 145
    iget-object v0, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 146
    iput-object v3, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    .line 147
    iget-object v0, p0, Lcom/google/maps/b/a/s;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 148
    iget-object v0, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 149
    iput-object v3, p0, Lcom/google/maps/b/a/s;->e:Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 151
    iget-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ay;->c()V

    .line 152
    iget-object v0, p0, Lcom/google/maps/b/a/s;->h:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 153
    iget-object v0, p0, Lcom/google/maps/b/a/s;->i:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 154
    iget-object v0, p0, Lcom/google/maps/b/a/s;->j:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 156
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    iget-object v0, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/maps/b/a/s;->t:[B

    iget-object v5, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->a:I

    iget-object v6, p0, Lcom/google/maps/b/a/s;->a:Lcom/google/maps/b/a/cr;

    iget v6, v6, Lcom/google/maps/b/a/cr;->b:I

    sget-object v7, Lcom/google/maps/b/a/s;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v4, v5, v6, v7}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xf

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "feature_id: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\"\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/s;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/google/maps/b/a/s;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x1f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "feature_id_format: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/s;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 205
    invoke-virtual {p0}, Lcom/google/maps/b/a/s;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xa

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "query: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\"\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_5

    .line 208
    iget-object v0, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ay;->c()V

    iget-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    :goto_1
    invoke-virtual {v0}, Lcom/google/maps/b/a/ay;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 210
    const-string v4, "  "

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 211
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 213
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "point {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "}\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/s;->h:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_6

    .line 216
    iget-object v0, p0, Lcom/google/maps/b/a/s;->h:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x1a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "entity_class: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/a/s;->i:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_7

    .line 219
    iget-object v0, p0, Lcom/google/maps/b/a/s;->i:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_b

    move v0, v1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x12

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "is_geocode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/a/s;->j:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_8

    .line 222
    iget-object v0, p0, Lcom/google/maps/b/a/s;->j:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_c

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x16

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "is_interactive: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 199
    :cond_9
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/s;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 208
    :cond_a
    iget-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    iget-object v4, p0, Lcom/google/maps/b/a/s;->t:[B

    iget-object v5, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iget v5, v5, Lcom/google/maps/b/a/cr;->a:I

    iget-object v6, p0, Lcom/google/maps/b/a/s;->f:Lcom/google/maps/b/a/cr;

    iget v6, v6, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/maps/b/a/ay;->a([BII)V

    iget-object v0, p0, Lcom/google/maps/b/a/s;->g:Lcom/google/maps/b/a/ay;

    goto/16 :goto_1

    :cond_b
    move v0, v2

    .line 219
    goto :goto_2

    :cond_c
    move v1, v2

    .line 222
    goto :goto_3
.end method
