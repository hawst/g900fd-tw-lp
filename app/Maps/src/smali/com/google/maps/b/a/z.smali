.class public Lcom/google/maps/b/a/z;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/db;

.field private b:Lcom/google/maps/b/a/cr;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/maps/b/a/cr;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 14
    new-instance v0, Lcom/google/maps/b/a/db;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/maps/b/a/db;-><init>(J)V

    iput-object v0, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    .line 18
    iput-object v1, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    .line 22
    iput-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/z;->f:Lcom/google/maps/b/a/cz;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 35
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/z;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/z;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    .line 40
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    return-object v0

    .line 37
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(I)Z
    .locals 1

    .prologue
    .line 81
    packed-switch p1, :pswitch_data_0

    .line 86
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 84
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 92
    packed-switch p1, :pswitch_data_0

    .line 98
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 94
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/z;->f:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 114
    packed-switch p1, :pswitch_data_0

    .line 123
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 116
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 119
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(IJ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 103
    packed-switch p1, :pswitch_data_0

    .line 109
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 105
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iput-wide p2, v1, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v0, v1, Lcom/google/maps/b/a/db;->c:Z

    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 69
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 70
    iget-object v0, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->a:J

    iput-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    iput-boolean v4, v0, Lcom/google/maps/b/a/db;->c:Z

    .line 71
    iget-object v0, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 72
    iput-object v1, p0, Lcom/google/maps/b/a/z;->c:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v4, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 74
    iput-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/google/maps/b/a/z;->f:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v4, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 77
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iget-boolean v1, v1, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v1, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "incident_id: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "L\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/a/z;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/google/maps/b/a/z;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "caption_text: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_3

    .line 136
    iget-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/maps/b/a/z;->t:[B

    iget-object v3, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->a:I

    iget-object v4, p0, Lcom/google/maps/b/a/z;->d:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->b:I

    sget-object v5, Lcom/google/maps/b/a/z;->r:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "description_text: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/google/maps/b/a/z;->f:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_4

    .line 139
    iget-object v1, p0, Lcom/google/maps/b/a/z;->f:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "incident_type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 136
    :cond_5
    const-string v1, ""

    iput-object v1, p0, Lcom/google/maps/b/a/z;->e:Ljava/lang/String;

    goto :goto_0
.end method
