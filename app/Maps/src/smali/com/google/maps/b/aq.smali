.class public final Lcom/google/maps/b/aq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/at;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/ao;",
        "Lcom/google/maps/b/aq;",
        ">;",
        "Lcom/google/maps/b/at;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:Lcom/google/n/ao;

.field private k:Z

.field private l:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 970
    sget-object v0, Lcom/google/maps/b/ao;->m:Lcom/google/maps/b/ao;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/aq;->b:Lcom/google/n/ao;

    .line 1254
    iput v1, p0, Lcom/google/maps/b/aq;->e:I

    .line 1290
    iput v1, p0, Lcom/google/maps/b/aq;->f:I

    .line 1358
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/aq;->h:Ljava/lang/Object;

    .line 1434
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/google/maps/b/aq;->i:I

    .line 1466
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/aq;->j:Lcom/google/n/ao;

    .line 1557
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/aq;->l:Lcom/google/n/ao;

    .line 971
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/ao;)Lcom/google/maps/b/aq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1062
    invoke-static {}, Lcom/google/maps/b/ao;->d()Lcom/google/maps/b/ao;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1102
    :goto_0
    return-object p0

    .line 1063
    :cond_0
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1064
    iget-object v2, p0, Lcom/google/maps/b/aq;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1065
    iget v2, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/maps/b/aq;->a:I

    .line 1067
    :cond_1
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1068
    iget v2, p1, Lcom/google/maps/b/ao;->c:I

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iput v2, p0, Lcom/google/maps/b/aq;->c:I

    .line 1070
    :cond_2
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1071
    iget v2, p1, Lcom/google/maps/b/ao;->d:I

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iput v2, p0, Lcom/google/maps/b/aq;->d:I

    .line 1073
    :cond_3
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 1074
    iget v2, p1, Lcom/google/maps/b/ao;->e:I

    invoke-static {v2}, Lcom/google/maps/b/ar;->a(I)Lcom/google/maps/b/ar;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/b/ar;->a:Lcom/google/maps/b/ar;

    :cond_4
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 1063
    goto :goto_1

    :cond_6
    move v2, v1

    .line 1067
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1070
    goto :goto_3

    :cond_8
    move v2, v1

    .line 1073
    goto :goto_4

    .line 1074
    :cond_9
    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iget v2, v2, Lcom/google/maps/b/ar;->Q:I

    iput v2, p0, Lcom/google/maps/b/aq;->e:I

    .line 1076
    :cond_a
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 1077
    iget v2, p1, Lcom/google/maps/b/ao;->f:I

    invoke-static {v2}, Lcom/google/maps/b/ar;->a(I)Lcom/google/maps/b/ar;

    move-result-object v2

    if-nez v2, :cond_b

    sget-object v2, Lcom/google/maps/b/ar;->a:Lcom/google/maps/b/ar;

    :cond_b
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    move v2, v1

    .line 1076
    goto :goto_5

    .line 1077
    :cond_d
    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iget v2, v2, Lcom/google/maps/b/ar;->Q:I

    iput v2, p0, Lcom/google/maps/b/aq;->f:I

    .line 1079
    :cond_e
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 1080
    iget v2, p1, Lcom/google/maps/b/ao;->g:I

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iput v2, p0, Lcom/google/maps/b/aq;->g:I

    .line 1082
    :cond_f
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 1083
    iget v2, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/maps/b/aq;->a:I

    .line 1084
    iget-object v2, p1, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/maps/b/aq;->h:Ljava/lang/Object;

    .line 1087
    :cond_10
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_8
    if-eqz v2, :cond_11

    .line 1088
    iget v2, p1, Lcom/google/maps/b/ao;->i:I

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iput v2, p0, Lcom/google/maps/b/aq;->i:I

    .line 1090
    :cond_11
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_9
    if-eqz v2, :cond_12

    .line 1091
    iget-object v2, p0, Lcom/google/maps/b/aq;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1092
    iget v2, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/maps/b/aq;->a:I

    .line 1094
    :cond_12
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_a
    if-eqz v2, :cond_13

    .line 1095
    iget-boolean v2, p1, Lcom/google/maps/b/ao;->k:Z

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/b/aq;->a:I

    iput-boolean v2, p0, Lcom/google/maps/b/aq;->k:Z

    .line 1097
    :cond_13
    iget v2, p1, Lcom/google/maps/b/ao;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1a

    :goto_b
    if-eqz v0, :cond_14

    .line 1098
    iget-object v0, p0, Lcom/google/maps/b/aq;->l:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1099
    iget v0, p0, Lcom/google/maps/b/aq;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/maps/b/aq;->a:I

    .line 1101
    :cond_14
    iget-object v0, p1, Lcom/google/maps/b/ao;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_15
    move v2, v1

    .line 1079
    goto/16 :goto_6

    :cond_16
    move v2, v1

    .line 1082
    goto :goto_7

    :cond_17
    move v2, v1

    .line 1087
    goto :goto_8

    :cond_18
    move v2, v1

    .line 1090
    goto :goto_9

    :cond_19
    move v2, v1

    .line 1094
    goto :goto_a

    :cond_1a
    move v0, v1

    .line 1097
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 962
    new-instance v2, Lcom/google/maps/b/ao;

    invoke-direct {v2, p0}, Lcom/google/maps/b/ao;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/aq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget-object v4, v2, Lcom/google/maps/b/ao;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/aq;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/aq;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/maps/b/aq;->c:I

    iput v4, v2, Lcom/google/maps/b/ao;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/maps/b/aq;->d:I

    iput v4, v2, Lcom/google/maps/b/ao;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/maps/b/aq;->e:I

    iput v4, v2, Lcom/google/maps/b/ao;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/maps/b/aq;->f:I

    iput v4, v2, Lcom/google/maps/b/ao;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/maps/b/aq;->g:I

    iput v4, v2, Lcom/google/maps/b/ao;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/maps/b/aq;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/maps/b/ao;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/maps/b/aq;->i:I

    iput v4, v2, Lcom/google/maps/b/ao;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/maps/b/ao;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/maps/b/aq;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/maps/b/aq;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v4, p0, Lcom/google/maps/b/aq;->k:Z

    iput-boolean v4, v2, Lcom/google/maps/b/ao;->k:Z

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v3, v2, Lcom/google/maps/b/ao;->l:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/maps/b/aq;->l:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/b/aq;->l:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/maps/b/ao;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 962
    check-cast p1, Lcom/google/maps/b/ao;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/aq;->a(Lcom/google/maps/b/ao;)Lcom/google/maps/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1106
    iget v0, p0, Lcom/google/maps/b/aq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 1126
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1106
    goto :goto_0

    .line 1110
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/aq;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1112
    goto :goto_1

    .line 1114
    :cond_2
    iget v0, p0, Lcom/google/maps/b/aq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 1115
    iget-object v0, p0, Lcom/google/maps/b/aq;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1117
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1114
    goto :goto_2

    .line 1120
    :cond_4
    iget v0, p0, Lcom/google/maps/b/aq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_6

    .line 1121
    iget-object v0, p0, Lcom/google/maps/b/aq;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 1123
    goto :goto_1

    :cond_5
    move v0, v1

    .line 1120
    goto :goto_3

    :cond_6
    move v0, v2

    .line 1126
    goto :goto_1
.end method
