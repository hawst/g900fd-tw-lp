.class public Lcom/google/maps/b/a/bq;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cr;

.field private b:Lcom/google/maps/b/a/cr;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/maps/b/a/cu;

.field private e:Lcom/google/maps/b/a/cy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 12
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cy;

    invoke-direct {v0}, Lcom/google/maps/b/a/cy;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->e:Lcom/google/maps/b/a/cy;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 24
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/bq;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/bq;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    .line 29
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    return-object v0

    .line 26
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 1

    .prologue
    .line 71
    packed-switch p1, :pswitch_data_0

    .line 77
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 73
    :pswitch_0
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->e:Lcom/google/maps/b/a/cy;

    invoke-virtual {v0, p2}, Lcom/google/maps/b/a/cy;->a(I)V

    .line 74
    const/4 v0, 0x1

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 82
    packed-switch p1, :pswitch_data_0

    .line 91
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 84
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 87
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 36
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bq;->t:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v2, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 39
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    .line 42
    :goto_0
    return-object v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/bq;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v3, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 42
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 61
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/b/a/bq;->c:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 64
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->d:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 65
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->e:Lcom/google/maps/b/a/cy;

    iput v2, v0, Lcom/google/maps/b/a/cy;->b:I

    .line 67
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/google/maps/b/a/bq;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "description: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/google/maps/b/a/bq;->b()Lcom/google/maps/b/a/cu;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "coverage: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/bq;->e:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v2, :cond_2

    .line 104
    iget-object v2, p0, Lcom/google/maps/b/a/bq;->e:Lcom/google/maps/b/a/cy;

    iget-object v2, v2, Lcom/google/maps/b/a/cy;->a:[I

    aget v2, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "style_index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
