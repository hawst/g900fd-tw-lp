.class public Lcom/google/maps/b/a/b;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Lcom/google/maps/b/a/cu;

.field private c:Lcom/google/maps/b/a/cr;

.field private d:Lcom/google/maps/b/a/cu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 14
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    .line 18
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cu;

    invoke-direct {v0}, Lcom/google/maps/b/a/cu;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/cu;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 22
    iget-object v0, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/b;->t:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v2, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 25
    iget-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    .line 28
    :goto_0
    return-object v0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    iget-object v1, p0, Lcom/google/maps/b/a/b;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v2, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v3, v0, Lcom/google/maps/b/a/cu;->c:I

    .line 28
    iget-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 59
    packed-switch p1, :pswitch_data_0

    .line 68
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 61
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 50
    iget-object v0, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 51
    iget-object v0, p0, Lcom/google/maps/b/a/b;->b:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 52
    iget-object v0, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 53
    iget-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    sget-object v1, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v1, v0, Lcom/google/maps/b/a/cu;->a:[B

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    iget-object v0, p0, Lcom/google/maps/b/a/b;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/google/maps/b/a/b;->a()Lcom/google/maps/b/a/cu;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "point_data: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    iget-object v2, p0, Lcom/google/maps/b/a/b;->t:[B

    iput-object v2, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v4, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v4, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "offset_data: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    iget-object v2, p0, Lcom/google/maps/b/a/b;->t:[B

    iget-object v3, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->a:I

    iget-object v4, p0, Lcom/google/maps/b/a/b;->c:Lcom/google/maps/b/a/cr;

    iget v4, v4, Lcom/google/maps/b/a/cr;->b:I

    iput-object v2, v0, Lcom/google/maps/b/a/cu;->a:[B

    iput v3, v0, Lcom/google/maps/b/a/cu;->b:I

    iput v4, v0, Lcom/google/maps/b/a/cu;->c:I

    iget-object v0, p0, Lcom/google/maps/b/a/b;->d:Lcom/google/maps/b/a/cu;

    goto :goto_0
.end method
