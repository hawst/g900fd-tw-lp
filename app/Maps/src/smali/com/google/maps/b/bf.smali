.class public final Lcom/google/maps/b/bf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/bi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bf;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/maps/b/bf;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/maps/b/bg;

    invoke-direct {v0}, Lcom/google/maps/b/bg;-><init>()V

    sput-object v0, Lcom/google/maps/b/bf;->PARSER:Lcom/google/n/ax;

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/b/bf;->h:Lcom/google/n/aw;

    .line 571
    new-instance v0, Lcom/google/maps/b/bf;

    invoke-direct {v0}, Lcom/google/maps/b/bf;-><init>()V

    sput-object v0, Lcom/google/maps/b/bf;->e:Lcom/google/maps/b/bf;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 165
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    .line 181
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    .line 211
    iput-byte v2, p0, Lcom/google/maps/b/bf;->f:B

    .line 242
    iput v2, p0, Lcom/google/maps/b/bf;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/bf;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/maps/b/bf;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v3, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 48
    iget v3, p0, Lcom/google/maps/b/bf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/bf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/bf;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 53
    iget v3, p0, Lcom/google/maps/b/bf;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/bf;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/maps/b/bf;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/bf;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/maps/b/bf;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/bf;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 165
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    .line 181
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    .line 211
    iput-byte v1, p0, Lcom/google/maps/b/bf;->f:B

    .line 242
    iput v1, p0, Lcom/google/maps/b/bf;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/maps/b/bf;
    .locals 1

    .prologue
    .line 574
    sget-object v0, Lcom/google/maps/b/bf;->e:Lcom/google/maps/b/bf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/maps/b/bh;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/google/maps/b/bh;

    invoke-direct {v0}, Lcom/google/maps/b/bh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/maps/b/bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/maps/b/bf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 229
    invoke-virtual {p0}, Lcom/google/maps/b/bf;->c()I

    .line 230
    iget v0, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 233
    :cond_0
    iget v0, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 234
    iget-object v0, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 236
    :cond_1
    iget v0, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 237
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/maps/b/bf;->d:I

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/bf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 240
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 213
    iget-byte v0, p0, Lcom/google/maps/b/bf;->f:B

    .line 214
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 224
    :goto_0
    return v0

    .line 215
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 217
    :cond_1
    iget v0, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 218
    iget-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/by;->d()Lcom/google/maps/b/by;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/by;

    invoke-virtual {v0}, Lcom/google/maps/b/by;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 219
    iput-byte v2, p0, Lcom/google/maps/b/bf;->f:B

    move v0, v2

    .line 220
    goto :goto_0

    :cond_2
    move v0, v2

    .line 217
    goto :goto_1

    .line 223
    :cond_3
    iput-byte v1, p0, Lcom/google/maps/b/bf;->f:B

    move v0, v1

    .line 224
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 244
    iget v0, p0, Lcom/google/maps/b/bf;->g:I

    .line 245
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 262
    :goto_0
    return v0

    .line 248
    :cond_0
    iget v0, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 249
    iget-object v0, p0, Lcom/google/maps/b/bf;->b:Lcom/google/n/ao;

    .line 250
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 252
    :goto_1
    iget v2, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 253
    iget-object v2, p0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    .line 254
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 256
    :cond_1
    iget v2, p0, Lcom/google/maps/b/bf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 257
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/maps/b/bf;->d:I

    .line 258
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 260
    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/bf;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    iput v0, p0, Lcom/google/maps/b/bf;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bf;->newBuilder()Lcom/google/maps/b/bh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/maps/b/bh;->a(Lcom/google/maps/b/bf;)Lcom/google/maps/b/bh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/maps/b/bf;->newBuilder()Lcom/google/maps/b/bh;

    move-result-object v0

    return-object v0
.end method
