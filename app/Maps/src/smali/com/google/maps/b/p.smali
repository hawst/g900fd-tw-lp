.class public final Lcom/google/maps/b/p;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/n;",
        "Lcom/google/maps/b/p;",
        ">;",
        "Lcom/google/maps/b/q;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Lcom/google/n/f;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1130
    sget-object v0, Lcom/google/maps/b/n;->p:Lcom/google/maps/b/n;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1417
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/maps/b/p;->f:I

    .line 1453
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/b/p;->g:I

    .line 1489
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/maps/b/p;->h:Lcom/google/n/f;

    .line 1556
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/p;->j:I

    .line 1720
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/p;->o:Ljava/lang/Object;

    .line 1131
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/n;)Lcom/google/maps/b/p;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1234
    invoke-static {}, Lcom/google/maps/b/n;->d()Lcom/google/maps/b/n;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1280
    :goto_0
    return-object p0

    .line 1235
    :cond_0
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1236
    iget v2, p1, Lcom/google/maps/b/n;->b:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->b:I

    .line 1238
    :cond_1
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1239
    iget v2, p1, Lcom/google/maps/b/n;->c:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->c:I

    .line 1241
    :cond_2
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1242
    iget v2, p1, Lcom/google/maps/b/n;->d:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->d:I

    .line 1244
    :cond_3
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1245
    iget v2, p1, Lcom/google/maps/b/n;->e:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->e:I

    .line 1247
    :cond_4
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1248
    iget v2, p1, Lcom/google/maps/b/n;->f:I

    invoke-static {v2}, Lcom/google/maps/b/j;->a(I)Lcom/google/maps/b/j;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/b/j;->d:Lcom/google/maps/b/j;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1235
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1238
    goto :goto_2

    :cond_8
    move v2, v1

    .line 1241
    goto :goto_3

    :cond_9
    move v2, v1

    .line 1244
    goto :goto_4

    :cond_a
    move v2, v1

    .line 1247
    goto :goto_5

    .line 1248
    :cond_b
    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iget v2, v2, Lcom/google/maps/b/j;->g:I

    iput v2, p0, Lcom/google/maps/b/p;->f:I

    .line 1250
    :cond_c
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 1251
    iget v2, p1, Lcom/google/maps/b/n;->g:I

    invoke-static {v2}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    :cond_d
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 1250
    goto :goto_6

    .line 1251
    :cond_f
    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iget v2, v2, Lcom/google/maps/b/l;->e:I

    iput v2, p0, Lcom/google/maps/b/p;->g:I

    .line 1253
    :cond_10
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_13

    .line 1254
    iget-object v2, p1, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    if-nez v2, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move v2, v1

    .line 1253
    goto :goto_7

    .line 1254
    :cond_12
    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput-object v2, p0, Lcom/google/maps/b/p;->h:Lcom/google/n/f;

    .line 1256
    :cond_13
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_14

    .line 1257
    iget v2, p1, Lcom/google/maps/b/n;->i:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->i:I

    .line 1259
    :cond_14
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_19

    .line 1260
    iget v2, p1, Lcom/google/maps/b/n;->j:I

    invoke-static {v2}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_15
    if-nez v2, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    move v2, v1

    .line 1256
    goto :goto_8

    :cond_17
    move v2, v1

    .line 1259
    goto :goto_9

    .line 1260
    :cond_18
    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iget v2, v2, Lcom/google/maps/b/h;->j:I

    iput v2, p0, Lcom/google/maps/b/p;->j:I

    .line 1262
    :cond_19
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_a
    if-eqz v2, :cond_1a

    .line 1263
    iget v2, p1, Lcom/google/maps/b/n;->k:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->k:I

    .line 1265
    :cond_1a
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_b
    if-eqz v2, :cond_1b

    .line 1266
    iget v2, p1, Lcom/google/maps/b/n;->l:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->l:I

    .line 1268
    :cond_1b
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_c
    if-eqz v2, :cond_1c

    .line 1269
    iget v2, p1, Lcom/google/maps/b/n;->m:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->m:I

    .line 1271
    :cond_1c
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_d
    if-eqz v2, :cond_1d

    .line 1272
    iget v2, p1, Lcom/google/maps/b/n;->n:I

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/maps/b/p;->a:I

    iput v2, p0, Lcom/google/maps/b/p;->n:I

    .line 1274
    :cond_1d
    iget v2, p1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_23

    :goto_e
    if-eqz v0, :cond_1e

    .line 1275
    iget v0, p0, Lcom/google/maps/b/p;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/maps/b/p;->a:I

    .line 1276
    iget-object v0, p1, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/maps/b/p;->o:Ljava/lang/Object;

    .line 1279
    :cond_1e
    iget-object v0, p1, Lcom/google/maps/b/n;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1f
    move v2, v1

    .line 1262
    goto :goto_a

    :cond_20
    move v2, v1

    .line 1265
    goto :goto_b

    :cond_21
    move v2, v1

    .line 1268
    goto :goto_c

    :cond_22
    move v2, v1

    .line 1271
    goto :goto_d

    :cond_23
    move v0, v1

    .line 1274
    goto :goto_e
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1122
    new-instance v2, Lcom/google/maps/b/n;

    invoke-direct {v2, p0}, Lcom/google/maps/b/n;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/maps/b/p;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_d

    :goto_0
    iget v1, p0, Lcom/google/maps/b/p;->b:I

    iput v1, v2, Lcom/google/maps/b/n;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/maps/b/p;->c:I

    iput v1, v2, Lcom/google/maps/b/n;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/maps/b/p;->d:I

    iput v1, v2, Lcom/google/maps/b/n;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/maps/b/p;->e:I

    iput v1, v2, Lcom/google/maps/b/n;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/maps/b/p;->f:I

    iput v1, v2, Lcom/google/maps/b/n;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/maps/b/p;->g:I

    iput v1, v2, Lcom/google/maps/b/n;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/maps/b/p;->h:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/maps/b/p;->i:I

    iput v1, v2, Lcom/google/maps/b/n;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/maps/b/p;->j:I

    iput v1, v2, Lcom/google/maps/b/n;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/maps/b/p;->k:I

    iput v1, v2, Lcom/google/maps/b/n;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/maps/b/p;->l:I

    iput v1, v2, Lcom/google/maps/b/n;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v1, p0, Lcom/google/maps/b/p;->m:I

    iput v1, v2, Lcom/google/maps/b/n;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v1, p0, Lcom/google/maps/b/p;->n:I

    iput v1, v2, Lcom/google/maps/b/n;->n:I

    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-object v1, p0, Lcom/google/maps/b/p;->o:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/maps/b/n;->o:Ljava/lang/Object;

    iput v0, v2, Lcom/google/maps/b/n;->a:I

    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1122
    check-cast p1, Lcom/google/maps/b/n;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/p;->a(Lcom/google/maps/b/n;)Lcom/google/maps/b/p;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1284
    const/4 v0, 0x1

    return v0
.end method
