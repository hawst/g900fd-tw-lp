.class public Lcom/google/maps/b/a/u;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field private a:Lcom/google/maps/b/a/cr;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/maps/b/a/cr;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/maps/b/a/cr;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/maps/b/a/cr;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/maps/b/a/cr;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 16
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    .line 18
    iput-object v1, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    .line 22
    iput-object v1, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    .line 26
    iput-object v1, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    .line 30
    iput-object v1, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    .line 34
    iput-object v1, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 38
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/u;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/u;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    .line 43
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    return-object v0

    .line 40
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 123
    packed-switch p1, :pswitch_data_0

    .line 141
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 125
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 128
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 131
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 134
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 137
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/u;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/u;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    .line 57
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    return-object v0

    .line 54
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 107
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 108
    iget-object v0, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 109
    iput-object v2, p0, Lcom/google/maps/b/a/u;->b:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 111
    iput-object v2, p0, Lcom/google/maps/b/a/u;->d:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 113
    iput-object v2, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 115
    iput-object v2, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iput-boolean v1, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 117
    iput-object v2, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/u;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/u;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    .line 71
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    return-object v0

    .line 68
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/u;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/u;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/u;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    .line 85
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    return-object v0

    .line 82
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/u;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 5

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/u;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/u;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    .line 99
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    return-object v0

    .line 96
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/u;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    iget-object v1, p0, Lcom/google/maps/b/a/u;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/google/maps/b/a/u;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "line1: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/a/u;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/google/maps/b/a/u;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "line2: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/a/u;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_2

    .line 154
    invoke-virtual {p0}, Lcom/google/maps/b/a/u;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "line3: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/a/u;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_3

    .line 157
    invoke-virtual {p0}, Lcom/google/maps/b/a/u;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "visible_url: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/google/maps/b/a/u;->i:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_4

    .line 160
    invoke-virtual {p0}, Lcom/google/maps/b/a/u;->f()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "destination_url: \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
