.class public final Lcom/google/maps/b/ax;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/maps/b/ba;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/maps/b/au;",
        "Lcom/google/maps/b/ax;",
        ">;",
        "Lcom/google/maps/b/ba;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 553
    sget-object v0, Lcom/google/maps/b/au;->e:Lcom/google/maps/b/au;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 669
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    .line 806
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    .line 943
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    .line 1017
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    .line 554
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/b/au;)Lcom/google/maps/b/ax;
    .locals 2

    .prologue
    .line 599
    invoke-static {}, Lcom/google/maps/b/au;->d()Lcom/google/maps/b/au;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 641
    :goto_0
    return-object p0

    .line 600
    :cond_0
    iget-object v0, p1, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 601
    iget-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 602
    iget-object v0, p1, Lcom/google/maps/b/au;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    .line 603
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 610
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 611
    iget-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 612
    iget-object v0, p1, Lcom/google/maps/b/au;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    .line 613
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 620
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 621
    iget-object v0, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 622
    iget-object v0, p1, Lcom/google/maps/b/au;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    .line 623
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 630
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 631
    iget-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 632
    iget-object v0, p1, Lcom/google/maps/b/au;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    .line 633
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 640
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/maps/b/au;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 605
    :cond_5
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 606
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/b/au;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 615
    :cond_7
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 616
    :cond_8
    iget-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/b/au;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 625
    :cond_9
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_a

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 626
    :cond_a
    iget-object v0, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/b/au;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 635
    :cond_b
    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/maps/b/ax;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/maps/b/ax;->a:I

    .line 636
    :cond_c
    iget-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/maps/b/au;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 545
    new-instance v0, Lcom/google/maps/b/au;

    invoke-direct {v0, p0}, Lcom/google/maps/b/au;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/maps/b/ax;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/b/au;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/maps/b/ax;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/b/au;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/maps/b/ax;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/ax;->d:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/b/au;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/maps/b/ax;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/maps/b/ax;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    iput-object v1, v0, Lcom/google/maps/b/au;->d:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 545
    check-cast p1, Lcom/google/maps/b/au;

    invoke-virtual {p0, p1}, Lcom/google/maps/b/ax;->a(Lcom/google/maps/b/au;)Lcom/google/maps/b/ax;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 645
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 646
    iget-object v0, p0, Lcom/google/maps/b/ax;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ao;->d()Lcom/google/maps/b/ao;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ao;

    invoke-virtual {v0}, Lcom/google/maps/b/ao;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 663
    :cond_0
    :goto_1
    return v2

    .line 645
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 651
    :goto_2
    iget-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 652
    iget-object v0, p0, Lcom/google/maps/b/ax;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/bb;->d()Lcom/google/maps/b/bb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/bb;

    invoke-virtual {v0}, Lcom/google/maps/b/bb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 657
    :goto_3
    iget-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 658
    iget-object v0, p0, Lcom/google/maps/b/ax;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/ak;->d()Lcom/google/maps/b/ak;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ak;

    invoke-virtual {v0}, Lcom/google/maps/b/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 663
    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method
