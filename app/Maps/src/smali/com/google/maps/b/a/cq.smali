.class public abstract Lcom/google/maps/b/a/cq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final r:Ljava/nio/charset/Charset;

.field public static final s:[B


# instance fields
.field public t:[B

.field public u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/maps/b/a/cq;->r:Ljava/nio/charset/Charset;

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/maps/b/a/cq;->s:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v0, p0, Lcom/google/maps/b/a/cq;->t:[B

    .line 364
    return-void
.end method

.method public static a([BLcom/google/maps/b/a/dc;)I
    .locals 3

    .prologue
    .line 440
    iget v0, p1, Lcom/google/maps/b/a/dc;->a:I

    .line 441
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p1, Lcom/google/maps/b/a/dc;->a:I

    .line 443
    add-int/lit8 v1, v0, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    add-int/lit8 v2, v0, 0x2

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    add-int/lit8 v2, v0, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static a([BIILcom/google/maps/b/a/cx;)V
    .locals 4

    .prologue
    .line 503
    new-instance v0, Lcom/google/maps/b/a/dc;

    invoke-direct {v0, p1}, Lcom/google/maps/b/a/dc;-><init>(I)V

    .line 504
    add-int v1, p1, p2

    .line 505
    :goto_0
    iget v2, v0, Lcom/google/maps/b/a/dc;->a:I

    if-ge v2, v1, :cond_0

    .line 506
    invoke-static {p0, v0}, Lcom/google/maps/b/a/cq;->c([BLcom/google/maps/b/a/dc;)I

    move-result v2

    ushr-int/lit8 v3, v2, 0x1

    and-int/lit8 v2, v2, 0x1

    neg-int v2, v2

    xor-int/2addr v2, v3

    invoke-virtual {p3, v2}, Lcom/google/maps/b/a/cx;->a(I)V

    goto :goto_0

    .line 508
    :cond_0
    return-void
.end method

.method public static a([BIILcom/google/maps/b/a/cy;)V
    .locals 3

    .prologue
    .line 476
    new-instance v0, Lcom/google/maps/b/a/dc;

    invoke-direct {v0, p1}, Lcom/google/maps/b/a/dc;-><init>(I)V

    .line 477
    add-int v1, p1, p2

    .line 478
    :goto_0
    iget v2, v0, Lcom/google/maps/b/a/dc;->a:I

    if-ge v2, v1, :cond_0

    .line 479
    invoke-static {p0, v0}, Lcom/google/maps/b/a/cq;->c([BLcom/google/maps/b/a/dc;)I

    move-result v2

    invoke-virtual {p3, v2}, Lcom/google/maps/b/a/cy;->a(I)V

    goto :goto_0

    .line 481
    :cond_0
    return-void
.end method

.method public static b([BLcom/google/maps/b/a/dc;)J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 451
    iget v0, p1, Lcom/google/maps/b/a/dc;->a:I

    .line 452
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p1, Lcom/google/maps/b/a/dc;->a:I

    .line 453
    add-int/lit8 v1, v0, 0x7

    aget-byte v1, p0, v1

    int-to-long v2, v1

    and-long/2addr v2, v6

    const/16 v1, 0x38

    shl-long/2addr v2, v1

    add-int/lit8 v1, v0, 0x6

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x30

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    add-int/lit8 v1, v0, 0x5

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x28

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    add-int/lit8 v1, v0, 0x4

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x20

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    add-int/lit8 v1, v0, 0x3

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x18

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x10

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    int-to-long v4, v1

    and-long/2addr v4, v6

    const/16 v1, 0x8

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    aget-byte v0, p0, v0

    int-to-long v0, v0

    and-long/2addr v0, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static c([BLcom/google/maps/b/a/dc;)I
    .locals 4

    .prologue
    const/16 v3, 0x80

    .line 379
    iget v0, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    .line 380
    if-ge v0, v3, :cond_1

    .line 415
    :cond_0
    return v0

    .line 383
    :cond_1
    and-int/lit8 v0, v0, 0x7f

    .line 385
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 386
    and-int/lit8 v2, v1, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v0, v2

    .line 387
    if-lt v1, v3, :cond_0

    .line 391
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 392
    and-int/lit8 v2, v1, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v0, v2

    .line 393
    if-lt v1, v3, :cond_0

    .line 397
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 398
    and-int/lit8 v2, v1, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v0, v2

    .line 399
    if-lt v1, v3, :cond_0

    .line 403
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 404
    and-int/lit8 v2, v1, 0x7f

    shl-int/lit8 v2, v2, 0x1c

    or-int/2addr v0, v2

    .line 405
    if-lt v1, v3, :cond_0

    .line 412
    :goto_0
    if-lt v1, v3, :cond_0

    .line 413
    iget v1, p1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)V
    .locals 10

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/maps/b/a/cq;->t:[B

    if-ne v0, p1, :cond_1

    iget v0, p0, Lcom/google/maps/b/a/cq;->u:I

    if-ne v0, p2, :cond_1

    .line 601
    :cond_0
    return-void

    .line 553
    :cond_1
    invoke-virtual {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 554
    iput-object p1, p0, Lcom/google/maps/b/a/cq;->t:[B

    .line 555
    iput p2, p0, Lcom/google/maps/b/a/cq;->u:I

    .line 556
    new-instance v1, Lcom/google/maps/b/a/dc;

    invoke-direct {v1, p2}, Lcom/google/maps/b/a/dc;-><init>(I)V

    .line 558
    add-int v4, p2, p3

    .line 559
    :cond_2
    :goto_0
    iget v0, v1, Lcom/google/maps/b/a/dc;->a:I

    if-ge v0, v4, :cond_6

    .line 560
    invoke-static {p1, v1}, Lcom/google/maps/b/a/cq;->c([BLcom/google/maps/b/a/dc;)I

    move-result v0

    .line 561
    and-int/lit8 v2, v0, 0x7

    .line 562
    shr-int/lit8 v5, v0, 0x3

    .line 563
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 565
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/google/maps/b/a/cq;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 566
    iget v0, v1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v2, v0, 0x1

    iput v2, v1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v2, v0

    const-wide/16 v6, 0x80

    cmp-long v0, v2, v6

    if-gez v0, :cond_4

    :cond_3
    invoke-virtual {p0, v5, v2, v3}, Lcom/google/maps/b/a/cq;->a(IJ)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_4
    const-wide/16 v6, 0x7f

    and-long/2addr v2, v6

    const/4 v0, 0x7

    :goto_1
    iget v6, v1, Lcom/google/maps/b/a/dc;->a:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v1, Lcom/google/maps/b/a/dc;->a:I

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    const-wide/16 v8, 0x7f

    and-long/2addr v8, v6

    shl-long/2addr v8, v0

    or-long/2addr v2, v8

    const-wide/16 v8, 0x80

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 571
    :cond_5
    invoke-static {p1, v1}, Lcom/google/maps/b/a/cq;->c([BLcom/google/maps/b/a/dc;)I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/google/maps/b/a/cq;->a(II)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 577
    :pswitch_2
    invoke-static {p1, v1}, Lcom/google/maps/b/a/cq;->b([BLcom/google/maps/b/a/dc;)J

    move-result-wide v2

    .line 578
    invoke-virtual {p0, v5, v2, v3}, Lcom/google/maps/b/a/cq;->a(IJ)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 584
    :pswitch_3
    invoke-static {p1, v1}, Lcom/google/maps/b/a/cq;->c([BLcom/google/maps/b/a/dc;)I

    move-result v0

    .line 585
    iget v2, v1, Lcom/google/maps/b/a/dc;->a:I

    invoke-virtual {p0, v5, v2, v0}, Lcom/google/maps/b/a/cq;->a(III)Z

    .line 588
    iget v2, v1, Lcom/google/maps/b/a/dc;->a:I

    add-int/2addr v0, v2

    iput v0, v1, Lcom/google/maps/b/a/dc;->a:I

    goto :goto_0

    .line 591
    :pswitch_4
    invoke-static {p1, v1}, Lcom/google/maps/b/a/cq;->a([BLcom/google/maps/b/a/dc;)I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/google/maps/b/a/cq;->a(II)Z

    goto :goto_0

    .line 597
    :cond_6
    iget v0, v1, Lcom/google/maps/b/a/dc;->a:I

    if-le v0, v4, :cond_0

    .line 598
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    iget v1, v1, Lcom/google/maps/b/a/dc;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x45

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Proto parsing overran buffer (ended at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", past "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected a(I)Z
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method

.method protected a(II)Z
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return v0
.end method

.method protected a(III)Z
    .locals 1

    .prologue
    .line 542
    const/4 v0, 0x0

    return v0
.end method

.method protected a(IJ)Z
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    return v0
.end method

.method c()V
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/google/maps/b/a/cq;->s:[B

    iput-object v0, p0, Lcom/google/maps/b/a/cq;->t:[B

    .line 608
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/b/a/cq;->u:I

    .line 609
    return-void
.end method
