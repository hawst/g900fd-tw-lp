.class public Lcom/google/maps/b/a/aj;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cz;

.field public b:Lcom/google/maps/b/a/cr;

.field public c:Lcom/google/maps/b/a/cr;

.field public d:Lcom/google/maps/b/a/cr;

.field public e:Lcom/google/maps/b/a/cr;

.field public f:Lcom/google/maps/b/a/cr;

.field public g:Lcom/google/maps/b/a/cr;

.field private h:Lcom/google/maps/b/a/i;

.field private i:Lcom/google/maps/b/a/av;

.field private j:Lcom/google/maps/b/a/be;

.field private k:Lcom/google/maps/b/a/cc;

.field private l:Lcom/google/maps/b/a/dk;

.field private m:Lcom/google/maps/b/a/by;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 20
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    .line 22
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    .line 24
    new-instance v0, Lcom/google/maps/b/a/i;

    invoke-direct {v0}, Lcom/google/maps/b/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    .line 26
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    .line 28
    new-instance v0, Lcom/google/maps/b/a/av;

    invoke-direct {v0}, Lcom/google/maps/b/a/av;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    .line 30
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    .line 32
    new-instance v0, Lcom/google/maps/b/a/be;

    invoke-direct {v0}, Lcom/google/maps/b/a/be;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    .line 34
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    .line 36
    new-instance v0, Lcom/google/maps/b/a/cc;

    invoke-direct {v0}, Lcom/google/maps/b/a/cc;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    .line 38
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    .line 40
    new-instance v0, Lcom/google/maps/b/a/dk;

    invoke-direct {v0}, Lcom/google/maps/b/a/dk;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    .line 42
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    .line 44
    new-instance v0, Lcom/google/maps/b/a/by;

    invoke-direct {v0}, Lcom/google/maps/b/a/by;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/b/a/i;
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    invoke-virtual {v0}, Lcom/google/maps/b/a/i;->c()V

    .line 56
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    .line 59
    :goto_0
    return-object v0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/i;->a([BII)V

    .line 59
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 151
    packed-switch p1, :pswitch_data_0

    .line 157
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 153
    :pswitch_0
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 162
    packed-switch p1, :pswitch_data_0

    .line 183
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 164
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 167
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 170
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 173
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 176
    :pswitch_5
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 179
    :pswitch_6
    iget-object v1, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public final b()Lcom/google/maps/b/a/av;
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    invoke-virtual {v0}, Lcom/google/maps/b/a/av;->c()V

    .line 69
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    .line 72
    :goto_0
    return-object v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/av;->a([BII)V

    .line 72
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    goto :goto_0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 133
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 134
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 135
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->h:Lcom/google/maps/b/a/i;

    invoke-virtual {v0}, Lcom/google/maps/b/a/i;->c()V

    .line 136
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 137
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->i:Lcom/google/maps/b/a/av;

    invoke-virtual {v0}, Lcom/google/maps/b/a/av;->c()V

    .line 138
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 139
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    invoke-virtual {v0}, Lcom/google/maps/b/a/be;->c()V

    .line 140
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 141
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    invoke-virtual {v0}, Lcom/google/maps/b/a/cc;->c()V

    .line 142
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 143
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dk;->c()V

    .line 144
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 145
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    invoke-virtual {v0}, Lcom/google/maps/b/a/by;->c()V

    .line 147
    return-void
.end method

.method public final d()Lcom/google/maps/b/a/be;
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    invoke-virtual {v0}, Lcom/google/maps/b/a/be;->c()V

    .line 82
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    .line 85
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/be;->a([BII)V

    .line 85
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->j:Lcom/google/maps/b/a/be;

    goto :goto_0
.end method

.method public final e()Lcom/google/maps/b/a/cc;
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    invoke-virtual {v0}, Lcom/google/maps/b/a/cc;->c()V

    .line 95
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    .line 98
    :goto_0
    return-object v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/cc;->a([BII)V

    .line 98
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->k:Lcom/google/maps/b/a/cc;

    goto :goto_0
.end method

.method public final f()Lcom/google/maps/b/a/dk;
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dk;->c()V

    .line 108
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    .line 111
    :goto_0
    return-object v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/dk;->a([BII)V

    .line 111
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->l:Lcom/google/maps/b/a/dk;

    goto :goto_0
.end method

.method public final g()Lcom/google/maps/b/a/by;
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    invoke-virtual {v0}, Lcom/google/maps/b/a/by;->c()V

    .line 121
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    .line 124
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    iget-object v1, p0, Lcom/google/maps/b/a/aj;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/by;->a([BII)V

    .line 124
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->m:Lcom/google/maps/b/a/by;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "zoom: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_2

    .line 193
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->a()Lcom/google/maps/b/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/i;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 195
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "area_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_4

    .line 201
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->b()Lcom/google/maps/b/a/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/av;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 204
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "label_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_6

    .line 209
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->d()Lcom/google/maps/b/a/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/be;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 211
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 212
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "line_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_6
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_8

    .line 217
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->e()Lcom/google/maps/b/a/cc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/cc;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 219
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 220
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "shader_op_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_8
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_a

    .line 225
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->f()Lcom/google/maps/b/a/dk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/dk;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 227
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 228
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 230
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "volume_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :cond_a
    iget-object v0, p0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_c

    .line 233
    invoke-virtual {p0}, Lcom/google/maps/b/a/aj;->g()Lcom/google/maps/b/a/by;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/by;->toString()Ljava/lang/String;

    move-result-object v0

    .line 234
    const-string v2, "\n"

    const-string v3, "\n  "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 235
    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 236
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 238
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "raster_style {\n  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    :cond_c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
