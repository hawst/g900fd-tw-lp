.class public Lcom/google/maps/b/a/al;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cs;

.field public b:Lcom/google/maps/b/a/cz;

.field final c:Lcom/google/maps/b/a/dd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/b/a/dd",
            "<",
            "Lcom/google/maps/b/a/ak;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/maps/b/a/ak;

.field private e:Lcom/google/maps/b/a/cz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 23
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    .line 25
    new-instance v0, Lcom/google/maps/b/a/ak;

    invoke-direct {v0}, Lcom/google/maps/b/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/al;->d:Lcom/google/maps/b/a/ak;

    .line 27
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/al;->e:Lcom/google/maps/b/a/cz;

    .line 29
    new-instance v0, Lcom/google/maps/b/a/cz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    .line 33
    new-instance v0, Lcom/google/maps/b/a/am;

    invoke-direct {v0, p0}, Lcom/google/maps/b/a/am;-><init>(Lcom/google/maps/b/a/al;)V

    iput-object v0, p0, Lcom/google/maps/b/a/al;->c:Lcom/google/maps/b/a/dd;

    return-void
.end method


# virtual methods
.method protected final a(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 84
    packed-switch p1, :pswitch_data_0

    .line 93
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 86
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/al;->e:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 89
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iput p2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 1

    .prologue
    .line 98
    packed-switch p1, :pswitch_data_0

    .line 104
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 100
    :pswitch_0
    iget-object v0, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v0, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    .line 101
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)Lcom/google/maps/b/a/ak;
    .locals 4

    .prologue
    .line 54
    iget-object v1, p0, Lcom/google/maps/b/a/al;->d:Lcom/google/maps/b/a/ak;

    iget-object v2, p0, Lcom/google/maps/b/a/al;->t:[B

    iget-object v0, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v3, v0, Lcom/google/maps/b/a/cr;->a:I

    iget-object v0, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/maps/b/a/ak;->a([BII)V

    .line 55
    iget-object v0, p0, Lcom/google/maps/b/a/al;->d:Lcom/google/maps/b/a/ak;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 75
    iget-object v0, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    iput v2, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 76
    iget-object v0, p0, Lcom/google/maps/b/a/al;->d:Lcom/google/maps/b/a/ak;

    invoke-virtual {v0}, Lcom/google/maps/b/a/ak;->c()V

    .line 77
    iget-object v0, p0, Lcom/google/maps/b/a/al;->e:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 78
    iget-object v0, p0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 80
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 110
    :goto_0
    iget-object v2, p0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v2, :cond_1

    .line 111
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/al;->b(I)Lcom/google/maps/b/a/ak;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v2

    .line 112
    const-string v4, "\n"

    const-string v5, "\n  "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 113
    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 114
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 116
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "element {\n  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "}\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/al;->e:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/maps/b/a/al;->e:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "multi_zoom_style_index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "justification: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
