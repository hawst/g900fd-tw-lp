.class public Lcom/google/maps/b/a/ae;
.super Lcom/google/maps/b/a/cq;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/b/a/cs;

.field public b:Lcom/google/maps/b/a/cz;

.field public c:Lcom/google/maps/b/a/cz;

.field public d:Lcom/google/maps/b/a/cr;

.field private e:Lcom/google/maps/b/a/cr;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/maps/b/a/cr;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/maps/b/a/cr;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/maps/b/a/bz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/maps/b/a/cq;-><init>()V

    .line 23
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    .line 25
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    .line 27
    new-instance v0, Lcom/google/maps/b/a/cs;

    invoke-direct {v0}, Lcom/google/maps/b/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->a:Lcom/google/maps/b/a/cs;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->g:Ljava/util/List;

    .line 31
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    .line 33
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    .line 35
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    .line 37
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v2}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->b:Lcom/google/maps/b/a/cz;

    .line 41
    new-instance v0, Lcom/google/maps/b/a/cz;

    invoke-direct {v0, v2}, Lcom/google/maps/b/a/cz;-><init>(I)V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->c:Lcom/google/maps/b/a/cz;

    .line 43
    new-instance v0, Lcom/google/maps/b/a/cr;

    invoke-direct {v0}, Lcom/google/maps/b/a/cr;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    .line 45
    new-instance v0, Lcom/google/maps/b/a/bz;

    invoke-direct {v0}, Lcom/google/maps/b/a/bz;-><init>()V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 49
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ae;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ae;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    .line 54
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    return-object v0

    .line 51
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 150
    packed-switch p1, :pswitch_data_0

    .line 159
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 152
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->b:Lcom/google/maps/b/a/cz;

    ushr-int/lit8 v2, p2, 0x1

    and-int/lit8 v3, p2, 0x1

    neg-int v3, v3

    xor-int/2addr v2, v3

    iput v2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 155
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->c:Lcom/google/maps/b/a/cz;

    ushr-int/lit8 v2, p2, 0x1

    and-int/lit8 v3, p2, 0x1

    neg-int v3, v3

    xor-int/2addr v2, v3

    iput v2, v1, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cz;->c:Z

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 164
    packed-switch p1, :pswitch_data_0

    .line 182
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 166
    :pswitch_1
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 169
    :pswitch_2
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v1, p2, p3}, Lcom/google/maps/b/a/cs;->a(II)V

    goto :goto_0

    .line 172
    :pswitch_3
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 175
    :pswitch_4
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 178
    :pswitch_5
    iget-object v1, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iput p2, v1, Lcom/google/maps/b/a/cr;->a:I

    iput p3, v1, Lcom/google/maps/b/a/cr;->b:I

    iput-boolean v0, v1, Lcom/google/maps/b/a/cr;->c:Z

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 78
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ae;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ae;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    .line 83
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    return-object v0

    .line 80
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 7

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/b/a/ae;->a:Lcom/google/maps/b/a/cs;

    invoke-virtual {v0}, Lcom/google/maps/b/a/cs;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/cr;

    iget-object v2, p0, Lcom/google/maps/b/a/ae;->g:Ljava/util/List;

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/maps/b/a/ae;->t:[B

    iget v5, v0, Lcom/google/maps/b/a/cr;->a:I

    iget v0, v0, Lcom/google/maps/b/a/cr;->b:I

    sget-object v6, Lcom/google/maps/b/a/ae;->r:Ljava/nio/charset/Charset;

    invoke-direct {v3, v4, v5, v0, v6}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 132
    invoke-super {p0}, Lcom/google/maps/b/a/cq;->c()V

    .line 133
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 134
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->f:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->a:Lcom/google/maps/b/a/cs;

    iput v2, v0, Lcom/google/maps/b/a/cs;->b:I

    iget-object v0, v0, Lcom/google/maps/b/a/cs;->c:Lcom/google/maps/b/a/dd;

    invoke-virtual {v0}, Lcom/google/maps/b/a/dd;->a()V

    .line 136
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 137
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 138
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->i:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 140
    iput-object v1, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    .line 141
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->b:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 142
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->c:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->a:I

    iput v1, v0, Lcom/google/maps/b/a/cz;->b:I

    iput-boolean v2, v0, Lcom/google/maps/b/a/cz;->c:Z

    .line 143
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iput-boolean v2, v0, Lcom/google/maps/b/a/cr;->c:Z

    .line 144
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bz;->c()V

    .line 146
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_1

    .line 92
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/b/a/ae;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    sget-object v4, Lcom/google/maps/b/a/ae;->r:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    .line 97
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    return-object v0

    .line 94
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/maps/b/a/ae;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Lcom/google/maps/b/a/bz;
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    invoke-virtual {v0}, Lcom/google/maps/b/a/bz;->c()V

    .line 121
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    .line 124
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    iget-object v1, p0, Lcom/google/maps/b/a/ae;->t:[B

    iget-object v2, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iget v2, v2, Lcom/google/maps/b/a/cr;->a:I

    iget-object v3, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iget v3, v3, Lcom/google/maps/b/a/cr;->b:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/b/a/bz;->a([BII)V

    .line 124
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->l:Lcom/google/maps/b/a/bz;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/maps/b/a/ae;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x7

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "id: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move v0, v1

    .line 191
    :goto_0
    iget-object v3, p0, Lcom/google/maps/b/a/ae;->a:Lcom/google/maps/b/a/cs;

    iget v3, v3, Lcom/google/maps/b/a/cs;->b:I

    if-ge v0, v3, :cond_1

    .line 192
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/ae;->b(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "enclosing_building: \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\"\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->h:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_2

    .line 195
    invoke-virtual {p0}, Lcom/google/maps/b/a/ae;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "name: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->j:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_3

    .line 198
    invoke-virtual {p0}, Lcom/google/maps/b/a/ae;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "short_name: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\"\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_4

    .line 201
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "average_elevation_meters: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_4
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_5

    .line 204
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x17

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "number_e3: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/ae;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_7

    .line 207
    invoke-virtual {p0}, Lcom/google/maps/b/a/ae;->e()Lcom/google/maps/b/a/bz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/b/a/bz;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    const-string v3, "\n"

    const-string v4, "\n  "

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 209
    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 210
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "preferred_viewport {\n  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
