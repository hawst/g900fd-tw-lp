.class Lcom/google/maps/mapsactivities/android/a;
.super Lcom/android/datetimepicker/date/h;
.source "PG"


# instance fields
.field final synthetic c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;


# direct methods
.method public constructor <init>(Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    .line 234
    invoke-direct {p0, p1, p2}, Lcom/android/datetimepicker/date/h;-><init>(Lcom/android/datetimepicker/date/MonthView;Landroid/view/View;)V

    .line 235
    return-void
.end method


# virtual methods
.method protected final a(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget-object v0, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->E:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v1, v1, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->m:I

    iget-object v2, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v2, v2, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->n:I

    invoke-virtual {v0, p1, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 241
    iget-object v0, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget-object v0, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->E:Landroid/text/format/Time;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 242
    iget-object v0, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget-object v0, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->E:Landroid/text/format/Time;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 243
    const-string v2, "EEEE dd MMMM yyyy"

    invoke-static {v2, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 244
    return-object v0
.end method

.method protected final a(ILandroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v0, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->g:I

    iget-object v1, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v1, v1, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->F:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 250
    iget-object v1, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v1, v1, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->p:I

    iget-object v2, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v2, v2, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->e:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v2, v2, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->p:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 251
    iget-object v2, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v2, v2, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->p:I

    .line 252
    iget-object v3, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v3, v3, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->e:I

    mul-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v4, v4, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->F:I

    add-int/2addr v3, v4

    .line 253
    add-int/lit8 v4, p1, -0x1

    iget-object v5, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    invoke-static {v5}, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->a(Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;)I

    move-result v5

    add-int/2addr v4, v5

    .line 254
    iget-object v5, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v5, v5, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->u:I

    div-int v5, v4, v5

    .line 255
    iget-object v6, p0, Lcom/google/maps/mapsactivities/android/a;->c:Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    iget v6, v6, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->u:I

    rem-int/2addr v4, v6

    .line 256
    mul-int/2addr v4, v3

    add-int/2addr v0, v4

    .line 257
    mul-int v4, v5, v2

    add-int/2addr v1, v4

    .line 259
    add-int/2addr v3, v0

    add-int/2addr v2, v1

    invoke-virtual {p2, v0, v1, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 260
    return-void
.end method
