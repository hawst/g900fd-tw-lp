.class public final Lcom/google/i/a/k;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/i/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/i/a/k;",
        ">;",
        "Lcom/google/i/a/n;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/k;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/i/a/k;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:J

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Ljava/lang/Object;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4315
    new-instance v0, Lcom/google/i/a/l;

    invoke-direct {v0}, Lcom/google/i/a/l;-><init>()V

    sput-object v0, Lcom/google/i/a/k;->PARSER:Lcom/google/n/ax;

    .line 4619
    const/4 v0, 0x0

    sput-object v0, Lcom/google/i/a/k;->n:Lcom/google/n/aw;

    .line 5381
    new-instance v0, Lcom/google/i/a/k;

    invoke-direct {v0}, Lcom/google/i/a/k;-><init>()V

    sput-object v0, Lcom/google/i/a/k;->k:Lcom/google/i/a/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4221
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 4332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    .line 4363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    .line 4379
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    .line 4395
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    .line 4411
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    .line 4427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    .line 4485
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    .line 4500
    iput-byte v1, p0, Lcom/google/i/a/k;->l:B

    .line 4569
    iput v1, p0, Lcom/google/i/a/k;->m:I

    .line 4222
    iget-object v0, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4223
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/i/a/k;->c:J

    .line 4224
    iget-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4225
    iget-object v0, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4226
    iget-object v0, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4227
    iget-object v0, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4228
    iget-object v0, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4229
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    .line 4230
    iget-object v0, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 4231
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 4237
    invoke-direct {p0}, Lcom/google/i/a/k;-><init>()V

    .line 4238
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 4243
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 4244
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 4245
    sparse-switch v5, :sswitch_data_0

    .line 4250
    iget-object v0, p0, Lcom/google/i/a/k;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/i/a/k;->k:Lcom/google/i/a/k;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/i/a/k;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 4253
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 4248
    goto :goto_0

    .line 4258
    :sswitch_1
    iget-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4259
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/i/a/k;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4305
    :catch_0
    move-exception v0

    .line 4306
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4311
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/k;->au:Lcom/google/n/bn;

    .line 4312
    iget-object v1, p0, Lcom/google/i/a/k;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 4263
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4264
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/i/a/k;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4307
    :catch_1
    move-exception v0

    .line 4308
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4309
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4268
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4269
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/i/a/k;->a:I

    goto :goto_0

    .line 4273
    :sswitch_4
    iget-object v0, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4274
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/i/a/k;->a:I

    goto/16 :goto_0

    .line 4278
    :sswitch_5
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/i/a/k;->a:I

    .line 4279
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/i/a/k;->c:J

    goto/16 :goto_0

    .line 4283
    :sswitch_6
    iget-object v0, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4284
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/i/a/k;->a:I

    goto/16 :goto_0

    .line 4288
    :sswitch_7
    iget-object v0, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4289
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/i/a/k;->a:I

    goto/16 :goto_0

    .line 4293
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4294
    iget v1, p0, Lcom/google/i/a/k;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/i/a/k;->a:I

    .line 4295
    iput-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4299
    :sswitch_9
    iget-object v0, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4300
    iget v0, p0, Lcom/google/i/a/k;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/i/a/k;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4311
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/k;->au:Lcom/google/n/bn;

    .line 4312
    iget-object v0, p0, Lcom/google/i/a/k;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 4313
    :cond_3
    return-void

    .line 4245
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x38 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x6a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/i/a/k;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 4219
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 4332
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    .line 4363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    .line 4379
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    .line 4395
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    .line 4411
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    .line 4427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    .line 4485
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    .line 4500
    iput-byte v1, p0, Lcom/google/i/a/k;->l:B

    .line 4569
    iput v1, p0, Lcom/google/i/a/k;->m:I

    .line 4220
    return-void
.end method

.method public static d()Lcom/google/i/a/k;
    .locals 1

    .prologue
    .line 5384
    sget-object v0, Lcom/google/i/a/k;->k:Lcom/google/i/a/k;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/i/a/m;
    .locals 1

    .prologue
    .line 4681
    new-instance v0, Lcom/google/i/a/m;

    invoke-direct {v0}, Lcom/google/i/a/m;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4327
    sget-object v0, Lcom/google/i/a/k;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 4534
    invoke-virtual {p0}, Lcom/google/i/a/k;->c()I

    .line 4537
    new-instance v1, Lcom/google/n/y;

    invoke-direct {v1, p0, v5}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 4538
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 4539
    iget-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4541
    :cond_0
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 4542
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4544
    :cond_1
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 4545
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4547
    :cond_2
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_3

    .line 4548
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4550
    :cond_3
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 4551
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/i/a/k;->c:J

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 4553
    :cond_4
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 4554
    iget-object v0, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4556
    :cond_5
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 4557
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4559
    :cond_6
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_7

    .line 4560
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4562
    :cond_7
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_8

    .line 4563
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4565
    :cond_8
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 4566
    iget-object v0, p0, Lcom/google/i/a/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4567
    return-void

    .line 4560
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4502
    iget-byte v0, p0, Lcom/google/i/a/k;->l:B

    .line 4503
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 4529
    :goto_0
    return v0

    .line 4504
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 4506
    :cond_1
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 4507
    iget-object v0, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    invoke-virtual {v0}, Lcom/google/b/f/p;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4508
    iput-byte v2, p0, Lcom/google/i/a/k;->l:B

    move v0, v2

    .line 4509
    goto :goto_0

    :cond_2
    move v0, v2

    .line 4506
    goto :goto_1

    .line 4512
    :cond_3
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 4513
    iget-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/u;->d()Lcom/google/i/a/u;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/u;

    invoke-virtual {v0}, Lcom/google/i/a/u;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 4514
    iput-byte v2, p0, Lcom/google/i/a/k;->l:B

    move v0, v2

    .line 4515
    goto :goto_0

    :cond_4
    move v0, v2

    .line 4512
    goto :goto_2

    .line 4518
    :cond_5
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 4519
    iget-object v0, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/g;->d()Lcom/google/i/a/g;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/g;

    invoke-virtual {v0}, Lcom/google/i/a/g;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 4520
    iput-byte v2, p0, Lcom/google/i/a/k;->l:B

    move v0, v2

    .line 4521
    goto :goto_0

    :cond_6
    move v0, v2

    .line 4518
    goto :goto_3

    .line 4524
    :cond_7
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 4525
    iput-byte v2, p0, Lcom/google/i/a/k;->l:B

    move v0, v2

    .line 4526
    goto :goto_0

    .line 4528
    :cond_8
    iput-byte v1, p0, Lcom/google/i/a/k;->l:B

    move v0, v1

    .line 4529
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 4571
    iget v0, p0, Lcom/google/i/a/k;->m:I

    .line 4572
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4614
    :goto_0
    return v0

    .line 4575
    :cond_0
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_a

    .line 4576
    iget-object v0, p0, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    .line 4577
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4579
    :goto_1
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 4580
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    .line 4581
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4583
    :cond_1
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_2

    .line 4584
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    .line 4585
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4587
    :cond_2
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 4588
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    .line 4589
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4591
    :cond_3
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_4

    .line 4592
    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/i/a/k;->c:J

    .line 4593
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4595
    :cond_4
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 4596
    iget-object v2, p0, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    .line 4597
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4599
    :cond_5
    iget v2, p0, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_9

    .line 4600
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    .line 4601
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 4603
    :goto_2
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 4604
    const/16 v3, 0xa

    .line 4605
    iget-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4607
    :cond_6
    iget v0, p0, Lcom/google/i/a/k;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_7

    .line 4608
    const/16 v0, 0xd

    iget-object v3, p0, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    .line 4609
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 4611
    :cond_7
    invoke-virtual {p0}, Lcom/google/i/a/k;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 4612
    iget-object v1, p0, Lcom/google/i/a/k;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4613
    iput v0, p0, Lcom/google/i/a/k;->m:I

    goto/16 :goto_0

    .line 4605
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_9
    move v2, v0

    goto :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4212
    invoke-static {}, Lcom/google/i/a/k;->newBuilder()Lcom/google/i/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/i/a/m;->a(Lcom/google/i/a/k;)Lcom/google/i/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4212
    invoke-static {}, Lcom/google/i/a/k;->newBuilder()Lcom/google/i/a/m;

    move-result-object v0

    return-object v0
.end method
