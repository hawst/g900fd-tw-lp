.class public final Lcom/google/i/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/i/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/i/a/b;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:J

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/google/i/a/c;

    invoke-direct {v0}, Lcom/google/i/a/c;-><init>()V

    sput-object v0, Lcom/google/i/a/b;->PARSER:Lcom/google/n/ax;

    .line 355
    const/4 v0, 0x0

    sput-object v0, Lcom/google/i/a/b;->i:Lcom/google/n/aw;

    .line 772
    new-instance v0, Lcom/google/i/a/b;

    invoke-direct {v0}, Lcom/google/i/a/b;-><init>()V

    sput-object v0, Lcom/google/i/a/b;->f:Lcom/google/i/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 77
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 298
    iput-byte v0, p0, Lcom/google/i/a/b;->g:B

    .line 326
    iput v0, p0, Lcom/google/i/a/b;->h:I

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/i/a/b;->c:J

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 88
    invoke-direct {p0}, Lcom/google/i/a/b;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 93
    const/4 v0, 0x0

    .line 94
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 96
    sparse-switch v3, :sswitch_data_0

    .line 101
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 103
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 109
    iget v4, p0, Lcom/google/i/a/b;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/i/a/b;->a:I

    .line 110
    iput-object v3, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 114
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/i/a/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/i/a/b;->a:I

    .line 115
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/i/a/b;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    .line 135
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 136
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 120
    iget v4, p0, Lcom/google/i/a/b;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/i/a/b;->a:I

    .line 121
    iput-object v3, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    goto :goto_0

    .line 125
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 126
    iget v4, p0, Lcom/google/i/a/b;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/i/a/b;->a:I

    .line 127
    iput-object v3, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 138
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->au:Lcom/google/n/bn;

    .line 139
    return-void

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 298
    iput-byte v0, p0, Lcom/google/i/a/b;->g:B

    .line 326
    iput v0, p0, Lcom/google/i/a/b;->h:I

    .line 76
    return-void
.end method

.method public static d()Lcom/google/i/a/b;
    .locals 1

    .prologue
    .line 775
    sget-object v0, Lcom/google/i/a/b;->f:Lcom/google/i/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/i/a/d;
    .locals 1

    .prologue
    .line 417
    new-instance v0, Lcom/google/i/a/d;

    invoke-direct {v0}, Lcom/google/i/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    sget-object v0, Lcom/google/i/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 310
    invoke-virtual {p0}, Lcom/google/i/a/b;->c()I

    .line 311
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 314
    :cond_0
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 315
    iget-wide v0, p0, Lcom/google/i/a/b;->c:J

    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 317
    :cond_1
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 318
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 320
    :cond_2
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 321
    iget-object v0, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 323
    :cond_3
    iget-object v0, p0, Lcom/google/i/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 324
    return-void

    .line 312
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 318
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 321
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 300
    iget-byte v1, p0, Lcom/google/i/a/b;->g:B

    .line 301
    if-ne v1, v0, :cond_0

    .line 305
    :goto_0
    return v0

    .line 302
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 304
    :cond_1
    iput-byte v0, p0, Lcom/google/i/a/b;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 328
    iget v0, p0, Lcom/google/i/a/b;->h:I

    .line 329
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 350
    :goto_0
    return v0

    .line 332
    :cond_0
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 334
    iget-object v0, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 336
    :goto_2
    iget v2, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    .line 337
    iget-wide v2, p0, Lcom/google/i/a/b;->c:J

    .line 338
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 340
    :goto_3
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 341
    const/4 v3, 0x3

    .line 342
    iget-object v0, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 344
    :cond_1
    iget v0, p0, Lcom/google/i/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/b;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 348
    :cond_2
    iget-object v0, p0, Lcom/google/i/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 349
    iput v0, p0, Lcom/google/i/a/b;->h:I

    goto/16 :goto_0

    .line 334
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 342
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 346
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/google/i/a/b;->newBuilder()Lcom/google/i/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/i/a/d;->a(Lcom/google/i/a/b;)Lcom/google/i/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/google/i/a/b;->newBuilder()Lcom/google/i/a/d;

    move-result-object v0

    return-object v0
.end method
