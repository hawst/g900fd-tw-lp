.class public final Lcom/google/i/a/w;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/i/a/x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/i/a/u;",
        "Lcom/google/i/a/w;",
        ">;",
        "Lcom/google/i/a/x;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 911
    sget-object v0, Lcom/google/i/a/u;->f:Lcom/google/i/a/u;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1011
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    .line 1148
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    .line 1284
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/w;->d:Ljava/lang/Object;

    .line 1360
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/w;->e:Ljava/lang/Object;

    .line 912
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/i/a/u;)Lcom/google/i/a/w;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 957
    invoke-static {}, Lcom/google/i/a/u;->d()Lcom/google/i/a/u;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 989
    :goto_0
    return-object p0

    .line 958
    :cond_0
    iget-object v2, p1, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 959
    iget-object v2, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 960
    iget-object v2, p1, Lcom/google/i/a/u;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    .line 961
    iget v2, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/i/a/w;->a:I

    .line 968
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 969
    iget-object v2, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 970
    iget-object v2, p1, Lcom/google/i/a/u;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    .line 971
    iget v2, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/i/a/w;->a:I

    .line 978
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/i/a/u;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 979
    iget v2, p0, Lcom/google/i/a/w;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/i/a/w;->a:I

    .line 980
    iget-object v2, p1, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/i/a/w;->d:Ljava/lang/Object;

    .line 983
    :cond_3
    iget v2, p1, Lcom/google/i/a/u;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    :goto_4
    if-eqz v0, :cond_4

    .line 984
    iget v0, p0, Lcom/google/i/a/w;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/i/a/w;->a:I

    .line 985
    iget-object v0, p1, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/i/a/w;->e:Ljava/lang/Object;

    .line 988
    :cond_4
    iget-object v0, p1, Lcom/google/i/a/u;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 963
    :cond_5
    iget v2, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/i/a/w;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/i/a/w;->a:I

    .line 964
    :cond_6
    iget-object v2, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 973
    :cond_7
    iget v2, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/i/a/w;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/i/a/w;->a:I

    .line 974
    :cond_8
    iget-object v2, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 978
    goto :goto_3

    :cond_a
    move v0, v1

    .line 983
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 903
    new-instance v2, Lcom/google/i/a/u;

    invoke-direct {v2, p0}, Lcom/google/i/a/u;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/i/a/w;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/i/a/w;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/i/a/u;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/i/a/w;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/i/a/w;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/i/a/u;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/i/a/w;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-object v1, p0, Lcom/google/i/a/w;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/i/a/u;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 903
    check-cast p1, Lcom/google/i/a/u;

    invoke-virtual {p0, p1}, Lcom/google/i/a/w;->a(Lcom/google/i/a/u;)Lcom/google/i/a/w;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 993
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 994
    iget-object v0, p0, Lcom/google/i/a/w;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/o;

    invoke-virtual {v0}, Lcom/google/i/a/o;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1005
    :cond_0
    :goto_1
    return v2

    .line 993
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 999
    :goto_2
    iget-object v0, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1000
    iget-object v0, p0, Lcom/google/i/a/w;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/o;

    invoke-virtual {v0}, Lcom/google/i/a/o;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 999
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1005
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method
