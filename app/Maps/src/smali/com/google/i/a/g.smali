.class public final Lcom/google/i/a/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/i/a/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/g;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/i/a/g;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1519
    new-instance v0, Lcom/google/i/a/h;

    invoke-direct {v0}, Lcom/google/i/a/h;-><init>()V

    sput-object v0, Lcom/google/i/a/g;->PARSER:Lcom/google/n/ax;

    .line 1593
    const/4 v0, 0x0

    sput-object v0, Lcom/google/i/a/g;->f:Lcom/google/n/aw;

    .line 1785
    new-instance v0, Lcom/google/i/a/g;

    invoke-direct {v0}, Lcom/google/i/a/g;-><init>()V

    sput-object v0, Lcom/google/i/a/g;->c:Lcom/google/i/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1476
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1536
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    .line 1551
    iput-byte v2, p0, Lcom/google/i/a/g;->d:B

    .line 1576
    iput v2, p0, Lcom/google/i/a/g;->e:I

    .line 1477
    iget-object v0, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1478
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1484
    invoke-direct {p0}, Lcom/google/i/a/g;-><init>()V

    .line 1485
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1490
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1491
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1492
    sparse-switch v3, :sswitch_data_0

    .line 1497
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1499
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1495
    goto :goto_0

    .line 1504
    :sswitch_1
    iget-object v3, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1505
    iget v3, p0, Lcom/google/i/a/g;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/i/a/g;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1510
    :catch_0
    move-exception v0

    .line 1511
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1516
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/g;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/g;->au:Lcom/google/n/bn;

    .line 1517
    return-void

    .line 1512
    :catch_1
    move-exception v0

    .line 1513
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 1514
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1492
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1474
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1536
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    .line 1551
    iput-byte v1, p0, Lcom/google/i/a/g;->d:B

    .line 1576
    iput v1, p0, Lcom/google/i/a/g;->e:I

    .line 1475
    return-void
.end method

.method public static d()Lcom/google/i/a/g;
    .locals 1

    .prologue
    .line 1788
    sget-object v0, Lcom/google/i/a/g;->c:Lcom/google/i/a/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/i/a/i;
    .locals 1

    .prologue
    .line 1655
    new-instance v0, Lcom/google/i/a/i;

    invoke-direct {v0}, Lcom/google/i/a/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1531
    sget-object v0, Lcom/google/i/a/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1569
    invoke-virtual {p0}, Lcom/google/i/a/g;->c()I

    .line 1570
    iget v0, p0, Lcom/google/i/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1571
    iget-object v0, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1573
    :cond_0
    iget-object v0, p0, Lcom/google/i/a/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1574
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1553
    iget-byte v0, p0, Lcom/google/i/a/g;->d:B

    .line 1554
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1564
    :goto_0
    return v0

    .line 1555
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1557
    :cond_1
    iget v0, p0, Lcom/google/i/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1558
    iget-object v0, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/o;

    invoke-virtual {v0}, Lcom/google/i/a/o;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1559
    iput-byte v2, p0, Lcom/google/i/a/g;->d:B

    move v0, v2

    .line 1560
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1557
    goto :goto_1

    .line 1563
    :cond_3
    iput-byte v1, p0, Lcom/google/i/a/g;->d:B

    move v0, v1

    .line 1564
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1578
    iget v1, p0, Lcom/google/i/a/g;->e:I

    .line 1579
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 1588
    :goto_0
    return v0

    .line 1582
    :cond_0
    iget v1, p0, Lcom/google/i/a/g;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 1583
    iget-object v1, p0, Lcom/google/i/a/g;->b:Lcom/google/n/ao;

    .line 1584
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1586
    :cond_1
    iget-object v1, p0, Lcom/google/i/a/g;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1587
    iput v0, p0, Lcom/google/i/a/g;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1468
    invoke-static {}, Lcom/google/i/a/g;->newBuilder()Lcom/google/i/a/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/i/a/i;->a(Lcom/google/i/a/g;)Lcom/google/i/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1468
    invoke-static {}, Lcom/google/i/a/g;->newBuilder()Lcom/google/i/a/i;

    move-result-object v0

    return-object v0
.end method
