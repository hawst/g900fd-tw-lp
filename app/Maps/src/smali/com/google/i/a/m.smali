.class public final Lcom/google/i/a/m;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/i/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/i/a/k;",
        "Lcom/google/i/a/m;",
        ">;",
        "Lcom/google/i/a/n;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;

.field private g:J

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4698
    sget-object v0, Lcom/google/i/a/k;->k:Lcom/google/i/a/k;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 4856
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->f:Lcom/google/n/ao;

    .line 4947
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->h:Lcom/google/n/ao;

    .line 5006
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->i:Lcom/google/n/ao;

    .line 5065
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    .line 5124
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->j:Lcom/google/n/ao;

    .line 5183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->k:Lcom/google/n/ao;

    .line 5242
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/m;->c:Ljava/lang/Object;

    .line 5318
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/i/a/m;->l:Lcom/google/n/ao;

    .line 4699
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/i/a/k;)Lcom/google/i/a/m;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4786
    invoke-static {}, Lcom/google/i/a/k;->d()Lcom/google/i/a/k;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4825
    :goto_0
    return-object p0

    .line 4787
    :cond_0
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4788
    iget-object v2, p0, Lcom/google/i/a/m;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4789
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4791
    :cond_1
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4792
    iget-wide v2, p1, Lcom/google/i/a/k;->c:J

    iget v4, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/i/a/m;->a:I

    iput-wide v2, p0, Lcom/google/i/a/m;->g:J

    .line 4794
    :cond_2
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4795
    iget-object v2, p0, Lcom/google/i/a/m;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4796
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4798
    :cond_3
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 4799
    iget-object v2, p0, Lcom/google/i/a/m;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4800
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4802
    :cond_4
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 4803
    iget-object v2, p0, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4804
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4806
    :cond_5
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 4807
    iget-object v2, p0, Lcom/google/i/a/m;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4808
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4810
    :cond_6
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 4811
    iget-object v2, p0, Lcom/google/i/a/m;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4812
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4814
    :cond_7
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 4815
    iget v2, p0, Lcom/google/i/a/m;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/i/a/m;->a:I

    .line 4816
    iget-object v2, p1, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/i/a/m;->c:Ljava/lang/Object;

    .line 4819
    :cond_8
    iget v2, p1, Lcom/google/i/a/k;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    :goto_9
    if-eqz v0, :cond_9

    .line 4820
    iget-object v0, p0, Lcom/google/i/a/m;->l:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4821
    iget v0, p0, Lcom/google/i/a/m;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/i/a/m;->a:I

    .line 4823
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/i/a/m;->a(Lcom/google/n/x;)V

    .line 4824
    iget-object v0, p1, Lcom/google/i/a/k;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 4787
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 4791
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 4794
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 4798
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 4802
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 4806
    goto :goto_6

    :cond_10
    move v2, v1

    .line 4810
    goto :goto_7

    :cond_11
    move v2, v1

    .line 4814
    goto :goto_8

    :cond_12
    move v0, v1

    .line 4819
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4691
    new-instance v2, Lcom/google/i/a/k;

    invoke-direct {v2, p0}, Lcom/google/i/a/k;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/i/a/m;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, v2, Lcom/google/i/a/k;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/i/a/m;->g:J

    iput-wide v4, v2, Lcom/google/i/a/k;->c:J

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/i/a/k;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/i/a/k;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/i/a/k;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/i/a/k;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/i/a/k;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/i/a/m;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/i/a/m;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, p0, Lcom/google/i/a/m;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/i/a/k;->i:Ljava/lang/Object;

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v3, v2, Lcom/google/i/a/k;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/i/a/m;->l:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/i/a/m;->l:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/i/a/k;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4691
    check-cast p1, Lcom/google/i/a/k;

    invoke-virtual {p0, p1}, Lcom/google/i/a/m;->a(Lcom/google/i/a/k;)Lcom/google/i/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4829
    iget v0, p0, Lcom/google/i/a/m;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 4830
    iget-object v0, p0, Lcom/google/i/a/m;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/p;->d()Lcom/google/b/f/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    invoke-virtual {v0}, Lcom/google/b/f/p;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 4851
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 4829
    goto :goto_0

    .line 4835
    :cond_1
    iget v0, p0, Lcom/google/i/a/m;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 4836
    iget-object v0, p0, Lcom/google/i/a/m;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/u;->d()Lcom/google/i/a/u;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/u;

    invoke-virtual {v0}, Lcom/google/i/a/u;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 4838
    goto :goto_1

    :cond_2
    move v0, v1

    .line 4835
    goto :goto_2

    .line 4841
    :cond_3
    iget v0, p0, Lcom/google/i/a/m;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 4842
    iget-object v0, p0, Lcom/google/i/a/m;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/g;->d()Lcom/google/i/a/g;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/g;

    invoke-virtual {v0}, Lcom/google/i/a/g;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 4844
    goto :goto_1

    :cond_4
    move v0, v1

    .line 4841
    goto :goto_3

    .line 4847
    :cond_5
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 4849
    goto :goto_1

    :cond_6
    move v0, v2

    .line 4851
    goto :goto_1
.end method
