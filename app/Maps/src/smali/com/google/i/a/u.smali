.class public final Lcom/google/i/a/u;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/i/a/x;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/u;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/i/a/u;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576
    new-instance v0, Lcom/google/i/a/v;

    invoke-direct {v0}, Lcom/google/i/a/v;-><init>()V

    sput-object v0, Lcom/google/i/a/u;->PARSER:Lcom/google/n/ax;

    .line 831
    const/4 v0, 0x0

    sput-object v0, Lcom/google/i/a/u;->i:Lcom/google/n/aw;

    .line 1440
    new-instance v0, Lcom/google/i/a/u;

    invoke-direct {v0}, Lcom/google/i/a/u;-><init>()V

    sput-object v0, Lcom/google/i/a/u;->f:Lcom/google/i/a/u;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 497
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 762
    iput-byte v0, p0, Lcom/google/i/a/u;->g:B

    .line 802
    iput v0, p0, Lcom/google/i/a/u;->h:I

    .line 498
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    .line 499
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    .line 500
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    .line 501
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    .line 502
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 508
    invoke-direct {p0}, Lcom/google/i/a/u;-><init>()V

    .line 511
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 514
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 515
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 516
    sparse-switch v4, :sswitch_data_0

    .line 521
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 523
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 519
    goto :goto_0

    .line 528
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 529
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    .line 531
    or-int/lit8 v1, v1, 0x1

    .line 533
    :cond_1
    iget-object v4, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 534
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 533
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 561
    :catch_0
    move-exception v0

    .line 562
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 567
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 568
    iget-object v2, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    .line 570
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 571
    iget-object v1, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    .line 573
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/u;->au:Lcom/google/n/bn;

    throw v0

    .line 538
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_4

    .line 539
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    .line 541
    or-int/lit8 v1, v1, 0x2

    .line 543
    :cond_4
    iget-object v4, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 544
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 543
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 563
    :catch_1
    move-exception v0

    .line 564
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 565
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 548
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 549
    iget v5, p0, Lcom/google/i/a/u;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/i/a/u;->a:I

    .line 550
    iput-object v4, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 554
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 555
    iget v5, p0, Lcom/google/i/a/u;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/i/a/u;->a:I

    .line 556
    iput-object v4, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 567
    :cond_5
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_6

    .line 568
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    .line 570
    :cond_6
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_7

    .line 571
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    .line 573
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->au:Lcom/google/n/bn;

    .line 574
    return-void

    .line 516
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 495
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 762
    iput-byte v0, p0, Lcom/google/i/a/u;->g:B

    .line 802
    iput v0, p0, Lcom/google/i/a/u;->h:I

    .line 496
    return-void
.end method

.method public static d()Lcom/google/i/a/u;
    .locals 1

    .prologue
    .line 1443
    sget-object v0, Lcom/google/i/a/u;->f:Lcom/google/i/a/u;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/i/a/w;
    .locals 1

    .prologue
    .line 893
    new-instance v0, Lcom/google/i/a/w;

    invoke-direct {v0}, Lcom/google/i/a/w;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/u;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    sget-object v0, Lcom/google/i/a/u;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 786
    invoke-virtual {p0}, Lcom/google/i/a/u;->c()I

    move v1, v2

    .line 787
    :goto_0
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 787
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 790
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 791
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 790
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 793
    :cond_1
    iget v0, p0, Lcom/google/i/a/u;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 794
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 796
    :cond_2
    iget v0, p0, Lcom/google/i/a/u;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 797
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 799
    :cond_3
    iget-object v0, p0, Lcom/google/i/a/u;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 800
    return-void

    .line 794
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 797
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 764
    iget-byte v0, p0, Lcom/google/i/a/u;->g:B

    .line 765
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 781
    :cond_0
    :goto_0
    return v2

    .line 766
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 768
    :goto_1
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 769
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/o;

    invoke-virtual {v0}, Lcom/google/i/a/o;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 770
    iput-byte v2, p0, Lcom/google/i/a/u;->g:B

    goto :goto_0

    .line 768
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 774
    :goto_2
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 775
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/o;

    invoke-virtual {v0}, Lcom/google/i/a/o;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 776
    iput-byte v2, p0, Lcom/google/i/a/u;->g:B

    goto :goto_0

    .line 774
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 780
    :cond_5
    iput-byte v3, p0, Lcom/google/i/a/u;->g:B

    move v2, v3

    .line 781
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 804
    iget v0, p0, Lcom/google/i/a/u;->h:I

    .line 805
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 826
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 808
    :goto_1
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 809
    iget-object v0, p0, Lcom/google/i/a/u;->b:Ljava/util/List;

    .line 810
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 808
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 812
    :goto_2
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 813
    iget-object v0, p0, Lcom/google/i/a/u;->c:Ljava/util/List;

    .line 814
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 812
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 816
    :cond_2
    iget v0, p0, Lcom/google/i/a/u;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 817
    const/4 v1, 0x3

    .line 818
    iget-object v0, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 820
    :cond_3
    iget v0, p0, Lcom/google/i/a/u;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 821
    const/4 v1, 0x4

    .line 822
    iget-object v0, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/u;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 824
    :cond_4
    iget-object v0, p0, Lcom/google/i/a/u;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 825
    iput v0, p0, Lcom/google/i/a/u;->h:I

    goto/16 :goto_0

    .line 818
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 822
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 489
    invoke-static {}, Lcom/google/i/a/u;->newBuilder()Lcom/google/i/a/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/i/a/w;->a(Lcom/google/i/a/u;)Lcom/google/i/a/w;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 489
    invoke-static {}, Lcom/google/i/a/u;->newBuilder()Lcom/google/i/a/w;

    move-result-object v0

    return-object v0
.end method
