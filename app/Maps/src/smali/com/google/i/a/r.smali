.class public final enum Lcom/google/i/a/r;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/i/a/r;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/i/a/r;

.field public static final enum b:Lcom/google/i/a/r;

.field public static final enum c:Lcom/google/i/a/r;

.field public static final enum d:Lcom/google/i/a/r;

.field public static final enum e:Lcom/google/i/a/r;

.field private static final synthetic g:[Lcom/google/i/a/r;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111
    new-instance v0, Lcom/google/i/a/r;

    const-string v1, "WEB_RESULT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/i/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/i/a/r;->a:Lcom/google/i/a/r;

    .line 115
    new-instance v0, Lcom/google/i/a/r;

    const-string v1, "LIVE_RESULT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/i/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/i/a/r;->b:Lcom/google/i/a/r;

    .line 119
    new-instance v0, Lcom/google/i/a/r;

    const-string v1, "LOCAL_RESULT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/i/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/i/a/r;->c:Lcom/google/i/a/r;

    .line 123
    new-instance v0, Lcom/google/i/a/r;

    const-string v1, "ACTION_RESULT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/i/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/i/a/r;->d:Lcom/google/i/a/r;

    .line 127
    new-instance v0, Lcom/google/i/a/r;

    const-string v1, "SHOWTIMES_RESULT"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/i/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/i/a/r;->e:Lcom/google/i/a/r;

    .line 106
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/i/a/r;

    sget-object v1, Lcom/google/i/a/r;->a:Lcom/google/i/a/r;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/i/a/r;->b:Lcom/google/i/a/r;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/i/a/r;->c:Lcom/google/i/a/r;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/i/a/r;->d:Lcom/google/i/a/r;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/i/a/r;->e:Lcom/google/i/a/r;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/i/a/r;->g:[Lcom/google/i/a/r;

    .line 172
    new-instance v0, Lcom/google/i/a/s;

    invoke-direct {v0}, Lcom/google/i/a/s;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 182
    iput p3, p0, Lcom/google/i/a/r;->f:I

    .line 183
    return-void
.end method

.method public static a(I)Lcom/google/i/a/r;
    .locals 1

    .prologue
    .line 157
    packed-switch p0, :pswitch_data_0

    .line 163
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 158
    :pswitch_0
    sget-object v0, Lcom/google/i/a/r;->a:Lcom/google/i/a/r;

    goto :goto_0

    .line 159
    :pswitch_1
    sget-object v0, Lcom/google/i/a/r;->b:Lcom/google/i/a/r;

    goto :goto_0

    .line 160
    :pswitch_2
    sget-object v0, Lcom/google/i/a/r;->c:Lcom/google/i/a/r;

    goto :goto_0

    .line 161
    :pswitch_3
    sget-object v0, Lcom/google/i/a/r;->d:Lcom/google/i/a/r;

    goto :goto_0

    .line 162
    :pswitch_4
    sget-object v0, Lcom/google/i/a/r;->e:Lcom/google/i/a/r;

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/i/a/r;
    .locals 1

    .prologue
    .line 106
    const-class v0, Lcom/google/i/a/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/r;

    return-object v0
.end method

.method public static values()[Lcom/google/i/a/r;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/i/a/r;->g:[Lcom/google/i/a/r;

    invoke-virtual {v0}, [Lcom/google/i/a/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/i/a/r;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/google/i/a/r;->f:I

    return v0
.end method
