.class public final Lcom/google/i/a/q;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/i/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/i/a/o;",
        "Lcom/google/i/a/q;",
        ">;",
        "Lcom/google/i/a/t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 329
    sget-object v0, Lcom/google/i/a/o;->c:Lcom/google/i/a/o;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 374
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/i/a/q;->b:I

    .line 330
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/i/a/o;)Lcom/google/i/a/q;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 355
    invoke-static {}, Lcom/google/i/a/o;->d()Lcom/google/i/a/o;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 361
    :goto_0
    return-object p0

    .line 356
    :cond_0
    iget v1, p1, Lcom/google/i/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 357
    iget v0, p1, Lcom/google/i/a/o;->b:I

    invoke-static {v0}, Lcom/google/i/a/r;->a(I)Lcom/google/i/a/r;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/i/a/r;->a:Lcom/google/i/a/r;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 356
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 357
    :cond_3
    iget v1, p0, Lcom/google/i/a/q;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/i/a/q;->a:I

    iget v0, v0, Lcom/google/i/a/r;->f:I

    iput v0, p0, Lcom/google/i/a/q;->b:I

    .line 359
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/i/a/q;->a(Lcom/google/n/x;)V

    .line 360
    iget-object v0, p1, Lcom/google/i/a/o;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 322
    new-instance v2, Lcom/google/i/a/o;

    invoke-direct {v2, p0}, Lcom/google/i/a/o;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/i/a/q;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget v1, p0, Lcom/google/i/a/q;->b:I

    iput v1, v2, Lcom/google/i/a/o;->b:I

    iput v0, v2, Lcom/google/i/a/o;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 322
    check-cast p1, Lcom/google/i/a/o;

    invoke-virtual {p0, p1}, Lcom/google/i/a/q;->a(Lcom/google/i/a/o;)Lcom/google/i/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    const/4 v0, 0x0

    .line 369
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
