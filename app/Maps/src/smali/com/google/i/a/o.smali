.class public final Lcom/google/i/a/o;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/i/a/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/i/a/o;",
        ">;",
        "Lcom/google/i/a/t;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/o;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/i/a/o;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/google/i/a/p;

    invoke-direct {v0}, Lcom/google/i/a/p;-><init>()V

    sput-object v0, Lcom/google/i/a/o;->PARSER:Lcom/google/n/ax;

    .line 250
    const/4 v0, 0x0

    sput-object v0, Lcom/google/i/a/o;->f:Lcom/google/n/aw;

    .line 414
    new-instance v0, Lcom/google/i/a/o;

    invoke-direct {v0}, Lcom/google/i/a/o;-><init>()V

    sput-object v0, Lcom/google/i/a/o;->c:Lcom/google/i/a/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 37
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 205
    iput-byte v0, p0, Lcom/google/i/a/o;->d:B

    .line 232
    iput v0, p0, Lcom/google/i/a/o;->e:I

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/i/a/o;->b:I

    .line 39
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 45
    invoke-direct {p0}, Lcom/google/i/a/o;-><init>()V

    .line 46
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 50
    const/4 v0, 0x0

    move v6, v0

    .line 51
    :cond_0
    :goto_0
    if-nez v6, :cond_3

    .line 52
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 53
    sparse-switch v5, :sswitch_data_0

    .line 58
    iget-object v0, p0, Lcom/google/i/a/o;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/i/a/o;->c:Lcom/google/i/a/o;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/i/a/o;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 61
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 56
    goto :goto_0

    .line 66
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 67
    invoke-static {v0}, Lcom/google/i/a/r;->a(I)Lcom/google/i/a/r;

    move-result-object v1

    .line 68
    if-nez v1, :cond_2

    .line 69
    const/4 v1, 0x1

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/i/a/o;->au:Lcom/google/n/bn;

    .line 85
    iget-object v1, p0, Lcom/google/i/a/o;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 71
    :cond_2
    :try_start_2
    iget v1, p0, Lcom/google/i/a/o;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/i/a/o;->a:I

    .line 72
    iput v0, p0, Lcom/google/i/a/o;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 80
    :catch_1
    move-exception v0

    .line 81
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 82
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/i/a/o;->au:Lcom/google/n/bn;

    .line 85
    iget-object v0, p0, Lcom/google/i/a/o;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 86
    :cond_4
    return-void

    .line 53
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/i/a/o;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 35
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 205
    iput-byte v0, p0, Lcom/google/i/a/o;->d:B

    .line 232
    iput v0, p0, Lcom/google/i/a/o;->e:I

    .line 36
    return-void
.end method

.method public static d()Lcom/google/i/a/o;
    .locals 1

    .prologue
    .line 417
    sget-object v0, Lcom/google/i/a/o;->c:Lcom/google/i/a/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/i/a/q;
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lcom/google/i/a/q;

    invoke-direct {v0}, Lcom/google/i/a/q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/i/a/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lcom/google/i/a/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 221
    invoke-virtual {p0}, Lcom/google/i/a/o;->c()I

    .line 224
    new-instance v0, Lcom/google/n/y;

    invoke-direct {v0, p0, v2}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 225
    iget v1, p0, Lcom/google/i/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_0

    .line 226
    iget v1, p0, Lcom/google/i/a/o;->b:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 228
    :cond_0
    :goto_0
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 229
    iget-object v0, p0, Lcom/google/i/a/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 230
    return-void

    .line 226
    :cond_1
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 207
    iget-byte v2, p0, Lcom/google/i/a/o;->d:B

    .line 208
    if-ne v2, v0, :cond_0

    .line 216
    :goto_0
    return v0

    .line 209
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 211
    :cond_1
    iget-object v2, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v2}, Lcom/google/n/q;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 212
    iput-byte v1, p0, Lcom/google/i/a/o;->d:B

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_2
    iput-byte v0, p0, Lcom/google/i/a/o;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 234
    iget v1, p0, Lcom/google/i/a/o;->e:I

    .line 235
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 245
    :goto_0
    return v0

    .line 238
    :cond_0
    iget v1, p0, Lcom/google/i/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 239
    iget v1, p0, Lcom/google/i/a/o;->b:I

    .line 240
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 242
    :cond_1
    invoke-virtual {p0}, Lcom/google/i/a/o;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    iget-object v1, p0, Lcom/google/i/a/o;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    iput v0, p0, Lcom/google/i/a/o;->e:I

    goto :goto_0

    .line 240
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/i/a/o;->newBuilder()Lcom/google/i/a/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/i/a/q;->a(Lcom/google/i/a/o;)Lcom/google/i/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/i/a/o;->newBuilder()Lcom/google/i/a/q;

    move-result-object v0

    return-object v0
.end method
