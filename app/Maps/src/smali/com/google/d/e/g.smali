.class public final Lcom/google/d/e/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/e/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/e/g;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/e/g;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/google/d/e/h;

    invoke-direct {v0}, Lcom/google/d/e/h;-><init>()V

    sput-object v0, Lcom/google/d/e/g;->PARSER:Lcom/google/n/ax;

    .line 284
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/e/g;->g:Lcom/google/n/aw;

    .line 567
    new-instance v0, Lcom/google/d/e/g;

    invoke-direct {v0}, Lcom/google/d/e/g;-><init>()V

    sput-object v0, Lcom/google/d/e/g;->d:Lcom/google/d/e/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 216
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    .line 231
    iput-byte v2, p0, Lcom/google/d/e/g;->e:B

    .line 263
    iput v2, p0, Lcom/google/d/e/g;->f:I

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 110
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Lcom/google/d/e/g;-><init>()V

    .line 117
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 122
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 123
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 124
    sparse-switch v3, :sswitch_data_0

    .line 129
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 131
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 127
    goto :goto_0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 137
    iget v4, p0, Lcom/google/d/e/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/e/g;->a:I

    .line 138
    iput-object v3, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/e/g;->au:Lcom/google/n/bn;

    throw v0

    .line 142
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 143
    iget v3, p0, Lcom/google/d/e/g;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/e/g;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 150
    :catch_1
    move-exception v0

    .line 151
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 152
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 154
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/g;->au:Lcom/google/n/bn;

    .line 155
    return-void

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 105
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 216
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    .line 231
    iput-byte v1, p0, Lcom/google/d/e/g;->e:B

    .line 263
    iput v1, p0, Lcom/google/d/e/g;->f:I

    .line 106
    return-void
.end method

.method public static d()Lcom/google/d/e/g;
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lcom/google/d/e/g;->d:Lcom/google/d/e/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/e/i;
    .locals 1

    .prologue
    .line 346
    new-instance v0, Lcom/google/d/e/i;

    invoke-direct {v0}, Lcom/google/d/e/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/e/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/google/d/e/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 253
    invoke-virtual {p0}, Lcom/google/d/e/g;->c()I

    .line 254
    iget v0, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 257
    :cond_0
    iget v0, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/google/d/e/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 261
    return-void

    .line 255
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233
    iget-byte v0, p0, Lcom/google/d/e/g;->e:B

    .line 234
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 248
    :goto_0
    return v0

    .line 235
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 237
    :cond_1
    iget v0, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 238
    iput-byte v2, p0, Lcom/google/d/e/g;->e:B

    move v0, v2

    .line 239
    goto :goto_0

    :cond_2
    move v0, v2

    .line 237
    goto :goto_1

    .line 241
    :cond_3
    iget v0, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 242
    iget-object v0, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/e/b;->d()Lcom/google/d/e/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/e/b;

    invoke-virtual {v0}, Lcom/google/d/e/b;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 243
    iput-byte v2, p0, Lcom/google/d/e/g;->e:B

    move v0, v2

    .line 244
    goto :goto_0

    :cond_4
    move v0, v2

    .line 241
    goto :goto_2

    .line 247
    :cond_5
    iput-byte v1, p0, Lcom/google/d/e/g;->e:B

    move v0, v1

    .line 248
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 265
    iget v0, p0, Lcom/google/d/e/g;->f:I

    .line 266
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 279
    :goto_0
    return v0

    .line 269
    :cond_0
    iget v0, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 271
    iget-object v0, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 273
    :goto_2
    iget v2, p0, Lcom/google/d/e/g;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 274
    iget-object v2, p0, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    .line 275
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/google/d/e/g;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 278
    iput v0, p0, Lcom/google/d/e/g;->f:I

    goto :goto_0

    .line 271
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/e/g;->newBuilder()Lcom/google/d/e/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/e/i;->a(Lcom/google/d/e/g;)Lcom/google/d/e/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/e/g;->newBuilder()Lcom/google/d/e/i;

    move-result-object v0

    return-object v0
.end method
