.class public final Lcom/google/d/e/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/e/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/e/b;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/e/b;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:J

.field d:J

.field e:J

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/google/d/e/c;

    invoke-direct {v0}, Lcom/google/d/e/c;-><init>()V

    sput-object v0, Lcom/google/d/e/b;->PARSER:Lcom/google/n/ax;

    .line 834
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/e/b;->j:Lcom/google/n/aw;

    .line 1321
    new-instance v0, Lcom/google/d/e/b;

    invoke-direct {v0}, Lcom/google/d/e/b;-><init>()V

    sput-object v0, Lcom/google/d/e/b;->g:Lcom/google/d/e/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, -0x1

    const-wide/16 v2, 0x0

    .line 81
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 762
    iput-byte v0, p0, Lcom/google/d/e/b;->h:B

    .line 801
    iput v0, p0, Lcom/google/d/e/b;->i:I

    .line 82
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    .line 83
    iput-wide v2, p0, Lcom/google/d/e/b;->c:J

    .line 84
    iput-wide v2, p0, Lcom/google/d/e/b;->d:J

    .line 85
    iput-wide v2, p0, Lcom/google/d/e/b;->e:J

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/16 v7, 0x10

    .line 93
    invoke-direct {p0}, Lcom/google/d/e/b;-><init>()V

    .line 96
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 99
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 101
    sparse-switch v4, :sswitch_data_0

    .line 106
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 108
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 114
    iget v5, p0, Lcom/google/d/e/b;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/e/b;->a:I

    .line 115
    iput-object v4, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_1

    .line 152
    iget-object v1, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    .line 154
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/e/b;->au:Lcom/google/n/bn;

    throw v0

    .line 119
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/d/e/b;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/e/b;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/e/b;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 147
    :catch_1
    move-exception v0

    .line 148
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 149
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/d/e/b;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/e/b;->a:I

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/e/b;->d:J

    goto :goto_0

    .line 129
    :sswitch_4
    and-int/lit8 v4, v1, 0x10

    if-eq v4, v7, :cond_2

    .line 130
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    .line 132
    or-int/lit8 v1, v1, 0x10

    .line 134
    :cond_2
    iget-object v4, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 134
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :sswitch_5
    iget v4, p0, Lcom/google/d/e/b;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/e/b;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/e/b;->e:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 151
    :cond_3
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_4

    .line 152
    iget-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    .line 154
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/b;->au:Lcom/google/n/bn;

    .line 155
    return-void

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 762
    iput-byte v0, p0, Lcom/google/d/e/b;->h:B

    .line 801
    iput v0, p0, Lcom/google/d/e/b;->i:I

    .line 80
    return-void
.end method

.method public static d()Lcom/google/d/e/b;
    .locals 1

    .prologue
    .line 1324
    sget-object v0, Lcom/google/d/e/b;->g:Lcom/google/d/e/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/e/d;
    .locals 1

    .prologue
    .line 896
    new-instance v0, Lcom/google/d/e/d;

    invoke-direct {v0}, Lcom/google/d/e/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/google/d/e/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 782
    invoke-virtual {p0}, Lcom/google/d/e/b;->c()I

    .line 783
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 784
    iget-object v0, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 786
    :cond_0
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 787
    iget-wide v0, p0, Lcom/google/d/e/b;->c:J

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 789
    :cond_1
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 790
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/d/e/b;->d:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    :cond_2
    move v1, v2

    .line 792
    :goto_1
    iget-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 793
    iget-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 792
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 784
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 795
    :cond_4
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 796
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/d/e/b;->e:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 798
    :cond_5
    iget-object v0, p0, Lcom/google/d/e/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 799
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 764
    iget-byte v2, p0, Lcom/google/d/e/b;->h:B

    .line 765
    if-ne v2, v0, :cond_0

    .line 777
    :goto_0
    return v0

    .line 766
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 768
    :cond_1
    iget v2, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 769
    iput-byte v1, p0, Lcom/google/d/e/b;->h:B

    move v0, v1

    .line 770
    goto :goto_0

    :cond_2
    move v2, v1

    .line 768
    goto :goto_1

    .line 772
    :cond_3
    iget v2, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 773
    iput-byte v1, p0, Lcom/google/d/e/b;->h:B

    move v0, v1

    .line 774
    goto :goto_0

    :cond_4
    move v2, v1

    .line 772
    goto :goto_2

    .line 776
    :cond_5
    iput-byte v0, p0, Lcom/google/d/e/b;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 803
    iget v0, p0, Lcom/google/d/e/b;->i:I

    .line 804
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 829
    :goto_0
    return v0

    .line 807
    :cond_0
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 809
    iget-object v0, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 811
    :goto_2
    iget v2, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 812
    iget-wide v2, p0, Lcom/google/d/e/b;->c:J

    .line 813
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 815
    :cond_1
    iget v2, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 816
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/d/e/b;->d:J

    .line 817
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_2
    move v2, v1

    move v3, v0

    .line 819
    :goto_3
    iget-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 820
    iget-object v0, p0, Lcom/google/d/e/b;->f:Ljava/util/List;

    .line 821
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 819
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 809
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 823
    :cond_4
    iget v0, p0, Lcom/google/d/e/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_5

    .line 824
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/d/e/b;->e:J

    .line 825
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 827
    :cond_5
    iget-object v0, p0, Lcom/google/d/e/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 828
    iput v0, p0, Lcom/google/d/e/b;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/e/b;->newBuilder()Lcom/google/d/e/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/e/d;->a(Lcom/google/d/e/b;)Lcom/google/d/e/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/e/b;->newBuilder()Lcom/google/d/e/d;

    move-result-object v0

    return-object v0
.end method
