.class public final Lcom/google/d/e/i;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/e/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/e/g;",
        "Lcom/google/d/e/i;",
        ">;",
        "Lcom/google/d/e/j;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 364
    sget-object v0, Lcom/google/d/e/g;->d:Lcom/google/d/e/g;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 428
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/e/i;->b:Ljava/lang/Object;

    .line 504
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/e/i;->c:Lcom/google/n/ao;

    .line 365
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/e/g;)Lcom/google/d/e/i;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 398
    invoke-static {}, Lcom/google/d/e/g;->d()Lcom/google/d/e/g;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 409
    :goto_0
    return-object p0

    .line 399
    :cond_0
    iget v2, p1, Lcom/google/d/e/g;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 400
    iget v2, p0, Lcom/google/d/e/i;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/e/i;->a:I

    .line 401
    iget-object v2, p1, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/e/i;->b:Ljava/lang/Object;

    .line 404
    :cond_1
    iget v2, p1, Lcom/google/d/e/g;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 405
    iget-object v0, p0, Lcom/google/d/e/i;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 406
    iget v0, p0, Lcom/google/d/e/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/e/i;->a:I

    .line 408
    :cond_2
    iget-object v0, p1, Lcom/google/d/e/g;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 399
    goto :goto_1

    :cond_4
    move v0, v1

    .line 404
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 356
    new-instance v2, Lcom/google/d/e/g;

    invoke-direct {v2, p0}, Lcom/google/d/e/g;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/e/i;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, p0, Lcom/google/d/e/i;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/e/g;->b:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/d/e/g;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/e/i;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/e/i;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/e/g;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 356
    check-cast p1, Lcom/google/d/e/g;

    invoke-virtual {p0, p1}, Lcom/google/d/e/i;->a(Lcom/google/d/e/g;)Lcom/google/d/e/i;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 413
    iget v0, p0, Lcom/google/d/e/i;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 423
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 413
    goto :goto_0

    .line 417
    :cond_1
    iget v0, p0, Lcom/google/d/e/i;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 418
    iget-object v0, p0, Lcom/google/d/e/i;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/e/b;->d()Lcom/google/d/e/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/e/b;

    invoke-virtual {v0}, Lcom/google/d/e/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 420
    goto :goto_1

    :cond_2
    move v0, v1

    .line 417
    goto :goto_2

    :cond_3
    move v0, v2

    .line 423
    goto :goto_1
.end method
