.class public final Lcom/google/d/e/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/e/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/e/b;",
        "Lcom/google/d/e/d;",
        ">;",
        "Lcom/google/d/e/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:J

.field private d:J

.field private e:J

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 914
    sget-object v0, Lcom/google/d/e/b;->g:Lcom/google/d/e/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1008
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/e/d;->b:Ljava/lang/Object;

    .line 1181
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    .line 915
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/e/b;)Lcom/google/d/e/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 965
    invoke-static {}, Lcom/google/d/e/b;->d()Lcom/google/d/e/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 991
    :goto_0
    return-object p0

    .line 966
    :cond_0
    iget v2, p1, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 967
    iget v2, p0, Lcom/google/d/e/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/e/d;->a:I

    .line 968
    iget-object v2, p1, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/e/d;->b:Ljava/lang/Object;

    .line 971
    :cond_1
    iget v2, p1, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 972
    iget-wide v2, p1, Lcom/google/d/e/b;->c:J

    iget v4, p0, Lcom/google/d/e/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/e/d;->a:I

    iput-wide v2, p0, Lcom/google/d/e/d;->c:J

    .line 974
    :cond_2
    iget v2, p1, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 975
    iget-wide v2, p1, Lcom/google/d/e/b;->d:J

    iget v4, p0, Lcom/google/d/e/d;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/e/d;->a:I

    iput-wide v2, p0, Lcom/google/d/e/d;->d:J

    .line 977
    :cond_3
    iget v2, p1, Lcom/google/d/e/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_4

    .line 978
    iget-wide v0, p1, Lcom/google/d/e/b;->e:J

    iget v2, p0, Lcom/google/d/e/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/e/d;->a:I

    iput-wide v0, p0, Lcom/google/d/e/d;->e:J

    .line 980
    :cond_4
    iget-object v0, p1, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 981
    iget-object v0, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 982
    iget-object v0, p1, Lcom/google/d/e/b;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    .line 983
    iget v0, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/d/e/d;->a:I

    .line 990
    :cond_5
    :goto_5
    iget-object v0, p1, Lcom/google/d/e/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 966
    goto :goto_1

    :cond_7
    move v2, v1

    .line 971
    goto :goto_2

    :cond_8
    move v2, v1

    .line 974
    goto :goto_3

    :cond_9
    move v0, v1

    .line 977
    goto :goto_4

    .line 985
    :cond_a
    iget v0, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/d/e/d;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/e/d;->a:I

    .line 986
    :cond_b
    iget-object v0, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/e/b;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 906
    new-instance v2, Lcom/google/d/e/b;

    invoke-direct {v2, p0}, Lcom/google/d/e/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/e/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/d/e/d;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/e/b;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/d/e/d;->c:J

    iput-wide v4, v2, Lcom/google/d/e/b;->c:J

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/d/e/d;->d:J

    iput-wide v4, v2, Lcom/google/d/e/b;->d:J

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/d/e/d;->e:J

    iput-wide v4, v2, Lcom/google/d/e/b;->e:J

    iget v1, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/d/e/d;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/d/e/d;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/e/b;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/d/e/b;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 906
    check-cast p1, Lcom/google/d/e/b;

    invoke-virtual {p0, p1}, Lcom/google/d/e/d;->a(Lcom/google/d/e/b;)Lcom/google/d/e/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 995
    iget v2, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 1003
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 995
    goto :goto_0

    .line 999
    :cond_2
    iget v2, p0, Lcom/google/d/e/d;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 1003
    goto :goto_1

    :cond_3
    move v2, v0

    .line 999
    goto :goto_2
.end method
