.class public final Lcom/google/d/b/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/b/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/b/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/d/b/a/b;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:J

.field e:J

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/google/d/b/a/c;

    invoke-direct {v0}, Lcom/google/d/b/a/c;-><init>()V

    sput-object v0, Lcom/google/d/b/a/b;->PARSER:Lcom/google/n/ax;

    .line 436
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/b/a/b;->k:Lcom/google/n/aw;

    .line 971
    new-instance v0, Lcom/google/d/b/a/b;

    invoke-direct {v0}, Lcom/google/d/b/a/b;-><init>()V

    sput-object v0, Lcom/google/d/b/a/b;->h:Lcom/google/d/b/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    .line 359
    iput-byte v2, p0, Lcom/google/d/b/a/b;->i:B

    .line 399
    iput v2, p0, Lcom/google/d/b/a/b;->j:I

    .line 96
    iget-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    .line 98
    iput-wide v4, p0, Lcom/google/d/b/a/b;->d:J

    .line 99
    iput-wide v4, p0, Lcom/google/d/b/a/b;->e:J

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    .line 102
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Lcom/google/d/b/a/b;-><init>()V

    .line 109
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 114
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 115
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 116
    sparse-switch v3, :sswitch_data_0

    .line 121
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 123
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 119
    goto :goto_0

    .line 128
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 129
    iget v3, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/b/a/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/b/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 133
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 134
    iget v4, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/b/a/b;->a:I

    .line 135
    iput-object v3, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 164
    :catch_1
    move-exception v0

    .line 165
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 166
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/b/a/b;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/b/a/b;->d:J

    goto :goto_0

    .line 144
    :sswitch_4
    iget v3, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/b/a/b;->a:I

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/b/a/b;->e:J

    goto :goto_0

    .line 149
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 150
    iget v4, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/b/a/b;->a:I

    .line 151
    iput-object v3, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    goto :goto_0

    .line 155
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 156
    iget v4, p0, Lcom/google/d/b/a/b;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/d/b/a/b;->a:I

    .line 157
    iput-object v3, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->au:Lcom/google/n/bn;

    .line 169
    return-void

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 93
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    .line 359
    iput-byte v1, p0, Lcom/google/d/b/a/b;->i:B

    .line 399
    iput v1, p0, Lcom/google/d/b/a/b;->j:I

    .line 94
    return-void
.end method

.method public static d()Lcom/google/d/b/a/b;
    .locals 1

    .prologue
    .line 974
    sget-object v0, Lcom/google/d/b/a/b;->h:Lcom/google/d/b/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/b/a/d;
    .locals 1

    .prologue
    .line 498
    new-instance v0, Lcom/google/d/b/a/d;

    invoke-direct {v0}, Lcom/google/d/b/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/b/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/google/d/b/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 377
    invoke-virtual {p0}, Lcom/google/d/b/a/b;->c()I

    .line 378
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 381
    :cond_0
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 382
    iget-object v0, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 384
    :cond_1
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 385
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/d/b/a/b;->d:J

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 387
    :cond_2
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 388
    iget-wide v0, p0, Lcom/google/d/b/a/b;->e:J

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 390
    :cond_3
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 391
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 393
    :cond_4
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 394
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 396
    :cond_5
    iget-object v0, p0, Lcom/google/d/b/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 397
    return-void

    .line 382
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 391
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 394
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 361
    iget-byte v0, p0, Lcom/google/d/b/a/b;->i:B

    .line 362
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 372
    :goto_0
    return v0

    .line 363
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 365
    :cond_1
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 366
    iget-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 367
    iput-byte v2, p0, Lcom/google/d/b/a/b;->i:B

    move v0, v2

    .line 368
    goto :goto_0

    :cond_2
    move v0, v2

    .line 365
    goto :goto_1

    .line 371
    :cond_3
    iput-byte v1, p0, Lcom/google/d/b/a/b;->i:B

    move v0, v1

    .line 372
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 401
    iget v0, p0, Lcom/google/d/b/a/b;->j:I

    .line 402
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 431
    :goto_0
    return v0

    .line 405
    :cond_0
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 406
    iget-object v0, p0, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    .line 407
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 409
    :goto_1
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 413
    :cond_1
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 414
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/d/b/a/b;->d:J

    .line 415
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 417
    :cond_2
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 418
    iget-wide v4, p0, Lcom/google/d/b/a/b;->e:J

    .line 419
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 421
    :cond_3
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 422
    const/4 v3, 0x5

    .line 423
    iget-object v0, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 425
    :cond_4
    iget v0, p0, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 426
    const/4 v3, 0x6

    .line 427
    iget-object v0, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 429
    :cond_5
    iget-object v0, p0, Lcom/google/d/b/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 430
    iput v0, p0, Lcom/google/d/b/a/b;->j:I

    goto/16 :goto_0

    .line 411
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 423
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 427
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/google/d/b/a/b;->newBuilder()Lcom/google/d/b/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/b/a/d;->a(Lcom/google/d/b/a/b;)Lcom/google/d/b/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/google/d/b/a/b;->newBuilder()Lcom/google/d/b/a/d;

    move-result-object v0

    return-object v0
.end method
