.class public final Lcom/google/d/b/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/b/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/b/a/b;",
        "Lcom/google/d/b/a/d;",
        ">;",
        "Lcom/google/d/b/a/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:J

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 516
    sget-object v0, Lcom/google/d/b/a/b;->h:Lcom/google/d/b/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 616
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/b/a/d;->b:Lcom/google/n/ao;

    .line 675
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/d;->c:Ljava/lang/Object;

    .line 815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/d;->f:Ljava/lang/Object;

    .line 891
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/b/a/d;->g:Ljava/lang/Object;

    .line 517
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/b/a/b;)Lcom/google/d/b/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 574
    invoke-static {}, Lcom/google/d/b/a/b;->d()Lcom/google/d/b/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 601
    :goto_0
    return-object p0

    .line 575
    :cond_0
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 576
    iget-object v2, p0, Lcom/google/d/b/a/d;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 577
    iget v2, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/b/a/d;->a:I

    .line 579
    :cond_1
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 580
    iget v2, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/b/a/d;->a:I

    .line 581
    iget-object v2, p1, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/b/a/d;->c:Ljava/lang/Object;

    .line 584
    :cond_2
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 585
    iget-wide v2, p1, Lcom/google/d/b/a/b;->d:J

    iget v4, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/b/a/d;->a:I

    iput-wide v2, p0, Lcom/google/d/b/a/d;->d:J

    .line 587
    :cond_3
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 588
    iget-wide v2, p1, Lcom/google/d/b/a/b;->e:J

    iget v4, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/b/a/d;->a:I

    iput-wide v2, p0, Lcom/google/d/b/a/d;->e:J

    .line 590
    :cond_4
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 591
    iget v2, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/b/a/d;->a:I

    .line 592
    iget-object v2, p1, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/b/a/d;->f:Ljava/lang/Object;

    .line 595
    :cond_5
    iget v2, p1, Lcom/google/d/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 596
    iget v0, p0, Lcom/google/d/b/a/d;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/b/a/d;->a:I

    .line 597
    iget-object v0, p1, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/d/b/a/d;->g:Ljava/lang/Object;

    .line 600
    :cond_6
    iget-object v0, p1, Lcom/google/d/b/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 575
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 579
    goto :goto_2

    :cond_9
    move v2, v1

    .line 584
    goto :goto_3

    :cond_a
    move v2, v1

    .line 587
    goto :goto_4

    :cond_b
    move v2, v1

    .line 590
    goto :goto_5

    :cond_c
    move v0, v1

    .line 595
    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 508
    new-instance v2, Lcom/google/d/b/a/b;

    invoke-direct {v2, p0}, Lcom/google/d/b/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/b/a/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/d/b/a/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/b/a/d;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/b/a/d;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/d/b/a/d;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/b/a/b;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/d/b/a/d;->d:J

    iput-wide v4, v2, Lcom/google/d/b/a/b;->d:J

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/d/b/a/d;->e:J

    iput-wide v4, v2, Lcom/google/d/b/a/b;->e:J

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/d/b/a/d;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/b/a/b;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/d/b/a/d;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/b/a/b;->g:Ljava/lang/Object;

    iput v0, v2, Lcom/google/d/b/a/b;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 508
    check-cast p1, Lcom/google/d/b/a/b;

    invoke-virtual {p0, p1}, Lcom/google/d/b/a/d;->a(Lcom/google/d/b/a/b;)Lcom/google/d/b/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 605
    iget v0, p0, Lcom/google/d/b/a/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/google/d/b/a/d;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 611
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 605
    goto :goto_0

    :cond_1
    move v0, v2

    .line 611
    goto :goto_1
.end method
