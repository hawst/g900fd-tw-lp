.class public final Lcom/google/d/c/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/c/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/c/a/b;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Ljava/lang/Object;

.field e:Z

.field f:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/d/c/a/c;

    invoke-direct {v0}, Lcom/google/d/c/a/c;-><init>()V

    sput-object v0, Lcom/google/d/c/a/b;->PARSER:Lcom/google/n/ax;

    .line 340
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/c/a/b;->j:Lcom/google/n/aw;

    .line 746
    new-instance v0, Lcom/google/d/c/a/b;

    invoke-direct {v0}, Lcom/google/d/c/a/b;-><init>()V

    sput-object v0, Lcom/google/d/c/a/b;->g:Lcom/google/d/c/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    .line 270
    iput-byte v3, p0, Lcom/google/d/c/a/b;->h:B

    .line 307
    iput v3, p0, Lcom/google/d/c/a/b;->i:I

    .line 77
    iget-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 78
    iput v2, p0, Lcom/google/d/c/a/b;->c:I

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    .line 80
    iput-boolean v2, p0, Lcom/google/d/c/a/b;->e:Z

    .line 81
    iput-boolean v2, p0, Lcom/google/d/c/a/b;->f:Z

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Lcom/google/d/c/a/b;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 94
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 101
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 103
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 109
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/c/a/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/c/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 113
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 114
    invoke-static {v0}, Lcom/google/d/a/a/bq;->a(I)Lcom/google/d/a/a/bq;

    move-result-object v5

    .line 115
    if-nez v5, :cond_1

    .line 116
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 145
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :cond_1
    :try_start_4
    iget v5, p0, Lcom/google/d/c/a/b;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/c/a/b;->a:I

    .line 119
    iput v0, p0, Lcom/google/d/c/a/b;->c:I

    goto :goto_0

    .line 124
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 125
    iget v5, p0, Lcom/google/d/c/a/b;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/d/c/a/b;->a:I

    .line 126
    iput-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    goto :goto_0

    .line 130
    :sswitch_4
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/c/a/b;->a:I

    .line 131
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/c/a/b;->e:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 135
    :sswitch_5
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/c/a/b;->a:I

    .line 136
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/c/a/b;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 147
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/c/a/b;->au:Lcom/google/n/bn;

    .line 148
    return-void

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 74
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    .line 270
    iput-byte v1, p0, Lcom/google/d/c/a/b;->h:B

    .line 307
    iput v1, p0, Lcom/google/d/c/a/b;->i:I

    .line 75
    return-void
.end method

.method public static d()Lcom/google/d/c/a/b;
    .locals 1

    .prologue
    .line 749
    sget-object v0, Lcom/google/d/c/a/b;->g:Lcom/google/d/c/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/c/a/d;
    .locals 1

    .prologue
    .line 402
    new-instance v0, Lcom/google/d/c/a/d;

    invoke-direct {v0}, Lcom/google/d/c/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/c/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    sget-object v0, Lcom/google/d/c/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 288
    invoke-virtual {p0}, Lcom/google/d/c/a/b;->c()I

    .line 289
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 292
    :cond_0
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 293
    iget v0, p0, Lcom/google/d/c/a/b;->c:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 295
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 296
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 298
    :cond_2
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 299
    iget-boolean v0, p0, Lcom/google/d/c/a/b;->e:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 301
    :cond_3
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 302
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/d/c/a/b;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_8

    :goto_3
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 304
    :cond_4
    iget-object v0, p0, Lcom/google/d/c/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 305
    return-void

    .line 293
    :cond_5
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 296
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_7
    move v0, v2

    .line 299
    goto :goto_2

    :cond_8
    move v1, v2

    .line 302
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 272
    iget-byte v0, p0, Lcom/google/d/c/a/b;->h:B

    .line 273
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 283
    :goto_0
    return v0

    .line 274
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 276
    :cond_1
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 277
    iget-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/d/b;->d()Lcom/google/d/d/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/d/b;

    invoke-virtual {v0}, Lcom/google/d/d/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 278
    iput-byte v2, p0, Lcom/google/d/c/a/b;->h:B

    move v0, v2

    .line 279
    goto :goto_0

    :cond_2
    move v0, v2

    .line 276
    goto :goto_1

    .line 282
    :cond_3
    iput-byte v1, p0, Lcom/google/d/c/a/b;->h:B

    move v0, v1

    .line 283
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 309
    iget v0, p0, Lcom/google/d/c/a/b;->i:I

    .line 310
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 335
    :goto_0
    return v0

    .line 313
    :cond_0
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 314
    iget-object v0, p0, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    .line 315
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 317
    :goto_1
    iget v2, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    .line 318
    iget v2, p0, Lcom/google/d/c/a/b;->c:I

    .line 319
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 321
    :goto_3
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 322
    const/4 v3, 0x3

    .line 323
    iget-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 325
    :cond_1
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 326
    iget-boolean v0, p0, Lcom/google/d/c/a/b;->e:Z

    .line 327
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 329
    :cond_2
    iget v0, p0, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 330
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/d/c/a/b;->f:Z

    .line 331
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 333
    :cond_3
    iget-object v0, p0, Lcom/google/d/c/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 334
    iput v0, p0, Lcom/google/d/c/a/b;->i:I

    goto/16 :goto_0

    .line 319
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    .line 323
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/c/a/b;->newBuilder()Lcom/google/d/c/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/c/a/d;->a(Lcom/google/d/c/a/b;)Lcom/google/d/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/c/a/b;->newBuilder()Lcom/google/d/c/a/d;

    move-result-object v0

    return-object v0
.end method
