.class public final Lcom/google/d/c/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/c/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/c/a/b;",
        "Lcom/google/d/c/a/d;",
        ">;",
        "Lcom/google/d/c/a/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 420
    sget-object v0, Lcom/google/d/c/a/b;->g:Lcom/google/d/c/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 507
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/c/a/d;->b:Lcom/google/n/ao;

    .line 566
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/c/a/d;->c:I

    .line 602
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/c/a/d;->d:Ljava/lang/Object;

    .line 421
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/c/a/b;)Lcom/google/d/c/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 472
    invoke-static {}, Lcom/google/d/c/a/b;->d()Lcom/google/d/c/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 492
    :goto_0
    return-object p0

    .line 473
    :cond_0
    iget v2, p1, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 474
    iget-object v2, p0, Lcom/google/d/c/a/d;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 475
    iget v2, p0, Lcom/google/d/c/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/c/a/d;->a:I

    .line 477
    :cond_1
    iget v2, p1, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 478
    iget v2, p1, Lcom/google/d/c/a/b;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/bq;->a(I)Lcom/google/d/a/a/bq;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/bq;->a:Lcom/google/d/a/a/bq;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 473
    goto :goto_1

    :cond_4
    move v2, v1

    .line 477
    goto :goto_2

    .line 478
    :cond_5
    iget v3, p0, Lcom/google/d/c/a/d;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/c/a/d;->a:I

    iget v2, v2, Lcom/google/d/a/a/bq;->hy:I

    iput v2, p0, Lcom/google/d/c/a/d;->c:I

    .line 480
    :cond_6
    iget v2, p1, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 481
    iget v2, p0, Lcom/google/d/c/a/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/c/a/d;->a:I

    .line 482
    iget-object v2, p1, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/c/a/d;->d:Ljava/lang/Object;

    .line 485
    :cond_7
    iget v2, p1, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 486
    iget-boolean v2, p1, Lcom/google/d/c/a/b;->e:Z

    iget v3, p0, Lcom/google/d/c/a/d;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/c/a/d;->a:I

    iput-boolean v2, p0, Lcom/google/d/c/a/d;->e:Z

    .line 488
    :cond_8
    iget v2, p1, Lcom/google/d/c/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    :goto_5
    if-eqz v0, :cond_9

    .line 489
    iget-boolean v0, p1, Lcom/google/d/c/a/b;->f:Z

    iget v1, p0, Lcom/google/d/c/a/d;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/d/c/a/d;->a:I

    iput-boolean v0, p0, Lcom/google/d/c/a/d;->f:Z

    .line 491
    :cond_9
    iget-object v0, p1, Lcom/google/d/c/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 480
    goto :goto_3

    :cond_b
    move v2, v1

    .line 485
    goto :goto_4

    :cond_c
    move v0, v1

    .line 488
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 412
    new-instance v2, Lcom/google/d/c/a/b;

    invoke-direct {v2, p0}, Lcom/google/d/c/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/c/a/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/d/c/a/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/c/a/d;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/c/a/d;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/c/a/d;->c:I

    iput v1, v2, Lcom/google/d/c/a/b;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/d/c/a/d;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/c/a/b;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/d/c/a/d;->e:Z

    iput-boolean v1, v2, Lcom/google/d/c/a/b;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/d/c/a/d;->f:Z

    iput-boolean v1, v2, Lcom/google/d/c/a/b;->f:Z

    iput v0, v2, Lcom/google/d/c/a/b;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 412
    check-cast p1, Lcom/google/d/c/a/b;

    invoke-virtual {p0, p1}, Lcom/google/d/c/a/d;->a(Lcom/google/d/c/a/b;)Lcom/google/d/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 496
    iget v0, p0, Lcom/google/d/c/a/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/google/d/c/a/d;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/d/b;->d()Lcom/google/d/d/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/d/b;

    invoke-virtual {v0}, Lcom/google/d/d/b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 502
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 496
    goto :goto_0

    :cond_1
    move v0, v2

    .line 502
    goto :goto_1
.end method
