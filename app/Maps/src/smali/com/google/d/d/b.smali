.class public final Lcom/google/d/d/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/d/g;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/d/b;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/d/b;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/n/ao;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/google/d/d/c;

    invoke-direct {v0}, Lcom/google/d/d/c;-><init>()V

    sput-object v0, Lcom/google/d/d/b;->PARSER:Lcom/google/n/ax;

    .line 402
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/d/b;->i:Lcom/google/n/aw;

    .line 880
    new-instance v0, Lcom/google/d/d/b;

    invoke-direct {v0}, Lcom/google/d/d/b;-><init>()V

    sput-object v0, Lcom/google/d/d/b;->f:Lcom/google/d/d/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    .line 339
    iput-byte v2, p0, Lcom/google/d/d/b;->g:B

    .line 373
    iput v2, p0, Lcom/google/d/d/b;->h:I

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/d/b;->c:I

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    .line 76
    iget-object v0, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 77
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lcom/google/d/d/b;-><init>()V

    .line 86
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 89
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 90
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 91
    sparse-switch v4, :sswitch_data_0

    .line 96
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 98
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 94
    goto :goto_0

    .line 103
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 104
    iget v5, p0, Lcom/google/d/d/b;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/d/b;->a:I

    .line 105
    iput-object v4, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1

    .line 143
    iget-object v1, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    .line 145
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/d/b;->au:Lcom/google/n/bn;

    throw v0

    .line 109
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 110
    invoke-static {v4}, Lcom/google/d/d/e;->a(I)Lcom/google/d/d/e;

    move-result-object v5

    .line 111
    if-nez v5, :cond_2

    .line 112
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 138
    :catch_1
    move-exception v0

    .line 139
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 140
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 114
    :cond_2
    :try_start_4
    iget v5, p0, Lcom/google/d/d/b;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/d/b;->a:I

    .line 115
    iput v4, p0, Lcom/google/d/d/b;->c:I

    goto :goto_0

    .line 120
    :sswitch_3
    and-int/lit8 v4, v1, 0x4

    if-eq v4, v7, :cond_3

    .line 121
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    .line 123
    or-int/lit8 v1, v1, 0x4

    .line 125
    :cond_3
    iget-object v4, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 126
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 125
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :sswitch_4
    iget-object v4, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 131
    iget v4, p0, Lcom/google/d/d/b;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/d/b;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 142
    :cond_4
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_5

    .line 143
    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    .line 145
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/b;->au:Lcom/google/n/bn;

    .line 146
    return-void

    .line 91
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0xfa2 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 70
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    .line 339
    iput-byte v1, p0, Lcom/google/d/d/b;->g:B

    .line 373
    iput v1, p0, Lcom/google/d/d/b;->h:I

    .line 71
    return-void
.end method

.method public static d()Lcom/google/d/d/b;
    .locals 1

    .prologue
    .line 883
    sget-object v0, Lcom/google/d/d/b;->f:Lcom/google/d/d/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/d/d;
    .locals 1

    .prologue
    .line 464
    new-instance v0, Lcom/google/d/d/d;

    invoke-direct {v0}, Lcom/google/d/d/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    sget-object v0, Lcom/google/d/d/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 357
    invoke-virtual {p0}, Lcom/google/d/d/b;->c()I

    .line 358
    iget v0, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 361
    :cond_0
    iget v0, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 362
    iget v0, p0, Lcom/google/d/d/b;->c:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 364
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 365
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 364
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 359
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 362
    :cond_3
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 367
    :cond_4
    iget v0, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 368
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 370
    :cond_5
    iget-object v0, p0, Lcom/google/d/d/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 371
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 341
    iget-byte v0, p0, Lcom/google/d/d/b;->g:B

    .line 342
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 352
    :cond_0
    :goto_0
    return v2

    .line 343
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 345
    :goto_1
    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 346
    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fz;->d()Lcom/google/d/a/a/fz;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fz;

    invoke-virtual {v0}, Lcom/google/d/a/a/fz;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 347
    iput-byte v2, p0, Lcom/google/d/d/b;->g:B

    goto :goto_0

    .line 345
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 351
    :cond_3
    iput-byte v3, p0, Lcom/google/d/d/b;->g:B

    move v2, v3

    .line 352
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 375
    iget v0, p0, Lcom/google/d/d/b;->h:I

    .line 376
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 397
    :goto_0
    return v0

    .line 379
    :cond_0
    iget v0, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 381
    iget-object v0, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 383
    :goto_2
    iget v2, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 384
    iget v2, p0, Lcom/google/d/d/b;->c:I

    .line 385
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 387
    :goto_4
    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 388
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/d/b;->d:Ljava/util/List;

    .line 389
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 387
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 381
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 385
    :cond_3
    const/16 v2, 0xa

    goto :goto_3

    .line 391
    :cond_4
    iget v0, p0, Lcom/google/d/d/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_5

    .line 392
    const/16 v0, 0x1f4

    iget-object v2, p0, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    .line 393
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 395
    :cond_5
    iget-object v0, p0, Lcom/google/d/d/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 396
    iput v0, p0, Lcom/google/d/d/b;->h:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/d/d/b;->newBuilder()Lcom/google/d/d/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/d/d;->a(Lcom/google/d/d/b;)Lcom/google/d/d/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/d/d/b;->newBuilder()Lcom/google/d/d/d;

    move-result-object v0

    return-object v0
.end method
