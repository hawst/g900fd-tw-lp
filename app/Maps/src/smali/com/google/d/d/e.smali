.class public final enum Lcom/google/d/d/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/d/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/d/e;

.field public static final enum b:Lcom/google/d/d/e;

.field private static final synthetic d:[Lcom/google/d/d/e;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 171
    new-instance v0, Lcom/google/d/d/e;

    const-string v1, "NON_PRIMARY"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/d/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/d/e;->a:Lcom/google/d/d/e;

    .line 175
    new-instance v0, Lcom/google/d/d/e;

    const-string v1, "PRIMARY"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/d/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/d/e;->b:Lcom/google/d/d/e;

    .line 166
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/d/d/e;

    sget-object v1, Lcom/google/d/d/e;->a:Lcom/google/d/d/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/d/e;->b:Lcom/google/d/d/e;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/d/e;->d:[Lcom/google/d/d/e;

    .line 205
    new-instance v0, Lcom/google/d/d/f;

    invoke-direct {v0}, Lcom/google/d/d/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 215
    iput p3, p0, Lcom/google/d/d/e;->c:I

    .line 216
    return-void
.end method

.method public static a(I)Lcom/google/d/d/e;
    .locals 1

    .prologue
    .line 193
    sparse-switch p0, :sswitch_data_0

    .line 196
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 194
    :sswitch_0
    sget-object v0, Lcom/google/d/d/e;->a:Lcom/google/d/d/e;

    goto :goto_0

    .line 195
    :sswitch_1
    sget-object v0, Lcom/google/d/d/e;->b:Lcom/google/d/d/e;

    goto :goto_0

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/d/e;
    .locals 1

    .prologue
    .line 166
    const-class v0, Lcom/google/d/d/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/d/e;

    return-object v0
.end method

.method public static values()[Lcom/google/d/d/e;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/d/d/e;->d:[Lcom/google/d/d/e;

    invoke-virtual {v0}, [Lcom/google/d/d/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/d/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/d/d/e;->c:I

    return v0
.end method
