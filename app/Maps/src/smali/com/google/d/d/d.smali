.class public final Lcom/google/d/d/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/d/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/d/b;",
        "Lcom/google/d/d/d;",
        ">;",
        "Lcom/google/d/d/g;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 482
    sget-object v0, Lcom/google/d/d/b;->f:Lcom/google/d/d/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 568
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/d/d;->b:Ljava/lang/Object;

    .line 644
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/d/d;->c:I

    .line 681
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    .line 817
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/d/d;->e:Lcom/google/n/ao;

    .line 483
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/d/b;)Lcom/google/d/d/d;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 529
    invoke-static {}, Lcom/google/d/d/b;->d()Lcom/google/d/d/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 553
    :goto_0
    return-object p0

    .line 530
    :cond_0
    iget v2, p1, Lcom/google/d/d/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 531
    iget v2, p0, Lcom/google/d/d/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/d/d;->a:I

    .line 532
    iget-object v2, p1, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/d/d;->b:Ljava/lang/Object;

    .line 535
    :cond_1
    iget v2, p1, Lcom/google/d/d/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 536
    iget v2, p1, Lcom/google/d/d/b;->c:I

    invoke-static {v2}, Lcom/google/d/d/e;->a(I)Lcom/google/d/d/e;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/d/e;->a:Lcom/google/d/d/e;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 530
    goto :goto_1

    :cond_4
    move v2, v1

    .line 535
    goto :goto_2

    .line 536
    :cond_5
    iget v3, p0, Lcom/google/d/d/d;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/d/d;->a:I

    iget v2, v2, Lcom/google/d/d/e;->c:I

    iput v2, p0, Lcom/google/d/d/d;->c:I

    .line 538
    :cond_6
    iget-object v2, p1, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 539
    iget-object v2, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 540
    iget-object v2, p1, Lcom/google/d/d/b;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    .line 541
    iget v2, p0, Lcom/google/d/d/d;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/d/d;->a:I

    .line 548
    :cond_7
    :goto_3
    iget v2, p1, Lcom/google/d/d/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_b

    :goto_4
    if-eqz v0, :cond_8

    .line 549
    iget-object v0, p0, Lcom/google/d/d/d;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 550
    iget v0, p0, Lcom/google/d/d/d;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/d/d;->a:I

    .line 552
    :cond_8
    iget-object v0, p1, Lcom/google/d/d/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 543
    :cond_9
    iget v2, p0, Lcom/google/d/d/d;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/d/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/d/d;->a:I

    .line 544
    :cond_a
    iget-object v2, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/d/b;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_b
    move v0, v1

    .line 548
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 474
    new-instance v2, Lcom/google/d/d/b;

    invoke-direct {v2, p0}, Lcom/google/d/d/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/d/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, p0, Lcom/google/d/d/d;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/d/b;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/d/d;->c:I

    iput v4, v2, Lcom/google/d/d/b;->c:I

    iget v4, p0, Lcom/google/d/d/d;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/d/d/d;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/d/d/d;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/d/b;->d:Ljava/util/List;

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v3, v2, Lcom/google/d/d/b;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/d/d;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/d/d;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/d/b;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 474
    check-cast p1, Lcom/google/d/d/b;

    invoke-virtual {p0, p1}, Lcom/google/d/d/d;->a(Lcom/google/d/d/b;)Lcom/google/d/d/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 557
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/google/d/d/d;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fz;->d()Lcom/google/d/a/a/fz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fz;

    invoke-virtual {v0}, Lcom/google/d/a/a/fz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    :goto_1
    return v2

    .line 557
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 563
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
