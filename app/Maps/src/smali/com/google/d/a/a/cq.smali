.class public final Lcom/google/d/a/a/cq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/cv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cq;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/cq;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/lang/Object;

.field f:Z

.field g:Lcom/google/n/ao;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lcom/google/d/a/a/cr;

    invoke-direct {v0}, Lcom/google/d/a/a/cr;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cq;->PARSER:Lcom/google/n/ax;

    .line 3946
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/cq;->n:Lcom/google/n/aw;

    .line 4844
    new-instance v0, Lcom/google/d/a/a/cq;

    invoke-direct {v0}, Lcom/google/d/a/a/cq;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cq;->k:Lcom/google/d/a/a/cq;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 122
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3630
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    .line 3746
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    .line 3805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    .line 3821
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    .line 3836
    iput-byte v3, p0, Lcom/google/d/a/a/cq;->l:B

    .line 3897
    iput v3, p0, Lcom/google/d/a/a/cq;->m:I

    .line 123
    const/16 v0, 0xf1

    iput v0, p0, Lcom/google/d/a/a/cq;->b:I

    .line 124
    iget-object v0, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 125
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/d/a/a/cq;->f:Z

    .line 128
    iget-object v0, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    .line 130
    iget-object v0, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 131
    iget-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 132
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x40

    const/4 v10, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 138
    invoke-direct {p0}, Lcom/google/d/a/a/cq;-><init>()V

    .line 141
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 144
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 145
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 146
    sparse-switch v0, :sswitch_data_0

    .line 151
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 153
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 149
    goto :goto_0

    .line 158
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 159
    invoke-static {v0}, Lcom/google/d/a/a/ct;->a(I)Lcom/google/d/a/a/ct;

    move-result-object v6

    .line 160
    if-nez v6, :cond_3

    .line 161
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x4

    if-ne v2, v10, :cond_1

    .line 228
    iget-object v2, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    .line 230
    :cond_1
    and-int/lit8 v1, v1, 0x40

    if-ne v1, v11, :cond_2

    .line 231
    iget-object v1, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    .line 233
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/cq;->au:Lcom/google/n/bn;

    throw v0

    .line 163
    :cond_3
    :try_start_2
    iget v6, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/d/a/a/cq;->a:I

    .line 164
    iput v0, p0, Lcom/google/d/a/a/cq;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 223
    :catch_1
    move-exception v0

    .line 224
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 225
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 169
    :sswitch_2
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v10, :cond_4

    .line 170
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    .line 172
    or-int/lit8 v1, v1, 0x4

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 175
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 174
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 180
    iget v6, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/d/a/a/cq;->a:I

    .line 181
    iput-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 185
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/cq;->a:I

    .line 186
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/cq;->f:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 190
    :sswitch_5
    iget-object v0, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 191
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/cq;->a:I

    goto/16 :goto_0

    .line 195
    :sswitch_6
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v11, :cond_6

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    .line 198
    or-int/lit8 v1, v1, 0x40

    .line 200
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 200
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 205
    :sswitch_7
    iget-object v0, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 206
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/cq;->a:I

    goto/16 :goto_0

    .line 210
    :sswitch_8
    iget-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 211
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/cq;->a:I

    goto/16 :goto_0

    .line 215
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 216
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/cq;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 227
    :cond_7
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v10, :cond_8

    .line 228
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    .line 230
    :cond_8
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v11, :cond_9

    .line 231
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    .line 233
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->au:Lcom/google/n/bn;

    .line 234
    return-void

    .line 146
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x30 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0xfa2 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 120
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3630
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    .line 3746
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    .line 3805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    .line 3821
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    .line 3836
    iput-byte v1, p0, Lcom/google/d/a/a/cq;->l:B

    .line 3897
    iput v1, p0, Lcom/google/d/a/a/cq;->m:I

    .line 121
    return-void
.end method

.method public static d()Lcom/google/d/a/a/cq;
    .locals 1

    .prologue
    .line 4847
    sget-object v0, Lcom/google/d/a/a/cq;->k:Lcom/google/d/a/a/cq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/cs;
    .locals 1

    .prologue
    .line 4008
    new-instance v0, Lcom/google/d/a/a/cs;

    invoke-direct {v0}, Lcom/google/d/a/a/cs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    sget-object v0, Lcom/google/d/a/a/cq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 3866
    invoke-virtual {p0}, Lcom/google/d/a/a/cq;->c()I

    .line 3867
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3868
    iget v0, p0, Lcom/google/d/a/a/cq;->b:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_0
    :goto_0
    move v1, v2

    .line 3870
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3871
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3870
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3868
    :cond_1
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 3873
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 3874
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3876
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_4

    .line 3877
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/d/a/a/cq;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_7

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 3879
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 3880
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3882
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 3883
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3882
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 3874
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_7
    move v0, v2

    .line 3877
    goto :goto_3

    .line 3885
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 3886
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3888
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 3889
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3891
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_b

    .line 3892
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3894
    :cond_b
    iget-object v0, p0, Lcom/google/d/a/a/cq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3895
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3838
    iget-byte v0, p0, Lcom/google/d/a/a/cq;->l:B

    .line 3839
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3861
    :cond_0
    :goto_0
    return v2

    .line 3840
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 3842
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3843
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nn;->d()Lcom/google/d/a/a/nn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nn;

    invoke-virtual {v0}, Lcom/google/d/a/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3844
    iput-byte v2, p0, Lcom/google/d/a/a/cq;->l:B

    goto :goto_0

    .line 3842
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3848
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3849
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3850
    iput-byte v2, p0, Lcom/google/d/a/a/cq;->l:B

    goto :goto_0

    .line 3848
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3854
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 3855
    iget-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ij;->d()Lcom/google/d/a/a/ij;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ij;

    invoke-virtual {v0}, Lcom/google/d/a/a/ij;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3856
    iput-byte v2, p0, Lcom/google/d/a/a/cq;->l:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 3854
    goto :goto_3

    .line 3860
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/cq;->l:B

    move v2, v3

    .line 3861
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v1, 0xa

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 3899
    iget v0, p0, Lcom/google/d/a/a/cq;->m:I

    .line 3900
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 3941
    :goto_0
    return v0

    .line 3903
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_b

    .line 3904
    iget v0, p0, Lcom/google/d/a/a/cq;->b:I

    .line 3905
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v3, v2

    move v4, v0

    .line 3907
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 3908
    iget-object v0, p0, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    .line 3909
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3907
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 3905
    goto :goto_1

    .line 3911
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    .line 3912
    const/4 v3, 0x3

    .line 3913
    iget-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 3915
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_4

    .line 3916
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/cq;->f:Z

    .line 3917
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 3919
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_5

    .line 3920
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    .line 3921
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    :cond_5
    move v3, v2

    .line 3923
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 3924
    iget-object v0, p0, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    .line 3925
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3923
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 3913
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 3927
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    .line 3928
    const/16 v0, 0x9

    iget-object v3, p0, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    .line 3929
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 3931
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_9

    .line 3932
    iget-object v0, p0, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    .line 3933
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 3935
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_a

    .line 3936
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    .line 3937
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 3939
    :cond_a
    iget-object v0, p0, Lcom/google/d/a/a/cq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 3940
    iput v0, p0, Lcom/google/d/a/a/cq;->m:I

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/d/a/a/cq;->newBuilder()Lcom/google/d/a/a/cs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/cs;->a(Lcom/google/d/a/a/cq;)Lcom/google/d/a/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/d/a/a/cq;->newBuilder()Lcom/google/d/a/a/cs;

    move-result-object v0

    return-object v0
.end method
