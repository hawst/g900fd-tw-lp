.class public final enum Lcom/google/d/a/a/mz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field private static final synthetic A:[Lcom/google/d/a/a/mz;

.field public static final enum a:Lcom/google/d/a/a/mz;

.field public static final enum b:Lcom/google/d/a/a/mz;

.field public static final enum c:Lcom/google/d/a/a/mz;

.field public static final enum d:Lcom/google/d/a/a/mz;

.field public static final enum e:Lcom/google/d/a/a/mz;

.field public static final enum f:Lcom/google/d/a/a/mz;

.field public static final enum g:Lcom/google/d/a/a/mz;

.field public static final enum h:Lcom/google/d/a/a/mz;

.field public static final enum i:Lcom/google/d/a/a/mz;

.field public static final enum j:Lcom/google/d/a/a/mz;

.field public static final enum k:Lcom/google/d/a/a/mz;

.field public static final enum l:Lcom/google/d/a/a/mz;

.field public static final enum m:Lcom/google/d/a/a/mz;

.field public static final enum n:Lcom/google/d/a/a/mz;

.field public static final enum o:Lcom/google/d/a/a/mz;

.field public static final enum p:Lcom/google/d/a/a/mz;

.field public static final enum q:Lcom/google/d/a/a/mz;

.field public static final enum r:Lcom/google/d/a/a/mz;

.field public static final enum s:Lcom/google/d/a/a/mz;

.field public static final enum t:Lcom/google/d/a/a/mz;

.field public static final enum u:Lcom/google/d/a/a/mz;

.field public static final enum v:Lcom/google/d/a/a/mz;

.field public static final enum w:Lcom/google/d/a/a/mz;

.field public static final enum x:Lcom/google/d/a/a/mz;

.field public static final enum y:Lcom/google/d/a/a/mz;


# instance fields
.field final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x15

    const/16 v7, 0x14

    const/16 v6, 0x13

    const/16 v5, 0x12

    const/16 v4, 0x11

    .line 1502
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_RAMP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->a:Lcom/google/d/a/a/mz;

    .line 1506
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ON_RAMP"

    const/4 v2, 0x1

    const/16 v3, 0x111

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->b:Lcom/google/d/a/a/mz;

    .line 1510
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_OFF_RAMP"

    const/4 v2, 0x2

    const/16 v3, 0x112

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->c:Lcom/google/d/a/a/mz;

    .line 1514
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ON_OFF_RAMP"

    const/4 v2, 0x3

    const/16 v3, 0x113

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->d:Lcom/google/d/a/a/mz;

    .line 1518
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_INTERCHANGE"

    const/4 v2, 0x4

    const/16 v3, 0x114

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->e:Lcom/google/d/a/a/mz;

    .line 1522
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_TURN_SEGMENT"

    const/4 v2, 0x5

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->f:Lcom/google/d/a/a/mz;

    .line 1526
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_SPECIAL_TRAFFIC_FIGURE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v5}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->g:Lcom/google/d/a/a/mz;

    .line 1530
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ROUNDABOUT"

    const/4 v2, 0x7

    const/16 v3, 0x121

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->h:Lcom/google/d/a/a/mz;

    .line 1534
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ROUNDABOUT_BYPASS"

    const/16 v2, 0x8

    const/16 v3, 0x122

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->i:Lcom/google/d/a/a/mz;

    .line 1538
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ROUNDABOUT_INTERNAL_BYPASS"

    const/16 v2, 0x9

    const/16 v3, 0x1221

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->j:Lcom/google/d/a/a/mz;

    .line 1542
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ROUNDABOUT_EXTERNAL_BYPASS"

    const/16 v2, 0xa

    const/16 v3, 0x1222

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->k:Lcom/google/d/a/a/mz;

    .line 1546
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ENCLOSED_TRAFFIC_AREA"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v6}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->l:Lcom/google/d/a/a/mz;

    .line 1550
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_PEDESTRIAN_MALL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v7}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->m:Lcom/google/d/a/a/mz;

    .line 1554
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_MAJOR_PEDESTRIAN_MALL"

    const/16 v2, 0xd

    const/16 v3, 0x141

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->n:Lcom/google/d/a/a/mz;

    .line 1558
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_MINOR_PEDESTRIAN_MALL"

    const/16 v2, 0xe

    const/16 v3, 0x142

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->o:Lcom/google/d/a/a/mz;

    .line 1562
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_WALKWAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v8}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->p:Lcom/google/d/a/a/mz;

    .line 1566
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_TRAIL"

    const/16 v2, 0x10

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->q:Lcom/google/d/a/a/mz;

    .line 1570
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_STATION_PATH"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->r:Lcom/google/d/a/a/mz;

    .line 1574
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_ACCESS_PATH"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->s:Lcom/google/d/a/a/mz;

    .line 1578
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_CROSSING"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->t:Lcom/google/d/a/a/mz;

    .line 1582
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_MARKED_CROSSING"

    const/16 v2, 0x191

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->u:Lcom/google/d/a/a/mz;

    .line 1586
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_UNMARKED_CROSSING"

    const/16 v2, 0x192

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->v:Lcom/google/d/a/a/mz;

    .line 1590
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_OVERPASS"

    const/16 v2, 0x16

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->w:Lcom/google/d/a/a/mz;

    .line 1594
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_UNDERPASS"

    const/16 v2, 0x17

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->x:Lcom/google/d/a/a/mz;

    .line 1598
    new-instance v0, Lcom/google/d/a/a/mz;

    const-string v1, "USAGE_HALLWAY"

    const/16 v2, 0x18

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mz;->y:Lcom/google/d/a/a/mz;

    .line 1497
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/google/d/a/a/mz;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/mz;->a:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/d/a/a/mz;->b:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/d/a/a/mz;->c:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/d/a/a/mz;->d:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/d/a/a/mz;->e:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/mz;->f:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/mz;->g:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/mz;->h:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/mz;->i:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/mz;->j:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/mz;->k:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/mz;->l:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/mz;->m:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/mz;->n:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/mz;->o:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/mz;->p:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/d/a/a/mz;->q:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/mz;->r:Lcom/google/d/a/a/mz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mz;->s:Lcom/google/d/a/a/mz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mz;->t:Lcom/google/d/a/a/mz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mz;->u:Lcom/google/d/a/a/mz;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/mz;->v:Lcom/google/d/a/a/mz;

    aput-object v1, v0, v8

    const/16 v1, 0x16

    sget-object v2, Lcom/google/d/a/a/mz;->w:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/d/a/a/mz;->x:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/d/a/a/mz;->y:Lcom/google/d/a/a/mz;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/mz;->A:[Lcom/google/d/a/a/mz;

    .line 1743
    new-instance v0, Lcom/google/d/a/a/na;

    invoke-direct {v0}, Lcom/google/d/a/a/na;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1752
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1753
    iput p3, p0, Lcom/google/d/a/a/mz;->z:I

    .line 1754
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mz;
    .locals 1

    .prologue
    .line 1708
    sparse-switch p0, :sswitch_data_0

    .line 1734
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1709
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mz;->a:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1710
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mz;->b:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1711
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mz;->c:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1712
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mz;->d:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1713
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/mz;->e:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1714
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/mz;->f:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1715
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/mz;->g:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1716
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/mz;->h:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1717
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/mz;->i:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1718
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/mz;->j:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1719
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/mz;->k:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1720
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/mz;->l:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1721
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/mz;->m:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1722
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/mz;->n:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1723
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/mz;->o:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1724
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/mz;->p:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1725
    :sswitch_10
    sget-object v0, Lcom/google/d/a/a/mz;->q:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1726
    :sswitch_11
    sget-object v0, Lcom/google/d/a/a/mz;->r:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1727
    :sswitch_12
    sget-object v0, Lcom/google/d/a/a/mz;->s:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1728
    :sswitch_13
    sget-object v0, Lcom/google/d/a/a/mz;->t:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1729
    :sswitch_14
    sget-object v0, Lcom/google/d/a/a/mz;->u:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1730
    :sswitch_15
    sget-object v0, Lcom/google/d/a/a/mz;->v:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1731
    :sswitch_16
    sget-object v0, Lcom/google/d/a/a/mz;->w:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1732
    :sswitch_17
    sget-object v0, Lcom/google/d/a/a/mz;->x:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1733
    :sswitch_18
    sget-object v0, Lcom/google/d/a/a/mz;->y:Lcom/google/d/a/a/mz;

    goto :goto_0

    .line 1708
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_6
        0x13 -> :sswitch_b
        0x14 -> :sswitch_c
        0x15 -> :sswitch_f
        0x16 -> :sswitch_10
        0x17 -> :sswitch_11
        0x18 -> :sswitch_12
        0x19 -> :sswitch_13
        0x1a -> :sswitch_16
        0x1b -> :sswitch_17
        0x1c -> :sswitch_18
        0x1d -> :sswitch_5
        0x111 -> :sswitch_1
        0x112 -> :sswitch_2
        0x113 -> :sswitch_3
        0x114 -> :sswitch_4
        0x121 -> :sswitch_7
        0x122 -> :sswitch_8
        0x141 -> :sswitch_d
        0x142 -> :sswitch_e
        0x191 -> :sswitch_14
        0x192 -> :sswitch_15
        0x1221 -> :sswitch_9
        0x1222 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mz;
    .locals 1

    .prologue
    .line 1497
    const-class v0, Lcom/google/d/a/a/mz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mz;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mz;
    .locals 1

    .prologue
    .line 1497
    sget-object v0, Lcom/google/d/a/a/mz;->A:[Lcom/google/d/a/a/mz;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1704
    iget v0, p0, Lcom/google/d/a/a/mz;->z:I

    return v0
.end method
