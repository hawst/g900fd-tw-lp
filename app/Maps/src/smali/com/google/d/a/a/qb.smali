.class public final Lcom/google/d/a/a/qb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qb;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/qb;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/google/d/a/a/qc;

    invoke-direct {v0}, Lcom/google/d/a/a/qc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qb;->PARSER:Lcom/google/n/ax;

    .line 423
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/qb;->j:Lcom/google/n/aw;

    .line 942
    new-instance v0, Lcom/google/d/a/a/qb;

    invoke-direct {v0}, Lcom/google/d/a/a/qb;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qb;->g:Lcom/google/d/a/a/qb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 232
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    .line 337
    iput-byte v3, p0, Lcom/google/d/a/a/qb;->h:B

    .line 390
    iput v3, p0, Lcom/google/d/a/a/qb;->i:I

    .line 77
    iget-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 78
    iget-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 79
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    .line 80
    iput v2, p0, Lcom/google/d/a/a/qb;->e:I

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/d/a/a/qb;->f:Z

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v10, 0x4

    const/4 v3, 0x0

    .line 88
    invoke-direct {p0}, Lcom/google/d/a/a/qb;-><init>()V

    .line 91
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 94
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 101
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 103
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 109
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qb;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v10, :cond_1

    .line 152
    iget-object v1, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    .line 154
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qb;->au:Lcom/google/n/bn;

    throw v0

    .line 113
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 114
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/qb;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 147
    :catch_1
    move-exception v0

    .line 148
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 149
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v10, :cond_2

    .line 119
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    .line 121
    or-int/lit8 v1, v1, 0x4

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 123
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 129
    invoke-static {v0}, Lcom/google/d/a/a/qe;->a(I)Lcom/google/d/a/a/qe;

    move-result-object v6

    .line 130
    if-nez v6, :cond_3

    .line 131
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 133
    :cond_3
    iget v6, p0, Lcom/google/d/a/a/qb;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/d/a/a/qb;->a:I

    .line 134
    iput v0, p0, Lcom/google/d/a/a/qb;->e:I

    goto/16 :goto_0

    .line 139
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/qb;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/qb;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 151
    :cond_5
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v10, :cond_6

    .line 152
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    .line 154
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qb;->au:Lcom/google/n/bn;

    .line 155
    return-void

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 74
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 232
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    .line 337
    iput-byte v1, p0, Lcom/google/d/a/a/qb;->h:B

    .line 390
    iput v1, p0, Lcom/google/d/a/a/qb;->i:I

    .line 75
    return-void
.end method

.method public static d()Lcom/google/d/a/a/qb;
    .locals 1

    .prologue
    .line 945
    sget-object v0, Lcom/google/d/a/a/qb;->g:Lcom/google/d/a/a/qb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/qd;
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lcom/google/d/a/a/qd;

    invoke-direct {v0}, Lcom/google/d/a/a/qd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/google/d/a/a/qb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 371
    invoke-virtual {p0}, Lcom/google/d/a/a/qb;->c()I

    .line 372
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 375
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 376
    iget-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 378
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 379
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 378
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 381
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 382
    iget v0, p0, Lcom/google/d/a/a/qb;->e:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 384
    :cond_3
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 385
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/d/a/a/qb;->f:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_4

    move v2, v3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 387
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/qb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 388
    return-void

    .line 382
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 339
    iget-byte v0, p0, Lcom/google/d/a/a/qb;->h:B

    .line 340
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 366
    :cond_0
    :goto_0
    return v2

    .line 341
    :cond_1
    if-eqz v0, :cond_0

    .line 343
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 344
    iput-byte v2, p0, Lcom/google/d/a/a/qb;->h:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 343
    goto :goto_1

    .line 347
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_5

    .line 348
    iput-byte v2, p0, Lcom/google/d/a/a/qb;->h:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 347
    goto :goto_2

    .line 351
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 352
    iput-byte v2, p0, Lcom/google/d/a/a/qb;->h:B

    goto :goto_0

    .line 355
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 356
    iput-byte v2, p0, Lcom/google/d/a/a/qb;->h:B

    goto :goto_0

    :cond_7
    move v1, v2

    .line 359
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 360
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 361
    iput-byte v2, p0, Lcom/google/d/a/a/qb;->h:B

    goto :goto_0

    .line 359
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 365
    :cond_9
    iput-byte v3, p0, Lcom/google/d/a/a/qb;->h:B

    move v2, v3

    .line 366
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 392
    iget v0, p0, Lcom/google/d/a/a/qb;->i:I

    .line 393
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 418
    :goto_0
    return v0

    .line 396
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 397
    iget-object v0, p0, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    .line 398
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 400
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 401
    iget-object v2, p0, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    .line 402
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 404
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 405
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    .line 406
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 404
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 408
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 409
    iget v0, p0, Lcom/google/d/a/a/qb;->e:I

    .line 410
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 412
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 413
    const/4 v0, 0x5

    iget-boolean v2, p0, Lcom/google/d/a/a/qb;->f:Z

    .line 414
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 416
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/qb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 417
    iput v0, p0, Lcom/google/d/a/a/qb;->i:I

    goto/16 :goto_0

    .line 410
    :cond_5
    const/16 v0, 0xa

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/qb;->newBuilder()Lcom/google/d/a/a/qd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/qd;->a(Lcom/google/d/a/a/qb;)Lcom/google/d/a/a/qd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/qb;->newBuilder()Lcom/google/d/a/a/qd;

    move-result-object v0

    return-object v0
.end method
