.class public final Lcom/google/d/a/a/rp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/rn;",
        "Lcom/google/d/a/a/rp;",
        ">;",
        "Lcom/google/d/a/a/rq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:F

.field private e:Z

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:I

.field private k:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 604
    sget-object v0, Lcom/google/d/a/a/rn;->l:Lcom/google/d/a/a/rn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->b:Lcom/google/n/ao;

    .line 842
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->c:Lcom/google/n/ao;

    .line 965
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->f:Lcom/google/n/ao;

    .line 1024
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->g:Lcom/google/n/ao;

    .line 1083
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->h:Lcom/google/n/ao;

    .line 1142
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rp;->i:Lcom/google/n/ao;

    .line 605
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/rn;)Lcom/google/d/a/a/rp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 696
    invoke-static {}, Lcom/google/d/a/a/rn;->d()Lcom/google/d/a/a/rn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 734
    :goto_0
    return-object p0

    .line 697
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 698
    iget-object v2, p0, Lcom/google/d/a/a/rp;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 699
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 701
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 702
    iget-object v2, p0, Lcom/google/d/a/a/rp;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 703
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 705
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 706
    iget v2, p1, Lcom/google/d/a/a/rn;->d:F

    iget v3, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/rp;->a:I

    iput v2, p0, Lcom/google/d/a/a/rp;->d:F

    .line 708
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 709
    iget-boolean v2, p1, Lcom/google/d/a/a/rn;->e:Z

    iget v3, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/rp;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rp;->e:Z

    .line 711
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 712
    iget-object v2, p0, Lcom/google/d/a/a/rp;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 713
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 715
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 716
    iget-object v2, p0, Lcom/google/d/a/a/rp;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 717
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 719
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 720
    iget-object v2, p0, Lcom/google/d/a/a/rp;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 721
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 723
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 724
    iget-object v2, p0, Lcom/google/d/a/a/rp;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 725
    iget v2, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/rp;->a:I

    .line 727
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 728
    iget v2, p1, Lcom/google/d/a/a/rn;->j:I

    iget v3, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/rp;->a:I

    iput v2, p0, Lcom/google/d/a/a/rp;->j:I

    .line 730
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_a

    .line 731
    iget-boolean v0, p1, Lcom/google/d/a/a/rn;->k:Z

    iget v1, p0, Lcom/google/d/a/a/rp;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/d/a/a/rp;->a:I

    iput-boolean v0, p0, Lcom/google/d/a/a/rp;->k:Z

    .line 733
    :cond_a
    iget-object v0, p1, Lcom/google/d/a/a/rn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 697
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 701
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 705
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 708
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 711
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 715
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 719
    goto :goto_7

    :cond_12
    move v2, v1

    .line 723
    goto :goto_8

    :cond_13
    move v2, v1

    .line 727
    goto :goto_9

    :cond_14
    move v0, v1

    .line 730
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 596
    new-instance v2, Lcom/google/d/a/a/rn;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/rn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/rp;->d:F

    iput v4, v2, Lcom/google/d/a/a/rn;->d:F

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/d/a/a/rp;->e:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/rn;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rp;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rp;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/d/a/a/rp;->j:I

    iput v1, v2, Lcom/google/d/a/a/rn;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v1, p0, Lcom/google/d/a/a/rp;->k:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/rn;->k:Z

    iput v0, v2, Lcom/google/d/a/a/rn;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 596
    check-cast p1, Lcom/google/d/a/a/rn;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/rp;->a(Lcom/google/d/a/a/rn;)Lcom/google/d/a/a/rp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 738
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 778
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 738
    goto :goto_0

    .line 742
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 744
    goto :goto_1

    :cond_2
    move v0, v1

    .line 742
    goto :goto_2

    .line 746
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/rp;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 748
    goto :goto_1

    .line 750
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/rp;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 752
    goto :goto_1

    .line 754
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_3
    if-eqz v0, :cond_7

    .line 755
    iget-object v0, p0, Lcom/google/d/a/a/rp;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 757
    goto :goto_1

    :cond_6
    move v0, v1

    .line 754
    goto :goto_3

    .line 760
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_4
    if-eqz v0, :cond_9

    .line 761
    iget-object v0, p0, Lcom/google/d/a/a/rp;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 763
    goto :goto_1

    :cond_8
    move v0, v1

    .line 760
    goto :goto_4

    .line 766
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_a

    move v0, v2

    :goto_5
    if-eqz v0, :cond_b

    .line 767
    iget-object v0, p0, Lcom/google/d/a/a/rp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 769
    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 766
    goto :goto_5

    .line 772
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/rp;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_c

    move v0, v2

    :goto_6
    if-eqz v0, :cond_d

    .line 773
    iget-object v0, p0, Lcom/google/d/a/a/rp;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 775
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 772
    goto :goto_6

    :cond_d
    move v0, v2

    .line 778
    goto/16 :goto_1
.end method
