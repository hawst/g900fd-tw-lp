.class public final Lcom/google/d/a/a/ob;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/oc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/nz;",
        "Lcom/google/d/a/a/ob;",
        ">;",
        "Lcom/google/d/a/a/oc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/google/d/a/a/nz;->d:Lcom/google/d/a/a/nz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 343
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ob;->b:Lcom/google/n/ao;

    .line 402
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ob;->c:Lcom/google/n/ao;

    .line 277
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/nz;)Lcom/google/d/a/a/ob;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 312
    invoke-static {}, Lcom/google/d/a/a/nz;->d()Lcom/google/d/a/a/nz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 322
    :goto_0
    return-object p0

    .line 313
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/nz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 314
    iget-object v2, p0, Lcom/google/d/a/a/ob;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/nz;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 315
    iget v2, p0, Lcom/google/d/a/a/ob;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/ob;->a:I

    .line 317
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/nz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 318
    iget-object v0, p0, Lcom/google/d/a/a/ob;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/nz;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 319
    iget v0, p0, Lcom/google/d/a/a/ob;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/ob;->a:I

    .line 321
    :cond_2
    iget-object v0, p1, Lcom/google/d/a/a/nz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 313
    goto :goto_1

    :cond_4
    move v0, v1

    .line 317
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 268
    new-instance v2, Lcom/google/d/a/a/nz;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/nz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ob;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/nz;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ob;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ob;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/d/a/a/nz;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ob;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ob;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/nz;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 268
    check-cast p1, Lcom/google/d/a/a/nz;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ob;->a(Lcom/google/d/a/a/nz;)Lcom/google/d/a/a/ob;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 326
    iget v0, p0, Lcom/google/d/a/a/ob;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/google/d/a/a/ob;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 338
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 326
    goto :goto_0

    .line 332
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ob;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 333
    iget-object v0, p0, Lcom/google/d/a/a/ob;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 335
    goto :goto_1

    :cond_2
    move v0, v1

    .line 332
    goto :goto_2

    :cond_3
    move v0, v2

    .line 338
    goto :goto_1
.end method
