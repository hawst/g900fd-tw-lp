.class public final Lcom/google/d/a/a/fp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fp;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/fp;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/d/a/a/ds;

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 615
    new-instance v0, Lcom/google/d/a/a/fq;

    invoke-direct {v0}, Lcom/google/d/a/a/fq;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fp;->PARSER:Lcom/google/n/ax;

    .line 710
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/fp;->g:Lcom/google/n/aw;

    .line 942
    new-instance v0, Lcom/google/d/a/a/fp;

    invoke-direct {v0}, Lcom/google/d/a/a/fp;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fp;->d:Lcom/google/d/a/a/fp;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 559
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 661
    iput-byte v0, p0, Lcom/google/d/a/a/fp;->e:B

    .line 689
    iput v0, p0, Lcom/google/d/a/a/fp;->f:I

    .line 560
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/fp;->c:I

    .line 561
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 567
    invoke-direct {p0}, Lcom/google/d/a/a/fp;-><init>()V

    .line 568
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 572
    const/4 v0, 0x0

    move v2, v0

    .line 573
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 574
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 575
    sparse-switch v0, :sswitch_data_0

    .line 580
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 582
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 578
    goto :goto_0

    .line 587
    :sswitch_1
    const/4 v0, 0x0

    .line 588
    iget v1, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 589
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    move-object v1, v0

    .line 591
    :goto_1
    sget-object v0, Lcom/google/d/a/a/ds;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    iput-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    .line 592
    if-eqz v1, :cond_1

    .line 593
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    .line 594
    invoke-virtual {v1}, Lcom/google/d/a/a/du;->c()Lcom/google/d/a/a/ds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    .line 596
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/fp;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fp;->au:Lcom/google/n/bn;

    throw v0

    .line 600
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/fp;->a:I

    .line 601
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fp;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 608
    :catch_1
    move-exception v0

    .line 609
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 610
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 612
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fp;->au:Lcom/google/n/bn;

    .line 613
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 575
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 557
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 661
    iput-byte v0, p0, Lcom/google/d/a/a/fp;->e:B

    .line 689
    iput v0, p0, Lcom/google/d/a/a/fp;->f:I

    .line 558
    return-void
.end method

.method public static d()Lcom/google/d/a/a/fp;
    .locals 1

    .prologue
    .line 945
    sget-object v0, Lcom/google/d/a/a/fp;->d:Lcom/google/d/a/a/fp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/fr;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Lcom/google/d/a/a/fr;

    invoke-direct {v0}, Lcom/google/d/a/a/fr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627
    sget-object v0, Lcom/google/d/a/a/fp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 679
    invoke-virtual {p0}, Lcom/google/d/a/a/fp;->c()I

    .line 680
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 681
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 683
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 684
    iget v0, p0, Lcom/google/d/a/a/fp;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 686
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/fp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 687
    return-void

    .line 681
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    goto :goto_0

    .line 684
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 663
    iget-byte v0, p0, Lcom/google/d/a/a/fp;->e:B

    .line 664
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 674
    :goto_0
    return v0

    .line 665
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 667
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 668
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 669
    iput-byte v2, p0, Lcom/google/d/a/a/fp;->e:B

    move v0, v2

    .line 670
    goto :goto_0

    :cond_2
    move v0, v2

    .line 667
    goto :goto_1

    .line 668
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    goto :goto_2

    .line 673
    :cond_4
    iput-byte v1, p0, Lcom/google/d/a/a/fp;->e:B

    move v0, v1

    .line 674
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 691
    iget v0, p0, Lcom/google/d/a/a/fp;->f:I

    .line 692
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 705
    :goto_0
    return v0

    .line 695
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 697
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 699
    :goto_2
    iget v2, p0, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 700
    iget v2, p0, Lcom/google/d/a/a/fp;->c:I

    .line 701
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 703
    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/fp;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    iput v0, p0, Lcom/google/d/a/a/fp;->f:I

    goto :goto_0

    .line 697
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    goto :goto_1

    .line 701
    :cond_3
    const/16 v1, 0xa

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 551
    invoke-static {}, Lcom/google/d/a/a/fp;->newBuilder()Lcom/google/d/a/a/fr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/fr;->a(Lcom/google/d/a/a/fp;)Lcom/google/d/a/a/fr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 551
    invoke-static {}, Lcom/google/d/a/a/fp;->newBuilder()Lcom/google/d/a/a/fr;

    move-result-object v0

    return-object v0
.end method
