.class public final enum Lcom/google/d/a/a/pg;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/pg;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/pg;

.field public static final enum b:Lcom/google/d/a/a/pg;

.field private static final synthetic d:[Lcom/google/d/a/a/pg;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 418
    new-instance v0, Lcom/google/d/a/a/pg;

    const-string v1, "WEEK_OF_MONTH"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/pg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pg;->a:Lcom/google/d/a/a/pg;

    .line 422
    new-instance v0, Lcom/google/d/a/a/pg;

    const-string v1, "WEEK_OF_YEAR"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/pg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pg;->b:Lcom/google/d/a/a/pg;

    .line 413
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/d/a/a/pg;

    sget-object v1, Lcom/google/d/a/a/pg;->a:Lcom/google/d/a/a/pg;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/pg;->b:Lcom/google/d/a/a/pg;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/pg;->d:[Lcom/google/d/a/a/pg;

    .line 452
    new-instance v0, Lcom/google/d/a/a/ph;

    invoke-direct {v0}, Lcom/google/d/a/a/ph;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 461
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 462
    iput p3, p0, Lcom/google/d/a/a/pg;->c:I

    .line 463
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/pg;
    .locals 1

    .prologue
    .line 440
    packed-switch p0, :pswitch_data_0

    .line 443
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 441
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/pg;->a:Lcom/google/d/a/a/pg;

    goto :goto_0

    .line 442
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/pg;->b:Lcom/google/d/a/a/pg;

    goto :goto_0

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/pg;
    .locals 1

    .prologue
    .line 413
    const-class v0, Lcom/google/d/a/a/pg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pg;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/pg;
    .locals 1

    .prologue
    .line 413
    sget-object v0, Lcom/google/d/a/a/pg;->d:[Lcom/google/d/a/a/pg;

    invoke-virtual {v0}, [Lcom/google/d/a/a/pg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/pg;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lcom/google/d/a/a/pg;->c:I

    return v0
.end method
