.class public final enum Lcom/google/d/a/a/ft;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ft;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ft;

.field public static final enum b:Lcom/google/d/a/a/ft;

.field public static final enum c:Lcom/google/d/a/a/ft;

.field public static final enum d:Lcom/google/d/a/a/ft;

.field private static final synthetic f:[Lcom/google/d/a/a/ft;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 458
    new-instance v0, Lcom/google/d/a/a/ft;

    const-string v1, "CROSSING_ALLOWED"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/d/a/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ft;->a:Lcom/google/d/a/a/ft;

    .line 462
    new-instance v0, Lcom/google/d/a/a/ft;

    const-string v1, "CROSSING_DISALLOWED"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ft;->b:Lcom/google/d/a/a/ft;

    .line 466
    new-instance v0, Lcom/google/d/a/a/ft;

    const-string v1, "CROSSING_LEGALLY_DISALLOWED"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ft;->c:Lcom/google/d/a/a/ft;

    .line 470
    new-instance v0, Lcom/google/d/a/a/ft;

    const-string v1, "CROSSING_PHYSICALLY_IMPOSSIBLE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ft;->d:Lcom/google/d/a/a/ft;

    .line 453
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/d/a/a/ft;

    sget-object v1, Lcom/google/d/a/a/ft;->a:Lcom/google/d/a/a/ft;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/ft;->b:Lcom/google/d/a/a/ft;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/ft;->c:Lcom/google/d/a/a/ft;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ft;->d:Lcom/google/d/a/a/ft;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/d/a/a/ft;->f:[Lcom/google/d/a/a/ft;

    .line 510
    new-instance v0, Lcom/google/d/a/a/fu;

    invoke-direct {v0}, Lcom/google/d/a/a/fu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 520
    iput p3, p0, Lcom/google/d/a/a/ft;->e:I

    .line 521
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ft;
    .locals 1

    .prologue
    .line 496
    sparse-switch p0, :sswitch_data_0

    .line 501
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 497
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/ft;->a:Lcom/google/d/a/a/ft;

    goto :goto_0

    .line 498
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/ft;->b:Lcom/google/d/a/a/ft;

    goto :goto_0

    .line 499
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/ft;->c:Lcom/google/d/a/a/ft;

    goto :goto_0

    .line 500
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/ft;->d:Lcom/google/d/a/a/ft;

    goto :goto_0

    .line 496
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x21 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ft;
    .locals 1

    .prologue
    .line 453
    const-class v0, Lcom/google/d/a/a/ft;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ft;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ft;
    .locals 1

    .prologue
    .line 453
    sget-object v0, Lcom/google/d/a/a/ft;->f:[Lcom/google/d/a/a/ft;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ft;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ft;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/google/d/a/a/ft;->e:I

    return v0
.end method
