.class public final enum Lcom/google/d/a/a/ef;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ef;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ef;

.field private static final synthetic c:[Lcom/google/d/a/a/ef;


# instance fields
.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    new-instance v0, Lcom/google/d/a/a/ef;

    const-string v1, "HTML_DESCRIPTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ef;->a:Lcom/google/d/a/a/ef;

    .line 130
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/d/a/a/ef;

    sget-object v1, Lcom/google/d/a/a/ef;->a:Lcom/google/d/a/a/ef;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/ef;->c:[Lcom/google/d/a/a/ef;

    .line 160
    new-instance v0, Lcom/google/d/a/a/eg;

    invoke-direct {v0}, Lcom/google/d/a/a/eg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 170
    iput p3, p0, Lcom/google/d/a/a/ef;->b:I

    .line 171
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ef;
    .locals 1

    .prologue
    .line 149
    packed-switch p0, :pswitch_data_0

    .line 151
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 150
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/ef;->a:Lcom/google/d/a/a/ef;

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ef;
    .locals 1

    .prologue
    .line 130
    const-class v0, Lcom/google/d/a/a/ef;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ef;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ef;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/d/a/a/ef;->c:[Lcom/google/d/a/a/ef;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ef;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ef;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/d/a/a/ef;->b:I

    return v0
.end method
