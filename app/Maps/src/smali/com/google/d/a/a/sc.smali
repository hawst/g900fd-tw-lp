.class public final Lcom/google/d/a/a/sc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/sf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/sc;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/d/a/a/sc;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:J

.field e:Ljava/lang/Object;

.field f:I

.field g:Z

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/d/a/a/sd;

    invoke-direct {v0}, Lcom/google/d/a/a/sd;-><init>()V

    sput-object v0, Lcom/google/d/a/a/sc;->PARSER:Lcom/google/n/ax;

    .line 471
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/sc;->l:Lcom/google/n/aw;

    .line 1045
    new-instance v0, Lcom/google/d/a/a/sc;

    invoke-direct {v0}, Lcom/google/d/a/a/sc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/sc;->i:Lcom/google/d/a/a/sc;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 104
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 374
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    .line 389
    iput-byte v1, p0, Lcom/google/d/a/a/sc;->j:B

    .line 430
    iput v1, p0, Lcom/google/d/a/a/sc;->k:I

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    .line 107
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/sc;->d:J

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    .line 109
    iput v3, p0, Lcom/google/d/a/a/sc;->f:I

    .line 110
    iput-boolean v3, p0, Lcom/google/d/a/a/sc;->g:Z

    .line 111
    iget-object v0, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 112
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    invoke-direct {p0}, Lcom/google/d/a/a/sc;-><init>()V

    .line 119
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 124
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 125
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 126
    sparse-switch v0, :sswitch_data_0

    .line 131
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 133
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 129
    goto :goto_0

    .line 138
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 139
    iget v5, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/sc;->a:I

    .line 140
    iput-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/sc;->au:Lcom/google/n/bn;

    throw v0

    .line 144
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 145
    iget v5, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/sc;->a:I

    .line 146
    iput-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    .line 180
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 181
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/sc;->a:I

    .line 151
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/d/a/a/sc;->d:J

    goto :goto_0

    .line 155
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 156
    iget v5, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/d/a/a/sc;->a:I

    .line 157
    iput-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    goto :goto_0

    .line 161
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/sc;->a:I

    .line 162
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/sc;->f:I

    goto :goto_0

    .line 166
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/sc;->a:I

    .line 167
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/sc;->g:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 171
    :sswitch_7
    iget-object v0, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 172
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/sc;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 183
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->au:Lcom/google/n/bn;

    .line 184
    return-void

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0xfa2 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 102
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 374
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    .line 389
    iput-byte v1, p0, Lcom/google/d/a/a/sc;->j:B

    .line 430
    iput v1, p0, Lcom/google/d/a/a/sc;->k:I

    .line 103
    return-void
.end method

.method public static d()Lcom/google/d/a/a/sc;
    .locals 1

    .prologue
    .line 1048
    sget-object v0, Lcom/google/d/a/a/sc;->i:Lcom/google/d/a/a/sc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/se;
    .locals 1

    .prologue
    .line 533
    new-instance v0, Lcom/google/d/a/a/se;

    invoke-direct {v0}, Lcom/google/d/a/a/se;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/sc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    sget-object v0, Lcom/google/d/a/a/sc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 405
    invoke-virtual {p0}, Lcom/google/d/a/a/sc;->c()I

    .line 406
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 407
    iget-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 409
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 410
    iget-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 412
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 413
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/d/a/a/sc;->d:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 415
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 416
    iget-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 418
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 419
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/d/a/a/sc;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_a

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 421
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 422
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/sc;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_b

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 424
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 425
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 427
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/sc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 428
    return-void

    .line 407
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 410
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 416
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 419
    :cond_a
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    :cond_b
    move v0, v2

    .line 422
    goto :goto_4
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 391
    iget-byte v2, p0, Lcom/google/d/a/a/sc;->j:B

    .line 392
    if-ne v2, v0, :cond_0

    .line 400
    :goto_0
    return v0

    .line 393
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 395
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 396
    iput-byte v1, p0, Lcom/google/d/a/a/sc;->j:B

    move v0, v1

    .line 397
    goto :goto_0

    :cond_2
    move v2, v1

    .line 395
    goto :goto_1

    .line 399
    :cond_3
    iput-byte v0, p0, Lcom/google/d/a/a/sc;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 432
    iget v0, p0, Lcom/google/d/a/a/sc;->k:I

    .line 433
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 466
    :goto_0
    return v0

    .line 436
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 438
    iget-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 440
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 442
    iget-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 444
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 445
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/d/a/a/sc;->d:J

    .line 446
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 448
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 450
    iget-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 452
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 453
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/d/a/a/sc;->f:I

    .line 454
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 456
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 457
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/sc;->g:Z

    .line 458
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 460
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 461
    const/16 v0, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    .line 462
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 464
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/sc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 465
    iput v0, p0, Lcom/google/d/a/a/sc;->k:I

    goto/16 :goto_0

    .line 438
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 442
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 450
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 454
    :cond_a
    const/16 v0, 0xa

    goto :goto_5

    :cond_b
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Lcom/google/d/a/a/sc;->newBuilder()Lcom/google/d/a/a/se;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/se;->a(Lcom/google/d/a/a/sc;)Lcom/google/d/a/a/se;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Lcom/google/d/a/a/sc;->newBuilder()Lcom/google/d/a/a/se;

    move-result-object v0

    return-object v0
.end method
