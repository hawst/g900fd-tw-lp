.class public final enum Lcom/google/d/a/a/nr;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/nr;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/nr;

.field public static final enum b:Lcom/google/d/a/a/nr;

.field private static final synthetic d:[Lcom/google/d/a/a/nr;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 463
    new-instance v0, Lcom/google/d/a/a/nr;

    const-string v1, "FLAG_NO_COLD_CALLS"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/nr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nr;->a:Lcom/google/d/a/a/nr;

    .line 467
    new-instance v0, Lcom/google/d/a/a/nr;

    const-string v1, "FLAG_PREFERRED"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/nr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nr;->b:Lcom/google/d/a/a/nr;

    .line 458
    new-array v0, v4, [Lcom/google/d/a/a/nr;

    sget-object v1, Lcom/google/d/a/a/nr;->a:Lcom/google/d/a/a/nr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/nr;->b:Lcom/google/d/a/a/nr;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/d/a/a/nr;->d:[Lcom/google/d/a/a/nr;

    .line 497
    new-instance v0, Lcom/google/d/a/a/ns;

    invoke-direct {v0}, Lcom/google/d/a/a/ns;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 506
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 507
    iput p3, p0, Lcom/google/d/a/a/nr;->c:I

    .line 508
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/nr;
    .locals 1

    .prologue
    .line 485
    packed-switch p0, :pswitch_data_0

    .line 488
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 486
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/nr;->a:Lcom/google/d/a/a/nr;

    goto :goto_0

    .line 487
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/nr;->b:Lcom/google/d/a/a/nr;

    goto :goto_0

    .line 485
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/nr;
    .locals 1

    .prologue
    .line 458
    const-class v0, Lcom/google/d/a/a/nr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nr;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/nr;
    .locals 1

    .prologue
    .line 458
    sget-object v0, Lcom/google/d/a/a/nr;->d:[Lcom/google/d/a/a/nr;

    invoke-virtual {v0}, [Lcom/google/d/a/a/nr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/nr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lcom/google/d/a/a/nr;->c:I

    return v0
.end method
