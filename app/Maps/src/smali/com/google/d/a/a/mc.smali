.class public final enum Lcom/google/d/a/a/mc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mc;

.field public static final enum b:Lcom/google/d/a/a/mc;

.field public static final enum c:Lcom/google/d/a/a/mc;

.field public static final enum d:Lcom/google/d/a/a/mc;

.field private static final synthetic f:[Lcom/google/d/a/a/mc;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1767
    new-instance v0, Lcom/google/d/a/a/mc;

    const-string v1, "BARRIER_NONE"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/d/a/a/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mc;->a:Lcom/google/d/a/a/mc;

    .line 1771
    new-instance v0, Lcom/google/d/a/a/mc;

    const-string v1, "BARRIER_PRESENT"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mc;->b:Lcom/google/d/a/a/mc;

    .line 1775
    new-instance v0, Lcom/google/d/a/a/mc;

    const-string v1, "BARRIER_LEGAL"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mc;->c:Lcom/google/d/a/a/mc;

    .line 1779
    new-instance v0, Lcom/google/d/a/a/mc;

    const-string v1, "BARRIER_PHYSICAL"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/mc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mc;->d:Lcom/google/d/a/a/mc;

    .line 1762
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/d/a/a/mc;

    sget-object v1, Lcom/google/d/a/a/mc;->a:Lcom/google/d/a/a/mc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mc;->b:Lcom/google/d/a/a/mc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/mc;->c:Lcom/google/d/a/a/mc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mc;->d:Lcom/google/d/a/a/mc;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/d/a/a/mc;->f:[Lcom/google/d/a/a/mc;

    .line 1819
    new-instance v0, Lcom/google/d/a/a/md;

    invoke-direct {v0}, Lcom/google/d/a/a/md;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1828
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1829
    iput p3, p0, Lcom/google/d/a/a/mc;->e:I

    .line 1830
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mc;
    .locals 1

    .prologue
    .line 1805
    sparse-switch p0, :sswitch_data_0

    .line 1810
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1806
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mc;->a:Lcom/google/d/a/a/mc;

    goto :goto_0

    .line 1807
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mc;->b:Lcom/google/d/a/a/mc;

    goto :goto_0

    .line 1808
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mc;->c:Lcom/google/d/a/a/mc;

    goto :goto_0

    .line 1809
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mc;->d:Lcom/google/d/a/a/mc;

    goto :goto_0

    .line 1805
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x21 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mc;
    .locals 1

    .prologue
    .line 1762
    const-class v0, Lcom/google/d/a/a/mc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mc;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mc;
    .locals 1

    .prologue
    .line 1762
    sget-object v0, Lcom/google/d/a/a/mc;->f:[Lcom/google/d/a/a/mc;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1801
    iget v0, p0, Lcom/google/d/a/a/mc;->e:I

    return v0
.end method
