.class public final Lcom/google/d/a/a/nk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/nl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ni;",
        "Lcom/google/d/a/a/nk;",
        ">;",
        "Lcom/google/d/a/a/nl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:J

.field private j:J

.field private k:Lcom/google/n/ao;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 925
    sget-object v0, Lcom/google/d/a/a/ni;->o:Lcom/google/d/a/a/ni;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nk;->b:Lcom/google/n/ao;

    .line 1207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    .line 1343
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/nk;->d:Ljava/lang/Object;

    .line 1419
    const/16 v0, 0x1111

    iput v0, p0, Lcom/google/d/a/a/nk;->e:I

    .line 1451
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/nk;->f:Ljava/lang/Object;

    .line 1527
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/nk;->g:Ljava/lang/Object;

    .line 1603
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/nk;->h:Ljava/lang/Object;

    .line 1743
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nk;->k:Lcom/google/n/ao;

    .line 1803
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    .line 1939
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nk;->m:Lcom/google/n/ao;

    .line 1998
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nk;->n:Lcom/google/n/ao;

    .line 926
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ni;)Lcom/google/d/a/a/nk;
    .locals 6

    .prologue
    const/16 v5, 0x400

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1033
    invoke-static {}, Lcom/google/d/a/a/ni;->d()Lcom/google/d/a/a/ni;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1100
    :goto_0
    return-object p0

    .line 1034
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1035
    iget-object v2, p0, Lcom/google/d/a/a/nk;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1036
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1038
    :cond_1
    iget-object v2, p1, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1039
    iget-object v2, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1040
    iget-object v2, p1, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    .line 1041
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1048
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_11

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1049
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1050
    iget-object v2, p1, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->d:Ljava/lang/Object;

    .line 1053
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1054
    iget v2, p1, Lcom/google/d/a/a/ni;->e:I

    iget v3, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/nk;->a:I

    iput v2, p0, Lcom/google/d/a/a/nk;->e:I

    .line 1056
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1057
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1058
    iget-object v2, p1, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->f:Ljava/lang/Object;

    .line 1061
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1062
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1063
    iget-object v2, p1, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->g:Ljava/lang/Object;

    .line 1066
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1067
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1068
    iget-object v2, p1, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->h:Ljava/lang/Object;

    .line 1071
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1072
    iget-wide v2, p1, Lcom/google/d/a/a/ni;->i:J

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/d/a/a/nk;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/nk;->i:J

    .line 1074
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1075
    iget-wide v2, p1, Lcom/google/d/a/a/ni;->j:J

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/d/a/a/nk;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/nk;->j:J

    .line 1077
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1078
    iget-object v2, p0, Lcom/google/d/a/a/nk;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1079
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1081
    :cond_a
    iget-object v2, p1, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1082
    iget-object v2, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1083
    iget-object v2, p1, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    .line 1084
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1091
    :cond_b
    :goto_b
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1092
    iget-object v2, p0, Lcom/google/d/a/a/nk;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1093
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1095
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v2, v2, 0x400

    if-ne v2, v5, :cond_1c

    :goto_d
    if-eqz v0, :cond_d

    .line 1096
    iget-object v0, p0, Lcom/google/d/a/a/nk;->n:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1097
    iget v0, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1099
    :cond_d
    iget-object v0, p1, Lcom/google/d/a/a/ni;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 1034
    goto/16 :goto_1

    .line 1043
    :cond_f
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_10

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1044
    :cond_10
    iget-object v2, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_11
    move v2, v1

    .line 1048
    goto/16 :goto_3

    :cond_12
    move v2, v1

    .line 1053
    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 1056
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 1061
    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 1066
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 1071
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 1074
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 1077
    goto/16 :goto_a

    .line 1086
    :cond_19
    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v2, v2, 0x400

    if-eq v2, v5, :cond_1a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/nk;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/d/a/a/nk;->a:I

    .line 1087
    :cond_1a
    iget-object v2, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_1b
    move v2, v1

    .line 1091
    goto/16 :goto_c

    :cond_1c
    move v0, v1

    .line 1095
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 917
    new-instance v2, Lcom/google/d/a/a/ni;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ni;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/nk;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/nk;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/nk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/nk;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/nk;->e:I

    iput v4, v2, Lcom/google/d/a/a/ni;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/nk;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/nk;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, p0, Lcom/google/d/a/a/nk;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-wide v4, p0, Lcom/google/d/a/a/nk;->i:J

    iput-wide v4, v2, Lcom/google/d/a/a/ni;->i:J

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-wide v4, p0, Lcom/google/d/a/a/nk;->j:J

    iput-wide v4, v2, Lcom/google/d/a/a/ni;->j:J

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, v2, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/nk;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/nk;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/d/a/a/nk;->a:I

    :cond_9
    iget-object v4, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-object v4, v2, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/nk;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/nk;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-object v3, v2, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/nk;->n:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/nk;->n:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/ni;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 917
    check-cast p1, Lcom/google/d/a/a/ni;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/nk;->a(Lcom/google/d/a/a/ni;)Lcom/google/d/a/a/nk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104
    iget v0, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 1142
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1104
    goto :goto_0

    .line 1108
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/nk;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 1112
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1113
    iget-object v0, p0, Lcom/google/d/a/a/nk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ji;->d()Lcom/google/d/a/a/ji;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ji;

    invoke-virtual {v0}, Lcom/google/d/a/a/ji;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1112
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1118
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1119
    iget-object v0, p0, Lcom/google/d/a/a/nk;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v1, v2

    .line 1124
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1125
    iget-object v0, p0, Lcom/google/d/a/a/nk;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1124
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_5
    move v0, v2

    .line 1118
    goto :goto_3

    .line 1130
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    .line 1131
    iget-object v0, p0, Lcom/google/d/a/a/nk;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1136
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/nk;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_6
    if-eqz v0, :cond_8

    .line 1137
    iget-object v0, p0, Lcom/google/d/a/a/nk;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/a/o;->d()Lcom/google/d/a/a/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/a/o;

    invoke-virtual {v0}, Lcom/google/d/a/a/a/o;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_8
    move v2, v3

    .line 1142
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 1130
    goto :goto_5

    :cond_a
    move v0, v2

    .line 1136
    goto :goto_6
.end method
