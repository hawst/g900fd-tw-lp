.class public final Lcom/google/d/a/a/bb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bb;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/d/a/a/bb;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/google/n/ao;

.field e:F

.field f:F

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/google/d/a/a/bc;

    invoke-direct {v0}, Lcom/google/d/a/a/bc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bb;->PARSER:Lcom/google/n/ax;

    .line 532
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/bb;->l:Lcom/google/n/aw;

    .line 1123
    new-instance v0, Lcom/google/d/a/a/bb;

    invoke-direct {v0}, Lcom/google/d/a/a/bb;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bb;->i:Lcom/google/d/a/a/bb;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    .line 427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    .line 442
    iput-byte v3, p0, Lcom/google/d/a/a/bb;->j:B

    .line 491
    iput v3, p0, Lcom/google/d/a/a/bb;->k:I

    .line 95
    iput v4, p0, Lcom/google/d/a/a/bb;->b:I

    .line 96
    iput v4, p0, Lcom/google/d/a/a/bb;->c:I

    .line 97
    iget-object v0, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 98
    iput v2, p0, Lcom/google/d/a/a/bb;->e:F

    .line 99
    iput v2, p0, Lcom/google/d/a/a/bb;->f:F

    .line 100
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    .line 101
    iget-object v0, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 102
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x20

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Lcom/google/d/a/a/bb;-><init>()V

    .line 111
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 114
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 115
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 116
    sparse-switch v4, :sswitch_data_0

    .line 121
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 123
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 119
    goto :goto_0

    .line 128
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 129
    invoke-static {v4}, Lcom/google/d/a/a/be;->a(I)Lcom/google/d/a/a/be;

    move-result-object v5

    .line 130
    if-nez v5, :cond_2

    .line 131
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v7, :cond_1

    .line 182
    iget-object v1, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    .line 184
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bb;->au:Lcom/google/n/bn;

    throw v0

    .line 133
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/bb;->a:I

    .line 134
    iput v4, p0, Lcom/google/d/a/a/bb;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 177
    :catch_1
    move-exception v0

    .line 178
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 179
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :sswitch_2
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/bb;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/bb;->c:I

    goto :goto_0

    .line 144
    :sswitch_3
    iget v4, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/bb;->a:I

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/bb;->e:F

    goto :goto_0

    .line 149
    :sswitch_4
    iget v4, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/a/a/bb;->a:I

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/bb;->f:F

    goto :goto_0

    .line 154
    :sswitch_5
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v7, :cond_3

    .line 155
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    .line 157
    or-int/lit8 v1, v1, 0x20

    .line 159
    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 160
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 159
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 164
    :sswitch_6
    iget-object v4, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 165
    iget v4, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/d/a/a/bb;->a:I

    goto/16 :goto_0

    .line 169
    :sswitch_7
    iget-object v4, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 170
    iget v4, p0, Lcom/google/d/a/a/bb;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/bb;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 181
    :cond_4
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v7, :cond_5

    .line 182
    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    .line 184
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bb;->au:Lcom/google/n/bn;

    .line 185
    return-void

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0xfa2 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    .line 427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    .line 442
    iput-byte v1, p0, Lcom/google/d/a/a/bb;->j:B

    .line 491
    iput v1, p0, Lcom/google/d/a/a/bb;->k:I

    .line 93
    return-void
.end method

.method public static d()Lcom/google/d/a/a/bb;
    .locals 1

    .prologue
    .line 1126
    sget-object v0, Lcom/google/d/a/a/bb;->i:Lcom/google/d/a/a/bb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/bd;
    .locals 1

    .prologue
    .line 594
    new-instance v0, Lcom/google/d/a/a/bd;

    invoke-direct {v0}, Lcom/google/d/a/a/bd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    sget-object v0, Lcom/google/d/a/a/bb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 466
    invoke-virtual {p0}, Lcom/google/d/a/a/bb;->c()I

    .line 467
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 468
    iget v1, p0, Lcom/google/d/a/a/bb;->b:I

    invoke-static {v2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 470
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 471
    iget v1, p0, Lcom/google/d/a/a/bb;->c:I

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 473
    :cond_1
    :goto_1
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 474
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/d/a/a/bb;->e:F

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 476
    :cond_2
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    .line 477
    iget v1, p0, Lcom/google/d/a/a/bb;->f:F

    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    :cond_3
    move v1, v0

    .line 479
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 480
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 479
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 468
    :cond_4
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 471
    :cond_5
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 482
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 483
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 485
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_8

    .line 486
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 488
    :cond_8
    iget-object v0, p0, Lcom/google/d/a/a/bb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 489
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 444
    iget-byte v0, p0, Lcom/google/d/a/a/bb;->j:B

    .line 445
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 461
    :cond_0
    :goto_0
    return v2

    .line 446
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 448
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 449
    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 450
    iput-byte v2, p0, Lcom/google/d/a/a/bb;->j:B

    goto :goto_0

    .line 448
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 454
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 455
    iget-object v0, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 456
    iput-byte v2, p0, Lcom/google/d/a/a/bb;->j:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 454
    goto :goto_2

    .line 460
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/bb;->j:B

    move v2, v3

    .line 461
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 493
    iget v0, p0, Lcom/google/d/a/a/bb;->k:I

    .line 494
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 527
    :goto_0
    return v0

    .line 497
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 498
    iget v0, p0, Lcom/google/d/a/a/bb;->b:I

    .line 499
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 501
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 502
    iget v3, p0, Lcom/google/d/a/a/bb;->c:I

    .line 503
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 505
    :cond_2
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 506
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/d/a/a/bb;->e:F

    .line 507
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 509
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 510
    iget v1, p0, Lcom/google/d/a/a/bb;->f:F

    .line 511
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_4
    move v1, v2

    move v3, v0

    .line 513
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 514
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    .line 515
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 513
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v0, v1

    .line 499
    goto :goto_1

    .line 517
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 518
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    .line 519
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 521
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_8

    .line 522
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    .line 523
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 525
    :cond_8
    iget-object v0, p0, Lcom/google/d/a/a/bb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 526
    iput v0, p0, Lcom/google/d/a/a/bb;->k:I

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/d/a/a/bb;->newBuilder()Lcom/google/d/a/a/bd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/bd;->a(Lcom/google/d/a/a/bb;)Lcom/google/d/a/a/bd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/d/a/a/bb;->newBuilder()Lcom/google/d/a/a/bd;

    move-result-object v0

    return-object v0
.end method
