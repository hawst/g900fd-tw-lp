.class public final Lcom/google/d/a/a/fj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/fh;",
        "Lcom/google/d/a/a/fj;",
        ">;",
        "Lcom/google/d/a/a/fk;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 258
    sget-object v0, Lcom/google/d/a/a/fh;->c:Lcom/google/d/a/a/fh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 300
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/fj;->b:Ljava/lang/Object;

    .line 259
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 284
    invoke-static {}, Lcom/google/d/a/a/fh;->d()Lcom/google/d/a/a/fh;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 291
    :goto_0
    return-object p0

    .line 285
    :cond_0
    iget v1, p1, Lcom/google/d/a/a/fh;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 286
    iget v0, p0, Lcom/google/d/a/a/fj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/fj;->a:I

    .line 287
    iget-object v0, p1, Lcom/google/d/a/a/fh;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/d/a/a/fj;->b:Ljava/lang/Object;

    .line 290
    :cond_1
    iget-object v0, p1, Lcom/google/d/a/a/fh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 285
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/d/a/a/fj;->c()Lcom/google/d/a/a/fh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 250
    check-cast p1, Lcom/google/d/a/a/fh;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/fj;->a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/d/a/a/fh;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 272
    new-instance v2, Lcom/google/d/a/a/fh;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/fh;-><init>(Lcom/google/n/v;)V

    .line 273
    iget v3, p0, Lcom/google/d/a/a/fj;->a:I

    .line 274
    const/4 v1, 0x0

    .line 275
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 278
    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/fj;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/fh;->b:Ljava/lang/Object;

    .line 279
    iput v0, v2, Lcom/google/d/a/a/fh;->a:I

    .line 280
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
