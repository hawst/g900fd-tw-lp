.class public final Lcom/google/d/a/a/nq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/nx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/nn;",
        "Lcom/google/d/a/a/nq;",
        ">;",
        "Lcom/google/d/a/a/nx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/aq;

.field private g:Z

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 933
    sget-object v0, Lcom/google/d/a/a/nn;->k:Lcom/google/d/a/a/nn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1095
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nq;->b:Lcom/google/n/ao;

    .line 1154
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nq;->c:Lcom/google/n/ao;

    .line 1213
    iput v1, p0, Lcom/google/d/a/a/nq;->d:I

    .line 1250
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    .line 1386
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    .line 1511
    iput v1, p0, Lcom/google/d/a/a/nq;->h:I

    .line 1548
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    .line 1622
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    .line 934
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/nn;)Lcom/google/d/a/a/nq;
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1015
    invoke-static {}, Lcom/google/d/a/a/nn;->d()Lcom/google/d/a/a/nn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1074
    :goto_0
    return-object p0

    .line 1016
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1017
    iget-object v2, p0, Lcom/google/d/a/a/nq;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1018
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1020
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1021
    iget-object v2, p0, Lcom/google/d/a/a/nq;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1022
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1024
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1025
    iget v2, p1, Lcom/google/d/a/a/nn;->d:I

    invoke-static {v2}, Lcom/google/d/a/a/nt;->a(I)Lcom/google/d/a/a/nt;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/d/a/a/nt;->a:Lcom/google/d/a/a/nt;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1016
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1020
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1024
    goto :goto_3

    .line 1025
    :cond_7
    iget v3, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/nq;->a:I

    iget v2, v2, Lcom/google/d/a/a/nt;->f:I

    iput v2, p0, Lcom/google/d/a/a/nq;->d:I

    .line 1027
    :cond_8
    iget-object v2, p1, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1028
    iget-object v2, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1029
    iget-object v2, p1, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    .line 1030
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1037
    :cond_9
    :goto_4
    iget-object v2, p1, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1038
    iget-object v2, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1039
    iget-object v2, p1, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    .line 1040
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1047
    :cond_a
    :goto_5
    iget v2, p1, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_b

    .line 1048
    iget-boolean v2, p1, Lcom/google/d/a/a/nn;->g:Z

    iget v3, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/nq;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/nq;->g:Z

    .line 1050
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_12

    :goto_7
    if-eqz v0, :cond_14

    .line 1051
    iget v0, p1, Lcom/google/d/a/a/nn;->h:I

    invoke-static {v0}, Lcom/google/d/a/a/nv;->a(I)Lcom/google/d/a/a/nv;

    move-result-object v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/d/a/a/nv;->a:Lcom/google/d/a/a/nv;

    :cond_c
    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1032
    :cond_d
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1033
    :cond_e
    iget-object v2, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 1042
    :cond_f
    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v5, :cond_10

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1043
    :cond_10
    iget-object v2, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_11
    move v2, v1

    .line 1047
    goto :goto_6

    :cond_12
    move v0, v1

    .line 1050
    goto :goto_7

    .line 1051
    :cond_13
    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/d/a/a/nq;->a:I

    iget v0, v0, Lcom/google/d/a/a/nv;->d:I

    iput v0, p0, Lcom/google/d/a/a/nq;->h:I

    .line 1053
    :cond_14
    iget-object v0, p1, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    .line 1054
    iget-object v0, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1055
    iget-object v0, p1, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    .line 1056
    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1063
    :cond_15
    :goto_8
    iget-object v0, p1, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1064
    iget-object v0, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1065
    iget-object v0, p1, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    .line 1066
    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1073
    :cond_16
    :goto_9
    iget-object v0, p1, Lcom/google/d/a/a/nn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1058
    :cond_17
    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_18

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1059
    :cond_18
    iget-object v0, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    .line 1068
    :cond_19
    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_1a

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/nq;->a:I

    .line 1069
    :cond_1a
    iget-object v0, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 925
    new-instance v2, Lcom/google/d/a/a/nn;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/nn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/nq;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/nq;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/nq;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/nq;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/nq;->d:I

    iput v1, v2, Lcom/google/d/a/a/nn;->d:I

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/d/a/a/nq;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/d/a/a/nq;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/nq;->f:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-boolean v1, p0, Lcom/google/d/a/a/nq;->g:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/nn;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/nq;->h:I

    iput v1, v2, Lcom/google/d/a/a/nn;->h:I

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/d/a/a/nq;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/d/a/a/nq;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/d/a/a/nq;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/nq;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/nn;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 925
    check-cast p1, Lcom/google/d/a/a/nn;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/nq;->a(Lcom/google/d/a/a/nn;)Lcom/google/d/a/a/nq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1078
    iget v0, p0, Lcom/google/d/a/a/nq;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1079
    iget-object v0, p0, Lcom/google/d/a/a/nq;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/g/a/b;->d()Lcom/google/g/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/g/a/b;

    invoke-virtual {v0}, Lcom/google/g/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1090
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1078
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1084
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1085
    iget-object v0, p0, Lcom/google/d/a/a/nq;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1090
    goto :goto_1
.end method
