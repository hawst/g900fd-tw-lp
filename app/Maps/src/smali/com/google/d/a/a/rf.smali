.class public final Lcom/google/d/a/a/rf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/rd;",
        "Lcom/google/d/a/a/rf;",
        ">;",
        "Lcom/google/d/a/a/rg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 374
    sget-object v0, Lcom/google/d/a/a/rd;->e:Lcom/google/d/a/a/rd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 447
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rf;->b:Lcom/google/n/ao;

    .line 506
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/rf;->c:Ljava/lang/Object;

    .line 582
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/rf;->d:Ljava/lang/Object;

    .line 375
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/rd;)Lcom/google/d/a/a/rf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 414
    invoke-static {}, Lcom/google/d/a/a/rd;->d()Lcom/google/d/a/a/rd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 430
    :goto_0
    return-object p0

    .line 415
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 416
    iget-object v2, p0, Lcom/google/d/a/a/rf;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 417
    iget v2, p0, Lcom/google/d/a/a/rf;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/rf;->a:I

    .line 419
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 420
    iget v2, p0, Lcom/google/d/a/a/rf;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/rf;->a:I

    .line 421
    iget-object v2, p1, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/rf;->c:Ljava/lang/Object;

    .line 424
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 425
    iget v0, p0, Lcom/google/d/a/a/rf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/rf;->a:I

    .line 426
    iget-object v0, p1, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/d/a/a/rf;->d:Ljava/lang/Object;

    .line 429
    :cond_3
    iget-object v0, p1, Lcom/google/d/a/a/rd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 415
    goto :goto_1

    :cond_5
    move v2, v1

    .line 419
    goto :goto_2

    :cond_6
    move v0, v1

    .line 424
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 366
    new-instance v2, Lcom/google/d/a/a/rd;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/rd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/rf;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rf;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rf;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/rf;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/rf;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/d/a/a/rd;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 366
    check-cast p1, Lcom/google/d/a/a/rd;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/rf;->a(Lcom/google/d/a/a/rd;)Lcom/google/d/a/a/rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 434
    iget v0, p0, Lcom/google/d/a/a/rf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 442
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 434
    goto :goto_0

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/rf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 440
    goto :goto_1

    :cond_2
    move v0, v2

    .line 442
    goto :goto_1
.end method
