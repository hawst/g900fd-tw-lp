.class public final Lcom/google/d/a/a/fm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fm;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/d/a/a/fm;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:I

.field e:I

.field f:F

.field g:F

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field j:F

.field k:F

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 251
    new-instance v0, Lcom/google/d/a/a/fn;

    invoke-direct {v0}, Lcom/google/d/a/a/fn;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fm;->PARSER:Lcom/google/n/ax;

    .line 1278
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/fm;->o:Lcom/google/n/aw;

    .line 2053
    new-instance v0, Lcom/google/d/a/a/fm;

    invoke-direct {v0}, Lcom/google/d/a/a/fm;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fm;->l:Lcom/google/d/a/a/fm;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1163
    iput-byte v0, p0, Lcom/google/d/a/a/fm;->m:B

    .line 1225
    iput v0, p0, Lcom/google/d/a/a/fm;->n:I

    .line 127
    iput v2, p0, Lcom/google/d/a/a/fm;->b:I

    .line 128
    iput-boolean v2, p0, Lcom/google/d/a/a/fm;->c:Z

    .line 129
    iput v3, p0, Lcom/google/d/a/a/fm;->d:I

    .line 130
    iput v3, p0, Lcom/google/d/a/a/fm;->e:I

    .line 131
    iput v1, p0, Lcom/google/d/a/a/fm;->f:F

    .line 132
    iput v1, p0, Lcom/google/d/a/a/fm;->g:F

    .line 133
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    .line 134
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    .line 135
    iput v1, p0, Lcom/google/d/a/a/fm;->j:F

    .line 136
    iput v1, p0, Lcom/google/d/a/a/fm;->k:F

    .line 137
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x80

    const/16 v10, 0x40

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 143
    invoke-direct {p0}, Lcom/google/d/a/a/fm;-><init>()V

    .line 146
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 149
    :cond_0
    :goto_0
    if-nez v4, :cond_8

    .line 150
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 151
    sparse-switch v0, :sswitch_data_0

    .line 156
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 158
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 154
    goto :goto_0

    .line 163
    :sswitch_1
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 164
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fm;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x40

    if-ne v2, v10, :cond_1

    .line 243
    iget-object v2, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    .line 245
    :cond_1
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v11, :cond_2

    .line 246
    iget-object v1, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    .line 248
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fm;->au:Lcom/google/n/bn;

    throw v0

    .line 168
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/fm;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 238
    :catch_1
    move-exception v0

    .line 239
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 240
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    move v0, v3

    .line 169
    goto :goto_1

    .line 173
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 174
    invoke-static {v0}, Lcom/google/d/a/a/fv;->a(I)Lcom/google/d/a/a/fv;

    move-result-object v6

    .line 175
    if-nez v6, :cond_4

    .line 176
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 178
    :cond_4
    iget v6, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/d/a/a/fm;->a:I

    .line 179
    iput v0, p0, Lcom/google/d/a/a/fm;->d:I

    goto :goto_0

    .line 184
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 185
    invoke-static {v0}, Lcom/google/d/a/a/ft;->a(I)Lcom/google/d/a/a/ft;

    move-result-object v6

    .line 186
    if-nez v6, :cond_5

    .line 187
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 189
    :cond_5
    iget v6, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/d/a/a/fm;->a:I

    .line 190
    iput v0, p0, Lcom/google/d/a/a/fm;->e:I

    goto/16 :goto_0

    .line 195
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 196
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fm;->f:F

    goto/16 :goto_0

    .line 200
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fm;->g:F

    goto/16 :goto_0

    .line 205
    :sswitch_7
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v10, :cond_6

    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    .line 208
    or-int/lit8 v1, v1, 0x40

    .line 210
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 211
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 210
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 215
    :sswitch_8
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v11, :cond_7

    .line 216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    .line 218
    or-int/lit16 v1, v1, 0x80

    .line 220
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 221
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 220
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 225
    :sswitch_9
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 226
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fm;->j:F

    goto/16 :goto_0

    .line 230
    :sswitch_a
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/fm;->a:I

    .line 231
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/fm;->k:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 242
    :cond_8
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v10, :cond_9

    .line 243
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    .line 245
    :cond_9
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v11, :cond_a

    .line 246
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    .line 248
    :cond_a
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fm;->au:Lcom/google/n/bn;

    .line 249
    return-void

    .line 151
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 124
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1163
    iput-byte v0, p0, Lcom/google/d/a/a/fm;->m:B

    .line 1225
    iput v0, p0, Lcom/google/d/a/a/fm;->n:I

    .line 125
    return-void
.end method

.method public static d()Lcom/google/d/a/a/fm;
    .locals 1

    .prologue
    .line 2056
    sget-object v0, Lcom/google/d/a/a/fm;->l:Lcom/google/d/a/a/fm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/fo;
    .locals 1

    .prologue
    .line 1340
    new-instance v0, Lcom/google/d/a/a/fo;

    invoke-direct {v0}, Lcom/google/d/a/a/fo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    sget-object v0, Lcom/google/d/a/a/fm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v0, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 1191
    invoke-virtual {p0}, Lcom/google/d/a/a/fm;->c()I

    .line 1192
    iget v2, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 1193
    iget v2, p0, Lcom/google/d/a/a/fm;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_6

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 1195
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1196
    iget-boolean v2, p0, Lcom/google/d/a/a/fm;->c:Z

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_7

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1198
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 1199
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/d/a/a/fm;->d:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_8

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 1201
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 1202
    iget v0, p0, Lcom/google/d/a/a/fm;->e:I

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1204
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 1205
    iget v0, p0, Lcom/google/d/a/a/fm;->f:F

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1207
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 1208
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/d/a/a/fm;->g:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    :cond_5
    move v2, v1

    .line 1210
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 1211
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1210
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1193
    :cond_6
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 1196
    goto/16 :goto_1

    .line 1199
    :cond_8
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 1202
    :cond_9
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 1213
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 1214
    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1213
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1216
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 1217
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/d/a/a/fm;->j:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1219
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 1220
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/d/a/a/fm;->k:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1222
    :cond_d
    iget-object v0, p0, Lcom/google/d/a/a/fm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1223
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1165
    iget-byte v0, p0, Lcom/google/d/a/a/fm;->m:B

    .line 1166
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1186
    :cond_0
    :goto_0
    return v2

    .line 1167
    :cond_1
    if-eqz v0, :cond_0

    .line 1169
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 1170
    iput-byte v2, p0, Lcom/google/d/a/a/fm;->m:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1169
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1173
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1174
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/kg;->d()Lcom/google/d/a/a/kg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/kg;

    invoke-virtual {v0}, Lcom/google/d/a/a/kg;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1175
    iput-byte v2, p0, Lcom/google/d/a/a/fm;->m:B

    goto :goto_0

    .line 1173
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 1179
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1180
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fp;->d()Lcom/google/d/a/a/fp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fp;

    invoke-virtual {v0}, Lcom/google/d/a/a/fp;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1181
    iput-byte v2, p0, Lcom/google/d/a/a/fm;->m:B

    goto :goto_0

    .line 1179
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1185
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/fm;->m:B

    move v2, v3

    .line 1186
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 1227
    iget v0, p0, Lcom/google/d/a/a/fm;->n:I

    .line 1228
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1273
    :goto_0
    return v0

    .line 1231
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_d

    .line 1232
    iget v0, p0, Lcom/google/d/a/a/fm;->b:I

    .line 1233
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1235
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 1236
    iget-boolean v3, p0, Lcom/google/d/a/a/fm;->c:Z

    .line 1237
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 1239
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 1240
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/d/a/a/fm;->d:I

    .line 1241
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1243
    :cond_2
    iget v3, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 1244
    iget v3, p0, Lcom/google/d/a/a/fm;->e:I

    .line 1245
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1247
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 1248
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/d/a/a/fm;->f:F

    .line 1249
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    .line 1251
    :cond_4
    iget v3, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 1252
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/d/a/a/fm;->g:F

    .line 1253
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    :cond_5
    move v3, v2

    move v4, v0

    .line 1255
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 1256
    const/4 v5, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    .line 1257
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1255
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_6
    move v0, v1

    .line 1233
    goto/16 :goto_1

    :cond_7
    move v3, v1

    .line 1241
    goto :goto_3

    :cond_8
    move v3, v1

    .line 1245
    goto :goto_4

    :cond_9
    move v3, v2

    .line 1259
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 1260
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    .line 1261
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1259
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 1263
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_b

    .line 1264
    const/16 v0, 0x9

    iget v3, p0, Lcom/google/d/a/a/fm;->j:F

    .line 1265
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 1267
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/fm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_c

    .line 1268
    iget v0, p0, Lcom/google/d/a/a/fm;->k:F

    .line 1269
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 1271
    :cond_c
    iget-object v0, p0, Lcom/google/d/a/a/fm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1272
    iput v0, p0, Lcom/google/d/a/a/fm;->n:I

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/google/d/a/a/fm;->newBuilder()Lcom/google/d/a/a/fo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/fo;->a(Lcom/google/d/a/a/fm;)Lcom/google/d/a/a/fo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/google/d/a/a/fm;->newBuilder()Lcom/google/d/a/a/fo;

    move-result-object v0

    return-object v0
.end method
