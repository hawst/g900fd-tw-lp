.class public final Lcom/google/d/a/a/du;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/dv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ds;",
        "Lcom/google/d/a/a/du;",
        ">;",
        "Lcom/google/d/a/a/dv;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/google/d/a/a/ds;->e:Lcom/google/d/a/a/ds;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 453
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/du;->d:Lcom/google/n/ao;

    .line 315
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 354
    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 366
    :goto_0
    return-object p0

    .line 355
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 356
    iget-wide v2, p1, Lcom/google/d/a/a/ds;->b:J

    iget v4, p0, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/du;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/du;->b:J

    .line 358
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 359
    iget-wide v2, p1, Lcom/google/d/a/a/ds;->c:J

    iget v4, p0, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/du;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/du;->c:J

    .line 361
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 362
    iget-object v0, p0, Lcom/google/d/a/a/du;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 363
    iget v0, p0, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/du;->a:I

    .line 365
    :cond_3
    iget-object v0, p1, Lcom/google/d/a/a/ds;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 355
    goto :goto_1

    :cond_5
    move v2, v1

    .line 358
    goto :goto_2

    :cond_6
    move v0, v1

    .line 361
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/d/a/a/du;->c()Lcom/google/d/a/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 306
    check-cast p1, Lcom/google/d/a/a/ds;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 370
    iget v0, p0, Lcom/google/d/a/a/du;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 384
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 370
    goto :goto_0

    .line 374
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/du;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 376
    goto :goto_1

    :cond_2
    move v0, v1

    .line 374
    goto :goto_2

    .line 378
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/du;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 379
    iget-object v0, p0, Lcom/google/d/a/a/du;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 381
    goto :goto_1

    :cond_4
    move v0, v1

    .line 378
    goto :goto_3

    :cond_5
    move v0, v2

    .line 384
    goto :goto_1
.end method

.method public final c()Lcom/google/d/a/a/ds;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332
    new-instance v2, Lcom/google/d/a/a/ds;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ds;-><init>(Lcom/google/n/v;)V

    .line 333
    iget v3, p0, Lcom/google/d/a/a/du;->a:I

    .line 335
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 338
    :goto_0
    iget-wide v4, p0, Lcom/google/d/a/a/du;->b:J

    iput-wide v4, v2, Lcom/google/d/a/a/ds;->b:J

    .line 339
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 340
    or-int/lit8 v0, v0, 0x2

    .line 342
    :cond_0
    iget-wide v4, p0, Lcom/google/d/a/a/du;->c:J

    iput-wide v4, v2, Lcom/google/d/a/a/ds;->c:J

    .line 343
    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 344
    or-int/lit8 v0, v0, 0x4

    .line 346
    :cond_1
    iget-object v3, v2, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/du;->d:Lcom/google/n/ao;

    .line 347
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/du;->d:Lcom/google/n/ao;

    .line 348
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 346
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 349
    iput v0, v2, Lcom/google/d/a/a/ds;->a:I

    .line 350
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
